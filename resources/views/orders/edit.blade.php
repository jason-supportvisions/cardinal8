
@extends('templates.crm')

@section('content')




<div class="row">
    <div class="col-sm-10 col-sm-offset-2">
        <h3>
            Edit Order {{ $job->JobID }}
        </h3>
    </div>
</div>

{{ Form::open(['class'=>'form-horizontal']) }}

    @include('orders.edit-create')

{{ Form::close() }}

<div class="row" style="margin-top: 40px">
    <div class="col-sm-10 col-sm-offset-2">
        <h3>Order History</h3>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Stage</th>
                    <th>Set On</th>
                    <th>Set By</th>
                </tr>
            </thead>
            <tbody>
            @forEach($job->history as $change)
                <tr>
                    <td>
                        {{ $change->stage->Name }}
                        @if($substage = $change->substage)
                            - {{ $substage->name }}
                        @endif
                    </td>
                    <td>
                        {{-- {{ $change->created_at->format('m/d/Y T') }}  --}}
                    @php
                         $change->created_at->setTimezone(new DateTimeZone('EST'));
                        echo "" . $change->created_at->format('m/d/Y H:i');    
                    @endphp 
                    </td>
                <td>
                    @if(isset($change->user->FirstName))
                        {{ $change->user->FirstName }}
                    @endif
                    @if(isset($change->user->LastName))
                        {{$change->user->LastName}}
                    @endif
                </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>


@stop


@section('scripts')


@stop


@section('styles')

@stop

