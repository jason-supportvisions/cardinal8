@extends('templates.crm')

@section('content')

<h3>
    {{ $page_title }}
    <a class="btn btn-default btn-sm" href="/crm/orders/download?stage_ids={{ $stage_ids_list }}">
        Excel Export
        <i class='glyphicon glyphicon-download'></i>
    </a>
</h3>

<br>

<table id="orders-table" class="display"></table>

@stop


@section('scripts')

<style> 

.substage-box{ 
    width: 20px; 
    height: 20px;
    display: inline-block;
    margin: 0 5px;
    border: 2px solid #666;
    border-radius: 3px;
}

</style>

<script>

    var url_current  = "{{ URL::current() }}";

    var dataSet = "{{ $jobs->toJson(); }}";

    var substages = "{{ isset($substages) ? $substages->toJson() : 'null' }}";

    $('#orders-table').DataTable({
        "pageLength": 25,
        "aaData": dataSet,
        "aoColumns": [
            {
                "sTitle": "Status",
                "mDataProp": "stage.Name",
                "render": function(mData, type, full, meta){
                    return "<button class='btn btn-xs btn-"+full.stage.color+"' style='width:100px;'><strong>"+full.stage.Name+"</strong></button>";
                }
            },{
                "sTitle": "Substatus",
                "mDataProp": "summary_link",
                "render": function(mData, type, full, meta){
                    var response = "";
                    if(!substages){response;}

                    $(substages).each(function(val){
                        var opacity = 0.30;
                        if(substage = full.substage){
                            opacity = substage.order >= this.order ? 1 : 0.30;
                        }
                        response += "<div class='substage-box "+this.slug+"' style='background-color: "+this.color+"; opacity: "+opacity+"' data-toggle='tooltip' title='"+this.name+"'></div>";
                    });
                    return response;
                },
                "visible": substages
            },{
                "sTitle": "Job #",
                "mDataProp": "summary_link"
            },{
                "sTitle": "Reference",
                "mDataProp": "Reference"
            },{
                "sTitle": "Account",
                "mDataProp": "account.Name"
            },{
                "sTitle": "Brnd",
                "mDataProp": "brand.abbreviation"
            },{
                "sTitle": "Fin",
                "mDataProp": "Fin"
            },{
                "sTitle": "Net",
                "mDataProp": "Net",
                "className": "dt-right"
            },{
                "sTitle": "Deposit",
                "mDataProp": "Deposit",
                "render": function(mData, type, full, meta){
                    return mData ? "<span class='glyphicon glyphicon-ok'></span><span style='display: none'>1</span>" : ""
                },
                "className": "dt-center"
            },{
                "sTitle": "RSO",
                "mDataProp": "rso_date"
            },{
                "sTitle": "Ship",
                "mDataProp": "EstOutProduction"
            },{
                "sTitle": "Edit",
                "mDataProp": "JobID",
                "className": "dt-center",
                "orderable": false,
                "render": function(mData, type, full, meta){
                    return "<a href='"+url_current+"/"+full.JobID+"/edit'><span title='edit' class='glyphicon glyphicon-pencil'></span></a>";
                }
            },{
                "sTitle": "To CV",
                "orderable": false,
                "mDataProp": "JobID",
                "className": "dt-center",
                "render": function(mData, type, full, meta){
                    return "<a target='_BLANK' href='/crm/orders/"+full.JobID+"/to-cv/preview'><span title='export to cv' class='glyphicon glyphicon-download'></span></a>";
                }
            }
        ],
        "order": [[ 2, "desc" ]]
    });

</script>

@stop


@section('styles')

@stop

