    <div class="form-group">
        <label for="Brand" class="col-sm-2 control-label">Brand</label>
        <div class="col-sm-10">
            {{ Form::select('Brand', $brands_list, $job->Brand, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('Brand', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="Account" class="col-sm-2 control-label">Account</label>
        <div class="col-sm-10">
            {{ Form::select('AccountID', $accounts_list, $job->AccountID, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('AccountID', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="Reference" class="col-sm-2 control-label">Reference</label>
        <div class="col-sm-10">
            {{ Form::text('Reference', $job->Reference, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('Reference', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="List" class="col-sm-2 control-label">List</label>
        <div class="col-sm-10">
            {{ Form::text('List', $job->List, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('List', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="Net" class="col-sm-2 control-label">Net</label>
        <div class="col-sm-10">
            {{ Form::text('Net', $job->Net, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('Net', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>
 
    <div class="form-group">
        <label for="Net" class="col-sm-2 control-label">Deposit</label>
        <div class="col-sm-10">
            {{ Form::checkbox('Deposit', 1, $job->Deposit) }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Accepted Date</label>
        <div class="col-sm-10">
            {{ Form::text('AcceptedDate', $job->AcceptedDate, ['class'=>'form-control', 'placeholder'=>'mm/dd/yyyy']) }}
            @php echo html_entity_decode($errors->first('AcceptedDate', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Est Ship Date</label>
        <div class="col-sm-10">
            @php 
                if($job->EstOutProduction){
                    $shipDate = $job->EstOutProduction->format('m/d/Y');
                }else{
                    $shipDate = Null;
                }
            @endphp
            {{ Form::text('EstOutProduction', $shipDate, ['class'=>'form-control', 'placeholder'=>'mm/dd/yyyy']) }}
            @php echo html_entity_decode($errors->first('EstOutProduction', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    <div class="form-group">
        <label for="Stage" class="col-sm-2 control-label">Stage</label>
        <div class="col-sm-10">
            {{ Form::select('Stage', $stages_list, $job->Stage, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('Stage', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>

    @if($substages_array = $job->stage->getSubstagesArray())
    <div class="form-group">
        <label for="Stage" class="col-sm-2 control-label">Substage</label>
        <div class="col-sm-10">
            {{ Form::select('substage_id', $substages_array, $job->substage_id, array('class'=>'form-control')) }}
            @php echo html_entity_decode($errors->first('substage_id', '<br><p class="text-danger">:message</p>')); @endphp
        </div>
    </div>
    @endif

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" class="btn btn-primary">
        </div>
    </div>