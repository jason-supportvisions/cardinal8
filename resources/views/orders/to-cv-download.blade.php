[Header]
Version=4
Unit=0
Name="{{ $job->JobID }}"
Description="{{ $job->Reference }}"
PurchaseOrder="{{ $job->JobID }}"
Comment=""
Customer="{{ $job->account->Name }}"
Contact="{{ $job->account->users->pluck('FristName') }}"
Address1="{{ $job->account->AddressLine1 }}"
Address2="{{ $job->account->AddressLine2 }}"
City="{{ $job->account->City }}"
State="{{ $job->account->State }}"
Zip="{{ $job->account->Zip }}"
Phone="{{ $job->account->Phone }}"
Fax=""
ShipToComment=""
ShipToContact=""
ShipToAddress1=""
ShipToAddress2=""
ShipToCity=""
ShipToState=""
ShipToZip=""
ShipToPhone=""
ShipToFax="" 
<?php print_r($job->construction); ?>
@if ($job->door != null)
BaseDoors="{{ $job->door->DoorTypeOrdOut }}","{{ $job->door->DoorNameOrdOut }}","","","","","Door.ddb"
WallDoors="{{ $job->door->DoorTypeOrdOut }}","{{ $job->door->DoorNameOrdOut }}","","","","","Door.ddb"
DrawerFront="{{ $job->door->DrawerTypeOrdOut }}","{{ $job->door->DrawerNameOrdOut }}","","","","","Door.ddb"
BaseEndPanels="{{ $job->door->DoorTypeOrdOut }}","{{ $job->door->DoorNameOrdOut }}","","","","","Door.ddb"
WallEndPanels="{{ $job->door->DoorTypeOrdOut }}","{{ $job->door->DoorNameOrdOut }}","","","","","Door.ddb"
TallEndPanels="{{ $job->door->DoorTypeOrdOut }}","{{ $job->door->DoorNameOrdOut }}","","","","","Door.ddb" 
@endif 
@if($job->construction != null) 
DrawerBoxConstruction="{{ $job->construction->cabinetDrawerBox->drawer_box_construction }}"
RollOutConstruction="{{ $job->construction->cabinetDrawerBox->rollout_construction }}"
BaseCabinetMaterials="{{ $job->base_cabinet_material }}"
WallCabinetMaterials="{{ $job->wall_cabinet_material }}"
BaseExposedCabinetMaterials="{{ $job->base_exposed_cabinet_material }}"
WallExposedCabinetMaterials="{{ $job->wall_exposed_cabinet_material }}"
DrawerBoxMaterials="{{ $job->construction->cabinetDrawerBox->drawer_box_material }}"
RollOutMaterials="{{ $job->construction->cabinetDrawerBox->rollout_material }}" 
@endif
PullMaterials="-NONE/-NONE"
HingeMaterials="Blum Blumotion 110" 
@if($job->construction != null)
GuideMaterials="{{ $job->construction->cabinetDrawerBox->guide_material }}" 
@endif
InteriorFinish=""
ExteriorFinish=""
CabinetConstruction="CoryMfg Current"

[Catalog]
Name="CM_Standard_v4"

@foreach($job->products as $product)
@include('partials.to-cv-cabinet', compact('product'))

@endforeach