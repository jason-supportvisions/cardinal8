@extends('templates.crm')

@section('content')
    <h3>
        {{ $page_title }}
    </h3>

    <br>
    
    <table id="orders-table" class="display">
        <thead>
            <tr>
                <th class=" text-left">Status</th>
                @if($page_title =='Quotes')
                <th >Substatus</th>
                @endif
                <th class=" text-right" style="width: 46px;">Job #</th>
                <th class=" text-center">Reference</th>
                <th class="text-center">Brnd</th>
                <th class=" text-center">Fin</th>
                <th class=" text-center">Net</th>
                <th class=" text-center">Deposit</th>
                <th class=" text-center">RSO</th>
                <th class=" text-center">Ship</th>
                <th class="text-center">Edit</th>
                <th class="text-center" >To CV</th>
                <th class="text-center" >Cab Card</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($jobs as $row)
                <tr class="">
                    <td class=" Status text-left"><button class='btn btn-xs btn-{{ $row->stage->color }}'
                            style='width:100px;'><strong>{{ $row->stage->Name }}</strong></button></td>
                    @if($page_title =='Quotes')
                    <td style="width: 100px;">
                    <?php
                        $opacity = 0.3;
                    ?>
                    @foreach($substages as $substage)
                        <div class="substage-box {{$substage->slug}}" style="background-color: <?php echo $substage->color; ?>; opacity:<?php echo $opacity; ?>}}" data-toggle='tooltip' title='{{$substage->name}}'></div>
                    @endforeach
                    </td>
                    @endif
                    <td class="JobID text-center"> 
                        {{-- <a href="{{route('quote_orders.summary', ['jobID' => $row->JobID]) }}">{{ $row->JobID }} </a>  --}}
                        <a href="/quote_orders/summaryPdf/{{ $row->JobID }}" target="__BLANK">{{ $row->JobID }} </a>
                    </td>
                    <td class=" Reference text-center">{{ $row->Reference }}</td>
                    <td class=" Brnd text-center">{{ $row->brand->abbreviation }}</td>
                    <td class=" Fin text-center">{{ $row->Fin }}</td>
                    <td class=" Net text-right">
                        @if(number_format($row->Net, 2) != 0)
                            ${{ number_format($row->Net, 2) }}
                        @endif
                    </td>
                    <td class=" Deposit text-center"><?php isset($row->Deposit) ? "<span
                            class='glyphicon glyphicon-ok'></span><span style='display: none'>1</span>" : ''; ?></td>
                    <td class=" RSO text-right">{{ $row->rso_date }} </td>
                    <td class=" Ship text-right"> <?php echo isset($row->EstOutProduction) ?
                        $row->EstOutProduction->format('m/d/y') : ''; ?></td>
                    <td class=" Edit text-center"><a href='/crm/orders/{{ $row->JobID }}/edit'><span title='edit'
                                class='glyphicon glyphicon-pencil'></span></a></td>
                    <td class=" tocv text-center"><a target='_BLANK'
                            href='/crm/orders/{{ $row->JobID }}/to-cv/preview'><span title='export to cv'
                                class='glyphicon glyphicon-download'></span></a></td>
                    <td class=" Cabcard text-center">
                    <a target='_BLANK' href="{{ route('cabinet.pdf', ['JobID' => $row->JobID])}}"><span title='print cabinet cards' class='glyphicon glyphicon-download'></span></a></td>
                </tr>
            @endforeach
        </tbody>
    </table>
@stop


@section('scripts')
    <script>
        var initial_ordered_column = <?php echo $initial_ordered_column; ?>;
        $(document).ready(function() {
            if(initial_ordered_column == 2){
                $('#orders-table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'colvis',
                        'excel',
                        'print',
                        'csv'
                    ],
                  
                    "order": [[ initial_ordered_column, "desc" ]],
                    "pageLength": 20,
                    "columnDefs": [
                        {
                            "targets": [ 10 ],
                            "orderable": false,
                            "searchable": false
                        },
                        {
                            "targets": [ 11 ],
                            "orderable": false,
                            "searchable": false
                        },
                        {
                            "targets": [ 12 ],
                            "orderable": false,
                            "searchable": false
                        }
                    ]
                })
            }else{
                $('#orders-table').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'colvis',
                        'excel',
                        'print'
                    ],                  
                    "order": [[ initial_ordered_column, "desc" ]],
                    "pageLength": 20,
                    "columnDefs": [
                        {
                            "targets": [ 9 ],
                            "orderable": false,
                            "searchable": false
                        },
                        {
                            "targets": [ 10 ],
                            "orderable": false,
                            "searchable": false
                        },
                        {
                            "targets": [ 11 ],
                            "orderable": false,
                            "searchable": false
                        }
                    ]
                })
            }
        });

    </script>

@stop


@section('styles')
 <style>
        .substage-box {
            width: 20px;
            height: 20px;
            display: inline-block;
            margin: 0 3px;
            border: 2px solid #666;
            border-radius: 3px;
        }

    </style>
    
@stop
