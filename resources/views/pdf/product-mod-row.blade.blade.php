
<tr>
    <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
        {{-- {{ $modification->Line }} --}}
    </td>
    <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
        {{ $modification->quantity }}
    </td>
    <td style="white-space:nowrap;border-right:1px solid #ddd; padding-left:40px;">
        {{ $modification->modification_item->Code }} 
        @if($modification->Value1)
      
        <small>
            @if($modification->modification_item->Value1Type == "meas")
            ({{ $modification->cv_value }}")
            @endif
        </small>
        @endif
    </td>
    <td style="white-space:nowrap;border-right:1px solid #ddd;">
    </td>
    <td>
        {{ $modification->modification_item->Description }}
        @if($modification->Notes) 
        <br>
        <small>{{ $modification->Notes }}</small>
        @endif
    </td>
    <td class="price-td LIST @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->ListQty }}</i></small>
    </td>
    <td class="price-td LIST @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{ $modification->ListQty }}</i></small>
    </td>
    <td class="price-td COST @if($pricing!='COST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->cost_per }}</i></small>
    </td>
    <td class="price-td COST @if($pricing!='COST'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{ $modification->cost_qty }}</i></small>
    </td>
    <td class="price-td CUSTOMER @if($pricing!='CUSTOMER'  || $stageHide == TRUE) hide @endif"style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->customer_per }}</i></small>
    </td>
    <td class="price-td CUSTOMER @if($pricing!='CUSTOMER'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{ $modification->customer_qty }}</i></small>
    </td>

    
</tr>