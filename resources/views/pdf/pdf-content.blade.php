@include('pdf.info_panels')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-UX">
            @include('pdf.list_table', ['editable'=>false])
        </div>
    </div>
</div>

@include('pdf.summary') 