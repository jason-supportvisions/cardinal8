@extends('templates.products')

@section('content')

{{ Form::open() }}
{{ Form::submit('Save', ['class'=>'btn btn-primary']) }}
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Category</th>
            <th>CARDINAL Code</th>
            <th>CV Code</th>
            <th>Description</th>
            <th>Note</th>
            <th>CARDINAL Type</th>
            <th>CV<br>Type</th>
            <th>Bool Val</th>
            <th>Min</th>
            <th>Max</th>
            <th>List</th>
            <th>Allow Price Customization</th>
            <th>Active</th>
        </tr>
    </thead>
    <tbody>
        @foreach($mods as $mod)
        <tr>
            <td>
                {{ Form::select("mods[{$mod->ID}][Type]", $mod_types, $mod->Type, ['style'=>'min-width: 80px; width: 100px']) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][Code]", $mod->Code, ['style'=>'min-width: 80px; width: 140px']) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][CVCode]", $mod->CVCode, ['style'=>'min-width: 80px; width: 140px']) }}
            </td>
            <td>
                {{ Form::textarea("mods[{$mod->ID}][Description]", $mod->Description, ['style'=>'min-width: 230px; width: 230px; height: 75px']) }}
            </td>
            <td>
                {{ Form::textarea("mods[{$mod->ID}][Note]", $mod->Note, ['style'=>'min-width: 200px; width: 200px; height: 75px']) }}
            </td>
            <td>
                {{ Form::select("mods[{$mod->ID}][Value1Type]", $formats, $mod->Value1Type) }}
            </td>
            <td>
                {{ Form::select("mods[{$mod->ID}][cv_type]", $formats, $mod->cv_type) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][bool_val]", $mod->bool_val, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][Value1Min]", $mod->Value1Min, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][Value1Max]", $mod->Value1Max, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("mods[{$mod->ID}][List]", $mod->List, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::checkbox("mods[{$mod->ID}][allow_price_customization]", 1, $mod->allow_price_customization) }}
            </td>
            <td>
                {{ Form::checkbox("mods[{$mod->ID}][Active]", 1, $mod->Active) }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}
@stop
