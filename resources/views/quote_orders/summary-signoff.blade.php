@extends('templates.pdf')

@section('content')

    @include('pdf.pdf-content')

    @include('partials.summary-signoff-content')

@stop
