<?php
use App\Model\Job;

$jobID = 0;     
$jobAcct = 0;   
$job_stages;
$job_types; 
$states;    
$jobResults;
$cusResults;
$shipResults;
$smod;

if(isset($data['jobID'])){
    $jobID          = $data['jobID'];
    $jobAcct        = $data['job_account']; 
    $job_stages     = $data['job_stages'];
    $job_types      = $data['job_types'];
    $states         = $data['states'];
    $jobResults     = $data['jobResults'];
    $cusResults     = $data['cusResults'];
    $shipResults    = $data['shipResults'];
    $smod           = $data['smod'];
    $job = Job::find($jobID);
    $job_customer_multiplier = floatval($job->multiplier_customer);

}else{
    echo '<script>window.location = "/jobs";</script>';
}


$Title          = "CARDINAL | Job";
$Select2        = 0;
$Select2beta    = 1;
$DataTables     = 0;
$jQuery_UI      = 0;
$jQuery_Validate = 1;
$jobPrint       = 0;
$jobPricing     = 0;
$pageid         = 'Header';
$brand          = 1;
$userid         = session('userid');;

$SesUser        = session('userid');
$SesAcct        = session('accountid');
$SesType        = session('accounttype');

date_default_timezone_set('America/New_York');
$created_at = $updated_at = date('Y-m-d H:i:s');

?>
<!DOCTYPE html>
<html lang="en">
@include('layouts.header')

<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        <form action="/quote_orders/store" id="header-form" method="post">
            @csrf
            @include('templates.progress')
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-UX">
                        <!--<div class="panel-heading">Job Information</div>-->
                        <div class="panel-body">
                            <div class="col-sm-2">
                                <div class="form-group">
                                    <label for="jobAcct">Account<span class="text-red"> * </span></label>
                                    <select class="form-control " id="jobAcct" name="jobAcct">
                                        <?php if(session('accounttype') == -1) {
                                            foreach ($jobAcct as $row) {
                                                if ($row->ID == $jobResults->AccountID) { ?>
                                                    <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name ; ?></option>
                                                <?php } else { ?>
                                                    <option value='<?php echo $row->ID; ?>'><?php echo $row->Name ; ?></option>
                                                <?php }
                                            }
                                        }else{
                                            foreach ($jobAcct as $row) {
                                                if ($row->ID == $jobResults->AccountID) { ?>
                                                    <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name ; ?></option>
                                                <?php }
                                            }
                                        } ?>                                    
                                    </select>
                                    <input name="brand" type="hidden" value="<?php echo $brand = 1; ?>" />
                                    <input name="userid" type="hidden" value="<?php echo $userid ; ?>" />
                                    <input name="created_at" type="hidden" value="<?php echo $created_at; ?>" />
                                    <input name="jobID" type="hidden" value="<?php echo $jobID; ?>" />
                                    <input name="jobmod" type="hidden" value="<?php echo $smod; ?>" />
                                </div>
                            </div>
                            <div class="col-sm-2 form-group">
                                <label for="jobType">&nbsp;&nbsp;Type<span class="text-red"> * </span></label>
                                <select class="form-control" id="jobType" name="jobType">
                                    <?php foreach ($job_types as $row) {
                                        if ($row->ID == $jobResults->Type) { ?>
                                            <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                        <?php } else { ?>
                                            <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                    <?php }
                                    } ?>
                                </select>
                            </div>
                            <div class="col-sm-2 form-group">
                                <label for="jobStage">&nbsp;&nbsp;Stage<span class="text-red"> * </span></label>
                                <select class="form-control" id="jobStage" name="jobStage">
                                    <?php if(session('accounttype') == -1) {
                                             foreach ($job_stages as $row) {
                                            if ($row->ID == $jobResults->Stage) { ?>
                                                <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                            <?php } else { ?>
                                                <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                        <?php   }
                                            } 
                                        }else{ 
                                            if($jobResults->Stage == 2){ ?>
                                                <option value="1">Quote</option>
                                                <option value="2" selected>Comfirmed</option>
                                        <?php }else{ ?>
                                                <option value="1" selected>Quote</option>
                                                <option value="2">Comfirmed</option>
                                     <?php }
                                        } ?>
                                </select>
                            </div>
                            <div class="col-sm-2 form-group">
                                <label for="jobMultiplier">&nbsp;&nbsp;% Multiplier<span class="text-red"> * </span></label>
                                <select class="form-control" id="jobMultiplier" name="jobMultiplier">        
                            <?php  
                                    $printI = "";
                                    $i = "1.00";
                                    $job_customer_multiplier = floatval($job_customer_multiplier);
                                    
                                    
                                    do {
                                        $i = number_format($i ,2);
                                        $i = strval($i);
                                        $printI = $i;    
                                        $i = floatval($i);
                                        if($printI == strval($job_customer_multiplier)){
                                            echo "<option value='$printI' selected>" . $printI . "</option>";
                                        }else{
                                            echo "<option value='$printI'>" . $printI . "</option>";
                                        }
                                        $i = $i + 0.05;
                                    } while ($i <= 2.0);
                                ?>
                                </select>
                            </div>

                            <div class="col-sm-4 form-group">
                                <label for="jobRef">&nbsp;&nbsp;Job Name<span class="text-red"> * </span></label>
                                <input class="form-control" id="jobRef" name="jobRef" placeholder="Job Name/Number" type="text" id="jobRef" value="{{ $jobResults->Reference }}"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           <?php if($cusResults != '1') { ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-UX">
                        <div class="panel-heading">Customer Information</div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusCompany">Company</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="cusCompany" name="cusCompany" placeholder="Company Name" type="text" value="{{ $cusResults->Company ?? '' }}"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusName">Contact</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="cusName" name="cusName" placeholder="First & Last Name" type="text" value="{{ $cusResults->ContactName ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusAdd1">Address</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusAdd1" name="cusAdd1" placeholder="Address line 1" type="text" value="{{ $cusResults->AddressLine1 ?? '' }}" />
                                        <input class="form-control" id="cusAdd2" name="cusAdd2" placeholder="Address line 2" type="text" value="{{ $cusResults->AddressLine2 ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusCity">City</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusCity" name="cusCity" placeholder="City" type="text" value="{{ $cusResults->City ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusState">State</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="cusState" name="cusState">
                                            <?php if($cusResults == null || $cusResults->State == ''){ 
                                                foreach ($states as $row) {
                                                    if ($row->ID == 'MA') { ?>
                                                        <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                    <?php } else { ?>
                                                        <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                                <?php }
                                                } 
                                            }else{
                                                foreach ($states as $row) {
                                                    if ($row->ID == $cusResults->State) { ?>
                                                        <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                    <?php } else { ?>
                                                        <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                                <?php }
                                                } 
                                            }
                                            
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusZip">Zip Code</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusZip" name="cusZip" placeholder="Zip Code" type="text" value="{{ $cusResults->PostalCode ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusPhone">Phone</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusPhone" name="cusPhone" placeholder="Phone Number" type="tel" value="{{ $cusResults->PhoneNumber ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusEmail">Email</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusEmail" name="cusEmail" placeholder="Email Address" type="email" value="{{ $cusResults->EmailAddress ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <label class="col-sm-2 control-label" for="cusNotes">Notes</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="cusNotes" name="cusNotes">{{ $cusResults->Notes ?? '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-UX">
                        <div class="panel-heading">Shipping Information</div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="pull-right">
                                    <label>
                                        <input name="shippingtoo" onclick="fillShipping(this.form)" type="checkbox" />
                                        Same as Customer
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipCompany">Company</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="shipCompany" name="shipCompany" placeholder="Company Name" type="text" value="{{ $shipResults->Company ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipName">Contact</label>

                                    <div class="col-sm-6">
                                        <input class="form-control" id="shipName" name="shipName" placeholder="First & Last Name" type="text" value="{{ $shipResults->ContactName ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipAdd1">Address</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipAdd1" name="shipAdd1" placeholder="Address line 1" type="text" value="{{ $shipResults->AddressLine1 ?? '' }}" />
                                        <input class="form-control" id="shipAdd2" name="shipAdd2" placeholder="Address line 2" type="text" value="{{ $shipResults->AddressLine2 ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipCity">City</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipCity" name="shipCity" placeholder="City" type="text" value="{{ $shipResults->City ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipState">State</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="shipState" name="shipState">
                                        <?php if($shipResults == null || $shipResults->State == ''){ 
                                                foreach ($states as $row) {
                                                    if ($row->ID == 'MA') { ?>
                                                        <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                    <?php } else { ?>
                                                        <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                                <?php }
                                                } 
                                            }else{
                                                foreach ($states as $row) {
                                                    if ($row->ID == $shipResults->State) { ?>
                                                        <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                    <?php } else { ?>
                                                        <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                                <?php }
                                                } 
                                            }
                                            
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipZip">Zip Code</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipZip" name="shipZip" placeholder="Zip Code" type="text" value="{{ $shipResults->PostalCode ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipPhone">Phone</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipPhone" name="shipPhone" placeholder="Phone Number" type="tel" value="{{ $shipResults->PhoneNumber ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipEmail">Email</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipEmail" name="shipEmail" placeholder="Email Address" type="email" value="{{ $shipResults->EmailAddress ?? '' }}" />
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <label class="col-sm-2 control-label" for="shipNotes">Notes</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="shipNotes" name="shipNotes">{{ $shipResults->Notes ?? '' }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php }else{ ?>
            <div class="row">
                <div class="col-sm-6">
                    <div class="panel panel-UX">
                        <div class="panel-heading">Customer Information</div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusCompany">Company</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="cusCompany" name="cusCompany" placeholder="Company Name" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusName">Contact</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="cusName" name="cusName" placeholder="First & Last Name" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusAdd1">Address</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusAdd1" name="cusAdd1" placeholder="Address line 1" type="text" />
                                        <input class="form-control" id="cusAdd2" name="cusAdd2" placeholder="Address line 2" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusCity">City</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusCity" name="cusCity" placeholder="City" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusState">State</label>

                                    <div class="col-sm-10">
                                        <select class="form-control" id="cusState" name="cusState">
                                            <?php foreach ($states as $row) {
                                                if ($row->ID == 'ST') { ?>
                                                    <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                <?php } else { ?>
                                                    <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusZip">Zip Code</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusZip" name="cusZip" placeholder="Zip Code" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusPhone">Phone</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusPhone" name="cusPhone" placeholder="Phone Number" type="tel" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="cusEmail">Email</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="cusEmail" name="cusEmail" placeholder="Email Address" type="email" />
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <label class="col-sm-2 control-label" for="cusNotes">Notes</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="cusNotes" name="cusNotes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="panel panel-UX">
                        <div class="panel-heading">Shipping Information</div>
                        <div class="panel-body">
                            <div class="form-horizontal">
                                <div class="pull-right">
                                    <label>
                                        <input name="shippingtoo" onclick="fillShipping(this.form)" type="checkbox" />
                                        Same as Customer
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipCompany">Company</label>
                                    <div class="col-sm-6">
                                        <input class="form-control" id="shipCompany" name="shipCompany" placeholder="Company Name" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipName">Contact</label>

                                    <div class="col-sm-6">
                                        <input class="form-control" id="shipName" name="shipName" placeholder="First & Last Name" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipAdd1">Address</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipAdd1" name="shipAdd1" placeholder="Address line 1" type="text" />
                                        <input class="form-control" id="shipAdd2" name="shipAdd2" placeholder="Address line 2" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipCity">City</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipCity" name="shipCity" placeholder="City" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipState">State</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" id="shipState" name="shipState">
                                            <?php foreach ($states as $row) {
                                                if ($row->ID == 'ST') { ?>
                                                    <option value='<?php echo $row->ID; ?>' selected><?php echo $row->Name; ?></option>
                                                <?php } else { ?>
                                                    <option value='<?php echo $row->ID; ?>'><?php echo $row->Name; ?></option>
                                            <?php }
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipZip">Zip Code</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipZip" name="shipZip" placeholder="Zip Code" type="text" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipPhone">Phone</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipPhone" name="shipPhone" placeholder="Phone Number" type="tel" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="shipEmail">Email</label>

                                    <div class="col-sm-10">
                                        <input class="form-control" id="shipEmail" name="shipEmail" placeholder="Email Address" type="email" />
                                    </div>
                                </div>
                                <div class="form-group" style="margin-bottom:0px;">
                                    <label class="col-sm-2 control-label" for="shipNotes">Notes</label>

                                    <div class="col-sm-10">
                                        <textarea class="form-control" id="shipNotes" name="shipNotes"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </form>
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')

    <script type="text/javascript">
        function fillShipping(f) {
            if (f.shippingtoo.checked == true) {
                f.shipName.value = f.cusName.value;
                f.shipCompany.value = f.cusCompany.value;
                f.shipAdd1.value = f.cusAdd1.value;
                f.shipAdd2.value = f.cusAdd2.value;
                f.shipCity.value = f.cusCity.value;
                f.shipState.value = f.cusState.value;
                f.shipZip.value = f.cusZip.value;
                f.shipPhone.value = f.cusPhone.value;
                f.shipEmail.value = f.cusEmail.value;
            }
        }
        $('#header-form').validate({
            rules: {
                jobRef: {
                    minlength: 3,
                    //maxlength: 15,
                    required: true
                },
                jobAcct: {
                    required: true
                },
                jobType: {
                    required: true
                },
                jobStage: {
                    required: true
                },
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'control-label',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
    </script>
</body>

</html>