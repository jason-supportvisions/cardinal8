<?php
$Title          =  " | Uploads";
$Select2        = 0;
$Select2beta    = 1;
$DataTables     = 0;
$jQuery_UI      = 1;
$jQuery_Validate = 0;
$jobPrint       = 0;
$jobPricing     = 0;
$flag           = 0;

$pageid         = 'GeneralNotes';
$jobID          = $data[0];
$jobAcct        = $data[1];
$notes          = $data[2];
$UserID         = session('userid');
$SesUser        = session('userid');
$SesAcct        = session('accountid');
$SesType        = session('accounttype');
date_default_timezone_set('america/new_york');
$add_date       = date('Y-m-d H:i:s');
?>
<!DOCTYPE html>
<html lang="en">
@include('layouts.header')

<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        @include('templates.progress')
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-UX">
                    <div class="panel-heading">Documents: Floor plans, appliances, custom cabinets</div>
                    <form method="post" action="{{ url('quote_orders/file_upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body app_table_div">
                            <button type="button" class="btn btn-success app_add_btn"><i class="glyphicon glyphicon-plus-sign"></i> Add File</button>
                            {{-- <button type="button" class="btn btn-outline-dark print_btn" style="margin-top: 0; margin-right: 0;border-color:#ccc">
                                <span class="glyphicon glyphicon-print"></span> Print</button> --}}
                            <div class="card-body">
                                <div class="form-group">
                                    <table id="table" class="table table-striped table-responsive table-bordered" width="100%" cellspacing="0" style="margin-top: 10px;">
                                        <thead class="note_thead">
                                            <tr>
                                                <th class="text-left floor_type">Title</th>
                                                <th class="text-left floor_file">File</th>
                                                <th class="text-left floor_note">Note</th>
                                                <th class="text-left floor_icon">Timestamp</th>
                                            </tr>
                                        </thead>
                                            <tbody>
                                                @foreach( $notes as $row) 
                                                    <input id="fileName" type="hidden" value="{{$row->File }}" />
                                                    <tr>
                                                        <td class="text-left">{{ $row->Type }}</td>
                                                        <td class="text-left"><a target="_blank" href="{{ $filePath }}{{ $row->File }}"> download</a></td>
                                                        <td class="text-left">{!!html_entity_decode($row->Note)!!}</td>
                                                        <td class="text-left">{{ $add_date }}</td>
                                                        <td class="text-center">
                                                            <span class="btn-group">
                                                                <a class="btn btn-sm btn-warning" href="{{route('upload-edit', ['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'id'=>$row->ID ]) }}"><i class="glyphicon glyphicon-edit"></i></a>
                                                                <a class="btn btn-sm btn-danger user_delete" href="{{route('upload-delete', ['id'=>$row->ID ]) }}" onclick="return confirm('Do you really want remove this user entry?') "><i class="glyphicon glyphicon-remove"></i></a>                                               
                                                            </span>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel-body app_add_div">
                            <div class="col-sm-12">
                                <div class="col-sm-9"></div>
                                <div class="col-sm-3">
                                    <div class="btn-group mybtn-group" >
                                        <button type="button" class="btn btn-primary btn-danger app_add_cancel">Cancel</button>
                                        <button type="submit" class="btn btn-primary btn-success">Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"> <strong>Type</strong> </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="usr" name="type">
                                    <input name="jobID" type="hidden" value="<?php echo $jobID; ?>" />
                                    <input name="jobAcct" type="hidden" value="<?php echo $jobAcct; ?>" /> 
                                    <input name="userID" type="hidden" value="<?php echo $UserID; ?>" />
                                    <input name="ID" type="hidden" value="" />
                                </div>
                            </div>
                            <div class="col-sm-12 cusmargin">
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"><strong class="st_file">File</strong> </div>
                                <div class="col-sm-9">
                                    <input type="file" id="real-file" style="display: none;" name="loadfile" />
                                    
                                    <span id="custom-text"></span>
                                    <button type="button" id="custom-button" class="btn btn-primary btn-success"><i class="glyphicon glyphicon-circle-arrow-up"></i>Add File</button>
                                </div>
                            </div>
                            <div class="col-sm-12 cusmargin" >
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"><strong class="st_file">Note</strong> </div> 
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <textarea id="TypeHere" name="note" ></textarea>
                                </div>                                
                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>      
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'note' );
    </script>

<script type = "text/javascript">
    $(document).ready(function() {
        $(function() {
            $('#datetimepicker1').datetimepicker({
                format: 'YYYY-MM-DD HH:mm:ss'
            });
        });
        $(".app_add_btn").click(function() {
            $(".app_table_div").hide();
            $(".app_add_div").show();
        });
        $(".app_add_cancel").click(function() {
            $(".app_table_div").show();
            $(".app_add_div").hide();
        });
     
  
    });
    const realFileBtn = document.getElementById("real-file");
    const customBtn = document.getElementById("custom-button");
    const customTxt = document.getElementById("custom-text");

    customBtn.addEventListener("click", function() {
        realFileBtn.click();
        
    });

    realFileBtn.addEventListener("change", function() {
        if (realFileBtn.value) {
            customTxt.innerHTML = realFileBtn.value.match(
                /[\/\\]([\w\d\s\.\-\(\)]+)$/
            )[1];
        } else {
            customTxt.innerHTML = "No file";
        }
    });

    $('#table').DataTable({
            ordering: false,
            dom: 'Bfrtip',
            buttons: [
                'print'
            ]     
    });

</script>
</body>

</html>