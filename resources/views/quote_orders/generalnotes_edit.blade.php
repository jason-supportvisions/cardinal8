<?php
$Title          =  " | Uploads";
$Select2        = 0;
$Select2beta    = 1;
$DataTables     = 0;
$jQuery_UI      = 1;
$jQuery_Validate = 0;
$jobPrint       = 0;
$jobPricing     = 0;
$flag           = 0;

$pageid         = 'GeneralNotes';
$jobID          = $data[0];
$jobAcct        = $data[1];
$notes          = $data[2];

$UserID         = session('userid');
$SesUser        = session('userid');
$SesAcct        = session('accountid');
$SesType        = session('accounttype');
date_default_timezone_set('america/new_york');
$add_date       = date('Y-m-d H:i:s');
?>
<!DOCTYPE html>
<html lang="en">
@include('layouts.header')

<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        @include('templates.progress')
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-UX">
                    <div class="panel-heading">Documents: Floor plans, appliances, custom cabinets<span class="upload_edit_title"></span></div>
                    <form method="post" action="{{ url('quote_orders/file_upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="panel-body">
                            <div class="col-sm-12">
                                <div class="col-sm-9"></div>
                                <div class="col-sm-3">
                                    <div class="btn-group mybtn-group" >
                                        <button type="button" class="btn btn-primary btn-danger edit_cancel">Cancel</button>
                                        <button type="submit" class="btn btn-primary btn-success">Save</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"> <strong>Title</strong> </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="usr" name="type" value="{{ $notes->Type ?? '' }}">
                                    <input type="hidden" class="form-control" id="title" name="title" value="{{ $notes->Title ?? '' }}">
                                    <input name="jobID" type="hidden" value="<?php echo $jobID; ?>" />
                                    <input name="jobAcct" type="hidden" value="<?php echo $jobAcct; ?>" /> 
                                    <input name="userID" type="hidden" value="<?php echo $UserID; ?>" />
                                    <input name="ID" type="hidden" value="<?php echo $notes->ID; ?>" />
                                    <input name="loaddate" type='hidden' class="form-control data_input" value="{{$notes->Date ?? $add_date }}"/>
                                </div>
                            </div>
                            <div class="col-sm-12 cusmargin">
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"><strong class="st_file">File</strong> </div>
                                <div class="col-sm-9">
                                    <input type="file" id="real-file" style="display: none;" name="loadfile" />
                                    
                                    <span id="custom-text">{{ $notes->File }}</span>
                                    <button type="button" id="custom-button" class="btn btn-primary btn-success"><i class="glyphicon glyphicon-circle-arrow-up"></i>Add File</button>
                                </div>
                            </div>
                            <div class="col-sm-12 cusmargin" >
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"><strong class="st_file">Note</strong> </div> 
                                <div class="col-sm-9 col-md-9 col-lg-9">
                                    <textarea id="TypeHere" name="note" >{{$notes->Note ?? ''}}</textarea>
                                </div>                                
                            </div>
                            {{-- <div class="col-sm-12 cusmargin" >
                                <div class="col-sm-2"> </div>
                                <div class="col-sm-1"><strong class="st_file">Date</strong> </div>
                                <div class="col-sm-9">
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepicker1'>
                                            <input type='text' class="form-control data_input" name="loaddate" value="{{$notes->Date ?? ''}}"/>
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-calendar"></span>
                                            </span>
                                        </div>
                                    </div>
                                </div>   
                            </div> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>      
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'note' );
    </script>

    <script type = "text/javascript">
    $(document).ready(function() {
    $(function() {
        $('#datetimepicker1').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss'
            
        });
    });
    $('.edit_cancel').click(function(){
        window.history.back();
    });
});
const realFileBtn = document.getElementById("real-file");
const customBtn = document.getElementById("custom-button");
const customTxt = document.getElementById("custom-text");

customBtn.addEventListener("click", function() {
    realFileBtn.click();
    
});

realFileBtn.addEventListener("change", function() {
    if (realFileBtn.value) {
        customTxt.innerHTML = realFileBtn.value.match(
            /[\/\\]([\w\d\s\.\-\(\)]+)$/
        )[1];
    } else {
        customTxt.innerHTML = "No file";
    }
});
</script>
</body>

</html>