<html>
<head>
<style>

    * {
        box-sizing: border-box;
        font-size: 14px;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }

    h1 {
        font-size: 30px;
        padding: 0px 7px;
    }

    h2 {
        font-size: 16px;
        font-weight: normal;
        padding: 0px 7px;
    }
    div.sticker {
        padding-top: 10px;
        width: 100%;
        height: 150mm;
        border: black;
        position: relative;   
    }

    div.sticker:nth-child(even) {
        margin-top: 14mm;
        page-break-after: always;
    }

    div.sticker:last-child{
        page-break-after: avoid;
        margin-bottom: 0px;
    }

    .outline{
        width: 100%;
        text-align: left;
        padding-left: 0px;
        padding-right: 8px;
    }

    table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid #CCC;
    }

    table td{
        padding: 4px;
        margin: 0px;
    }

    div.grid-table td{
        border: 1px solid #CCC;
    }

    div.grid-row-table tr{
        border: 1px solid #CCC;
    }

    div.printed-date{
        position: absolute;
        left: 10px;
        bottom: 20px;
    }

    div.spacer
    {
        font-size: 0; 
        height: 15px;
        line-height: 0;
    }
</style>
</head>
<body>
<?php 

    $stickerCounter = 0;
?>
@include('templates.function')
@foreach($job->products as $product)

<?php

    //bottom sticker needs more padding than the top of page first label
    $stickerCounter++; 
    if($stickerCounter == 2){
        $spacer = "<div class='spacer'></div>"; 
        $stickerCounter = 0;
    }else{
        $spacer = "";
        //for each get sticker count
    }





    //Provide proper tcsa footer for new regulations
    //If it's a white label brand we need to do special text
        $brandID =  $job->specifications->brand->id;
        $brandName =  $job->specifications->brand->name;
        $brandWhiteLabel = $job->specifications->brand->whitelabel;
        $brandCabCard = $job->specifications->brand->cabcard;

    //replace the word [DATE] in the footer with the actual date
        $todaysDate = date('m/Y');
        $todaysDateTime = date('m/d/Y g:i a');
        $brandFooter =  str_replace("[DATE]", " $todaysDate ",  $brandCabCard);

    //if it's white lable replace company name with ours
        if($brandWhiteLabel > 0){
            $brandFooter =  str_replace("Cory Mfg. Inc.", $brandName,  $brandFooter);
        }

?>

<div class="sticker">
        <?php echo $spacer; ?>
        <div class="outline">
            <table width="100%">
                <tr>
                    <td width="33%">
                        <h1>#{{ $job->JobID }}&nbsp;&nbsp;&nbsp;Item {{ $product->Line }}</h1>
                        <h2>Job Name: {{ $job->Reference }}</h2>
                    </td>
                    <td width="33%">
                        <?php $imgWidth = ($job->brand->image_width * 1.75); ?>
                        <img style="max-height: 600px; max-width: <?php echo $imgWidth ; ?>px " src="{{ $http_base_path . $job->brand->logo_image_small_path }}">
                    </td>
                    <td width="33%">
                        <strong>{{ $job->account->Name }}</strong><br>
                        @if($job->account->AddressLine1)
                            {{ $job->account->AddressLine1 }}<br>    
                        @endif
                        @if($job->account->AddressLine2)
                            {{ $job->account->AddressLine2 }}<br>    
                        @endif
                        {{ $job->account->City }}, {{ $job->account->State }} {{ $job->account->Zip }}
                        @if($job->account->phone)
                            {{ $job->account->phone }}<br> 
                        @endif
                        @if($job->account->email)
                            {{ $job->account->email }}<br> 
                        @endif
                        {{ $job->specifications->account_info->email }}
                    </td>
                </tr>
            </table>
        </div>
        
        <div class="outline grid-row-table">
            <table width="80%">
                <tr>
                    <td width="33%"><b>Door Style:</b> {{ $job->door_style_name }}</td>
                    <td width="33%"><b>Door Color:</b> {{ $job->door_color_name }}</td>
                    <td width="33%"><b>Door Finish:</b> {{ $job->door_finish_name }}</td>
                </tr>
                <tr>
                    <td width="33%"><b>Drawer Box:</b> {{ $job->cabinet_drawer_box_name }}</td>
                    <td width="33%"><b>Handle Pull:</b> {{ $job->specifications->door_pull }}</td>
                    <td width="33%">&nbsp;</td>
                </tr>
                <tr>
                    <td width="33%"><b>Cabinet Interior:</b> {{ $job->cabinet_box_material_name }}</td>
                    <td width="33%"><b>Cabinet Exterior:</b> {{ $job->door_color_name }}</td>
                    <td width="33%">&nbsp;</td>
                </tr>
            </table>
        </div>

        <div class="outline grid-table">
            <table width="80%">

                @include('partials.cabinet-cards.mods-table-header')

                <tbody>
                    <tr valign="top" style="background-color: #DDD">
                        <td>
                            {{ $product->Qty }}
                        </td>
                        <td>
                            <?php 
                                $ORDCode = getORDCode($product);
                            ?>
                            {{ $ORDCode  }}

                        </td>
                        <td>
                            
                                {{ round($product->Width, 3) }}"
                                x 
                                {{ round($product->Height, 3)}}"
                                x
                                {{ round($product->Depth, 3) }}"
                            
                            <br>
                        </td>
                        <td>
                            {{ $product->Hinging }}
                        </td>
                        <td>
                            {{ $product->FinishedEnd }}
                        </td>
                        <td>
                            {{ $product->Description }}
                            @if($product->Notes)
                            <br>
                            {{ $product->Notes }}
                            @endif
                        </td>
                    </tr>
                    @if($product->has_finished_interior)
                    <tr valign="top">
                        <td>1</td>
                        <td>Finished Interior</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                    @endif

                    @foreach($product->cab_card_initial_modifications as $modification)
                        @include('partials.cabinet-cards.mods-table-row')
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="outline printed-date" style="text-align:center;">
            {{ $brandFooter }} <br>
             Printed ({{ $todaysDateTime }}) 
        </div>

    </div>

    @foreach($product->cab_card_overflow_modification_sets as $modifications)

        <div class="sticker">

            <div class="outline grid-table">
                <table width="80%">

                    @include('partials.cabinet-cards.mods-table-header')

                    <tbody>
                        @foreach($modifications as $modification)
                            @include('partials.cabinet-cards.mods-table-row')
                        @endforeach
                    </tbody>
                </table>
            </div>

            <div class="outline printed-date">
                 Printed ({{ $todaysDateTime }}) - {{ $brandFooter }} 
            </div>
        </div>

    @endforeach
@endforeach
</body>
</html>

