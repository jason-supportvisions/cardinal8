<?php 
    $page_name  = "Summary";     
?>
@extends('templates.default')

@section('content')

@include('partials.summary.nav', ['job'=>$job])

@include('partials.summary.button_bar')

@include('partials.summary.content')

@stop

@section('scripts')

<script>

    var jsJobID = "{{ $job->JobID }}";
    var jsJobAcct = "{{ $job->AccountID }}";

    $('.Price').change(function(){
        // hide everything
        $('#price-total-row, .price-td').hide().removeClass('hide');
        var price_option = $(this).val();

        //show as needed
        if(price_option != 'No'){
            $('#price-total-row').show();
            $('.'+price_option).show();
        }

    });
</script>
@stop