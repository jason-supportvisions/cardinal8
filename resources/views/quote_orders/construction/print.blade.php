<?php
date_default_timezone_set('america/new_york'); 

//include('dbconfig.php');





//======================== Price Sheet Modal ==========================
if($action =='PrintPriceSheet'){
	
    $response = '<style>
            td.priceStyle {
			    text-align: center;
			    background-color: #c9daf8;
			    color: black;
			}
			td.nullStyle{
				text-align: center;
			    color: #F5F5F5;
			}
            body {
                font-family: Verdana,Geneva,sans-serif; 
            }
            table {
                width: 100%;
            }

            td {
                text-align: center;
            }

            td.title {
                text-align: left;
            }
    </style>
	<table class="table table-striped">
		<tH>
			<td><b>Door</b></td>
			<td><b>Tall</b></td>
			<td><b>Full</b></td>
			<td><b>DRW</b></td>
			<td><b>LGDRW</b></td>
			<td><b>OLay</b></td>
			<td><b>Dsq</b></td>
			<td><b>DWsq</b></td>
			<td><b>WallE</b></td>
			<td><b>BaseE</b></td>
			<td><b>TallE</b></td>
			<td><b>P1sq</b></td>
			<td><b>P2sq</b></td>
			<td><b>FinInt</b></td>
		</tH>';

		//get ids from construction page
		$jobDoor 			= isset($varDoor) ? $varDoor : 0;
		$jobStyleMaterial 	= isset($varMaterial) ? $varMaterial : 0;
		$jobStyleColor 		= isset($varStyle) ? $varStyle : 0;
		$jobStyleFinish	 	= isset($varFinish) ? $varFinish : 0;
		$jobDoorInside	 	= isset($varInside) ? $varInside : 0;
		$jobDoorOutside	 	= isset($varOutside) ? $varOutside : 0;
		$jobDoorCenter	 	= isset($varCenter) ? $varCenter : 0;
		$jobDoorStile	 	= isset($varStile) ? $varStile : 0;
		$jobDoorHardware	= isset($varHardware) ? $varHardware : 1; //3 is default?

	
		//for totalling
		$totalDoor 		= 0.00;
		$totalTDoor 	= 0.00;
		$totalFDoor 	= 0.00;
		$totalDrw 		= 0.00;
		$totalLgDrw 	= 0.00;
		$totalOver 		= 0.00;
		$totalDSq 		= 0.00;
		$totalDrwSq 	= 0.00;
		$totalFinInt 	= 0.00;
		$totalP1 		= 0.00;
		$totalP2 		= 0.00;
		$totalWE 		= 0.00;
		$totalTE 		= 0.00;
		$totalBE 		= 0.00;

		
		//pricing sheet details
		$tableList = array('styles', 
						'style_insideprofile',
						'style_centerpanel',
						'style_outsideprofile',
						'style_stilerail',
						'style_hardware',
						'style_material', 
						'style_color', 
						'style_finish');

			//create a row for each table requested above				
			foreach ($tableList as $table) {

				if($table == 'styles'){
					$styleID = $jobDoor; 
					$styleLabel = "Door: ";
				}elseif($table == 'style_material'){
					$styleID = $jobStyleMaterial;
					$styleLabel = "Material: ";
				}elseif($table == 'style_color'){
					$styleID = $jobStyleColor;
					$styleLabel = "Color: ";
				}elseif($table == 'style_finish'){
					$styleID = $jobStyleFinish;
					$styleLabel = "Finish: ";
				}elseif($table == 'style_insideprofile'){
					$styleID = $jobDoorInside;
					$styleLabel = "Inside Profile: ";
				}elseif($table == 'style_outsideprofile'){
					$styleID = $jobDoorOutside;
					$styleLabel = "Outside Profile: ";
				}elseif($table == 'style_centerpanel'){
					$styleID = $jobDoorCenter;
					$styleLabel = "Center Panel: ";
				}elseif($table == 'style_stilerail'){
					$styleID = $jobDoorStile;
					$styleLabel = "Stile Rail: ";
				}elseif($table == 'style_hardware'){
					$styleID = $jobDoorHardware;
					$styleLabel = "Hardware: ";
				}else{
					$styleLabel = "";
				}
			
				$Results = DB::select(DB::raw("SELECT ID, Name, DoorList, GlassList, TallDoorList, 
			 	FullTallDoorList, DrawerList, LargeDrawerList, OverlayList, DoorSqFtList, DrawerSqFtList, WallEndList, BaseEndList, TallEndList, Panel1SSqFtList, Panel2SSqFtList, FinishedInteriorList 
				 FROM $table WHERE ID = $styleID"));
				 
			if($Results === NULL OR empty($Results)){
				// echo "fail";
				// echo "SELECT ID, Name, DoorList, GlassList, TallDoorList, FullTallDoorList, DrawerList, LargeDrawerList, OverlayList, DoorSqFtList, DrawerSqFtList, WallEndList, BaseEndList, TallEndList, Panel1SSqFtList, Panel2SSqFtList, FinishedInteriorList FROM $table WHERE ID = $styleID";
					
					

			}else {
				
					$ID 				= 0.00;
					$Name 				= "N/A";
					$DoorList 			= 0.00;
					$TallDoorList 		= 0.00;
					$FullTallDoorList 	= 0.00;
					$DrawerList 		= 0.00;
					$LargeDrawerList 	= 0.00;
					$OverlayList 		= 0.00;
					$DoorSqFtList 		= 0.00;
					$DrawerSqFtList 	= 0.00;
					$WallEndList 		= 0.00;
					$BaseEndList 		= 0.00;
					$TallEndList 		= 0.00;
					$Panel1SSqFtList 	= 0.00;
					$Panel2SSqFtList 	= 0.00;
					$FinIntList		 	= 0.00;

				foreach($Results as $row) {
					$ID 				= $row->ID;
					$Name 				= $row->Name;
					$DoorList 			= $row->DoorList;
					$TallDoorList 		= $row->TallDoorList;
					$FullTallDoorList 	= $row->FullTallDoorList;
					$DrawerList 		= $row->DrawerList;
					$LargeDrawerList 	= $row->LargeDrawerList;
					$OverlayList 		= $row->OverlayList;
					$DoorSqFtList 		= $row->DoorSqFtList;
					$DrawerSqFtList 	= $row->DrawerSqFtList;					
					$WallEndList 		= $row->WallEndList;
					$BaseEndList 		= $row->BaseEndList;
					$TallEndList 		= $row->TallEndList;
					$Panel1SSqFtList 	= $row->Panel1SSqFtList;
					$Panel2SSqFtList 	= $row->Panel2SSqFtList;
					$FinIntList		 	= $row->FinishedInteriorList;

				}


						//apply styling
						echo "<Script>console.log('Name: $Name');</script>";
						if($Name == "None Selected"){
							$priceStyle = "nullStyle"; //class
							$titleStyle = "nullTitleStyle";
							echo "<Script>console.log('priceStyle: $priceStyle');</script>";
						}else{
							$priceStyle = "priceStyle"; 
							$priceStyle = "priceTitleStyle"; 
							echo "<Script>console.log('priceStyle: $priceStyle');</script>";
						}

						//properly style the non 0 values
						if($DoorList > 0){ $DoorBold = $priceStyle; }else{ $DoorBold = ""; }
						if($TallDoorList > 0){ $TallDoorBold = $priceStyle;	}else{$TallDoorBold = "";}
						if($FullTallDoorList > 0){ $FullTallDoorBold = $priceStyle;	}else{ $FullTallDoorBold = "";}
						if($DrawerList > 0){ $DrawerBold = $priceStyle;	}else{ $DrawerBold = "";}
						if($LargeDrawerList > 0){ $LargeDrawerBold = $priceStyle;}else{	$LargeDrawerBold = "";}
						if($OverlayList > 0){ $OverlayBold = $priceStyle; }else{ $OverlayBold = ""; }
						if($DoorSqFtList > 0){ $DoorSqBold = $priceStyle; }else{ $DoorSqBold = ""; }
						if($DrawerSqFtList > 0){ $DrawerSqBold = $priceStyle; }else{ $DrawerSqBold = ""; }
						if($WallEndList > 0){ $WallEndBold = $priceStyle; }else{ $WallEndBold = "";	}
						if($BaseEndList > 0){ $BaseEndBold = $priceStyle; }else{ $BaseEndBold = ""; }
						if($TallEndList > 0){ $TallEndBold = $priceStyle; }else{ $TallEndBold = ""; }
						if($Panel1SSqFtList > 0){ $Panel1Bold = $priceStyle; }else{ $Panel1Bold = ""; }
						if($Panel2SSqFtList > 0){ $Panel2Bold = $priceStyle; }else{ $Panel2Bold = ""; }

				//Create Totals
					$totalDoor 		= $DoorList + $totalDoor;
					$totalTDoor 	= $TallDoorList + $totalTDoor;
					$totalFDoor 	= $FullTallDoorList + $totalFDoor;
					$totalDrw 		= $DrawerList + $totalDrw;
					$totalLgDrw 	= $LargeDrawerList + $totalLgDrw;
					$totalOver 		= $OverlayList + $totalOver;
					$totalDSq 		= $DoorSqFtList + $totalDSq;
					$totalDrwSq 	= $DrawerSqFtList + $totalDrwSq;
					$totalFinInt 	= $FinIntList + $totalFinInt;
					$totalP1 		= $Panel1SSqFtList + $totalP1;
					$totalP2 		= $Panel2SSqFtList + $totalP2;
					$totalBE		= $BaseEndList + $totalBE;
					$totalWE		= $WallEndList + $totalWE;
					$totalTE		= $TallEndList + $totalTE;

					$response .= " <tr>
						<td class='title'><b>$styleLabel</b> $Name</td>
						<td class='$DoorBold'>$DoorList</td>
						<td class='$TallDoorBold'>$TallDoorList</td>
						<td class='$FullTallDoorBold'>$FullTallDoorList</td>
						<td class='$DrawerBold'>$DrawerList</td>
						<td class='$LargeDrawerBold'>$LargeDrawerList</td>
						<td class='$OverlayBold'>$OverlayList</td>
						<td class='$DoorSqBold'>$DoorSqFtList</td>
						<td class='$DrawerSqBold'>$DrawerSqFtList</td>
						<td class='$WallEndBold'>$WallEndList</td>
						<td class='$BaseEndBold'>$BaseEndList</td>
						<td class='$TallEndBold'>$TallEndList</td>
						<td class='$Panel1Bold'>$Panel1SSqFtList</td>
						<td class='$Panel2Bold'>$Panel2SSqFtList</td>
						<td class='$Panel2Bold'>$FinIntList</td>
					</tr>";

					
				
			}// else
		}//foreach

					$response .= "  <tr style='border:1px;'>
						<td></td>
						<td><b>$totalDoor</b></td>
						<td><b>$totalTDoor</b></td>
						<td><b>$totalFDoor</b></td>
						<td><b>$totalDrw</b></td>
						<td><b>$totalLgDrw</b></td>
						<td><b>$totalOver</b></td>
						<td><b>$totalDSq</b></td>
						<td><b>$totalDrwSq</b></td>
						<td><b>$totalWE</b></td>
						<td><b>$totalBE</b></td>
						<td><b>$totalTE</b></td>
						<td><b>$totalP1</b></td>
						<td><b>$totalP2</b></td>
						<td><b>$totalFinInt</b></td>
					</tr>";

			// }


				$response .= '</table>';
	echo $response;			
}
//========================================================================
