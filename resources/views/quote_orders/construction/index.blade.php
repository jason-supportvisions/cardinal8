@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title 				=  " | Specifications";
$Select2 			= 0;
$Select2beta 		= 1;
$DataTables 		= 0;
$jQuery_UI 			= 0;
$jQuery_Validate 	= 1;
$xCRUD_16 			= 0;
$jobPrint 			= isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing 		= isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

$pageid 			= 'Construction';
$jobID				= $JobID;
$jobAcct			= $JobAcct;

// echo "jobID: $JobID";

//===== Brands ============
$acct_sql = "SELECT * FROM account where ID = $SesAcct";
$acctResults = DB::select(DB::raw($acct_sql));
$prefBrand = 1; //imperia
$showBrands = "1,2"; //imperia and pk
if(count($acctResults) != 0)
	{
		foreach ($acctResults as $row) {
			$prefBrand = $row->PrefBrand;
			$showBrands = $row->ShowBrands;
		}
		
	}
// dd($data);
$brand_sql = "SELECT ID, Name, WhiteLabel, abbreviation, logo_image_small, image_height, image_width FROM style_brand where active=1";
$brandResults = DB::select(DB::raw($brand_sql));
$default_brand_id = $prefBrand;
$allowedBrands = explode(",", $showBrands);


$jobResults = DB::select(DB::raw("SELECT * FROM job_construction WHERE JobID = '$jobID'"));
if(count($jobResults) === 0)	{
	$jobBrand = $default_brand_id;
	$jobAcct = $jobAcct;
	$jobStyleGroup = "1";
	$jobDoor = "";
	$jobDoorOutside = "";
	$jobDoorSR = "";
	$jobDoorPeg = "";
	$jobDoorInside = "";
	$jobDoorCenter = "";
	$jobDoorHardware = "";
	$jobDoorLocation = "";
	$jobDrw = "";
	$jobDrwOutside = "";
	$jobDrwSR = "";
	$jobDrwPeg = "";
	$jobDrwInside = "";
	$jobDrwCenter = "";
	$jobDrwHardware = "";
	$jobDrwLocation = "";
	$jobLgDrw = "";
	$jobLgDrwOutside = "";
	$jobLgDrwSR = "";
	$jobLgDrwPeg = "";
	$jobLgDrwInside = "";
	$jobLgDrwCenter = "";
	$jobLgDrwHardware = "";
	$jobLgDrwLocation = "";
	$jobStyleMaterial = "";
	$jobStyleMaterialCustom = "";
	$jobStyleColor = "";
	$jobStyleColorCustom = "";
	$jobStyleFinish = "";
	$jobStyleFinishCustom = "";

	$jobCabinetMaterial = "3";
	$jobCabinetDrwBox = "1";
	$jobCabinetHinge = "1";
	$jobCabinetToeKick = "1";
	$jobCabinetExtMaterial = "";
	$jobCabinetExtColor = "";
	$jobCabinetExtFinish = "";
	$jobCabinetIntEdge = "";
	$jobCabinetExtEdge = "";
	$jobCabinetTK = "";
	$jobCabinetToeKick = "";
	$jobLoadName = "";
	$jobHeaderNote = "";
	$AdminNote = "";
}else{	
	foreach ($jobResults as $row) {
		$jobID = $jobID;
		$jobAcct = $jobAcct;
        $jobBrand = $row->BrandID;
        $jobStyleGroup = $row->StyleGroupID;
		$jobDoor = $row->DoorStyleID;
		$jobDoorOutside = $row->DoorOutsideProfileID;
		$jobDoorSR = $row->DoorStileRailID;
		$jobDoorPeg = $row->DoorPegID;
		$jobDoorInside = $row->DoorInsideProfileID;
		$jobDoorCenter = $row->DoorCenterPanelID;
		$jobDoorHardware = $row->DoorHardwareID;
		$jobDoorLocation = $row->DoorHardwareLocationID;
		$jobDrw = $row->DrawerStyleID;
		$jobDrwOutside = $row->DrawerOutsideProfileID;
		$jobDrwSR = $row->DrawerStileRailID;
		$jobDrwPeg = $row->DrawerPegID;
		$jobDrwInside = $row->DrawerInsideProfileID;
		$jobDrwCenter = $row->DrawerCenterPanelID;
		$jobDrwHardware = $row->DrawerHardwareID;
		$jobDrwLocation = $row->DrawerHardwareLocationID;
		$jobLgDrw = $row->LargeDrawerStyleID;
		$jobLgDrwOutside = $row->LargeDrawerOutsideProfileID;
		$jobLgDrwSR = $row->LargeDrawerStileRailID;
		$jobLgDrwPeg = $row->LargeDrawerPegID;
		$jobLgDrwInside = $row->LargeDrawerInsideProfileID;
		$jobLgDrwCenter = $row->LargeDrawerCenterPanelID;
		$jobLgDrwHardware = $row->LargeDrawerHardwareID;
		$jobLgDrwLocation = $row->LargeDrawerHardwareLocationID;
		$jobStyleMaterial = $row->StyleMaterialID;
		$jobStyleColor = $row->StyleColorID;
		$jobStyleMaterialCustom = $row->StyleMaterialCustom;
		$jobStyleColorCustom = $row->StyleColorCustom;
		$jobStyleFinishCustom = $row->StyleFinishCustom;
		$jobStyleFinish = $row->StyleFinishID;
		$jobCabinetMaterial = $row->CabinetBoxMaterialID;
		$jobCabinetDrwBox = $row->CabinetDrawerBoxID;
		$jobCabinetHinge = $row->CabinetHingeID;
		$jobCabinetToeKick = $row->CabinetToeKickID;
		$jobCabinetExtMaterial = $row->CabinetExteriorMaterialID;
		$jobCabinetExtColor = $row->CabinetExteriorColorID;
		$jobCabinetExtFinish = $row->CabinetExteriorFinishID;
		$jobCabinetIntEdge = $row->CabinetInteriorEdgeID;
		$jobCabinetExtEdge = $row->CabinetExteriorEdgeID;
		$jobCabinetTK = $row->CabinetToeHeightID;	
		$jobCabinetToeKick = $row->CabinetToeKickID;	
		$jobLoadName = $row->LoadName;	
		$jobHeaderNote = $row->HeaderNote;
		$AdminNote = $row->AdminNote;
    }
}
// echo "row->brandID: $jobBrand";
?>
@include('layouts.header')

<body>          
	
		<!-- START:navbar -->
		<div class="container">
			@include('layouts.navbar')
		</div>
		<!-- END:navbar -->

	<form action="{{ route('spec.post') }}" name="construction-form" id="construction-form" method="post">
		@csrf
		<input type="hidden" value="<?php echo $jobID; ?>" id="myJobId" name="JobID">
		<input type="hidden" value="<?php echo $jobAcct; ?>" name="JobAcct">

		<!-- START:primary content container -->
		<div class="container">
			@include('templates.progress')


			<script>
				const btn = document.getElementById("nextsave")
	
				btn.disabled = true;
				setTimeout(()=>{
				btn.disabled = false;
				console.log('Button Activated')}, 2000);
	
				</script>



			<div name="header_debug" id="header_debug" style="color:red;"></div>
			<div class="row">
				<div class="col-sm-8 col-md-8">					
					<div class="panel panel-UX">
						<div class="panel-heading">Doors & Drawers</div>
						<div class="panel-body">
							<div class="form-horizontal">
								<div class="form-group">
									<label class="col-sm-3 control-label"><span class="text-red"></span></label>
									<div class="col-sm-9 ">
										<?php 

										foreach ($brandResults as $brand){
											
											
											$brandWhiteLabel = $brand->WhiteLabel;
											$brandID = $brand->ID;
											
											//show all brands to admins
											if($SesType == -1){
												if ($jobBrand == $brandID) { $checked =  "checked";} else{ $checked =  ""; }
											
												?>

												<input id="<?= $brand->abbreviation ?>" type="radio" name="brand" class="brand" value="<?= $brandID ?>" <?= $checked ?>	>
												<label for="<?= $brand->abbreviation ?>">
													<img src="/images/logos/<?= $brand->logo_image_small ?>" height="<?= $brand->image_height ?>" width="<?= $brand->image_width ?>">&nbsp;&nbsp;&nbsp;&nbsp;
												</label>

												<?php
											
											}else{

												//show only allowed brands
												if(in_array($brandID,$allowedBrands)){
													if ($jobBrand == $brandID) { $checked =  "checked";} else{ $checked =  ""; }
											
												?>
												<input id="<?= $brand->abbreviation ?>" type="radio" name="brand" class="brand" value="<?= $brandID ?>" <?= $checked ?>	>
												<label for="<?= $brand->abbreviation ?>">
													<img src="/images/logos/<?= $brand->logo_image_small ?>" height="<?= $brand->image_height ?>" width="<?= $brand->image_width ?>">&nbsp;&nbsp;&nbsp;&nbsp;
												</label>
										<?php 	}	
											}
										}
										?>
									</div>
								</div>
								<div class="form-group ">
									<label class="col-sm-3 control-label" for="cabMatl">Style Group<span class="text-red"> * </span></label>
									<div class="col-sm-6">
										<select class="form-control styGroup" id="styGroup" name="styGroup">
										<option value="0"></option>
										</select>
									</div>	
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="doorSty">Door<span class="text-red"> * </span></label>
									<div class="col-sm-6">
										<select class="form-control doorSty" id="doorSty" name="doorSty">
										<option value="0"></option>
										
										</select> 
										<div name="doorButtons" id="doorButtons">
												<br>
											<input type="button" name="doorOptions" id="doorOptions" class="btn btn-sm btn-outline-primary" data-toggle="modal" href="#DoorModal" role="button" value ="Profile Options">
											
											<input type="button" name="doorPDF" id="doorPDF" class="btn btn-sm btn-outline-primary" data-toggle="modal" value="Spec PDF">

											<input type="button" name="doorAll" id="doorAll" class="btn btn-sm btn-outline-primary" data-toggle="modal" value="View Door Styles">
										</div>
									</div>
									<div class="col-sm-3">
										<br>				
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="drwSty">Small Drawer<span class="text-red"> * </span></label>
									<div class="col-sm-6">

										<select class="form-control drwSty" id="drwSty" name="drwSty">
											<option value="0"></option>
										</select>
										
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="lgdrwSty">Large Drawer<span class="text-red"> * </span></label>
									<div class="col-sm-6">
										<select class="form-control lgdrwSty" id="lgdrwSty" name="lgdrwSty">
										<option value="0"></option>
										</select>
									</div>
								</div>		
							</div>
							<div class="form-horizontal">			
								<div class="form-group">
									<label class="col-sm-3 control-label" for="styMatl">Material<span class="text-red"> * </span></label>
									<div class="col-sm-6">
										<select class="form-control styMatl" id="styMatl" name="styMatl">
											<option value="0"></option>
										</select>					
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="styColor">Color/Pattern<span class="text-red"> * </span></label>
									<div id="stdColor" class="col-sm-6">					
										<select class="form-control styColor" id="styColor" name="styColor">
											<option value="0"></option>
										</select>				
									</div>			
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="styFinish">Finish<span class="text-red"> * </span></label>
									<div id="stdFinish" class="col-sm-6">
										<select class="form-control styFinish" id="styFinish" name="styFinish">
											<option value="0"></option>
										</select>
									</div>				
								</div>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="jobHeaderNotes">Header Note</label>
									<div class="col-sm-6">
										<textarea class="form-control" rows="4" id="jobHeaderNotes" name="jobHeaderNotes"><?php 
											$Results = DB::select(DB::raw("SELECT HeaderNote FROM job_construction WHERE JobID = $jobID "));
											$jobHeaderNote = "";
											
											foreach ($Results as $row){
												$jobHeaderNote = $row->HeaderNote;		
											}

											echo $jobHeaderNote;
											?>
										</textarea>
										<a class="btn btn-default" data-toggle="modal" href="#PriceModal" role="button" id="PriceSheetBtn"> <span class="glyphicon glyphicon-tag"></span> Price Sheet </a>
									</div>			
								</div>						
							</div>					
						</div>
					</div> 			
				</div>
				

				<div class="col-md-4 ">
					<div class="row">					
						<div class="col-md-12">
							<div class="panel panel-UX">
								<div class="panel-heading">Door Details / Specs</div> 
								<div class="panel-body" >
									<table class="spec-table" style="padding: 20px;" border="0" width="100%">
										<tr>
											<td width="125" align="center">
												<a data-toggle="modal" href="#DrawerImage" id="drwStyImgModalLink"><img id="drwStyImg" src="/images/no_image.png" class="text-center"  title="Small Drawer Front"   ></a>
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;"  id="construction_drawer_title"></div>
											</td>
											<td width="125" align="center">
												<a href="#DoorModal" name="doorOptions" id="doorOptions" data-toggle="modal">
												<div style="background-color: white; color:#888888; text-align:center; width:100px; font-size: 10pt;" id="construction_inside_label"></div>
												<img id="construction_inside" src="/images/no_image.png" class="text-center" title="" style="width: 75px;"><BR /><BR />
												<div style="background-color: white; color:#888888; text-align:center; width:100px; font-size: 8pt;" id="construction_inside_title"></div>
												</a>
											</td>
										</tr>
										<tr>
											<td align="center">
												<a data-toggle="modal" href="#DoorImage"><img id="doorStyImg" src="/images/no_image.png" class="text-center" title="Door Front" ></a>
												<div style="background-color: white; color:#888888; text-align:center; font-size: 8pt;" id="construction_door_title"></div>

											</td>
											<td  align="center">
												<a href="#DoorModal" name="doorOptions" id="doorOptions" data-toggle="modal">
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 10pt;" id="construction_center_label"></div>
												<img id="construction_center" src="/images/no_image.png" class="text-center" title="" style="width: 75px;"><BR /><BR />
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;" id="construction_center_title"></div>
												</a>
											</td>
										</tr>
										<tr>
											<td   align="center">					
												<a data-toggle="modal" href="#LgDrawerImage" id="lgdrwStyImgModalLink"><img id="lgdrwStyImg" src="/images/no_image.png" class="text-center" title="Lg Drawer Front"  ></a>
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;" id="construction_lgdrawer_title"></div>
											</td>
											<td   align="center">
												<a href="#DoorModal" name="doorOptions" id="doorOptions" data-toggle="modal">
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 10pt;" id="construction_outside_label"></div>
												<img id="construction_outside" src="/images/no_image.png" class="text-center" title="coutside profile" style="width: 75px;"><br/><br/>
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;" id="construction_outside_title"></div>
												</a>
											</td>
										</tr>
										<tr>
											<td  align="center">
												<a href="#DoorModal" name="doorOptions" id="doorOptions" data-toggle="modal">
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 10pt;" id="construction_hardware_label"></div>
												<img id="construction_hardware" src="/images/no_image.png" style="width: 75px; height: 75px;" class="text-center noimage"  title="Hardware"> 
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;" id="construction_hardware_title"></div>
												</a>
											</td>
											<td  align="center">
												<a href="#DoorModal" name="doorOptions" id="doorOptions" data-toggle="modal">
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 10pt;"  id="construction_siterail_label"></div>
												<br />
												<div style="background-color: white; color:#888888; text-align:center;   font-size: 8pt;" id="construction_siterail_title"></div>
												</a>
											</td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-8">
					<div class="panel panel-UX">
						<div class="panel-heading">Cabinet Materials</div>
						<div class="panel-body">
							<div class="form-horizontal">						
								<div class="form-group">
									<label class="col-sm-3 control-label" for="cabMatl">Cabinet Box Material<span class="text-red"> * </span></label>
									<div class="col-sm-5">
										<select class="form-control cabMatl" id="cabMatl" name="cabMatl">
											<option value="0"></option>
										</select>
									</div>			
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="drwBox">Drawer&nbsp;Box<span class="text-red"> * </span></label>
									<div class="col-sm-5">
										<select class="form-control drwBox" id="drwBox" name="drwBox">
											<option value="0"></option>
										</select>
									</div>
									
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="hinge">Hinges<span class="text-red"> * </span></label>
									<div class="col-sm-5">
										<select class="form-control hinge" id="hinge" name="hinge">
											<option value="0"></option>
										</select>
									</div>
								</div>
								
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="cabMatl">Fin End Material<span class="text-red">  </span></label>
									<div class="col-sm-5">
										<select class="form-control" style="width:100%;" id="cabFinEndMat" name="cabFinEndMat">				
											<option value="0"></option>
										</select>
									</div>
								</div>
								
								<div class="form-group">
									<label class="col-sm-3 control-label" for="cabMatl">Fin End Color<span class="text-red"> </span></label>
									<div class="col-sm-5">
										<select class="form-control" style="width:100%;" id="cabFinEndColor" name="cabFinEndColor">						
											<option value="0"></option>
										</select>
									</div>
										
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="cabMatl">Fin End Finish<span class="text-red"> *</span></label>
									<div class="col-sm-5">
										<select class="form-control" style="width:100%;" id="cabFinEndFin" name="cabFinEndFin">									
											<option value="0"></option>
										</select>
									</div>
								</div>

								<div class="form-group">
									<label class="col-sm-3 control-label" for="cabMatl">Edge Banding<span class="text-red"> * </span></label>
									<div class="col-sm-5">
										<select class="form-control" style="width:100%;" id="cabFinEndEdge" name="cabFinEndEdge">
											<option value="0"></option>
										</select>
									</div>
								</div>
								<?php 
								if($SesType == -1){
								?>
								<div class="form-group">
									<label class="col-sm-3 control-label" for="adminNote">Admin Note</label>
									<div class="col-sm-5">
										<textarea class="form-control" rows="4" id="adminNote" name="adminNote"><?php 
											$Results = DB::select(DB::raw("SELECT AdminNote FROM job_construction WHERE JobID = $jobID "));
											$AdminNote = "";
											foreach ($Results as $row) {
												$AdminNote = $row->AdminNote;		
											}
											echo $AdminNote;
										?></textarea>
									</div>
								</div>
								<?php 
								}
								?>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 ">
					<div class="row">			
						<div class="col-md-12">
							<div class="panel panel-UX">
								<div class="panel-heading">Did you know?</div>
								<div class="panel-body" style="min-height: 150px; max-height: 150;">
									<h4>
										<?php 
										$tipResults = DB::select(DB::raw("SELECT Tip FROM tips AS r1 JOIN (SELECT (RAND() *	(SELECT MAX(ID)	FROM tips)) AS id) AS r2 WHERE r1.id >= r2.id ORDER BY r1.id ASC LIMIT 1"));
										foreach ($tipResults as $row) {		
											echo $row->Tip;	
									} ?>
									</h4>
								</div>
							</div>
						</div>				
					</div>
					<?php 
					$feature = rand(1,3);

					if($feature == 1){
						$featureType = "Visit Door Gallery";
						$title = "Explore our Gallery";
						$image = "/images/door-gallery.png";
						$link = "/gallery/index.php";
					}elseif($feature == 2){
						$featureType = "View our Designs";
						$title = "Explore our Designs";
						$image = "/images/door-drawings.png";
						$link = "/style_overview/index.php";
					}else{
						$featureType = "View our Catalog";
						$title = "Explore our Catalog";
						$image = "/images/catalog-gallery.png";
						$link = "/backend/catalog";
					}
					?>			
					<div class="panel panel-UX">
						<div class="panel-heading"><?=$title?></div>
						<div class="panel-body" >						
							<p class="lead" style="min-height:225px; max-height: 225px;">
								<a href="<?=$link?>" target="_BLANK"><img src="<?=$image?>" width="300"></a><br><br>
							</p>					
						</div>
						
					</div>	
				</div>
			</div>
		</div>
		@include('quote_orders.construction.modals')
	</form>
	<div class="container">
		@include('layouts.footer')
	</div>
	<script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
	@include('layouts.loadjs')

	<script type="text/javascript" src="{{ asset('js/construction.js') }}" ></script>

	<script type="text/javascript">
		$(document).ready(function () {	
			$("label").click(function(e){
				console.log('label.click called');
				e.preventDefault(); 
				$("#"+$(this).attr("for")).click().change(); 
			});

			$(".cabDrwbox").change(function () {
				console.log('.cabDrwbox called');
				var id = $(this).val();
				var dataString = 'id=' + id;
				var dataStringjpg = id +
					'.jpg';

				$("#cabDrwboxImg").attr("src", dataStringjpg);
			});
			$(".cabHinge").change(function () {
				console.log('.cabHinge called');
				var id = $(this).val();
				var dataString = 'id=' + id;
				var dataStringjpg = id +
					'.jpg';

				$("#cabHingeImg").attr("src", dataStringjpg);
			});
			
			<?php if(is_null($jobDoor) or empty($jobDoor)){ ?>
							$("#styGroup").focus().trigger('click');
			<?php } ?>		
		});

		$('#construction-form').validate({
			rules: {
				brand: { required: true },
				styGroup: { required: true },
				doorSty: { required: true },
				lgdrwSty: { required: true },
				drwSty: { required: true },
				styMatl: { required: true },
				styColor: { required: true },
				styFinish: { required: true },
				cabMatl: { required: true },
				drwBox: { required: true },
				hinge: { required: true }
			},
			highlight: function (element) {
				$(element).closest('.form-group').addClass('has-error');
			},
			unhighlight: function (element) {
				$(element).closest('.form-group').removeClass('has-error');
			},
			errorElement: 'span',
			errorClass: 'control-label',
			errorPlacement: function (error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else {
					error.insertAfter(element);
				}
			}
			
		});
	</script>			 
</body>
</html>
	

