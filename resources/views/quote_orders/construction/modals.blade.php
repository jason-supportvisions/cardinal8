<?php use Illuminate\Support\Facades\DB;?>
<div class="modal fade" id="DoorModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Door Options </h4>
            </div>
            <div class="modal-body">
                <div class="pull-right">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <img id="doorStyImgInModal" src="/images/no_image.png" class="" width="160px">
                        </div>
                    </div>
                </div>
                <div class="form-inline">
                    <div class="doorSpec">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close/Save</button>
            </div>
        </div>
    </div>
</div>
<!-- LargeDrawer Modal -->
<div class="modal fade" id="LargeDrawerModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button class="btn btn-default pull-right" data-dismiss="modal" type="button">Close/Save</button> -->
                <h4 class="modal-title">Large Drawer Specification</h4>
            </div>
            <div class="modal-body">
                <div class="pull-right">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <img id="lgdrwStyImgInModal" src="/images/no_image.png" class="" width="157px">
                        </div>
                    </div>
                </div>

                <div class="form-inline">
                    <div class="lgdrwSpec">
                        <?php if (is_null($jobLgDrw) or empty($jobLgDrw)) {
                        } else {
                        radioarray('styles', 'InsideProfileID', "WHERE ID = $jobLgDrw AND Active = 1",
                        'style_insideprofile', $jobLgDrwInside, '', 'LgDrwInside', '<strong>Inside Profiles</strong>',
                        'inside', 'LgDrw_');
                        radioarray('styles', 'CenterPanelID', "WHERE ID = $jobLgDrw AND Active = 1",
                        'style_centerpanel', $jobLgDrwCenter, '', 'LgDrwCenterPanel', '<strong>Center Panels</strong>',
                        'centerpanel', 'LgDrw_');
                        radioarray('styles', 'OutsideProfileID', "WHERE ID = $jobLgDrw AND Active = 1",
                        'style_outsideprofile', $jobLgDrwOutside, '', 'LgDrwOutside', '<strong>Outside
                            Profiles</strong>', 'outside', 'LgDrw_');
                        radioarray('styles', 'StileRailID', "WHERE ID = $jobLgDrw AND Active = 1", 'style_stilerail',
                        $jobLgDrwSR, '', 'LgDrwStileRail', '<strong>Stiles & Rails</strong>', 'stilerail', 'LgDrw_');
                        } ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close/Save</button>
            </div>
        </div>
    </div>
</div>

<!-- Drawer Modal -->
<div class="modal fade" id="DrawerModal" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <!-- <button class="btn btn-default pull-right" data-dismiss="modal" type="button">Close/Save</button> -->
                <h4 class="modal-title">Small Drawer Specification</h4>
            </div>
            <div class="modal-body">
                <div class="pull-right">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <img id="drwStyImgInModal" src="/images/no_image.png" class="" width="157px">
                        </div>
                    </div>
                </div>

                <div class="form-inline">
                    <div class="drwSpec">
                        <?php if (is_null($jobDrw) or empty($jobDrw)) {
                        } else {
                        radioarray('styles', 'InsideProfileID', "WHERE ID = $jobDrw", 'style_insideprofile',
                        $jobDrwInside, '', 'DrwInside', '<strong>Inside Profiles</strong>', 'inside', 'Drw_');
                        radioarray('styles', 'CenterPanelID', "WHERE ID = $jobDrw", 'style_centerpanel', $jobDrwCenter,
                        '', 'DrwCenterPanel', '<strong>Center Panels</strong>', 'centerpanel', 'Drw_');
                        radioarray('styles', 'OutsideProfileID', "WHERE ID = $jobDrw", 'style_outsideprofile',
                        $jobDrwOutside, '', 'DrwOutside', '<strong>Outside Profiles</strong>', 'outside', 'Drw_');
                        radioarray('styles', 'StileRailID', "WHERE ID = $jobDrw", 'style_stilerail', $jobDrwSR, '',
                        'DrwStileRail', '<strong>Stiles & Rails</strong>', 'stilerail', 'Drw_');
                        } ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close/Save</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Door Image Modal -->
<div id="DoorImage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Door Image @ 12" x 15"</h4>
            </div>
            <div class="modal-body">
                <img id="doorStyImgModel" src="/images/no_image.png" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <div class="pull-left">
                    Download:
                    <a href="#" id="doorStyDXF" class="btn btn-default" download> DXF </a>
                    <a href="#" id="doorStyPDF" class="btn btn-default" download> PDF </a>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Large Drawer Image Modal -->
<div id="LgDrawerImage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Large Drawer Image @ 12" x 10.50"</h4>
            </div>
            <div class="modal-body">
                <img id="lgdrwStyImgModel" src="/images/no_image.png" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <div class="pull-left">
                    Download:
                    <a href="#" id="lgdrwStyDXF" class="btn btn-default" download> DXF </a>
                    <a href="#" id="lgdrwStyPDF" class="btn btn-default" download> PDF </a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Drawer Image Modal -->
<div id="DrawerImage" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Drawer Image @ 12" x 6"</h4>
            </div>
            <div class="modal-body">
                <img id="drwStyImgModel" src="/images/no_image.png" class="img-responsive">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <div class="pull-left">
                    Download:
                    <a href="#" id="drwStyDXF" class="btn btn-default" download> DXF </a>
                    <a href="#" id="drwStyPDF" class="btn btn-default" download> PDF </a>
                </div>
            </div>
        </div>
    </div>
</div>



<div id="style_overview" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Style Overview</h4>
            </div>
            <div class="modal-body">
                <?php
                //include "construction_style_overview.php";
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <div class="pull-left">
                    Download:
                    <a href="#" id="drwStyDXF" class="btn btn-default" download> DXF </a>
                    <a href="#" id="drwStyPDF" class="btn btn-default" download> PDF </a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>


<!-- Style List Modal -->
<div id="StyleList" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Style Overview</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="StyleListDoor" class="col-sm-4">

                        </div>

                        <div id="StyleListLgDrw" class="col-sm-4">

                        </div>

                        <div id="StyleListDrw" class="col-sm-4">

                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>
<!-- Custom Style -->
<div id="CustomStyle" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Custom Style</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <?php
                        $cusstysql = DB::select(DB::raw("SELECT * FROM acct_custom_styles WHERE AccountID = '$jobAcct' ORDER BY Name ASC"));
					
                        if (count($cusstysql) === 0) {
                        echo 'You have no saved styles.';
                        } else {
                        echo "<table class=\"table table-hover\">";
                            echo '<thead>';
                                echo '<tr>';
                                    echo "<th class=\"col-sm-3\">Saved Name</th>";
                                    echo "<th class=\"col-sm-3\">Door Style</th>";
                                    echo "<th class=\"col-sm-4\">Lg Drawer / Drawer Styles</th>";
                                    echo "<th class=\"col-sm-2 text-center\">Product Line</th>";
                                    echo '</tr>';
                                echo '</thead>';
                            echo '<tbody>';
                                foreach ( $cusstysql as $row) {
									$cusName = $row->Name;
									$cusBrandID = $row->BrandID;
									$cusDoorStyID = $row->DoorStyleID;
									$cusLgDrwStyID = $row->LargeDrawerStyleID;
									$cusDrwStyID = $row->DrawerStyleID;

									$cusdoorstyget = DB::select(DB::raw("SELECT DISTINCT * FROM styles WHERE ID = '$cusDoorStyID'"));
									if(count($cusdoorstyget) == 0){
										$cusDoorStyName = '';
									}else{
										foreach ($cusdoorstyget as $row) {
											$cusDoorStyName = $row->Name;
										}
									}
									
									$cuslgdrwstyget = DB::select(DB::raw("SELECT DISTINCT * FROM styles WHERE ID = '$cusLgDrwStyID'"));
									foreach ( $cuslgdrwstyget as $row) {
										$cusLgDrwStyName = $row->Name;
									}

									$cusdrwstyget = DB::select(DB::raw("SELECT DISTINCT * FROM styles WHERE ID = '$cusDrwStyID'"));
									foreach ( $cusdrwstyget as $row) {
										$cusDrwStyName = $row->Name;
									}

									echo '<tr>';
										echo "<td><a
												href=\"query.php?JobID=$jobID&JobAcct=$jobAcct&Load=1&Save=0\">";
												echo $cusName;

												echo '</a></td>';

										echo '<td>';
											echo $cusDoorStyName;
											echo '</td>';
										echo '<td>';
											echo $cusLgDrwStyName;
											echo ' / ';
											echo $cusDrwStyName;
											echo '</td>';
										echo '</td>';
										echo "<td class=\"text-center\">";

											if ($cusBrandID == '1') {
											echo "<img src=\"/images/logos/imperia.png\" style=\"height: 20px;\">";
											} elseif ($cusBrandID == '2') {
											echo "<img src=\"/images/logos/purekitchen.png\" style=\"height: 20px;\">";
											}

											echo '</td>';
										echo '</tr>';
                                }

                                echo '</tbody>';
                            echo '</table>';
                        }
                        ?>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>


<!-- Save Style -->
<div id="SaveStyle" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Save Style</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-horizontal">
                            <div class="form-group">

                                <div class="col-sm-9">
                                    <input class="form-control" id="SaveCustomName" name="SaveCustomName"
                                        placeholder="Enter Custom Style Name" type="text" value="">
                                </div>
                                <div class="col-sm-3">
                                    <button class="btn btn-primary" type="submit" name="action" value="Appliances"
                                        formaction="construction_query.php?JobID=<?php echo $jobID; ?>&JobAcct=<?php echo $jobAcct; ?>&Load=0&Save=1">Save
                                        / Next</button>
                                </div>
                            </div>




                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

            </div>
        </div>
    </div>
</div>















<!-- Price Modal -->
<div class="modal fade" id="PriceModal" role="dialog">
    <div class="modal-dialog modal-lg" style="width:1250px;">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Pricing for current Specification - ADMIN ONLY currently</h4>
            </div>
            <div class="modal-body">
                <style>
                    td.priceStyle {
                        text-align: center;
                        font-weight: bold;
                        font-size: 110%;
                        background-color: #008800;
                        color: #FFFFFF;
                    }

                </style>


                <div class="form-inline">
                    <div class="" id="priceModalTable">

                        <!-- sourced from construction.js -> ajax_construction.php -->


                    </div>
                </div>


                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!-- PRICE MODAL END -->
