@extends('templates.default')

@section('content')

    {{ Form::open(['url' => route('update-product', ['jobID'=>$job_product->JobID, 'job_product_id'=>$job_product->ID])]) }}
    
        @include('partials.products.general')

        @include('partials.products.dimensions')

        @include('partials.products.standard-options')
        
        {{-- @include('partials.products.admin-options') --}}

        @include('partials.products.modifications')

        @include('partials.products.add_edit_old')

    {{ Form::close() }}

@stop