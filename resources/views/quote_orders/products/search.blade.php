@include('templates.function')
<?php
// Page Settings (1 = True | 0 = False)
$Title              =  " | Add Product";
$Select2            = 0;
$Select2beta        = 1;
$DataTables         = 0;
$jQuery_UI          = 0;
$jQuery_Validate    = 1;
$xCRUD_16           = 0;
$jobPrint           = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing         = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

$pageid             = 'Add Products';
$jobID              = isset($jobID) ? $jobID : 0;
$jobAcct            = isset($jobAcct) ? $jobAcct : 0;

$productType        = "Blind Corner";
$on1                = "";
$on2                = "";
$on3                = "";
$on4                = "";
$on5                = "";
$on6                = "";
$on7                = "";
$on8                = "";
$on9                = "";
$on10               = "";
$on11               = "";
$productID          = "";
$searchSQL          = "";
$Search             = isset($search) ? $search : "";
$searchItemCode     = isset($searchItemCode) ? $searchItemCode : "";
$autosearch         = isset($autosearch) ? $autosearch : "";
$searchGroupCode    = isset($_GET['GroupCode']) ? $_GET['GroupCode'] : "";
$searchDescription  = isset($searchDescription) ? $searchDescription : "";
$knowCode           = isset($_POST['knowCode']) ? $_POST['knowCode'] : "";
$groupType          = isset($_GET['groupType']) ? $_GET['groupType'] : "";
$rowCount           = 0;
$concatSearch       = "";
$searchPosition     = "";
$searchStyle        = "";
if($searchDescription == "") $searchDescription = $autosearch; 

use Illuminate\Support\Facades\DB;
?>
<style>
div {
    font-size: 12px;
}
</style>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>

@include('layouts.header')

<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <!-- START:primary content container -->
    <div class="container">

        <div class="row hidden-xs hidden-sm" style="margin-top: 15px;">
            <div class="col-xs-12 col-sm-12">
                <div class="panel panel-UX">
                    <div class="panel-heading" style="border: none;height: 42px;">
                        <div class="pull-right" style="margin-right: -10px; margin-top: -5px; margin-bottom: -5px;">
                            <a  class="btn btn-danger search_cancel"> Cancel<span class="glyphicon glyphicon-remove"></span> </a>
                        </div>

                        <h4 style="margin-bottom: 0px;margin-top: 0px; float:left;">
                            <?php if ($jobID != 0) : ?>
                                <strong>Job: </strong>#<?php echo $jobID ?>
                            <?php endif ?>
                        </h4>

                    </div>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="panel panel-UX">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <!-- Category -->
                                    <div class="form-group">

                                        <div class="col-sm-9">
                                            <!-- search results -->
                                            <?php
                                            if ($searchItemCode != "" or  $searchGroupCode != "" or $searchDescription != "" or $knowCode != "") {
                                                $Category = isset($_GET['Category']) ? $_GET['Category'] : 0;
                                                $GroupCode = "";
                                                //Item Code
                                                if ($searchItemCode != "") {
                                                    $searchItemCode = getORDCodeClean($searchItemCode);
                                                    //search in both items and accessories
                                                    $itemCodeSQL  = '(SELECT GroupCode FROM product_items WHERE Code = "' . $searchItemCode . '") UNION (SELECT GroupCode FROM accessory_items WHERE Code = "' . $searchItemCode . '")';
                                                    // echo "<br>itemCodeSQL = " . $itemCodeSQL;
                                                    $itemCodeSQL  = DB::select(DB::raw($itemCodeSQL));
                                                    if(empty($itemCodeSQL)){
                                                        
                                                    }else{
                                                        foreach ( $itemCodeSQL as $row) {
                                                            $GroupCode = $row->GroupCode;
                                                            //if we find a group code get all the items associated with it
                                                            if ($GroupCode != "") {
                                                                $groupSQL     = '(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM  product_groups WHERE GroupCode = "' . $GroupCode . '") UNION (SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM  accessory_groups WHERE GroupCode = "' . $GroupCode . '")';

                                                                if ($concatSearch != "") {
                                                                    $concatSearch .= ' UNION ' . $groupSQL;
                                                                } else {
                                                                    $concatSearch = $groupSQL;
                                                                }
                                                            }
                                                        }
                                                        $concatSearch      = DB::select(DB::raw($concatSearch));
                                                        $searchSQL    = $concatSearch;
                                                    }
                                                }
                                                //Description
                                                if ($searchDescription != "") {
                                                    //get all searched keywords
                                                    $search_sql_products = "(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM product_groups WHERE";
                                                    $search_sql_accessory = "(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM accessory_groups WHERE";
                                                    $search_sql_order = " ORDER BY GroupCode )";
                                                    $search_union = " UNION ";

                                                    $query = ($searchDescription);
                                                    $keywords = explode(" ", $query);
                                                    $keyCount = 0;
                                                    $search_sql = "";
                                                    $search_tags = "";

                                                    foreach ($keywords as $keys) {
                                                        if ($keyCount > 0) {
                                                            $search_sql .= " AND";
                                                            $search_tags .= " AND";
                                                        }

                                                        $search_sql .= " BookDescription LIKE '%$keys%'";
                                                        $search_tags .= " Tags LIKE '%$keys%'";
                                                        ++$keyCount;
                                                    }

                                                    //build full query string
                                                    $fullQuery = $search_sql_products . $search_sql . " OR " . $search_tags . $search_sql_order
                                                        .  $search_union .
                                                        $search_sql_accessory . $search_sql . " OR " . $search_tags . $search_sql_order;

                                                    //execute query
                                                    $searchDescriptionSQL = DB::select(DB::raw($fullQuery));
                                                    $searchSQL = $searchDescriptionSQL;
                                                }

                                                //Know your code
                                                if ($searchGroupCode != "") {

                                                    //what kind of code is it?
                                                    if ($groupType == "product") {
                                                        $groupTable = "product_groups";
                                                        $itemTable = "product_items";
                                                    } elseif ($groupType == "accessory") {
                                                        $groupTable = "accessory_groups";
                                                        $itemTable = "accessory_items";
                                                    } else {
                                                        $groupTable = "product_groups";
                                                        $itemTable = "product_items";
                                                    }

                                                    $searchSQL = '(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM product_groups WHERE GroupCode = "' . $searchGroupCode . '" ORDER BY GroupCode) UNION (SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM accessory_groups WHERE GroupCode = "' . $searchGroupCode . '" ORDER BY GroupCode)';
                                                    $searchSQL = DB::select(DB::raw($searchSQL));
                                                }
                                                $ProductSQL = $searchSQL;
                                                if ($searchSQL) {
                                                    $rowCount = count($searchSQL);
                                                }
                                            ?>
                                            <div class="form-group">
                                                <div class="col-sm-12">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-UX">
                                                            <?php if( $autosearch == ""){ ?>
                                                            <h3>&nbsp;&nbsp;&nbsp;<?php echo "($rowCount)"; ?> search
                                                                results...
                                                                <span style="background: #DEF3E2;">
                                                                    <?php
                                                                        if ($searchItemCode != "") {
                                                                            echo "'" . $searchItemCode . "'";
                                                                        }
                                                                        if ($searchGroupCode != "") {
                                                                            echo "'" . $searchGroupCode . "'";
                                                                        }
                                                                        if ($searchDescription != "") {
                                                                            echo "'" . $searchDescription . "'";
                                                                        }
                                                                        ?> </span>
                                                            </h3>
                                                            <hr>
                                                            <?php }                                                            
                                                                if ($rowCount == 0) { ?>
                                                                    <div class="row">
                                                                        <div class="col-md-2 col-md-offset-5">
                                                                            <a href="{{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>0]) }}">
                                                                                <button type="submit" class="btn btn-primary btn-sm">Back to Add Product</button>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                            <?php } ?>
                                                            <div class="panel-body">
                                                                <?php
                                                                    if ($ProductSQL != "") {
                                                                        foreach ($ProductSQL as $row) {
                                                                            $productID = $row->ID;
                                                                            $GroupCode = $row->GroupCode;
                                                                            $Description = $row->Description;
                                                                            $BookDescription = $row->BookDescription;
                                                                            $Image_File = $row->Image_File;
                                                                            $ClassBuild = $row->ClassBuild;
                                                                            $Class = $row->Class;
                                                                            $Type = $row->Type;
                                                                            $Notes = $row->Notes;
                                                                            $groupType = $row->Category;

                                                                            //image type product or accessory
                                                                            if ($groupType == "Accessory") {
                                                                                $imagePath = "accessories";
                                                                            } else {
                                                                                $imagePath = "cabinets";
                                                                            }
                                                                    ?>
                                                                <div class="row" style="background-color: #ffffff;">
                                                                    <div class="col-sm-12">
                                                                        <h4><?php echo $BookDescription; ?></h4>
                                                                    </div>
                                                                    <div class="col-sm-4" style="text-align:center;">
                                                                        <img src="/images/<?php echo $imagePath; ?>/<?php echo $Image_File; ?>"
                                                                            width="200" height="200"
                                                                            alt="<?php echo $Image_File; ?>"> <br />
                                                                        <?php echo $GroupCode; ?>
                                                                    </div>
                                                                    <div class="col-sm-8">
                                                                        <br /><br />
                                                                        <ul>
                                                                        <?php
                                                                            $note = $Notes;
                                                                            // Get an array of items from lines
                                                                            $list = explode("\n", $note);
                                                                            // Output the items as list elements
                                                                            foreach ($list as $num => $item) {
                                                                                if ($item != "") {
                                                                                    $item = nl2br($item);
                                                                                    // echo "<li>" . utf8_decode($item) . "</li>";
                                                                                    // $item = utf8_decode($item);
                                                                                    echo "<li>" . htmlentities($item) . "</li>";
                                                                                }
                                                                            }
                                                                        ?>
                                                                        </ul>
                                                                        <br /><br />
                                                                        <div class="col-sm-12">
                                                                            <div class="col-sm-5">
                                                                                <strong>Code</strong>
                                                                            </div>
                                                                            <div class="col-sm-2">
                                                                                <strong>Size</strong>
                                                                            </div>
                                                                            <div class="col-sm-5">
                                                                                <strong> </strong>
                                                                            </div>
                                                                        </div>
                                                                        <?php
                                                                            //items in a group
                                                                            $ItemSQL = "(SELECT Type, ID, Code, Width FROM product_items WHERE GroupCode = '$GroupCode' AND Active = 1  ORDER BY Width) UNION (SELECT Type, ID, Code, Width FROM accessory_items WHERE GroupCode = '$GroupCode' AND Active = 1  ORDER BY Width)";
                                                                            $ItemSQL = DB::select(DB::raw($ItemSQL));

                                                                            foreach ($ItemSQL as $row) {
                                                                                $itemID = $row->ID;
                                                                                $Code = $row->Code;
                                                                                $Width = $row->Width;
                                                                                $productType = $row->Type;

                                                                                if ($searchItemCode != "") {
                                                                                    //if item code has the search string highlight the row
                                                                                    $searchItemCode = strtoupper($searchItemCode);
                                                                                    $searchPosition = strpos($Code, $searchItemCode);
                                                                                    if ($searchPosition === false) {
                                                                                        $searchStyle = '';
                                                                                    } else {
                                                                                        $searchStyle = ' style="background-color: #DEF3E2;"';
                                                                                    }
                                                                                }
                                                                                ?>
                                                                                <!-- Items row -->
                                                                                <br><br>
                                                                                <div class="col-sm-12" <?php echo $searchStyle; ?>>
                                                                                    <div class="col-sm-5">
                                                                                        <?php echo $Code; ?>
                                                                                    </div>
                                                                                    <div class="col-sm-2">
                                                                                        <?php echo round($Width, 2); ?>"
                                                                                    </div>
                                                                                    <div class="col-sm-5 pull-left">
                                                                                        <a
                                                                                            href="{{ route('create-product',['jobID'=>$jobID,'product_item_id'=>$itemID, 'productType'=>$productType]) }}">
                                                                                            <button type="submit" class="btn btn-success btn-xs">Add</button></a>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- end item row -->

                                                                        <?php } //end item while 
                                                                            ?>
                                                                    </div>
                                                                </div>
                                                                <hr>
                                                                <?php
                                                                        } // *while sql 
                                                                    } // if num_rows
                                                                } // end if search terms have data
                                                                ?>
                                                                <!-- end each product -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <!-- spacer below search -->
                                                </div>
                                            </div>
                                            <!-- end search results -->
                                        </div>

                                        <div class="col-sm-3">
                                            <!-- custom search -->
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    <div class="panel-heading">Know what you're looking for?</div>
                                                    <div class="panel-body">
                                                        
                                                        <form action="{{ route('search-product') }}" method="post">
                                                            @csrf
                                                            <div class="form-horizontal">
                                                                <div class="col-sm-1">
                                                                    <!-- spacer -->
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <input type="text" name="searchItemCode" style="width: 200px;" placeholder="  Exact Item Code">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" name="searchDescription" style="width: 200px;" placeholder="  Description Keyword Search">
                                                                        <input type="hidden" id="jobid" class="form-control" name="jobID" value="{{ $jobID }}">
                                                                        <input type="hidden" id="jobacct" class="form-control" name="jobAcct" value="{{ $jobAcct }}">
                                                                    </div>
                                                                    <div class="form-group pull-right">
                                                                        <button type="submit" class="btn btn-primary btn-sm"> Search </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>
                                                </div>
                                            </div>
                                            <!-- end custom search -->

                                            <!-- Category Side Bar -->
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    <div class="panel-heading">Category</div>
                                                    <div class="panel-body">
                                                        <div class="form-horizontal">
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>1]) }}">
                                                                    <img src="/images/addproduct/category-1<?php echo $on1; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br />Wall<br /><br />
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <!-- spacer -->
                                                            </div>
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>3]) }}">
                                                                    <img src="/images/addproduct/category-3<?php echo $on3; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br />Base<br /><br />
                                                            </div>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>4]) }}">
                                                                    <img src="/images/addproduct/category-4<?php echo $on4; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br />Tall<br /><br />
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <!-- spacer -->
                                                            </div>
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>9]) }}">
                                                                    <img src="/images/addproduct/category-8<?php echo $on9; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br>Vanity<br /><br />
                                                            </div>
                                                        </div>
                                                        <div class="form-horizontal">
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>10]) }}">
                                                                    <img src="/images/addproduct/category-custom<?php echo $on10; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br />Custom<br /><br />
                                                            </div>
                                                            <div class="col-sm-1">
                                                                <!-- spacer -->
                                                            </div>
                                                            <div class="col-sm-4 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>11]) }}">
                                                                    <img src="/images/addproduct/category-11<?php echo $on11; ?>.png"
                                                                        width="75" height="75"></a>
                                                                <br />Accessories<br /><br />
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- Category Side Bar-->
                                        </div>
                                    </div>
                                </div>
                                <!-- RIGHT BAR-->
                            </div>
                        </div>
                        <?php
                        //is Category set?
                        //checkmark on selected category image
                        $Category = isset($_GET['Category']) ? $_GET['Category'] : 0;
                        if ($Category) {
                            $CategorySQL = DB::select(DB::raw("SELECT Type FROM product_groups WHERE product_category_id = $Category GROUP BY Type ORDER BY Type"));
                        ?>
                        <?php
                        } // *if Category
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <!-- START:footer container -->
    <div class="container">
        @include('layouts.footer')
    </div>
    <!-- END:footer container -->

    @include('layouts.loadjs')
    <script>
        $('.search_cancel').click(function(){
            window.history.back();
        });
    //onclick of codes select pulldown
    $("#allItemCodes").change(function() {
        document.location.href =
            '/quotes_orders/search-products.php?JobID=<?php echo $jobID; ?>&JobAcct=<?php echo $jobAcct; ?>&knowCode=yes&GroupCode=' +
            $(this).val();
    });
    //prevent search from submit on no fields
    function empty() {
        var searchDescription, searchItemCode;
        searchItemCode = document.getElementById("searchItemCode").value;
        searchDescription = document.getElementById("searchDescription").value;
        if (searchItemCode == "" && searchDescription == "") {
            alert("Enter a value to search");
            return false;
        };
    }
    </script>
</body>

</html>