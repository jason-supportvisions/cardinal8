<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="user-scalable=yes,
                   initial-scale=1,
                   minimum-scale=0.2, 
                   maximum-scale=2, 
                   width=device-width" 
          name="viewport"
          id="viewport">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* -->
    <meta name="description" content="">
    <meta name="author" content="Cory Manufacturing">

    <title>{{config('app.name')}} | {{ $page_name }}</title>

    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png?v=3">
    <meta name="theme-color" content="#ffffff">

    <!-- Bootstrap 3.3.6 core CSS -->
    {{ HTML::style('/css/bootstrap.min.css') }}
    {{ HTML::style('/css/bootstrap-theme.min.css') }}
    {{ HTML::style('/css/select2.min.css') }}

    {{ HTML::style('/css/jquery-ui.css') }}
    {{ HTML::style('/css/jquery.dataTables.min.css') }} 

    {{ HTML::style('/css/custom.css?n=1') }}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]> 
    {{-- {{ HTML::style('/css/html5shiv.min.js') }}
    {{ HTML::style('/css/respond.min.js') }} --}}
    <![endif]-->

    @yield('styles')
</head>