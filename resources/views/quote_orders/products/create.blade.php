@extends('templates.default')

@section('content')

    {{ Form::open(['url' => route('store-product', ['JobID'=>$job_product->JobID])]) }}

        {{ Form::hidden('product_item_id', $job_product->product_item_id) }}

        @include('partials.products.general')

        @include('partials.products.dimensions')

        @include('partials.products.standard-options')

        @include('partials.products.modifications')

        @include('partials.products.add_edit_old')

    {{ Form::close() }}
 
@stop