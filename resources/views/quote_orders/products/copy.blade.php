@include('templates.function')
<?php
$Title              =  " | Add Product";
$Select2            = 0;
$Select2beta        = 1;
$DataTables         = 0;
$jQuery_UI          = 0;
$jQuery_Validate    = 1;
$jobPrint           = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing         = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

$pageid             = 'Products';
$jobID              = isset($jobID) ? $jobID : 0;
$jobAcct            = isset($_GET['jobAcct']) ? $_GET['jobAcct'] : 0;
$productID          = isset($productID) ? $productID : "";

?>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha256-pasqAKBDmFT4eHoN2ndd6lN370kFiGUFyTiUHWhU7k8=" crossorigin="anonymous"></script>
@include('layouts.header')
<!-- END:navbar -->
<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="panel panel-UX">
                            <div class="panel-body">
                                <div class="form-horizontal">

                                    <!-- GET copy ITEM info -->
                                    <div class="form-group">
                                        <div class="col-sm-3">
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX"><br><br>
                                                    <div class="panel-body text-center" style="height: 300px;">
                                                        <?php
                                                        $ProductSQL = DB::select(DB::raw("SELECT pi.GroupCode, pg.Image_File, jp.`ID`, jp.`Code`, jp.`Description`, jp.`Qty`, jp.`ListPer`, jp.`Notes`  
                                                        FROM `job_product` jp 
                                                        INNER JOIN `product_items` pi ON pi.Code = jp.Code 
                                                        INNER JOIN product_groups pg on pi.GroupCode = pg.GroupCode 
                                                        WHERE jp.`ID`= $productID"));

                                                        ?>
                                                        <?php
                                                        foreach ($ProductSQL as $row) {
                                                            $ID = $row->ID;
                                                            $Code = $row->Code;
                                                            $Image_File = $row->Image_File;
                                                            $Description = $row->Description;
                                                            $ListPer  = $row->ListPer;
                                                            $Notes  = $row->Notes;
                                                        ?>                                                            
                                                            <img src="/images/cabinets/<?php echo $Image_File; ?>" width="100" height="100">
                                                            <h4><?php echo $Code; ?></h4>
                                                            <h5><b><?php echo $Description; ?></b></h5>
                                                            <h5><b><?php echo "$" . $ListPer; ?></b></h5>
                                                            <h5><b><?php echo $Notes; ?></b></h5>

                                                        <?php
                                                        } //end while job product
                                                        ?>
                                                        <br />
                                                        <button onclick="window.history.back()" class=" btn btn-secondary">Back / Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-sm-9">
                                            <!-- COPY ACTIONS -->
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    <div class="panel-heading">Copy Actions</div>
                                                    <div class="panel-body" style="height: 300px;">
                                                        <div class="row">
                                                            <div class="col-sm-1 text-center" style=""></div>
                                                            <div class="col-sm-10" style="background-color: white;">
                                                            </div>
                                                        </div>
                                                        <br><br>
                                                        <div class="row" style="background-color: #ffffff;">
                                                            <div class="col-sm-1 text-center" ></div>
                                                            <div class="col-sm-4 text-center alert alert-success" style="background-color: white; height: 175px;">
                                                            <form method="post" action="{{route('paste-product')}}">
                                                                @csrf
                                                                <h4>COPY to this Job </h4>
                                                                <br>
                                                                <h4><?php echo $jobID ?></h4>
                                                                <br>
                                                                <button class="btn btn-success" type="submit">COPY <span class="glyphicon glyphicon-copy"></span></button>
                                                                <input type="hidden" value="no" name="copyJobAction">
                                                                <input type="hidden" value="<?php echo $jobID ?>" name="jobID">
                                                                <input type="hidden" value="<?php echo $productID; ?>" name="productID">
                                                                <input type="hidden" value="<?php echo $jobAcct; ?>" name="jobAcct">
                                                            </form>
                                                            </div>
                                                            <div class="col-sm-1">
                                                            </div>
                                                            <div class="col-sm-5 text-center alert alert-info" style="background-color: white; height: 175px;">
                                                                <h4>COPY to different Job</h4>
                                                                <br>
                                                                <?php
                                                                $JobSQL = DB::select(DB::raw("SELECT `JobID`, `Reference`, `List` FROM job WHERE `AccountID` = $SesAcct ORDER BY `JobID` DESC LIMIT 50"));
                                                                ?>
                                                                <form method="post" action="{{route('paste-product')}}">
                                                                    @csrf
                                                                    <select name="copyJob">
                                                                        <?php
                                                                        foreach ($JobSQL as $row) {
                                                                            $JobID      = $row->JobID;
                                                                            $Reference  = $row->Reference;
                                                                            $List       = $row->List;
                                                                            ?>
                                                                            <option value="<?php echo $JobID ?>"><?php echo $JobID ?> - <?php echo $Reference ?></option>
                                                                        <?php
                                                                        } //end while jobs
                                                                        ?>
                                                                    </select>
                                                                    <br>
                                                                    <br>
                                                                    <button class="btn btn-primary" type="submit">COPY <span class="glyphicon glyphicon-copy"></span></button>
                                                                    <input type="hidden" value="yes" name="copyJobAction">
                                                                    <input type="hidden" value="<?php echo $jobID ?>" name="jobID">
                                                                    <input type="hidden" value="<?php echo $productID; ?>" name="productID">
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- START:footer container -->
    <div class="container">
       @include('layouts.footer')
    </div>
    <!-- END:footer container -->
    @include('layouts.loadjs')
</body>
</html>