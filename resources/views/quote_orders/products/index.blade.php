
@extends('templates.default')

@section('content')
    @include('partials.products.nav', ['job'=>$job])
    <?php $add_or_edit = "edit"; ?>
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-UX">
                <div class="panel-heading">
                    <div class="pull-right">
                        <h5 style="margin:0px;">
                            Pricing: &nbsp;
                            <input type="radio" class="Price" name="Price" value="LIST" checked=""> List <small>(x 1)</small>&nbsp;&nbsp;
                            <input type="radio" class="Price" name="Price" value="COST"> Net <small>(x {{ number_format($job->multiplier_discount, 3) }})</small>&nbsp;&nbsp;
                            <input type="radio" class="Price" name="Price" value="CUSTOMER"> Customer <small>(x {{ number_format($job->multiplier_customer, 3) }})</small>&nbsp;
                        </h5>
                    </div>
                    <a href=" {{ route('add-product',['jobID'=>$job->JobID,'jobAcct'=>$job->AccountID, 'Category'=>0]) }} " > 
                    <button type="button" class="btn btn-success">Add Product</button></a>              
                </div>
                <div class="panel-body">
                    <div class="pull-right" style="margin:0px;">
                    <form action="{{ route('fastadd')}}" method="post">
                        @csrf
                    <?php 
                    //remember checked setting for fast add
                    if(isset($_SESSION['user']['FastAdd'])){
                        if($_SESSION['user']['FastAdd'] == "list"){
                            $listChecked = " checked";
                            $editChecked = "";
                        }else{
                            $editChecked = " checked";
                            $listChecked = "";
                        }
                    }else{
                        $editChecked = " checked";
                        $listChecked = "";
                    }

                    //if error returned in query string
                    if( session('alert') ){
                        $alert = session('alert');
                        session(['alert'=>'']);
                    }else{
                        $alert = "";
                    }
                    ?>
                        <span style="color: red;"><?php echo $alert; ?></span>
                        <input type="radio" value="edit" name="addType" <?php echo $editChecked; ?>><small>edit &nbsp;</small> 
                        <input type="radio" value="list" name="addType" <?php echo $listChecked; ?>><small>list &nbsp;&nbsp;</small>
                        <input type="hidden" value="<?php echo $job->JobID ?>" name="jobID">
                        <input type="text" value="" name="fastAdd" placeholder="Item Code" size="10">  
                        <input type="submit" value="Fast Add" id="fastSubmit">                
                    </form>
                    <script>
                            document.getElementById('fastSubmit').onkeydown = function(e){
                                if(e.keyCode == 13){
                                    document.getElementById("fastSubmit").click();
                                }
                            };
                        </script>
                    </div>
                    <div class="row">
                        @include('partials.products.list-table')
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop

@section('scripts')

<script>

    var jsJobID = "{{ $job->JobID }}";
    var jsJobAcct = "{{ $job->AccountID }}";

    //radio pricing buttons
    $('.Price').change(function(){
        
        // hide all prices 
        $('#price-total-row, .price-td').hide().removeClass('hide');
        
        //current radio button selected
        var price_option = $(this).val();

        //show as needed
        if(price_option != 'No'){
            $('#price-total-row').show();
            $('.'+price_option).show();
        }

    });
</script>

@stop

