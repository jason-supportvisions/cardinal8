@include('templates.function')
<?php

use App\Http\Controllers\RulesController;
// Page Settuseings (1 = True | 0 = False)
$Title              =  " | Add Product";
$Select2            = 0;
$Select2beta        = 1;
$DataTables         = 0;
$jQuery_UI          = 0;
$jQuery_Validate    = 0;
$xCRUD_16           = 0;
$jobPrint           = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing         = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

$pageid             = 'Add Products';
$jobID              = isset($jobID) ? $jobID : 0;
$jobAcct            = isset($jobAcct) ? $jobAcct : 0;
$productType        = "Blind Corner";
$mouldingSearch;
$mouldingShow       = "no";
$on1                = "";
$on2                = "";
$on3                = "";
$on4                = "";
$on5                = "";
$on6                = "";
$on7                = "";
$on8                = "";
$on9                = "";
$on10               = "";
$on11               = "";
$productID          = "";
$andSub             = "";
$OrderByGroup       = " ORDER BY ClassBuild desc, Category, Class, SubClass, catalog_order, GroupCode";
$OderByItem         = " ORDER BY GroupCode";
$OrderByGroupAcc    = " ORDER BY GroupCode";
$OderByItemAcc      = " ORDER BY catalog_order, ClassBuild, GroupCode, Code";


$Search              = isset($_GET['Search']) ? $_GET['Search'] : "";
$searchSubType       = isset($_GET['subType']) ? $_GET['subType'] : "";
$searchClassBuild    = isset($_GET['classBuild']) ? $_GET['classBuild'] : "";


//highlight rows
$highlightrow       = 0;
$highlightclass     = "add-product";
$buttonText         = "ADD";
$buttonLink         = True;

use Illuminate\Support\Facades\DB;


?>
<style>
    div {
        font-size: 12px;
    }
</style>
@include('layouts.product_header')
<body>
<div class="container">
    @include('layouts.navbar')
</div>
    <!-- START:primary content container -->
    <div class="container">
        @include('templates.progress')
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="row">
                    <div class="col-sm-12 col-md-12">
                        <div class="panel panel-UX">
                            <div class="panel-body">
                                <div class="form-horizontal">
                                    <!-- Category -->
                                    <div class="form-group">
                                        <div class="col-sm-9">
                                            <?php
                                            //checkmark on selected category image
                                            //some categories need multiple to show all of the products due to legacy catalog categories
                                            $Category = isset($Category) ? $Category : 0;

                                            if ($Category == "1") { //Wall
                                                $on1 = "-on";
                                                $Category = "1,2";
                                                $CategoryName = "Wall";
                                            } elseif ($Category == "2") {
                                                $on2 = "-on";
                                                $CategoryName = "Wall";
                                            } elseif ($Category == "3") {
                                                $on3 = "-on";
                                                $CategoryName = "Base";
                                            } elseif ($Category == "4") {
                                                $on4 = "-on";
                                                $Category = "4,5,6,7";
                                                $CategoryName = "Tall";
                                            } elseif ($Category == "5") {
                                                $CategoryName = "Tall";
                                                $on5 = "-on";
                                                $CategoryName = "Tall";
                                            } elseif ($Category == "6") {
                                                $on6 = "-on";
                                                $CategoryName = "Tall";
                                            } elseif ($Category == "7") {
                                                $on7 = "-on";
                                                $CategoryName = "Tall";
                                            } elseif ($Category == "8") {
                                                $on8 = "-on";
                                                $CategoryName = "Panels";
                                            } elseif ($Category == "9") {
                                                $on9 = "-on";
                                                $CategoryName = "Vanity";
                                            } elseif ($Category == "10") {
                                                $on10 = "-on";
                                                $CategoryName = "Custom";
                                            } elseif ($Category == "11") {
                                                $on11 = "-on";
                                                $CategoryName = "Accessory";
                                            } elseif ($Category == "12") {
                                                $on12 = "-on";
                                            }
                                            ?>
                                            <style>
                                                
                                                .myButton {
                                                    box-shadow:inset 0px 39px 0px -24px #e67a73;
                                                    background-color:#e4685d;
                                                    border-radius:4px;
                                                    border:1px solid #ffffff;
                                                    display:inline-block;
                                                    cursor:pointer;
                                                    color:#ffffff;
                                                    font-family:Arial;
                                                    font-size:15px;
                                                    padding:6px 15px;
                                                    text-decoration:none;
                                                    text-shadow:0px 1px 0px #b23e35;
                                                }
                                                .myButton:hover {
                                                    background-color:#eb675e;
                                                }
                                                .myButton:active {
                                                    position:relative;
                                                    top:1px;
                                                }

                                                .css-input {
                                                padding: 6px;
                                                font-size: 14px;
                                                border-width: 2px;
                                                border-color: #dad5d5;
                                                background-color: #FFFFFF;
                                                width: 80%;
                                                color: #000000;
                                                border-style: solid;
                                                border-radius: 0px;
                                                box-shadow: -50px 0px 0px rgba(255,255,255,.0);
                                                text-shadow: -50px 0px 0px rgba(255,255,255,.0);
                                            }
                                            .css-input:focus {
                                                outline:none;
                                            }

                                            </style>
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    <div class="panel-body" style="height:">
                                                        <form method="post" action="{{ route('search-product') }}" name="autocomplete-textbox" id="autocomplete-textbox">
                                                            @csrf
                                                                <h4>Search the Catalog</h4>
                                                                <input type="text" id="autocomplete" class="css-input" name="autosearch" placeholder="">
                                                                <input type="hidden" id="jobid" class="form-control" name="jobID" value="{{ $jobID }}">
                                                                <input type="hidden" id="jobacct" class="form-control" name="jobAcct" value="{{ $jobAcct }}">
                                                                <input type="submit" value="Submit" class="myButton" onClick="return input_empty()">
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-12">
                                                    <div class="text-center">
                                                        <h4 style="align-content: center">OR</h4>
                                                    </div>
                                                </div>    
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    {{-- <div class="panel-heading">ADD PRODUCTS</div> --}}
                                                    <h4>&nbsp;&nbsp;&nbsp;Choose a Category</h4>
                                                    <div class="panel-body">
                                                        <div class="row" style="background-color: #ffffff;">
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>1]) }}#bases">
                                                                    <img src="/images/addproduct/category-1<?php echo $on1; ?>.png" width="100" height="100"></a>
                                                                <br />Wall
                                                            </div>
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>3]) }}#bases">
                                                                    <img src="/images/addproduct/category-3<?php echo $on3; ?>.png" width="100" height="100"></a>
                                                                <br />Base
                                                            </div>
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>4]) }}#bases">
                                                                    <img src="/images/addproduct/category-4<?php echo $on4; ?>.png" width="100" height="100"></a>
                                                                <br />Tall
                                                            </div>
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>9]) }}#bases">
                                                                    <img src="/images/addproduct/category-9<?php echo $on9; ?>.png" width="100" height="100"></a>
                                                                <br>Vanity
                                                            </div>
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>10]) }}#bases">
                                                                    <img src="/images/addproduct/category-custom<?php echo $on10; ?>.png" width="100" height="100"></a>
                                                                <br />Custom
                                                            </div>
                                                            <div class="col-sm-2 text-center">
                                                                <a href=" {{ route('add-product',['jobID'=>$jobID,'jobAcct'=>$jobAcct, 'Category'=>11]) }}#bases">
                                                                    <img src="/images/addproduct/category-11<?php echo $on11; ?>.png" width="100" height="100"></a>
                                                                <br />Accessories
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                
                                            
                                            
                                            </div>
                                            
                                        </div>
                                        <div class="col-sm-3">
                                            <!-- custom search -->
                                            <div class="col-sm-12">
                                                <div class="panel panel-UX">
                                                    <div class="panel-heading">Advanced Search</div>
                                                    <div class="panel-body" style="height: 185px;">
                                                        <?php
                                                        //if search was made
                                                        if ($Search != "") {
                                                            //Item Code
                                                            if ($searchItemCode != "") {
                                                                //get the GroupCode of the item we're looking for
                                                                $itemSQL    = DB::select(DB::raw('(SELECT GroupCode FROM accessory_items WHERE Code LIKE "' . $searchItemCode . '%") UNION (SELECT GroupCode FROM product_items WHERE Code LIKE "' . $searchItemCode . '%")' . $OrderByItem));
                                                                foreach ($itemSQL as $row) {
                                                                    $itemGroupCode = $row->GroupCode;
                                                                }
                                                                //get all the items for that GroupCode
                                                                $groupSQL     = DB::select(DB::raw('(SELECT GroupCode, Description, BookDescription, Type, ClassBuild, Category, Class, SubClass, catalog_order FROM product_groups WHERE GroupCode = "' . $itemGroupCode . '") UNION (SELECT GroupCode, Description, BookDescription, Type, ClassBuild, Category, Class, SubClass, catalog_order FROM  accessory_groups WHERE GroupCode = "' . $itemGroupCode . '") ' . $OrderByGroup));
                                                                $ProductSQL = $groupSQL; //assign it to this for results                                                               
                                                            }
                                                            //ItemCodeAll
                                                            if ($searchItemCodeAll != "") {
                                                                // echo "<br> item code pulldown ... ";
                                                                $searchItemCodeAllSQL = DB::select(DB::raw('SELECT GroupCode, Description, BookDescription, Type, ClassBuild, Category, Class, SubClass, catalog_order FROM product_groups WHERE GroupCode = "' . $searchItemCodeAll . '" ' . $OrderByGroup));
                                                                $ProductSQL = $searchItemCodeAllSQL; //assign it to this for results
                                                            }
                                                            //Description
                                                            if ($searchDescription != "") {
                                                                // echo "<br> description search ... ";
                                                                $descriptionSQL = DB::select(DB::raw('SELECT GroupCode, Description, BookDescription, Type, ClassBuild, Category, Class, SubClass, catalog_order FROM product_groups WHERE Type LIKE "%Drawer" Order by Type ' . $OrderByGroup));
                                                                $ProductSQL = $descriptionSQL; //assign it to this for results
                                                            }
                                                        }
                                                        ?>
                                                        <form action="{{ route('search-product') }}" method="post">
                                                            @csrf
                                                            <div class="form-horizontal">
                                                                <div class="col-sm-1">
                                                                    <!-- spacer -->
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <div class="form-group">
                                                                        <input type="text" id="searchItemCode" class="form-control" name="searchItemCode" style="width: 200px;" placeholder="Exact Item Code">
                                                                    </div>
                                                                    <div class="form-group">
                                                                        <input type="text" id="searchDescription" class="form-control" name="searchDescription" style="width: 200px;" placeholder="Description Keyword Search">
                                                                        <input type="hidden" id="jobid" class="form-control" name="jobID" value="{{ $jobID }}">
                                                                        <input type="hidden" id="jobacct" class="form-control" name="jobAcct" value="{{ $jobAcct }}">
                                                                    </div>
                                                                    <div class="form-group pull-right">
                                                                        <button type="submit" class="btn btn-primary btn-sm" onClick="return empty()">
                                                                            Search
                                                                        </button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                            
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                      
                                          
                                           
                       
                                   
                                    
                                    {{-- <div class="col-sm-3"></div> --}}
                                    
                                        
                                  
                                    <?php
                                    //get the spec info
                                    $SpecSQL = DB::select(DB::raw("SELECT StyleColorID, StyleMaterialID FROM job_construction WHERE JobID = $jobID"));
                                    foreach($SpecSQL as $row){
                                        
                                        $StyleColorID       = $row->StyleColorID;
                                        $StyleMaterialID    = $row->StyleMaterialID;

                                        // echo "StyleColorID: " . $StyleColorID. "<br>";
                                        // echo "StyleMaterialID: " . $StyleMaterialID . "<br>";

                                    }
                                    //is Category set?
                                    if ($Category) {
                                        if ($Category == 11) {
                                            $group_table = "accessory_groups";
                                            $item_table = "accessory_items";
                                            $product_type = "accessory";
                                            $CategorySQL = DB::select(DB::raw("SELECT ID, Code, ClassBuild FROM $item_table WHERE Active = 1 GROUP BY ClassBuild ORDER BY ClassBuild"));
                        
                                        } else {
                                            $group_table = "product_groups";
                                            $item_table = "product_items";
                                            $product_type = "product";
                                            $CategorySQL = DB::select(DB::raw("SELECT ID, GroupCode, Type, ClassBuild FROM $group_table WHERE product_category_id IN ($Category) GROUP BY Type ORDER BY Type"));
                                        }
                                    ?>
                                        <!-- Type -->
                                        <div class="form-group">
                                            <div class="col-sm-9">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-UX"><a id="bases"></a>
                                                        <h4>&nbsp;&nbsp;&nbsp;2. Filter by Type</h4>
                                                       
                                                       <?php
                                                        // ==============================================================
                                                        // if BASES then show the top level Dartmouth, Spectra, Standard
                                                        // ==============================================================
                                                        if ($Category == "3") {
                                                        ?>
                                                            <div class="panel-body">
                                                                <div class="row col-sm-12" style="background-color: #ffffff; text-align:center; padding-left: 50px;">
                                                                    <div class="pull-left col-sm-3" style=" padding:0px; text-align: left;">
                                                                        <a href="?classBuild=Standard&subType=Standard#bases" ><img src="/images/addproduct/base-standard.png" ></a>
                                                                        <br /><br />
                                                                      
                                                                            <div class="pull-center"><h4>Standard</h4></div>
                                                                            <?php
                                                                            echo '<ul>';
                                                                            foreach ($CategorySQL as $row) {
                                                                                $productType = $row->Type;
                                                                                $rowCount = count($CategorySQL);
                                                                                // echo strpos($productType, 'Spectra');
                                                                                if(strpos($productType, 'Spectra') !== 0 && strpos($productType, 'Dartmouth') !== 0){
                                                                                    echo '<li><a href="?subType=' . $productType . '#product">' . $productType . '</a></li>';
                                                                                }
                                                                                
                                                                            }
                                                                            echo '</ul>';
                                                                            ?>
                                                                     
                                                                    </div>
                                                                    <div class="pull-left col-sm-1"></div>
                                                                    <div class="pull-left col-sm-3" style=" padding:3px; text-align: left;">
                                                                        <a href="?classBuild=Dartmouth&subType=Dartmouth#bases"><img src="/images/addproduct/base-dartmouth.png"></a>
                                                                        <br /><br />
                                                                        
                                                                            <div ><h4>Dartmouth</h4></div>
                                                                            <?php
                                                                            echo '<ul>';
                                                                            foreach ($CategorySQL as $row) {
                                                                                $productType = $row->Type;
                                                                                $rowCount = count($CategorySQL);
                                                                                // echo strpos($productType, 'Spectra');
                                                                                if(strpos($productType, 'Dartmouth') === 0){
                                                                                    echo '<li><a href="?subType=' . $productType . '#product">' . $productType . '</a></li>';
                                                                                }
                                                                                
                                                                            }
                                                                            echo '</ul>';
                                                                            ?>
                                                                        
                                                                    </div>
                                                                    <div class="pull-left col-sm-1"></div>
                                                                    <div class="pull-left col-sm-3" style=" padding:3px; text-align: left;">
                                                                        <a href="?classBuild=Spectra&subType=Spectra#bases"><img src="/images/addproduct/base-spectra.png"></a>
                                                                        <br /><br />
                                                                            <div ><h4>Spectra</h4></div>
                                                                            <?php
                                                                            echo '<ul>';
                                                                            foreach ($CategorySQL as $row) {
                                                                                $productType = $row->Type;
                                                                                $rowCount = count($CategorySQL);
                                                                                // echo strpos($productType, 'Spectra');
                                                                                if(strpos($productType, 'Spectra') === 0){
                                                                                    echo '<li><a href="?subType=' . $productType . '#product">' . $productType . '</a></li>';
                                                                                }
                                                                                
                                                                            }
                                                                            echo '</ul>';
                                                                            ?>
                                                                    </div>
                                                           </div>
                                                        
                                                            <?php
                                                        } else { 
                                                            // ========================================
                                                            //end if bases
                                                            // ========================================
                                                            ?>
                                                                <div class="panel-body" style="height: 150px;">
                                                                        <div class="row" style="background-color: #ffffff; text-align:center; padding-left: 15px;">
                                                                        <?php

                                                                        //if accessory show classbuild not type
                                                                        if ($Category == 11) {
                                                                            foreach ($CategorySQL as $row) {
                                                                                $productType = $row->ClassBuild;
                                                                                $Code        = $row->Code;
                                                                                $rowCount = count($CategorySQL);
                                                                                if($searchSubType == $productType){
                                                                                    
                                                                                ?>  
                                                                                    <div class="pull-left" style="width:200px;  padding:3px; text-align: left;"><span class="glyphicon"></span>
                                                                                    <a href="?subType=<?php echo $productType; ?>#accessory" style="color: #23527c; text-decoration: underline;"><?php echo $productType; ?></a>
                                                                                    </div>
                                                                                <?php
                                                                                }else{
                                                                                ?>
                                                                                    <div class="pull-left" style="width:200px;  padding:3px; text-align: left;"><span class="glyphicon"></span>
                                                                                    <a href="?subType=<?php echo $productType; ?>#accessory"><?php echo $productType; ?></a><br />
                                                                                    </div>
                                                                            <?php
                                                                                }
                                                                            }
                                                                        } else {
                                                                            foreach ($CategorySQL as $row) {
                                                                                $productType = $row->Type;
                                                                                $rowCount = count($CategorySQL);
                                                                                if($searchSubType == $productType){ 
                                                                            ?>
                                                                                <div class="pull-left" style="width:200px;  padding:3px; text-align: left;"><span class="glyphicon"></span>
                                                                                    <a href="?subType=<?php echo $productType; ?>#product" style="color: #23527c; text-decoration: underline;"><?php echo $productType; ?></a>
                                                                                </div>
                                                                            <?php
                                                                                }else{
                                                                            ?>
                                                                                <div class="pull-left" style="width:200px;  padding:3px; text-align: left;"><span class="glyphicon"></span>
                                                                                    <a href="?subType=<?php echo $productType; ?>#product"><?php echo $productType; ?></a>
                                                                                </div>
                                                                        <?php
                                                                            }
                                                                            }
                                                                        }
                                                                        ?>
                                                                    </div>
                                                                <?php
                                                            }
                                                            ?>
                                                            </div>
                                                            </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">

                                                </div>
                                            </div>
                                            <!-- end type -->

                                            <!-- results -->
                                            <div class="form-group">
                                                <div class="col-sm-9">
                                                    <div class="col-sm-12">
                                                        <div class="panel panel-UX add_result">
                                                            <?php
                                                            if ($searchSubType != "") {
                                                                $titleSubType = " -- [$searchSubType]";
                                                            } else {
                                                                $titleSubType = " -- [all $CategoryName]";
                                                            }
                                                            ?>
                                                            <!-- each product group -->
                                                            <?php
                                                            //if search wasn't made then do it by subtype
                                                            if ($Search == "") {
                                                                //accessory
                                                                if ($Category == 11) {
                                                                    $ProductSQL = DB::select(DB::raw("SELECT `ID`, `GroupCode`, `Description`, `Image_File`, `ClassBuild`, `Class`, `Type`, `Notes` FROM `accessory_items`  WHERE  `Active` = 1 AND `ClassBuild` LIKE '%" . $searchSubType . "%'" . $OrderByGroupAcc));
                                                                } else {
                                                                    $ProductSQL = DB::select(DB::raw("SELECT `ID`, `GroupCode`, `Description`, `BookDescription`, `Image_File`, `ClassBuild`, `Class`, `Type`, `Notes` FROM $group_table WHERE `catalog_category_id` IN ($Category) AND `Active` = 1 AND `Type` LIKE '%" . $searchSubType . "%'" . $OrderByGroup));
                                                                   
                                                                }
                                                                //handle base cabinet types Standard/Dartmouth/Spectra
                                                                if ($searchClassBuild != "") {
                                                                    $ProductSQL = DB::select(DB::raw("SELECT `ID`, `GroupCode`, `Description`, `BookDescription`, `Image_File`, `ClassBuild`, `Class`, `Type`, `Notes` FROM $group_table WHERE `catalog_category_id` IN ($Category) AND `Active` = 1 AND `ClassBuild` LIKE '%" . $searchClassBuild . "%'" . $OrderByGroup));
                                                                }
                                                            }
                                                            //if search item
                                                            ?><a id="accessory"></a>
                                                                <a id="product"></a>
                                                            <h4>&nbsp;&nbsp;&nbsp;3. Select Product <span style="color: grey;"><?php echo $titleSubType; ?>
                                                                    <small>(# Groups of Items <?php echo count($ProductSQL); ?>)</small></span></h4>
                                                            <div class="panel-body">
                                                                
                                                                <?php
                                                                //if accessory
                                                                if ($Category == 11) {

                                                                    //change to class build to fix standard
                                                                    if ($searchSubType != "") {
                                                                        $andSub = " AND i.ClassBuild LIKE '%" . $searchSubType . "%'";
                                                                    } elseif ($searchClassBuild != "") {
                                                                        $andSub = " AND i.ClassBuild LIKE '%" . $searchClassBuild . "%'";
                                                                    } else {
                                                                        $andSub = " ";
                                                                    }
                                                                    //items in a group
                                                                    $ItemSQL = DB::select(DB::raw("SELECT i.Code, i.ID, i.Width, i.MinWidth, i.MaxWidth, i.Description, i.Image_File, i.Image, i.ClassBuild FROM `accessory_items` i WHERE i.Active = 1 $andSub"));
                                                                     
                                                                ?>
                                                                    <div class="row" style="background-color: #ffffff;">
                                                                        <?php
                                                                        foreach ($ItemSQL as $row) {
                                                                            $itemID         = $row->ID;
                                                                            $Code           = $row->Code;
                                                                            $Width          = $row->Width;
                                                                            $MinWidth       = $row->MinWidth;
                                                                            $MaxWidth       = $row->MaxWidth;
                                                                            $Description    = $row->Description;
                                                                            $Image_File     = $row->Image_File;
                                                                            $Image          = $row->Image;
                                                                            $ClassBuild     = $row->ClassBuild;

                                                                            //=============================================
                                                                            // Accessory PANELS rule
                                                                            //=============================================

                                                                            $ShowAccessory = true;
                                                                            $buttonColor = "btn-success";
                                                                            //is this accessory a panel?
                                                                            if ($ClassBuild == "Panel1" || $ClassBuild == "Panel2" || $ClassBuild == "Panel1,Panel2") {
                                                                                // echo "panels chosen <br>";
                                                                                $panelCheck = true;

                                                                                //what door are they using in their spec?
                                                                                $JobSQL = DB::select(DB::raw("SELECT DoorStyleID FROM job_construction WHERE JobID = $jobID"));
                                                                                $DoorStyleID = 0;
                                                                                foreach ($JobSQL as $row) {
                                                                                    $DoorStyleID = $row->DoorStyleID;
                                                                                }

                                                                                //Does this door allow panels?
                                                                                $DoorSQL = DB::select(DB::raw("SELECT PanelsAllowed FROM styles WHERE ID = $DoorStyleID"));
                                                                                $PanelsAllowed = 0;
                                                                                foreach ( $DoorSQL as $row ) {
                                                                                    $PanelsAllowed = $row->PanelsAllowed;
                                                                                    $debugReason = "Panels Allowed by Door = " . $PanelsAllowed . "<br>";
                                                                                }

                                                                                //Does it allow 1 sided, 2 sided, both?
                                                                                if ($PanelsAllowed == "Panel1" && $ClassBuild == "Panel1") {
                                                                                    $buttonLink = true;
                                                                                    $buttonText = "ADD";
                                                                                    $ShowAccessory = true;
                                                                                    $Panel1Only = true;
                                                                                    $debugReason .= '1/1 - ' . $PanelsAllowed . ' == "Panel1" &&'. $ClassBuild .' == "Panel1"';

                                                                                } elseif ($PanelsAllowed == "Panel2" && $ClassBuild == "Panel2") {
                                                                                    $buttonLink = true;
                                                                                    $buttonText = "ADD";
                                                                                    $ShowAccessory = true;
                                                                                    $Panel2Only = true;
                                                                                    $debugReason .= '2/2 - ' . $PanelsAllowed . ' == "Panel2" && '. $ClassBuild .'  == "Panel2"';

                                                                                } elseif ($PanelsAllowed == "Panel1,Panel2" && ($ClassBuild == "Panel1,Panel2" || $ClassBuild == "Panel1" || $ClassBuild == "Panel2")) {
                                                                                    $buttonLink = true;
                                                                                    $buttonText = "ADD";
                                                                                    $ShowAccessory = true;
                                                                                    $PanelAll = true;
                                                                                    $debugReason .= '12/12 - ' .$PanelsAllowed . ' == "Panel1,Panel2" && ('. $ClassBuild .'  == "Panel1,Panel2" || '. $ClassBuild .'  == "Panel1" || '. $ClassBuild .'  == "Panel2")';

                                                                                } else {
                                                                                    $buttonLink = false;
                                                                                    $buttonText = "Not available because of door style";
                                                                                    $buttonColor = "btn-secondary";
                                                                                    $buttonMessage = "Panels are not allowed when using certain door styles. _______ Does not allow for panels";
                                                                                    $ShowAccessory = false;
                                                                                    $debugReason .= '0/0 - ' .$PanelsAllowed . ' == "blank or Classbuild blank "' . $ClassBuild;
                                                                                }
                                                                                // echo "<br> " . $debugReason;

                                                                            } else {
                                                                                $panelCheck = false;
                                                                            }
                                                                            //=============================================
                                                                            // /* Accessory PANELS rule
                                                                            //=============================================

            //=============================================
            // MOULDINGS RULE
            //=============================================

            if($ClassBuild == "Mouldings"){ 

                    $mouldingSearch = "";
                    $mouldingShow;   
                    $buttonLink;     
                    $buttonColor;    
                    $buttonText;     
                    $ruleResponse;
                    $runRule = false;
                    
                    /*
                    LAM
                        LAM
                        Mouldings
                        23,131,132,233,234
                        56
                        Available
                        Not available because of MATERIAL choice
                        234 LPL ( TFL )  - FIN - Textured         
                    */


                    if(str_contains($Code,"LAM")){  $ruleResponse   = RulesController::Mouldings("LAM", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"VEN")){  $ruleResponse   = RulesController::Mouldings("VEN", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"PG")){   $ruleResponse   = RulesController::Mouldings("PG", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"SSTK")){ $ruleResponse   = RulesController::Mouldings("SSTK", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"TRM")){  $ruleResponse   = RulesController::Mouldings("TRM", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"WD")){   $ruleResponse   = RulesController::Mouldings("WD", $StyleMaterialID); $runRule = true;}
                    if(str_contains($Code,"CRW")){  $ruleResponse   = RulesController::Mouldings("CRW", $StyleMaterialID); $runRule = true;}

                    // //Loop through any rules returned
                    if($runRule == true){
                        foreach ($ruleResponse as $ruleAttribute){
                            $mouldingSearch = $ruleAttribute["name"];
                            $mouldingShow   = $ruleAttribute["show"];
                            $buttonLink     = $ruleAttribute["show"];
                            $buttonColor    = $ruleAttribute["btnColor"];
                            $buttonText     = $ruleAttribute["btnText"];
                            $notes          = $ruleAttribute["notes"];
                            
                            // if($mouldingShow == true){
                            //     print_r($ruleResponse);
                            // }else{
                            //     echo "<br><i>don't show this one because: " . $notes . "</i>";
                            // }
                        }
                        // print_r($ruleResponse);
                        
                    }


                    // echo "moulding found: " . $Code . " - " . $mouldingSearch  .  "<br>";
                    
            }
?>
                            <!-- Items for group -->
                            
                            <div class="col-sm-12">
                                <div class="col-sm-3" style="background: #ffffff;">
                                    <br><br><br>
                                    <img src="/images/accessories/<?php echo $Image_File; ?>" width="150" alt="<?php echo $Image_File; ?>">
                                </div>
                                <div class="col-sm-9">
                                    <br><br>
                                    <h4><?php echo $Code; ?> <br><span style="color:Gray"><?php echo $Description; ?></span> </h4>
                                    <?php
                                    if ($Width != 0) {
                                    ?>
                                        Widths: <?php echo round($MinWidth, 4); ?>" - <?php echo round($MaxWidth, 4); ?>"<br>
                                    <?php
                                    }
                                    ?>

                                    <?php
                                    
                                    if ($buttonLink == true || $mouldingShow == true) {?>
                                        <a href="{{ route('create-product',['jobID'=>$jobID,'product_item_id'=>$itemID, 'productType'=>$product_type]) }}">
                                        <button type="submit" class="btn <?php echo $buttonColor; ?> btn-xs" style=""><?php echo $buttonText; ?></button></a>
                                <?php    
                                } else {?>
                                        <a><button type="submit" class="btn <?php echo $buttonColor; ?> btn-xs" style=""><?php echo $buttonText; ?></button></a>
                                    <?php
                                    }
                                    ?>

                                    
                                    <hr />
                                </div>
                            </div>
                            <!-- end item row -->

                        <?php } //end item while 
                        ?>
                    </div>
                    <?php
                }
                //If not accessory
                if ($Category != "11") {
                    foreach ($ProductSQL as $row) {
                        $productID      = $row->ID;
                        $GroupCode      = $row->GroupCode;
                        $Description    = $row->Description;
                        $BookDescription = $row->BookDescription;
                        $Image_File     = $row->Image_File;
                        $ClassBuild     = $row->ClassBuild;
                        $Class          = $row->Class;
                        $Type           = $row->Type;
                        $Notes          = $row->Notes;
                    ?>
                        <div class="row" style="background-color: #ffffff;">
                            <div class="col-sm-12">
                                <h4><?php echo $BookDescription; ?></h4>
                            </div>
                            <div class="col-sm-4" style="text-align:center;">
                                <img src="/images/cabinets/<?php echo $Image_File; ?>" width="200" height="200" alt="<?php echo $Image_File; ?>"> <br />
                            </div>
                            <div class="col-sm-8">
                                <br /><br />
                                <ul>
                                    <?php
                                    $note = $Notes;
                                    // Get an array of items from lines
                                    $list = explode("\n", $note);
                                    // Output the items as list elements
                                    foreach ($list as $num => $item) {
                                        if ($item != "") {
                                            $item = nl2br($item);                                                                                            
                                        }
                                    }
                                    ?>
                                </ul>
                                <br /><br />
                                <div class="col-sm-12">
                                    <div class="col-sm-5">
                                        <strong>Code</strong>
                                    </div>
                                    <div class="col-sm-2">
                                        <strong>Size</strong>
                                    </div>
                                    <div class="col-sm-5">
                                        <strong> </strong>
                                    </div>
                                </div>
                                <!-- add product css -->
                                <style>
                                    .add-product-container {
                                        display: inline-block;
                                    }

                                    .add-product-text {
                                        padding-top: 7px;
                                        height: 50px;
                                    }

                                    .add-product {
                                        background: #F5F5F5;
                                    }
                                </style>
                                <!-- add product css -->
                                <?php
                                //items in a group
                                $ItemSQL = DB::select(DB::raw("SELECT ID, Code, Width FROM $item_table WHERE GroupCode = '$GroupCode' AND Active = 1 ORDER BY Width"));

                                foreach ($ItemSQL as $row) {
                                    $itemID = $row->ID;
                                    $Code = $row->Code;
                                    $Width = $row->Width;

                                    if ($highlightrow == 0) {
                                        $highlightclass = "";
                                        $highlightrow = 1;
                                    } else {
                                        $highlightclass = "";
                                        $highlightrow = 0;
                                    }
                                ?>
                                    <?php
                                    if ($Category != 11) {
                                    ?>
                                        <!-- Items for group -->
                                        <br><br>
                                        <div class="col-sm-12 add-product-container">

                                            <div class="col-sm-5 add-product-text <?php echo $highlightclass; ?>">
                                                <strong><?php echo $Code; ?></strong>
                                            </div>
                                            <div class="col-sm-2  add-product-text <?php echo $highlightclass; ?>">
                                                <?php
                                                if ($Width == "0") {
                                                    $Width = "n/a";
                                                } else {
                                                    $Width = round($Width, 2);
                                                }
                                                echo "<span style='padding:2px;'>" . $Width . "\"</span>";
                                                ?>
                                            </div>
                                            <div class="col-sm-5 pull-left <?php echo $highlightclass; ?>">
                                                <a href="{{ route('create-product',['jobID'=>$jobID,'product_item_id'=>$itemID, 'productType'=>$product_type]) }}">
                                                    <button type="submit" class="btn btn-success btn-sm" style=""><?php echo $buttonText; ?></button>
                                                </a>
                                            </div>

                                        </div>
                                    <?php
                                    } //not an accessory end if
                                    ?>
                                    <!-- end item row -->
                                <?php } //end item while 
                                ?>
                            </div>
                        </div>
                        <hr>
                    <?php
                        //If not end accessory
                    }
            
                } // *while sql 
                ?>
                <!-- end each product -->
            </div>
        </div>
                                                        <div class="panel panel-UX search_result">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <!-- spacer below search -->
                                                </div>
                                            </div>
                                            <!-- end results -->
                                        <?php
                                    } // *if Category
                                        ?>
                                        <div class="form-group search_result_div" >
                                            <div class="col-sm-9">
                                                <div class="col-sm-12">
                                                    <div class="panel panel-UX search_result">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <!-- spacer below search -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START:footer container -->
    <div class="container">
        @include('layouts.footer')
    </div>
    <!-- END:footer container -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/search-autocomplete.js') }}" ></script>
  
    <script>
        //onclick of codes select pulldown
        $("#allItemCodes").change(function() {
            document.location.href = '/quotes_orders/search-products.php?JobID=<?php echo $jobID; ?>&JobAcct=<?php echo $jobAcct; ?>&knowCode=yes&GroupCode=' + $(this).val();
        });
         
    </script>
</body>
</html>