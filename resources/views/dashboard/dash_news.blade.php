<!-- NEWS/DOWNLOADS -->
<div class="panel panel-UX" style="max-height:370px;">
	<div class="panel-heading">News/Downloads</div>
	<div class="panel-body" style="margin:7px; padding:0px;">
		<div class="row">
			<div class="col-xs-12">
				@php $ndResults = $jobs[4]; @endphp
				@foreach($ndResults as $row) 
					<div class="col-xs-12" style="max-height:50px;">
							<?php 
								if (strlen($row->Subject) >= 40){ 
									$title = substr($row->Subject, 0, 40) . " ...";
								} else { 
									$title = $row->Subject;
								} 
							?>
							
							<?php if($row->Type == 'Bulletin' || $row->Type == 'News' ){ ?>
								<a href="../news/index.php?id=<?php echo $row->ID; ?>"><?php echo $title; ?></a>
							<?php } else { ?>
								<!-- <img src="/images/news/pdf-icon.png" width="20" height="20"> --><a href="../downloads/<?php echo $row->File; ?>" target="_blank"><?php echo $title; ?></a>
							<?php } ?>
							<br />
							<small><?php echo $row->Type; ?>:</small> 
							<small><?php echo $row->Date; ?></small>
							<br /><br /></span>
					</div>

				@endforeach
				<div class="col-xs-12" style="max-height:50px;"><br/><br/><br/></div>
			
			</div>
		</div>
	</div>
</div>
<!-- NEWS/DOWNLOADS -->