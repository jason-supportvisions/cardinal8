@php
	$statLabel = '';
	$AccountName = '';
	$jobResults = $jobs[3];
	if(!$jobResults){
		$for = "for All Accounts";
		$Quote = 0;
		$Confirmed = 0;
		$UnderReview = 0;
		$Accepted = 0;
		$Shipping = 0;
		$Completed = 0;
		$Cancelled = 0;
		$TotalJobs = 0;
		$Total = 0;
	}else{

		foreach ($jobResults as $row){
			$for = "All Accounts";
			if($row->Quote === NULL){$Quote = 0;} else {$Quote = $row->Quote;}
			if($row->Confirmed === NULL){$Confirmed = 0;} else {$Confirmed = $row->Confirmed;}
			if($row->UnderReview === NULL){$UnderReview = 0;} else {$UnderReview = $row->UnderReview;}
			if($row->Accepted === NULL){$Accepted = 0;} else {$Accepted = $row->Accepted;}
			if($row->Shipping === NULL){$Shipping = 0;} else {$Shipping = $row->Shipping;}
			if($row->Completed === NULL){$Completed = 0;} else {$Completed = $row->Completed;}
			if($row->Cancelled === NULL){$Cancelled = 0;} else {$Cancelled = $row->Cancelled;}
			if($row->TotalJobs === NULL){$TotalJobs = 0;} else {$TotalJobs = $row->TotalJobs;}
			if($row->Total === NULL){$Total = 0;} else {$Total = $row->Total;}
		}
	}
@endphp

<!-- JOB STATS -->
<div class="panel panel-UX">
	<div class="panel-heading"><span class="pull-right">(Lifetime)</span> Stats {{ $statLabel }} {{ $for }} </div>
	@if($Total != 0)
	<div class="panel-body" style="margin-bottom: 5px;">



		<label class="col-md-4 text-right">Quotes</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{{ $Quote/$Total*100 }}" aria-valuemin="0" aria-valuemax=" {{ $Total }} " style="width:<?php echo $Confirmed/$Total*100; ?>%">
					{{ $Quote }}
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Confirmed</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow=" {{ $Confirmed}}/{{$Total}}*100 }}" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Confirmed / $Total * 100; ?>%">
					<?php echo $Confirmed; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Review</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $UnderReview / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $UnderReview / $Total * 100; ?>%">
					<?php echo $UnderReview; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Accepted</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo $Accepted / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Accepted / $Total * 100; ?>%">
					<?php echo $Accepted; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Shipping</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $Shipping / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Shipping / $Total * 100; ?>%">
					<?php echo $Shipping; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Completed</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $Completed / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Completed / $Total * 100; ?>%">
					<?php echo $Completed; ?>
				</div>
			</div>
		</div>
		<hr>
		<label class="col-md-4 text-right">Cancelled</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?php echo $Cancelled / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Cancelled / $Total * 100; ?>%">
					<?php echo $Cancelled; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Total</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="<?php echo $Total / $Total * 100; ?>" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" style="width:<?php echo $Total / $Total * 100; ?>%">
					<?php echo $Total; ?>
				</div>
			</div>
		</div>

	</div>
	@else
	<div class="panel-body" style="margin-bottom: 5px;">
	<label class="col-md-4 text-right">Quotes</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax=" {{ $Total }} " >
					{{ $Quote }}
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Confirmed</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Confirmed; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Review</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $UnderReview; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Accepted</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Accepted; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Shipping</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Shipping; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Completed</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Completed; ?>
				</div>
			</div>
		</div>
		<hr>
		<label class="col-md-4 text-right">Cancelled</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Cancelled; ?>
				</div>
			</div>
		</div>

		<label class="col-md-4 text-right">Total</label>
		<div class="col-md-8">
			<div class="progress">
				<div class="progress-bar progress-bar-primary" role="progressbar" aria-valuenow="" aria-valuemin="0" aria-valuemax="<?php echo $Total; ?>" >
					<?php echo $Total; ?>
				</div>
			</div>
		</div>

	</div>
	@endif
</div>
<!-- JOB STATS -->