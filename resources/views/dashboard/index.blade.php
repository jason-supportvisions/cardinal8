<?php
$Title				=  " | Dashboard";
$Select2			= 1;
$Select2beta		= 0;
$DataTables			= 0;
$jQuery_UI			= 0;
$jQuery_Validate	= 0;

$jobPrint			= '';
$jobPrint			= 0;
$jobPricing			= 0; 

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');
?>
<!DOCTYPE html>
<html lang="en">
	@include('layouts.header')
	<body>
		<div class="container">
			@include('layouts.navbar')
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					@include('dashboard.dashjobs')
				</div>
			</div>
			<div class="row">	
				<div class="col-md-3">
					@include('dashboard.dash_stats')
				</div>
				<div class="col-md-3">
					@include('dashboard.dash_news')
				</div>
				<div class="col-md-6">
					@include('dashboard.dash_gallery')
				</div>				
			</div>
		</div>
		<div class="container">
			@include('layouts.footer')
		</div> 
		@include('layouts.loadjs')			
	</body>
</html>