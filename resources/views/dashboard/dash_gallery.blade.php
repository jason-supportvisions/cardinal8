@php
$randGallery = $jobs[5];
$public_path = public_path();
@endphp

<?php
// Turn off all error reporting
error_reporting(0);
//counter
$echoJustOne = 0;

foreach ($randGallery as $row) {

	$ImgGallery = $row->Original_Image;
	$GalleryImg = str_replace(".", "_800.", $row->Original_Image);
	$GalleryTitle = $row->Title;

	$thumbnailSize = 320;
	//if image exists 
	if (null !== getimagesize("/images/gallery/" . $GalleryImg)) {

		list($width, $height, $type, $attr) = getimagesize("$public_path/images/gallery/$GalleryImg");
		if ($height > $thumbnailSize) {
			$conPercent = ($thumbnailSize / $height);
			$height = $thumbnailSize;
			$width = ($width * $conPercent);
		}

		$width = round($width, 0);
		$height = round($height, 0);
		if ($echoJustOne <= 0) {
?>
			<!-- Start GALLERY IMAGE -->
			<div class="panel panel-UX">
				<div class="panel-body text-center center-block" style="margin-bottom: -15px;">
					<a href="/gallery/show">
						<div class="image-box">
							<img class="img-responsive gallery-title" src="/images/dashboard/gallery.png">
							<img class="img-responsive" style="height: {{ $height}}px; width: {{ $width}}px" src="/images/gallery/<?php echo $GalleryImg; ?>"><?php echo $GalleryTitle; ?>
						</div>
					</a>
				</div>
			</div>
			<!-- End Row -->
<?php
			$echoJustOne = 1;
		}
	}
}
?>