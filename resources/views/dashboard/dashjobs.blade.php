<!-- Start Jobs -->
<div class="panel panel-UX">
	<div class="panel-body">
		<ul class="nav nav-tabs hidden-xs hidden-sm">
			<li class="active"><a data-toggle="pill" href="#Recent">Recent Job Activity</a></li>
			<li><a data-toggle="pill" href="#Deposits">Deposits/Balances</a></li>
			<li><a data-toggle="pill" href="#Shipping">Shipping Next</a></li>
		</ul>
		<ul class="nav nav-tabs hidden-md hidden-lg">
			<li role="presentation" class="dropdown active">
				<a href="#" id="myTabDrop1" class="dropdown-toggle" data-toggle="dropdown" aria-controls="myTabDrop1-contents" aria-expanded="true">Dropdown <span class="caret"></span></a>
				<ul class="dropdown-menu" aria-labelledby="myTabDrop1" id="myTabDrop1-contents">
					<li class="active"><a data-toggle="tab" aria-controls="dropdown1" aria-expanded="false" href="#Recent">Recent Job Activity</a></li>
					<li><a data-toggle="tab" aria-controls="dropdown2" aria-expanded="false" href="#Deposits">Deposits/Balances</a></li>
					<li><a data-toggle="tab" aria-controls="dropdown3" aria-expanded="false" href="#Shipping">Shipping Next</a></li>
				</ul>
			</li>
		</ul>
		<div class="tab-content">
			<div id="Recent" class="tab-pane fade in active">
				@php $jobsAct = $jobs[0]; @endphp
				<?php
				if (!count($jobsAct)) {
					$data4[] = 0; ?>
					<div class="col-xs-12 hidden-xs hidden-sm"><h5>No Jobs Found<br>&nbsp;</h5></div>
					<?php while(count($data4) <= 5) { $data4[] = array(); ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } 
				}else{ ?>
					@foreach ($jobsAct as $row)
					<div class="col-xs-12 hidden-xs hidden-sm">
						<div class="col-xs-4">
							@if($row->Stage == '1' )
							<h5>
								&nbsp;&nbsp;&nbsp;&nbsp;<strong><a href="{{ route('quote_orders.pdf',['jobID'=>$row->JobID]) }}" target="_blank">{{ $row->JobID }}</strong></a>: <strong>{{ $row->Reference }}</strong><br>
								&nbsp;&nbsp;&nbsp;&nbsp;<strong></strong> {{ $row->FirstName }}&nbsp; {{ $row->LastName }}</h5>
							@else
							<h5>&nbsp;&nbsp;&nbsp;&nbsp;<strong>{{ $row->JobID }}</strong>: <strong>{{ $row->Reference }}</strong><br>
								&nbsp;&nbsp;&nbsp;&nbsp;<strong></strong> {{ $row->FirstName }}&nbsp; {{ $row->LastName }}</h5>
							@endif
						</div>
						<div class="col-xs-2">
							<h5><small>Type: </small><br><strong>{{ $row->TypeName }}</strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>Updated: </small><br><strong>{{ $row->Date }}</strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>Estimated Leadtime: </small><br><strong>{{ $row->EstLeadtime }}</strong></h5>
						</div>
						<div class="col-xs-2 text-center">
							@if($row->Stage == '1' )
							<h4><a class="btn btn-xs btn-info" href="/jobs" style="width:100px;"><strong>Quote</strong></a></h4>
							@endif
							@if($row->Stage == '2' )
							<h4><a class="btn btn-xs btn-warning" href="{{route('job-stage',['jobStage'=>2])}}" style="width:100px;"><strong>Confirmed</strong></a></h4>
							@endif
							@if($row->Stage == '3' )
							<h4><a class="btn btn-xs btn-danger" href="{{route('job-stage',['jobStage'=>3])}}" style="width:100px;"><strong>Under Review</strong></a></h4>
							@endif
							@if($row->Stage == '4' )
							<h4><a class="btn btn-xs btn-success" href="{{route('job-stage',['jobStage'=>4])}}" style="width:100px;"><strong>Accepted</strong></a></h4>
							@endif
							@if($row->Stage == '5' )
							<h4><a class="btn btn-xs btn-default" href="{{route('job-stage',['jobStage'=>5])}}" style="width:100px;"><strong>Shipping</strong></a></h4>
							@endif
							@if($row->Stage == '6' )
							<h4><a class="btn btn-xs btn-primary" href="{{route('job-stage',['jobStage'=>6])}}" style="width:100px;"><strong>Completed</strong></a></h4>
							@endif
							@if( $row -> Stage == '7' )
							<h4><a class="btn btn-xs btn-primary" href="jobs/cancel" style="width:100px;"><strong>Cancelled</strong></a></h4>
							@endif
						</div>
					</div>
					@endforeach
					<?php
					$rest = count($jobsAct);
					while($rest <= 5) { $rest ++; ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } 
				 	}?>
			</div>

			<div id="Deposits" class="tab-pane fade">
				@php $jobsDB = $jobs[1]; @endphp
				<?php 
				if (!count($jobsDB)) {
					$data3[] = 0; ?>
					<div class="col-xs-12 hidden-xs hidden-sm"><h5>No Jobs Found<br>&nbsp;</h5></div>
					<?php while(count($data3) <= 5) { $data3[] = array(); ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } ?>
				<?php } else {?>
					@foreach($jobsDB as $row)
					<div class="col-xs-12 hidden-xs hidden-sm">
						<div class="col-xs-4">
							<h5>&nbsp;&nbsp;&nbsp;&nbsp;Job: <strong> {{ $row->JobID }} </strong><br>Name: <strong> {{ $row->Reference }} </strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>List: </small><br><strong>$<?php echo number_format($row->List, 2, ".", ","); ?></strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>Net: </small><br><strong>$<?php echo number_format($row->Net, 2); ?> </strong></h5>
						</div>
						@if(empty($row -> Deposit))
						<div class="col-xs-2 bg-info">
							<h5><small>Deposit Due: </small><br><strong>$<?php echo number_format($row->Net / 2, 2); ?></strong></h5>
						</div>
						@elseif( $row -> Balance != $row-> Net )
						<div class="col-xs-2 bg-warning">
							<h5><small>Balance Due: </small><br><strong>$<?php echo number_format($row->Net - $row->Deposit, 2); ?></strong></h5>
						</div>
						@else
						<div class="col-xs-2 bg-success">
							<h5><small>&nbsp;</small><br><strong>Paid in Full</strong><br></h5>
						</div>
						@endif
						<div class="col-xs-2 text-center">
							@if( $row -> Stage == '1' )
							<h4><a class="btn btn-xs btn-info" href="" style="width:100px;"><strong>Quote</strong></a></h4>
							@endif
							@if( $row -> Stage == '2' )
							<h4><a class="btn btn-xs btn-warning" href="" style="width:100px;"><strong>Confirmed</strong></a></h4>
							@endif
							@if( $row -> Stage == '3' )
							<h4><a class="btn btn-xs btn-danger" href="" style="width:100px;"><strong>Under Review</strong></a></h4>
							@endif
							@if( $row -> Stage == '4' )
							<h4><a class="btn btn-xs btn-success" href="" style="width:100px;"><strong>Accepted</strong></a></h4>
							@endif
							@if( $row -> Stage == '5' )
							<h4><a class="btn btn-xs btn-default" href="" style="width:100px;"><strong>Shipping</strong></a></h4>
							@endif
							@if( $row -> Stage == '6' )
							<h4><a class="btn btn-xs btn-primary" href="" style="width:100px;"><strong>Completed</strong></a></h4>
							@endif
							@if( $row -> Stage == '7' )
							<h4><a class="btn btn-xs btn-primary" href="../quotes_orders/?JobStage=6" style="width:100px;"><strong>Cancelled</strong></a></h4>
							@endif
						</div>
					</div>
					@endforeach
					<?php
					$rest = count($jobsDB);
					while($rest <= 5) { $rest ++; ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } 
				}?>
			</div>

			<div id="Shipping" class="tab-pane fade">
				@php $jobsShip = $jobs[2]; @endphp
				<?php 
				if (!count($jobsShip)) {
					$data2[] = 0; ?>
					<div class="col-xs-12 hidden-xs hidden-sm"><h5>No Jobs Found<br>&nbsp;</h5></div>
					<?php while(count($data2) <= 5) { $data2[] = array(); ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } ?>
				<?php } else {?>
					@foreach($jobsShip as $row)
					<?php 
					// $format = 'd-m-Y';
					// $date = DateTime::createFromFormat($format, '22009-02-15 15:16:17');
					// echo "<br>Format: $format; " . $date->format('Y-m-d') . "\n";
					// echo "<br>ROW: " . $row->EstOutProduction;
					// $formatEstTime = DateTime::createFromFormat($format, $row->EstOutProduction);
					// $formatEstTime = strtotime($row->EstOutProduction);
						// $formatEstTime = date($row->EstOutProduction, 'm-d-Y');
					// echo "<br>DateTime(): " . $formatEstTime;

						// settype($formatEstTime, 'int');
						// $formatEstTime = date(2020-7-7, 'm-d-Y');
						// echo DateTime::createFromFormat('H\h i\m s\s','23h 15m 03s')->format('m-d-Y');
						$dateObj = new DateTime($row->EstOutProduction);
						$estOutProd = $dateObj->format('m-d-Y');
					?>
					<div class="col-xs-12 hidden-xs hidden-sm">
						<div class="col-xs-4">
							<h5>
								&nbsp;&nbsp;&nbsp;&nbsp;Job: <strong>{{ $row->JobID }} </strong><br>
								Name: <strong>{{ $row->Reference }}</strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>&nbsp;&nbsp;&nbsp;&nbsp;Cabinet (Qty): </small><strong>{{ $row->CabinetCount }}</strong><br><small>Accessory (Qty): </small><strong>{{ $row->AccessoryCount }}</strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>Est. Volume (cbft): </small><strong><?php echo number_format($row->Volume,0); ?></strong><br><small>&nbsp;&nbsp;Est. Weight (lbs): </small><strong><?php echo number_format($row->Weight,0); ?></strong></h5>
						</div>
						<div class="col-xs-2">
							<h5><small>Estimated Out of Production: </small><br><strong>{{ $estOutProd }}</strong></h5>
						</div>
						<div class="col-xs-2 text-center">
							@if( $row -> Stage == '1' )
							<h4><a class="btn btn-xs btn-info" href="../quotes_orders/?JobStage=1" style="width:100px;"><strong>Quote</strong></a></h4>
							@endif
							@if( $row -> Stage == '2' )
							<h4><a class="btn btn-xs btn-warning" href="../quotes_orders/?JobStage=2" style="width:100px;"><strong>Confirmed</strong></a></h4>
							@endif
							@if( $row -> Stage == '3' )
							<h4><a class="btn btn-xs btn-danger" href="../quotes_orders/?JobStage=3" style="width:100px;"><strong>Under Review</strong></a></h4>
							@endif
							@if( $row -> Stage == '4' )
							<h4><a class="btn btn-xs btn-success" href="../quotes_orders/?JobStage=4" style="width:100px;"><strong>Accepted</strong></a></h4>
							@endif
							@if( $row -> Stage == '5' )
							<h4><a class="btn btn-xs btn-default" href="../quotes_orders/?JobStage=5" style="width:100px;"><strong>Shipping</strong></a></h4>
							@endif
							@if( $row -> Stage == '6' )
							<h4><a class="btn btn-xs btn-primary" href="../quotes_orders/?JobStage=6" style="width:100px;"><strong>Completed</strong></a></h4>
							@endif
							@if( $row -> Stage == '7' )
							<h4><a class="btn btn-xs btn-primary" href="../quotes_orders/?JobStage=6" style="width:100px;"><strong>Cancelled</strong></a></h4>
							@endif
						</div>
					</div>
					@endforeach
					<?php
					$rest = count($jobsShip);
					while($rest <= 5) { $rest ++; ?>
						<div class="col-xs-12 hidden-xs hidden-sm"><h5>&nbsp;<br>&nbsp;</h5></div>
					<?php } 
				}?>
			</div>
			<div class="text-right">
				<a href="/quote_orders" class="btn btn-outline-success btn-xs" style="width:50px; font-weight:bold;"><i style="width:14px;" class="glyphicon glyphicon-plus"></i> New</a>
				<a href="/jobs" class="btn btn-outline-primary btn-xs" style="width:50px; font-weight:bold;"><i style="width:14px;" class="glyphicon glyphicon-th-list"></i> All</a>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			</div>
		</div>
	</div>
</div>