@php
    $Title          =  " | News';
    $Select2        = 1;
    $Select2beta    = 0;
    $DataTables     = 0;
    $jQuery_UI      = 0;
    $jQuery_Validate= 0;
    $xCRUD_16       = 0;
    $jobPrint       = 0;
    $jobPricing     = 0;

    $SesUser        = session('userid');
    $SesAcct        = session('accountid');
    $SesType        = session('accounttype');
@endphp

@extends('templates.theme')

@section('content')
<div class="row">
	<div class="col-sm-12">
		<div class="panel panel-UX">
			<div class="panel-heading">News</div>
			<div class="panel-body">
				@if( !count($news)) 
					 <p> No records</p>
				@else 
					<div class="media">
                    <?php
                    $allORmore  = count($news);
                    $num        = 0;
					foreach ($news as $row){
                        $num++;
						$ID = $row->ID;
						$Thumbnail = $row->Thumbnail;
						$File = $row->File;
						$Type = $row->Type;
						$Date = $row->Date;
						$Subject = $row->Subject;
						if($allORmore == 1){							
							$Content = $row->Content;
						} else {
							$Content = substr($row->Content."...", 0, 400);
						}
                        ?>						
						<div class="row">
							<div class="col-xs-2">
								<div class="media-left">
								<a href="../downloads/{{$File}}" target="_blank"><img class="media-object thumbnail" src="/images/news/{{$Thumbnail}}" alt="..." style="width:155px;height:200px;padding-bottom: 0px;"></a>
								<div class="text-center">
								<a href="../downloads/{{$File}}" target="_blank">Download PDF</a>
								</div>
								</div>
							</div>
							<div class="col-xs-10">
								<div class="media-body">
								<div class="media-heading">
								<h4><strong>{{$Type}}: </strong><small>{{$Date}}</small><br>{{$Subject}}</h4>
								<p>@php echo html_entity_decode($Content); @endphp</p>		
								</div>
								</div>
								 @if($allORmore == 1)
								    <a href="/news/">Return to News</a>
								@else  
                                    <a class="" href="{{ route('readmore',['id'=>$ID])}}">Read More...</a>
								@endif 
							</div>
                        </div>
                        
                        @if($allORmore != 1)
                            @if($allORmore != $num)
                            <hr>  
                            @endif                     
                        @endif 
                    <?php }?>
				@endif
				
			</div>
			</div>
		</div>
	</div>
</div>

@endsection


@section('page-script')

<script>
    $(document).ready(function() {
    
    });

</script>

@endsection

