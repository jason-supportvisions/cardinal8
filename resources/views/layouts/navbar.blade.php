<nav class="navbar navbar-default">
    <div class="container-fluid" style="padding-right:0px;">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only text-darkblue">Toggle navigation</span>
                <span class="icon-bar text-darkblue"></span>
                <span class="icon-bar text-darkblue"></span>
                <span class="icon-bar text-darkblue"></span>
            </button>
            <a class="navbar-brand" style="padding:0px; padding-top:4px;" href="/"> 
                <img src="/images/logos/CARDINAL.png" >
            </a>
        </div> 
        <div id="navbar" class="navbar-collapse collapse navbar-right">
            <ul class="nav navbar-nav">
                <li><a href="/" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-dashboard"></i> <span>Dashboard</span></a></li>
                <li><a href="/jobs" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-folder-open"></i> <span>Jobs</span></a></li>
                <li>
                   <a data-toggle="dropdown" class="dropdown-toggle text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-wrench"></i> Tools<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/tools/abbreviations/"><i class="glyphicon glyphicon-flash"></i> <span>Abbreviation</span></a></li>
                        <li><a href="/gallery/doors" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-picture"></i> <span>Doors</span></a></li>
                        <li><a href="/download"><i style="width:14px;" class="glyphicon glyphicon-download-alt"></i> <span>Downloads</span></a></li>
                        <li><a href="/tools/faq"><i class="glyphicon glyphicon-question-sign"></i> <span>FAQ</span></a></li>
                        <li><a href="/gallery/show" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-camera"></i> <span>Gallery</span></a> </li>
                        <li><a href="/tools/glossary"><i class="glyphicon glyphicon-book"></i> <span>Glossary</span></a></li>
                        <li><a href="/tools/news" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-comment"></i> <span>News</span></a></li>
                        <li><a href="/catalog" class="text-darkblue"><i style="width:14px;" class="glyphicon glyphicon-book"></i> <span>Catalog</span></a></li>
                    </ul>
                </li>
                <li><a href="/quote_orders"><i style="width:14px;" class="glyphicon glyphicon-plus"></i> New Job</a></li>   
                <?php if($SesType == -1): ?>
                <li class="">
                    <a data-toggle="dropdown" class="dropdown-toggle" aria-expanded="false"><span class="text-red">Internal <span class="caret"></span></span></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="/crm/orders/accepted">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>CRM</span>
                            </a>
                        </li>                        
                        <li>
                            <a href="/administration">
                                <i class="glyphicon glyphicon-lock"></i>
                                <span>Administration</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ config('app.manager_url') }}"  target="_blank">
                                <i class="glyphicon glyphicon-barcode"></i>
                                <span>Manager</span>
                            </a>
                        </li>                        
                    </ul>
                </li>
                <?php endif ?>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <a data-toggle="dropdown" class="dropdown-toggle text-darkblue">
                    {{ session('firstname') }} {{ session('lastname') }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                        <?php if($SesType == -1): ?>
                        <li>
                            <a href="/admin/account">
                                <i class="glyphicon glyphicon glyphicon-cog"></i>
                                <span>Account Settings</span>
                            </a>
                        </li>
                        <?php endif ?>
                        <li>
                            <a href="{{ route('all-users')}}">
                                <i class="glyphicon glyphicon glyphicon-cog"></i>
                                <span>User Settings</span>
                            </a>
                        </li>
                        <li role="separator" class="divider"></li>
                        <li>
                            <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                <i class="glyphicon glyphicon-log-out"></i>
                                <span>Log out</span>
                            </a>

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                @csrf
                            </form>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>  
</nav>

