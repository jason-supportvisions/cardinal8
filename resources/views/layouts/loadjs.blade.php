<?php 
$pageid = isset($pageid)? $pageid : '';
if($pageid != 'GeneralNotes'){ ?>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.js" crossorigin="anonymous" SameSite="None"></script> 
<?php }else{?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.15.1/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.7.14/js/bootstrap-datetimepicker.min.js"></script>
<?php }?>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"  crossorigin="anonymous"></script>
<?php if($Select2 == 1){ ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
	$('select').select2({
	minimumResultsForSearch: Infinity,
	});
});
</script>
<?php };
if($Select2beta == 1){ ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script> 
<script type="text/javascript">
$(document).ready(function(){
	$('select').select2({
	minimumResultsForSearch: Infinity,
	});
});
</script>
<?php }; 
if($DataTables == 1){ ?>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script> 
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mark.js/7.0.0/jquery.mark.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.bootstrap.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.2.1/js/buttons.colVis.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/dataTables.responsive.min.js"></script> 
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.0/js/responsive.bootstrap.min.js"></script>
<?php }; ?>
<script src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
<?php if($jQuery_UI == 1){ ?>
<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<?php }; ?>
<?php if($jQuery_Validate == 1){ ?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.15.0/jquery.validate.min.js"></script>
<?php }; ?>

