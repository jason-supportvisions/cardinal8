<?php 
date_default_timezone_set('america/new_york'); 
?>
<footer class="footer">
	<div class="visible-mx dash_footer">
		<p style="font-size: 12px;">&copy; Cory Manufacturing, Inc. <?php echo date("Y"); ?> all rights reserved </p>
	</div>
</footer>