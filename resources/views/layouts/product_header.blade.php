<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="user-scalable=yes, initial-scale=1, minimum-scale=0.2, maximum-scale=2, width=device-width" name="viewport" id="viewport">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="PSB">

    <title>{{config('app.name')}} <?php echo $Title; ?></title>

    <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png?v=3">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png?v=3">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png?v=3">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png?v=3">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png?v=3">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png?v=3">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png?v=3">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png?v=3">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png?v=3">
    <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png?v=3">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png?v=3">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png?v=3">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png?v=3">
    <link rel="manifest" href="/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png?v=3">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}" />             
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Select2 HTML input -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" />
    <link rel="stylesheet" type="text/css" href="https://select2.github.io/select2-bootstrap-theme/css/select2-bootstrap.css">

    <!-- CUSTOM CSS -->
    <link rel="stylesheet" type="text/css" href="/css/custom.css?n=1" media="screen">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" SameSite="None"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>

</head>