@extends('templates.default')

@section('content')



{{ Form::open() }}

<div class="row">
    <div class="col-sm-12">
        <h2 class="page-title">
            Finishes

            @if(Auth::user()->hasAccountType('master-administrator'))
                {{ Form::submit('Save Changes', ['class'=>'btn btn-primary']) }}
            @endif
        </h2>
    </div>
</div>
<div class="row">
    <div class="col-md-6">

        <h3>Styles</h3>

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Abbreviation</th>
                </tr>
            </thead>
            <tbody>
                @foreach($style_finishes as $style_finish)
                <tr>
                    <td>{{ $style_finish->Name }}</td>
                    <td>
                        @if(Auth::user()->hasAccountType('master-administrator'))
                        <input name="style_finishes[{{ $style_finish->ID }}][Abbreviation]" 
                               value="{{ $style_finish->Abbreviation }}">
                        @else
                            {{ $style_finish->Abbreviation }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
    <div class="col-md-6">

        <h3>Groups</h3>

        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Abbreviation</th>
                    <th>Major Category</th>
                </tr>
            </thead>
            <tbody>
                @foreach($style_groups as $style_group)
                <tr>
                    <td>{{ $style_group->Name }}</td>
                    <td>
                        @if(Auth::user()->hasAccountType('master-administrator'))
                        <input name="style_groups[{{ $style_group->ID }}][Abbreviation]" 
                               value="{{ $style_group->Abbreviation }}">
                        @else
                            {{ $style_group->Abbreviation }}
                        @endif
                    </td>
                    <td>
                        @if(Auth::user()->hasAccountType('master-administrator'))
                        {{ Form::select("style_groups[{$style_group->ID}][MajorCategory]", $style_group_major_categories, $style_group->MajorCategory) }}
                        @else
                            {{ $style_group->MajorCategory }}
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

    </div>
</div>

{{ Form::close() }}


@stop

@section('scripts')

<script>

    $('form').submit(function(e){

        var ans = confirm("Save all changes?");

        if(!ans){e.preventDefault();}

    });

</script>

@stop
