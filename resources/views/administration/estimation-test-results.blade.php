<div class="col-sm-4">

    <h3>Blocking Effect Test Results</h3>

    <div class="form-group">
        <label>Formula:</label>
        <input class="form-control" readonly="readonly" type="text" value="{{ $total_blocking_equation }}">
    </div>

    <div class="form-group">
        <label>Value:</label>
        {{ Form::text('weeks_to_ship', $total_blocking_weeks . " weeks", ['class'=>'form-control', 'readonly'=>'readonly']) }}
    </div>

</div>

<div class="col-sm-4">

    <h3>Lead Time Test Results</h3>

    <div class="form-group">
        <label>Formula:</label>
        <input class="form-control" readonly="readonly" type="text" value="{{ $total_total_equation }}">
    </div>

    <div class="form-group">
        <label>Value:</label>
        {{ Form::text('weeks_to_ship', $total_total_weeks . " weeks", ['class'=>'form-control', 'readonly'=>'readonly']) }}
    </div>

</div>