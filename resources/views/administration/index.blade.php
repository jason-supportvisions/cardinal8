<?php 

// Page Settings (1 = True | 0 = False)
$Title = "Administration | Links";
$Select2 = 0;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

?>
@extends('templates.theme')

@section('content')
	<!--<a class="btn btn-primary pull-right" href="#">Button Name Here...</a>-->
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-admin">
					<h3 class="panel-title"><strong><?php echo $Title ?></strong></h3>
				</div>
				<div class="panel-body">
					

					<div class="row">	
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading"><h3 class="panel-title">Main Pages</h3></div>
							<div class="panel-body">
							<ul class="nav nav-list">
					   <li class="disabled h4"><a href="#"><i class="glyphicon glyphicon-dashboard">&nbsp;</i>Dashboard</a></li>
					   <li class="disabled h4"><a href="#"><i class="glyphicon glyphicon-folder-open">&nbsp;</i>Jobs</a></li>
					   <li class="h4"><a href="/download"><i class="glyphicon glyphicon-comment">&nbsp;</i>News/Downloads</a></li>
					   <li class="h4"><a href="/gallery/show" target="_blank"><i class="glyphicon glyphicon-camera">&nbsp;</i>Gallery</a></li>
					   <li class="h4"><a href="/gallery/doors"><i class="glyphicon glyphicon-picture">&nbsp;</i>Doors</a></li>
					   <li class="disabled h4"><a href="#"><i class="glyphicon glyphicon-wrench">&nbsp;</i>New Job</a></li>
					   </ul>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class="panel panel-default">
							<div class="panel-heading"><h3 class="panel-title">User Dropdown</h3></div>
							<div class="panel-body">
							
							<ul class="nav nav-list">
					   <li class="disabled h4"><a href="#"><i class="glyphicon glyphicon-cog">&nbsp;</i>Settings</a></li>
					   <li class="h4"><a href="/tools/abbreviations"><i class="glyphicon glyphicon-flash">&nbsp;</i>Abbreviation</a></li>
					   <li class="h4"><a href="/tools/faq"><i class="glyphicon glyphicon-question-sign">&nbsp;</i>FAQ</a></li>
					   <li class="h4"><a href="/tools/glossary"><i class="glyphicon glyphicon-book">&nbsp;</i>Glossary</a></li>
					   <li class="h4"><a href="/administration/finishes"><i class="glyphicon glyphicon-tint">&nbsp;</i>Finishes</a></li>
					    <li class="h4">
					    	<a href="/administration/estimation-tuning">
					    		<i class="glyphicon glyphicon-calendar">&nbsp;</i>Estimation Tuning
					    	</a>
					    </li>
					  
					   </ul>
					
							</div>
						</div>
					</div>
	
				
		
					<div class="col-md-3">
					<div class="panel panel-default">
							<div class="panel-heading"><h3 class="panel-title">TBD</h3></div>
							<div class="panel-body">
					   <ul class="nav nav-list">
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-tent">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-piggy-bank">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-sunglasses">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-fire">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-road">&nbsp;</i>TBD</a></li>
					  
					   </ul>
							</div>
						</div>
					</div>
					
					<div class="col-md-3">
					<div class="panel panel-default">
							<div class="panel-heading"><h3 class="panel-title">TBD</h3></div>
							<div class="panel-body">
					    <ul class="nav nav-list">
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-ice-lolly-tasted">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-education">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-cutlery">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-glass">&nbsp;</i>TBD</a></li>
					   <li class="disabled h4"><a href="../#/admin.php"><i class="glyphicon glyphicon-scissors">&nbsp;</i>TBD</a></li>
					  
					   </ul>
							</div>
						</div>
						
					</div>
					
				</div>
				
					
			</div>
		</div>
	</div>
@stop


	



