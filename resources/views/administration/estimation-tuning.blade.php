@extends('templates.default')

@section('content')

<style>
    #explanation-list{
        list-style-position: inside;
        padding-left:0
    }

    #explanation-list li{
        margin: 12px 0px;
    }

    input[disabled]{
        background-color: #EEE;
        border: 1px solid #BBB;
        border-radius: 3px;
        margin-left: 10px;
        margin-top: -5px;
        padding: 2px;
        width: 65px;
        text-align: right;
    }

    div.parameters div.form-group{
        margin-top: 35px;
    }
</style>

{{ Form::open(['id'=>'parameter-form']) }}
    <div class="row">
        <div class="col-sm-12">
            <h2 class="page-title">
                Estimation Tuning {{ Form::submit('Save Changes', ['class'=>'btn btn-primary']) }}
            </h2>
        </div>
    </div>


    <div class="row parameters">
        <div class="col-sm-4">
            <h3>Throughput</h3>
            <div class="form-group">
                <label>Boxes Per Week:</label>
                <input disabled class="slider-display pull-right">
                <input name="boxes_per_week" type="range" min="0" max="150" step="1" value="{{ $estimation_parameters->boxes_per_week }}"/>
            </div>

            <div class="form-group">
                <label>Accessories Per Box Equivalent:</label>
                <input disabled class="slider-display pull-right">
                <input name="accessories_per_box_equivalent" type="range" min="0" max="10" step="1" value="{{ $estimation_parameters->accessories_per_box_equivalent }}"/>
            </div>

            <div class="form-group">
                <label>List Value Per Week:</label>
                <input disabled class="slider-display pull-right">
                <input name="list_value_per_week" class="money" type="range" min="0" max="120000" step="1000" value="{{ $estimation_parameters->list_value_per_week }}"/>
            </div>

        </div>
        <div class="col-sm-4">

            <h3>Blocking Effect</h3>
            <div class="form-group">
                <label>Blocking Box Count Significance:</label>
                <input disabled class="slider-display pull-right">
                <input name="blocking_boxes_weight" class="percent" type="range" min="0" max="1" step="0.01" value="{{ $estimation_parameters->blocking_boxes_weight }}"/>
            </div>

            <div class="form-group">
                <label>Blocking List Value Significance</label>
                <input disabled class="slider-display pull-right">
                <input name="blocking_list_value_weight" class="percent" type="range" min="0" max="1" step="0.01" value="{{ $estimation_parameters->blocking_list_value_weight }}"/>
            </div>

            <div class="form-group">
                <label>Blocking Base for Laminate (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_blocking_weeks_for_laminate" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_blocking_weeks_for_laminate }}"/>
            </div>

            <div class="form-group">
                <label>Blocking Base For Veneer (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_blocking_weeks_for_veneer" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_blocking_weeks_for_veneer }}"/>
            </div>

            <div class="form-group">
                <label>Blocking Base For Paint (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_blocking_weeks_for_paint" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_blocking_weeks_for_paint }}"/>
            </div>

        </div>
        <div class="col-sm-4">

            <h3>Lead Time</h3>

             <div class="form-group">
                <label>Lead Time Box Count Significance</label>
                <input disabled class="slider-display pull-right">
                <input name="total_boxes_weight" class="percent" type="range" min="0" max="1" step="0.01" value="{{ $estimation_parameters->total_boxes_weight }}"/>
            </div>

            <div class="form-group">
                <label>Lead Time List Value Significance</label>
                <input disabled class="slider-display pull-right">
                <input name="total_list_value_weight" class="percent" type="range" min="0" max="1" step="0.01" value="{{ $estimation_parameters->total_list_value_weight }}"/>
            </div>

            <div class="form-group">
                <label>Lead Time Base For Laminate (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_total_weeks_for_laminate" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_total_weeks_for_laminate }}"/>
            </div>

            <div class="form-group">
                <label>Lead Time Base For Veneer (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_total_weeks_for_veneer" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_total_weeks_for_veneer }}"/>
            </div>

            <div class="form-group">
                <label>Lead Time Base For Paint (Weeks):</label>
                <input disabled class="slider-display pull-right">
                <input name="additional_total_weeks_for_paint" type="range" min="0" max="20" step="0.1" value="{{ $estimation_parameters->additional_total_weeks_for_paint }}"/>
            </div>
        </div>
    </div>

    <hr>

{{ Form::close() }}

<form id="test-form">
    <div class="row">

        <div class="col-sm-4">

            <h3>Test Data</h3>

            <div class="form-group">
                <label>Box Count</label>
                {{ Form::text('CabinetCount', 8, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                <label>Accessory Count</label>
                {{ Form::text('AccessoryCount', 4, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                <label>List Price</label>
                {{ Form::text('List', 10000, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                <label>Finish</label>
                {{ Form::select('finish_major_category', ['laminate'=>'Laminate', 'veneer'=>'Veneer', 'paint'=>'Paint'], 'laminiate', ['class'=>'form-control']) }}
            </div>
        </div>

        <span id="test-results">
        </span>
    </div>
</form>


@stop

@section('scripts')

<script>

    $('#parameter-form').submit(function(e){

        var ans = confirm("Save all changes?");

        if(!ans){e.preventDefault();}

    });

    $("#test-form :input").change(updateTestResults);

    updateTestResults();

    function updateTestResults(){
        var parameter_data = $('#parameter-form').serialize();
        var test_data = $('#test-form').serialize();
        var data = parameter_data + '&' + test_data;
        $('#test-results').load('/administration/estimation-test-results', data);
    }

    function updateRangeDisplay($input){

        var $container = $input.closest('div');
        var $output_node = $container.find('input.slider-display');
        var output;

        if($input.hasClass("money")){
            output = "$" + $input.val().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }else if($input.hasClass("percent")){
            output = Math.floor(100 * $input.val()/1) + '%';
        }else{
            output = $input.val();
        }

        $output_node.val(output);
    }

    $("input[type=range]").each(function(){

        updateRangeDisplay($(this));

        $(this).on('input', function(){
            updateRangeDisplay($(this));
        });

        $(this).on('change', updateTestResults);
    });

</script>

@stop
