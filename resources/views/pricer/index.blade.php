<?php 

// Page Settings (1 = True | 0 = False)
$Title = "Price Checker";
$Select2 = 0;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');

if(isset($product_item)){
    $item = $product_item;
    $itemType = "Product";
}

if(isset($accessory_item)){
    $item = $accessory_item;
    $itemType = "Accessory";
}


?>
@extends('templates.theme')
@section('content')

<div class="row">
<div class="col-sm-12">
<div class="panel panel-default">
            
        <div class="panel-heading">
            <h3 class="panel-title"><strong><?php echo $Title ?></strong></h3>
        </div>

        <div class="panel-body bg-info">
            
            <!-- command -->
            <div class="col-md-12">
                <div><mark> get item info  ... </mark><br/><br/></div>
                <div><mark> searching for <b>['{{$item->Code}}']</b>... ... ... ... </mark></div>
                <br />
                <br />
            </div>

            <!-- left -->
            <div class="col-md-6 bg-primary">
                <div class="row"><div class="col-md-12"><h2>{{ $item->Code }}</h2> <br /></div></div>
                <div class="row"><div class="col-md-12"><B>ID:</B> {{ $item->ID }}</div></div>
                <div class="row"><div class="col-md-12"><B>Type:</B> {{ $itemType }}</div></div> 
                <div class="row"><div class="col-md-12"><B>Code:</B> {{ $item->Code }}</div></div>
                <div class="row"><div class="col-md-12"><B>Group:</B> {{ $item->GroupCode }}</div></div>
                <div class="row"><div class="col-md-12"><B>Desc:</B> {{ $item->Description }}</div></div>
                <div class="row"><div class="col-md-12"><B>w/h/d</B> {{ $item->Width }} / {{ $item->Height }} / {{ $item->Depth }}  </div></div>
                <div class="row"><div class="col-md-12"><B>Category:</B> {{ $item->Category }}</div></div>
                <div class="row"><div class="col-md-12"><B>ClassBuild:</B> {{ $item->ClassBuild }}</div></div>
                <div class="row"><div class="col-md-12"><B>Img:</B> {{ $item->Image_File }}</div></div>
                <div class="row"><div class="col-md-12">------- additional charges ------- </div></div>
                <div class="row"><div class="col-md-12"><B>Ignore Group Style Charge:</B> {{ $item->IgnoreGroupStyleCharge }}</div></div>
                <div class="row"><div class="col-md-12"><B>Style Charge:</B> {{ $item->addStyleCharge }}</div></div>
                <div class="row"><div class="col-md-12"><B>Door Opt Charge:</B> {{ $item->addDoorOptCharge }}</div></div>
                <div class="row"><div class="col-md-12"><B>Style Charge Type:</B> {{ $item->addStyleChargeType }}</div></div>
                <div class="row"><div class="col-md-12"><B>Door Type:</B> {{ $item->addDoorType }}</div></div>
                <div class="row"><div class="col-md-12"><B>Door Type 2:</B> {{ $item->addDoorType2 }}</div></div>
                <div class="row"><div class="col-md-12"><B>Door Qty:</B> {{ $item->addDoorQty }}</div></div>
                <div class="row"><div class="col-md-12"><B>Door Qty 2:</B> {{ $item->addDoorQty2 }}</div></div>
                <br><br><bR><br><br><bR>
            </div>

            <!-- right -->
            <div class="col-md-6 bg-warning">
                <div class="row"><div class="col-md-12"><h2><a href="">{{ $job->JobID }} Spec</a></h2> <br /> </div></div>

                <div class="row"><div class="col-md-12"><B>Cust:</B> {{ $specifications->customer_info->name  }} </div></div>
                <div class="row"><div class="col-md-12"><B>Brand:</B> {{ $specifications->brand->name  }} </div></div>
                <div class="row"><div class="col-md-12"><B>Group:</B> {{ $specifications->style_group->name  }} </div></div>
                <div class="row"><div class="col-md-12"><B>Door:</B> {{ $specifications->door->name }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;inside: {{ $specifications->door->inside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;center_panel: {{ $specifications->door->center_panel }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;outside: {{ $specifications->door->outside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;style_rail: {{ $specifications->door->style_rail }}<br /></div></div>

                <div class="row"><div class="col-md-12"><B>Drawer:</B> {{ $specifications->drawer->name }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;inside: {{ $specifications->drawer->inside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;center_panel: {{ $specifications->drawer->center_panel }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;outside: {{ $specifications->drawer->outside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;style_rail: {{ $specifications->drawer->style_rail }}<br /></div></div>

                <div class="row"><div class="col-md-12"><B>LG Drawer:</B> {{ $specifications->large_drawer->name }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;inside: {{ $specifications->large_drawer->inside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;center_panel: {{ $specifications->large_drawer->center_panel }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;outside: {{ $specifications->large_drawer->outside }}</div></div>
                <div class="row"><div class="col-md-12">&nbsp;style_rail: {{ $specifications->large_drawer->style_rail }}<br /></div></div>

                <div class="row"><div class="col-md-12"><B>Material:</B> {{ $specifications->style->material }}</div></div>
                <div class="row"><div class="col-md-12"><B>Color:</B> {{ $specifications->style->color }}</div></div>
                <div class="row"><div class="col-md-12"><B>Finish:</B> {{ $specifications->style->finish }}</div></div>
                <div class="row"><div class="col-md-12"><B>Edge:</B> {{ $specifications->style->edge }}</div></div>
                <div class="row"><div class="col-md-12"><B>DrawerBox:</B> {{ $specifications->drawer_box_name }}</div></div>
                <div class="row"><div class="col-md-12"><B>AdminNote:</B> {{ $specifications->admin_note }}</div></div>
            </div>    
        


            <!-- command -->
            <div class="col-md-12" >
                <br />
                <div ><mark> </mark></div><br />
                <div ><h2>JOBID :: {{ $job->JobID }} </h2></mark></div><br />
                @foreach($debug_out as $debug_line)
                    <div ><mark>&nbsp;&nbsp; {{!! $debug_line !!}}  </mark></div><br />
                @endforeach
                <br /><br />
            </div>
        
            <!-- left -->
            <div class="col-md-6">
                <div class="row"><div class="col-md-12"></div></div>
                <div class="row"><div class="col-md-12"><B></B> </div></div>

            </div>
            
            <!-- right -->
            <div class="col-md-6">
                <div class="row"><div class="col-md-12"> </div></div>
                <div class="row"><div class="col-md-12"><B></B> </div></div>
            </div>     

            {{-- {{ dd($accessory_item->accessory_group) }} --}}
            {{-- {{ dd($product_item->product_group) }} --}}

        </div>
        
</div>
</div>
</div>

@stop


	



