@extends('templates.default')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Downloads</div>           
            <div class="panel-body">
                <div class="content-body">
                    <div class="row">
                    <a href="{{route('download.create')}}"> <button type="button" class="btn btn-success add_btn"><i class="glyphicon glyphicon-plus-sign"></i>Add</button></a>
                    </div>
                    <table id="table" class="table table-striped table-responsive table-bordered dataTable no-footer"  role="grid" aria-describedby="table_info" cellspacing="0">
                        <thead class="account_view">
                            <tr>
                                <th class="text-left">Type</th>
                                <th class="text-left" >Subject</th>
                                <th class="text-left" >File</th>
                                <th class="text-left" > </th>
                            </tr>
                        @if(count($data))
                        <tbody>
                            @foreach($data as $row)                                
                            <tr>
                                <td class="text-left">{{$row->Type}}</td>
                                <td class="text-left">{{$row->Subject}}</td>
                                <td class="text-left"><a target="_blank" href="/downloads/<?php echo $row->File; ?>">{{$row->File}}</a></td>
                                <td class="text-center" style="width: 100px;padding:5px;">
                                    <span class="btn-group">
                                        <a class="btn btn-sm btn-info"  href="{{route('downloadinfo',['id'=>$row->ID])}}" ><i class="glyphicon glyphicon-search"></i></a>
                                        <a class="btn btn-sm btn-warning" href="{{route('download.edit',['id'=>$row->ID])}}"><i class="glyphicon glyphicon-edit"></i></a>
                                        <a class="btn btn-sm btn-danger" href="{{route('download.delete', ['id'=>$row->ID]) }}" onclick="return confirm('Do you really want remove this entry?') "><i class="glyphicon glyphicon-remove"></i></a>                                               
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('#table').DataTable({        
        dom: 'Bfrtip',
        buttons: [
            'print',
            'csv'
        ],   
        "columnDefs": [
            {
                "targets": [ 3 ],
                "orderable": false,
                "searchable": false
            }
        ],
        "pageLength": 20    
    });
      
});

</script>

@stop


@section('styles')
<style>    
    .add_btn{
        margin-left: 15px;
    }
    #table{
        border-top: 1px solid rgb(175, 169, 169);
        font-size: 14px;
    }
  
</style>
@stop


