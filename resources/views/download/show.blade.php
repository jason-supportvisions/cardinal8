@extends('templates.default')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Downloads</div>           
            <div class="panel-body">               
                <div class="user-view-div">
                    <div class="content-body">
                        <div class="row">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                            </div>
                        </div>
                        <table class="table form-horizontal user-view-table">
                            <tbody>
                                
                                @foreach ($news as $row)
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Type<td>
                                    <td class="col-sm-9">{{ $row->Type }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Subject<td>
                                    <td class="col-sm-9">{{$row->Subject}}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">File<td>
                                    <td class="col-sm-9"><a href="/downloads/<?php echo $row->File; ?>" target="_blank">{{$row->File}}</a><td>
                                </tr>                              
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
               
            </div>
        </div>
    </div>
</div>


@stop


@section('scripts')

<script>
    $('.add_cancel').click(function(){
        window.history.back();
    });

</script>

@stop


@section('styles')
<style>    
    .content-header{
        padding: 0px 15px;
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    .user-view-table{
        border-top: 1px solid rgb(175, 169, 169);
        margin-top: 20px;
    }
    

</style>
@stop


