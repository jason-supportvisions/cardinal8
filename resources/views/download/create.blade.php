@extends('templates.default')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Downloads</div>           
            <div class="panel-body">               
                <div class="user-view-div">
                    <div class="content-body">
                        <form method="post" action="/download/store" class="form-horizontal" id="download-form">
                            @csrf
                            <div class="row">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                                    <button type="submit" class="btn btn-success add_btn" ></i>Save</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="EditType" class="col-sm-3 control-label">Type</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="type" id="type" value="" required>                                                        
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="EditSubject" class="col-sm-3 control-label" >Subject</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" name="subject" id="subject" value="" required>                                                          
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="EditFile" class="col-sm-3 control-label">File</label>
                                <div class="col-sm-9">
                                    <input type="file" id="real-file" style="display: none;" name="loadfile" required />                                  
                                    <span id="custom-text">No file</span>                                
                                    <button type="button" id="custom-button" class="btn btn-primary btn-success"><i class="glyphicon glyphicon-circle-arrow-up"></i>Add file</button>                                                 
                                </div>
                            </div>                              
                        </form>              
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>
            
@stop

@section('scripts')
<script>
    $('.add_cancel').click(function(){
        window.history.back();
    });
    const realFileBtn = document.getElementById("real-file");
    const customBtn = document.getElementById("custom-button");
    const customTxt = document.getElementById("custom-text");

    customBtn.addEventListener("click", function() {
        realFileBtn.click();
        
    });

    realFileBtn.addEventListener("change", function() {
        if (realFileBtn.value) {
            customTxt.innerHTML = realFileBtn.value.match(
                /[\/\\]([\w\d\s\.\-\(\)]+)$/
            )[1];
        } else {
            customTxt.innerHTML = "No file";
        }
    });
</script>

@stop


@section('styles')
<style>    
    .content-header{
        padding: 0px 15px;
    }
    .bottom_line{
        float: left;
        border-bottom: 1px solid #ddd;
        width: 97%;
        margin-left: 15px;
        
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    .user-view-table{
        border-top: 1px solid rgb(175, 169, 169);
        margin-top: 20px;
    }
    .add_btn{
        margin-top: 0px;
    }
    .fileload{
        display: none;
    }

</style>
@stop


