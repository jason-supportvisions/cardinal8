@extends('templates.products')

@section('content')

<h3 class="page-title">
    Doors With Missing Images
</h3>
@foreach($doors_with_missing_images as $door)
    <table class="table table-bordered">
        <thead>
            <tr style="background-color: #EEE">
                <th>Inside Profile</th>
                <th>Center Panel</th>
                <th>Outside Profile</th>
                <th>Stile/Rail</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{ $door->inside_profile->slider_name }}<br>
                    <img src="{{ $door->inside_profile->image_url }}">
                </td>
                <td>
                    {{ $door->center_panel->slider_name }}<br>
                    <img src="{{ $door->center_panel->image_url }}">
                </td>
                <td>
                    {{ $door->outside_profile->slider_name }}<br>
                    <img src="{{ $door->outside_profile->image_url }}">
                </td>
                <td>
                    {{ $door->stile_rail->VendorCode }}<br>
                    <img src="{{ $door->stile_rail->image_url }}">
                </td>
            </tr>
            <tr>
                <td colspan="100%">
                    <b>Door Image: </b>
                    <?php
                        $doorPath = trim(urldecode($door->internal_image_path));
                        $doorPath = str_replace("public","",$doorPath);
                        $doorImage = "<img src='$doorPath' width='250'>"; 
                    ?>
                    {{ $doorImage }} 
                    {{ $doorPath }}

                </td>
            </tr>
        </tbody>
    </table>
    <br>
@endforeach

@stop


@section('scripts')


@stop