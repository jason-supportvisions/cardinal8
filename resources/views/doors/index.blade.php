@extends('templates.products')

@section('content')

<h3 class="page-title">
    Door Combinations
</h3>

<div class="row">
    <div class="col-sm-10">

        <table id="door-combinations-table" class="table table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Inside Profile Name</th>
                    <th>Brand</th>
                    <th>Image</th>
                    <th>Show in Door Builder</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
            @foreach($inside_profiles as $inside_profile)
                <tr>
                    <td>{{ $inside_profile->ID }}</td>
                    <td>{{ $inside_profile->Name }}</td>
                    <td>{{ $inside_profile->brand_name }}</td>
                    <td>
                        @if($inside_profile->imageExists())
                            <img src="{{ $inside_profile->image_url }}">
                        @else
                            <i>Missing: {{ $inside_profile->internal_image_path }}</i>
                        @endif
                    </td>
                    <td>
                        @if($inside_profile->show_in_door_builder)
                        ✓
                        @endif
                    </td>
                    <td>
                        <a href="/backend/door-combinations/{{ $inside_profile->ID }}/edit">
                        Edit
                        </a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>

    </div>
</div>

@stop


@section('scripts')


@stop