@extends('templates.products')

@section('content')

{{ Form::open(['method' => 'put', 'url' => "/backend/door-combinations/{$inside_profile->ID}"]) }}

    <h3 class="page-title">
        Door Combinations - {{ $inside_profile->Name }} 
        {{ Form::submit("Save", ['class'=>'btn btn-primary']) }}
    </h3>

    <div class="row">
        <div class="col-sm-8">

            <div class="form-group">
                <label for="call_back_number">Name</label>
                {{ Form::text('Name', $inside_profile->Name, ['class'=>'form-control']) }}
            </div>

            <div class="form-group">
                <label for="call_back_number">Brand</label>
                {{ Form::text('Name', $inside_profile->brand_name, ['class'=>'form-control', 'disabled']) }}
            </div>

            <div class="form-group">
                <label>Image</label><br>
                <img src="{{ $inside_profile->image_url }}">
            </div>

            <div class="form-group">
                <label for="call_back_number">Show in Door Builder</label>
                {{ Form::checkbox('show_in_door_builder', '1', $inside_profile->show_in_door_builder) }}
            </div>

            <br>

            <div class="row">
                <div class="col-sm-4">
                    <label>Outside Profiles</label>
                    <br>

                    @foreach($outside_profiles as $outside_profile)
                        {{ $outside_profile->Name }}<br>

                        @if($outside_profile->imageExists())
                            <img src="{{ $outside_profile->image_url }}">
                        @else
                            <i>Missing: {{ $outside_profile->internal_image_path }}</i>
                        @endif
                        
                        {{ Form::checkbox("outside_profile_ids[]", $outside_profile->ID, $inside_profile->hasOutsideProfile($outside_profile)) }}
                        <br><br>
                    @endforeach
                </div>
                <div class="col-sm-4">
                    <label>Center Panels</label>
                    <br>

                    @foreach($center_panels as $center_panel)
                        {{ $center_panel->Name }}<br>
                        @if($center_panel->imageExists())
                            <img src="{{ $center_panel->image_url }}">
                        @else
                            <i>Missing: {{ $center_panel->internal_image_path }}</i>
                        @endif
                        {{ Form::checkbox("center_panel_ids[]", $center_panel->ID, $inside_profile->hasCenterPanel($center_panel)) }}
                        <br><br>
                    @endforeach
                </div>
                <div class="col-sm-4">
                    <label>Stile/Rails</label>
                    <br>

                    @foreach($stile_rails as $stile_rail)
                        {{ $stile_rail->Name }}<br>
                        @if($stile_rail->imageExists())
                            <img src="{{ $stile_rail->image_url }}">
                        @else
                            <i>Missing: {{ $stile_rail->internal_image_path }}</i>
                        @endif
                        {{ Form::checkbox("stile_rail_ids[]", $stile_rail->ID, $inside_profile->hasStileRail($stile_rail)) }}
                        <br><br>
                    @endforeach
                </div>
            </div>

        </div>
    </div>

{{ Form::close() }}

@stop


@section('scripts')


@stop