@extends('templates.crm')

@section('content')

<h3 class="page-title">Edit Phone Message</h3>

{{ Form::open(['method' => 'put', 'url' => URL::route('phone-log.update', $phone_message->id)]) }}
    @include('phone-message.edit-create')
{{ Form::close() }}



@stop