@extends('templates.crm')

@section('content')

<h3 class="page-title">

    Phone Message

    <a href="{{ URL::route('phone-log.edit', $phone_message->id) }}">
        <i class="glyphicon glyphicon-pencil" title="edit"></i>
    </a>

</h3>

<p><b>Assigned To: </b><br>{{ $phone_message->assigned_to_list }}

<p><b>Message: </b><br>{{{ $phone_message->content }}}</p>

<p><b>Call Back #: </b><br>{{{ $phone_message->call_back_number }}}</p>

<p><b>Date: </b><br>{{ $phone_message->datetime }}</p>

{{ Form::open(['method' => 'delete', 'id'=>'delete-form', 'style'=>'display: none']) }}

{{ Form::close() }}

@stop

@section('scripts')

<script>

    $('.delete').click(function(e){
        e.preventDefault();
        var ans = confirm($(this).attr('message'));

        if(ans){
            $('#delete-form').attr('action', $(this).attr('href')).submit();
        }
    });
</script>

@stop