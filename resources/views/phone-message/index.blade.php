@extends('templates.crm')

@section('content')

<h3 class="page-title">
    Phone Log
    <a class="btn btn-primary" href="{{ URL::route('phone-log.create') }}">Add</a>
</h3>

<table id="phone-message-table" class="display">
    <thead class="account_view">
        <tr>
            <th class="text-left" > Date</th>
            <th class="text-left" >Message</th>
            <th class="text-left" >Callback Number</th>
            <th class="text-left">Assigned To</th>
            <th class="text-center">Edit</th>
        </tr>
    @if(count($phone_messages))
    <tbody>
        @foreach($phone_messages as $row)                                
        <tr>
        <td class="text-left" style="width: 40px;">{{ $row->datetime}}</td>
        <td class="text-left"><a href="/crm/phone-log/{{ $row->id}}">{{$row->content}}</a></td>
            <td class="text-left">{{$row->call_back_number}}</td>
            <td class="text-left">@php echo html_entity_decode($row->assigned_to_list); @endphp</td>
            <td class="text-center"><a href="/crm/phone-log/edit/{{$row->id}}"><span title='edit' class='glyphicon glyphicon-pencil'></span></a></td>           
        @endforeach
    </tbody>
    @endif
    </thead>
</table>

{{ Form::open(['method' => 'delete', 'id'=>'delete-form', 'style'=>'display: none']) }}

{{ Form::close() }}

@stop

@section('scripts')

<script>

    $('#phone-message-table').DataTable({
        "pageLength": 25,
        "order": [[ 0, "desc" ]],
        "columnDefs": [
                {
                    "targets": [ 4 ],
                    "orderable": false,
                    "searchable": false
                }
            ]
    });

</script>


@stop


@section('styles')

@stop

