@extends('templates.crm')

@section('content')

<h3 class='page-title'>Add Phone Message</h3>

{{ Form::open(['method' => 'post', 'url' => "crm/phone-log"]) }}
    @include('phone-message.edit-create')
{{ Form::close() }}

@stop