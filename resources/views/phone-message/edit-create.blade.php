<div class="form-group">
    <label for="assignees">Assigned To</label>
    {{ Form::select('assignee_ids[]', $user_list, $phone_message->assignees->pluck('UserID'), ['multiple'=>true, 'class'=>'form-control select2']) }}
</div>

<div class="form-group <?php if($errors->has('content')): ?>has-error<?php endif; ?>">
    <label for="content">Message</label>
    {{ Form::textarea('content', $phone_message->content, ['class'=>'form-control', 'id'=>'content', 'rows'=>3]) }}
    @php echo html_entity_decode($errors->first('content', '<br><p class="text-danger">:message</p>')); @endphp
</div>

<div class="form-group">
    <label for="call_back_number">Call Back #</label>
    {{ Form::text('call_back_number', $phone_message->call_back_number, ['class'=>'form-control', 'id'=>'call_back_number']) }}
</div>

<div class="form-group  <?php if($errors->has('datetime')): ?>has-error<?php endif; ?>">
    <label for="datetime">Date / Time</label>
    {{ Form::text('datetime', $phone_message->datetime, ['class'=>'form-control', 'id'=>'datetime']) }}
    {{ $errors->first('datetime', '<span class="text-danger">:message</span>') }}
</div>

<input type="submit" value="Log Message" class="btn btn-primary">