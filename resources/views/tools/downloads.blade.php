@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  ' | Downloads';
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

?>
@include('layouts.header')
<body>
<div class="container">
    @include('layouts.navbar')
</div>
<div class="container">
    {{-- <div class="row row-search">
        <div class="col-sm-12">
            <div class="well well-sm"> --}}
                <div class="col-sm-12">






                    
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="panel panel-UX">
                                    <div class="panel-heading">Downloads</div>
                                    <div class="panel-body">
                                        <div class="xcrud">
                            <div class="xcrud-container">
                            <div class="xcrud-ajax">
                                <input type="hidden" class="xcrud-data" name="key" value="397689c47c43213bc1b5bd670f6c735157f66c8c" /><input type="hidden" class="xcrud-data" name="orderby" value="" /><input type="hidden" class="xcrud-data" name="order" value="asc" /><input type="hidden" class="xcrud-data" name="start" value="0" /><input type="hidden" class="xcrud-data" name="limit" value="20" /><input type="hidden" class="xcrud-data" name="instance" value="dac486634f6325d7040880717b5eb74f911e914f" /><input type="hidden" class="xcrud-data" name="task" value="list" /><input type="hidden" class="xcrud-data" name="sess_name" value="PHPSESSID" />        <div class="xcrud-top-actions">
                                <div class="btn-group pull-right">
                                    {{-- <a href="javascript:;" data-task="print" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-print"></i> Print</a><a href="javascript:;" data-task="csv" class="btn btn-default xcrud-in-new-window xcrud-action"><i class="glyphicon glyphicon-file"></i> Export into CSV</a>            </div> --}}
                                {{-- <a href="javascript:;" data-task="create" class="btn btn-success xcrud-action"><i class="glyphicon glyphicon-plus-sign"></i> Add</a>			<a class="xcrud-search-toggle btn btn-default" href="javascript:;">Search</a><span class="xcrud-search form-inline" style="display:none;"><input class="xcrud-searchdata xcrud-search-active input-small form-control" name="phrase" data-type="text" style="" data-fieldtype="default" type="text" value="" /><select class="xcrud-data xcrud-columns-select input-small form-control" name="column"><option value="">All fields</option><option value="downloads.Type" data-type="text">Type</option><option value="downloads.Subject" data-type="text">Subject</option><option value="downloads.File" data-type="file">File</option></select><span class="btn-group"><a class="xcrud-action btn btn-primary" href="javascript:;" data-search="1">Go</a></span></span>            <div class="clearfix"></div> --}}
                            </div>
                            <div class="xcrud-list-container">
                            <table class="xcrud-list table table-striped table-hover table-bordered">
                                <thead>
                                    <tr class="xcrud-th"><th data-order="asc" data-orderby="downloads.Type" class="xcrud-column xcrud-action">Type</th><th data-order="asc" data-orderby="downloads.Subject" class="xcrud-column xcrud-action">Subject</th><th data-order="asc" data-orderby="downloads.File" class="xcrud-column xcrud-action">File</th><th class="xcrud-actions">&nbsp;</th></tr>            </thead>
                                <tbody>
                                        <tr class="xcrud-row xcrud-row-0"><td>Lighting</td>
                                        <td>LED Installation</td><td>
                                        <a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=20&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=1diwujpaoj34og&xcrud%5Bsess_name%5D=PHPSESSID">PKImperiaLEDInstallationGuidelines.pdf</a></td>
                                        <td class=""></td>
                                        </tr>
                                        
                                        <tr class="xcrud-row xcrud-row-1"><td>Lighting</td>
                                            <td>LED Color Options</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=21&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=dpy4qm0pwsg0k8&xcrud%5Bsess_name%5D=PHPSESSID">TrescoLigtingExamplePhoto.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="21" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="21" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="21" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr>
                                            
                                        <tr class="xcrud-row xcrud-row-0"><td>MSDS</td><td>White & Gray Plywood</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=22&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=2jbtbn17elgkg&xcrud%5Bsess_name%5D=PHPSESSID">MSDSWhitehallPlywood.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="22" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="22" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="22" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>MSDS</td><td>Nu Green 2 (Uniboard)</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=23&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=6a3o6co4lnokgswg&xcrud%5Bsess_name%5D=PHPSESSID">MSDSNuGreen2Zero.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="23" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="23" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="23" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Info 101</td><td>Why Us?</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=25&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=21zgp7uy88lc8w0ogo&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---Why-Us.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="25" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="25" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="25" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Lighting</td><td>LED Routing Guide</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=34&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=tzangkw3avkc4g0c8g&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---LED-Routes.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="34" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="34" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="34" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Information</td><td>Doors by Style Group</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=35&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=3s1tka0qm82swkow&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---Door-Style-by-Style-Group-Availability.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="35" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="35" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="35" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Tech Page</td><td>Wood 5 Piece Door & Drawer Fronts</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=36&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=kw6dtg5da6840wg4o&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---5-Piece.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="36" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="36" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="36" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Tech Page</td><td>Bi-Fold Door - Tech Page</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=37&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=c9manvtpy4o4kkgo0&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---Bi-Fold-Sliding-Doors.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="37" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="37" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="37" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Info 101</td><td>Wood Characteristics</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=38&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=5nk4oqcq0a8swgc44&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---Wood-Characteristics.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="38" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="38" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="38" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Information</td><td>Finishes</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=39&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=1l71mt7qffwkgcsk&xcrud%5Bsess_name%5D=PHPSESSID">Tech-Pages---Finishing.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="39" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="39" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="39" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Hinging</td><td>Lift Up Clearances - Blum</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=40&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=1xwxeeb7utog4ow84&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="40" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="40" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="40" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Hinging</td><td>Lift Up Clearance - Blum HK</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=41&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=1d6cn4ha5a9wg&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances-HK.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="41" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="41" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="41" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Hinging</td><td>Lift Up Clearance - Blum HS</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=42&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=49r81wl8gni8gk4g4&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances-HS.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="42" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="42" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="42" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Hinging</td><td>Lift Up Clearance - Blum HL</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=43&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=4pz05fupo54w&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances-HL_2019-06-26_19_19_26.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="43" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="43" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="43" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Hinging</td><td>Lift Up Clearance - Blum HKS</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=44&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=181nrv1g7l348sco8&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances-HKS.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="44" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="44" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="44" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-0"><td>Hinging</td><td>Lift Up Clearance - Blum HF</td><td><a target="_blank" href="http://corymfg.ngrok.io/xcrud/xcrud_ajax.php?xcrud%5Binstance%5D=dac486634f6325d7040880717b5eb74f911e914f&xcrud%5Bfield%5D=downloads.File&xcrud%5Bprimary%5D=45&xcrud%5Bkey%5D=397689c47c43213bc1b5bd670f6c735157f66c8c&xcrud%5Btask%5D=file&xcrud%5Brand%5D=svhlhceplv4s4c4ck&xcrud%5Bsess_name%5D=PHPSESSID">Blum---Lift-Sizes--Clearances-HF.pdf</a></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="45" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="45" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="45" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr><tr class="xcrud-row xcrud-row-1"><td>Information</td><td>Want More Inserts?</td><td></td><td class=""><span class="btn-group"><a class="" title="View" href="javascript:;" data-primary="46" data-task="view"><a class="" title="Edit" href="javascript:;" data-primary="46" data-task="edit"><a class="" title="Remove" href="javascript:;" data-primary="46" data-task="remove" data-confirm="Do you really want remove this entry?"></span></td></tr>            </tbody>
                                
                            </table>
                            </div>
                            <div class="xcrud-nav">
                                {{-- <div class="btn-group xcrud-limit-buttons" data-toggle="buttons-radio"><button type="button" class="btn btn-default xcrud-action" data-limit="5">5</button><button type="button" class="btn btn-default xcrud-action" data-limit="10">10</button><button type="button" class="btn btn-default active xcrud-action" data-limit="20">20</button><button type="button" class="btn btn-default xcrud-action" data-limit="50">50</button></div>                         --}}
                                        </div>
                            </div>
                            <div class="xcrud-overlay"></div>
                        </div>
                    </div>


                {{-- <div class="panel panel-UX">
                    <div class="panel-heading">Downloads</div>
                    <div class="panel-body">
                    Downloads Here - what should be here?   
                    </div>
                </div>
                </div> --}}
            






                    

                </div>
            </div>
        {{-- </div>
    </div>
</div> --}}




<div class="container">
    @include('layouts.footer')
</div>
@include('layouts.loadjs')

</body>
</html>
