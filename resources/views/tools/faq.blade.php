@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  " | FAQ";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

?>
@include('layouts.header')
<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        {{-- <div class="row row-search">
            <div class="col-sm-12">
                <div class="well well-sm"> --}}
                    <div class="col-sm-12">
                        <div class="panel panel-UX">
                       
                            <div class="panel-heading">FAQ</div>
                            <div class="panel-body">
                                <p><h3>What defines your five piece wood door styles?</h3><h5><p>The door style name is defined by the inside frame profile. Each door name can then be customized to have the desired center panel, outside edge, and rail and stile width.</p></h5></p><br><p><h3>What are the standard specifications of a cabinet priced in SNAPIE?</h3><h5><p>White melamine Carb2 box, maple dovetail drawerbox, blum soft close slides and hinges. </p></h5></p><br><p><h3>What is the difference between Purekitchen and Imperia cabinets?</h3><h5><p>Purekitchen cabinets are made to the most exacting and clean standards in terms of finishes and panel adhesives. Imperia cabinets can be modified to use many of the finishes and core materials to bring them to the higher environmental specifications of the Purekitchen line. All Cory Manufacturing cabinetry is made to CARB2 standards for North American manufactured wood products that comply with or often surpass indoor environmental air quality requirements. </p></h5></p><br><p><h3>What are the new nomenclature rules or basic format?</h3><h5><table border="1" cellspacing="0" cellpadding="0">
                            <tbody>
                            <tr>
                            <td width="203">Code:</td>
                            <td width="322">DB30-2D</td>
                            <td width="34"> </td>
                            <td width="34"> </td>
                            <td width="34"> </td>
                            <td width="34"> </td>
                            <td width="34"> </td>
                            </tr>
                            <tr>
                            <td>Description:</td>
                            <td>Drawer Base Cabinet - 2 Large Drawers</td>
                            <td align="center" bgcolor="#CCCCCC">D</td>
                            <td align="center">B</td>
                            <td align="center" bgcolor="#CCCCCC">30</td>
                            <td align="center">-2D</td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td> </td>
                            <td> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td bgcolor="#CCCCCC">Cabinet Type:</td>
                            <td bgcolor="#CCCCCC">Drawer</td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td>Category</td>
                            <td>Base</td>
                            <td align="center"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td bgcolor="#CCCCCC">Width/Height/Depth</td>
                            <td bgcolor="#CCCCCC">30</td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td>Configuration (Top to Bottom)</td>
                            <td>2 Large Drawers</td>
                            <td align="center"> </td>
                            <td align="center"> </td>
                            <td align="center"> </td>
                            <td align="center"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            <tr>
                            <td bgcolor="#CCCCCC">Special/Other/Direction</td>
                            <td bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            <td align="center" bgcolor="#CCCCCC"> </td>
                            </tr>
                            </tbody>
                            </table></h5></p><br><p><h3>How can I use the search function to find a cabinet that I want?</h3><h5><p>You can type in "1530" to find a cabinet that is 15 x 30 You can type in a keyword such as "Base" or "Tall" Others?</p></h5></p><br><p><h3>Where do I change the &quot;Customer&quot; multiplier amount?</h3><h5><p>"Hello" button drop down, "Settings"</p></h5></p><br><p><h3>How do I add a glass door to a cabinet?</h3><h5><p>The "Glass Options" button in the cabinet level product page give you the ability to add a glass door to a single cabinet. You can even specify a lower door in a bi-fold lift up or just the right or left door in a side opening upper cabinet.</p></h5></p><br><p><h3>How do I insure that I have ordered all the correct parts for an integrated handle option kitchen project?</h3><h5><p>Select the "Handle Option" on doors that offer the option. This is typically slab doors but can be found on the overview door grid: ? In "Products" selection area, use the "Spectra" or "Dartmouth" tab to select cabinets that are available in that line. Be sure to order the extrusions or door match valences for the continuous look required on this product line. It is up to the specifier to order enough for the project as we do not know the scope or desired look of the cabinet runs.</p></h5></p><br><p><h3>Where do I upload my drawings or plans?</h3><h5><p>"Project Drawings and Files". It is highly recommended that you submit drawings and all appliance specification sheets.</p></h5></p><br><p><h3>If there is something I want and it is not in the system, can I get it?</h3><h5><p>We have selected a large variety of products but as always in the custom kitchen business, there are always things that a customer will want that is not in the catalog. You can always ask us and we will see if it can be accommodated.</p></h5></p><br><p><h3>Can I use the images that are in the gallery to promote the cabinets on my own website?</h3><h5><p>All images in the gallery are owned by Cory Mfg. and are available for use as needed. Please contact us if you are going to be doing print advertising so we can make sure you have the accurate information for photo credits or product descriptions.</p></h5></p><br><p><h3>What are the ship dates and how accurate are they?</h3><h5><p>The shipping dates are "out of production" dates and mean that we are estimating the product to be wrapped on that date. We will invoice and update scheduling the Monday of the week that the ship date falls under.</p></h5></p><br>                        </div>
                                                
                    </div>
                {{-- </div>
            </div>
        </div> --}}










                        </div>
                    </div>
                    </div>
                






                        

                    </div>
                </div>
            </div>
        </div>
    </div>
    



    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')

</body>
</html>
