@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  " | Glossary";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;
$public_path = public_path();


$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

?>
@include('layouts.header')
<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        {{-- <div class="row row-search">
            <div class="col-sm-12">
                <div class="well well-sm"> --}}
                    <div class="col-sm-12">






                        
                    <div class="panel panel-UX">
                        <div class="panel-heading">GLOSSARY</div>
                        <div class="panel-body">
                            <div class="row"><h3 style="margin-left: 15px;">Glossary</h3><hr style="margin: 10px;"><div class="col-md-12"><div class="row"><div class="col-md-3 text-left"><h4><strong>Blind: </strong></h4></div><div class="col-md-9"><h4><p>Description of a cabinet that has a hidden section, typically in a corner where the adjacent cabinet abuts the face plane of the cabinet.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Book Matched: </strong></h4></div><div class="col-md-9"><h4><p>Veneers that have a matched pattern and are effectively cut from the same piece of wood and applied in order to create the visual of opening a book and seeing the matching veneers</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Butt Joint: </strong></h4></div><div class="col-md-9"><h4><p>Joint with two panels where one panels end sits directly on the face of the other, creating a seam on one side that shows the end thickness of the longer panel.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>CARB / CARB2: </strong></h4></div><div class="col-md-9"><h4><p>California Air Resources Board. CARB 1 and CARB 2 were the indoor air quality standards for finishes that specified levels of VOC's and Formaldehyde in wood products. See more here: link?</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Dado: </strong></h4></div><div class="col-md-9"><h4><p>a square cut into a panel used most often to set a perpendicular panel into the slot</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Dovetail: </strong></h4></div><div class="col-md-9"><h4><p>Traditionally hand cut wood box connection technique. Most modern dovetails are machine cut. The shape provides a mechanical advantage in that the notch traps the adjoining material. Quality of dovetails vary as some are cut accurately, some are finished after fit and sanding and some are manufactured using prefinished material that leaves the end grain of the dovetail join unfinished.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Dowel construction: </strong></h4></div><div class="col-md-9"><h4><p>Small wooden pins, often impregnated with glue, that are used to assemble two pieces of wood or panel material. Fully assembled cabinets are typically assembled with dowels and not RTA or "ready to assemble", fasteners</h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Expansion and contraction: </strong></h4></div><div class="col-md-9"><h4><p>Movement in materials due to heat and moisture fluctuations. Solid wood materials are prone to expansion when heat and moisture are applied. Engineered materails such as MDF and particleboard are more stable than solid wood.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Faceframe: </strong></h4></div><div class="col-md-9"><h4><p>Traditional cabinet construction technique that consists of frames of solid wood that surround the fronts of each cabinet. Often applied after the cabinets were installed on site.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Formaldehyde: </strong></h4></div><div class="col-md-9"><h4><p>A natural substance that is used in concentrated forms in adhesives. Known to cause health problems and due to CARB regulations, has been lowered in wood products in the United States. More&hellip;..</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Frameless: </strong></h4></div><div class="col-md-9"><h4>Modern cabinetry construction technique that does not have a faceframe applied to the front of each cabinet. Also known as "full access" the cabinets allow the full frontal access to the space in the cabinet and maximize the drawerbox size to fit the cabinet fully. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>HPL High Pressure Laminate: </strong></h4></div><div class="col-md-9"><h4>Often known by the brand name "Formica", laminate is a thin resin impregnated paper material that is pressed under high pressure. Typically has a decorative paper face that is solid color or simulated wood grain. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Hybrid Door: </strong></h4></div><div class="col-md-9"><h4>Five piece wood framed door with MDF center panel. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Legrabox: </strong></h4></div><div class="col-md-9"><h4><p>Blum&rsquo;s new drawerbox. Specs? Side thickness. No gallery rails, etc&hellip;only one color. Introduced 2015.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>LPL Low Pressure Laminate: </strong></h4></div><div class="col-md-9"><h4><p>Also know as "Melamine" the thin laminate skin is typically a single sheet of paper with out the phenolic backer of HPL.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>MDF : </strong></h4></div><div class="col-md-9"><h4>Medium Density Fiberboard. Typically industrial wood byproduct sawdust and chips that are pressed into highly engineered panels of consistent and high density. More consistent than particleboard, which tends to have larger flakes and more air. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Melamine : </strong></h4></div><div class="col-md-9"><h4>Melamine is technically a resin but is often used to describe the particleboard core with paper impregnated melamine skin on one or two sides. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Metabox : </strong></h4></div><div class="col-md-9"><h4>Blum low cost metal nylon roller bearing side mount slides. Often used in commercial applications. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Miter : </strong></h4></div><div class="col-md-9"><h4>angle cut on the end of two panels that create a single seam at the very edge corner of the joint. </h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Mortise and Tenon : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>NAUF: </strong></h4></div><div class="col-md-9"><h4><p>No Added Urea Formaldehyde</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Paper Composite: </strong></h4></div><div class="col-md-9"><h4><p>Low pressure phenolic impregnated paper</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Particleboard: </strong></h4></div><div class="col-md-9"><h4><p>Panel product manufactured with chips of wood material and bound with an adhesive. Product quality varies greatly from local to imported product as well as core density. A high quality particleboard has a softer open core and a more dense face and skin.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Passive House: </strong></h4></div><div class="col-md-9"><h4><p>Design and construction of housing that uses high R value insulation and windows to achieve a very thermally stable interior. Combined with a heat exchange system, the energy usage of this type of construction can be very low.&nbsp;</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Plain Sawn : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Plywood: </strong></h4></div><div class="col-md-9"><h4><p>Panel product manufactured from a softer thick veneer core in multiple layers. Often alder or pine, is typically faced with a higher quality wood veneer. Came into widespread use around 1945 and replaced more solid wood construction materials. .</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Polished: </strong></h4></div><div class="col-md-9"><h4><p>Top coat finish where the applied finish is then hand or machine polished. This is done in multiple stages with varying grit of abrasive to achieve a highly reflective finish. This is very similar to an automotive or furniture finish. &ldquo;Gloss&rdquo; finish is similar and can be used to describe a &ldquo;polished&rdquo; finish but in our process we differentiate as there are non-polished gloss finishes that can be applied to cabinet doors.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Quartersawn : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Rabbit : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Rail and Stile : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Rift-cut : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Rotary Cut : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Routed : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Scribe : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Slab Door: </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Soffit : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Stock/Semi-custom/Custom Cabinetry : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Stretcher : </strong></h4></div><div class="col-md-9"><h4></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Tandembox: </strong></h4></div><div class="col-md-9"><h4><p>Blum Grey metal sided drawer system</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Thermofoil: </strong></h4></div><div class="col-md-9"><h4><p>A plastic sheet that is formed via heat and pressure to a substrate and typically forms around the edges and into any profiles cut into a face of a cabinet door. Quality varies tremendously from box store imported product to high quality high finish products.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>Veneer: </strong></h4></div><div class="col-md-9"><h4><p>A thin layer of wood that is typically applied to a substrate of lower cost or more stable structural core.</p></h4><br></div></div><div class="row"><div class="col-md-3 text-left"><h4><strong>VOC�s: </strong></h4></div><div class="col-md-9"><h4><p>Volative Organic Compounds. Organic chemicals that can be hazardous to ones health. Examples include formaldehyde and petroleum based solvents used in spray applied finishes. Due to cost, regulation and environmental concerns, many of these are moving towards water based finishes.</p></h4><br></div></div></div></div><br>                        </div>
                    </div>
                    </div>
                    </div>
                






                        

                    </div>
                </div>
            {{-- </div>
        </div>
    </div> --}}
    



    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')

</body>
</html>
