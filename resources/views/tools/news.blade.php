@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  ' | News';
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

?>


@include('layouts.header')
<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        {{-- <div class="row row-search"> --}}
            {{-- <div class="col-sm-12"> --}}
                {{-- <div class="well well-sm"> --}}
                    <div class="col-sm-12">






                        
                    <div class="panel panel-UX">
                        <div class="panel-heading">NEWS</div>
                            <div class="panel-body">
                                <div class="media"><div class="row"><div class="col-xs-2"><div class="media-left"><a href="/downloads/111215UpdatesHalifaxetc.pdf" target="_blank"><img class="media-object thumbnail" src="/downloads/111215UpdatesHalifaxetc.png" alt="..." style="width:155px;height:200px"></a><div class="text-center"><a href="../downloads/111215UpdatesHalifaxetc.pdf" target="_blank">Download PDF</a></div></div></div><div class="col-xs-10"><div class="media-body"><div class="media-heading"><h4><strong>Bulletin: </strong><small>2015-11-12</small><br>New Halifax Purekitchen Door and factory updates</h4><p><p>Greetings,</p>
                                <p>I would like to thank you all for a great 2015 and your continued support of the Imperia and Purekitchen cabinet lines manufactured by the team here at Cory Manufacturing.</p>
                                <p>A few updates:</p>
                                <ul>
                                <li>We are excited to introduce a new door in the Purekitchen line. The “Halifax” door is available in maple, walnut, white oak, cherry, and paint grade maple. Please note it is 7/8” thick. For more detailed specifications, see the attached drawing.  <br />  </li>
                                <li>In the search for a new employee to run our edgebander, we explored the option to purchase a new machine and get some updated automation into our plant. During this process, we found that what was more important was the software that serves as the back end to our OES system and runs the information fed into our machinery. This led us to the recent purchase of a comprehensive piece of software that will take our multiple back end software tools and pull them all together into an integrated solution.<br />  </li>
                                <li>The software is an object-based, design, pricing, and machinery management product. This full integration means that we will be revamping our product offerings and documentation. This process will be a lot of work to implement but will give us further efficiencies and more accurate data that allows us to continue growing while keeping our overhead costs low.  <br />  </li>
                                <li>This new software is currently being implemented by Patrick Bannasch and should take a few more months to put in place. In order to have him focus on this project, we brought in a new employee, Michael Endreny, who has taken on some of Patrick’s previous responsibilities as well as some of Emily Bible’s bookkeeping roles.<br />  </li>
                                <li>Michael comes from a material and construction background. He has managed both product and services at a material distribution company and an architectural re-use center, both in New York City. He recently relocated to the Boston area. He has fit it quickly and has been a vital asset as many of you have been coordinating with him on product shipping and financial matters for most of this year. He can be reached in our offices in West Bridgewater or at <a href="mailto:mike@corymfg.com">mike@corymfg.com</a>.<br />  </li>
                                <li>The Imperia Order Entry System has long been both a resource and a source of pain. We have made fixes and worked hard to get it to be a usable product to assist our dealers. Over the summer it became very clear that we would need to build a new one from scratch. With the software updates we have implemented, we have also been building a new OES that should be working and functional around the New Year. It has been a tremendous project but we have all put in a lot of time to make it both current and as easy as possible to use.<br />  </li>
                                <li>We are working hard on our production infrastructure to be ready for a solid spring of kitchen cabinet sales. Keep us updated on movement on projects and in your businesses and good luck out there.</li>
                                </ul>
                                <p> </p>
                                <p>Thank you very much,</p>
                                <p>Sergei Hasegawa</p></p></div></div></div></div>			</div>
                            </div>
                        </div>
                    </div>
                    </div>
                






                        

                    </div>
                </div>
            {{-- </div> --}}
        {{-- </div> --}}
    {{-- </div> --}}
    



    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')

</body>
</html>
