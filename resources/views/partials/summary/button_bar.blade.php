{{ Form::open(['url'=>"/quote_orders/summaryPdf/{$job->JobID}", 'style'=>'margin-bottom: 1px']) }}
<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-UX">
            <div class="panel-heading" style="border: none;">
                <div class="pull-right">
                    <h5 style="margin:0px;">
                        Pricing: &nbsp;
                        <input type="radio" class="Price" name="Price" value="LIST" checked="" > List <small>(x 1)</small>&nbsp;&nbsp;
                        <input type="radio" class="Price" name="Price" value="COST" > Net <small>(x {{ number_format($job->multiplier_discount, 3) }})</small>&nbsp;&nbsp;
                        <input type="radio" class="Price" name="Price" value="CUSTOMER" > Customer <small>(x {{ number_format($job->multiplier_customer, 3) }})</small>&nbsp;
                        <input type="radio" class="Price" name="Price" value="No" > No Pricing &nbsp;
                    </h5>
                </div>

                <button type="submit" class="btn btn-primary" style="margin-top: -5px;margin-bottom: -5px" name="type" value="view-summary" >
                    Create &amp; View PDF
                </button>
                <button type="submit" class="btn btn-primary" style="margin-top: -5px; margin-bottom: -5px" name="type" value="download-summary" >
                    Create &amp; Download PDF 
                </button>

                <button type="submit" class="btn btn-warning" style="margin-top: -5px; margin-bottom: -5px" name="type" value="download-signoff" >
                    Sign Off Order
                </button>
                             
                <?php if(session('accounttype') == '-1'){ ?>
                    <button type="submit" class="btn btn-warning" style="margin-top: -5px; margin-bottom: -5px" name="type" value="download-cabcard" >
                        Cab Card
                    </button>
                <?php } ?>

            </div>
        </div>
    </div>
</div>
{{ Form::close() }}
