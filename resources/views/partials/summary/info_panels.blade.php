<?php
 

//get the addresses for this individual job
  use Illuminate\Support\Facades\DB;
  
  $jobIDSummary     = $job->JobID;
  $custInfo         = DB::table("job_header")->where('AddressType', '=', 'Customer')->where('JobID', '=', $jobIDSummary )->get();
  $shipInfo         = DB::table("job_header")->where('AddressType', '=', 'Shipping')->where('JobID', '=', $jobIDSummary )->get();
  $jobHeaderNote    = DB::table("job_header")->where('JobID', '=', $jobIDSummary )->get();
    
    if(isset($job->construction->HeaderNote)){
        $constructionNote = $job->construction->HeaderNote;
    }else{
        $constructionNote = "";
    }
  
  $users =  DB::table("users")->select('FirstName','LastName')->where('UserID', '=', $job->CreatedID )->get();
  $fName = '';
  $lName = '';

  foreach ($users as $user) {
    $fName = $user->FirstName;
    $lName = $user->LastName;
  }


?>


<style>

.infoPanels { font-size: 12px; }


</style>
<div class="row">
    <div class="col-xs-9">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-UX" style="height: 135px;">
                        <div class="pull-left" style="padding-top: 5px; padding-left: 15px">
                            <?php 
                                //Create Date
                                $mysqlTime = strtotime($job->CreatedDate);
                                $CreatedDate = date("M j, Y, g:i a");    

                                //Create Date
                                $mysqlTime = strtotime($job->ModifiedDate);
                                $ModifiedDate = date("M j, Y, g:i a", $mysqlTime);   
                            ?>
                            <span style="font-size: 2em;">{{ $job->JobID }}</span>
                            
                            <table cellspacing="10" cellpadding="10" class="infoPanels">
                                <tr>
                                    <td >Job</td><td>&nbsp; {{ $job->Reference  }}</td>
                                </tr>
                                <tr>
                                    <td >Created</td><td>&nbsp; {{ $fName . ' ' . $lName }}</td>
                                </tr>
                                <tr>
                                    <td >Modified</td><td>&nbsp; {{ $ModifiedDate }}</td>
                                </tr>
                                <tr>  
                                    <td >Printed</td><td>&nbsp; {{ $CreatedDate }}</td>
                                </tr>                                
                            </table>
                        </div>   
                        
                        <div class="pull-right infoPanels" style="border: 1px; padding-top: 5px; padding-right: 15px;">
                             <span style="font-size: 2em;">{{ strtoupper(($job->stage->Name)) }}</span><br />
                            {{ $job->specifications->account_info->name }}<br>
                            {{-- {{ $job->specifications->account_info->users->full_name }} <br /> --}}
                            {{ $job->specifications->account_info->address }}<br>          
                            {{ $job->specifications->account_info->phone }}<br>
                            {{ $job->specifications->account_info->email }}
                            {{-- {{ print_r($job->specifications); }} --}}
                        </div>    
                        <div align="center">
                            <br /><br />
                            <img style="max-width: 150px; max-height: 50px;" src="{{ $logo_image_path }}">
                        </div>  
                        
                    </div>
                </div>
            </div>
            <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-UX">
                    <div class="panel-heading">{{ $job->specifications->style_group->name }}</div>
                    
                    <table class="table table-condensed table-bordered table-hover infoPanels">
                       
                            <tr>
                                <td class="text-center" style="background-color: #E9EDEF; width:16%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:16.8%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Name
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:16.8%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Inside
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:16.8%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Center Panel
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:16.8%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Outside
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:16.8%;white-space:nowrap;border:none;">
                                    Stile/Rail
                                </td>
                            </tr>
                      
                        <tbody>
                            <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">
                                    Door
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->door->name)) ? '' : '-'; ?> {{ $job->specifications->door->name }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->door->inside)) ? '' : '-'; ?> {{ $job->specifications->door->inside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->door->center_panel)) ? '' : '-'; ?> {{ $job->specifications->door->center_panel }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->door->outside)) ? '' : '-'; ?> {{ $job->specifications->door->outside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;">
                                    <?php echo (isset($job->specifications->door->style_rail)) ? '' : '-'; ?> {{ $job->specifications->door->style_rail }}
                                </td>
                            </tr>

                            <?php 
                                
                                //If Drawer is a slab don't show profile options as they are not inherited. 
                                
                                $DrawerName     = $job->specifications->drawer->name;
                                $DrawerInside   = $job->specifications->drawer->inside; 
                                $DrawerOutside  = $job->specifications->drawer->outside;
                                $DrawerCenter   = $job->specifications->drawer->center_panel;
                                $DrawerRail     = $job->specifications->drawer->style_rail;

                                $SlabDrawer = strpos($DrawerName, "DRW");
                                if($SlabDrawer === false){
                                    //echo "slab NOT found: $DrawerName - $SlabDrawer";
                                }else{
                                    $DrawerInside   = "";
                                    $DrawerCenter   = "";
                                    $DrawerOutside  = "";
                                    $DrawerRail     = "";
                                    //echo "slab found: $DrawerName - $SlabDrawer";
                                }
                            ?>
                             <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">
                                    Drawer
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->drawer->name )) ? '' : '-'; ?> {{ $DrawerName }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->drawer->inside )) ? '' : '-'; ?> {{ $DrawerInside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                   <?php echo (isset($job->specifications->drawer->center_panel )) ? '' : '-'; ?>  {{ $DrawerCenter }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->drawer->outside )) ? '' : '-'; ?> {{ $DrawerOutside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;">
                                    <?php echo (isset($job->specifications->drawer->style_rail )) ? '' : '-'; ?> {{ $DrawerRail }}
                                </td>
                            </tr>

                            <?php 
                                
                                //If LGDrawer is a slab don't show profile options as they are not inherited. 
                                
                                $LgDrawerName     = $job->specifications->large_drawer->name;
                                $LgDrawerInside   = $job->specifications->large_drawer->inside; 
                                $LgDrawerOutside  = $job->specifications->large_drawer->outside;
                                $LgDrawerCenter   = $job->specifications->large_drawer->center_panel;
                                $LgDrawerRail     = $job->specifications->large_drawer->style_rail;

                                $LgSlabDrawer = strpos($LgDrawerName, "DRW");
                                if($LgSlabDrawer === false){
                                    //echo "slab NOT found: $DrawerName - $SlabDrawer";
                                }else{
                                    $LgDrawerInside   = "";
                                    $LgDrawerCenter   = "";
                                    $LgDrawerOutside  = "";
                                    $LgDrawerRail     = "";
                                    //echo "slab found: $DrawerName - $SlabDrawer";
                                }
                            ?>

                            <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">
                                    Large Drawer
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->large_drawer->name )) ? '' : '-'; ?> {{ $LgDrawerName }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->large_drawer->inside)) ? '' : '-'; ?>{{ $LgDrawerInside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->large_drawer->center_panel)) ? '' : '-'; ?>{{ $LgDrawerCenter }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->large_drawer->outside )) ? '' : '-'; ?>{{ $LgDrawerOutside }}
                                </td>
                                <td class="text-center" style="white-space:nowrap;">
                                    <?php echo (isset($job->specifications->large_drawer->style_rail)) ? '' : '-'; ?>{{ $LgDrawerRail  }}
                                </td>
                            </tr>
                           
                        </tbody>
                    </table>
                </div>
                <div class="panel panel-UX">
                    <table class="table table-condensed table-bordered table-hover infoPanels">
                        
                            <tr>
                                <td class="text-center" style="background-color: #E9EDEF; width:16%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:21%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Material
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:21%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Color
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:21%;white-space:nowrap;border:none;border-right:1px solid #ddd;">
                                    Finish
                                </td>
                                <td class="text-center" style="background-color: #E9EDEF; width:21%;white-space:nowrap;border:none;">
                                    Banding
                                </td>
                            </tr>
                        
                        <tbody>
                            <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">
                                    Door
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                   <?php 
                                        if(isset($job->specifications->style->material )){
                                        
                                                echo $job->specifications->style->material;
                                          
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php 
                                        if(isset($job->specifications->style->color )){    
                                                echo $job->specifications->style->color;                                    
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                    
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php 
                                        if(isset($job->specifications->style->finish )){                                         
                                            echo $job->specifications->style->finish;                                         
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </td>
                                
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                </td>
                            </tr>
                            
                            <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">CabExt</td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                   <?php 
                                        if(isset($job->specifications->cabinet_exterior->name )){                                           
                                                echo $job->specifications->cabinet_exterior->name;                                      
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                       
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                     <?php 
                                        if(isset($job->specifications->cabinet_exterior->color )){                                         
                                                echo $job->specifications->cabinet_exterior->color;                                           
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                   <?php 
                                        if(isset($job->specifications->cabinet_exterior->finish )){                                       
                                                echo $job->specifications->cabinet_exterior->finish;                                     
                                        }else{
                                            echo "-";
                                        }
                                    ?>
                                </td>
                                 <td class="text-center" style="border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->cabinet_exterior->edge )) ? '' : '-'; ?> {{ $job->specifications->cabinet_exterior->edge }} 
                                </td>
                           </tr>
                           
                           <tr>
                                <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd;">CabInt</td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <?php echo (isset($job->specifications->cabinet_interior->name )) ? '' : '-'; ?> {{ $job->specifications->cabinet_interior->name }}      
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                   <!-- <?php echo (isset($job->specifications->cabinet_interior->brand )) ? '' : '-'; ?>  {{ $job->specifications->cabinet_interior->brand }} -->
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <!-- <?php echo (isset($job->specifications->cabinet_interior->material_group )) ? '' : '-'; ?> {{ $job->specifications->cabinet_interior->material_group}} -->
                                </td>
                                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                                    <!-- <?php echo (isset($job->specifications->cabinet_interior->edge )) ? '' : '-'; ?> {{ $job->specifications->cabinet_interior->edge }} -->
                                </td>
                            </tr>
                        </tbody>
                    </table>                            
                </div>
                <div class="panel panel-UX">
                    <table class="table table-condensed table-bordered table-hover infoPanels">
                        <tr>
                            <td style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd; width: 125px">Drawer Box</td>
                            <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;"><?php echo (isset($job->specifications->drawer_box_name )) ? '' : '-'; ?>{{ $job->specifications->drawer_box_name }}</td>
                            <td class="text-center" style="background-color: #E9EDEF; white-space:nowrap;border-right:1px solid #ddd; width: 125px">Handle/Pull</td>
                            <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;"><?php echo (isset($job->specifications->door_pull)) ? '' : '-'; ?>{{ $job->specifications->door_pull }}</td>
                            <td class="text-center" style="background-color: #E9EDEF; white-space:nowrap; width: 125px">Toe Kick</td>
                            <td class="text-center" style="white-space:nowrap;"><?php echo (isset($job->specifications->toe_kick )) ? '' : '-'; ?>{{ $job->specifications->toe_kick }}</td>
                        </tr>
                    </table>
                </div>
            
            </div>
        </div>
    </div> <!-- END COL 9 -->
    <div class="col-xs-3">
        <div class="row">
            <div class="col-xs-12">
                <!-- <div class="panel panel-UX" align="center" style="height: 135px;"><br>
                    <span style="font-size:48pt; text-align:center;"><?php echo $jobIDSummary?></span>
                </div> -->
                <div class="panel panel-UX" style="height: 230px;">
                    {{-- <div class="panel-heading">Customer</div> --}}
                    <div class="panel-body" style="padding-top: 5px;padding-bottom: 5px;">
                      <strong>Customer </strong>
                      <?php
                      $cusflag = 1;
                      foreach ($custInfo as $customer) {
                          if($cusflag == 1){
                            if ($customer->Company != '') {
                                    echo "<br /> ";
                                    echo $customer->Company . " <br> ";
                            }
                            echo $customer->ContactName . " <br></strong> ";
                            echo $customer->AddressLine1 . " <br> ";
                            if ($customer->AddressLine2){
                                echo $customer->AddressLine2 . " <br> ";
                            }
                            if($customer->City){
                                echo $customer->City . ", " . $customer->State . " " . $customer->PostalCode .  " <br> ";
                            }else{
                                echo  " <br> ";
                            }
                            $in = $customer->EmailAddress;
                            $truncated = strlen($in) > 21 ? substr($in,0,21)."..." : $in; 
                            echo $truncated;
                            echo " <br> ";
                            echo "<i>" . $customer->Notes . "</i><br> ";
                            $cusflag =2;
                          }
                        }
                      ?>
                    </div>
                </div>

                <div class="panel panel-UX" style="height: 230px;">
                    {{-- <div class="panel-heading">Ship-To</div> --}}
                    <div class="panel-body" style="padding-top: 5px;padding-bottom: 5px; ">
                      <strong>Ship-to </strong>
                    <?php
                    $shipflag = 1;
                      foreach ($shipInfo as $shipping) {
                          if($shipflag == 1){
                            if($shipping->Company != ''){
                                echo "<br /> ";
                                echo  $shipping->Company . " <br> ";
                            }
                            echo $shipping->ContactName . " <br>";
                            echo $shipping->AddressLine1 . " <br> ";
                            if ($shipping->AddressLine2){
                                echo $shipping->AddressLine2 . " <br> ";
                            }
                            if($shipping->City){
                                echo $shipping->City . ", " . $shipping->State . " " . $shipping->PostalCode .  " <br> ";
                            }else{
                                echo  " <br> ";
                            }
                                echo $shipping->PhoneNumber;
                                echo "<br>";
                                $in = $shipping->EmailAddress;
                                $truncated = strlen($in) > 21 ? substr($in,0,21)."..." : $in; 
                                echo $truncated;
                                echo " <br> ";
                                echo "<i>" . $shipping->Notes . "</i><br> ";
                                $shipflag = 2;
                        }
                      }
                      ?>
                    </div>
                </div>    
            </div>
        </div>
    </div> <!-- END COL 3 -->
    <div class="col-xs-12">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-UX">
                    <div class="panel-heading">Notes</div>
                    <div style="margin: 10px; min-height: 50px;"><?php echo nl2br($constructionNote);  ?></div>
                </div>
                <?php 
                 if($job->specifications->admin_note != ""){
                ?>
                <div class="panel btn-danger">
                    <div class="panel-heading "><strong>** INTERNAL NOTES **</strong></div>
                    <div style="margin: 10px; min-height: 50px;"><?php echo nl2br($job->specifications->admin_note);  ?></div>
                </div>
                <?php
                 }                
                 ?>
            </div>
        </div>
    </div>

</div>

    