<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Products Summary</div>
            <div class="panel-body">
                <div class="col-xs-5">
                    <div class="panel panel-UX">
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <td><label>Type</label></td>
                                    <td class="text-center"><label>Quantity</label></td>
                                    <td class="text-center"><label>Finished Ends</label></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Walls</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Wall->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Wall->FinishedEnds }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Bases</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Base->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Base->FinishedEnds }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Talls</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Tall->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Tall->FinishedEnds }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Vanities</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Vanity->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Vanity->FinishedEnds }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Panels</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Vanity->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Vanity->FinishedEnds }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Accessories</td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Accessory->Qty }}
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Accessory->FinishedEnds }}
                                    </td>
                                </tr>    
                                {{-- <tr>
                                    <td colspan="3" class="text-center">&nbsp;</td>
                                </tr>     --}}
                                <tr>
                                    <td><label>Total Cabinets</label></td>
                                    <td class="text-center">
                                        <label>{{ $job->product_categories->Cabinet->Qty }}</label>
                                    </td>
                                    <td class="text-center">
                                        <label>{{ $job->product_categories->Cabinet->FinishedEnds }}</label>
                                    </td>
                                </tr>                       
                                <tr>
                                    <td><label>Total Products</label></td>
                                    <td class="text-center">
                                        <label>{{ $job->product_categories->Total->Qty }}</label>
                                    </td>
                                    <td class="text-center">
                                        {{ $job->product_categories->Total->FinishedEnds }}
                                    </td>
                                </tr>
                            </tbody>
                            
                        </table>
                    </div>
                </div>

                <div class="col-xs-3">
                    <div class="panel panel-UX">
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <td><label>Fronts</label></td>
                                    <td class="text-center"><label>Quantity</label></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Doors</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('DoorCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Tall Doors</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('TallDoorCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Full Tall Doors</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('FullTallDoorCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Drawers</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('DrawerCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Large Drawers</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('LargeDrawerCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Overlays</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('OverlayCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Panels</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('PanelCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Total Fronts</label></td>
                                    <td class="text-center">
                                        <label>{{ $job->totalCount('TotalFrontCount') }}</label>
                                    </td>
                                </tr>
                            </tbody>
                        </table>                        
                    </div>
                </div>

                <div class="col-xs-4">
                    <div class="panel panel-UX">
                        <table class="table table-bordered table-hover table-condensed">
                            <thead>
                                <tr>
                                    <td><label>Hardware</label></td>
                                    <td class="text-center"><label>Quantity</label></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Handle/Pull</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('HandleCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Hinges</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('HingeCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Drawer Box</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('DrawerBoxCount') }}
                                    </td>
                                </tr>
                                <tr>
                                    <td>Large Drawer Box</td>
                                    <td class="text-center">
                                        {{ $job->totalCount('LargeDrawerBoxCount') }}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>