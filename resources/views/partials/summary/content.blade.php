@include('partials.summary.info_panels')

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-UX">
            @include('partials.list_table', ['editable'=>false])
        </div>
    </div>
</div>

@include('partials.summary.summary') 