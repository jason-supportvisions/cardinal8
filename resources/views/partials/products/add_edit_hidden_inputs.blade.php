<?php
$UOM                 = $job_product->getComponent('General')->UOM;
$Code                 = $job_product->getComponent('General')->Code;
$Type                = $job_product->getComponent('General')->Type;
$Category             = $job_product->getComponent('General')->Category;
$Class                 = $job_product->getComponent('General')->Class;
$Description         = $job_product->getComponent('General')->Description;
$CabinetList         = $job_product->getComponent('General')->CabinetList;
$Door                 = $job_product->getComponent('Door')->Qty;
// echo "DOOR DOOR: " . $job_product->getComponent('Door')->ListPer;
$TallDoor             = $job_product->getComponent('TallDoor')->Qty;
$FullTallDoor         = $job_product->getComponent('FullTallDoor')->Qty;
$Drawer             = $job_product->getComponent('Drawer')->Qty;
$LargeDrawer         = $job_product->getComponent('LargeDrawer')->Qty;
$Overlay            = $job_product->getComponent('Overlay')->Qty;
$Frame                = $job_product->getComponent('Frame')->Qty;
$DrawerBox            = $job_product->getComponent('DrawerBox')->Qty;
$DrwBoxList            = $job_product->getComponent('DrawerBox')->ListPer;
$LargeDrawerBox        = $job_product->getComponent('LargeDrawerBox')->Qty;
$AdjShelf            = $job_product->getComponent('Shelf')->Qty;
$FixedShelf            = $job_product->getComponent('Shelf')->Qty;
$Partition            = $job_product->getComponent('Partition')->Qty;
$Divider            = $job_product->getComponent('Divider')->Qty;
$Hinge                = $job_product->getComponent('Hinge')->Qty;
$Handle                = $job_product->getComponent('Handle')->Qty;
$HandleList            = $job_product->getComponent('Handle')->ListPer;
$EdgeBandingList    = $job_product->getComponent('EdgeBanding')->ListPer;
$FinIntList            = $job_product->getComponent('FinishedInterior')->ListPer;
$DrwBoxList            = $job_product->getComponent('DrawerBox')->ListPer;
$ModList            = $job_product->getModificationsTotal();
$AdminList          = $job_product->ListPer;

//FinEndList depends on qty of ends across three components
$FinEndList            = 0;
if ($job_product->getComponent('BaseEnd')->Qty > 0) {
    $FinEndList += $job_product->getComponent('BaseEnd')->ListPer;
}
if ($job_product->getComponent('TallEnd')->Qty > 0) {
    $FinEndList += $job_product->getComponent('TallEnd')->ListPer;
}
if ($job_product->getComponent('WallEnd')->Qty > 0) {
    $FinEndList += $job_product->getComponent('WallEnd')->ListPer;
}

?>

<div class="row hide">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading"><strong>HIDDEN INPUTS</strong></div>
            <div class="panel-body">
                <div class="col-xs-4">
                    <input value="<?php echo session('userid'); ?>" name="prodCreatedBy" id="prodCreatedBy" readonly="readonly" />
                    <input value="<?php echo $product_item_id; ?>" name="prodID" id="prodID" readonly="readonly" />
                    <input value="<?php echo $Code;  ?>" name="prodOrigCode" id="prodOrigCode" readonly="readonly" />
                    <input value="<?php echo $AdminList;  ?>" name="AdminList" id="AdminList" readonly="readonly" />
                    <input value="<?php echo $Class; ?>" name="prodClass" id="prodClass" readonly="readonly" />
                    <input value="<?php echo $Type; ?>" name="prodType" id="prodType" readonly="readonly" />
                    <input value="<?php echo $Type; ?>" name="Type" id="Type" readonly="readonly" />
                    <input value="<?php echo $UOM; ?>" name="prodUOM" id="prodUOM" readonly="readonly" />
                    <input value="<?php echo $Code; ?>" name="prodCode" id="prodCode" readonly="readonly" />
                    <input value="<?php echo $Category; ?>" name="prodCat" id="prodCat" readonly="readonly" />
                    <input value="<?php echo $TotalCalc; ?>" name="listPer" id="listPer" readonly="readonly" />
                    <input value="<?php echo $TotalCost; ?>" name="costPer" id="costPer" readonly="readonly" />
                    <input value="<?php echo $TotalCost; ?>" name="costTotal" id="costTotal" readonly="readonly" />
                    <input value="<?php echo $TotalCustomer; ?>" name="customerPer" id="customerPer" readonly="readonly" />
                    <input value="<?php echo $TotalCustomer; ?>" name="customerTotal" id="customerTotal" readonly="readonly" />
                    <textarea value="<?php echo $Description; ?>" name="prodDesc" id="prodDesc" readonly="readonly"><?php echo $Description; ?></textarea>

                    <input value="<?php echo $Door; ?>" class="form-control" name="DoorCount" id="DoorCount" readonly="readonly" />
                    <input value="<?php echo $TallDoor; ?>" class="form-control" name="TallDoorCount" id="TallDoorCount" readonly="readonly" />
                    <input value="<?php echo $FullTallDoor; ?>" class="form-control" name="FullTallDoorCount" id="FullTallDoorCount" readonly="readonly" />
                    <input value="<?php echo $Drawer; ?>" class="form-control" name="DrawerCount" id="DrawerCount" readonly="readonly" />
                    <input value="<?php echo $LargeDrawer; ?>" class="form-control" name="LargeDrawerCount" id="LargeDrawerCount" readonly="readonly" />
                    <input value="<?php echo $Overlay; ?>" class="form-control" name="OverlayCount" id="OverlayCount" readonly="readonly" />
                    <input value="<?php echo $Frame; ?>" class="form-control" name="FrameCount" id="FrameCount" readonly="readonly" />
                    <input value="<?php echo $DrawerBox; ?>" class="form-control" name="DrawerBoxCount" id="DrawerBoxCount" readonly="readonly" />
                    <input value="<?php echo $LargeDrawerBox; ?>" class="form-control" name="LargeDrawerBoxCount" id="LargeDrawerBoxCount" readonly="readonly" />
                    <input value="<?php echo $AdjShelf; ?>" class="form-control" name="AdjShelfCount" id="AdjShelfCount" readonly="readonly" />
                    <input value="<?php echo $FixedShelf; ?>" class="form-control" name="FixedShelfCount" id="FixedShelfCount" readonly="readonly" />
                    <input value="<?php echo $Partition; ?>" class="form-control" name="PartitionCount" id="PartitionCount" readonly="readonly" />
                    <input value="<?php echo $Divider; ?>" class="form-control" name="DividerCount" id="DividerCount" readonly="readonly" />
                    <input value="<?php echo $Hinge; ?>" class="form-control" name="HingeCount" id="HingeCount" readonly="readonly" />
                    <input value="<?php echo $Handle; ?>" class="form-control" name="HandleCount" id="HandleCount" readonly="readonly" />
                    <input value="<?php echo $HandleList; ?>" class="form-control" name="prodHandleList" id="prodHandleList" readonly="readonly" />
                    <input value="<?php echo $EdgeBandingList; ?>" class="form-control" name="prodEdgeBandingList" id="prodEdgeBandingList" readonly="readonly" />


                    <input value="<?php echo $FrontList; ?>" name="prodFrontList" id="prodFrontList" readonly="readonly" />
                    <input value="<?php echo $job_product->getComponent('Glass')->ListPer; ?>" name="prodGlassList" id="prodGlassList" readonly="readonly" />
                    <input value="<?php echo $DoorList; ?>" name="prodDoorList" id="prodDoorList" readonly="readonly" />
                    <input value="<?php echo $DrwBoxList; ?>" name="prodDrwBoxList" id="prodDrwBoxList" readonly="readonly" />
                    <input value="<?php echo $FinEndList; ?>" name="prodFinEndList" id="prodFinEndList" readonly="readonly" />
                    <input value="<?php echo $FinIntList; ?>" name="prodFinIntList" id="prodFinIntList" readonly="readonly" />
                    <input value="<?php echo $ModList; ?>" name="prodModList" id="prodModList" readonly="readonly" />
                    <input value="<?php echo $FinishedInteriorSqFt; ?>" name="ProdFinishedInteriorSqFt" id="ProdFinishedInteriorSqFt" readonly="readonly" />
                    <input value="<?php echo $CabinetList; ?>" name="prodOrigList" id="prodOrigList" readonly="readonly" />
                    <input value="<?php echo $OrigCost; ?>" name="prodOrigCost" id="prodOrigCost" readonly="readonly" />
                    <input value="<?php echo $OrigCustomer; ?>" name="prodOrigCustomer" id="prodOrigCustomer" readonly="readonly" />


                    <input value="<?php echo $DoorStyleID; ?>" name="prodDoorStyID" id="prodDoorStyID" readonly="readonly" />
                    <input value="<?php echo $DoorHardwareID; ?>" name="prodDoorHdwID" id="prodDoorHdwID" readonly="readonly" />
                    <input value="<?php echo $LargeDrawerStyleID; ?>" name="prodLgDrwStyID" id="prodLgDrwStyID" readonly="readonly" />
                    <input value="<?php echo $LargeDrawerHardwareID; ?>" name="prodLgDrwHdwID" id="prodLgDrwHdwID" readonly="readonly" />
                    <input value="<?php echo $DrawerStyleID; ?>" name="prodDrwStyID" id="prodDrwStyID" readonly="readonly" />
                    <input value="<?php echo $DrawerHardwareID; ?>" name="prodDrwHdwID" id="prodDrwHdwID" readonly="readonly" />
                    <input value="<?php echo $StyleMaterialID; ?>" name="prodStyMaterialID" id="prodStyMaterialID" readonly="readonly" />
                    <input value="<?php echo $StyleMaterialGroup; ?>" name="prodStyMaterialGroup" id="prodStyMaterialGroup" readonly="readonly" />
                    <input value="<?php echo $StyleColorID; ?>" name="prodStyColorID" id="prodStyColorID" readonly="readonly" />
                    <input value="<?php echo $StyleColorGroup; ?>" name="prodStyColorGroup" id="prodStyColorGroup" readonly="readonly" />
                    <input value="<?php echo $StyleFinishID; ?>" name="prodStyFinishID" id="prodStyFinishID" readonly="readonly" />
                    <input value="<?php echo $StyleFinishGroup; ?>" name="prodStyFinishGroup" id="prodStyFinishGroup" readonly="readonly" />
                    <input value="<?php echo $CabinetBoxMaterialID; ?>" name="prodCabinetBoxMaterialID" id="prodCabinetBoxMaterialID" readonly="readonly" />
                    <input value="<?php echo $CabinetBoxMaterialGroup; ?>" name="prodCabinetBoxMaterialGroup" id="prodCabinetBoxMaterialGroup" readonly="readonly" />
                    <input value="<?php echo $CabinetDrawerBoxID; ?>" name="prodCabinetDrawerBoxID" id="prodCabinetDrawerBoxID" readonly="readonly" />
                    <input value="<?php echo $FrontType; ?>" name="prodFrontType" id="prodFrontType" readonly="readonly" />
                    <input value="<?php echo $FrontLabel; ?>" name="prodFrontLabel" id="prodFrontLabel" readonly="readonly" />
                    <input value="<?php echo $FrontWidth; ?>" name="prodFrontWidth" id="prodFrontWidth" readonly="readonly" />
                    <input value="<?php echo $FrontHeight; ?>" name="prodFrontHeight" id="prodFrontHeight" readonly="readonly" />
                </div>
            </div>
        </div>
    </div>
    <?php
    //Fast Add 
    $addType  = session('addtype') ? session('addtype') : "";
    session(['addtype'=>'']);
    if ($addType == "fast") {
    ?>

        <script>
            document.getElementById("saveItem").click();
        </script>

    <?php
    } //end fast add 
    ?>