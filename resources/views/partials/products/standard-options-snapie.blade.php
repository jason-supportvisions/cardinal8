
<?php 
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

$glassBtnClass = "btn-primary";

	//we have to do some different things if we're on a SHOW blade versus a CREATE blade
	//if it's SHOW we have to pull the main vars from the job_product
    //if it's CREATE there are no records in job_product so we pull default from product or accessory item tables    

        //get productType variables from functions.php
            $itemArray          = getProductType($job_product);
            $productType		= $itemArray["productType"]; 
            $itemType 			= $itemArray["itemType"]; 
            $groupType 		    = $itemArray["groupType"]; 
            $pricingType 		= $itemArray["pricingType"]; 
            $noteType 			= $itemArray["noteType"]; 
            $itemTable 		    = $itemArray["itemTable"];  
            $groupTable 		= $itemArray["groupTable"]; 	
            // dd('dimensions.blade 20: ' . print_r($itemArray));P
        
            $fullCode =  getORDCode($job_product);  //format code to machine readable with dimensions
            $Code = $job_product->$itemType->Code;
            $Code = getORDCodeClean($Code);
            //echo "<br>s - job_product->$itemType: " . "->Code: $Code";
            //echo "<br>s - job_product->Code: " . ($job_product->Code);
            //echo "<br>s - getORDCodeClean($Code): " . ($Code);
            //echo "<br>s - groupType: " . ($groupType);



        //Get the proper width depending on if we're editing or creating it from scratch
		if(!isset($_GET["product_item_id"])){
			$pageType               = "show";
            $productType            = $job_product->Type;
            $Width                  = $job_product->Width; 
			$Height                 = $job_product->Height; 
            $Depth                  = $job_product->Depth; 
            // echo "<br>I'm an existing product <br>";
            //dd('dimensions.php: 17 -' . $job_product->$itemType);

		}else{
			$pageType               = "create";
            $productType            = $_GET['product_item_id'];
			$Width                  = $job_product->$itemType->Width; 
			$Height                 = $job_product->$itemType->Height; 
            $Depth                  = $job_product->$itemType->Depth; 
            
            // echo "<br>I'm a NEW product <br>";
            // echo ('standard options 41 ' . $job_product->ListQty . '<br>');
        }
    
    //dd($job_product);
        $sqftTotal = ($Width * $Height) / 144;
        if($job_product->$itemType->$groupType){
            $StdOptions             = $job_product->$itemType->$groupType->StdOptions;
        }else{
            $StdOptions             = 0;
        }

    
    //hide this section if it's an accessory
    if ($StdOptions == "1" ) {        
        $txtAreaRows = 4; //default notes text area size
        $hideClass = "";
    }else{
        $txtAreaRows = 7; //make notes field bigger if accessory
        $hideClass = "visibility: hidden";
    }
    
    //echo $hideClass . " : " . $StdOptions;
    //print_r($job_product);
    

    //only show glass option button if WALL is selected
    if(isset($job_product->$groupType->GlassOption)){

        //vars
        $showTotal = "";
        $intTotal = 0;

        //if there are glass options chosen enable the button green instead of blue
        if(isset($job_product->FrontSelection)){
            
            //format number
            $intTotal = number_format($job_product->getComponent('Glass')->ListPer, 2, '.', ','); 

           if($job_product->FrontSelection == "All Doors" || $job_product->FrontSelection == " All Doors"){
               $glassBtnClass = "btn-success";
               $showTotal = ' -- $' . $intTotal;
            }else{
                $glassBtnGlass = "btn-primary";
                $showTotal = "";
            }

        }

        

        

        //only show items that offer glass swap
        if($job_product->$groupType->GlassOption == "No" || $job_product->$groupType->GlassOption == ""){
            $glassButton = '<br><small>* Glass options not available for the ' .  $Code . '</small>'; 
        }else{
            $glassButton = '<button type="button" class="btn ' . $glassBtnClass . '" data-toggle="modal" data-target="#Glass" onclick="check_fronsel_change()">Glass Options'.$showTotal.'</button>';
        }

    }else{
        $glassButton = '';
    }

?>
<div class="row">
    <div class="col-xs-12 col-sm-12">
    <div class="panel panel-UX">
    <div class="panel-body">
    <div class="row">

        <div class="col-md-7">
        <div class="form-horizontal">
            <div class="{{ $hideClass }}">
                    <div class="form-group">
                        <label for="Hinging" class="col-sm-3 control-label">Hinging</label>
                        <div class="col-sm-3">
                        <?php 
                            //echo "hinge " . $job_product->Type . "<br>";
                            $hinge_options = $job_product->$itemType->$groupType->hinge_options;
                            //print_r($hinge_options);
                           // echo "<br>";
                            //print_r($job_product->$groupType->hinge_options);
                            //foreach($job_product->$groupType->hinge_options as $hingeOption){
                              //  echo "<br>hingeOption: " . $hingeOption;
                            //}
                        ?>
                            {{ Form::select('Hinging', $job_product->$itemType->$groupType->hinge_options, $job_product->Hinging, ['id'=>'hinging-select', 'class'=>'form-control']) }}
                        </div>
                        <div class="col-sm-6">
                            <div class="pull-right">
                            
                            <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Front">Front Options</button> -->

                            <?php echo $glassButton; ?>
                    
                            </div>
                        </div>
                    </div>
                  
                    <?php 
                            
                    //print_r($job_product->getComponent("FinishedInterior")->Qty); 
                    //getFinIntList("B10.5");
                    ?>

                    <div class="modal" tabindex="-1" role="dialog">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Modal title</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Modal body text goes here.</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary">Save changes</button>
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                        </div>
                    </div>
                    </div>

                    <div class="form-group">
                        <label for="prodFinishedEnd" class="col-sm-3 control-label">Finished End(s)</label>
                        <div class="col-sm-3">
                            {{ Form::select('FinishedEnd', $job_product->$itemType->$groupType->finished_end_options, $job_product->FinishedEnd, ['class'=>'form-control']) }}
                        </div>
                        <label for="prodFinishedInterior" class="col-sm-3 control-label">Finished Interior</label>
                        <div class="col-sm-3">
                            {{ Form::select('FinishedInterior', ['No'=>'No', 'Yes'=>'Yes'], $job_product->FinishedInterior, ['class'=>'form-control']) }}
                        </div>
                    </div>

                </div>

            <div class="form-group">
                <label for="prodNotes" class="col-sm-3 control-label">Notes</label>
                <div class="col-sm-9">
                <textarea type="text" value="" rows="{{ $txtAreaRows }}" class="form-control" name="Notes" id="prodNotes" placeholder="Additional Notes">{{ $job_product->Notes }}</textarea>
                </div>
            </div>
        </div>
        </div>
        <div class="col-md-1">
            <!-- SPACER -->
        </div>
        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-2 control-label">Code</label>
                    <div class="col-sm-7">
                        <input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCodeFull" id="prodCodeFull" readonly="readonly">
                        <input type="hidden" value="<?php echo $Code; ?>" class="form-control" name="Code" id="Code" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="prodUOM" class="col-sm-2 control-label">Units</label>
                    <div class="col-sm-7"><input id="prodUOM" type="text" value="<?php echo $job_product->$itemType->$groupType->UOM; ?>" class="form-control" readonly="readonly" name="prodUOM"></div>  
                </div>
                
                
                        
                <div class="form-group">
                    <label for="listTotal" class="col-sm-2 control-label">Per</label>
                    <div class="col-sm-7">
                        
                            <?php

                            //Factory Quote override
                                if($job_product->$itemType->$groupType->FactoryQuote != 0 && $job_product->CustomList == 0){
                                    echo '<div class="alert alert-danger">The price below is only a gross estimate. A factory quote is needed.</div>';
                                }elseif($job_product->$itemType->$groupType->FactoryQuote != 0 && $job_product->CustomList != 0){
                                    echo '<div class="alert alert-success">The price below is the official quote.</div>';
                                }
                            ?>

                        <div class="input-group">
                            
                            <span class="input-group-addon">$</span>
                            <?php 
                                //if($job_product->ComponentList > 0){
                                //    $showList = $job_product->OrigList;
                                //}else{
                                //if($job_product->ListPer > 0){

                                 //   $showList =  $job_product->ListPer; 
                                    
                                 //  if($job_product->ComponentList > 0){
                                 //      $showList = $job_product->ComponentList;
                                  //  } 
                                
                                // }else{
                                    $componentList =  $job_product->getComponentsTotal();
                               // }

                                $showList = $componentList + $job_product->getModificationsTotal();
                                //echo "<br>getComponentsTotal()" . $job_product->getComponentsTotal();
                                 //echo "<br>getModificationsTotal()" . $job_product->getModificationsTotal();
                                //echo "<br>showList()" . $showList;

                                //}

                                //Factory Quote
                                if($job_product->CustomList > 0){
                                    $showList = $job_product->CustomList; 
                                }

                                
                            ?>
                            <input
                                type="text"
                                value="{{ number_format($showList,2) }}"
                                style="text-align:right;"
                                class="form-control"
                                name="listTotal"
                                id="listTotal" 
                                readonly="readonly"
                            >
                            <input
                                type="hidden"
                                value="{{ number_format($componentList,2) }}"
                                style="text-align:right;"
                                class="form-control"
                                name="componentList"
                                id="componentList" 
                                readonly="readonly">
                                
                            <input
                                type="hidden"
                                id="adminCustomList"
                                name="adminCustomList"
                                value="{{ number_format($job_product->CustomList, 2) }}"
                                class="form-control"
                                min="0"
                                step="1"
                            >

                            <input
                                type="hidden"
                                id="AdminList"
                                name="AdminList"
                                value="{{  number_format($job_product->ListPer, 2) }}"
                                class="form-control"
                                min="0"
                                step="1"
                            >
                                
                        </div>
                    </div>
                </div>
            <?php 
                if ($job_product->Qty > 1){
            ?>
                <div class="form-group">
                    <label for="listTotal" class="col-sm-2 control-label">Total<?php echo "(" . $job_product->Qty . ") "; ?></label>
                    <div class="col-sm-7">
                        <div class="input-group">
                            <span class="input-group-addon">$</span>
                            <input
                                type="text"
                                value="{{ number_format($job_product->ListQty,2) }}"
                                style="text-align:right;"
                                class="form-control"
                                name="listTotal"
                                id="listTotal" 
                                readonly="readonly"
                            >
                        </div>
                    </div>
                </div> 
            <?php
            }
            ?>
            </div>
            <div class="text-center"><small>Price changes calculated after save</small></div>
            
        </div>
    </div>
    </div>
    </div>
    </div>
</div>


<!--
<div class="row">
    <div class="col-xs-12 col-sm-12">
    <div class="panel panel-UX">
    <div class="panel-heading" style="background: MAROON;">
    <strong><span style="color: #ffffff;">DEBUG </span></strong> 
    </div>
    <div class="panel-body">
    <div class="row">

        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">GroupCode</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $job_product->$itemType->GroupCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">Code</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $job_product->$itemType->Code; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">TotalCalc</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">TotalList</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
        </div>





        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">Width</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">Height</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">SqftMath</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">Sided</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
        </div>








        <div class="col-md-4">
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label">Description</label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $job_product->$itemType->Description; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label"></label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label"></label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
            <div class="form-horizontal">
                <div class="form-group">
                    <label for="prodCode" class="col-sm-5 control-label"></label>
                    <div class="col-sm-7"><input type="text" value="<?php echo $fullCode; ?>" class="form-control" name="prodCode" id="prodCode" readonly="readonly"></div>
                </div>
            </div>
        </div>







            
        </div>
    </div>
    </div>
    </div>
    </div>
</div> 
-->