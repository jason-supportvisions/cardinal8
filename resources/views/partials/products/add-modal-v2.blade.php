<?php 
    use Illuminate\Support\Facades\DB;
?>
<style>
    table.dataTable thead .sorting,
    table.dataTable thead .sorting_asc,
    table.dataTable thead .sorting_desc {
        background: none;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body"><button type="button" class="close pull-right btn btn-primary" data-dismiss="modal">&times;</button>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#stdcab" id="stdcabbutton" aria-controls="stdcab" role="tab" data-toggle="tab">Standard Cabinets</a></li>
                    <li role="presentation"><a href="#dartmouth" id="dartmouthbutton" aria-controls="dartmouth" role="tab" data-toggle="tab">Dartmouth</a></li>
                    <li role="presentation"><a href="#spectra" id="spectrabutton" aria-controls="spectra" role="tab" data-toggle="tab">Spectra</a></li>
                    <li role="presentation"><a href="#accessories" id="accessbutton" aria-controls="accessories" role="tab" data-toggle="tab">Accessories</a></li>
                    {{-- <li role="presentation"><a href="#appliances" id="applbutton" aria-controls="dartmouth" role="tab" data-toggle="tab">Appliance Panels</a></li>
        <li role="presentation"><a href="#misc" id="miscbutton" aria-controls="dartmouth" role="tab" data-toggle="tab">Misc</a></li> --}}
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="stdcab">
                        <div class="table-responsive">
                            <table id="product_std" class="table table-striped table-products" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <select class="input input-sm form-control" style="width: 100%" id="stdcabcat" name="stdcabcat">
                                                <option value="">All Products</option>
                                                <?php                                              
                                                $stdcabcat = DB::select(DB::raw("SELECT * FROM (SELECT DISTINCT CASE WHEN product_groups.Type = NULL THEN product_groups.Category  WHEN product_groups.Type = '' THEN product_groups.Category ELSE CONCAT(product_groups.Category,' - ',product_groups.Type) END AS DROPDOWN, product_groups.Category, product_groups.Type FROM product_items INNER JOIN product_groups ON product_items.GroupCode = product_groups.GroupCode WHERE product_groups.ClassGroup = 'Standard' ORDER BY  FIELD(product_groups.Category,'Wall','Base','Tall','Vanity','Accessory',NULL,''), Type) AS C"));
                                                foreach ( $stdcabcat as $row ) {
                                                    echo "<option value=\"" . $row->DROPDOWN . "\">" . $row->DROPDOWN . "</option>";
                                                };
                                                ?>
                                            </select>
                                        </th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Item Code" id="stdcabcode" name="stdcabcode"></th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Description" id="stdcabdesc" name="stdcabdesc"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="accessories">
                        <div class="table-responsive">
                            <table id="access_std" class="table table-striped table-products" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <select class="input input-sm form-control" style="width: 100%" id="accesscat" name="accesscat">
                                                <option value="">All Accessories</option>
                                                <?php                                                
                                                $accesscat = DB::select(DB::raw("SELECT * FROM (SELECT DISTINCT CASE WHEN accessory_groups.Type = NULL THEN accessory_groups.Category WHEN accessory_groups.Type = '' THEN accessory_groups.Category ELSE accessory_groups.Type END AS DROPDOWN, accessory_groups.Category, accessory_groups.Type FROM accessory_items INNER JOIN accessory_groups ON accessory_items.GroupCode = accessory_groups.GroupCode ORDER BY FIELD(accessory_groups.Category,'Wall','Base','Tall','Vanity','Accessory',NULL,''), accessory_groups.Type) AS C"));
                                                foreach ( $accesscat as $row) {
                                                    echo "<option value=\"" . $row->DROPDOWN . "\">" . $row->DROPDOWN . "</option>";
                                                };
                                                ?>
                                            </select>
                                        </th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Item Code" id="accesscode" name="accesscode"></th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Description" id="accessdesc" name="accessdesc"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="spectra">
                        <div class="table-responsive">
                            <table id="spectra_std" class="table table-striped table-products" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <select class="input input-sm form-control" style="width: 100%" id="spectracat" name="spectracat">
                                                <option value="">All Products</option>
                                                <?php
                                                $speccabcat = DB::select(DB::raw("SELECT * FROM (SELECT DISTINCT CASE WHEN product_groups.Type = NULL THEN product_groups.Category  WHEN product_groups.Type = '' THEN product_groups.Category ELSE CONCAT(product_groups.Category,' - ',product_groups.Type) END AS DROPDOWN, product_groups.Category, product_groups.Type FROM product_items INNER JOIN product_groups ON product_items.GroupCode = product_groups.GroupCode WHERE product_groups.ClassGroup = 'Spectra' ORDER BY  FIELD(product_groups.Category,'Wall','Base','Tall','Vanity','Accessory',NULL,''), Type) AS C"));
                                                foreach ( $speccabcat as $row) {
                                                    echo "<option value=\"" . $row->DROPDOWN . "\">" . $row->DROPDOWN . "</option>";
                                                };
                                                ?>
                                            </select>
                                        </th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Item Code" id="spectracode" name="spectracode"></th>
                                        <th class="" width=""><input type="search" style="width:100%;" class="form-control input" placeholder="Description" id="spectradesc" name="spectradesc"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="dartmouth">
                        <div class="table-responsive">
                            <table id="dartmouth_std" class="table table-striped table-products" width="100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>
                                            <select class="input input-sm form-control" style="width: 100%" id="dartmouthcat" name="dartmouthcat">
                                                <option value="">All Products</option>
                                                <?php 
                                                $dartcabcat = DB::select(DB::raw("SELECT * FROM (SELECT DISTINCT CASE WHEN product_groups.Type = NULL THEN product_groups.Category  WHEN product_groups.Type = '' THEN product_groups.Category ELSE CONCAT(product_groups.Category,' - ',product_groups.Type) END AS DROPDOWN, product_groups.Category, product_groups.Type FROM product_items INNER JOIN product_groups ON product_items.GroupCode = product_groups.GroupCode WHERE product_groups.ClassGroup = 'Dartmouth' ORDER BY FIELD(product_groups.Category,'Wall','Base','Tall','Vanity','Accessory',NULL,''), Type) AS C"));
                                                foreach ($dartcabcat as $row) {
                                                    echo "<option value=\"" . $row->DROPDOWN . "\">" . $row->DROPDOWN . "</option>";
                                                };
                                                ?>
                                            </select>
                                        </th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Item Code" id="dartmouthcode" name="dartmouthcode"></th>
                                        <th><input type="search" style="width:100%;" class="form-control input" placeholder="Description" id="dartmouthdesc" name="dartmouthdesc"></th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="hidden-md hidden-lg"> <br><br> </div>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>