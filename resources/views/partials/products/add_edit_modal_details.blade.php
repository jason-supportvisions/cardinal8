<?php
use Illuminate\Support\Facades\DB;
//need to pass a job product ID here and get an object based on that. 
$noProduct = FALSE;

if (!isset($job_product)) {

    if (isset($product)) {
        $job_product = $product;
    } else {
        $noProduct = TRUE;
    }
} else {
}

if ($noProduct != TRUE) {
    
?>
    <style>
        .modal-lg {
          
            width: 800px;
            margin: auto;
          
        }

        .highlite {
            color: black;
        }

        .dim {
            color: grey;
        }
    </style>
    <div id="Detail" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>Product (Per) Details:</strong>
                        &nbsp;&nbsp;&nbsp; <?php if (isset($job_product->Code)) {
                                                echo $job_product->Code;
                                            } ?>
                        &nbsp;&nbsp;&nbsp;<small><?php if (isset($CVCode)) {
                                                        echo $CVCode;
                                                    } ?></small></h4>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th style="width:25%;" class="text-center"><label></label></th>
                                    <th style="width:10%" class="text-center"><label>Qty</label></th>
                                    <th style="width:10%" class="text-center"><label>Style</label></th>
                                    <th style="width:10%" class="text-center"><label>Material</label></th>
                                    <th style="width:10%" class="text-center"><label>Color</label></th>
                                    <th style="width:10%" class="text-center"><label>Finish</label></th>
                                    <th style="width:15%" class="text-center"><label>List (Per)</label></th>
                                    <th style="width:15%" class="text-center"><label>List (Qty)</label></th>
                                  
                                    <th style="width:5%" class="text-center">
                                        <button type="button" class="btn btn-xs btn-primary" style="width:100%;" data-toggle="modal" data-target="#Parts">Parts</button>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>                               
                                <?php
                                $ShelfComponentQty = $job_product->getComponent("Shelf")->Qty;                              
                                foreach ($job_product->components as $component) :
                                    $StyleType = "";
                                    if ($component->name == "Specification") {                               

                                    } else {                                       
                                        if ($component->ListQty > 0) {
                                            $highlite = " highlite";
                                        } else {
                                            $highlite = " dim";
                                        }                                       
                                        if ($component->name == "Door") {                                        
                                            $StyleType = "(<b>" . $component->StyleType . "</b>) "; 
                                        } elseif ($component->name == "TallDoor") {
                                              $StyleType = "(<b>" . $component->StyleType . "</b>) ";
                                        } elseif ($component->name == "FullTallDoor") {                                      
                                            $StyleType = "(<b>" . $component->StyleType . "</b>) ";
                                        } elseif ($component->name == "Glass") {                                            
                                            $StyleType = "(<b>" . $component->StyleType . "</b>) "; 
                                        }
                                        $component->StyleList = isset($component->StyleList)?$component->StyleList:0;
                                        $component->MaterialList = isset($component->MaterialList)?$component->MaterialList:0;
                                        $component->ColorList = isset($component->ColorList)?$component->ColorList:0;
                                        $component->FinishList = isset($component->FinishList)?$component->FinishList:0;
                                        $OrigCode = isset($OrigCode) ? $OrigCode : 0;
                                ?>
                                        <tr>
                                            <td class="text-right"><?php echo $StyleType; ?><?= $component->name ?></td>
                                            <td>
                                                <input value="<?php echo $component->Qty; ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                            <td>
                                                <input value="<?php echo number_format($component->StyleList, 2); ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                            <td>
                                                <input value="<?php echo number_format($component->MaterialList, 2); ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                            <td>
                                                <input value="<?php echo number_format($component->ColorList, 2); ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                            <td>
                                                <input value="<?php echo number_format($component->FinishList, 2); ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                        
                                            <td>
                                                <input value="<?= number_format($component->ListPer, 2) ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                                            <td>
                                                <input value="<?= number_format($component->ListQty, 2) ?>" class="form-control input-sm text-right<?= $highlite ?>" readonly="readonly">
                                            </td>
                           
                                            <td>
                                                <?php
                                                if ($component->Qty != 0) {
                                                    echo "<img src='/images/check-blue.png'>";
                                                } else {
                                                    echo  "<span class='badge badge-secondary'></span>";
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                    <?php } //end if 
                                    ?>
                                <?php endforeach ?>

                                <tr>
                                    <td class="text-right"><label>TOTAL</label></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td colspan="2">
                                        <input value="<?= number_format($job_product->ListPer, 2) ?>" style="text-align:right;" class="form-control input-sm highlite" readonly="readonly">
                                    </td>
                                  
                                    <td></td>

                                </tr>
                            </tbody>
                        </table>
                    </div>

                    <div class="table-responsive">
                        <table class="table table-hover table-striped table-condensed">
                            <thead>
                                <tr>
                                    <th style="width:10%;" class="text-center"><label></label></th>
                                    <th style="width:10%;" class="text-center"><label>Quantity</label></th>
                                    <th style="width:10%;" class="text-center"><label></label></th>
                                    <th style="width:10%;" class="text-center"><label>Quantity</label></th>
                                    <th style="width:10%;" class="text-center"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-right"><label>Adjustable Shelf: </label></td>
                                    <td><input type="number" value="<?php if (isset($ShelfComponentQty)) {
                                                                        echo $ShelfComponentQty;
                                                                    } ?>" class="form-control input-sm" name="prodAdjShelfQty" id="prodAdjShelfQty" readonly="readonly" /></td>
                                    <td class="text-right"><label>Fixed Shelf</label></td>
                                    <td><input type="number" value="<?php if (isset($FixedShelf)) {
                                                                        echo $FixedShelf;
                                                                    } ?>" class="form-control input-sm" name="prodFixedShelfQty" id="prodFixedShelfQty" readonly="readonly" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><label>Partition</label></td>
                                    <td><input type="number" value="<?php if (isset($Partition)) {
                                                                        echo $Partition;
                                                                    } ?>" class="form-control input-sm" name="prodPartitionQty" id="prodPartitionQty" readonly="readonly" /></td>
                                    <td class="text-right"><label>Divider</label></td>
                                    <td><input type="number" value="<?php if (isset($Divider)) {
                                                                        echo $Divider;
                                                                    } ?>" class="form-control input-sm" name="prodDividerQty" id="prodDividerQty" readonly="readonly" /></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-right"><label>Hinge</label></td>
                                    <td><input type="number" value="<?php if (isset($Divider)) {
                                                                        echo $Hinge;
                                                                    } ?>" class="form-control input-sm" name="prodHingeQty" id="prodHingeQty" readonly="readonly" /></td>
                                    <td class="text-right">
                                        <!-- <label>Misc</label> -->
                                    </td>
                                    <td>
                                        <!-- <input type="number" value="" class="form-control input-sm" name="" id="" readonly="readonly"/> -->
                                    </td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div id="Parts" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><strong>Product (Per) Details:</strong>
                        &nbsp;&nbsp;&nbsp; <?php if (isset($Code)) {
                                                echo $Code;
                                            } ?>
                        &nbsp;&nbsp;&nbsp;<small><?php if (isset($CVCode)) {
                                                        echo $CVCode;
                                                    } ?></small></h4>
                </div>
                <div class="modal-body">
                    <?php                    
                        if ($add_or_edit == 'edit') {
                            $PartsQuery  = DB::table("product_parts")->SELECT ('*')->where('Code', '=', $OrigCode)->get();
                        } else {
                            $PartsQuery  = DB::table("product_parts")->where('Code', '=', $Code)->get();
                        }

                        echo "<div class=\"table-responsive\">";
                        echo "<table class=\"table table-hover table-striped table-condensed\">";
                        echo "<thead>";
                        echo "<tr>";
                        echo "<th>Class</th>";
                        echo "<th>Type</th>";
                        echo "<th>Description</th>";
                        echo "<th>Width</th>";
                        echo "<th>Height</th>";
                        echo "<th>Depth</th>";
                        echo "<th>Qty</th>";
                        echo "<th>UOM</th>";                     
                        echo "<th>ListPer</th>";
                        echo "<th>ListQty</th>";
                        echo "</tr>";
                        echo "</thead>";

                        echo "<tbody>";

                        foreach($PartsQuery as $row) {
                            echo "<tr>";
                            echo "<td>" . $row->Class . "</td>";
                            echo "<td>" . $row->Type . "</td>";
                            echo "<td>" . $row->Description . "</td>";
                            echo "<td>" . (float)$row->Width . "</td>";
                            echo "<td>" . (float)$row->Height . "</td>";
                            echo "<td>" . (float)$row->Depth . "</td>";
                            echo "<td>" . (float)$row->Qty . "</td>";
                            echo "<td>" . $row->UOM . "</td>";                        
                            echo "<td>" . $row->ListPer . "</td>";
                            echo "<td>" . $row->ListQty . "</td>";
                            echo "</tr>";
                        }
                        echo "</tbody>";
                        echo "</table>";
                        echo "</div>";            
                    ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
<?php } 
?>