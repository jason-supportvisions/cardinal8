@include('templates.function')
<?php
$editable = isset($editable) ? $editable : true;
$stageHide = "";
$noteDisplay = "";
$showAlert = "";
$showAdminButtons = "";
$groupNotes = "";
$itemNotes = ""; 
$noteDisplay = "";
$showTooltip = "";
$glassSuffix = "";
$stageID = $job->stage->ID;
$modificationTotals = 0;
$stageName = $job->stage->Name;

if ($stageID == 4 || $stageID == 5) {
    $stageHide = TRUE;
} else {
    $stageHide = FALSE;
}

?>
        <script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/datetime-moment.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/datatables.min.js"></script>

<script>
    $(document).ready(function($) {
        $(".table-row").click(function() {
            window.document.location = $(this).data("href");
        });
    });
</script>


<style>
    .table-row {
        cursor: pointer;
    }

    .color-tooltip+.tooltip>.tooltip-inner {
        background-color: #2E6DA3;
    }

    .color-tooltip+.tooltip>.tooltip-arrow {
        border-bottom-color: #2E6DA3;
    }

    .color-tooltip.show {
        text-align: left;
    }

    .tooltip-inner {
        text-align: left;
    }
</style>

<style>
    ul {
        margin-left: 5px;
        margin-right: 0px;
        padding-left: 10px;
        padding-right: 0px;
    }
</style>
<table class='table table-condensed table-hover' id='table'>
    
    <tr class="active">
            <th class="text-center" style="width:4%;white-space:nowrap;border-right:1px solid #ddd;"></th>
            <th class="text-center" style="width:4%;white-space:nowrap;border-right:1px solid #ddd;">Qty</th>
            <th style="width:25%;white-space:nowrap;border-right:1px solid #ddd;">Code<br><small>W x H x D</small></th>
            <th style="width:12%;white-space:nowrap;border-right:1px solid #ddd;">Hinging<br>Fin. End(s)</th>
            <th style="border-right:1px solid #ddd;">Description</th>
            <th class="price-td LIST text-center @if($pricing!='LIST' || $stageHide == TRUE) hide @endif" style="width:7.5%;white-space:nowrap;border-right:1px solid #ddd;">List<br>(Per)</th>
            <th class="price-td LIST text-center @if($pricing!='LIST'|| $stageHide == TRUE) hide @endif" width="7.5%">List<br>(Qty)</th>
            <th class="price-td COST text-center @if($pricing!='COST' || $stageHide == TRUE) hide @endif" style="width:7.5%;white-space:nowrap;border-right:1px solid #ddd;">Net<br>(Per)</th>
            <th class="price-td COST text-center @if($pricing!='COST' || $stageHide == TRUE) hide @endif" style="width:7.5%;white-space:nowrap;border-right:1px solid #ddd;">Net<br>(Qty)</th>
            <th class="price-td CUSTOMER text-center @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif" style="width:7.5%;white-space:nowrap;border-right:1px solid #ddd;">Customer<br>(Per)</th>
            <th class="price-td CUSTOMER text-center @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif" style="width:7.5%;white-space:nowrap;border-right:1px solid #ddd;">Customer<br>(Qty)</th>
            @if($editable)
            <th style="min-width: 105px; width:10%;">action</th>
            @endif
        </tr>
    
    <tbody>
        @foreach($job->products as $product)
        <?php

            $itemArray          = getProductType($product);
            $productType        = $itemArray["productType"];
            $itemType             = $itemArray["itemType"];
            $groupType             = $itemArray["groupType"];
            $pricingType         = $itemArray["pricingType"];
            $noteType             = $itemArray["noteType"];
            $itemTable             = $itemArray["itemTable"];
            $groupTable         = $itemArray["groupTable"];

            if(isset($product->$itemType->$groupType)){

                $showAlert = "";
                //managerButton to edit job product
                if($job->user->AccountType == -1){
                    $showAdminURL = config('app.manager_url') . "JobProductEdit/".$product->ID. "/" .$product->JobID ."/". $product->Line;
                    $showAdminButtons = '<a href="' . $showAdminURL . '" class="btn btn-xs btn-primary" target="_BALNK"><i class="glyphicon glyphicon-star"></i></a>';
                }else{
                    $showAdminURL = "";
                    $showAdminButtons = "";
                }

                //Factory quote without customList price 
                if ($product->$itemType->$groupType->FactoryQuote != 0  && $product->CustomList == 0) {
                    $money          = "no";
                    $factorySuccess = "no";
                    $showAlert      = "danger";
                }elseif($product->$itemType->$groupType->FactoryQuote != 0  && $product->CustomList != 0){
                    $money          = "no";
                    $factorySuccess = "yes";
                    $showAlert      = "success";
                }else{
                    // $showAlert      = "";
                    // $showAdminButtons = "";
                }

                //Admin quote with pricing 
                if ($product->AdminList != 0) {
                    $money          = "no";
                    $factorySuccess = "no";
                    $showAlert      = "success";
                }

            }
        ?>
        <tr rel='tooltip' 
            data-placement='bottom' 
            data-html='true' 
            data-animation='true' 
            data-delay='{"show":"150", "hide":"300"}' 
            class='table table-hover color-tooltip <?php echo $showAlert; ?>' data-href='/quotes_orders/{{$job->JobID}}/products/{{$product->ID}}' <?php //echo $showTooltip; ?>>
            
            <td>
            @if($editable)
                {{ Form::open(array('route' => array('product-line'))) }}
                {{ Form::hidden('Line', null, ['class'=>'line-input'])}}
                {{ Form::hidden('jobID', $job->JobID) }}
                {{ Form::hidden('productID', $product->ID) }}
                    <div class="btn-group">
                        <button type="button" class="btn btn-sm btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ $product->Line }} <span class="caret"></span></button>                      
                        <ul class="dropdown-menu" product-id = " {{ $product->ID }} ">

                            @foreach($job->products as $product_inner)
                                @if($product->ID == $product_inner->ID)
                                    <li class="disabled list-group-item-danger">
                                        <a>{{ $product_inner->Line }}</a>
                                    </li>
                                @else
                                    <li>
                                        <a class="line-option">
                                            {{ $product_inner->Line }}
                                        </a>
                                    </li>
                                @endif
                            @endforeach
                        
                        </ul>
                    </div>
                {{ Form::close() }}
                @else
                {{ $product->Line }}
                @endif
            </td>
            <td class="table-row">
                {{ $product->Qty }}
            </td>
                 
            <td style="white-space:nowrap;border-right:1px solid #ddd;">
                <strong>{{ getORDCode($product) }}</strong><br>
            </td>
        
            <td style="white-space:nowrap;border-right:1px solid #ddd;">
            <small>
                H: {{ $product->HingeCode }}<br>
                F: {{ $product->FinishedEnd }}
                </small>
            </td>
            <td style="border-right:1px solid #ddd;">
            {{ $product->Description }}
            <br />
            <span><small><?php echo nl2br($product->Notes); ?> </small></span>

            </td>
            <td class="price-td LIST text-right @if($pricing!='LIST') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                ${{ number_format($product->ListPer,2) }}
               
                @if($product->has_mods) 
                    <br><small>w/ mod</small>
                @endif
            </td>
           
            <td class="price-td LIST text-right @if($pricing!='LIST') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                ${{ number_format($product->ListQty,2) }}
                @if($product->has_mods)
                    <br><small>w/ mod</small>
                @endif
            </td>
            <td class="price-td COST text-right @if($pricing!='COST') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                ${{ number_format($product->CostPer,2) }}
                @if($product->has_mods)
                    <br><small>w/ mod</small>
                @endif
            </td>
            <td class="price-td COST text-right @if($pricing!='COST') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                ${{ number_format($product->CostQty,2) }}
                @if($product->has_mods)
                    <br><small>w/ mod</small>
                @endif
            </td>
            <td class="price-td CUSTOMER text-right @if($pricing!='CUSTOMER') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                ${{ number_format($product->CustomerPer,2) }}
                @if($product->has_mods)
                    <br><small>w/ mod</small>
                @endif
            </td>
            <td class="price-td CUSTOMER text-right @if($pricing!='CUSTOMER') hide @endif" style="white-space:nowrap;border-right:1px solid #ddd;">
                {{ number_format($product->CustomerQty,2) }}
                @if($product->has_mods)
                    <br><small>w/ mod</small>
                @endif
            </td>
            

            <?php //} ?>

            <?php 
            //managerButton to edit job product
            if($job->user->AccountType == -1){
                $showAdminURL = config('app.manager_url'). "JobProductEdit/".$product->ID. "/" .$product->JobID ."/". $product->Line;
                $showAdminButtons = '<a href="' . $showAdminURL . '" class="btn btn-xs btn-primary"  target="_BLANK"><i class="glyphicon glyphicon-star"></i></a>';
            }else{
                $showAdminURL = "";
                $showAdminButtons = "";
            }

            ?>
            @if($editable)
                <td class="text-right" style="padding-left: 0px; padding-right: 0px;">                
                    <span class="btn-group" style="margin-right: 8px">
                        <a class="btn btn-warning btn-xs" title="Edit" href="{{ route('show-product',['jobID'=>$product->JobID, 'job_product_id'=>$product->ID]) }}" data-primary="1" data-task="edit">
                            <i class="glyphicon glyphicon-edit "></i>
                        </a>
                        <a class="btn btn-success btn-xs" title="Copy" href="{{ route('copy-product',['jobID'=>$product->JobID,'productID'=>$product->ID]) }}" data-primary="1" data-task="copy">
                            <i class="glyphicon glyphicon-copy "></i>
                        </a>
                        <a href="{{ route('delete-product', ['JobID'=>$product->JobID,'product_item_id'=>$product->ID]) }}" class="btn btn-xs btn-danger" onclick="return confirm('Do you really want remove this product entry?') ">
                            <i class="glyphicon glyphicon-remove"></i>
                        </a>
                        {!! htmlspecialchars_decode(nl2br(e($showAdminButtons))) !!}
                    </span>
                </td>
            @endif

        </tr>
        
            @if($product->has_finished_interior)
            <tr class="warning">
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                    **<?php echo $product->Line ?>.
                </td>
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                    1
                </td>
                <td style="white-space:nowrap;border-right:1px solid #ddd;">
                    Finished Interior
                </td>
                <td style="white-space:nowrap;border-right:1px solid #ddd;"></td>
                <td>
                    </td>
                 <!-- Pricing -->
                 <td class="LIST text-left price-td @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($product->getComponent('FinishedInterior')->ListQty,2) }}
                        </i>
                    </small>
                </td>
                <td class="LIST text-left price-td @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            ${{ number_format($product->getComponent('FinishedInterior')->ListQty * $product->Qty,2) }}
                        </i>
                    </small>
                </td>
                <td class="COST text-left price-td @if($pricing!='COST' || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($product->getComponent('FinishedInterior')->CostQty,2) }}
                        </i>
                    </small>
                </td>
                <td class="COST text-left price-td @if($pricing!='COST' || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            ${{ number_format($product->getComponent('FinishedInterior')->CostQty * $product->Qty,2) }}
                        </i>
                    </small>
                </td>
                <td class="CUSTOMER text-left price-td @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($product->getComponent('FinishedInterior')->CustomerQty,2) }}
                        </i>
                    </small>
                </td>
                <td class="CUSTOMER text-left price-td @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            
                        </i>
                    </small>
                </td>
                @if($editable)
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                </td>
                @endif
            </tr>
            @endif
            <!-- END finished Interior -->

            <!-- Finished Ends -->
            @if($product->finished_ends_count)
            <?php

            if ($product->Category == "base") {
                $FinishedEndList = $product->getComponent('BaseEnd')->ListPer;
                $FinishedEndListQty = $product->getComponent('BaseEnd')->ListPer * $product->Qty;
                $FinishedEndCost = $product->getComponent('BaseEnd')->CostPer;
                $FinishedEndCust = $product->getComponent('BaseEnd')->CustomerPer;
                $finishTitle = "Finished Base End";
            } elseif ($product->Category == "wall") {
                $FinishedEndList = $product->getComponent('WallEnd')->ListPer;
                $FinishedEndListQty = $product->getComponent('WallEnd')->ListPer * $product->Qty;
                $FinishedEndCost = $product->getComponent('WallEnd')->CostPer;
                $FinishedEndCust = $product->getComponent('WallEnd')->CustomerPer;
                $finishTitle = "Finished Wall End";
            } elseif ($product->Category == "tall") {
                $FinishedEndList = $product->getComponent('TallEnd')->ListPer;
                $FinishedEndListQty = $product->getComponent('TallEnd')->ListPer * $product->Qty;
                $FinishedEndCost = $product->getComponent('TallEnd')->CostPer;
                $FinishedEndCust = $product->getComponent('TallEnd')->CustomerPer;
                $finishTitle = "Finished Tall End";
            } else {
                $FinishedEndList = 0;
                $FinishedEndListQty = 0;
                $FinishedEndCost = 0;
                $FinishedEndCust = 0;
                $finishTitle = "Finished End [included in $]";
            }



            
            ?>
            <tr>
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                    **<?php echo $product->Line; ?>
                </td>
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                    1
                </td>
                <td style="white-space:nowrap;border-right:1px solid #ddd;">
                    {{ $finishTitle }} ({{ $product->FinishedEnd }})
                </td>
                <td style="white-space:nowrap;border-right:1px solid #ddd;"></td>
                <td>
                </td>
                <!-- Pricing -->
                <td class="LIST text-left price-td @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndList,2) }}
                        </i>
                    </small>
                </td>
                <td class="LIST text-left price-td @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndListQty,2) }}
                        </i>
                    </small>
                </td>
                <td class="COST text-left price-td @if($pricing!='COST' || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndCost,2) }}
                        </i>
                    </small>
                </td>
                <td class="COST text-left price-td @if($pricing!='COST' || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndCost,2) }}
                        </i>
                    </small>
                </td>
                <td class="CUSTOMER text-left price-td @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndCust,2) }}
                        </i>
                    </small>
                </td>
                <td class="CUSTOMER text-left price-td @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif">
                    <small>
                        <i>
                            ${{ number_format($FinishedEndCust,2) }}
                        </i>
                    </small>
                </td>
                @if($editable)
                <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
                </td>
                @endif
            </tr>

            @endif
            <!-- /* Finished Ends --> <tr>

          

            @foreach($product->modifications as $modification)
                 @include('partials.product-mod-row', ['modification'=>$modification])
            @endforeach
        @endforeach
           
        @if($pricing != 'No')
        
        
        
        <tr id='price-total-row' class="@if($stageHide == TRUE) hide @endif">

            <td colspan="5" align="right" class="@if($stageHide == TRUE) hide @endif">
                <b><i>Modification pricing included in main line total</i></b>
            </td>
            <td>
            </td>
            <td class="price-td LIST text-right @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif">
                <strong>${{ number_format($job->List,2) }}</strong>
            </td>
            <td class="price-td COST text-right @if($pricing!='COST' || $stageHide == TRUE) hide @endif">
                <strong>${{ number_format($job->Net,2) }}</strong>
            </td>
            <td class="price-td CUSTOMER text-right @if($pricing!='CUSTOMER' || $stageHide == TRUE) hide @endif">
                <strong>${{ number_format($job->Customer,2) }}</strong>
            </td>
            @if($editable)
            <td>
                <strong>Total</strong>
            </td>
            @endif
        </tr>
        <tr class="COST hide" style="background-color: #EEF1F3">
            <td colspan="5" align="right">
                <strong>Profit: Customer - Net<strong>
            </td>
            <td>
            </td>
            <td class="text-right">
                <strong>${{  round($job->Profit,2) }}</strong>
            </td>

            @if($editable)
            <td>
                <strong>Total</strong>
            </td>
            @endif
        </tr>
        @endif
        
    </tbody>

</table>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<script>
    $("a.line-option").click(function() {
        var form = $(this).closest('form');
        var input = form.find('input.line-input');
        var new_line = $(this).html();

        input.val(new_line);
        form.submit();

    });

    $('[data-toggle="tooltip"]').tooltip();

    $('.table-hover').tooltip();

    
</script>



<?php
// if there are no products, don't run this
//solves error when using details page on list table
$fullURI = $_SERVER["REQUEST_URI"];
$productPage = strpos($fullURI, "products");
// echo $fullURI;
if (isset($job->products) && $productPage == True) { ?>
    @include('partials.products.add_edit_modal_details')
<?php } ?>
