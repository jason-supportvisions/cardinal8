<div class="row">
    <div class="col-md-12" style="padding-bottom: 15px">
        <div class="btn-group" style="width: 100%">
            <a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{ route('post.edit',['jobID'=>$job->JobID, 'jobAcct'=>$job->AccountID, 'jobmod'=>'edit']) }}">
                Job &nbsp;&nbsp;
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>
            <a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{route('cons-first')}}">
                Specifications
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>
            <a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{ route('quote_orders.notes',['jobID'=>$job->JobID, 'jobAcct'=>$job->AccountID]) }}">
                Uploads &nbsp;&nbsp;
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>
            <a class="btn btn-xs btn-primary" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{ route('quote_orders.product',['jobID'=>$job->JobID ]) }}">
                Products
            </a>
            <a class="btn btn-xs btn-UX-todo text-black" style="width: 20%; margin-left: 0px;" disabled>
                Summary
            </a>
        </div>
    </div>
</div>
<div class="row hidden-xs hidden-sm">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading" style="border: none;height: 42px;">
                <div class="pull-right" style="margin-right: -10px;margin-top: -5px;margin-bottom: -5px">
                    <a href="{{ route('quote_orders.notes',['jobID'=>$job->JobID, 'jobAcct'=>$job->AccountID]) }}" class="btn btn-primary">
                        Back
                    </a>
                    <a href="{{ route('quote_orders.summary',['jobID'=>$job->JobID]) }}" class="btn btn-primary">
                        Next / Save
                    </a>
                </div>
                <h4 style="margin-bottom: 0px;margin-top: 0px;">
                    <strong>Job:</strong>#{{ $job->JobID }}&nbsp; &nbsp; {{ $job->Reference }}
                </h4>
            </div>
        </div>
    </div>
</div>