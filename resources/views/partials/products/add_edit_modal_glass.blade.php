<div id="Glass" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title"><strong>Glass Door Options:</strong>&nbsp;&nbsp;&nbsp; <?php echo $Code; ?> &nbsp;&nbsp;&nbsp;<small><?php echo $CVCode; ?></small></h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-sm-7">
						<div class="form-horizontal">
							<div class="form-group">
								<label class="col-sm-4 control-label" for="frontSel">Front Selection<span class="text-red"> * </span></label>
								<div class="col-sm-7">
									<?php selectexplode(',', $GlassOption, 'frontSel', $FrontSelection, 'None') ?>
								</div>
							</div>
							<hr>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="doorSty">Door<span class="text-red"> * </span></label>
								<div class="col-sm-7">
									<input type="hidden" name="doorSty" id="doorStyField" value="">
									<select class="form-control doorSty" id="doorSty" name="doorSty-select" style="width:100%;">
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="styMatl">Material<span class="text-red"> * </span></label>
								<div class="col-sm-7">
									<select class="form-control styMatl" id="styMatl" name="styMatl" style="width:100%;">
										<?php
										if (is_null($GlassStyleID) or empty($GlassStyleID)) {
											echo "<option value=\"\">Select (Door Material)</option>";
										} else {
											selectedarray("styles", "MaterialID", "WHERE ID = $GlassStyleID", "style_material", $GlassStyleMaterialID, "", "196");
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="styColor">Color<span class="text-red"> * </span></label>
								<div class="col-sm-7">
									<select class="form-control styColor" id="styColor" name="styColor" style="width:100%;">
										<?php
										if (is_null($GlassStyleMaterialID) or empty($GlassStyleMaterialID)) {
											echo "<option value=\"\">Select (Door Color)</option>";
										} else {
											$jobAllColors = $conn->query("SELECT ColorID FROM style_material WHERE ID = $GlassStyleMaterialID");
											$AllColors = $jobAllColors->fetch_assoc();
											$AllColorsGroup = $AllColors['ColorID'];
											if ($AllColorsGroup == 'All') {
												selected("style_color", $GlassStyleColorID, "WHERE MaterialID = $GlassStyleMaterialID", "");
											} else {
												selectedarray("style_material", "ColorID", "WHERE ID = $GlassStyleMaterialID", "style_color", $GlassStyleColorID, "", "");
											}
										}
										?>
									</select>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label" for="styFinish">Finish<span class="text-red"> * </span></label>
								<div class="col-sm-7">
									<select class="form-control styFinish" id="styFinish" name="styFinish" style="width:100%;">
										<?php
										if (is_null($GlassStyleColorID) or empty($GlassStyleColorID)) {
											echo "<option value=\"\">Select (Style Finish)</option>";
										} else {
											selectedarray("style_color", "FinishID", "WHERE ID = $GlassStyleColorID", "style_finish", $GlassStyleFinishID, "", "");
										}
										?>
									</select>
								</div>
							</div>

						</div>
					</div>
					<div id="StyleImagePlace" class="col-sm-5">
						<div class="pull-right">
							<div class="panel panel-default">
								<div class="panel-body control-label">
									<?php 
									if (isset($job_product->job->construction)) {
										$mBrandName 				= $job_product->job->door->BrandName;
										$mStyleGroupName 			= $job_product->job->door->StyleGroupName;
										$mStyleType 				= $job_product->job->door->StyleType;
										$mDoorStyleName 			= $job_product->job->door->DoorStyleName;
										$mStyleMaterialName 		= $job_product->job->door->StyleMaterialName;
										$mStyleColorName 			= $job_product->job->door->StyleColorName;
										$mStyleFinishName 			= $job_product->job->door->StyleFinishName;
										$mDoorOutsideProfileCode 	= $job_product->job->door->DoorOutsideProfileCode;
										$mDoorStileRailCode 		= $job_product->job->door->DoorStileRailCode;
										$mDoorCenterPanelCode 		= $job_product->job->door->DoorCenterPanelCode;
										$mDoorInsideProfileCode 	= $job_product->job->door->DoorInsideProfileCode;
										$inside 					= $job_product->job->door->inside_profile;
										$imagesFolder = "/images/doors/";
										$imageUrl = $imagesFolder . $mDoorStyleName . "/";
										$imageUrl .= $mStyleType  . $mDoorStyleName . "Thumbnail" . $mDoorInsideProfileCode ;
										$imageUrl .= $mDoorStileRailCode  . $mDoorCenterPanelCode  . $mDoorOutsideProfileCode . ".png";
										$imageUrl = str_replace(" ", "%20", $imageUrl);

										//finish
										// ex /images/finish/Standard Paint Antique White.jpg" 
										$finImagesFolder = "/images/finish/";
										$finImageUrl = $finImagesFolder . $mStyleMaterialName . " " . $mStyleColorName . ".jpg";
										$finImageUrl = str_replace(" ", "%20", $finImageUrl);
									}
									?>

									<div id="matchDoor">
										<table width="300px" cellspacing="5" border="0" cellpadding="5" align="center" id="glassdoortable">
											<tr>
												<td>
													<img id="glassdoorimg" src="{{$imageUrl??''}}" style="width:150px; height:150px;">
													<br />
													DOOR
												</td>
												<td>

												</td>
											</tr>
											<tr>
												<td>

												</td>
												<td>

												</td>
											</tr>
										</table>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>

				<input type="hidden" id="vFrontSelection" value="<?php echo $job_product->FrontSelection; ?>">
				<input type="hidden" id="vFrontSelectionMaterial" value="<?php echo $job_product->FrontSelectionMaterial; ?>">
				<input type="hidden" id="vFrontSelectionColor" value="<?php echo $job_product->FrontSelectionColor; ?>">
				<input type="hidden" id="vFrontSelectionFinish" value="<?php echo $job_product->FrontSelectionFinish; ?>">
				<input type="hidden" id="vFrontSelectionDoor" value="<?php echo $job_product->FrontSelectionDoor; ?>">
				<input type="hidden" id="glassFront" value="<?php if (isset($_GET['Front'])) {
																echo 'boop';
															} ?>">
				<input type="hidden" id="glassMaterial" value="<?php if (isset($_GET['Material'])) {
																	echo 'boop';
																} ?>">
				<input type="hidden" id="glassColor" value="<?php if (isset($_GET['Color'])) {
																echo 'boop';
															} ?>">
				<input type="hidden" id="glassFinish" value="<?php if (isset($_GET['Finish'])) {
																	echo 'boop';
																} ?>">
				<input type="hidden" id="glassWidth" value="<?php if (isset($_GET['GlassWidth'])) {
																echo 'boop';
															} ?>">
				<input type="hidden" id="glassHeight" value="<?php if (isset($_GET['GlassHeight'])) {
																	echo 'boop';
																} ?>">
				<input type="hidden" id="glassSqFt" value="<?php if (isset($_GET['GlassSqFt'])) {
																echo 'boop';
															} ?>">


			</div>
			<div class="modal-footer">
				<button data-dismiss="modal" id="hidemodalbtn" style="display:none">hide me</button>
				<button class="btn btn-primary pull-right" type="button" id="glassOptions">Save</button>
			</div>
		</div>
	</div>
</div>