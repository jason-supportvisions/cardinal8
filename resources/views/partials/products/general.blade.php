<div class="row">
    <div class="col-md-12" style="padding-bottom: 15px">
        <div class="btn-group" style="width: 100%">
            <a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="#">
                Job &nbsp;&nbsp;
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>
            <a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="#">
                Specifications
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>          
            <a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="#">
                Uploads &nbsp;&nbsp;
                <span class="">
                    <i class="check-green glyphicon glyphicon-ok"></i>
                </span>
            </a>
            <a class="btn btn-xs btn-primary disabled" style="width: 20%; margin-left: 0px; border-right: 0px;" href="#">
                Products
            </a>
            <a class="btn btn-xs btn-UX-todo disabled text-black disabled" style="width: 20%; margin-left: 0px;" href="#">
                Summary
            </a>       
        </div>
    </div>
</div>

<div class="row hidden-xs hidden-sm">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading" style="border: none;height: 42px;">
                
                <div class="pull-right" style="margin-right: -10px;margin-top: -5px;margin-bottom: -5px">

                    <a href="{{ route('quote_orders.product',['jobID'=>$job_product->JobID]) }}">                    
                        <button type="button" class="btn btn-danger">Cancel</button>
                    </a>
                            
                    <button type="submit" value="Validate" class="btn btn-primary" id="saveItem">Save</button>
                </div>
                
                <h4 style="margin-bottom: 0px;margin-top: 0px;">
                    <strong>Job: </strong>#{{ $job_product->JobID }}&nbsp; &nbsp;
                </h4>
                
            </div>
        </div>
    </div>
</div>