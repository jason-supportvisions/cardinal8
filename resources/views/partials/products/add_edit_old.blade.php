
<?php
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
// =====================================================================
// Create variables to go between Product type and Accessory type easily
// =====================================================================

// if it's in show/edit mode we can get the type this way
$itemStr = $job_product->Type;

//if it's an add page then we need to get the type of product from the querystring    
if ($itemStr == "") {

    if (session('product_type')) {
            if (session('product_type') == "accessory") {
                $itemStr = "accessory";
            } else {
                $itemStr = "product";
            }
    } else {
            $itemStr = "product";
    }
}


$Table       = $itemStr;
$Type        = $itemStr;
$prodType    = $itemStr;
$itemType    = $itemStr . "_item";
$groupType   = $itemStr . "_group";
$pricingType = $itemStr . "_pricing";
$noteType    = $groupType . "_notes";
$itemTable   = $itemType . "s";
$groupTable  = $groupType . "s";

//===========================================================================
//Should we be listing products, editing a product, or adding a product?
//===========================================================================

if (isset($job_product->product_item_id)) {
    $action = "add";
} else {
    $action = "show";
}

if ($action == "add") {
    $add_or_edit     = 'add';
    $product_item_id = $job_product->product_item_id;
} else {

    if (isset($job_product->product_item->ID)) {
        $product_item_id = $job_product->product_item->ID;
        $add_or_edit     = 'edit';
    }
    if (isset($job_product->accessory_item->ID)) {
        $product_item_id = $job_product->accessory_item->ID;
        $add_or_edit     = 'edit';
    }
}

$ProdID = $product_item_id;

// ==========================================================================   
// Page Settings (1 = True | 0 = False)
// ==========================================================================
$Title           =  " | Products";
$debug           = '';
$Select2         = 0;
$Select2beta     = 1;
$DataTables      = 0;
$jQuery_UI       = 1;
$jQuery_Validate = 1;
$xCRUD_16        = 1;
$jobPrint        = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing      = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;
$JobID           = $job_product->JobID;
$pageid          = 'Add/Edit Products';
$jobID           = $job_product->JobID;
$jobAcct         = $job_product->job->AccountID;
$jobProdLine     = isset($job_product->Line) ? $job_product->line : 0;
$Code            = $job_product->Code;
$qty             = 1;
$SqFtQty         = 1;
$editable        = 1;
$SqFtProblem     = 0;
$AcctMultDis     = $job_product->job->multiplier_discount;
$AcctMultCus     = $job_product->job->multiplier_customer;



// ===========================================================
// User variables / session
// ===========================================================
$SesUser            = session('userid');
$SesAcct            = session('accountid');
$SesType            = session('accounttype');
$UserID             = session('userid');

// ==========================================================
// Variable declarations
// ==========================================================
$LargeDrawerStyleID      = 0;
$DrawerStyleID           = 0;
$DoorHardwareID          = 0;
$DoorStyleID             = 0;
$DoorList                = 0;
$TallDoorList            = 0;
$FullTallDoorList        = 0;
$DrawerList              = 0;
$LargeDrawerList         = 0;
$OverlayList             = 0;
$DrawerBoxList           = 0;
$LargeDrawerBoxList      = 0;
$CabinetBoxMaterialGroup = 0;
$FrameList               = 0;
$FinishedInteriorList    = 0;

$Panel1SSqFtQty          = 0;
$Panel2SSqFtQty          = 0;
$DoorSqFtQty             = 0;
$DrawerSqFtQty           = 0;
$OrigList                = 0;
$HandleList              = 0;
$totalSqft               = 0;
//Glass
$GlassList               = 0;
$GlassStyleID            = 0;
$GlassStyleMaterialID    = 0;
$GlassStyleColorID       = 0;
$GlassStyleFinishID      = 0;
$GlassQty                = 0;
$GlassUpcharge           = 0;
$StyleMaterialID         = 0;
$StyleColorID            = 0;
$StyleFinishID           = 0;

//Door
$StyleDoorList           = 0;
$StyleMaterialDoorList   = 0;
$StyleColorDoorList      = 0;
$StyleFinishDoorList     = 0;
$StyleType               = 0;

//Tall Door
$StyleTallDoorList              = 0;
$StyleMaterialTallDoorList      = 0;
$StyleColorTallDoorList         = 0;
$StyleFinishTallDoorList        = 0;

//Full Tall Door
$StyleFullTallDoorList          = 0;
$StyleMaterialFullTallDoorList  = 0;
$StyleColorFullTallDoorList     = 0;
$StyleFinishFullTallDoorList    = 0;

//Large Drawer
$StyleLargeDrawerList           = 0;
$StyleMaterialLargeDrawerList   = 0;
$StyleColorLargeDrawerList      = 0;
$StyleFinishLargeDrawerList     = 0;

//Drawer
$StyleDrawerList                = 0;
$StyleMaterialDrawerList        = 0;
$StyleColorDrawerList           = 0;
$StyleFinishDrawerList          = 0;

//Overlay
$StyleOverlayList               = 0;
$StyleMaterialOverlayList       = 0;
$StyleColorOverlayList          = 0;
$StyleFinishOverlayList         = 0;

//DoorSqFt
$DoorSqFtList                   = 0;
$StyleDoorSqFtList              = 0;
$StyleMaterialDoorSqFtList      = 0;
$StyleColorDoorSqFtList         = 0;
$StyleFinishDoorSqFtList        = 0;

//DrawerSqFt
$DrawerSqFtList                 = 0;
$StyleDrawerSqFtList            = 0;
$StyleMaterialDrawerSqFtList    = 0;
$StyleColorDrawerSqFtList       = 0;
$StyleFinishDrawerSqFtList      = 0;

//Wall End
$WallEndList                    = 0;
$StyleWallEndList               = 0;
$StyleMaterialWallEndList       = 0;
$StyleColorWallEndList          = 0;
$StyleFinishWallEndList         = 0;

//Base End
$BaseEndList              = 0;
$StyleBaseEndList         = 0;
$StyleMaterialBaseEndList = 0;
$StyleColorBaseEndList    = 0;
$StyleFinishBaseEndList   = 0;

//Tall End
$TallEndList              = 0;
$StyleTallEndList         = 0;
$StyleMaterialTallEndList = 0;
$StyleColorTallEndList    = 0;
$StyleFinishTallEndList   = 0;

//Panel 1 sided
$Panel1SSqFtList         = 0;
$StylePanel1List         = 0;
$StyleMaterialPanel1List = 0;
$StyleColorPanel1List    = 0;
$StyleFinishPanel1List   = 0;

//Panel 2 sided
$Panel2SSqFtList         = 0;
$StylePanel2List         = 0;
$StyleMaterialPanel2List = 0;
$StyleColorPanel2List    = 0;
$StyleFinishPanel2List   = 0;

//Accessory
$AccessoryDoorList     = 0;
$AccessoryMaterialList = 0;
$AccessoryColorList    = 0;
$AccessoryFinishList   = 0;

//Box Material
$BoxList = 0;
$BoxSqFt = 0;

//ToeKick
$ToeKickSqFt = 0;
$ToeKickList = 0;
$Class       = "";
$ClassBuild  = "";

//Hinge
$Hinge      = 0;
$HingeList  = 0;
$HingeOption = "";


//FrontSelection
$FrontSelection         = "None";
$FrontSelectionDoor     = 0;
$FrontSelectionMaterial = 0;
$FrontSelectionColor    = 0;
$FrontSelectionFinish   = 0;
$StyleDoorType          = 0;
$StyleTallType          = 0;
$StyleFullTallType      = 0;

//Finished
$FinishedInteriorSqFt = 0;
$AdjShelf             = 0;
$FixedShelf           = 0;
$Partition            = 0;
$Divider              = 0;
$Hinge                = 0;
$HingeType            = 0;

//Glass
$GlassDoorList              = 0;  //cost of a glass door
$StyleGlassDoorList         = 0;
$StyleMaterialGlassDoorList = 0;
$StyleColorGlassDoorList    = 0;
$StyleFinishGlassDoorList   = 0;
$StyleGlassType             = 0;
$TotalDoorList              = 0;


//Money
$TotalCalc    = 0;
$AccessoryQty = 0;
$OrigCode     = 0;
$TotalList    = 0;
$UnitList     = 0;
$ListMinimum  = 0;

$AccessoryItem = "";
$CabinetItem   = 0;
$SqFtItem      = 0;

$StyleName    = "";
$MaterialName = "";
$ColorName    = "";
$FinishName   = "";

$IgnoreStyleCharges     = 0;
$StyleChargeArray       = array();
$ShelvingSqFt           = 0;
$Shelf                  = 0;
$ShelfList              = 0;
$StyleShelfList         = 0;
$StyleMaterialShelfList = 0;
$StyleColorShelfList    = 0;
$StyleFinishShelfList   = 0;
$StyleShelfType         = 0;
$totalCharge            = 0;

$addStyleCharge     = 0;
$addStyleChargeType = "none";
$addStyleChargeQty  = 0;
$addDoorType        = "";
$addDoorQty         = 0;
$addDoorType2       = "";
$addDoorQty2        = 0;
$addDoorList        = 0;

$WallEnd          = 0;
$BaseEnd          = 0;
$TallEnd          = 0;
$FinishedInterior = 0;
$ProdID           = 0;

$FinishedEnd              = 0;
$FinishedInteriorSelected = 0;

$StyleMaterialCustom        = "";
$StyleColorCustom           = "";
$StyleFinishCustom          = "";
$CabinetDrawerBoxID         = 0;
$EdgeBandingOption          = "";
$EdgeBanding                = 0;
$CabinetExteriorEdgeID      = 0;
$LargeDrawerHardwareID      = 0;
$DrawerHardwareID           = 0;
$FrontType                  = "";
$FrontLabel                 = "";
$FrontWidth                 = "";
$FrontHeight                = "";
$Notes                      = "";
$StyleMaterialGroup         = 0;
$StyleColorGroup            = 0;
$StyleFinishGroup           = 0;
$CabinetBoxMaterialID       = 0;
$Door                       = 0;
$TallDoor                   = 0;
$FullTallDoor               = 0;
$Drawer                     = 0;
$LargeDrawer                = 0;
$Overlay                    = 0;
$Frame                      = 0;
$DrawerBox                  = 0;
$LargeDrawerBox             = 0;
$Handle                     = 0;
$Description                = '';
//========================================
// /* vars 
//========================================


//========================================
//item info
//========================================


$ProdResults = DB::select(DB::raw("SELECT * FROM $itemTable WHERE $itemTable.`ID` = '$product_item_id'"));

if (is_array($ProdResults)) {

    foreach ($ProdResults as $row) {
        $GroupCode          = $row->GroupCode;
        $Code               = $row->Code;
        $Width              = $row->Width;
        $Height             = $row->Height;
        $Depth              = $row->Depth;
        $MinWidth           = $row->MinWidth;
        $MinHeight          = $row->MinHeight;
        $MinDepth           = $row->MinDepth;
        $MaxWidth           = $row->MaxWidth;
        $MaxHeight          = $row->MaxHeight;
        $MaxDepth           = $row->MaxDepth;
        $IgnoreStyleCharges = $row->IgnoreGroupStyleCharge;
        $ItemDescription    = $row->Description;
       

        //if the style charge row exists (accessory only at this time)
        if (isset($row->addStyleCharge)) {
            if ($row->addStyleCharge > 0) {

                $addStyleCharge     = $row->addStyleCharge;
                $addStyleChargeType = $row->addStyleChargeType;

                $addDoorType = $row->addDoorType;
                $addDoorQty  = $row->addDoorQty;

                $addDoorQty2  = $row->addDoorQty2;
                $addDoorType2 = $row->addDoorType2;
            }
        }
        if(session('product_type') == 'accessory'){
            $itemGroupCode      = $GroupCode;
            // $itemGroupId        = $row->accessory_group_id;
            $itemCVCode         = $row->CVCode;
        }
    }
    $sql2 = "SELECT * FROM `$groupTable` WHERE `GroupCode`='{$GroupCode}'";
    $group_result = DB::select(DB::raw($sql2));
    
    if(count($group_result)){
        foreach ($group_result as $group_row) {
            $product_group_id       = $group_row->ID;
            $UOM                    = $group_row->UOM;
            $Door                   = $group_row->DoorCount;
            $TallDoor               = $group_row->TallDoorCount;
            $FullTallDoor           = $group_row->FullTallDoorCount;
            $Drawer                 = $group_row->DrawerCount;
            $LargeDrawer            = $group_row->LargeDrawerCount;
            $Overlay                = $group_row->OverlayCount;
            $Frame                  = $group_row->FrameCount;
            $DrawerBox              = $group_row->DrawerBoxCount;
            $LargeDrawerBox         = $group_row->LargeDrawerBoxCount;
            $AdjShelf               = $group_row->AdjShelfCount;
            $FixedShelf             = $group_row->FixedShelfCount;
            $Partition              = $group_row->PartitionCount;
            $Divider                = $group_row->DividerCount;
            $Hinge                  = $group_row->HingeCount;
            $Handle                 = $group_row->HandleCount;
            $GroupCode              = $group_row->GroupCode;
            $Class                  = $group_row->Class;
            $ClassGroup             = $group_row->ClassGroup;
            $CVCode                 = $group_row->CVCode;
            $OldCode                = $group_row->OldCode;
            $CodePrefixSuffix       = $group_row->CodePrefixSuffix;
            $Category               = strtolower($group_row->Category);
            $GroupType              = $group_row->Type;
            $GroupDescription       = $group_row->Description;
            $GroupBookDescription   = $group_row->BookDescription;
            $FinishedInteriorSelected = $group_row->FinishedInterior;
            $FinishedInterior       = $group_row->FinishedInterior;
            $CornerCabinet          = $group_row->CornerCabinet;
            $PeninsulaCabinet       = $group_row->PeninsulaCabinet;
            $PullOption             = $group_row->PullOption;
            $HingeOption            = $group_row->HingeOption;
            $des                    = $group_row->Description;
            $Description            = $des;
            $GlassOption            = $group_row->GlassOption;
            $ClassBuild             = $group_row->ClassBuild;
            $EdgeBandingOption      = $group_row->EdgeBanding;
            $CustomList             = "";
            
        }
                
    }else{
        $Category       = session('product_type'); 
        $UOM            = 'Each';
        $GroupCode      = $itemGroupCode;
        // $product_group_id = $itemGroupId;
        $CVCode         = $itemCVCode;
        $Code           = $Code;
        $GlassOption    = ''; 
                   
    }
}


//====================================================
// SHOW / EDIT products
//====================================================
// EDIT ONLY

if ($add_or_edit == "edit") {

    //=================================================
    //get existing job_product
    //=================================================
    $jpDoorType = $job_product->job->specifications->door->style_type;
    $jobResults = DB::select(DB::raw("SELECT * FROM job_product WHERE Line = $jobProdLine AND JobID = $jobID"));
   
    foreach ($jobResults as $row) {
        $ProdID                     = $row->ID;
        $Type                       = $row->Type;
        $Line                       = $row->Line;
        $qty                        = $row->Qty;
        $UOM                        = $row->UOM;
        $Code                       = $row->Code;
        $OrigCode                   = $row->OrigCode;
        $Width                      = (float)$row->Width;
        $Height                     = (float)$row->Height;
        $Depth                      = (float)$row->Depth;
        $Hinging                    = $row->Hinging;
        $FinishedEnd                = $row->FinishedEnd;
        $des                        = $row->Description;
        $Notes                      = $row->Notes;
        $UnitList                   = $row->ListPer;
        $OrigList                   = $row->OrigList;
        $FrontList                  = $row->FrontList;
        $GlassList                  = $row->GlassList;
        $DrwBoxList                 = $row->DrwBoxList;
        $FinEndList                 = $row->FinEndList;
        $FinIntList                 = $row->FinIntList;
        $ModList                    = $row->ModList;
        $FinList                    = $row->FinList;
        $HandleList                 = $row->HandleList;
        $MiscList                   = $row->MiscList;
        $ListPer                    = $row->ListPer;
        $TotalCalc                  = $row->ListPer;
        $TotalList                  = $row->ListQty;
        $CreatedBy                  = $row->CreatedBy;
        $CreatedDate                = $row->CreatedDate;
        $ModifiedBy                 = $row->ModifiedBy;
        $ModifiedDate               = $row->ModifiedDate;
        $Fronts                     = $row->Fronts;
        $Internal                   = $row->Internal;
        $Door                       = $row->DoorCount;
        $TallDoor                   = $row->TallDoorCount;
        $FullTallDoor               = $row->FullTallDoorCount;
        $Drawer                     = $row->DrawerCount;
        $LargeDrawer                = $row->LargeDrawerCount;
        $Overlay                    = $row->OverlayCount;
        $Frame                      = $row->FrameCount;
        $DrawerBox                  = $row->DrawerBoxCount;
        $LargeDrawerBox             = $row->LargeDrawerBoxCount;
        $AdjShelf                   = $row->AdjShelfCount;
        $FixedShelf                 = $row->FixedShelfCount;
        $Partition                  = $row->PartitionCount;
        $Divider                    = $row->DividerCount;
        $Hinge                      = $row->HingeCount;
        $Handle                     = $row->HandleCount;
        $FinishedInteriorSelected   = $row->FinishedInterior;
        $FinishedInterior           = $row->FinishedInterior;
        $FinishedInteriorSqFt       = $row->FinishedInteriorSqFt;
        $Volume                     = $row->Volume;
        $Weight                     = $row->Weight;
        $Category                   = $row->Category;
        $StyleDoorID                = $row->DoorStyleID;
        $StyleMaterialID            = $row->StyleMaterialID;
        $StyleMaterialGroup         = $row->StyleMaterialGroupID;
        $StyleColorID               = $row->StyleColorID;
        $StyleColorGroup            = $row->StyleColorGroupID;
        $StyleFinishID              = $row->StyleFinishID;
        $StyleFinishGroup           = $row->StyleFinishGroupID;
        $CabinetBoxMaterialGroup    = $row->CabinetBoxMaterialGroup;
        $CabinetBoxMaterialID       = $row->CabinetBoxMaterialID;
        $CabinetDrawerBoxID         = $row->CabinetDrawerBoxID;
        $StyleOR                    = $row->StyleOverride;
        $CabinetBoxOverride         = $row->CabinetBoxOverride;
        $FrontSelection             = $row->FrontSelection;
        $FrontSelectionMaterial     = $row->FrontSelectionMaterial;
        $FrontSelectionColor        = $row->FrontSelectionColor;
        $FrontSelectionFinish       = $row->FrontSelectionFinish;
        $FrontSelectionDoor         = $row->FrontSelectionDoor;
        $FrontType                  = $row->FrontType;
        $FrontLabel                 = $row->FrontLabel;
        $FrontWidth                 = $row->FrontWidth;
        $FrontHeight                = $row->FrontHeight;
    }
}

//Dimension Widths / Limits
if ($Width ==  $MinWidth) {
    $MinWidth       = (float)$MinWidth;
} else {
    $MinWidth       = round((float)$MinWidth * 2, 0) / 2;
}
if ($Height == $MinHeight) {
    $MinHeight      = (float)$MinHeight;
} else {
    $MinHeight      = round((float)$MinHeight * 2, 0) / 2;
}
if ($MinDepth > 1) {
    $MinDepth       = round((float)$MinDepth * 2, 0) / 2;
} else {
    $MinDepth       = (float)$MinDepth;
}
if ($Width == $MaxWidth) {
    $MaxWidth       = (float)$MaxWidth;
} else {
    $MaxWidth       = round((float)$MaxWidth * 2, 0) / 2;
}
if ($Height == $MaxHeight) {
    $MaxHeight      = (float)$MaxHeight;
} else {
    $MaxHeight      = round((float)$MaxHeight * 2, 0) / 2;
}
if ($MaxDepth > 1) {
    $MaxDepth       = round((float)$MaxDepth * 2, 0) / 2;
} else {
    $MaxDepth       = (float)$MaxDepth;
}

if ($Category == "accessory") {

    if ($UOM == "Each") {
        $AccessoryItem = $qty;
        $CabinetItem = 0;
        $SqFtItem = 0;
    }
    if ($UOM == "SqFt") {
        $AccessoryItem = 0;
        $CabinetItem = 0;
        $SqFtItem = "Yes";
    }

    //   //if --product-- UOM = EACH      
} else {

    if ($UOM == "Each") {
        $AccessoryItem = 0;
        $CabinetItem = 1;
        // $CabinetItem = $qty; //GLASS REVIEW - from product_add_edit
        $SqFtItem = 0;
    }

    if ($UOM == "SqFt") {
        $AccessoryItem = 0;
        $CabinetItem = 0;
        $SqFtItem = "Yes";
    }
} // /* UOM EACH    

//$FinishedEndOption = $row['FinishedEndOption'];

$FinishedEnd                = strtolower($FinishedEnd);
$Category                   = strtolower($Category);
$FinishedInteriorSelected   = strtolower($FinishedInteriorSelected);



//Finished Ends
if ($FinishedEnd == 'left' || $FinishedEnd == 'right') {
    $FinEndCount = 1;
} elseif ($FinishedEnd == 'both') {
    $FinEndCount = 2;
} else {
    $FinEndCount = 0;
}

//Counts based on Category
if ($Category == 'wall') {
    $WallEnd = $FinEndCount;
    $BaseEnd = 0;
    $TallEnd = 0;
} elseif ($Category == 'base') {
    $WallEnd = 0;
    $BaseEnd = $FinEndCount;
    $TallEnd = 0;
} elseif ($Category == 'tall') {
    $WallEnd = 0;
    $BaseEnd = 0;
    $TallEnd = $FinEndCount;
} else {
    $WallEnd = 0;
    $BaseEnd = 0;
    $TallEnd = 0;
}
//Interior Finished?
if ($FinishedInteriorSelected == 'yes') {
    $FinishedInteriorSqFt = $Width * $Height / 144;
    $FinishedInterior = $FinishedInteriorSqFt;
} else {
    $FinishedInterior = 0;
}
//=================================================
// SPECIFICATIONS ALL
//=================================================

$jobConst = DB::select(DB::raw("SELECT DISTINCT * FROM job_construction WHERE JobID = $jobID"));
foreach ($jobConst as $row) {
    $BrandID                    = $row->BrandID;
    $StyleGroupID               = $row->StyleGroupID;

    $DoorStyleID                = $row->DoorStyleID;
    $DoorOutsideProfileID       = $row->DoorOutsideProfileID;
    $DoorStileRailID            = $row->DoorStileRailID;
    $DoorPegID                  = $row->DoorPegID;
    $DoorInsideProfileID        = $row->DoorInsideProfileID;
    $DoorCenterPanelID          = $row->DoorCenterPanelID;
    $DoorHardwareID             = $row->DoorHardwareID;
    $DoorHardwareLocationID     = $row->DoorHardwareLocationID;

    $DrawerStyleID              = $row->DrawerStyleID;
    $DrawerOutsideProfileID     = $row->DrawerOutsideProfileID;
    $DrawerStileRailID          = $row->DrawerStileRailID;
    $DrawerPegID                = $row->DrawerPegID;
    $DrawerInsideProfileID      = $row->DrawerInsideProfileID;
    $DrawerCenterPanelID        = $row->DrawerCenterPanelID;
    $DrawerHardwareID           = $row->DrawerHardwareID;
    $DrawerHardwareLocationID   = $row->DrawerHardwareLocationID;

    $LargeDrawerStyleID         = $row->LargeDrawerStyleID;
    $LargeDrawerOutsideProfileID = $row->LargeDrawerOutsideProfileID;
    $LargeDrawerStileRailID     = $row->LargeDrawerStileRailID;
    $LargeDrawerPegID           = $row->LargeDrawerPegID;
    $LargeDrawerInsideProfileID = $row->LargeDrawerPegID;
    $LargeDrawerCenterPanelID   = $row->LargeDrawerCenterPanelID;
    $LargeDrawerHardwareID      = $row->LargeDrawerHardwareID;
    $LargeDrawerHardwareLocationID = $row->LargeDrawerHardwareLocationID;

    $StyleMaterialID            = $row->StyleMaterialID;
    $StyleMaterialGroup         = $row->StyleMaterialGroupID;
    $StyleColorID               = $row->StyleColorID;
    $StyleColorGroup            = $row->StyleColorGroupID;
    $StyleFinishID              = $row->StyleFinishID;
    $StyleFinishGroup           = $row->StyleFinishGroupID;

    $StyleMaterialCustom        = $row->StyleMaterialCustom;
    $StyleColorCustom           = $row->StyleColorCustom;
    $StyleFinishCustom          = $row->StyleFinishCustom;

    $CabinetBoxMaterialID       = $row->CabinetBoxMaterialID;
    $CabinetBoxMaterialGroup    = $row->CabinetBoxMaterialGroup;
    $CabinetDrawerBoxID         = $row->CabinetDrawerBoxID;
    $CabinetHingeID             = $row->CabinetHingeID;

    $CabinetExteriorMaterialID  = $row->CabinetExteriorMaterialID;
    $CabinetExteriorColorID     = $row->CabinetExteriorMaterialID;
    $CabinetExteriorFinishID    = $row->CabinetExteriorMaterialID;
    $CabinetExteriorEdgeID      = $row->CabinetExteriorEdgeID;

    $CabinetInteriorMaterialID  = $row->CabinetInteriorMaterialID;
    $CabinetInteriorColorID     = $row->CabinetInteriorColorID;
    $CabinetInteriorFinishID    = $row->CabinetInteriorFinishID;
    $CabinetInteriorEdgeID      = $row->CabinetInteriorEdgeID;

    $CabinetToeHeightID         = $row->CabinetToeHeightID;
}

if ($itemStr == "product") {
    $prodPricing = DB::select(DB::raw("SELECT * FROM product_pricing WHERE Code = '$Code' AND MaterialGroup = '$CabinetBoxMaterialGroup'"));
} elseif ($itemStr == "accessory") {
    $prodPricing = DB::select(DB::raw("SELECT * FROM accessory_pricing WHERE Code = '$Code' AND MaterialGroup = '$CabinetBoxMaterialGroup'"));
}

foreach ($prodPricing as $row) {
    $Price[] = $row;
    $UnitList = $row->List;
    $ListMinimum = $row->ListMinimum;
    $sqlMaterialGroup = $row->MaterialGroup;
}

//============================================
//If it's LargeDrawerStyle then use MATCH DOOR
// door price
//=============================================
if ($LargeDrawerStyleID == 54) {
    $LargeDrawerStyleID = $DoorStyleID;
}

if ($DrawerStyleID == 54) {
    $DrawerStyleID = $DoorStyleID;
}
//=============================================


//=================================================
// Hardware Pricing
//=================================================

$jobHdw = DB::select(DB::raw("SELECT DISTINCT * FROM style_hardware WHERE ID = $DoorHardwareID"));

//Handle pricing -- currently zero in the app
foreach ($jobHdw as $row) {
    $HandleList = $row->ListPer;
}
//=================================================
// /* Hardware Pricing
//=================================================

//=================================================
// DOORS Pricing
//=================================================

$jobDoorFronts = DB::select(DB::raw("
        SELECT
            `styles`.`ID` AS `StyleID`,
            `styles`.`StyleType` AS `StyleType`,
            `styles`.`Name` AS `StyleName`,
            `style_material`.`ID` AS `MaterialID`,
            `style_material`.`Name` AS `MaterialName`,
            `style_color`.`ID` AS `ColorID`,
            `style_color`.`Name` AS `ColorName`,
            `style_finish`.`ID` AS `FinishID`,
            `style_finish`.`Name` AS `FinishName`,

            `styles`.`DoorList` AS `StyleDoorList`,
            `style_material`.`DoorList` AS `StyleMaterialDoorList`,
            `style_color`.`DoorList` AS `StyleColorDoorList`,
            `style_finish`.`DoorList` AS `StyleFinishDoorList`, 

            `styles`.`TallDoorList` AS `StyleTallDoorList`,
            `style_material`.`TallDoorList` AS `StyleMaterialTallDoorList`,
            `style_color`.`TallDoorList` AS `StyleColorTallDoorList`,
            `style_finish`.`TallDoorList` AS `StyleFinishTallDoorList`,

            `styles`.`FullTallDoorList` AS `StyleFullTallDoorList`,
            `style_material`.`FullTallDoorList` AS `StyleMaterialFullTallDoorList`,
            `style_color`.`FullTallDoorList` AS `StyleColorFullTallDoorList`,
            `style_finish`.`FullTallDoorList` AS `StyleFinishFullTallDoorList`,

            `styles`.`OverlayList` AS `StyleOverlayList`,
            `style_material`.`OverlayList` AS `StyleMaterialOverlayList`,
            `style_color`.`OverlayList` AS `StyleColorOverlayList`,
            `style_finish`.`OverlayList` AS `StyleFinishOverlayList`,

            `styles`.`DoorSqFtList` AS `StyleDoorSqFtList`,
            `style_material`.`DoorSqFtList` AS `StyleMaterialDoorSqFtList`,
            `style_color`.`DoorSqFtList` AS `StyleColorDoorSqFtList`,
            `style_finish`.`DoorSqFtList` AS `StyleFinishDoorSqFtList`,

            `styles`.`DrawerSqFtList` AS `StyleDrawerSqFtList`,
            `style_material`.`DrawerSqFtList` AS `StyleMaterialDrawerSqFtList`,
            `style_color`.`DrawerSqFtList` AS `StyleColorDrawerSqFtList`,
            `style_finish`.`DrawerSqFtList` AS `StyleFinishDrawerSqFtList`,

            `styles`.`WallEndList` AS `StyleWallEndList`,
            `style_material`.`WallEndList` AS `StyleMaterialWallEndList`,
            `style_color`.`WallEndList` AS `StyleColorWallEndList`,
            `style_finish`.`WallEndList` AS `StyleFinishWallEndList`,

            `styles`.`BaseEndList` AS `StyleBaseEndList`,
            `style_material`.`BaseEndList` AS `StyleMaterialBaseEndList`,
            `style_color`.`BaseEndList` AS `StyleColorBaseEndList`,
            `style_finish`.`BaseEndList` AS `StyleFinishBaseEndList`,

            `styles`.`TallEndList` AS `StyleTallEndList`,
            `style_material`.`TallEndList` AS `StyleMaterialTallEndList`,
            `style_color`.`TallEndList` AS `StyleColorTallEndList`,
            `style_finish`.`TallEndList` AS `StyleFinishTallEndList`,

            `styles`.`FinishedInteriorList` AS `StyleFinishedInteriorList`,
            `style_material`.`FinishedInteriorList` AS `StyleMaterialFinishedInteriorList`,
            `style_color`.`FinishedInteriorList` AS `StyleColorFinishedInteriorList`,
            `style_finish`.`FinishedInteriorList` AS `StyleFinishFinishedInteriorList`,

            `styles`.`Panel1SSqFtList` AS `StylePanel1List`,
            `style_material`.`Panel1SSqFtList` AS `StyleMaterialPanel1List`,
            `style_color`.`Panel1SSqFtList` AS `StyleColorPanel1List`,
            `style_finish`.`Panel1SSqFtList` AS `StyleFinishPanel1List`,
            
            `styles`.`Panel2SSqFtList` AS `StylePanel2List`,
            `style_material`.`Panel2SSqFtList` AS `StyleMaterialPanel2List`,
            `style_color`.`Panel2SSqFtList` AS `StyleColorPanel2List`,
            `style_finish`.`Panel2SSqFtList` AS `StyleFinishPanel2List`,

            `styles`.`DoorList` + `style_material`.`DoorList` + `style_color`.`DoorList` + `style_finish`.`DoorList` AS `DoorList`,
            `styles`.`TallDoorList` + `style_material`.`TallDoorList` + `style_color`.`TallDoorList` + `style_finish`.`TallDoorList` AS `TallDoorList`,
            `styles`.`FullTallDoorList` + `style_material`.`FullTallDoorList` + `style_color`.`FullTallDoorList` + `style_finish`.`FullTallDoorList` AS `FullTallDoorList`,

            `styles`.`DrawerList` + `style_material`.`DrawerList` + `style_color`.`DrawerList` + `style_finish`.`DrawerList` AS `DrawerList`,
            `styles`.`LargeDrawerList` + `style_material`.`LargeDrawerList` + `style_color`.`LargeDrawerList` + `style_finish`.`LargeDrawerList` AS `LargeDrawerList`,
            `styles`.`OverlayList` + `style_material`.`OverlayList` + `style_color`.`OverlayList` + `style_finish`.`OverlayList` AS `OverlayList`,
            
            `styles`.`DoorSqFtList` + `style_material`.`DoorSqFtList` + `style_color`.`DoorSqFtList` + `style_finish`.`DoorSqFtList` AS `DoorSqFtList`,
            `styles`.`DrawerSqFtList` + `style_material`.`DrawerSqFtList` + `style_color`.`DrawerSqFtList` + `style_finish`.`DrawerSqFtList` AS `DrawerSqFtList`,
            `style_material`.`FinishMaterialType`,
            
            `styles`.`WallEndList` + `style_material`.`WallEndList` + `style_color`.`WallEndList` + `style_finish`.`WallEndList` AS `WallEndList`,
            `styles`.`BaseEndList` + `style_material`.`BaseEndList` + `style_color`.`BaseEndList` + `style_finish`.`BaseEndList` AS `BaseEndList`,
            `styles`.`TallEndList` + `style_material`.`TallEndList` + `style_color`.`TallEndList` + `style_finish`.`TallEndList` AS `TallEndList`,
            
            `styles`.`FinishedInteriorList` + `style_material`.`FinishedInteriorList` + `style_color`.`FinishedInteriorList` + `style_finish`.`FinishedInteriorList` AS `FinishedInteriorList`,

            `styles`.`Panel1SSqFtList` + `style_material`.`Panel1SSqFtList` + `style_color`.`Panel1SSqFtList` + `style_finish`.`Panel1SSqFtList` AS `Panel1SSqFtList`,
            `styles`.`Panel2SSqFtList` + `style_material`.`Panel2SSqFtList` + `style_color`.`Panel2SSqFtList` + `style_finish`.`Panel2SSqFtList` AS `Panel2SSqFtList`
        FROM
            `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
        WHERE
            styles.ID = $DoorStyleID AND style_material.ID = $StyleMaterialID AND style_color.ID = $StyleColorID AND style_finish.ID = $StyleFinishID"));

foreach ($jobDoorFronts as $row) {
    //Base Price
    $DoorList                           = $row->DoorList;
    $TallDoorList                       = $row->TallDoorList;
    $FullTallDoorList                   = $row->FullTallDoorList;
    $OverlayList                        = $row->OverlayList;
    $FrameList                          = $row->DoorList;
    $DoorSqFtList                       = $row->DoorSqFtList;
    $FinishMaterialType                 = $row->FinishMaterialType;
    $WallEndList                        = $row->WallEndList;
    $BaseEndList                        = $row->BaseEndList;
    $TallEndList                        = $row->TallEndList;
    $FinishedInteriorList               = $row->FinishedInteriorList;
    $Panel1SSqFtList                    = $row->Panel1SSqFtList;
    $Panel2SSqFtList                    = $row->Panel2SSqFtList;

    //Door
    $StyleDoorType                      = $row->StyleType;
    $StyleDoorList                      = $row->StyleDoorList;
    $StyleMaterialDoorList              = $row->StyleMaterialDoorList;
    $StyleColorDoorList                 = $row->StyleColorDoorList;
    $StyleFinishDoorList                = $row->StyleFinishDoorList;

    //TallDoor
    $StyleTallDoorList                  = $row->StyleTallDoorList;
    $StyleMaterialTallDoorList          = $row->StyleMaterialTallDoorList;
    $StyleColorTallDoorList             = $row->StyleColorTallDoorList;
    $StyleFinishTallDoorList            = $row->StyleFinishTallDoorList;

    //FullTallDoor
    $StyleFullTallDoorList              = $row->StyleFullTallDoorList;
    $StyleMaterialFullTallDoorList      = $row->StyleMaterialFullTallDoorList;
    $StyleColorFullTallDoorList         = $row->StyleColorFullTallDoorList;
    $StyleFinishFullTallDoorList        = $row->StyleFinishFullTallDoorList;

    //TallEnd
    $StyleTallEndList                   = $row->StyleTallEndList;
    $StyleMaterialTallEndList           = $row->StyleMaterialTallEndList;
    $StyleColorTallEndList              = $row->StyleColorTallEndList;
    $StyleFinishTallEndList             = $row->StyleFinishTallEndList;

    //WallEnd
    $StyleWallEndList                   = $row->StyleWallEndList;
    $StyleMaterialWallEndList           = $row->StyleMaterialWallEndList;
    $StyleColorWallEndList              = $row->StyleColorWallEndList;
    $StyleFinishWallEndList             = $row->StyleFinishWallEndList;

    //BaseEnd
    $StyleBaseEndList                   = $row->StyleBaseEndList;
    $StyleMaterialBaseEndList           = $row->StyleMaterialBaseEndList;
    $StyleColorBaseEndList              = $row->StyleColorBaseEndList;
    $StyleFinishBaseEndList             = $row->StyleFinishBaseEndList;

    //Overlay
    $StyleOverlayList                   = $row->StyleOverlayList;
    $StyleMaterialOverlayList           = $row->StyleMaterialOverlayList;
    $StyleColorOverlayList              = $row->StyleColorOverlayList;
    $StyleFinishOverlayList             = $row->StyleFinishOverlayList;

    //DoorSqFt
    $StyleDoorSqFtList                  = $row->StyleDoorSqFtList;
    $StyleMaterialDoorSqFtList          = $row->StyleMaterialDoorSqFtList;
    $StyleColorDoorSqFtList             = $row->StyleColorDoorSqFtList;
    $StyleFinishDoorSqFtList            = $row->StyleFinishDoorSqFtList;

    //DrawerSqFt
    $StyleDrawerSqFtList                = $row->StyleDrawerSqFtList;
    $StyleMaterialDrawerSqFtList        = $row->StyleMaterialDrawerSqFtList;
    $StyleColorDrawerSqFtList           = $row->StyleColorDrawerSqFtList;
    $StyleFinishDrawerSqFtList          = $row->StyleFinishDrawerSqFtList;

    //FinishedInteriorSqFt
    $StyleFinishedInteriorList          = $row->StyleFinishedInteriorList;
    $StyleMaterialFinishedInteriorList  = $row->StyleMaterialFinishedInteriorList;
    $StyleColorFinishedInteriorList     = $row->StyleColorFinishedInteriorList;
    $StyleFinishFinishedInteriorList    = $row->StyleFinishFinishedInteriorList;

    //Panel 1 Sided
    $StylePanel1List                    = $row->StylePanel1List;
    $StyleMaterialPanel1List            = $row->StyleMaterialPanel1List;
    $StyleColorPanel1List               = $row->StyleColorPanel1List;
    $StyleFinishPanel1List              = $row->StyleFinishPanel1List;

    //Panel 1 Sided
    $StylePanel2List                    = $row->StylePanel2List;
    $StyleMaterialPanel2List            = $row->StyleMaterialPanel2List;
    $StyleColorPanel2List               = $row->StyleColorPanel2List;
    $StyleFinishPanel2List              = $row->StyleFinishPanel2List;

    //SpecNames
    $StyleName                          = $row->StyleName;
    $MaterialName                       = $row->MaterialName;
    $ColorName                          = $row->ColorName;
    $FinishName                         = $row->FinishName;
}
//=================================================
// Drawers Pricing
//=================================================

$jobDrwFronts = DB::select(DB::raw("
            SELECT
                `styles`.`ID` AS `StyleID`,
                `styles`.`Name` AS `StyleName`,
                `style_material`.`ID` AS `MaterialID`,
                `style_material`.`Name` AS `MaterialName`,
                `styles`.`DrawerList` AS `StyleDrawerList`,
                `style_material`.`DrawerList` AS `StyleMaterialDrawerList`,
                `style_color`.`DrawerList` AS `StyleColorDrawerList`,
                `style_finish`.`DrawerList` AS `StyleFinishDrawerList`, 
                `style_color`.`ID` AS `ColorID`,
                `style_color`.`Name` AS `ColorName`,
                `style_finish`.`ID` AS `FinishID`,
                `style_finish`.`Name` AS `FinishName`,
                `styles`.`DrawerList` + `style_material`.`DrawerList` + `style_color`.`DrawerList` + `style_finish`.`DrawerList` AS `DrawerList`,
                `styles`.`DrawerSqFtList` + `style_material`.`DrawerSqFtList` + `style_color`.`DrawerSqFtList` + `style_finish`.`DrawerSqFtList` AS `DrawerSqFtList`
            FROM
                `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
            WHERE
                styles.ID = $DrawerStyleID AND style_material.ID = $StyleMaterialID AND style_color.ID = $StyleColorID AND style_finish.ID = $StyleFinishID"));

foreach ($jobDrwFronts as $row) {
    $DrawerList               = $row->DrawerList;
    $DrawerSqFtList           = $row->DrawerSqFtList;
    $StyleDrawerList          = $row->StyleDrawerList;
    $StyleMaterialDrawerList  = $row->StyleMaterialDrawerList;
    $StyleColorDrawerList     = $row->StyleColorDrawerList;
    $StyleFinishDrawerList    = $row->StyleFinishDrawerList;
}
//=================================================
// Large Drawers Pricing
//=================================================
$jobLgDrwFronts = DB::select(DB::raw("
        SELECT
            `styles`.`ID` AS `StyleID`,
            `styles`.`Name` AS `StyleName`,
            `style_material`.`ID` AS `MaterialID`,
            `style_material`.`Name` AS `MaterialName`,
            `style_color`.`ID` AS `ColorID`,
            `style_color`.`Name` AS `ColorName`,
            `style_finish`.`ID` AS `FinishID`,
            `style_finish`.`Name` AS `FinishName`,
            `styles`.`LargeDrawerList` AS `StyleLargeDrawerList`,
            `style_material`.`LargeDrawerList` AS `StyleMaterialLargeDrawerList`,
            `style_color`.`LargeDrawerList` AS `StyleColorLargeDrawerList`,
            `style_finish`.`LargeDrawerList` AS `StyleFinishLargeDrawerList`, 
            `styles`.`LargeDrawerList` + `style_material`.`LargeDrawerList` + `style_color`.`LargeDrawerList` + `style_finish`.`LargeDrawerList` AS `LargeDrawerList`
        FROM
            `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
        WHERE
            styles.ID = $LargeDrawerStyleID AND style_material.ID = $StyleMaterialID AND style_color.ID = $StyleColorID AND style_finish.ID = $StyleFinishID"));

foreach ($jobLgDrwFronts as $row) {
    $LargeDrawerList               = $row->LargeDrawerList;
    $StyleLargeDrawerList          = $row->StyleLargeDrawerList;
    $StyleMaterialLargeDrawerList  = $row->StyleMaterialLargeDrawerList;
    $StyleColorLargeDrawerList     = $row->StyleColorLargeDrawerList;
    $StyleFinishLargeDrawerList    = $row->StyleFinishLargeDrawerList;
}
//if "Custom Quote" is chosen for material, color, finish 
//we need to redo the math for the LIST prices
if ($StyleMaterialCustom != "" || $StyleColorCustom != "" || $StyleFinishCustom != "") {

    $customSpec = 1;

    //should fetch 3 rows for each job
    $jobCustomStyle = DB::select(DB::raw("SELECT DISTINCT * FROM style_custom WHERE JobID = $jobID"));

    foreach ($jobCustomStyle as $row) {

        $csStyleType                      = $row->StyleType;

        $csDoorList                       = $row->DoorList;
        $csTallDoorList                   = $row->TallDoorList;
        $csFullTallDoorList               = $row->FullTallDoorList;

        $csWallEndList                    = $row->WallEndList;
        $csBaseEndList                    = $row->BaseEndList;
        $csTallEndList                    = $row->TallEndList;

        $csDrawerList                     = $row->DrawerList;
        $csLargeDrawerList                = $row->LargeDrawerList;
        $csOverlayList                    = $row->OverlayList;

        $csDrawerSqFtList                 = $row->DrawerSqFtList;
        $csFinishedInteriorList           = $row->FinishedInteriorList;
        $csPanel1SSqFtList                = $row->Panel1SSqFtList;
        $csPanel2SSqFtList                = $row->Panel2SSqFtList;
        $csDoorSqFtList                   = $row->DoorSqFtList;

        $csDoorNameOrdOut                 = $row->DoorNameOrdOut;
        $csDoorTypeOrdOut                 = $row->DoorTypeOrdOut;
        $csDrawerNameOrdOut               = $row->DrawerNameOrdOut;
        $csDrawerTypeOrdOut               = $row->DrawerTypeOrdOut;


        //override the component values for any custom quotes (door, drawer, lg drawer)
        $OverlayList = $OverlayList + $csOverlayList;
        $WallEndList = $WallEndList + $csWallEndList;
        $BaseEndList = $BaseEndList + $csBaseEndList;
        $TallEndList = $TallEndList + $csTallEndList;
        $DrawerSqFtList = $DrawerSqFtList + $csDrawerSqFtList;
        $FinishedInteriorList = $FinishedInteriorList + $csFinishedInteriorList;
        $Panel1SSqFtList = $Panel1SSqFtList + $csPanel1SSqFtList;
        $Panel2SSqFtList = $Panel2SSqFtList + $csPanel2SSqFtList;
        $DoorSqFtList = $DoorSqFtList + $csDoorSqFtList;


        //DOOR
        if ($csStyleType == "Door") {

            //custom spec price (mat, finish, color combined)
            $StyleMaterialDoorList  = $csDoorList;
            $StyleColorDoorList     = 0;
            $StyleFinishDoorList    = 0;

            //set sqft values                    
            $StyleMaterialDoorSqFtList  = $csDoorSqFtList;
            $StyleColorDoorSqFtList     = 0;
            $StyleFinishDoorSqFtList    = 0;

            //total value of door with finishes etc
            $DoorList       = $StyleDoorList + $csDoorList;
            $DoorSqFtList   = $StyleDoorSqFtList + $csDoorSqFtList;

            //DRAWER
        } elseif ($csStyleType == "Drawer") {

            //custom spec price (mat, finish, color combined)
            $StyleMaterialDrawerList  = $csDrawerList;
            $StyleColorDrawreList     = 0;
            $StyleFinishDrawerList    = 0;

            //set sqft values                    
            $StyleMaterialDrawerSqFtList  = $csDrawerSqFtList;
            $StyleColorDrawerSqFtList     = 0;
            $StyleFinishDrawerSqFtList    = 0;

            //total value of door with finishes etc
            $DrawerList         = $StyleDrawerList + $csDrawerList;
            $DrawerSqFtList     = $StyleDrawerSqFtList + $csDrawerSqFtList;

            //LG DRAWER
        } elseif ($csStyleType == "Large Drawer") {

            //custom spec price (mat, finish, color combined)
            $StyleMaterialLargeDrawerList  = $csLargeDrawerList;
            $StyleColorLargeDrawerList     = 0;
            $StyleFinishLargeDrawerList    = 0;

            //set sqft values (large drawer and drawer use same sqft calculations)                   
            $StyleMaterialDrawerSqFtList  = $csDrawerSqFtList;
            $StyleColorDrawerSqFtList     = 0;
            $StyleFinishDrawerSqFtList    = 0;

            //total value of door with finishes etc
            $LargeDrawerList        = $StyleLargeDrawerList + $csLargeDrawerList;
            $DrawerSqFtList         = $StyleDrawerSqFtList + $csDrawerSqFtList;
        } //end type of custom style

    } // end while sql

} // end if custom filled in by customer



//=================================================
// Drawer Boxes Pricing
//=================================================
$jobDrwBox = DB::select(DB::raw("SELECT DISTINCT * FROM cabinet_drwbox WHERE ID = $CabinetDrawerBoxID"));
foreach ($jobDrwBox as $row) {
    $DrawerBoxList          = $row->SmallAvgList;
    $LargeDrawerBoxList     = $row->LargeAvgList;
}

//=================================================
// Edge Banding Pricing (CabinetExteriorEdgeID)
//=================================================
//only run if group allows for edgebanding charges
$EdgeBandingList = 0;
$EdgeBanding = 0;
if ($EdgeBandingOption == 1) { 
    if($CabinetExteriorEdgeID){
        $jobEdgeBanding = DB::select(DB::raw("SELECT DISTINCT * FROM edge_banding WHERE ID = $CabinetExteriorEdgeID"));
            foreach ($jobEdgeBanding as $row) {
                $EdgeBandingList          = $row->List;
                $EdgeBandingName          = $row->Name;
                
                if ($EdgeBandingList > 0) {
                    $EdgeBanding = 1;                  

                } else {
                    $EdgeBanding = 0;
                }
            }
    }
} else {
    $EdgeBanding = 0;
    $EdgeBandingList = 0;
}

if ($UOM == "SqFt") {
   
    //do not count accessory pricing since it's really a front
    $AccessoryItem = 0;

    if ($job_product->Type) {
        $Width         = $job_product->Width; //rewrite if ADD get it from item
        $Height        = $job_product->Height;
        $Depth         = $job_product->Depth;
    }
    $DoorThickness      = 0.75;
    $Panel1SSqFt        = 0;
    $Panel2SSqFt        = 0;
    $DoorSqFt           = 0;
    $DrawerSqFt         = 0;
    if ($Class == "Front") {
        if ($CodePrefixSuffix == 'DOOR__') {
            // echo "[Door]";
            $UnitList           = $DoorSqFtList;
            $DoorSqFt           = (($Width * $Height) / 144);
            $TotalCalc          = $DoorSqFt * $UnitList;
            $TotalList          = $TotalCalc * $qty;
            $DoorList           = $TotalList;
            $Door               = $qty;
            $UOMVal             = $DoorSqFt;
            $DrawerSqFt         = 0;

            $MinWidth           = 6;
            $MinHeight          = 6;
            $MinDepth           = $DoorThickness;

            $MaxWidth           = 26;
            $MaxHeight          = 42;
            $MaxDepth           = $DoorThickness;
            
            $Limit = DB::select(DB::raw("SELECT DISTINCT * FROM style_limit WHERE StyleID = $DoorStyleID AND Type = '$Code' AND Active = 1"));
            
            foreach ($Limit as $row) {
                $MinWidth       = (float)$row->MinWidth;
                $MinHeight      = (float)$row->MinHeight;
                $MinDepth       = $DoorThickness;
                $MaxWidth       = (float)$row->MaxWidth;
                $MaxHeight      = (float)$row->MaxHeight;
                $MaxDepth       = $DoorThickness;
            }
        } elseif ($CodePrefixSuffix == 'TALLDOOR__') {

            $DoorSqFt           = (($Width * $Height) / 144);
            $DrawerSqFt         = 0;

            $MinWidth           = 9;
            $MinHeight          = 9;
            $MinDepth           = $DoorThickness;

            $MaxWidth           = 26;
            $MaxHeight          = 96;
            $MaxDepth           = $DoorThickness;

            $TotalCalc          = $DoorSqFt * $UnitList;
            $TotalList          = $TotalCalc * $qty;
            $UOMVal             = $DoorSqFt;
        } elseif ($CodePrefixSuffix == 'FULLTALLDOOR__') {

            $DoorSqFt           = (($Width * $Height) / 144);
            $DrawerSqFt         = 0;

            $MinWidth           = 9;
            $MinHeight          = 9;
            $MinDepth           = $DoorThickness;

            $MaxWidth           = 26;
            $MaxHeight          = 96;
            $MaxDepth           = $DoorThickness;

            $TotalCalc          = $DoorSqFt * $UnitList;
            $TotalList          = $TotalCalc * $qty;
            $UOMVal             = $DoorSqFt;
        } elseif (strpos($GroupCode, 'DRAWER') !== false) { //contains drawers / largedrawers
            $DoorSqFt           = 0;
            $DrawerSqFt         = (($Width * $Height) / 144);
            $TotalCalc          = $DrawerSqFt * $DrawerSqFtList;
            $TotalList          = $TotalCalc * $qty;
            $DrawerSqFtQty      = $qty;
            $UOMVal             = $DrawerSqFt;
            $DrawerSqFtList     = $TotalCalc;
            $Drawer             = 0; //so it doesn't pass an item of drawer but only the sqft of drawer 
            $LargeDrawer        = 0; //ditto 
            $Limit = DB::select(DB::raw("SELECT DISTINCT * FROM style_limit WHERE StyleID = $DrawerStyleID AND Type = '$Code' AND Active = 1"));

            foreach ($Limit as $row) {
                $MinWidth           = (float)$row->MinWidth;
                $MinHeight          = (float)$row->MinHeight;
                $MinDepth           = (float)$DoorThickness;

                $MaxWidth           = (float)$row->MaxWidth;
                $MaxHeight          = (float)$row->MaxHeight;
                $MaxDepth           = (float)$DoorThickness;
            }
        }
        $UOMVal         = (($Width * $Height) / 144);
        $Depth          = (float)$DoorThickness;
    }
   

    if ($ClassBuild == "Panel1") {
        $GroupCodeFirst6    = false;
        $Panel1SSqFt        = 0;
        $Panel1SSqFt        = (($Width * $Height) / 144);
        $TotalCalc          = $Panel1SSqFt * $Panel1SSqFtList;
        $TotalList          = $TotalCalc * $qty;
        $Panel1SSqFtQty     = 1;
        $UOMVal             = $Panel1SSqFt;
        $Panel1SSqFtList    = $TotalCalc;
    } elseif ($ClassBuild == "Panel2") {

        $GroupCodeFirst6    = false;
        $Panel2SSqFt        = 0;
        $Panel2SSqFt        = (($Width * $Height) / 144);
        $TotalCalc          = $Panel2SSqFt * $Panel2SSqFtList;
        $TotalList          = $TotalCalc * $qty;
        $Panel2SSqFtQty     = 1;
        $UOMVal             = $Panel2SSqFt;
        $Panel2SSqFtList    = $TotalCalc;
    } elseif ($ClassBuild == "Box") {

        $BoxSqFt           = (($Width * $Height) / 144);
        $TotalCalc          = $BoxSqFt * $UnitList; //doesn't look right
        $BoxList            = $TotalCalc * $qty;
        $UOMVal             = $BoxSqFt;
        $Panel1SSqFt        = 0;
        $Panel2SSqFt        = 0;
        $DrawerSqFt         = 0;
        $AccessoryQty       = $BoxSqFt;
    } elseif ($Class == "TOEKICK__") {
        $ToeKickSqFt        = (($Width * $Height) / 144);
        $TotalCalc          = $ToeKickSqFt * $Panel1SSqFtList; //doesn't look right
        $ToeKickList        = $TotalCalc * $qty;
        $UOMVal             = $ToeKickSqFt;
        $Panel1SSqFt        = 0;
        $Panel2SSqFt        = 0;
        $DrawerSqFt         = 0;
        $AccessoryQty       = $ToeKickSqFt;
    } else {
        $UOMVal         = (($Width * $Height) / 144);
        $Panel1SSqFt    = 0;
        $Panel2SSqFt    = 0;
        if (!isset($OrigList)) {
            $OrigList       = $job_product->OrigList;
        }
        $DoorSqFt       = 0;
        $DrawerSqFt     = 0;
        $TotalCalc      = number_format($OrigList * $UOMVal, 2); //uncomment
        $TotalList      = $TotalCalc * $qty; //uncomment                    
    }
} else {

    //reset all sqft variables
    $Panel1SSqFt = 0;
    $Panel2SSqFt = 0;
    $UOMVal = 1;
    $DoorSqFt = 0;
    $DrawerSqFt = 0;
}

//add style charge to this item?
if ($addStyleCharge > 0) {

    //First check to see if it's a stand alone style charge
    if ($addStyleChargeType != "no") {

        $addStyleChargeQty = 1; //addDoorList will always be qty of 1 because qty is truly handled above. 

        if ($addStyleChargeType == "Door") {
            $addDoorList = ($addDoorList + $StyleDoorList);
        } elseif ($addStyleChargeType == "Tall") {
            $addDoorList = ($addDoorList + $StyleTallDoorList);
        } elseif ($addStyleChargeType == "Full") {
            $addDoorList = ($addDoorList + $StyleFullTallDoorList);
        } elseif ($addStyleChargeType == "Drawer") {
            $addDoorList = ($addDoorList + $StyleDrawerList);
        } elseif ($addStyleChargeType == "Lg Drawer") {
            $addDoorList = ($addDoorList + $StyleLargeDrawerList);
        } elseif ($addStyleChargeType == "Overlay") {
            $addDoorList = ($addDoorList + $StyleOverlayList);
        }
    }

    //did they add additional doors to the cost? how many?
    if ($addDoorType == "Door" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleDoorList);
    } elseif ($addDoorType == "Tall" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleTallDoorList);
    } elseif ($addDoorType == "Full" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleFullTallDoorList);
    } elseif ($addDoorType == "Drawer" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleDrawerList);
    } elseif ($addDoorType == "Lg Drawer" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleLargeDrawerList);
    } elseif ($addDoorType == "Overlay" && $addDoorQty > 0) {
        $addDoorList = $addDoorQty * ($addDoorList + $StyleOverlayList);
    }

    //add another door nad how many to the charge?
    if ($addDoorType2 == "Door" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleDoorList);
    } elseif ($addDoorType2 == "Tall" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleTallDoorList);
    } elseif ($addDoorType2 == "Full" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleFullTallDoorList);
    } elseif ($addDoorType2 == "Drawer" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleDrawerList);
    } elseif ($addDoorType2 == "Lg Drawer" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleLargeDrawerList);
    } elseif ($addDoorType2 == "Overlay" && $addDoorQty2 > 0) {
        $addDoorList = $addDoorQty2 * ($addDoorList + $StyleOverlayList);
    }
}
//======================================
// Pricing - Glass Replace/Match
//======================================

if ($FrontSelection != "No" && $FrontSelection != "None" && $FrontSelection != "") {

    //how many doors total?
    $pDoorCount         = $Door;
    $pTallDoorCount     = $TallDoor;
    $pFullTallDoorCount = $FullTallDoor;

    //total number of doors
    $ProductDoorCount   = $Door + $FullTallDoor + $TallDoor;

    //multiply list price by door count (does it make sense in details when displayed for them?)
    $pDoorList          = $DoorList * $Door;
    $pTallDoorList      = $TallDoorList * $TallDoor;
    $pFullTallDoorList  = $FullTallDoorList * $FullTallDoor;

    //how much is it if we do sqft for the glass?
    /* not complete */

    //total cost of existing doors
    $TotalDoorList   = $pDoorList + $pTallDoorList + $pFullTallDoorList;

    //Replace all doors with glass
    //Build query to add up all costs for a glass door
    $FrontSelectionResults = DB::select(DB::raw("SELECT `styles`.`StyleType` AS `StyleType`,
                 
                    `styles`.`ID` AS `StyleID`,
                    `styles`.`Name` AS `StyleName`,
                    `styles`.`LgDrawerStyleID` AS `LgDrawerStyleID`,
                    `styles`.`DrawerStyleID` AS `DrawerStyleID`,
                    
                    `style_material`.`ID` AS `MaterialID`,
                    `style_material`.`Name` AS `MaterialName`,
                    `style_color`.`ID` AS `ColorID`,
                    `style_color`.`Name` AS `ColorName`,
                    `style_finish`.`ID` AS `FinishID`,
                    `style_finish`.`Name` AS `FinishName`,

                    `styles`.`DoorList` + `style_material`.`DoorList` + `style_color`.`DoorList` + `style_finish`.`DoorList` AS `DoorList`,
                    `styles`.`TallDoorList` + `style_material`.`TallDoorList` + `style_color`.`TallDoorList` + `style_finish`.`TallDoorList` AS `TallDoorList`, 
                    `styles`.`FullTallDoorList` + `style_material`.`FullTallDoorList` + `style_color`.`FullTallDoorList` + `style_finish`.`FullTallDoorList` AS `FullTallDoorList`,

                    `styles`.`DoorList` AS `StyleDoorList`,
                    `style_material`.`DoorList` AS `StyleMaterialDoorList`,
                    `style_color`.`DoorList` AS `StyleColorDoorList`,
                    `style_finish`.`DoorList` AS `StyleFinishDoorList`,

                    `styles`.`TallDoorList` AS `StyleTallDoorList`,
                    `style_material`.`TallDoorList` AS `StyleMaterialTallDoorList`,
                    `style_color`.`TallDoorList` AS `StyleColorTallDoorList`,
                    `style_finish`.`TallDoorList` AS `StyleFinishTallDoorList`,

                    `styles`.`FullTallDoorList` AS `StyleFullTallDoorList`,
                    `style_material`.`FullTallDoorList` AS `StyleMaterialFullTallDoorList`,
                    `style_color`.`FullTallDoorList` AS `StyleColorFullTallDoorList`,
                    `style_finish`.`FullTallDoorList` AS `StyleFinishFullTallDoorList`   
                    
                    FROM
                    `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
                    
                    WHERE
                    styles.ID = $FrontSelectionDoor AND
                    style_material.ID = $FrontSelectionMaterial AND
                    style_color.ID = $FrontSelectionColor AND
                    style_finish.ID = $FrontSelectionFinish"));



    //apply the spec for the door instead of the construction spec and then return that as the door component
    foreach ($FrontSelectionResults as $row) {
        $StyleName                      = $row->StyleName;
        $MaterialName                   = $row->MaterialName;
        $ColorName                      = $row->ColorName;
        $FinishName                     = $row->FinishName;


        //Door  (not matching door)
        if ($pDoorCount > 0 && $FrontSelectionDoor != 188) {
            $DoorList                       = $row->DoorList; //cost of a glass door
            $StyleDoorList                  = $row->StyleDoorList;
            $StyleMaterialDoorList          = $row->StyleMaterialDoorList;
            $StyleColorDoorList             = $row->StyleColorDoorList;
            $StyleFinishDoorList            = $row->StyleFinishDoorList;
            $StyleDoorType                  = "Glass";

            //tall door (not matching)
        } elseif ($pTallDoorCount > 0 && $FrontSelectionDoor != 188) {
            $TallDoorList                   = $row->TallDoorList; //cost of a glass door
            $StyleTallDoorList              = $row->StyleTallDoorList;
            $StyleMaterialTallDoorList      = $row->StyleMaterialTallDoorList;
            $StyleColorTallDoorList         = $row->StyleColorTallDoorList;
            $StyleFinishTallDoorList        = $row->StyleFinishTallDoorList;
            $StyleTallType                  = "Glass";
        } elseif ($pFullTallDoorCount > 0 && $FrontSelectionDoor != 188) {
            $FullTallDoorList               = $row->FullTallDoorList;
            $StyleFullTallDoorList          = $row->StyleFullTallDoorList;
            $StyleMaterialFullTallDoorList  = $row->StyleMaterialFullTallDoorList;
            $StyleColorFullTallDoorList     = $row->StyleColorFullTallDoorList;
            $StyleFinishFullTallDoorList    = $row->StyleFinishFullTallDoorList;
            $StyleFullTallType              = "Glass";
        } else {
            $GlassUpcharge       = $ProductDoorCount;
        }
        //GLASS component
        $GlassDoorList                   = $DoorList + $TallDoorList + $FullTallDoorList; //cost of a glass door
        $StyleGlassDoorList              = $StyleDoorList + $StyleTallDoorList + $StyleFullTallDoorList;
        $StyleMaterialGlassDoorList      = $StyleMaterialDoorList + $StyleMaterialTallDoorList + $StyleMaterialFullTallDoorList;
        $StyleColorGlassDoorList         = $StyleColorDoorList + $StyleColorTallDoorList + $StyleColorFullTallDoorList;
        $StyleFinishGlassDoorList        = $StyleFinishDoorList + $StyleFinishTallDoorList + $StyleFinishFullTallDoorList;


        if ($FrontSelectionDoor != 188) {
            $StyleDoorType      = "Glass - REPLACE Door";
            $StyleFullTallType  = "Glass - REPLACE Door";
            $StyleTallType      = "Glass - REPLACE Door";
            $StyleGlassType     = "see door";
        } else {
            $StyleDoorType      = "Glass - Match Door";
            $StyleFullTallType  = "Glass - Match Door";
            $StyleTallType      = "Glass - Match Door";
            $StyleGlassType     = "Upcharge";
        }

        $GlassList = $GlassDoorList;
    } //end while    

}

//======================================
// /* Pricing - Glass Replace/Match
//======================================


//Interior Finished?
if ($FinishedInteriorSelected == 'yes') {
    $partsResults = "SELECT Code, Type, Description, Width, Height, Depth, Qty, UOMPer, UOMQty FROM `product_parts` WHERE Code = '$Code' AND (Type = '1S' OR Type = '2S')";
    $partsSql = DB::select(DB::raw($partsResults));
    foreach ($partsSql as $row) {

        $pCode      = $row->Code;
        $pType      = $row->Type;
        $pDes       = $row->Description;
        $pW         = $row->Width;
        $pH         = $row->Height;
        $pD         = $row->Depth;
        $pQty       = $row->Qty;
        $pUPer      = $row->UOMPer;
        $pUQty      = $row->UOMQty;

        //add up all the uomQTY
        if ($pType == "2S") {
            $totalSqft = ($pUQty * 2) + $totalSqft;
        } else {
            $totalSqft =  ($pUQty + $totalSqft);
        }

        $FinishedInterior = $totalSqft;
    }
}

//=================================================================================
// Pricing - Door Options (Outside Profile, Inside Profile, Centerpanel, StileRail)
//=================================================================================

$DoorOptionsInsideList      = 0.00;
$DoorOptionsOutsideList     = 0.00;
$DoorOptionsCenterList      = 0.00;
$DoorOptionsStileList       = 0.00;

//INSIDE PROFILE
if (isset($DoorInsideProfileID)) {
    $jobDoorOptions = DB::select(DB::raw("SELECT ID, Code, DoorList, TallDoorList, FullTallDoorList,  List FROM style_insideprofile WHERE ID =  $DoorInsideProfileID"));
    foreach ($jobDoorOptions as $row) {

        $InsideList             = $row->DoorList;
        $InsideTallList         = $row->TallDoorList;
        $InsideFullTallList     = $row->TallDoorList;
        //door
        if ($Door > 0) {
            $InsideList   = $InsideList * $Door;
        }
        //tall door
        if ($TallDoor > 0) {
            $InsideTallList   = $InsideTallList * $TallDoor;
        }
        //full tall door
        if ($FullTallDoor > 0) {
            $InsideFullTallList   = $InsideFullTallList * $FullTallDoor;
        }
        //Total up the doors * door options list
        $DoorOptionsInsideList  = $InsideList + $InsideTallList + $InsideFullTallList; //legacy
    }
}
//OUTSIDE PROFILE
if (isset($DoorOutsideProfileID)) {
    $jobDoorOptions = DB::select(DB::raw("SELECT ID, Code, DoorList, TallDoorList, FullTallDoorList,  List FROM style_outsideprofile WHERE ID = $DoorOutsideProfileID"));
    foreach ($jobDoorOptions as $row) {

        $OutsideList             = $row->DoorList;
        $OutsideTallList         = $row->TallDoorList;
        $OutsideFullTallList     = $row->FullTallDoorList;

        //door
        if ($Door > 0) {
            $OutsideList   = $OutsideList * $Door;
        }

        //tall door
        if ($TallDoor > 0) {
            $OutsideTallList   = $OutsideTallList * $TallDoor;
        }

        //full tall door
        if ($FullTallDoor > 0) {
            $OutsideFullTallList   = $OutsideFullTallList * $FullTallDoor;
        }

        //Total up the doors * door options list
        $DoorOptionsOutsideList  = $OutsideList + $OutsideTallList + $OutsideFullTallList; //legacy
        //  echo "<script>alert('DoorOptionsOutsideList: ' + $DoorOptionsOutsideList + $OutsideList + $OutsideTallList + $OutsideFullTallList);</script>";

    }
}

//CENTER PANEL
if (isset($DoorCenterPanelID)) {
    $jobDoorOptions = DB::select(DB::raw("SELECT ID, Code, DoorList, TallDoorList, FullTallDoorList, List FROM style_centerpanel WHERE ID = $DoorCenterPanelID"));
    foreach ($jobDoorOptions as $row) {
        $CenterList             = $row->DoorList;
        $CenterTallList         = $row->TallDoorList;
        $CenterFullTallList     = $row->FullTallDoorList;

        //door
        if ($Door > 0) {
            $CenterList   = $CenterList * $Door;
        }

        //tall door
        if ($TallDoor > 0) {
            $CenterTallList   = $CenterTallList * $TallDoor;
        }

        //full tall door
        if ($FullTallDoor > 0) {
            $CenterFullTallList   = $CenterFullTallList * $FullTallDoor;
        }

        //Total up the doors * door options list
        $DoorOptionsCenterList  = $CenterList + $CenterTallList + $CenterFullTallList;
    }
}

//Stiles & Rails
if (isset($DoorStileRailID)) {
    $jobDoorOptions = DB::select(DB::raw("SELECT ID, Code, DoorList, TallDoorList, FullTallDoorList, List FROM style_stilerail WHERE ID = $DoorStileRailID"));
    foreach ($jobDoorOptions as $row) {
        $StileList             = $row->DoorList;
        $StileTallList         = $row->TallDoorList;
        $StileFullTallList     = $row->FullTallDoorList;
        //door
        if ($Door > 0) {
            $StileList   = $StileList * $Door;
        }
        //tall door
        if ($TallDoor > 0) {
            $StileTallList   = $StileTallList * $TallDoor;
        }
        //full tall door
        if ($FullTallDoor > 0) {
            $StileFullTallList   = $StileFullTallList * $FullTallDoor;
        }
        //Total up the doors * door options list
        $DoorOptionsStileList  = $StileList + $StileTallList + $StileFullTallList; //legacy
    }
}

//Total up all door options
$DoorOptionsList = 0;
$DoorOptionsList = $DoorOptionsList + $DoorOptionsInsideList;
$DoorOptionsList = $DoorOptionsList + $DoorOptionsOutsideList;
$DoorOptionsList = $DoorOptionsList + $DoorOptionsCenterList;
$DoorOptionsList = $DoorOptionsList + $DoorOptionsStileList;
//Add Only
// if($add_or_edit == 'add'){

$OrigList       = number_format($UnitList, 2, '.', '');
$OrigCost       = number_format($UnitList * $AcctMultDis, 2, '.', '');
$OrigCustomer   = number_format((float)$UnitList * (float)$AcctMultDis * (float)$AcctMultCus, 2, '.', '');


$FrontList      =   ($DoorList * $Door) +
    ($TallDoorList * $TallDoor) +
    ($FullTallDoorList * $FullTallDoor) +
    ($DrawerList * $Drawer) +
    ($LargeDrawerList * $LargeDrawer) +
    ($OverlayList * $Overlay)
    + $addDoorList + $HandleList + $EdgeBandingList;

$DrwBoxCalc     = ($DrawerBoxList * $DrawerBox) + ($LargeDrawerBoxList * $LargeDrawerBox);

$DrwBoxList     = isset($DrwBoxCalc) ? $DrwBoxCalc : '0';
$FinEndList     = isset($FinEndList) ? $FinEndList : '0';
$FinIntList     = isset($FinIntList) ? $FinIntList : '0';
$ModList        = isset($ModList) ? $ModList : '0';
$HandleList     = isset($HandleList) ? $HandleList : '0.00';

// $TotalCalc = ($OrigList*$UOMVal)+$FrontList+$DrwBoxList+$FinEndList+$FinIntList+$ModList;
// $TotalCalc      = 5;/
$TotalCalc = ($OrigList * $UOMVal) + $FrontList + $DrwBoxList + $FinEndList + $FinIntList + $ModList + $ToeKickList;

$TotalList      = number_format($TotalCalc, 2, '.', '');
$TotalCost      = number_format($TotalCalc * $AcctMultDis, 2, '.', '');
$TotalCustomer  = number_format($TotalCalc * $AcctMultDis * $AcctMultCus, 2, '.', '');

//=====================================
// Dimensions Pricing Math
//=====================================

$WidthDimChange         = 0;
$HeightDimChange        = 0;
$DepthDimChange         = 0;
$DICQty                 = 0;

if ($WidthDimChange > 0) {
    $DICQty += $WidthDimChange;
}
if ($HeightDimChange > 0) {
    $DICQty += $HeightDimChange;
}
if ($DepthDimChange > 0) {
    $DICQty += $DepthDimChange;
}
//===================================
// /* Dimensions Pricing Math
//===================================

//swap pricing to component if it's an accessory version of the item like loose door
if ($itemStr == "accessory" && $Drawer > 0) {
    $AccessoryItem = 0;
    $OrigList = $DrawerList;
} elseif ($itemStr == "accessory" && $LargeDrawer > 0) {
    $AccessoryItem = 0;
    $OrigList = $LargeDrawerList;
} elseif ($itemStr == "accessory" && $Overlay > 0) {
    $AccessoryItem = 0;
    $OrigList = $OverlayList;
} elseif ($itemStr == "accessory" && $Door > 0) {
    $AccessoryItem = 0;
    $OrigList = $DoorList;
}


?>


<body>
    <!-- START:navbar -->
    <!-- END:navbar -->

    <input type="hidden" name="product_group_id" value="<?= $product_group_id ?>">
  
    @include('partials.products.add_edit_modal_details')
    @include('partials.products.add_edit_hidden_inputs')
    </div><!-- END:primary content container -->



    
    <script type="text/javascript" src="{{ asset('js/product_style_change.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/product_material_change.js') }}"></script>
    <div id="response" name="response"></div>

    <script type="text/javascript">
        $('document').ready(function() {

            //hinging image swap
            $("#hinging-select").change(setCabinetImageOrientation);
            setCabinetImageOrientation();

            //FinishedEnd call to pop up message
            $("[name='FinishedEnd']").change(function() {
                var MaterialID = $("[name='prodStyMaterialID']").val();
                console.log("material id: " + MaterialID);
                getPopUpMessage("FinishedEnd", MaterialID);
            });

        });

        function setCabinetImageOrientation() {
            var Hinge = $("#hinging-select").val();
            if (Hinge === 'Left') {
                $('#prodImg').addClass('reflection');
            } else {
                $('#prodImg').removeClass('reflection');
            }
        };
    </script>

