@include('templates.function')
<?php
$SesType = session('accounttype');

//get productType variables from functions.php
$itemArray          = getProductType($job_product);
$productType        = $itemArray["productType"];
$itemType             = $itemArray["itemType"];
$groupType             = $itemArray["groupType"];
$pricingType         = $itemArray["pricingType"];
$noteType             = $itemArray["noteType"];
$itemTable             = $itemArray["itemTable"];
$groupTable         = $itemArray["groupTable"];

$fullCode =  getORDCode($job_product);  //format code to machine readable with dimensions
$Code = $job_product->$itemType->Code;
$Code = getORDCodeClean($Code);
//Get the proper width depending on if we're editing or creating it from scratch
if ($job_product->Type != "") {
    $pageType               = "show";
    $productType            = $job_product->Type;
    $Width                  = $job_product->Width;
    $Height                 = $job_product->Height;
    $Depth                  = $job_product->Depth;
} else {
    $pageType               = "create";
    $Width                  = $job_product->$itemType->Width;
    $Height                 = $job_product->$itemType->Height;
    $Depth                  = $job_product->$itemType->Depth;
}
?>

<div class="row">
    <div class="col-xs-12 col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">
                <strong>{{ $job_product->$itemType->Description }} </strong>, {{ $fullCode }}
                <span class="pull-right">{{ $job_product->job->Fin }} {{ $job_product->job->specifications->door->name }} / {{ $job_product->job->specifications->style->material }} / {{ $job_product->job->specifications->style->color }} / {{ $job_product->job->specifications->style->finish }}</span>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="col-xs-12">

                            <?php
                            //accessory needs the item image and product needs a group image
                            if ($productType == "accessory") {
                                $filePath = public_path() . '/images/accessories/' . $job_product->$itemType->Image_File;
                                $address = '/images/accessories/' . $job_product->$itemType->Image_File;
                            } else {
                                $filePath = public_path() . '/images/cabinets/' . $job_product->$groupType->Image_File;
                                $address = '/images/cabinets/' . $job_product->$groupType->Image_File;
                            }
                            echo '<img src="' . $address . '" class="img-responsive" id="prodImg" alt="' . $address . '">';
                            ?>
                            <br />
                            <div class="text-center"><small> {{ $Code  }} </small> / <small> {{ $job_product->$itemType->GroupCode }} </small></div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <ul>
                            <?php
                            // echo "no notes here";
                            // if ($job_product->$itemType->$groupType->Extended_Notes) {
                            // print_r($job_product->$itemType->$groupType->Extended_Notes);

                                // $notes_formated = '';
                                // $Note = $job_product->$itemType->$groupType->extended_note;
                                // print_r($Note);
                                // $note_lines = explode("\n", $Note);
                                // print_r($note_lines);

                                // foreach ($job_product->$itemType->$groupType->extended_notes as $extended_note) {
                                    // echo "<li>" . $extended_note . "</li>";
                                // }
                                /*$debug = "";
                                foreach ($note_lines as $note_line) {

                                    if (isset($note_line)) {
                                        $notes_check    = "<li>" . $note_line . "</li>";
                                        $mystring       = $notes_check;
                                        $findme         = '><'; //lookign for <li><li> meaning an empty bullet
                                        $pos            = strpos($mystring, $findme);

                                        if (!$pos) {
                                            $notes_formated .= $notes_check;
                                        } else {
                                            $notes_check = "";
                                            $notes_formated .= $notes_check;
                                        }
                                        $notes_check = "";
                                        $mystring = "";
                                        $findme = "";
                                        $pos = "";
                                    }
                                }*/
                                // echo $notes_formated;
                            // }
                            $notes_formated = '';
                            // $Note = $job_product->$itemType->Notes;
                            if($job_product->$itemType->$groupType){
                                $note_lines = $job_product->$itemType->$groupType->extended_notes;

                                $debug = "";
                            foreach ($note_lines as $note_line) {
                                if (isset($note_line)) {
                                    $notes_check    = "<li>" . $note_line . "</li>";
                                    $mystring       = $notes_check;
                                    $findme         = '><'; //lookign for <li><li> meaning an empty bullet
                                    $pos            = strpos($mystring, $findme);

                                    //if you found <li></li> then don't add to notes lines
                                    if (!$pos) {
                                        $notes_formated .= $notes_check;
                                    } else {
                                        $notes_check = "";
                                        $notes_formated .= $notes_check;
                                    }
                                    $notes_check = "";
                                    $mystring = "";
                                    $findme = "";
                                    $pos = "";
                                }
                            }
                            echo $notes_formated;
                            }else{
                                $note_lines = "";
                            }
                            // print_r($note_lines);
                            // $Note = parseCodes($Note, $job_product);
                            // $note_lines = explode("\n", $Note);
                            
                            // echo "<li>" . $job_product->$groupType->Description . "</li>";
                            ?>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <div class="form-horizontal">
                            <table class="table table-hover" style="margin-bottom:0px;">
                                <tbody>
                                    <tr>
                                        <td width="20%" class="text-right">
                                            <label for="prodQty" class="control-label">Quantity</label>
                                        </td>
                                        <td width="50%" colspan="">
                                            <input type="hidden" name="product_item_id" value="{{ $job_product->product_item_id }}">
                                            <input id="prodQty" name="Qty" value="{{ $job_product->Qty }}" class="form-control" type="number" min="1" step="1">
                                        </td>
                                        <td width="30%" colspan="">
                                            <?php
                                            //if (session('accounttype') == -1) {
                                            ?>
                                                <button type="button" class="btn btn-primary" style="width:100%;" data-toggle="modal" data-target="#Detail">Details</button>
                                            <?php
                                            //} //end if
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">
                                            <label for="prodWidth" class="control-label">Width</label>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <?php
                                                if ($SesType != -1) {
                                                    $minW = $job_product->$itemType->MinWidth;
                                                    $maxW = $job_product->$itemType->MaxWidth;
                                                    $minH = $job_product->$itemType->MinHeight;
                                                    $maxH = $job_product->$itemType->MaxHeight;
                                                    $minD = $job_product->$itemType->MinDepth;
                                                    $maxD = $job_product->$itemType->MaxDepth;
                                                } else {
                                                    $minW = 0;
                                                    $maxW = 200;
                                                    $minH = 0;
                                                    $maxH = 200;
                                                    $minD = 0;
                                                    $maxD = 200;
                                                }
                                                ?>
                                                <input type="number" value="{{ (float)$Width }}" min="{{ (float)$minW }}" max="{{ $maxW }}" step="0.0625" style="width:100%;" class="form-control" name="Width" id="prodWidth">
                                                <span class="input-group-addon">"</span>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <div class="small">
                                                <strong>Min: </strong>
                                                <span id="DisMinWidth">
                                                    {{ (float)$job_product->$itemType->MinWidth }}"
                                                </span>
                                                <br>
                                                <strong>Max: </strong>
                                                <span id="DisMaxWidth">
                                                    {{ (float)$job_product->$itemType->MaxWidth }}"
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">
                                            <label for="prodHeight" class="control-label">Height</label>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" value="{{ (float)$Height }}" min="{{ (float)$minH }}" max="{{ $maxH }}" step="0.0625" style="width:100%;" class="form-control" name="Height" id="prodHeight">
                                                <span class="input-group-addon">"</span>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <div class="small">
                                                <strong>Min: </strong>
                                                <span id="DisMinHeight">
                                                    {{ (float)$job_product->$itemType->MinHeight }}"
                                                </span>
                                                <br>
                                                <strong>Max: </strong>
                                                <span id="DisMaxHeight">
                                                    {{ (float)$job_product->$itemType->MaxHeight }}"
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="text-right">
                                            <label for="prodDepth" class="control-label">Depth</label>
                                        </td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" value="{{ (float)$Depth }}" min="{{ (float)$minD }}" max="{{ $maxD }}" step="0.0625" style="width:100%;" class="form-control" name="Depth" id="prodDepth">
                                                <span class="input-group-addon">"</span>
                                            </div>
                                        </td>
                                        <td class="text-left">
                                            <div class="small">
                                                <strong>Min: </strong>
                                                <span id="DisMinDepth">
                                                    {{ (float)$job_product->$itemType->MinDepth }}"
                                                </span>
                                                <br>
                                                <strong>Max: </strong>
                                                <span id="DisMaxDepth">
                                                    {{ (float)$job_product->$itemType->MaxDepth }}"
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td colspan="2" class="text-left">
                                            <small>Rounding to the nearest 16th (0.0625")</small>
                                        </td>
                                        <td>
                                            
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                            <div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>