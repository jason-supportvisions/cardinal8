<?php 
	//we have to do some different things if we're on a SHOW blade versus a CREATE blade
	//if it's SHOW we have to pull the main vars from the job_product
    //if it's CREATE there are no records in job_product so we pull default from product or accessory item tables    

        //get productType variables from functions.php
            $itemArray          = getProductType($job_product); 
            $productType		= $itemArray["productType"]; 
            $itemType 			= $itemArray["itemType"]; 
            $groupType 		    = $itemArray["groupType"]; 
            $pricingType 		= $itemArray["pricingType"]; 
            $noteType 			= $itemArray["noteType"]; 
            $itemTable 		    = $itemArray["itemTable"];  
            $groupTable 		= $itemArray["groupTable"]; 

        //hide this section if it's an accessory
        if($job_product->$itemType->$groupType){
            if ($job_product->$itemType->$groupType->ShowMods != 1) {
                $txtAreaRows = 7; //make notes field bigger if accessory
                $hideClass = "visibility: hidden";
            }
        }else{
            $txtAreaRows = 4; //default notes text area size
            $hideClass = "wee";
        }

?>
<style>
    a{
        cursor: pointer;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script>


$(document).ready(function() {
    $("#modSearch").on("keyup", function() {
        var value = $(this).val().toLowerCase();
        $("#mod-table tr").filter(function() {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});
</script>


<!-- hide mods for accessory -->

<div class="{{ $hideClass ?? '' }}">
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-UX">
                <div class="panel-heading">Add Modification <small>(to add modification, fill in a value) -
                        {{ $job_product->$itemType->Category }}</small></div>
                <div class="panel-body">
                    <div>
                        <input id="modSearch" type="text" placeholder=" Filter Modifications">
                        <?php 
                $SesType = session('accounttype');
                if($SesType == "-1")  {
                    echo "&nbsp;&nbsp;&nbsp; <span class='bg-info'> &nbsp;&nbsp;&nbsp;* blue modification rows only visible to Administrators &nbsp;&nbsp;&nbsp;</span>";
                }
                ?>
                        <br /><br />
                    </div>
                    <ul class="nav nav-tabs" id="nav-tabs">
                        @foreach($modification_item_types as $i => $modification_item_type)
                        <li <?php if($i==0): ?>class="active" <?php endif ?>>
                            <a data-toggle="pill" href="#{{ $modification_item_type->slug }}">
                                {{ $modification_item_type->type }}
                            </a>
                        </li>
                        @endforeach
                    </ul>

                    <div class="tab-content" id="tab-content">
                        @foreach($modification_item_types as $i => $modification_item_type)
                        <div id="{{ $modification_item_type->slug }}"
                            class="tab-pane fade <?php if($i==0): ?>active in<?php endif ?>">
                            <div class="table-responsive">
                                <table class="table table-hover" id="mod-table">
                                    <thead>
                                        <tr>
                                            <th width="15%">Code</th>
                                            <th width="40%">Modification</th>
                                            <th width="20%">Note</th>
                                            <th width="15%">Value</th>
                                            <th width="10%" class="text-center">List</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
                                            $i              = 0;
                                            $productCat     = strtolower($job_product->$itemType->Category); //item cat
                                            $SesType        = session('accounttype')                            
                                        ?>
                                        {{-- Only show active mods --}}
                                        @foreach($modification_item_type->active_items as $item)
                                        <?php                                            
                                            $itemCategories = explode(',', $item->Class); //categories to show mod in [array] 
                                            $i              = $i + 1;
                                            $show           = 0;                                            
                                            // loop through array compare to product cat and decide to show or hide                                            
                                            foreach($itemCategories as $itemCat){                                                
                                                $itemCat = strtolower($itemCat); //lcase
                                                $productCat = strtolower($productCat); //lcase 
                                                //if they match flag it to show
                                                if($productCat == $itemCat){
                                                    $show = $show + 1;                                          
                                                }else{
                                                    $show = $show + 0;                                               
                                                }
                                            }                                            
                                            $showModToUser = 0;
                                            $showModToAdmin = 0;
                                            $showModHasValue = 0;
                                            // loop through array of already saved mods and make sure they're flagged to show    
                                            if(isset($saved_mods_array))  {                                      
                                                foreach($saved_mods_array as $savedMod){                                                    
                                                    if($item->ID == $savedMod->modification_item_id){
                                                        $showModHasValue = $showModHasValue + 1;
                                                    }else{
                                                        $showModHasValue = $showModHasValue + 0;
                                                    }
                                                }
                                            }
                                            // if any of the class types match the mod type
                                            // set it to show the mod
                                            if($show > 0){
                                                    $showModToUser = $showModToUser + 1;
                                                    $flagClass = "";                                                  
                                            }else{
                                                    $showModToUser = $showModToUser + 0;
                                                    $flagClass = " d-none"; 
                                            }                                            
                                            // if admin is logged in, show any active mod and flag it a different color
                                            if($show == 0){
                                            if($SesType == "-1" || $showModHasValue > 0)  {
                                                        $showModToAdmin = $showModToAdmin + 1;
                                                        $flagClass = "bg-info";                                                       
                                                }
                                            }   
                                            if ($showModToAdmin > 0 || $showModToUser > 0 || $showModHasValue > 0){
                                                //assign some variables
                                                $itemCost           = number_format($item->Cost, 2, '.', ''); 
                                                $itemList           = number_format($item->List, 2, '.', '');                                        
                                                $itemOrder          = $item->mod_order; 
                                                $itemCode           = $item->Code;
                                                $itemNote           = $item->Note; 
                                                $itemID             = $item->ID;
                                                $itemDescription    = $item->Description;
                                                $itemAddDoor        = $item->AddDoor;                                                 
                                                //handle admin override of price
                                                foreach($job_product->modifications as $jpmod){
                                                    if($itemCode == $jpmod->Code){
                                                        if($jpmod->AdminList != 0){
                                                            $itemList = $jpmod->AdminList;
                                                        }elseif($jpmod->ListPer != 0){
                                                            $itemList = $jpmod->ListPer;
                                                        }else{
                                                            $itemList = number_format($item->ListPer, 2, '.', '');  
                                                        }                                                    
                                                    }
                                                }
                                        ?>

                                        
                                        <tr class="mod-tr" id="mod-tr">
                                            <input type="hidden" class="mod-cost-per-input" value="{{ $itemCost }}">
                                            <input type="hidden" class="mod-list-per-input" value="{{ $itemList }}">
                                            <input type="hidden" class="mod-list-order" value="{{ $itemOrder }}">
                                            <td style="color:gray;font-size:14px;">
                                                {{ $itemCode }}
                                            </td>
                                            <td>
                                                {{ $itemDescription }}
                                             

                                                @if($note = $itemNote)
                                                <a data-placement="right" data-toggle="tooltip" data-placement="top"
                                                    data-content="{{$note }}" data-original-title="{{ $note }}"
                                                    title="{{ $note }}">(?)</a>
                                                @endif
                                            </td>
                                            <td>
                                                <div>
                                                    <textarea rows="1"
                                                        @if($item->Value1Type == "text" && $SesType != -1)
                                                            readonly
                                                            @else
                                                            placeholder="Note to manufacturer"
                                                            @endif
                                                            class="form-control mod-value-input"
                                                            style="width: 100%"
                                                            name="modification_items[{{ $itemID }}][Notes]"
                                                        ><?php if (isset($saved_mods_array[$itemID])){echo $saved_mods_array[$itemID]->Notes;}?></textarea>
                                                </div>
                                            </td>
                                            <td>
                                                <div>
                                                    @if($item->Value1Type == "int")
                                                    <input type="number" step="1" min="{{ $item->Value1Min }}"
                                                        max="{{ $item->Value1Max }}" class="form-control mod-qty-input"
                                                        style="width: 100%"
                                                        placeholder="Qty (Max: {{ round($item->Value1Max,2) }})"
                                                        name="modification_items[{{ $itemID }}][Value1]"
                                                        @if(isset($saved_mods_array[$itemID]))
                                                        value="{{ $saved_mods_array[$itemID]->Value1 }}" @endif>
                                                    @elseif($item->Value1Type == "meas")
                                                    <input type="number" step="0.0625" min="{{ $item->Value1Min }}"
                                                        max="{{ $item->Value1Max }}" class="form-control mod-qty-input"
                                                        style="width: 100%" placeholder="Dimension"
                                                        name="modification_items[{{ $itemID }}][Value1]"
                                                        @if(isset($saved_mods_array[$itemID]))
                                                        value="{{ $saved_mods_array[$itemID]->Value1 }}" @endif>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small
                                                        style="color:gray; text-align:center;">(Min:
                                                        {{ round($item->Value1Min,4) }} Max:
                                                        {{ round($item->Value1Max, 4) }})</small>
                                                    @elseif($item->Value1Type == "bool")
                                                    <center>
                                                        <input type="checkbox" value="1"
                                                            name="modification_items[{{ $itemID }}][Value1]"
                                                            @if(isset($saved_mods_array[$itemID])) checked @endif>
                                                    </center>
                                                    @elseif($item->Value1Type == "text")
                                                    <input class="form-control mod-qty-input" style="width: 100%"
                                                        placeholder="Text"
                                                        name="modification_items[{{ $itemID }}][Value1]"
                                                        @if(isset($saved_mods_array[$itemID]))
                                                        value="{{ $saved_mods_array[$itemID]->Value1 }}" @endif>
                                                    @endif
                                                </div>
                                            </td>
                                            <td class="text-center nowrap" width="400">
                                                <div class="input-group">
                                                    <span class="input-group-addon">$</span>
                                                    <input type="text" style="text-align:right;min-width:80px"
                                                        class="form-control mod-list-input"
                                                        name="modification_items[{{ $itemID }}][ListPer]"
                                                        id="ModList[671]"
                                                        <?php if($SesType != -1){ echo 'readonly="readonly"'; }
                                                        // dd($saved_mods_array[$itemID]);
                                                        ?> 
                                                        @if(isset($saved_mods_array[$itemID]))
                                                            value="{{ round($itemList,2) }}" 
                                                        @else
                                                            value="{{ round($itemList,2) }}" 
                                                        @endif
                                                    >
                                                </div>
                                            
                                            </td>
                                        </tr>
                                        <?php                                      

                                        }//end foreach category in the mod list
                                            ?>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div><!-- END Panel Body -->
            </div><!-- END Panel -->
        </div><!-- END col-sm-12 -->
    </div>
</div>
{{-- <button type="button" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Tooltip on top">
    Tooltip on top
  </button> --}}
<Script>
    $(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>