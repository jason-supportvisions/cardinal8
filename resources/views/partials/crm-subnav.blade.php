<?php 

$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$subnav = '';
if (strpos($path, 'crm/orders') !== false) {
    $subnav = 'orders';
}elseif(strpos($path, 'crm/quotes') !== false) {
    $subnav = 'quotes';
}elseif(strpos($path, 'crm/phone-log') !== false) {
    $subnav = 'phone-log';
}elseif(strpos($path, 'crm/contacts') !== false) {
    $subnav = 'accounts';
}elseif(strpos($path, 'settings') !== false) {
    $subnav = 'administration';
}

?>

<div style="background: red; height:1px;"></div>
<div class="navbar navbar-default">
    <ul class="nav navbar-nav">

        <li <?php if($subnav=='orders'): ?> class="active" <?php endif ?>>
            <a data-toggle="dropdown" class="dropdown-toggle text-darkblue">
                Orders
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="/crm/orders">
                        <span>Overview</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/under-review">
                        <span>Under Review</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/confirmed">
                        <span>Confirmed</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/accepted">
                        <span>Accepted</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/shipping">
                        <span>Shipping</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/completed">
                        <span>Completed</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/cancelled">
                        <span>Cancelled</span>
                    </a>
                </li>
                <li>
                    <a href="/crm/orders/all">
                        <span>All</span>
                    </a>
                </li>
                            
            </ul>
        </li>
        <li class="<?php if($subnav =='quotes'): ?>active<?php endif ?>">
            <a href="/crm/quotes">
                Quotes
            </a>
        </li>
        <li class="<?php if($subnav =='phone-log'): ?>active<?php endif ?>">
            <a href="/crm/phone-log">
                Phone Log
            </a>
        </li>

        <li class="<?php if($subnav =='accounts'): ?>active<?php endif ?>">
            <a data-toggle="dropdown" class="dropdown-toggle text-darkblue">
                Accounts
                <b class="caret"></b>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="/crm/contacts">
                        <span>Contacts</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="<?php if($subnav =='administration'): ?>active<?php endif ?>">
            <a data-toggle="dropdown" class="dropdown-toggle text-darkblue">
                Administration
                <b class="caret"></b>
            </a>

            <ul class="dropdown-menu">
                <li>
                    <a href="/admin/account">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span>Account Settings</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('all-users')}}">
                        <i class="glyphicon glyphicon-cog"></i>
                        <span>User Settings</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>