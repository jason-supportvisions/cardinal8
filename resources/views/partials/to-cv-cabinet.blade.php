@include('templates.function')
<?php
//print_r($product);
//echo "test data: " .  $product->style->Name;

        //remove extraneous titles ending in : -- and remove space after commma
        $cabinetTextNoColon = preg_replace("/(\w+):/", " ", $product->cabfinext_text);

        //replace space after commas
        $cabinetTextNoColon = str_replace(', ', ',', $cabinetTextNoColon);

        //remove leading white space
        $cabinetTextNoColon = ltrim($cabinetTextNoColon);

        //escape quotes in descriptions
        $htmlDescription = htmlspecialchars_decode($product->Description);

         //if panel then reverse width and depth for output code dimensions
         $dimensions = substr($product->Width, 0, -2) . "," . substr($product->Height, 0, -2) . "," . substr($product->Depth, 0, -2);
        $revDimensions = substr($product->Depth, 0, -2) . "," . substr($product->Height, 0, -2) . "," . substr($product->Width, 0, -2);
        $panelCode = "";
        if($product->accessory_item != null){
            $panelCode = strpos($product->accessory_item->GroupCode, "PANEL");
        }
        if($panelCode != ""){
                $accDimensions = $dimensions; //keep same, not a panel
        }else{
                $accDimensions = $revDimensions;  //reverse depth/width
        }
?>
[Parameters]
Attribute="Cabinet Number","CABNO","int",{{ $product->Line }}
@if($product->has_finished_interior)
Note="Finished Interior on cabinet","FININT","bool",1
@endif
Note="Cabinet Finish Exterior","CM_CABFINEXT","text","{{ $cabinetTextNoColon }}"
Note="Cabinet Finish Interior","CM_CABFININT","text","{{ $job->cabinet_box_material_name }}"
Note="Cabinet Nomenclature","CM_CABDETAIL","text","{{ getORDCode($product) }}"
@if($job->door != null)
Note="Door Style","CM_DOORSTYLE","text","{{ $job->door->DoorStyleName }}"
@endif
Note="Cabinet Description","CM_CABDESC","text","{{ $htmlDescription }}"
@foreach($product->modifications as $modification)<?php $htmlNotes = htmlspecialchars_decode($modification->Notes);?>
Attribute="{{ str_replace(",", "-",htmlspecialchars_decode($modification->modificationItem->Description)) }}<?php if($htmlNotes): ?>: {{ str_replace(",", "-", $htmlNotes) }}<?php endif ?>","{{ $modification->modificationItem->CVCode }}","{{ $modification->modificationItem->cv_type }}",{{ $modification->cv_value }}
@endforeach

[Cabinets]
@if($product->Type != "accessory")
{{ $product->Line }},"{{ $product->CVCode }}",{{ $dimensions }},"{{ $product->hinge_code }}","{{ $product->end_type_code }}",{{ $product->Qty }},"{{ htmlspecialchars_decode($product->Notes) }}"
@else
{{ $product->Line }},"{{ $product->accessory_item->GroupCode }}",{{ $accDimensions }},"{{ $product->hinge_code }}","{{ $product->end_type_code }}",{{ $product->Qty }},"{{ htmlspecialchars_decode($product->Notes) }}"
@endif
