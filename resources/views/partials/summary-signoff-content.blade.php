
<base href="{{ $http_base_path }}">
<?php 
    $stickerCounter = 0;
    $deposit = $job->Net / 2; 
?>
<div class="content">
    
<table>
    <tr>
        <td>
            <br /><br />
            <p>X____________________________________________________________ Date_____________________</p><br />

            <p>I agree to purchase the above order for custom manufactured kitchen cabinets. I have read the order above and acknowledged that I am responsible for the order to be made per the above dimensions and specifications. This includes all header information, materials, finishes, inserts and accessories, custom requests, and details as well as general cabinet and door product specifications. </p><br />

            <p>Regardless of providing drawings or references for the above order, some which may differ from the above order, Cory Manufacturing Inc. is responsible for only the signed order above. Any variation from the order to drawings provided in the design or communication process is the responsibility of the customer. Returns or refunds will not be accepted. Changes to the orders after sign-off will incur a change order fee as defined in your dealer agreement. </p><br />

            <p>We require a 50% deposit of the total cost of your order, shipping included, upon confirmation of the final acknowledgement.
            <br /><strong>The deposit amount for the above order is <?php echo "$" . sprintf('%01.2f', $deposit); ?></strong></p><br />

            <p>Ship date will be provided at receipt of deposit. Any delay in receiving your deposit will impact the delivery time. 
            Processed and finalized / signed orders are not scheduled or submitted for production without deposit payment. Cory Manufacturing Inc. will provide any changes to the ship date due to factors outside of our control as soon as we are aware of them and are not responsible for costs incurred by the customer due to delays in estimated ship dates. The customer is responsible for giving a window of tolerance for ship dates to end users to allow for scheduling and logistics. </p><br />

            <p>Unless other terms have been established in writing, the balance of your total cost, including any portion(s) of the
            original order that were pulled and resubmitted with final dealer approval of modifications, will be required prior to or at completion. Cabinetry orders must be picked up within 14 working days from completion date and storage charges will be applied if not picked up within that time frame. Storage for cabinetry in climate controlled and secure locations prior to installation is the responsibility of the customer to insure stability of wood casework provided by Cory Manufacturing Inc.  </p><br />

            <p><strong>All payments to be in the form of a check made payable to: </strong><br />
                <br />Cory Manufacturing, Inc.
                <br />343 Manley St.
                <br />West Bridgewater, MA 02379
                <br />508.510.3699                
                <br />info@corymfg.com
            </p>
        </td>
    </tr>
</table>
</div>


</body>
</html>

