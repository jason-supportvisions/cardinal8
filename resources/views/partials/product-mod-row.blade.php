
<tr>
    <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
        
    </td>
    <td class="text-center" style="white-space:nowrap;border-right:1px solid #ddd;">
        {{ $modification->quantity }}
    </td>
    <td style="white-space:nowrap;border-right:1px solid #ddd; padding-left:40px;">
        @if($modification->modificationitem != null)
            {{ $modification->modificationitem->Code }}
        @endif 
        @if($modification->Value1)
        <small>
            @if($modification->modificationitem->Value1Type == "meas")
            ({{ $modification->cv_value }}")
            @endif
        </small>
        @endif
    </td>
    <td style="white-space:nowrap;border-right:1px solid #ddd;">
    </td>
    <td>
        {{ $modification->modificationitem->Description }}
        @if($modification->Notes) 
        <br>
        <small>{{ $modification->Notes }}</small>
        @endif
    </td>
    <td class="price-td LIST @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->ListQty }}</i></small>
    </td>
    <td class="price-td LIST @if($pricing!='LIST'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{$modification->ListQty }}</i></small>
    </td>
    <td class="price-td COST @if($pricing!='COST'  || $stageHide == TRUE) hide @endif" style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->cost_per }}</i></small>
    </td>
    <td class="price-td COST @if($pricing!='COST'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{ $modification->cost_qty }}</i></small>
    </td>
    <td class="price-td CUSTOMER @if($pricing!='CUSTOMER'  || $stageHide == TRUE) hide @endif"style="white-space:nowrap;border-left:1px solid #ddd;border-right:1px solid #ddd;">
        <small><i><b>$</b>{{ $modification->customer_per }}</i></small>
    </td>
    <td class="price-td CUSTOMER @if($pricing!='CUSTOMER'  || $stageHide == TRUE) hide @endif">
        <small><i><b>$</b>{{ $modification->customer_qty }}</i></small>
    </td>
    
    @if($editable)
    <td class="text-center">
        {{ Form::open(['method'=>'post', 'url' => route('delete-modification', ['jobID'=>$job->JobID, 'job_product_id'=> $product->ID, 'modification_id'=> $modification->ID]), 'class'=>'delete-form', 'data-confirm'=>'Do you really want remove this modification entry?']) }}
            <button type="submit" class="btn btn-danger btn-sm" style="margin-left:66px;" title="Remove" href="product_mod_delete.php?JobID=40263&amp;JobAcct=22&amp;Line=3&amp;ID=2675" data-primary="1" data-task="remove" data-confirm="Do you really want remove this entry?">
                <i class="glyphicon glyphicon-remove"></i> 
            </button>
        {{ Form::close() }}
        </span>
    </td>
    @endif
    
</tr>