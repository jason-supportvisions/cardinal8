<div class="outline grid-table">
    <table width="80%">
        <tr valign="bottom">
            <td>
                <b>Qty</b>
            </td>
            <td>
                <b>Code</b>
            </td>
            <td>
                <b>W x H x D</b>
            </td>
            <td>
                <b>Hinging</b>
            </td>
            <td>
                <b>Fin. End(s)</b>
            </td>
            <td>
                <b>Description</b>
            </td>
        </tr>
        <tr valign="top">
            <td>
                {{ $product->Qty }}
            </td>
            <td>
                <strong>{{ $product->Code }}</strong>
            </td>
            <td>
                <strong>
                    {{ HTML::dimension($product->Width) }}"
                    x 
                    {{ HTML::dimension($product->Height) }}"
                    x
                    {{ HTML::dimension($product->Depth) }}"
                </strong>
            </td>
            <td>
                {{ $product->Hinging }}
            </td>
            <td>
                {{ $product->FinishedEnd }}
            </td>
            <td>
                {{ $product->Description }}
                @if($product->Notes)
                <br>
                {{ $product->Notes }}
                @endif
            </td>
        </tr>
        @if($product->has_finished_interior)
        <tr valign="top">
            <td>1</td>
            <td>Finished Interior</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        @endif

        @foreach($modifications as $modification)
        <tr valign="top">
            <td>
                {{{ $modification->quantity }}}
            </td>
            <td>
                {{ $modification->modification_item->Code }}
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                &nbsp;
            </td>
            <td>
                {{ $modification->modification_item->Description }}
                @if($modification->Notes)
                <br>
                {{{ $modification->Notes }}}
                @endif
            </td>
        </tr>
        @endforeach
    </table>
</div>