<tr valign="top">
    <td>
        {{ $modification->quantity }}
    </td>
    <td>
        {{ $modification->code }}
    </td>
    <td>
        {{ $modification->Value1 }} 
    </td>
    <td>
        &nbsp;
    </td>
    <td>
        &nbsp;
    </td>
    <td>
        {{ $modification->description }}
        @if($modification->Notes)
        : {{$modification->Notes }}
        @endif
    </td>
</tr>