<?php
use Illuminate\Support\Facades\DB;

if (!function_exists('parseCodes')) {
	function parseCodes($text, $job_product)
	{
		$delimeter = "[[";
		$codeCheck = strpos($text, $delimeter);
		if ($codeCheck) {
			$width = strpos($text, "[[width]]");
			$height = strpos($text, "[[height]]");
			$depth = strpos($text, "[[height]]");
			if ($width != 0) {
				if ($job_product->Width) {
					$jobWidth = round($job_product->Width, 4) . '"';
				} else {
					$jobWidth = $job_product->product_item->Width;
				}
				$text = str_replace('[[width]]', $jobWidth, $text);
			}
			if ($height != 0) {
				$jobHeight = round($job_product->Height, 4) . '"';
				$text = str_replace('[[height]]', $jobHeight, $text);
			}
			if ($depth != 0) {
				$jobDepth = round($job_product->Depth, 4) . '"';
				$text = str_replace('[[depth]]', $jobDepth, $text);
			}
		}
		return $text;
	}
}

if (!function_exists('getProductType')) {
	function getProductType($product)
	{
		$itemStr = 0;
		if (strtolower($product->product_type) == "accessory" || strtolower($product->Type) == "accessory") {
			$itemStr    	   = "accessory";
		} elseif (strtolower($product->product_type) == "product" || strtolower($product->Type) == "product") {
			$itemStr    	   = "product";
		} else {
			$itemStr    	   = "product";
		}
		$Table              = $itemStr;
		$prodType           = $itemStr;
		$itemType           = $itemStr . "_item";
		$groupType			= $itemStr . "_group";
		$pricingType		= $itemStr . "_pricing";
		$noteType           = $groupType . "_notes";
		$itemTable 			= $itemType . "s";
		$groupTable			= $groupType . "s";
		$itemArray = [
			"productType" 		=> $prodType,
			"itemType" 			=> $itemType,
			"groupType" 		=> $groupType,
			"pricingType" 		=> $pricingType,
			"noteType" 			=> $noteType,
			"itemTable" 		=> $itemTable,
			"groupTable" 		=> $groupTable,
		];
		return $itemArray;
	}
}


if (!function_exists('getORDCode')) {
	function getORDCode($product)
	{
		
		$debug 			= "";
		$customPrefix 	= "";
		$customSuffix 	= "";
		$ordType 		= 0;
		$concatCode 	= "";
		$ordDimensions 	= 0;
		$notOriginalCheck = false;

		// return $product; 
		if(isset($product->product_type)){
			$productType = $product->product_type;
		}else{
			$productType	= $product->Type;
		}

		if ($productType == "accessory") {
			$itemStr    	= "accessory";
			$origWidth			= $product->accessory_item->Width;
			$origHeight			= $product->accessory_item->Height;
			$origDepth 			= $product->accessory_item->Depth;
		} else {
			$itemStr    	= "product";
			$origWidth			= $product->product_item->Width;
			$origHeight			= $product->product_item->Height;
			$origDepth 			= $product->product_item->Depth;

		}

		$itemType           = $itemStr . "_item";
		$groupType			= $itemStr . "_group";
		$Code 				= $product->Code;
		$ItemCode 			= $Code;

		//get CODE from item if new
		if(!isset($Code)){
			$Code = $product->$itemType->Code;
			$ItemCode = $Code;
		}

		//get ORD Settings from Manager / Group
		if (isset($product->$itemType->GroupCode)) {
			$concatCode = $product->$itemType->GroupCode;
		}

		//show this PREFIX string only on ORD CRM processes
		if (isset($product->$itemType->$groupType->ORDPrefix)) {
			$customPrefix = $product->$itemType->$groupType->ORDPrefix;
		}

		//show this PREFIX string only on ORD CRM processes
		if (isset($product->$itemType->$groupType->ORDSuffix)) {
			$customSuffix = $product->$itemType->$groupType->ORDSuffix;
		}

		//show ITEM code only OR GROUP CODE as usual
		if (isset($product->$itemType->$groupType->ORDCodeType)) {
			$ordType = $product->$itemType->$groupType->ORDCodeType;
		}

		//show the dimensions if changed
		if (isset($product->$itemType->$groupType->ORDDimensions)) {
			$ordDimensions  = $product->$itemType->$groupType->ORDDimensions;
		}

		//ORDCodeType
			//unchecked display the ITEM code without any dimensions
			//checked show the normal group code with all the dimensions filled in where __ exists
			if ($ordType === 0) {
				$groupCode 		 	= $Code; //items code
				$concatGroupCode 	= $groupCode;
				$needDash 		 	= '-';
			} else {
				$groupCode 		 	= $concatCode; //groupCode
				$concatGroupCode 	= $groupCode;
				$needDash 		 	= '-';
			}
		
		//Check Dimensions
			//dimensions from the job_product / construction
			//give originally if nothing saved
			if (isset($product->Width)) {
				$Width 			= $product->Width;
				$Height 		= $product->Height;
				$Depth 			= $product->Depth;
			}else {
				$Width			= $origWidth;
				$Height			= $origHeight;
				$Depth 			= $origDepth;
			} 
		
			//are any of the dimensions different than original?
			if($Width != $origWidth){ $notOriginalCheck = true; }
			if($Height != $origHeight){ $notOriginalCheck = true; }
			if($Depth != $origDepth){ $notOriginalCheck = true; }

			//force showing dimensions?
			if ($ordDimensions === 1) {
				$showDimensions 	= TRUE;
			} else {
				$showDimensions 	= FALSE;
			}

			//dimensions have changed?
			if($notOriginalCheck === true){
				$showDimensions = TRUE;
			}

			//create string used in CODE
			$ORDCodeDimensions = round($Width, 4) . "x" . round($Height, 4)  . "x" . round($Depth, 4) . $needDash;
		
		//Massage CODES to properly escape necessary characters
		$codeDashLocation = strcspn($concatCode, "-", 0);
		$codeUnderscoreLocation = strcspn($groupCode, "__", 0);
		$leftParenLocation = strcspn($concatGroupCode, "(", 0);
		$rightParenLocation = strcspn($concatGroupCode, ")", 0);
		$underscoreLocation = strcspn($concatGroupCode, "__", 0);
		$plusLocation = strcspn($concatGroupCode, "+", 0);
		$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);
		$parenthesisData = substr($concatGroupCode, $leftParenLocation);
		$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);
		$initDimension = substr($concatGroupCode, ($underscoreLocation + 2), 2);
		$initDimensionNumber = is_numeric($initDimension);
		$initDash = strpos($initDimension, "-");
		$initDashNumber = is_numeric($initDash);
		
		//no dash 
		if ($initDimensionNumber == true && $initDashNumber == false) {
			$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);
		} 		

		//if dims are 0
		if (stristr($ORDCodeDimensions, "0x0x0")) {
			$ORDCodeDimensions = "";
		}


		if ($showDimensions === TRUE) {
			$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode);
		} else {
			$ORDCode = str_replace("__", "", $concatGroupCode);
		}

		
		$ORDCode = str_replace("--", "-", $ORDCode);
		$ORDCode = rtrim($ORDCode, "-");
		if ($customPrefix != "") {
			$ORDCode = $customPrefix . $ORDCode;
		}
		if ($customSuffix != "") {
			$ORDCode = $ORDCode . $customSuffix;
		}

		//if no dimensions changed, simply return the itemcode
		if($Width == $origWidth && $Height == $origHeight && $Depth == $origDepth){
			$ORDCode = $ItemCode;
		}

		// return $ORDCode . "" . $Width.$origWidth."".$Height.$origHeight."".$Depth.$origDepth;
		// return $ORDCode . " " . $ItemCode;
		return $ORDCode;	
	}
}

// if (!function_exists('getORDCode')) {
// 	function getORDCode($product)
// 	{
// 		$debug 			= "";
// 		$customPrefix 	= "";
// 		$customSuffix 	= "";
// 		$ordType 		= "";
// 		$concatCode 	= "";
// 		$ordDimensions 	= "";
// 		$productType	= $product->Type;
// 		if ($productType == "accessory") {
// 			$itemStr    	= "accessory";
// 		} else {
// 			$itemStr    	= "product";
// 		}
		
// 		$itemType           = $itemStr . "_item";
// 		$groupType			= $itemStr . "_group";
// 		$itemTable			= $itemType . "s";
// 		$groupTable			= $groupType . "s";
// 		$Code 				= $product->Code;
// 		if (isset($product->$itemType->GroupCode)) {
// 			$concatCode = $product->$itemType->GroupCode;
// 		}
// 		if (isset($product->$itemType->$groupType->ORDPrefix)) {
// 			$customPrefix = $product->$itemType->$groupType->ORDPrefix;
// 		}
// 		if (isset($product->$itemType->$groupType->ORDCodeType)) {
// 			$ordType = $product->$itemType->$groupType->ORDCodeType;
// 		}
// 		if (isset($product->$itemType->$groupType->ORDDimensions)) {
// 			$ordDimensions  = $product->$itemType->$groupType->ORDDimensions;
// 		}
// 		if (isset($product->$itemType->$groupType->ORDSuffix)) {
// 			$customSuffix = $product->$itemType->$groupType->ORDSuffix;
// 		}
// 		$Code 			= $product->Code;
// 		if ($ordType == 0) {
// 			$concatCode 		= $Code;
// 			$groupCode 		 	= $concatCode;
// 			$concatGroupCode 	= $groupCode;
// 			$needDash 		 	= '-';
// 		} else {
// 			$groupCode 		 	= $concatCode;
// 			$concatGroupCode 	= $groupCode;
// 			$needDash 		 	= '-';
// 		}
// 		if (isset($product->Width)) {
// 			$Width 			= $product->Width;
// 			$Height 		= $product->Height;
// 			$Depth 			= $product->Depth;
// 		} else {
			
// 			$Width			= $product->$itemType->Width;
// 			$Height			= $product->$itemType->Height;
// 			$Depth 			= $product->$itemType->Depth;
// 		}
// 		if ($ordDimensions == 1) {
// 			$showDimensions 	= TRUE;
// 		} else {
// 			$showDimensions 	= FALSE;
// 		}
// 		$codeDashLocation = strcspn($concatCode, "-", 0);
// 		$codeUnderscoreLocation = strcspn($groupCode, "__", 0);
// 		$leftParenLocation = strcspn($concatGroupCode, "(", 0);
// 		$rightParenLocation = strcspn($concatGroupCode, ")", 0);
// 		$underscoreLocation = strcspn($concatGroupCode, "__", 0);
// 		$plusLocation = strcspn($concatGroupCode, "+", 0);
// 		$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);
// 		$parenthesisData = substr($concatGroupCode, $leftParenLocation);
// 		$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);
// 		$initDimension = substr($concatGroupCode, ($underscoreLocation + 2), 2);
// 		$initDimensionNumber = is_numeric($initDimension);
// 		$initDash = strpos($initDimension, "-");
// 		$initDashNumber = is_numeric($initDash);
// 		if ($initDimensionNumber == true && $initDashNumber == false) {
// 			$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);
// 		} else {
// 		}
// 		$ORDCodeDimensions = round($Width, 4) . "x" . round($Height, 4)  . "x" . round($Depth, 4) . $needDash;
// 		if (stristr($ORDCodeDimensions, "0x0x0")) {
// 			$ORDCodeDimensions = "";
// 		}
// 		if ($showDimensions 	== TRUE) {
// 			$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode);
// 		} else {
// 			$ORDCode = str_replace("__", "", $concatGroupCode);
// 		}
// 		$ORDCode = str_replace("--", "-", $ORDCode);
// 		$ORDCode = rtrim($ORDCode, "-");
// 		if ($customPrefix != "") {
// 			$ORDCode = $customPrefix . $ORDCode;
// 		}
// 		if ($customSuffix != "") {
// 			$ORDCode = $ORDCode . $customSuffix;
// 		}
// 		return $ORDCode;
// 	}
// }

if (!function_exists('getORDCodeCV')) {

	function getORDCodeCV($product)
	{

		$productType = $product->Type;
		$debug = "";
		if ($productType == "accessory") {
			$itemStr    	   = "accessory";
		} else {
			$itemStr    	   = "product";
		}
		$Table              = $itemStr;
		$prodType           = $itemStr;
		$itemType           = $itemStr . "_item";
		$groupType			= $itemStr . "_group";
		$pricingType		= $itemStr . "_pricing";
		$noteType           = $groupType . "_notes";
		$groupTable			= $groupType . "s";
		$itemTable 			= $itemType . "s";
		if (!isset($_GET['productType'])) {
			$pageType 		= "show";

			$concatCode 	= $product->GroupCode;
			$groupCode 		= $concatCode;

			$Width 			= $product->Width;
			$Height 		= $product->Height;
			$Depth 			= $product->Depth;
		} else {
			$pageType		= "create";
			$productType 	= $_GET['productType'];

			if ($productType == "accessory") {
				$Width			= $product->accessory_item->Width;
				$Height			= $product->accessory_item->Height;
				$Depth 			= $product->accessory_item->Depth;
			} elseif ($productType == "product") {
				$Width			= $product->product_item->Width;
				$Height			= $product->product_item->Height;
				$Depth 			= $product->product_item->Depth;
			} else {
				$Width			= $product->product_item->Width;
				$Height			= $product->product_item->Height;
				$Depth 			= $product->product_item->Depth;
			}
			$concatCode 	= $product->GroupCode;
			$groupCode 		= $concatCode;
		}
		$needDash 		 = '-';
		$concatGroupCode = $groupCode;
		$codeDashLocation = strcspn($concatCode, "-", 0);
		$codeUnderscoreLocation = strcspn($groupCode, "__", 0);
		$debug .= "<table border='1' style='padding:6px'>";
		$debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
		$leftParenLocation = strcspn($concatGroupCode, "(", 0);
		$rightParenLocation = strcspn($concatGroupCode, ")", 0);
		$underscoreLocation = strcspn($concatGroupCode, "__", 0);
		$debug .= "<tr><td style='padding:6px'><b>'(' location </b></td><td style='padding:6px'>" . $leftParenLocation . "th</td></tr>";
		$debug .= "<tr><td style='padding:6px'><b>')' location </b></td><td style='padding:6px'>" . $rightParenLocation . "th</td></tr>";
		$debug .= "<tr><td style='padding:6px'><b>'_' location </b></td><td style='padding:6px'>" . $underscoreLocation . "th</td></tr>";
		$debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
		$plusLocation = strcspn($concatGroupCode, "+", 0);
		$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);
		$debug .= "<tr><td style='padding:6px'><b>'+' location </b></td><td style='padding:6px'>" . $plusLocation . "th</td></tr>";
		$parenthesisData = substr($concatGroupCode, $leftParenLocation);
		$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);
		$initDimension = substr($concatGroupCode, ($underscoreLocation + 2), 2);
		$debug .= "<tr><td style='padding:6px'><b>remove '$parenthesisData' </b></td><td style='padding:6px'>" . $groupCode . "</td></tr>";
		$debug .= "<tr><td style='padding:6px'><b>removed result </b></td><td style='padding:6px'>" . $concatGroupCode . "</td></tr>";
		$debug .= "<tr><td style='padding:6px'><b>init dimension </b></td><td style='padding:6px'> '" . $initDimension . "' </td></tr>";
		$initDimensionNumber = is_numeric($initDimension);
		$initDash = strpos($initDimension, "-");
		$initDashNumber = is_numeric($initDash);
		if ($initDimensionNumber == true && $initDashNumber == false) {
			$debug .= "<tr><td style='padding:6px' colspan='2'>'$initDimension' <b> is</b> a number, so we remove it.</td></tr>";
			$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);
		} else {
			$debug .= "<tr><td style='padding:6px' colspan='2'>initial dimension '$initDimension' is <b>NOT</b> a number, so we keep it.</td></tr>"; //is the above dimension really numbers? 
		}
		$debug .= "<tr><td style='padding:6px'><b>CODE </b></td><td style='padding:6px'>$concatGroupCode</td></tr>";
		$debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
		$ORDCodeDimensions = round($Width, 4) . "x" . round($Height, 4)  . "x" . round($Depth, 4) . $needDash;
		$debug .= "<tr><td style='padding:6px'><b>Dimensions</b></td><td style='padding:6px'>$ORDCodeDimensions</td></tr>";
		if (stristr($ORDCodeDimensions, "0x0x0")) {
			$ORDCodeDimensions = "";
			$debug .= "<tr><td style='padding:6px' colspan='2'>found '0x0x0' and zero it out</td></tr>"; //is the above dimension really numbers? 
		}
		$debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
		$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode);
		$ORDCode = str_replace("--", "-", $ORDCode);
		$debug .= "<tr><td style='padding:6px'><b>replace '--'</b></td><td style='padding:6px'>$ORDCode</td></tr>";
		$ORDCode = rtrim($ORDCode, "-");
		$debug .= "<tr><td style='padding:6px'><b>rtrim(code,'-')</b></td><td style='padding:6px'>$ORDCode</td></tr>";
		$debug .= "</table>";
		$debug .= "<br><h2>Final Output: <b><font color=blue>" . $ORDCode . "</font></b></h2><br>";
		return $ORDCode;
	}
}

if (!function_exists('getImageFileName')) {

	function getImageFileName($groupCode, $extension)
	{
		$external_path = "/images/cabinets/" . $groupCode . "." . $extension;
		return $external_path;
	}
}

if (!function_exists('getItemImageFileName')) {

	function getItemImageFileName($fileName)
	{
		$external_path = "/" . $fileName;
		return $external_path;
	}
}

if (!function_exists('getORDCodeNew')) {
	function getORDCodeNew($code, $groupCode, $width, $height, $depth, $productType)
	{
		$concatCode 	= $groupCode;
		$groupCode 		= $concatCode;
		$Width 			= $width;
		$Height 		= $height;
		$Depth 			= $depth;
		$productType	= $productType;
		$needDash 		 = '-';
		$concatGroupCode = $groupCode;
		$codeDashLocation = strcspn($concatCode, "-", 0);
		$codeUnderscoreLocation = strcspn($groupCode, "__", 0);
		$leftParenLocation = strcspn($concatGroupCode, "(", 0);
		$rightParenLocation = strcspn($concatGroupCode, ")", 0);
		$underscoreLocation = strcspn($concatGroupCode, "__", 0);
		$plusLocation = strcspn($concatGroupCode, "+", 0);
		$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);
		$parenthesisData = substr($concatGroupCode, $leftParenLocation);
		$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);
		$initDimension = substr($concatGroupCode, ($underscoreLocation + 2), 2);
		$initDimensionNumber = is_numeric($initDimension);
		$initDash = strpos($initDimension, "-");
		$initDashNumber = is_numeric($initDash);
		if ($initDimensionNumber == true && $initDashNumber == false) {
			$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);
		} else {
		}
		$ORDCodeDimensions = round($Width, 4) . "x" . round($Height, 4)  . "x" . round($Depth, 4) . $needDash;
		if (stristr($ORDCodeDimensions, "0x0x0")) {
			$ORDCodeDimensions = "";
		}
		$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode);
		$ORDCode = str_replace("--", "-", $ORDCode);
		$ORDCode = rtrim($ORDCode, "-");
		return $ORDCode;
	}
}
if (!function_exists('getORDCodeClean')) {
	function getORDCodeClean($code)
	{

		if (strpos($code, "(")) {
			$code = substr($code, 0, strpos($code, "("));
			// echo "<br>clean code: " . $code;
		} else {
			$code = $code;
		}

		return $code;
	}
}

if (!function_exists('selected')) {
	//ie. selected("job_types", $jobType, "WHERE ID = 'MA'") $default == $row['ID']
	function selected($table, $selected, $where, $default)
	{
		if ($selected == '' or empty($selected) or is_null($selected)) {
			$selected = $default;
		}
		if (empty($where)) {
			$Results = DB::select(DB::raw("SELECT DISTINCT * FROM $table WHERE Active = 1 ORDER BY Name ASC"));
		} else {
			$Results = DB::select(DB::raw("SELECT DISTINCT * FROM $table $where AND Active = 1 ORDER BY Name ASC"));
		}
		foreach ($Results as $row) {

			if (isset($row->Label)) {
				if (!$row->Label) {
					$Label = "";
				} else {
					$Label = $row->Label;
				}
				if ($selected == $row->ID) {

					echo "<option label=\"" . $Label . "\" value=\"" . $row->ID . "\" selected=\"selected\">" . $row->Name . "</option>";
				} else {
					echo "<option label=\"" . $Label . "\" value=\"" . $row->ID . "\">" . $row->Name . "</option>";
				}
			} else {
				if ($selected == $row->ID) {
					echo "<option value=\"" . $row->ID . "\" selected=\"selected\">" . $row->Name . "</option>";
				} else {
					echo "<option value=\"" . $row->ID . "\">" . $row->Name . "</option>";
				}
			}
		}
	}
}

if (!function_exists('selectedarray')) {
	function selectedarray($table1, $column, $where1, $table2, $selected2, $where2, $NullValue, $appendText = "")
	{
		echo "<script>console.log('selectedarray(SELECT DISTINCT $column FROM $table1 $where1 LIMIT 1)');</script>";
		$Results = DB::select(DB::raw("SELECT DISTINCT $column FROM $table1 $where1 LIMIT 1"));
		foreach ($Results as $value) {
			if (is_null($value->$column) or empty($value->$column)) {
				echo "<option value=\"0\" selected=\"selected\">$NullValue $appendText</option>";
			} else {
				if (empty($where2)) {
					$Results2 = DB::select(DB::raw("SELECT DISTINCT ID, Name FROM $table2 WHERE ID = $value AND Active = 1 ORDER BY Name ASC"));
					echo "<script>console.log('selectedarray(SELECT DISTINCT ID, Name FROM $table2 WHERE ID = $value AND Active = 1 ORDER BY Name ASC)');</script>";
				} else {
					echo "<script>console.log('selectedarray(SELECT DISTINCT ID, Name FROM $table2 $where2 AND ID = $value AND Active = 1 ORDER BY Name ASC)');</script>";
					$Results2 = DB::select(DB::raw("SELECT DISTINCT ID, Name FROM $table2 $where2 AND ID = $value AND Active = 1 ORDER BY Name ASC"));
				}
				
				foreach ($Results2 as $row) {
					if ($selected2 == $row->ID) {
						echo "<script>console.log('" . $row->Name . " -- SELECTED');</script>";
						echo "<option  value=\"" . $row->ID . "\" selected=\"selected\">" . $row->Name . "</option>";
					} else {
						echo "<script>console.log('" . $row->Name . "');</script>";
						echo "<option value=\"" . $row->ID . "\">" . $row->Name . "</option>";
					}
				}
			}
		}
		
	}
}

//Create select box by comma dilimeted field values
if(!function_exists('selectexplode')){
	function selectexplode($delimiter, $column, $name, $selected, $default){
		$array = explode($delimiter,$column);

		echo "<select id=\"".$name."\" class=\"form-control\" name=\"".$name."\" style=\"width:100%;\">";
		$state = 0;
		foreach($array as $value) {
			if($default == $value && $selected!=""){
				echo "a<option value=\"".$value."\" selected=\"selected\">".$value."</option>";
			}	
			elseif($selected == $value){
			
				echo "b<option value=\"".$value."\" selected=\"selected\">".$value."</option>";
			} else {
				echo "c<option value=\"".$value."\">".$value."</option>";
			}
		
		}

		echo "</select>";
		
	}
}

//multiple select jquery select box
if(!function_exists('selectexplode')){
	function selectexplode($delimiter, $column, $name, $selected, $default){
		$array = explode($delimiter,$column);

		echo "<select id=\"".$name."\" class=\"form-control\" name=\"".$name."\" style=\"width:100%;\" multiple=\"multiple\">";
		$state = 0;
		foreach($array as $value) {
			if($default == $value && $selected!=""){
				echo "a<option value=\"".$value."\" selected=\"selected\">".$value."</option>";
			}	
			elseif($selected == $value){
			
				echo "b<option value=\"".$value."\" selected=\"selected\">".$value."</option>";
			} else {
				echo "c<option value=\"".$value."\">".$value."</option>";
			}
		
		}

		echo "</select>";
		
	}
}

if(!function_exists('radioarray')){
	function radioarray($table1, $column, $where1, $table2, $selected2, $where2, $RadioName, $Label, $directory, $IDLabel){
		
		$Results = DB::select(DB::raw("SELECT DISTINCT $column FROM $table1 $where1 LIMIT 1"));
		
		foreach ($Results as $row) {
			$array = $row->$column;
		}
		
		if($array === NULL OR empty($array)){
		
		} else {
		
			echo "<div>";
			echo "<label class=\"h4\">".$Label."</label><br>";
			foreach(explode(',',$array) as $value){
				if(is_null($value) or empty($value)){
				} else {
				if(empty($where2)){
					$Results2 = DB::select(DB::raw("SELECT DISTINCT * FROM $table2 WHERE ID = $value  ORDER BY Name ASC"));
				} else {
					$Results2 = DB::select(DB::raw("SELECT DISTINCT * FROM $table2 $where2 AND ID = $value  ORDER BY Name ASC"));
				}
				
				foreach ($Results2 as $row){
					
					echo "<div class=\"radio\" style=\"margin: 4px;\">";
					$RadioID = $IDLabel.$row->Code;
					if ($row->ID == $selected2){
						echo "<input type=\"radio\" class=\"imagebtn $RadioName\" name=\"$RadioName\" code=\"".$row->Code."\" code=\"".$row->Code."\" id=\"".$RadioID."\" value=\"".$row->ID."\" checked=\"checked\"/>";
					} else {
						echo "<input type=\"radio\" class=\"imagebtn $RadioName\" name=\"$RadioName\" code=\"".$row->Code."\" id=\"".$RadioID."\" value=\"".$row->ID."\"/>";
					}
					echo "<label for=\"".$RadioID."\" ><img src=\"..//images/".$directory."/".$row->Code.".png\"><br>";
					echo "&nbsp;".$row->Name;
					echo "</label>";
					echo "</div>";
			
				}
				}
			}
			echo "</div>";
			echo "<hr style=\"margin:10px\">";
		}
	}
}

