<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="user-scalable=yes, initial-scale=1, minimum-scale=0.2, maximum-scale=2, width=device-width" name="viewport" id="viewport"> 
        <meta name="description" content="">
        <meta name="author" content="PSB">
        <title>CARDINAL || {{ $page_name ?? '' or 'Cabinet Manufacturing Excellence' }}</title>
        <link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png?v=3">
        <link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png?v=3">
        <link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png?v=3">
        <link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png?v=3">
        <link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png?v=3">
        <link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png?v=3">
        <link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png?v=3">
        <link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png?v=3">
        <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png?v=3">
        <link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png?v=3">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png?v=3">
        <link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png?v=3">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png?v=3">
        <link rel="manifest" href="/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png?v=3">
        <meta name="theme-color" content="#ffffff">

        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap-theme.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/select2.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery-ui.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.dataTables.min.css') }}">
        <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css?n=1') }}">
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/datatables.min.css"/>

        @yield('styles')
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" />
        <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script>
        <script>
            window.addEventListener("load", function() {
                window.cookieconsent.initialise({
                    "palette": {
                        "popup": {
                            "background": "#eaf7f7",
                            "text": "#5c7291"
                        },
                        "button": {
                            "background": "#56cbdb",
                            "text": "#ffffff"
                        }
                    }
                })
            });
        </script>
    </head>
    <body>
        <div class="container">
            @section('nav')
            @include('partials.primary_navbar')
            @show

            @if(Session::get('warning'))
            <div class="alert alert-warning">
                {{ Session::get('warning') }}
            </div>
            @endif

            @if(Session::get('success'))
            <div class="alert alert-success">
                {{ Session::get('success') }}
            </div>
            @endif

            @if(Session::get('info'))
            <div class="alert alert-info">
                {{ Session::get('info') }}
            </div>
            @endif

            @if(Session::get('danger'))
            <div class="alert alert-danger">
                {{ Session::get('danger') }}
            </div>
            @endif

            @if($errors->any())
            <div class="alert alert-danger">
                Errors on form.
            </div>
            @endif

            @yield('content')

        </div>
        <div class="container">
            <footer class="footer">
                <div class="visible-xs">
                    <p style="font-size: 12px;">&copy; Cory Manufacturing, Inc. <?php echo date("Y"); ?> all rights reserved </p>                
                </div>
                <div class="hidden-xs">
                    <p class="pull-left" style="font-size: 12px;">&copy; Cory Manufacturing, Inc. <?php echo date("Y"); ?> all rights reserved </p>                
                </div>
            </footer>
        </div>

        <script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/datetime-moment.js') }}"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/pdfmake.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.36/vfs_fonts.js"></script>
        <script type="text/javascript" src="https://cdn.datatables.net/v/dt/jszip-2.5.0/dt-1.10.22/b-1.6.5/b-colvis-1.6.5/b-flash-1.6.5/b-html5-1.6.5/b-print-1.6.5/datatables.min.js"></script>

        <script type="text/javascript">
            //$.fn.dataTable.moment('MM/DD/YYYY h:mm a');
            $('.select2').select2();
            $("form.delete-form").submit(function(e) {
                var ans = confirm($(this).attr('data-confirm'));
                if (!ans) {
                    e.preventDefault();
                }
            });
        </script>
        @yield('scripts')   
    </body>
</html>