<!DOCTYPE html>
<html lang="en">
@include('layouts.header')

<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        @yield('content')
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')
    @yield('page-script')
</body>

</html>