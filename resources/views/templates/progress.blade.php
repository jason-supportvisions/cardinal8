<?php
	$JobReference = "";
	if(!isset($JobID)) { $JobID = 0;}
?>
<?php if ($pageid == 'Header') { ?>

	<?php if ($JobID == 0) {	?>
		<div class="row">
			<div class="col-md-12" style="padding-bottom: 15px">
				<div class="btn-group" role="group" style="width: 100%">
					<input class="btn btn-xs btn-primary" type="button" name="action" value="Job" style="width: 20%; margin-left: 0px; border-right: 0px;">
					<input class="btn btn-xs btn-UX-todo disabled" type="button" name="action" value="Specification" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
					<input class="btn btn-xs btn-UX-todo disabled" type="button" name="action" value="Uploads" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
					<input class="btn btn-xs btn-UX-todo disabled" type="button" name="action" value="Products" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
					<input class="btn btn-xs btn-UX-todo disabled" type="button" name="action" value="Summary" style="width: 20%; margin-left: 0px;" disabled>
				</div>
			</div>
		</div>
		<?php
		$BackButton = "header";
		$ForwardButton = "Specification";
		?>

	<?php } else { ?>
		<div class="row">
			<div class="col-md-12" style="padding-bottom: 15px">
				<div class="btn-group" role="group" style="width: 100%">
					<input class="btn btn-xs btn-primary" type="button" name="action" value="Job" style="width: 20%; margin-left: 0px; border-right: 0px;" />
					<input class="btn btn-xs btn-UX-todo" type="button" name="action" value="Specification" style="width: 20%; margin-left: 0px; border-right: 0px;" />
					<input class="btn btn-xs btn-UX-todo" type="button" name="action" value="Uploads" style="width: 20%; margin-left: 0px; border-right: 0px;" />
					<input class="btn btn-xs btn-UX-todo" type="button" name="action" value="Products" style="width: 20%; margin-left: 0px; border-right: 0px;" />
					<input class="btn btn-xs btn-UX-todo" type="button" name="action" value="Summary" style="width: 20%; margin-left: 0px;" />
				</div>
			</div>
		</div>
		<?php
		$BackButton = "header";
		$ForwardButton = "Specification";
		?>
	<?php } ?>

<?php } elseif ($pageid == 'Construction') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%;">
				<button class="btn btn-xs btn-UX-done" type="button" name="action" value="Job" style="width: 20%; margin-left: 0px; border-right: 0px;" ><a href="{{ route('post.edit',['jobID'=>$jobID, 'jobAcct'=>$jobAcct, 'jobmod'=>'edit']) }}"> Job &nbsp;&nbsp;<span  ><i class="check-green glyphicon glyphicon-ok"></i></span></a></button>
				<button class="btn btn-xs btn-primary" type="button" name="action" value="Specification" style="width: 20%; margin-left: 0px; border-right: 0px;">Specifications</button>
				<button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Uploads" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
					Uploads
				</button> 
				<button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Products" style="width: 20%; margin-left: 0px;" disabled>
					Products
				</button>
				<button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Summary" style="width: 20%; margin-left: 0px;" disabled>
					Summary
				</button>
			</div>
		</div>
	</div>
	<?php
	$BackButton = "header";
	$ForwardButton = "Uploads";
	?>
<?php } elseif ($pageid == 'GeneralNotes') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{ route('post.edit',['jobID'=>$jobID, 'jobAcct'=>$jobAcct, 'jobmod'=>'edit']) }}">Job &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" href="{{route('cons-first')}}">
					Specifications &nbsp;&nbsp;
					<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span>
				</a>
				<a class="btn btn-xs btn-primary" style="width: 20%; margin-left: 0px; border-right: 0px;">
					Uploads
				</a>
				<a  class="btn btn-xs btn-UX-todo text-black"  name="action" value="Products" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
					Products
				</a>
				<a class="btn btn-xs btn-UX-todo text-black" style="width: 20%; margin-left: 0px;"  disabled>
					Summary
				</a>
			</div>
		</div>
	</div>
	<?php
	$BackButton = "construction";
	$ForwardButton = "products";
	?>
<?php } elseif ($pageid == 'Products') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Job &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Specifications &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;">Uploads &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-primary" style="width: 20%; margin-left: 0px; border-right: 0px;">Products</a>
				<a class="btn btn-xs btn-UX-todo text-black" style="width: 20%; margin-left: 0px;" >Summary</a>
			</div>
		</div>
	</div>
	<?php 
	$BackButton = "generalnotes";
	$ForwardButton = "summary";
	?>
<?php } elseif ($pageid == 'Add/Edit Products') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">
				<a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;">Job &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Specifications<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done disabled text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Uploads &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-primary disabled" style="width: 20%; margin-left: 0px; border-right: 0px;">Products</a>
				<a class="btn btn-xs btn-UX-todo disabled text-black disabled" style="width: 20%; margin-left: 0px;" >Summary</a>
			</div>
		</div>
	</div>
	<?php
	$BackButton = "products";
	$ForwardButton = "Validate";
	?>

<?php } elseif ($pageid == 'Summary') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Job &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;"  >Specifications &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;" >Uploads &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black" style="width: 20%; margin-left: 0px; border-right: 0px;"  >Products&nbsp;&nbsp;<i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-primary" style="width: 20%; margin-left: 0px;">Summary</a>
			</div>
		</div>
	</div>
	<?php
	$BackButton = "products";
	$ForwardButton = "/quotes_orders/";
	?>
<?php } elseif ($pageid == 'Locked') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">
				<a class="btn btn-xs btn-UX-done text-black disabled" style="width: 20%; margin-left: 0px; border-right: 0px;" >Job &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black disabled" style="width: 20%; margin-left: 0px; border-right: 0px;" >Specifications &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black disabled" style="width: 20%; margin-left: 0px; border-right: 0px;" >Uploads &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-UX-done text-black disabled" style="width: 20%; margin-left: 0px; border-right: 0px;" >Products &nbsp;&nbsp;<span class=""><i class="check-green glyphicon glyphicon-ok"></i></span></a>
				<a class="btn btn-xs btn-primary" style="width: 20%; margin-left: 0px;">Summary</a>
			</div>
		</div>
	</div>
	<?php
	$BackButton = "products";
	$ForwardButton = "/quotes_orders/";
	?>
<?php } elseif ($pageid == 'Add Products') { ?>
	<div class="row">
		<div class="col-md-12" style="padding-bottom: 15px">
			<div class="btn-group" style="width: 100%">

			</div>
		</div>
	</div>
	<?php
	$BackButton = "products";
	$ForwardButton = "/quotes_orders/";
	?>
<?php } else { ?>
	<section class="content-header">
		<div class="progress">
			<div class="progress-bar progress-bar-danger" style="width: 100%">Error please contact administrator</div>
		</div>
	</section>
<?php } ?>


<div class="row hidden-xs hidden-sm">
	<div class="col-xs-12 col-sm-12">
		<div class="panel panel-UX">
			<div class="panel-heading" style="border: none;height: 42px;">
				<div class="pull-right" style="margin-right: -10px;margin-top: -5px;margin-bottom: -5px">
					<?php if ($pageid == 'Header') { ?>
					<?php } elseif ($pageid == 'Add/Edit Products') { ?>
						<a href="<?php echo $BackButton; ?>.php?JobID=<?PHP echo $jobID ?>&JobAcct=<?PHP echo $jobAcct ?>"><button type="button" class="btn btn-danger">Cancel</button></a>
					<?php } elseif ($pageid == 'Add Products') { ?>

					<?php } elseif ($pageid == 'Summary') { ?>
						<a href="/quote_orders/generalnotes" class="btn btn-primary">Back</a>
					<?php } elseif ($pageid == 'GeneralNotes'){ ?>
						<a href="{{route('cons-first')}}" class="btn btn-primary">Back</a>
					<?php }else{?>
						<a href="{{route('post.edit', ['jobID'=>$jobID, 'jobAcct'=>$jobAcct, 'jobmod'=>'edit'])}}" class="btn btn-primary">Back</a>
					<?php } ?>

					<?php if ($pageid == 'Header' || $pageid == 'Construction') { ?>
						<button class="btn btn-primary" type="submit" name="action" id="nextsave" value="<?php echo $ForwardButton; ?>">Next / Save</button>
					<?php } elseif ($pageid == 'Add/Edit Products') { ?>
						<?php if (isset($stopsave)) { ?>
							<button class="btn btn-primary disabled" disabled="disabled">Save</button>
						<?php } else { ?>
							<button type="submit" value="Validate" class="btn btn-primary">Save</button>
						<?php } ?>
					<?php } elseif ($pageid == 'Products' || $pageid == 'GeneralNotes') { ?>

						<a href="{{ route('quote_orders.product',['jobID'=>$jobID]) }}"><button class="btn btn-primary" id="nextsave" type="button">Next / Save</button></a>
					<?php } elseif ($pageid == 'Summary') { ?>

						
						<a href="/"><button class="btn btn-success" type="button">Close / Save</button></a>
						
					<?php } elseif ($pageid == 'Add Products') { ?>
						<a href="{{ route('quote_orders.product',['jobID'=>$jobID]) }}" class="btn btn-danger search_cancel"> Cancel<span class="glyphicon glyphicon-remove"></span> </a> <?php } else { ?> <input type="button" class="btn btn-success" onClick="history.go(-1)" value="Close">
					<?php } ?>
				</div>

				<h4 style="margin-bottom: 0px;margin-top: 0px; float:left;">
					<?php if ($jobID != 0) : ?>
						<strong>Job:: </strong>#<?php echo $jobID ?>
					<?php endif ?>
					<?php if ($pageid == 'Products') { ?>
						<button class="btn btn-xs btn-info" type="button" data-toggle="collapse" data-target="#collapseSpecDetails" aria-expanded="false" aria-controls="collapseSpecDetails">Specifications Details</button> 
					<?php } ?>
				</h4>

			</div>
		</div>
	</div>
</div>

<div class="row hidden-md hidden-lg">
	<div class="col-xs-12 col-sm-12">
		<div class="panel panel-UX">
			<div class="panel-heading" style="border: none;">
				<h4><strong>Job: </strong>#<?php echo $JobID ?>&nbsp; &nbsp;</h4>
			</div>
		</div>
	</div>
</div>
<div class="row hidden-md hidden-lg">
	<div class="col-xs-12 col-sm-12">
		<div class="panel panel-UX">
			<div class="panel-heading" style="border: none;">

				<input type="button" class="btn btn-default" onClick="history.go(0)" value="Undo">
				<?php if ($pageid == 'Header') { ?>
				<?php } elseif ($pageid == 'Add/Edit Products') { ?>
					<a href="<?php echo $BackButton; ?>.php?JobID=<?PHP echo $jobID ?>&JobAcct=<?PHP echo $jobAcct ?>"><button type="button" class="btn btn-danger">Cancel</button></a>
				<?php } else { ?>
					<a href="" class="btn btn-primary">Back</a>
				<?php } ?>
				<?php if ($pageid == 'Header' || $pageid == 'Construction') { ?>
					
					<button class="btn btn-primary" type="submit" name="action" id="nextsave" value="<?php echo $ForwardButton; ?>">Next / Save</button>
				<?php } elseif ($pageid == 'Add/Edit Products') { ?>
					<?php if (isset($stopsave)) { ?>
						<button class="btn btn-primary disabled" disabled="disabled">Save</button>
					<?php } else { ?>
						<button type="submit" value="Validate" class="btn btn-primary">Save</button>
					<?php } ?>
				<?php } elseif ($pageid == 'Products' || $pageid == 'GeneralNotes') { ?>

					<a href="quote_orders/summary"><button class="btn btn-primary" id="nextsave" type="button">Next / Save</button></a>
				<?php } else { ?>
					<a href="<?php echo $ForwardButton; ?>"><button type="button" class="btn btn-success">Close / Save</button></a>
				<?php } ?>
			</div>
		</div>
	</div>
</div>

<?php if ($pageid == 'Products') { ?>
	<div class="collapse" id="collapseSpecDetails">
		<div class="panel panel-UX">
			<table class="table table-hover table-bordered table-striped table-condensed">
				<thead>
					<tr>
						<th style="width: 12.4%"></th>
						<th class="text-center" style="width: 21%">Door/Drawer Fronts</th>
						<th style="width: 12.3%"></th>
						<th class="text-center" style="width: 21%">Style</th>
						<th style="width: 12.3%"></th>
						<th class="text-center" style="width: 21%">Cabinet</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="text-right"><strong>Door</strong></td>
						<td><?php echo $DoorStyleName; ?></td>
						<td class="text-right"><strong>Material</strong></td>
						<td><?php echo $StyleMaterialName; ?></td>
						<td class="text-right"><strong>Material</strong></td>
						<td><?php echo $CabinetMaterialName; ?></td>
					</tr>
					<tr>
						<td class="text-right"><strong>Large Drawer</strong></td>
						<td><?php echo $LargeDrawerStyleName; ?></td>
						<td class="text-right"><strong>Color/Pattern</strong></td>
						<td><?php echo $StyleColorName; ?></td>
						<td class="text-right"><strong>Drawer Box</strong></td>
						<td><?php echo $DrawerBoxName; ?></td>
					</tr>
					<tr>
						<td class="text-right"><strong>Drawer</strong></td>
						<td><?php echo $DrawerStyleName; ?></td>
						<td class="text-right"><strong>Finish</strong></td>
						<td><?php echo $StyleFinishName; ?></td>
						<td class="text-right"><strong>
								<!-- Hinges --></strong></td>
						<td><?php echo ""; ?></td>
					</tr>
				</tbody>
			</table>

		</div>
	</div>
<?php } ?>
