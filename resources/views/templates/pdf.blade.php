<html>
<head>
    <title>{{config('app.name')}}  | Cabinet Manufacturing Excellence</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="user-scalable=yes,
                   initial-scale=1,
                   minimum-scale=0.2, 
                   maximum-scale=2, 
                   width=device-width" 
          name="viewport"
          id="viewport">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* -->
    <base href="{{ $http_base_path }}"> 
    <link rel="manifest" href="{{ $http_base_path }}/favicon/manifest.json">
    
    <!-- Bootstrap 3.3.6 core CSS -->
    <link rel="stylesheet" href="{{ $http_base_path }}/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ $http_base_path }}/css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="{{ $http_base_path }}/css/select2.min.css">
    <link rel="stylesheet" href="{{ $http_base_path }}/css/jquery-ui.css">
    <link rel="stylesheet" href="{{ $http_base_path }}/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="{{ $http_base_path }}/css/custom.css">

  
    <style>
        tr, td{
            font-size: 14px;
        }
        
        .logo {
            border: 1px solid #ffffff;
            border-radius: 4px;
            padding: 5px;
            padding-right: 25px;
            padding-bottom: 25px;
            width: 150px;
        }
        body {
            padding-left: 30px;
            padding-right: 30px;
            }
    </style>
    @yield('styles')    
</head>
<body>
    <div class="container">
        @yield('content')
    </div>
</body>
</html>