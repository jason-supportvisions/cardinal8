
    <div class="form-group">
        <label for="Account" class="col-sm-2 control-label">Account</label>
        <div class="col-sm-10">
            {{ Form::select('AccountID', $accounts_list, $user->AccountID, array('class'=>'form-control')) }}
            {{ $errors->first('AccountID', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">First Name</label>
        <div class="col-sm-10">
            {{ Form::text('FirstName', $user->FirstName, array('class'=>'form-control')) }}
            {{ $errors->first('FirstName', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Last Name</label>
        <div class="col-sm-10">
            {{ Form::text('LastName', $user->LastName, array('class'=>'form-control')) }}
            {{ $errors->first('LastName', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Job Title</label>
        <div class="col-sm-10">
            {{ Form::text('JobTitle', $user->JobTitle, array('class'=>'form-control')) }}
            {{ $errors->first('JobTitle', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            {{ Form::text('Phone', $user->Phone, array('class'=>'form-control')) }}
            {{ $errors->first('Phone', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Extension</label>
        <div class="col-sm-10">
            {{ Form::text('Extension', $user->Extension, array('class'=>'form-control')) }}
            {{ $errors->first('Extension', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Fax</label>
        <div class="col-sm-10">
            {{ Form::text('Fax', $user->Fax, array('class'=>'form-control')) }}
            {{ $errors->first('Fax', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Mobile</label>
        <div class="col-sm-10">
            {{ Form::text('Mobile', $user->Mobile, array('class'=>'form-control')) }}
            {{ $errors->first('Mobile', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <label for="CreatedDate" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            {{ Form::text('Email', $user->email, array('class'=>'form-control')) }}
            {{ $errors->first('Email', '<br><p class="text-danger">:message</p>') }}
        </div>
    </div>

    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <input type="submit" class="btn btn-primary">
        </div>
    </div>