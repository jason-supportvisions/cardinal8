@extends('templates.crm')

@section('content')

<h3>
    Edit Contact {{ $user->FirstName }} {{$user->LastName}}
</h3>

<br>

{{ Form::open(['method'=>'put', 'class'=>'form-horizontal', 'url'=>"crm/contacts/{$user->UserID}"]) }}

    @include('contacts.edit-create')

{{ Form::close() }}

@stop


@section('scripts')

@stop


@section('styles')

@stop

