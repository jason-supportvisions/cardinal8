@extends('templates.crm')

@section('content')


<table class="display" id="contacts-table">
    <thead>
        <tr>
            <th class="text-left" style="width: 194px;">Account</th>
            <th class="text-right" style="width: 85px;">Full Name</th>
            <th class="text-center">Job Title</th>
            <th class="text-center">Phone</th>
            <th class="text-right">Extension</th>
            <th class="text-right">Fax</th>
            <th class="text-right">Mobile</th>
            <th class="text-left">Email</th>
            <th class="dt-center">Edit</th>
        </tr>
    </thead>
    <tbody>
        @foreach($users as $row)
        <tr class="">
            <td class="text-left" > {{ $row->account->Name }}</td>
            <td class="text-left"> {{ $row->FirstName }}{{$row->LastName}}</td>
            <td class="text-center"> {{ $row->JobTitle }}</td>
            <td class="text-center"> {{ $row->Phone }}</td>
            <td class="text-center"> {{ $row->Extension }}</td>
            <td class="text-center"> {{ $row->Fax }}</td>
            <td class="text-center"> {{$row->Mobile}}</td>
            <td class="text-left"> <a href='mailto:{{ $row->email }}'>{{ $row->email }}</td>
            <td class="text-center"> <a href='/crm/contacts/{{ $row->UserID}}/edit'><span title='edit' class='glyphicon glyphicon-pencil'></span></a></td>
        </tr>
        @endforeach
        
    </tbody>
</table>

@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('#contacts-table').DataTable({
        "order": [[ 0, "desc" ]],
        "pageLength": 25,
        "columnDefs": [
            {
                "targets": [ 8 ],
                "orderable": false,
                "searchable": false
            }
        ]
    })
});

</script>

@stop


@section('styles')


@stop

