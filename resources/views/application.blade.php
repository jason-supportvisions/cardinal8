<?php

$jobID = session('jobID');
$jobAcct = session('jobAcct');
$pageid = 'Specification'
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- <link rel="icon" href="<%= BASE_URL %>favicon.ico"> -->

  <title>{{config('app.name')}}  || Specification</title>
  <!-- Styles -->
  @include('layouts.vue_header')
  <link rel="stylesheet" href="{{ asset(mix('css/main.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/iconfont.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/material-icons/material-icons.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/vuesax.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/prism-tomorrow.css')) }}">
  <link rel="stylesheet" href="{{ asset(mix('css/app.css')) }}">
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.5.2/select2-bootstrap.min.css">
  <!-- Favicon -->
  <link rel="shortcut icon" href="{{ asset('images/logo/favicon.png') }}">

</head>

<body>
  <div class="container">
    @include('partials.primary_navbar')
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="padding-bottom: 15px">
        <div class="btn-group" style="width: 100%;">
          <button class="btn btn-xs btn-UX-done" type="button" name="action" value="Job" style="width: 20%; margin-left: 0px; border-right: 0px;">Job &nbsp;&nbsp;<span class="" disabled><i class="check-green glyphicon glyphicon-ok"></i></span></button>
          <button class="btn btn-xs btn-primary" type="button" name="action" value="Specification" style="width: 20%; margin-left: 0px; border-right: 0px;">Specifications</button>
          <button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Uploads" style="width: 20%; margin-left: 0px; border-right: 0px;" disabled>
            Uploads
          </button>
          <button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Products" style="width: 20%; margin-left: 0px;" disabled>
            Products
          </button>
          <button class="btn btn-xs btn-UX-todo" type="button" name="action" value="Summary" style="width: 20%; margin-left: 0px;" disabled>
            Summary
          </button>
        </div>
      </div>
  </div>
  </div>
  <div class="container">
    <div class="row hidden-xs hidden-sm">
      <div class="col-xs-12 col-sm-12">
        <div class="panel panel-UX">
          <div class="panel-heading" style="border: none;height: 42px;">
            <div class="pull-right" style="margin-right: -10px;margin-top: -5px;margin-bottom: -5px">
              <a href="/quote_orders" class="btn btn-primary" style="color: white;">Back</a>
              <a href=""><button class="btn btn-primary" type="" name="action" value="">Next / Save</button>
              </a>
              
            </div>
            <h4 style="margin-bottom: 0px;margin-top: 0px; float:left;">
                <?php if (isset($jobID)) : ?>
                  <strong>Job: </strong>#<?php echo $jobID ?>
                <?php endif ?>
              </h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="container">
    <div id="app"></div>
  </div>
  <noscript>
    <strong>We're sorry but Vuexy - Vuejs, HTML & Laravel Admin Dashboard Template doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
  </noscript>


  <!-- <script src="js/app.js"></script> -->

  <script src="{{ asset(mix('js/app.js')) }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery-2.2.4.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/select2.full.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/ie10-viewport-bug-workaround.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/moment.min.js') }}"></script>
  <script type="text/javascript" src="{{ asset('js/datetime-moment.js') }}"></script>

</body>

</html>