@php
    $Title          =  ' | Gallery';
    $Select2        = 1;
    $Select2beta    = 0;
    $DataTables     = 0;
    $jQuery_UI      = 0;
    $jQuery_Validate= 0;
    $xCRUD_16       = 0;
    $jobPrint       = 0;
    $jobPricing     = 0;

    $SesUser        = session('userid');
    $SesAcct        = session('accountid');
    $SesType        = session('accounttype');
@endphp
@extends('templates.theme')

@section('content')

    <iframe src="{{url('gallery/show')}}" name="gallery-iframe" frameborder="0" width="100%" height="1000"></iframe>

@stop


@section('page-script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight.js"></script>
    <script>
        $(document).ready(function () {    
            $('[data-toggle="tooltip"]').tooltip({html:true});        
            $('[data-toggle="popover"]').popover({html:true}); 
            $('.thumbnail').matchHeight();
        });

    </script>

@endsection


