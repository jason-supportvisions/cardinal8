@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  ' | Gallery';
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;
$public_path = public_path();


$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

if (!empty($_POST['Tag']) and !empty($_POST['Keyword'])) {
    //Tags
    $tagid = $_POST['Tag'];
    $tagResults = DB::select(DB::raw("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder"));

    foreach ($tagResults as $row) {
        $tagName = $row->Name;
        $tagOrder = $row->DisplayOrder;
    }

    //Gallery Images Keyword and Tag
    $Results = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%" . $tagid . "%' AND (Title LIKE '%" . $_POST['Keyword'] . "%' OR Description LIKE '%" .$_POST['Keyword'] . "%') ORDER BY DisplayOrder"));
    $keyword = $_POST['Keyword'];

    //if only TAG is selected
} elseif (isset($_POST['Tag']) and empty($_POST['Keyword'])) {
    //which tag selected
    $tagid = $_POST['Tag'];
    $tagResults = DB::select(DB::raw("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder"));

    if ($tagid != 'None') {
        foreach ($tagResults as $row) {
            $tagName = $row->Name;
            $tagOrder = $row->DisplayOrder;
        }
    }
    //Gallery Images only TAG
    $Results = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%" . $tagid . "%' ORDER BY DisplayOrder"));
    $keyword = '';

    //if only Keyword has data
} elseif (isset($_POST['Keyword']) and empty($_POST['Tag'])) {
    //Gallery Imagesonly Keyword
    $Results = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Title LIKE '%" . $_POST['Keyword'] . "%' OR Description LIKE '%" . $_POST['Keyword'] . "%' ORDER BY DisplayOrder"));
    $keyword = $_POST['Keyword'];

    //no search initiated so show all images
} else {
    $Results = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 ORDER BY DisplayOrder"));
    $keyword = '';
    $tagid = '';
    $tagName = '';
}

?>
@include('layouts.header')
<body>
    <div class="container">
        @include('layouts.navbar')
    </div>
    <div class="container">
        <div class="row row-search">
            <div class="col-sm-12">
                <div class="well well-sm">
                    <div class="col-sm-12">
                        <form action="/gallery/gallery-iframe.php" id="Search" method="post">
                            @csrf
                            <div class="form-horizontal">
                                <div class="form-group" style="margin-bottom: 0px">
                                    <label class="col-sm-1 control-label" for="Keyword">Keyword</label>
                                    <div class="col-sm-3">
                                        <input class="form-control" id="Keyword" name="Keyword" placeholder="Keyword"
                                            type="text"
                                            value="<?php echo $keyword; ?>" />
                                    </div>

                                    <label class="col-sm-1 control-label" for="Tag">Type</label>
                                    <div class="col-sm-3">
                                        <select class="form-control" id="Tag" name="Tag">
                                            <option value="None">Select Type</option>
                                            <?php selected('gallery_tags', $tagid, '', 'Please Select');
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <?php if (!empty($_POST['Tag']) or !empty($_POST['Keyword'])) {
                                        ?>
                                        <a href="gallery/show.php"><button type="button"
                                                class="btn btn-danger">Clear</button></a>
                                        <?php } ?>
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>
                                </div>
                            </div> 
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
	<div class="row row-gallery">
	<?php if (!count($Results)) {
		echo "<div class=\"col-md-3 col-sm-6 col-xs-12 thumb\">";
		echo "<div class=\"thumbnail\">";
		echo "<div class=\"image-box\">";
				echo '<h3 class=\"block-center\">', 'No Images Found', '</h3>';
				echo "</div>";
		echo "</div>";
		echo "</div>";
		echo "</div>";

		(isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;

		//configure number of images per page
		$num_of_records_per_page = 60;
		$total_records = count($Results);

		$start = ($page - 1) * $num_of_records_per_page;
		$nextpage = ($page + 1);
		$prevpage = ($page - 1);
		$total_pages = ceil($total_records / $num_of_records_per_page);

		if ($page > $total_pages) {
			$page = $total_pages;
		}
		if ($page < 1) {
			$page = 1;
		}
		
		$i = 1;
			
	
	}else{
		(isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;

		//configure number of images per page
		$num_of_records_per_page = 60;
		$total_records = count($Results);

		$start = ($page - 1) * $num_of_records_per_page;
		$nextpage = ($page + 1);
		$prevpage = ($page - 1);
		$total_pages = ceil($total_records / $num_of_records_per_page);

		if ($page > $total_pages) {
			$page = $total_pages;
		}
		if ($page < 1) {
			$page = 1;
		}
		
		$i = 1;

	    ?>

		<div class="col-xs-12 align-center pull-right">
			<ul class="pagination">
			
			
			<?php
			if($page == 1){
				echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Previous</a></li>";
			} else {
				echo "<li class=\"paginate_button previous\"><a href=\"?page=$prevpage\">Previous</a></li>";
			}
					
					for($i=1; $i <= $total_pages; $i++) {
						if($i == $page){
							echo "<li class=\"active\"><a href='?page=$i'>$i</a></li>";
						} else {
							echo "<li><a href='?page=$i'>$i</a></li>";
						}
					}
					
				if($page == $total_pages){
				echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Next</a></li>";
				} else {
				echo "<li class=\"paginate_button\"><a href=\"?page=$nextpage\">Next</a></li>";
				}
				?>
				
			
			</ul>
		</div>
        <?php
        if (!empty($_POST['Tag']) and !empty($_POST['Keyword'])) {
            $tagid = $_POST['Tag'];
            $tagResults = DB::select(DB::raw("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder"));
            foreach ($tagResults as $row) {
                $tagName = $row->Name;
                $tagOrder = $row->DisplayOrder;
            }
            $query = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%" . $tagid . "%' AND (Title LIKE '%" . $_POST['Keyword'] . "%' OR Description LIKE '%" . $_POST['Keyword'] . "%') ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page"));
            $keyword = $_POST['Keyword'];
        } elseif (isset($_POST['Tag']) and empty($_POST['Keyword'])) {
            $tagid = $_POST['Tag'];
            $tagResults = DB::select(DB::raw("SELECT Name FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder"));
            $query = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%" . $tagid . "%' ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page"));
            $keyword = '';
        } elseif (isset($_POST['Keyword']) and empty($_POST['Tag'])) {
            $query = DB::select(DB::raw("SELECT * FROM gallery WHERE Active = 1 AND Title LIKE '%" . $_POST['Keyword'] . "%' OR Description LIKE '%" . $_POST['Keyword'] . "%' ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page"));
            $keyword = $_POST['Keyword'];
        } else {
            $query = DB::select(DB::raw("SELECT * FROM gallery ORDER BY DisplayOrder ASC LIMIT $start, $num_of_records_per_page"));
        }

        $thumbnailSize = 340;
        foreach ($query as $row) {
            $title = $row->Title;
            $description = $row->Description;
            $location = $row->Location;
            $imgName = $row->Original_Image;
            $tags = $row->Tags;

            if ($title == '') {
                $title = 'Misc CoryMFG'; //default a title
            }
        
            if ($location == '') {
                $location = 'Brockton, MA'; //default a location
            }

            if (!isset($imgCnt)) {
                $imgCnt = 1;
                echo " <div class='row row-gallery'>";
            } else {
                if ($imgCnt >= 3) {
                    echo "</div>";
                    echo "<div class=\"row row-gallery\">";
                    $imgCnt = 1;
                } else {
                    $imgCnt = $imgCnt + 1;
                }
            } //end "started" check
            //dd(getimagesize('/images/gallery/' . $row->Original_Image));
            list($width, $height, $type, $attr) = getimagesize($public_path.'/images/gallery/' . $row->Original_Image);
            if ($width >= $height) {
                if ($width > $thumbnailSize) {
                    $conPercent = $thumbnailSize / $width;
                    $width = $thumbnailSize;
                    $height = $height * $conPercent;
                } else {
                    $width = $width;
                    $height = $height;
                }
            } else {
                if ($height > $thumbnailSize) {
                    $conPercent = $thumbnailSize / $height;
                    $height = $thumbnailSize;
                    $width = $width * $conPercent;
                } else {
                    $width = $width;
                    $height = $height;
                }
            }
            $width = round($width, 0);
            $height = round($height, 0);
            ?>

            <div class="col-md-4">
                <div class="thumbnail">
                    <div class="center-block pad-bottom">
                        <a data-toggle="modal" href="#<?php echo $row->ID; ?>">
                            <img width="<?php echo $width; ?>" height="<?php echo $height; ?>" style="align-items: center;" src="/images/gallery/<?php echo $imgName; ?>" alt="Cory Manufacturing Kitchen Gallery <?php echo $description; ?>">
                        </a>
                    </div>
                    <br /><br /><br /><br />
                    <div class="image-info">
                        <table>
                            <tr>
                                <td colspan="2">&nbsp; <?php echo $title; ?></td>
                            </tr>
                            <tr>
                                <td class="gallery-city">&nbsp; <small><?php echo $location; ?></small></td>
                                <?php if ($description != '') { ?>
                                <td><a href="#" class="btn btn-xs" data-toggle="tooltip"
                                        title="<?php echo $description; ?>">[spec]</a>
                                </td>
                                <?php } else { ?>
                                <td>&nbsp;</td>
                                <?php } ?>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <?php
            echo "<div class=\"modal modal-main-gallery fade centered-modal\" id=\"" . $row->ID . "\" role=\"dialog\">";
                echo "<div class=\"modal-dialog\">";
                    echo "<div class=\"modal-content\">";
                        echo "<div class=\"modal-header\">";
                            echo "<button type=\"button\" class=\"close\" data-dismiss=\"modal\">&times;</button>";
                            echo "<h4 class=\"modal-title\">$title <small>" . $row->Location . '</small></h4>';
                            echo '</div>';
                        echo "<div class=\"modal-body modal-gallery align-center\">";
                            echo "<img class=\"img-responsive\" alt=\"gallery image\" style=\"max-height:500px;\"
                                src=\"/images/gallery/$imgName\">";

                            echo '</div>';
                        echo "<div class=\"modal-footer\">";
                            echo "<table width='100%' border='0' cellpadding='5' cellspacing='5'>";
                                if ($description != '') {
                                echo "<tr>
                                    <td colspan='100%' align='left'>
                                        &nbsp; $description
                                        <br />
                                        <br />
                                    </td>
                                </tr>";
                                }

                                echo "<tr>
                                    <td align='left'>";
                                        echo "<a class=\"btn btn\" style=\"margin-right:30px;\"
                                            href=\"/images/gallery/$imgName\" target=\"_blank\">Larger Original</a>";
                                        echo "</td>
                                    <td>";
                                        echo "<button type=\"button\" class=\"btn btn-primary\"
                                            data-dismiss=\"modal\">Close</button>";
                                        echo '</td>';
                                    echo "
                                </tr>
                            </table>";
                            echo '</div>';
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
    } ?>
    <div class="col-xs-12 align-center pull-right">
        <ul class="pagination">
            <?php
            if ($page == 1) {
                echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Previous</a></li>";
            } else {
                echo "<li class=\"paginate_button previous\"><a href=\"?page=$prevpage\">Previous</a></li>";
            }

            for ($i = 1; $i <= $total_pages; $i++) {
                 if ($i==$page) { 
                    echo "<li class=\" active\"><a href='?page=$i'>$i</a></li>";
                } else {
                    echo "<li><a href='?page=$i'>$i</a></li>";
                }
            }

            if ($page == $total_pages) {
                echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Next</a></li>";
            } else {
                echo "<li class=\"paginate_button\"><a href=\"?page=$nextpage\">Next</a></li>";
            }
            ?>
        </ul>
    </div>
    </div>
    </div>
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
            $('[data-toggle="popover"]').popover({
                html: true
            });
            $('.thumbnail').matchHeight(); /* match height of gallery boxes */
        });
    </script>
</body>
</html>
