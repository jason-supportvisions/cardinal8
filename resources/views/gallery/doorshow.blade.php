@include('templates.function')
<?php
use Illuminate\Support\Facades\DB;
$Title =  " | Gallery";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');


$StyleGroupLinks =  DB::select(DB::raw("SELECT DISTINCT StyleGroup FROM style_overview WHERE Active = '1' ORDER BY StyleGroup, Title"));
    


?>
@include('layouts.header')
<body>
	<div class="container">
        @include('layouts.navbar')
    </div>
    <!-- Navigation Row -->
			{{-- <div class="row"> --}}
				{{-- <div class="col-sm-12">
					<div class="well well-sm">
						<?php 
							// //show door groups as links
							// foreach ($StyleGroupLinks as $row) {		
							// 	$StyleGroupBookmark1 = str_replace(" ","-",$row->StyleGroup);
							// 	echo "<div class='col-xs-3'>";
							// 	echo "<a href='#$StyleGroupBookmark1'>$row->StyleGroup</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
							// 	echo "</div>";	
							// }
						?>
					</div>
				 </div> --}}
			{{-- </div> --}}
			<!-- End Navigation -->

    <div class="container">
		<div class="col-sm-12">
			<div class="well well-sm">
				<?php 
					//show door groups as links
					foreach ($StyleGroupLinks as $row) {		
						$StyleGroupBookmark1 = str_replace(" ","-",$row->StyleGroup);
						echo "<div class='col-xs-3'>";
						echo "<a href='#$StyleGroupBookmark1'>$row->StyleGroup</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
						echo "</div>";	
					}
				?>
			</div>
		 </div>
	<div class="row row-gallery">
		<?php 
		$groupRowCount = 0;
		

		//EACH GROUP 
		foreach ($StyleGroupLinks  as $row1) { 
			
			$GROUP 					= $row1->StyleGroup;
			$StyleGroupBookmark 	= str_replace(" ","-",$row->StyleGroup);			
			// $Results 				= $conn->query("SELECT * FROM style_overview WHERE StyleGroup = '$GROUP' AND Active = 1 ORDER BY StyleGroup, Title");
			$Results				 =  DB::select(DB::raw("SELECT * FROM style_overview WHERE StyleGroup = '$GROUP' AND Active = 1 ORDER BY StyleGroup, Title"));
			
				if (!$Results) {
					//no results
					echo "<div class='col-md-3 col-sm-6 col-xs-12 thumb'>";
					echo "<div class='thumbnail'>";
					echo "<div class='image-box'>";
							echo "<h3 class='block-center'> 'No Images Found' </h3>";
							echo "</div>";
					echo "</div>";
					echo "</div>";
					echo "</div>";

				}else{
					//show imaeges
					if($groupRowCount == 0){
						$rowEnd = "";
						$groupRowCount = 1; 
					}else{
						$rowEnd = "</div>";
					}
		?>
		
		<!-- Group Title --> 
		<?php
		//echo ending div if it's not the first time through the loop
		echo $rowEnd; 
		?>
		<br />
		<br />
		<br />
		<div class='row' style='width: 95%; padding-left:40px;'>
			<h4 id='<?php echo $StyleGroupBookmark;?>' style='margin-left: 15px;'><?php echo $GROUP; ?></h4>
			<hr style='margin: 10px;'>
		</div>
		<!-- End Group Title -->


		<?php
				
				//var
				$imgCnt = 1;
				$rowCnt = 0;

				//got results for images in each group
				foreach ($Results as $row) { 

					//clean up description string
					$desc = htmlentities($row->Description, ENT_QUOTES);


						
						//preface with </div> or not
						if($rowCnt == 0){
							echo "<!-- NEW ROW#  $rowCnt; ?>-->";
							echo "<div class='row row-gallery'>";
							$rowCnt = 1;
						}elseif($rowCnt <= 4){
							echo "<!-- 
									
									ROW#  $rowCnt 
									
									-->";
							$rowCnt++;
							// echo "</div>"; //if row has been created close it
							// echo "<!-- IMAGES ROW <?php echo $rowCnt;>-->";
							//<!-- echo "<div class='row row-gallery'>"; -->
						}else{
							// echo "<!-- 
									
							// 		ROW#  $rowCnt  reset row counter - divs here
									
							// 		-->";
							$rowCnt = 1;
						}

				
					//insert image unless it's time for a new row
					//encode iumages 
					$doorImg = str_replace(".","_194.","".$row->Original_Image);
					$doorImg = htmlentities($doorImg);
					$doorImg = str_replace(" ","%20","".$doorImg);

					$doorImgLg = $row->Original_Image; 
					$doorImgLg = htmlentities($doorImgLg);
					$doorImgLg = str_replace(" ","%20","".$doorImgLg);



					// put only x number on a row
					// new query just started
					if($imgCnt <= 4) {
					?>
	<!-- image <?php echo $imgCnt; ?> --> 
						<div class="col-sm-3"> 
							<div class="thumbnail">
								<div class="center-block pad-bottom">
									<a data-toggle="modal" href="#<?php echo $row->ID; ?>">
										<img class="img-responsive"  style="width: 200px;" src="/images/style_overview/<?php echo $doorImg; ?>" alt="door drawing" title='Door Image'>	
									</a>
								</div> 
								<div class="image-info pad-bottom">
									<p>
									<?php 
									if($row->BrandID == '1'){
										echo "<img src=\"/images/logos/imperia.png\" style=\"height: 15px;\">";	
									} elseif ($row->BrandID == '2'){
										echo "<img src=\"/images/logos/purekitchen.png\" style=\"height: 15px;\">";
									} elseif ($row->BrandID  == '1,2'){
										echo "<img src=\"/images/logos/imperia.png\" style=\"height: 15px;\"><br>";
										echo "<img src=\"/images/logos/purekitchen.png\" style=\"height: 15px;\">";
									}else {
									
									}
									?>
									</p>
									<p style="padding-left:20px;"><?php echo $row->Title; ?><br />(<?php echo $row->ID; ?>)</p>
									
								</div>
							</div>
						</div>
					<!-- ./image <?php echo $imgCnt; ?> --> 


								<!-- Image <?php echo $imgCnt; ?> Modal -->
								<div class='modal fade' id='<?php echo $row->ID; ?>' role='dialog'>
									<div class='modal-dialog'>
										<div class='modal-content'>
											<div class='modal-header'>
												<button type='button' class='close' data-dismiss='modal'>&times;</button>
												<h4 class='modal-title'><?php echo $row->Title; ?></h4>
											</div>
											<div class='modal-body'>
												<img class='img-responsive' src='/images/style_overview/<?php echo $doorImgLg; ?>' alt='Door Image' title='Door Image'>
												<?php echo $desc; ?>
											</div>
											<div class='modal-footer'>
												<button type='button' class='btn btn-default' data-dismiss='modal'>Close</button>
											</div>
										</div>
									</div>
								</div>
								<!-- ./Image <?php echo $imgCnt; ?> Modal -->

				<?php
						$imgCnt = $imgCnt +1;

					}else{
						echo "</div> <!-- based on image count -->
						
						
						<div class='row row-gallery'>
						";
						$imgCnt = 1;

					}



					//link image tooltip if description present
					if($desc){
						$tooltip = 'data-toggle="tooltip" 
									title="'.$desc.'" 
									data-content="$desc"';
						$tooltipEnd = '</a>';
					}else {
						$tooltip = '';
						$tooltipEnd = '';
					}


			
		
		
		
			}
		} // end images inside of a group while
		?>
		</div><!-- end this group of images -->



				<!-- start new group of images -->
		<div class='row row-gallery'>
				
		<?php
	} // end group while
	?>
	<!-- </div> end finally new row -->
            
            
            
            
	

        

  
   </div>
   
    <!-- /.container -->

	
		

		<div class="col-xs-12 align-center pull-right">
			<ul class="pagination">
			
				
			
			</ul>
		</div>
  
           
            
    </div>
    </div>
    <div class="container">
        @include('layouts.footer')
    </div>
    @include('layouts.loadjs')
 
</body>
</html>
