@extends('templates.crm')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>
            @if($page == 'view')           
            <div class="panel-body">
                <div class="row "> <h2 class="bottom_line">Account<span class="subpage"> - {{ $page}}</span></h2></div>
                <div class="row">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                    </div>
                </div>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#account">Account Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#users">Users</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="account">
                        <table class="table form-horizontal">
                            <tbody>
                                @foreach ($accounts as $row)
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Company Name<td>
                                    <td class="col-sm-9">{{ $row->Name }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Address<td>
                                    <td class="col-sm-9">{{ $row->AddressLine1 }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Address2<td>
                                    <td class="col-sm-9">{{ $row->AddressLine2 }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">City<td>
                                    <td class="col-sm-9">{{ $row->City }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">State<td>
                                    <td class="col-sm-9">{{ $row->State }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Zip<td>
                                    <td class="col-sm-9">{{ $row->Zip }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Phone<td>
                                    <td class="col-sm-9">{{ $row->Phone }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">X Discount<td>
                                    <td class="col-sm-9">{{ $row->MultiplierDiscount }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Brands<td>
                                    @if($row->PrefBrand == 1) <td class="col-sm-9"> Imperia</td>
                                    @elseif($row->PrefBrand == 2) <td class="col-sm-9"> PureKitchen</td>
                                    @elseif($row->PrefBrand == 3) <td class="col-sm-9"> NKD</td>
                                    @elseif($row->PrefBrand == 4) <td class="col-sm-9"> Turkel</td>
                                    @else <td class="col-sm-9"> </td>
                                    @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Default Brands<td>
                                    <td class="col-sm-9">Imperia<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Show In Door Builder<td>
                                    @if($row->show_in_door_builder == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Active<td>
                                    @if($row->Active == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Potential<td>
                                    @if($row->potential == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>                                
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane" id="users">
                        <div class="row "> <h2 class="bottom_line">Users</h2></div>
                        <table id="table" class="table table-striped table-responsive table-bordered dataTable no-footer"  role="grid" aria-describedby="table_info" cellspacing="0">
                            <thead class="account_view">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-left" >Username</th>
                                    <th class="text-left" >First Name</th>
                                    <th class="text-left">Last Name</th>
                                    <th class="text-left">Email</th>
                                    {{-- <th class="dt-left">X Customer</th> --}}
                                    <th class="dt-left" style="width: 100px;">Receive DYOD Emails</th>
                                    <th class="dt-left">Active</th>
                                    <th class="dt-left" > </th>
                                </tr>
                            @if(count($users))
                            <?php //dd($users);?>
                            <tbody>
                                @php $i = 1; @endphp
                                @foreach($users as $row)                                
                                <tr>
                                    <td class="text-center">{{$i++}}</th>
                                    <td class="text-left">{{$row->Username}}</th>
                                    <td class="text-left">{{$row->FirstName}}</th>
                                    <td class="text-left">{{$row->LastName}}</th>
                                    <td class="text-left">{{$row->Email}}</th>
                                    <td class="text-left">{{$row->MultiplierCustomer}}</th>
                                    @if($row->receive_door_spec_emails == 1)
                                    <td class="text-left"> Yes</td>
                                    @else
                                    <td class="text-left"> No</td>
                                    @endif
                                    @if($row->Active == 1)
                                    <td class="text-center" style="background-color: #419641; color:white"> Yes</td>
                                    @else
                                    <td class="text-center" style="background-color: #c12e2a; color:white"> No</td>
                                    @endif
                                    <td class="text-center"><a class="btn btn-sm btn-info" data-toggle="modal" href=""><i class="glyphicon glyphicon-search"></i></a></th>
                                </tr>
                                @endforeach
                            </tbody>
                            @endif
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            @elseif($page == 'edit')
            <div class="panel-body">     
                <div class="row "> <h2 class="bottom_line">Account<span class="subpage"> - {{ $page}}</span></h2></div>       
                <div class="row">
                    <div class="btn-group pull-right">
                        <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                        <button type="button" class="btn btn-success add_btn" onclick="document.querySelector('#account-form').submit()"></i>Save</button>
                    </div>
                </div>
            
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#account">Account Info</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#users">Users</a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="account">
                        {{ Form::open(['method'=>'post', 'id'=>'account-form', 'class'=>'form-horizontal', 'url'=>"admin/account/create"]) }}
                        
                            @foreach($accounts as $row)
                            {{Form::hidden('id',$row->ID)}}
                            @php $accountID = $row->ID; @endphp 
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Company Name</label>
                                <div class="col-sm-9">
                                    {{ Form::text('CompanyName', $row->Name, array('class'=>'form-control')) }}   
                                    @php echo html_entity_decode($errors->first('CompanyName', '<br><p class="text-danger">:message</p>')); @endphp                       
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Address', $row->AddressLine1,  array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Address2', $row->AddressLine2,  array('class'=>'form-control')) }}                         
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    {{ Form::text('City', $row->City, array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">State</label>
                                <div class="col-sm-9">
                                    {{ Form::text('State', $row->State,  array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Zip</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Zip', $row->Zip, array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Phone', $row->Phone,  array('class'=>'form-control')) }}                         
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">X Discount</label>
                                <div class="col-sm-9">
                                    {{ Form::text('XDiscount', $row->MultiplierDiscount,  array('class'=>'form-control','data-pattern'=>'numeric')) }}
                                    @php echo html_entity_decode($errors->first('XDiscount', '<br><p class="text-danger">:message</p>')); @endphp                           
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Brands</label>
                                <div class="col-sm-9">
                                    {{ Form::select('brand', array('' => '-none-', '1' => 'Imperia', '3' => 'NKD',  '2' => 'PureKitchen',  '4' => 'Turkel'), $row->PrefBrand,array('class'=>'form-control','size'=>10)) }}
                                    @php echo html_entity_decode($errors->first('brand', '<br><p class="text-danger">:message</p>')); @endphp
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Default Brand</label>
                                <div class="col-sm-9">
                                    {{ Form::select('defaultbrand', array('' => '-none-', '1' => 'Imperia', '3' => 'NKD',  '2' => 'PureKitchen',  '4' => 'Turkel'), '1',array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Show IN Door Builder</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('builder', $row->show_in_door_builder,true) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('active', $row->Active, true) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Portential</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('potential', $row->potential, false) }}
                                </div>
                            </div>
                            @endforeach
                        {{ Form::close() }}
                    </div>

                    <div class="tab-pane" id="users" class="users_show">
                        <div class="show_user_add">
                            <div class="row "> <h2 class="bottom_line">Users</h2></div>
                            <div class="row">
                                <button type="button" class="btn btn-success add_btn add_user"><i class="glyphicon glyphicon-plus-sign"></i>Add</button>
                            </div>
                            <table id="table" class="table table-striped table-responsive table-bordered dataTable no-footer"  role="grid" aria-describedby="table_info" cellspacing="0">
                                <thead class="account_view">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th class="text-left" >Username</th>
                                        <th class="text-left" >First Name</th>
                                        <th class="text-left">Last Name</th>
                                        <th class="text-left">Email</th>
                                        {{-- <th class="dt-left">X Customer</th> --}}
                                        <th class="dt-left" style="width: 120px;">Receive DYOD Emails</th>
                                        <th class="dt-left">Active</th>
                                        <th class="text-left" > </th>
                                    </tr>
                                @if(count($users))
                                <?php //dd($users);?>
                                <tbody>
                                    @php $i = 1; @endphp
                                    @foreach($users as $row)                                
                                    <tr>
                                        <td class="text-center">{{$i++}}</td>
                                        <td class="text-left">{{$row->Username}}</td>
                                        <td class="text-left">{{$row->FirstName}}</td>
                                        <td class="text-left">{{$row->LastName}}</td>
                                        <td class="text-left">{{$row->Email}}</td>
                                        <td class="text-left">{{$row->MultiplierCustomer}}</td>
                                        @if($row->receive_door_spec_emails == 1)
                                        <td class="text-left"> Yes</td>
                                        @else
                                        <td class="text-left"> No</td>
                                        @endif
                                        @if($row->Active == 1)
                                        <td class="text-center" style="background-color: #419641; color:white"> Yes</td>
                                        @else
                                        <td class="text-center" style="background-color: #c12e2a; color:white"> No</td>
                                        @endif
                                        <td class="text-center" style="width: 100px;padding:5px;">
                                            <span class="btn-group">
                                                <a class="btn btn-sm btn-info "  href="{{route('view-edit',['id'=>$row->UserID,'page'=>'view'])}}" ><i class="glyphicon glyphicon-search"></i></a>
                                                <a class="btn btn-sm btn-warning" href="{{route('view-edit',['id'=>$row->UserID,'page'=>'edit'])}}"><i class="glyphicon glyphicon-edit"></i></a>
                                                <a class="btn btn-sm btn-danger user_delete" href="{{route('user-delete', ['id'=>$row->UserID]) }}" onclick="return confirm('Do you really want remove this user entry?') "><i class="glyphicon glyphicon-remove"></i></a>                                                
                                            </span>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                @endif
                                </thead>
                            </table>
                        </div>
                        <div class="show_user_edit">
                            <div class="row "> <h2 class="bottom_line">Users<span class="subpage"> - Add</span></h2></div>
                            {{ Form::open(['method'=>'post', 'class'=>'form-horizontal', 'route'=>"users.store"]) }}
                            {{Form::hidden('accountid',$accountID)}}
                            <div class="row">
                                <div class="btn-group pull-right">
                                    <button type="button" class="btn btn-danger edit_user"></i>Cancel</button>
                                    <button type="submit" class="btn btn-success add_btn" ></i>Save</button>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Username</label>
                                <div class="col-sm-9">
                                    {{ Form::text('username','', array('class'=>'form-control')) }}   
                                    @php echo html_entity_decode($errors->first('username', '<br><p class="text-danger">:message</p>')); @endphp                       
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Password</label>
                                <div class="col-sm-9">
                                    {{ Form::password('password', array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Email</label>
                                <div class="col-sm-9">
                                    {{ Form::text('email','',  array('class'=>'form-control')) }}                         
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Type</label>
                                <div class="col-sm-9">
                                    {{ Form::select('accounttype', array('' => '-none-', '1' => 'Account Administrator', '-1' => 'Master Administrator',  '2' => 'User'), '2',array('class'=>'form-control')) }}
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">First Name</label>
                                <div class="col-sm-9">
                                    {{ Form::text('firstname','', array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Last Name</label>
                                <div class="col-sm-9">
                                    {{ Form::text('lastname','',  array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    {{ Form::text('addressline1','', array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address 2</label>
                                <div class="col-sm-9">
                                    {{ Form::text('addressline2','',  array('class'=>'form-control')) }}                         
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    {{ Form::text('city','',  array('class'=>'form-control')) }}              
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">State</label>
                                <div class="col-sm-9">
                                    {{ Form::text('state','',  array('class'=>'form-control')) }}                                                            
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Zip Code</label>
                                <div class="col-sm-9">
                                    {{ Form::text('zipcode','',  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    {{ Form::text('phone','',  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                                </div>
                            </div>

                            {{-- <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">X Customer</label>
                                <div class="col-sm-9">
                                    {{ Form::text('multipliercustomer', '1.80000',  array('class'=>'form-control')) }}                                                             
                                </div>
                            </div> --}}

                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Receive DYOD Emails</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('receive_dyod_emails',1, false) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('active', 1, true) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Chart Support</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('chartsupport', 1, false) }}
                                </div>
                            </div>
                            {{ Form::close() }}
                        </div>
                    </div>
                </div>              
            </div>
            @endif
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('.nav-tabs a').click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a[href="#account"]').tab('show');
   
    $('.add_cancel').click(function(){
        window.history.back();
    });
    $('.add_user').click(function(){
        $('.show_user_add').hide();
        $('.show_user_edit').show();
    });
    $('.edit_user').click(function(){
        $('.show_user_add').show();
        $('.show_user_edit').hide();
    });
    
    $('#table').DataTable({
        ordering : false ,
        dom: 'Bfrtip',
        buttons: [
            'print',
            'csv'
        ]       
    });
});


</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
        
    }
    .bottom_line{
        float: left;
        border-bottom: 1px solid #ddd;
        width: 97%;
        margin-left: 15px;
        
    }
    .add_btn{
        margin-left: 15px;
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }

    .subpage { 
        font-size:0.6em; 
    }
    
    .custom_check{
        margin-top: 7px;
    }

    .tab-content{
        box-shadow: 0 1px 1px 1px gray;
        padding: 20px;
    }
    .add_cancel, .edit_user{
        margin-top: 10px;
    }
    #table{
        border-top: 1px solid rgb(175, 169, 169);
        font-size: 14px;
    }
    #table_length ,#table_info, #table_paginate{
        display: none;
    }
    table.dataTable thead th{
        padding:5px;
    }
    table.dataTable thead td{
        padding:0px;
    }
    .show_user_edit{
        display: none;
    }
</style>
@stop


