@extends('templates.crm')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>           
            <div class="panel-body">               
                <div class="user-edit-div">
                    <div class="row "> <h2 class="bottom_line">Users<span class="subpage"> - {{$page}}</span></h2></div>
                    {{ Form::open(['method'=>'post', 'class'=>'form-horizontal', 'route'=>"user-store"]) }}
                    
                        <div class="row">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                                <button type="submit" class="btn btn-success add_btn" ></i>Save</button>
                            </div>
                        </div>
                     
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                {{ Form::text('username', '', array('class'=>'form-control')) }}   
                                @php echo html_entity_decode($errors->first('username', '<br><p class="text-danger">:message</p>')); @endphp                    
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                {{ Form::password('password', array('class'=>'form-control')) }}
                                @php echo html_entity_decode($errors->first('password', '<br><p class="text-danger">:message</p>')); @endphp                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                {{ Form::text('email', '',  array('class'=>'form-control')) }}  
                                @php echo html_entity_decode($errors->first('email', '<br><p class="text-danger">:message</p>')); @endphp                       
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Account</label>
                            <div class="col-sm-9">
                                {{ Form::select('accountid', $accounts, 55, array('class'=>'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-9">
                                {{ Form::select('accounttype', array('' => '-none-', '1' => 'Account Administrator', '-1' => 'Master Administrator',  '2' => 'User'), '2',array('class'=>'form-control')) }}
                            </div>
                        </div>                       
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('firstname', '', array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('lastname', '',  array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                {{ Form::text('addressline1', '', array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Address 2</label>
                            <div class="col-sm-9">
                                {{ Form::text('addressline2', '',  array('class'=>'form-control')) }}                         
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">City</label>
                            <div class="col-sm-9">
                                {{ Form::text('city', '',  array('class'=>'form-control')) }}              
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">State</label>
                            <div class="col-sm-9">
                                {{ Form::text('state', '', array('class'=>'form-control')) }}                                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Zip Code</label>
                            <div class="col-sm-9">
                                {{ Form::text('zipcode', '',  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                {{ Form::text('phone', '',  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">X Customer</label>
                            <div class="col-sm-9">
                                {{ Form::text('multipliercustomer', '',  array('class'=>'form-control')) }}                                                             
                            </div>
                        </div> --}}
                        
                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Active</label>
                            <div class="col-sm-9 custom_check">                               
                                {{ Form::checkbox('active', 1, true) }}                              
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Phone Log</label>
                            <div class="col-sm-9 custom_check">                              
                                {{ Form::checkbox('includeinphonelog', 0, false) }}                               
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Chart Support</label>
                            <div class="col-sm-9 custom_check">                               
                                {{ Form::checkbox('ChatSupport', 0, false) }}                               
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
       
            </div>
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
     $('.add_cancel').click(function(){
        window.history.back();
    });
    $(document).ready(function() {
        $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'csv'
            ],
            "pageLength": 1000,
            "columnDefs": [
                {
                    "targets": [ 5 ],
                    "orderable": false,
                    "searchable": false
                }
            ]
        });
      
});

</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
    }
    .bottom_line{
        float: left;
        border-bottom: 1px solid #ddd;
        width: 97%;
        margin-left: 15px;
        
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    .user-view-table{
        border-top: 1px solid rgb(175, 169, 169);
        margin-top: 20px;
    }
    .subpage { 
        font-size:0.8em;
        color:gray; 
    }
    .add_cancel{
        margin-top: 10px;
    }
</style>
@stop


