@extends('templates.crm')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>           
            <div class="panel-body">
                <div class="row "> <h2 class="bottom_line">Account<span class="subpage"> - {{ $page}}</span></h2></div>
                {{ Form::open(['method'=>'post', 'class'=>'form-horizontal', 'url'=>"admin/account/create"]) }}
                    {{Form::hidden('act','accountCreate')}}
                    <div class="row">
                        <div class="btn-group pull-right">
                            <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                            <button type="submit" class="btn btn-success add_btn"></i>Save</button>
                        </div>
                    </div>
                
                    <ul class="nav nav-tabs">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#account">Account Info</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="account">

                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Company Name</label>
                                <div class="col-sm-9">
                                    {{ Form::text('CompanyName','', array('class'=>'form-control')) }}   
                                    @php echo html_entity_decode($errors->first('CompanyName', '<br><p class="text-danger">:message</p>')); @endphp                       
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Address','',  array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Address2','',  array('class'=>'form-control')) }}                         
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">City</label>
                                <div class="col-sm-9">
                                    {{ Form::text('City','', array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">State</label>
                                <div class="col-sm-9">
                                    {{ Form::text('State','',  array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Zip</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Zip','', array('class'=>'form-control')) }}                          
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">Phone</label>
                                <div class="col-sm-9">
                                    {{ Form::text('Phone','',  array('class'=>'form-control')) }}                         
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="CreatedDate" class="col-sm-3 control-label">X Discount</label>
                                <div class="col-sm-9">
                                    {{ Form::text('XDiscount','',  array('class'=>'form-control','data-pattern'=>'numeric')) }}
                                    @php echo html_entity_decode($errors->first('XDiscount', '<br><p class="text-danger">:message</p>')); @endphp                           
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Brands</label>
                                <div class="col-sm-9">
                                    {{ Form::select('brand', array('' => '-none-', '1' => 'Imperia', '2' => 'NKD-JJ',  '3' => 'PureKinchen-JJ'), '',array('class'=>'form-control','size'=>10)) }}
                                    @php echo html_entity_decode($errors->first('brand', '<br><p class="text-danger">:message</p>')); @endphp
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Default Brand</label>
                                <div class="col-sm-9">
                                    {{ Form::select('defaultbrand', array('' => '-none-', '1' => 'Imperia', '2' => 'NKD-JJ',  '3' => 'PureKinchen-JJ'), '1',array('class'=>'form-control')) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Show IN Door Builder</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('builder',1,true) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Active</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('active', 1, true) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="Account" class="col-sm-3 control-label">Portential</label>
                                <div class="col-sm-9 custom_check">
                                    {{ Form::checkbox('potential', 1, false) }}
                                </div>
                            </div>
                        </div>
                    </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('.nav-tabs a').click(function(){
        $(this).tab('show');
    });
    $('.nav-tabs a[href="#account"]').tab('show');
    $('.nav-tabs a:first').tab('show');
    $('.nav-tabs a:last').tab('show'); 
    $('.add_cancel').click(function(){
        window.history.back();
    });
});


</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
        
    }
    .bottom_line{
        float: left;
        border-bottom: 1px solid #ddd;
        width: 97%;
        margin-left: 15px;
        
    }
    .add_btn{
        margin-left: 15px;
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }

    .subpage { 
        font-size:0.6em; 
    }
    
    .custom_check{
        margin-top: 7px;
    }

    .tab-content{
        box-shadow: 0 1px 1px 1px gray;
        padding: 20px;
    }
    .add_cancel{
        margin-top: 10px;
    }
</style>
@stop


