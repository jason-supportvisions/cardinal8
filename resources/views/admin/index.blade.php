@extends('templates.crm')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>           
            <div class="panel-body">
                <div class="row content-header "> <h2 class="bottom_line">Users</h2></div>
                <div class="content-body">
                    <div class="row">
                    <a href="{{route('users.create')}}"> <button type="button" class="btn btn-success add_btn"><i class="glyphicon glyphicon-plus-sign"></i>Add</button></a>
                    </div>
                    <table id="table" class="table table-striped table-responsive table-bordered dataTable no-footer"  role="grid" aria-describedby="table_info" cellspacing="0">
                        <thead class="account_view">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-left" >Username</th>
                                <th class="text-left" >First Name</th>
                                <th class="text-left">Last Name</th>
                                <th class="text-left">Email</th>
                                {{-- <th class="dt-left">X Customer</th> --}}
                                <th class="dt-left" style="width: 120px;">Receive DYOD Emails</th>
                                <th class="dt-left">Active</th>
                                <th class="text-left" > </th>
                            </tr>
                        @if(count($users))
                        <?php //dd($users);?>
                        <tbody>
                            @php $i = 1; @endphp
                            @foreach($users as $row)                                
                            <tr>
                                <td class="text-center">{{$i++}}</td>
                                <td class="text-left">{{$row->Username}}</td>
                                <td class="text-left">{{$row->FirstName}}</td>
                                <td class="text-left">{{$row->LastName}}</td>
                                <td class="text-left">{{$row->Email}}</td>
                                <td class="text-left">{{$row->MultiplierCustomer}}</td>
                                @if($row->receive_door_spec_emails == 1)
                                <td class="text-left"> Yes</td>
                                @else
                                <td class="text-left"> No</td>
                                @endif
                                @if($row->Active == 1)
                                <td class="text-center" style="background-color: #419641; color:white"> Yes</td>
                                @else
                                <td class="text-center" style="background-color: #c12e2a; color:white"> No</td>
                                @endif
                                <td class="text-center" style="width: 110px;padding:5px;">
                                    <span class="btn-group">
                                        <a class="btn btn-sm btn-info "  href="{{route('view-edit',['id'=>$row->UserID,'page'=>'view'])}}" ><i class="glyphicon glyphicon-search"></i></a>
                                        <a class="btn btn-sm btn-warning" href="{{route('view-edit',['id'=>$row->UserID,'page'=>'edit'])}}"><i class="glyphicon glyphicon-edit"></i></a>
                                        <a class="btn btn-sm btn-danger user_delete" href="{{route('user-delete', ['id'=>$row->UserID]) }}" onclick="return confirm('Do you really want remove this user entry?') "><i class="glyphicon glyphicon-remove"></i></a>                                               
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                        @endif
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('#table').DataTable({
        ordering : false ,
        dom: 'Bfrtip',
        buttons: [
            'print',
            'csv'
        ],   
        "pageLength": 1000,    
    });
      
});

</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
    }
    .bottom_line{
        border-bottom: 1px solid #ddd;
    }
    .add_btn{
        margin-left: 15px;
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    #table{
        border-top: 1px solid rgb(175, 169, 169);
        font-size: 14px;
    }
    table.dataTable thead th{
        padding:5px;
    }
</style>
@stop


