@extends('templates.crm')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>           
            <div class="panel-body">
                @if($page == 'view')
                <div class="user-view-div">
                    <div class="row content-header "> <h2 class="bottom_line">Users<span class="subpage"> - {{ $page}}</span></h2></div>
                    <div class="content-body">
                        <div class="row">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                            </div>
                        </div>
                        <table class="table form-horizontal user-view-table">
                            <tbody>
                                @foreach ($user as $row)
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Username<td>
                                    <td class="col-sm-9">{{ $row->Username }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Password<td>
                                    <td class="col-sm-9">*****<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Email<td>
                                    <td class="col-sm-9">{{ $row->Email }}<td>
                                </tr>
                                @php 
                                foreach ($accounts as $key=>$value){
                                    
                                    if($key == $row->AccountID){
                                        $user_account = $value;
                                    }
                                }
                                @endphp
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Account<td>
                                    <td class="col-sm-9">{{ $user_account }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Account Type<td>
                                        @if($row->AccountType == 1) <td class="col-sm-9">Account Administrator<td>
                                        @elseif($row->AccountType == -1) <td class="col-sm-9">Master Administrator<td>
                                        @elseif($row->AccountType == 2) <td class="col-sm-9">User<td>
                                        @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">First Name<td>
                                    <td class="col-sm-9">{{ $row->FirstName }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Last Name<td>
                                    <td class="col-sm-9">{{ $row->LastName }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Address<td>
                                    <td class="col-sm-9">{{ $row->AddressLine1 }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Address2<td>
                                    <td class="col-sm-9">{{ $row->AddressLine2 }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">City<td>
                                    <td class="col-sm-9">{{ $row->City }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">State<td>
                                    <td class="col-sm-9">{{ $row->State }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Zip Code<td>
                                    <td class="col-sm-9">{{ $row->ZipCode }}<td>
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Phone<td>
                                    <td class="col-sm-9">{{ $row->Phone }}<td>
                                </tr>
                                {{-- <tr class="form-group">
                                    <td class="control-label col-sm-3">X Customer<td>
                                    <td class="col-sm-9">{{ $row->MultiplierDiscount }}<td>
                                </tr> --}}
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Active<td>
                                    @if($row->Active == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Phone Log<td>
                                    @if($row->IncludeInPhoneLog == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>
                                <tr class="form-group">
                                    <td class="control-label col-sm-3">Chart Support<td>
                                    @if($row->CharSupport == 0)
                                    <td class="col-sm-9"> No</td>
                                    @else
                                    <td class="col-sm-9"> Yes</td>
                                    @endif
                                </tr>                                
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                @else
                <div class="user-edit-div">
                    <div class="row "> <h2 class="bottom_line">Users<span class="subpage"> - {{$page}}</span></h2></div>
                    {{ Form::open(['method'=>'post', 'class'=>'form-horizontal', 'route'=>"users-update"]) }}
                    
                        <div class="row">
                            <div class="btn-group pull-right">
                                <button type="button" class="btn btn-danger add_cancel"></i>Cancel</button>
                                <button type="submit" class="btn btn-success add_btn" ></i>Save</button>
                            </div>
                        </div>
                        @foreach ($user as $row)
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Username</label>
                            <div class="col-sm-9">
                                {{ Form::text('username', $row->Username, array('class'=>'form-control')) }}   
                                @php echo html_entity_decode($errors->first('username', '<br><p class="text-danger">:message</p>')); @endphp  
                                {{Form::hidden('userid',$row->UserID)}}                     
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Password</label>
                            <div class="col-sm-9">
                                {{ Form::password('password',  array('class'=>'form-control','placeholder'=>'*****')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Email</label>
                            <div class="col-sm-9">
                                {{ Form::text('email', $row->email,  array('class'=>'form-control')) }}                         
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Account</label>
                            <div class="col-sm-9">
                                {{ Form::select('accountid', $accounts, $row->AccountID,array('class'=>'form-control')) }}
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Type</label>
                            <div class="col-sm-9">
                                {{ Form::select('accounttype', array('' => '-none-', '1' => 'Account Administrator', '-1' => 'Master Administrator',  '2' => 'User'), '2',array('class'=>'form-control')) }}
                            </div>
                        </div>                       
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">First Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('firstname', $row->FirstName, array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Last Name</label>
                            <div class="col-sm-9">
                                {{ Form::text('lastname', $row->LastName,  array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Address</label>
                            <div class="col-sm-9">
                                {{ Form::text('addressline1', $row->AddressLine1, array('class'=>'form-control')) }}                          
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Address 2</label>
                            <div class="col-sm-9">
                                {{ Form::text('addressline2', $row->AddressLine2,  array('class'=>'form-control')) }}                         
                            </div>
                        </div>
                    
                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">City</label>
                            <div class="col-sm-9">
                                {{ Form::text('city', $row->City,  array('class'=>'form-control')) }}              
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">State</label>
                            <div class="col-sm-9">
                                {{ Form::text('state', $row->State, array('class'=>'form-control')) }}                                                            
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Zip Code</label>
                            <div class="col-sm-9">
                                {{ Form::text('zipcode', $row->ZipCode,  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">Phone</label>
                            <div class="col-sm-9">
                                {{ Form::text('phone', $row->Phone,  array('class'=>'form-control','data-pattern'=>'numeric')) }}                                                             
                            </div>
                        </div>

                        {{-- <div class="form-group">
                            <label for="CreatedDate" class="col-sm-3 control-label">X Customer</label>
                            <div class="col-sm-9">
                                {{ Form::text('multipliercustomer', $row->multipliercustomer,  array('class'=>'form-control')) }}                                                             
                            </div>
                        </div> --}}

                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Receive DYOD Emails</label>
                            <div class="col-sm-9 custom_check">
                                @if($row->receive_door_spec_emails == 1)
                                {{ Form::checkbox('receive_door_spec_emails', 1, true) }}
                                @else
                                {{ Form::checkbox('receive_door_spec_emails', 0, false) }}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Active</label>
                            <div class="col-sm-9 custom_check">
                                @if($row->Active == 1)
                                {{ Form::checkbox('active', 1, true) }}
                                @else
                                {{ Form::checkbox('active', 0, false) }}
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="Account" class="col-sm-3 control-label">Chat Support</label>
                            <div class="col-sm-9 custom_check">
                                @if($row->chartsupport == 1)
                                {{ Form::checkbox('ChartSupport', 1, true) }}
                                @else
                                {{ Form::checkbox('ChartSupport', 0, false) }}
                                @endif
                            </div>
                        </div>
                        @endforeach
                    {{ Form::close() }}
                </div>
                @endif
            </div>
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
     $('.add_cancel').click(function(){
        window.history.back();
    });
    $(document).ready(function() {
        $('#table').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'print',
                'csv'
            ],
            "pageLength": 1000,
            "columnDefs": [
                {
                    "targets": [ 5 ],
                    "orderable": false,
                    "searchable": false
                }
            ]
        });
      
});

</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
    }
    .bottom_line{
        float: left;
        border-bottom: 1px solid #ddd;
        width: 97%;
        margin-left: 15px;
        
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    .user-view-table{
        border-top: 1px solid rgb(175, 169, 169);
        margin-top: 20px;
    }
    .subpage { 
        font-size:0.8em;
        color:gray; 
    }
    .add_cancel{
        margin-top: 10px;
    }
</style>
@stop


