@extends('templates.crm')

@section('content')


<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-UX">
            <div class="panel-heading">Settings</div>           
            <div class="panel-body">
                <div class="row content-header "> <h2 class="bottom_line">Account</h2></div>
                <div class="content-body">
                    <div class="row">
                    <a href="{{route('add-account')}}"> <button type="button" class="btn btn-success add_btn"><i class="glyphicon glyphicon-plus-sign"></i>Add</button></a>
                    </div>
                    <table id="table" class="table table-striped table-responsive table-bordered dataTable no-footer"  role="grid" aria-describedby="table_info" cellspacing="0">
                        <thead>
                            <tr>
                                <th class="text-left">Company Name</th>
                                <th class="text-left">X Discount</th>
                                <th class="text-right">Show in Door Builder</th>
                                <th class="text-right">Active</th>
                                <th class="text-left">Potential</th>
                                <th class="dt-center"> </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($accounts as $row)
                            <tr class="" role="row" >
                                <td class="text-left" > {{ $row->Name }}</td>
                                <td class="text-left"> {{ $row->MultiplierDiscount }}{{$row->LastName}}</td>
                                @if($row->show_in_door_builder == 0)
                                    <td class="text-center" style="background-color: #c12e2a; color:white "> No</td>
                                @else
                                    <td class="text-center" style="background-color: #419641; color:white "> Yes</td>
                                @endif
                                @if($row->Active == 0)
                                    <td class="text-center" style="background-color: #c12e2a; color:white "> No</td>
                                @else
                                    <td class="text-center" style="background-color: #419641; color:white "> Yes</td>
                                @endif
                                @if($row->potential == 0)
                                    <td class="text-center" style="background-color:#419641; color:white "> No</td>
                                @else
                                    <td class="text-center" style="background-color:#F69231; color:white "> Yes</td>
                                @endif
                                <td class="text-center"> 
                                    <span class="btn-group">
                                        <a class="btn btn-sm btn-info"  href="{{route('show-edit',['id'=>$row->ID,'page'=>'view'])}}"><i class="glyphicon glyphicon-search"></i></a>
                                        <a class="btn btn-sm btn-warning" href="{{route('show-edit',['id'=>$row->ID,'page'=>'edit'])}}"><i class="glyphicon glyphicon-edit"></i></a>
                                    </span>
                                </td>
                            </tr>
                            @endforeach
                            
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>



@stop


@section('scripts')

<script>
$(document).ready(function() {
    $('#table').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'print',
            'csv'
        ],
        "pageLength": 1000,
        "columnDefs": [
            {
                "targets": [ 5 ],
                "orderable": false,
                "searchable": false
            }
        ]
    })
      
});

</script>

@stop


@section('styles')
<style>
    .content-header{
        padding: 0px 15px;
    }
    .bottom_line{
        border-bottom: 1px solid #ddd;
    }
    .add_btn{
        margin-left: 15px;
    }
    .pull-right{
        margin-top: 10px;
        margin-right: 15px;
    }
    #table{
        border-top: 1px solid rgb(175, 169, 169);
        font-size: 14px;
    }
</style>
@stop


