@extends('templates.products')

@section('content')

{{ Form::open(['method'=>'get', 'id'=>'filters-form']) }}

<h3>
    Product Images - Catalog Category: 
    {{ Form::select('catalog_category_id', [0=>'all'] + $catalog_category_list, $catalog_category_id) }}
</h3>

{{ Form::close() }}


{{ Form::open(['id'=>'content-form']) }}

{{ Form::submit('save') }}

<table class="table table-striped">
    <thead>
        <tr>
            {{-- <th>Catalog Order</th>
            <th>Catalog Category</th>
            <th>Category</th> --}}
            <th>Group Code</th>
            {{-- <th>Description</th>
            <th>Extra Notes (one per line)</th> --}}
            <th style="min-width: 150px">Image</th>
            <th>SVG</th>
            {{-- @if($user->hasAccountType('master-administrator'))
            <th>Delete</th>
            @endif --}}
        </tr>
    </thead>
    <tbody>

    @foreach($catalog_categories as $catelog_category)
        @foreach($catelog_category->product_groups as $product_group)
        <tr valign="top">
            {{-- <td>
                {{ Form::number("product_groups[{$product_group->ID}][catalog_order]", $product_group->catalog_order, ['class'=>'catalog-order-setting']) }}
            </td>
            <td>
                {{ Form::select("product_groups[{$product_group->ID}][catalog_category_id]", $catalog_category_list, $product_group->catalog_category_id, ['class'=>'catalog-category-setting']) }}
            </td> --}}
            {{-- <td>{{ $product_group->Category }}</td> --}}
            <td>{{ $product_group->GroupCode }}</td>
            {{-- <td>{{ $product_group->Description }}</td> --}}
            {{-- <td>
                <textarea 
                    style="white-space: nowrap; overflow: auto; width: 300px; padding: 5px;"
                    rows="{{ $product_group->notes->count() + 1 }}"
                    name="product_groups[{{ $product_group->ID }}][note_lines]"
                >{{ $product_group->note_lines }}</textarea>
            </td> --}}
            <td>
                @if($product_group->image_address)
                <a href="{{ $product_group->image_address }}" target="_BLANK">
                    <img style="width: 100px;" src="{{ $product_group->image_address }}">
                </a>
                @endif
            </td>
            <td>
                @if($product_group->svg_address)
                    Present
                @else 
                    <span style='color: #DDD'>Mising</span>
                @endif
            </td>
            {{-- @if($user->hasAccountType('master-administrator'))
            <td>
                <a url="/backend/product-images/{{ $product_group->ID }}"
                        message='Delete product group "{{{ $product_group->GroupCode }}}" ?'
                        class="btn btn-xs btn-danger delete-button">
                        Delete
                </a>
            </td>
            @endif --}}
        </tr>
        @endforeach
    @endforeach

    </tbody>
</table>

{{ Form::submit('save') }}

{{ Form::close() }}

@stop

@section('scripts')

<script>

    $('select[name=catalog_category_id]').change(function(){
        $('#filters-form').submit();
    });

    $('#content-form').submit(function(e){
        var ans = confirm('Save updates?');
        if(!ans){e.preventDefault();}
    });

    $('.delete-button').click(function(e){
        e.preventDefault();
        var $button = $(this);
        var url     = $button.attr('url');
        var ans     = confirm($button.attr('message'));
        var $tr     = $button.closest('tr');

        if(ans){
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function(result) {
                    $tr.fadeOut( function() { $(this).remove(); }); 
                }
            });
        }
    });

</script>

@stop

@section('styles')

<style>

select.catalog-category-setting{
    width: 200px;
}

#content-form input.catalog-order-setting{
    min-width: 50px;
    width: 50px;
}

</style>

@stop
