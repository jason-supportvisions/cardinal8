@extends('templates.products')
<!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

@section('content')
<?php include "../public/includes/functions.php"; ?>

<h3>Accessory Item Manager </h3>
    <div class="pull-right">
    {{ Form::open(['id'=>'content-form']) }}
        <input type="submit" value="UPDATE ALL" class="btn btn-primary">
    </div>
    
<style>

.table-condensed{
  font-size: 12px;
}
.th-center {
   text-align: center;   
}
</style>

<div class="container">

<!--- PRICE MODAL --------->
        <div class="modal fade" id="price_modal">
        <div class="col-md-4 text-center">
            <form action="/upload.php" method="post" enctype="multipart/form-data">        
                <div class="modal-dialog" role="dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        </div>
                        <div class="modal-body">
                        <h2>Product Title + ID</h2><br>
                        <table cellspacing="5" cellpadding="5" align="center" border="0" width="300px">
                            <tr>
                                <td align="right">List: <br><br></td>
                                <td>$<input type="text" name="list" size="5"><br><br></td>
                            </tr>
                            <tr>
                                <td align="right">Cost: <br><br></td>
                                <td>$<input type="text" name="cost" size="5"><br><br></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><label class="btn btn-default btn-primary">
                                        &nbsp;&nbsp; Update &nbsp;&nbsp;<input type="submit" style="display: none;" name="submit">
                                    </label>
                                </td>
                            </tr>
                        </table>     
                       
                        Hidden ID:  <input type="text" name="id" id="id" value="" />
                        <input type="hidden" id="productId" name="product_id" />
                            
                        </div>
                        <div class="modal-footer">
                            
                        </div>
                    </div>
            </form>
        </div>
        </div>
<!------- END MODAL ----->


</div>
<table class="table table-striped table-condensed"> 
     <thead>
        <tr>
            <th>Group / Item</th>
            <th>Description</th>        
            <th>Notes</th>
            <th class="th-center">Image</th>
            <th class="th-center">List <br />[Cost]</th>
            @if($user->hasAccountType('master-administrator'))
            <th class="th-center"></th>
            @endif    
        </tr>
    </thead>
    <tbody>
    <?php
        //call delete function
        if(isset($_GET['makeInactive'])){
            if ($_GET['makeInactive'] == 'true'){                         
                $activeResults = updateRow("product_items", "ID", $_GET['id'], "Active", 0);   

            }
        }
        //call restore function
        if(isset($_GET['makeActive'])){
            if ($_GET['makeActive'] == 'true'){                         
                $activeResults = updateRow("product_items", "ID", $_GET['id'], "Active", 1);   

            }
        }
    ?>
    @foreach($product_groups as $product_group)

        @foreach($product_group->items as $product_item)

        <?php $product_price = $product_item->pricingOptions(); ?>

        <!-- 
            <pre>
                <?php 
                    //echo "<br />" . $product_price; 
                   // print_r($product_price);
                ?>
            </pre>
        -->          
                <tr valign="top">
                    <td>{{ $product_item->GroupCode }} <br> <b><?php echo $product_item->Code; ?></b></td>
                    <td width="200">{{ $product_item->Description }}</td>
                    <td>                
                        <textarea 
                            style="white-space: nowrap; overflow: auto; width: 350px; padding: 5px;"
                            rows="5"
                            name="product_items[{{ $product_item->ID }}][Notes]"><?php echo $product_item->Notes; ?></textarea>           
                    </td>                    
                    <td style="vertical-align: middle;"> 
                        @if($product_item->Image)
                            <div class="th-center">

                            <a href="https://cardinal.corymfg.com/{{ $product_item->Image }}" target="_BLANK">
                                <img style="width: 60px;" src="https://cardinal.corymfg.com/{{ $product_item->Image }}"> 
                            </a> 
                            <br>
                            <a href="" data-target="#my_modal" data-toggle="modal" class="btn btn-xs btn-upload" data-id="<?php echo $product_item->ID; ?>" data-name="CabNameVar">Update</a>
                            </div>
                        @else
                            <div class="text-center vcenter">
                            <a href="" data-target="#my_modal" data-toggle="modal" class="btn btn-xs btn-upload" data-id="<?php echo $product_item->ID; ?>" data-name="CabNameVar">Add Image</a>
                            </div>
                        @endif
                    </td>
                    <td style="vertical-align: middle;"> 
                        <div class="th-center"> <?php //echo $_POST['itemList']; ?>
                            $<?php echo $product_price["List"]; ?><br />
                            <?php echo "[$" . $product_price["Cost"] . "]"; ?>
                            <br>
                            <a href="" data-target="#price_modal" data-toggle="modal" data-id="<?php echo $product_item->ID; ?>" data-name="price_modal">Update</a>
                        <!-- $ <input type="text" value="<?php echo $product_price["List"]; ?>" name="itemList" id="itemList" style="width: 50px;"> -->
                        <!-- <br><a href="accessory-images?makeInactive=true&catalog_category_id=11&id=<?php echo $product_item->ID; ?>" class="btn btn-xs"> Update</a> -->
                        </div>
                    </td>
                    <td style="vertical-align: middle;">
                        <div class="th-center">
                         <?php if($product_item->Active != 0){ ?>
                            <a href="accessory-images?makeInactive=true&catalog_category_id=11&id=<?php echo $product_item->ID; ?>" class="btn btn-xs btn-danger"> Delete</a>
                         <?php }else{ ?>
                            <a href="accessory-images?makeActive=true&catalog_category_id=11&id=<?php echo $product_item->ID; ?>" class="btn btn-xs btn-success"> Restore</a>
                         <?php } ?>
                        </div>
                    </td>
                    

                </tr>

        @endforeach 



    @endforeach 
    </tbody>
</table>

    <div class="pull-right">
    {{ Form::open(['id'=>'content-form']) }}
        <input type="submit" value="UPDATE ALL" class="btn btn-primary">
    </div>
{{ Form::close() }}
<br /><br /><br /><br /><br /><br /><br /><br /><br /><br />




<!--- IMAGE MODAL --------->

<div class="modal fade" id="my_modal" tabindex="-1" role="dialog" aria-labelledby="my_modalLabel">
<div class="col-md-4 text-center">
    <form action="/upload.php" method="post" enctype="multipart/form-data">        
        <div class="modal-dialog" role="dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                <h2>Select image to upload </h2><br>
                <label class="btn btn-file btn-primary">
                    Browse <input type="file" style="display: none;" name="fileToUpload" id="fileToUpload">
                </label>
                <label class="btn btn-default btn-submit">
                    Upload<input type="submit" style="display: none;" name="submit">
                </label>
                <input type="hidden" id="productId" name="product_id" />
                    
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
    </form>
</div>
</div>
<!------- END MODAL ----->





                       

@stop

@section('scripts')

<script>

    $('select[name=catalog_category_id]').change(function(){
        $('#filters-form').submit();
    });

    $('#content-form').submit(function(e){
        var ans = confirm('Save updates?');
        if(!ans){e.preventDefault();}
    });

    // Dynamically put the product id to the modal form on upload button click
    $(document).on('click', '.btn-upload', function(e) {
        var id = $(this).data('id');
        $('#productId').val(id);
    });

</script>

   <!-- Optional JavaScript --> 
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"  SameSite="None"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>





@stop

@section('styles')

<style>

select.catalog-category-setting{
    width: 200px;
}

#content-form input.catalog-order-setting{
    min-width: 50px;
    width: 50px;
}

</style>



@stop
