@extends('templates.products')

@section('content')

<table class="table table-stiped table-bordered">
    <thead>
        <tr>
            <th>Part</th>
            <th>Qty</th>
            <th>Price Per</th>
            <th>Price Qty</th>
        </tr>
    </thead>
    <tbody>
        @foreach($job_product->parts as $part)
        <tr>
            <td>{{ $part->name }}</td>
            <td>{{ $part->Qty }}</td>
            <td>{{ $part->ListPer }}</td>
            <td>{{ $part->ListQty }}</td>
        </tr>
        @endforeach
    </tbody>
</table>



<?php l($job_product->toArray()) ?>

@stop