@extends('templates.products')

@section('content')

<h1>Finished Ends Pricing</h1>

{{ Form::open() }}
{{ Form::submit('Save', ['class'=>'btn btn-primary']) }}
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Vendor</th>
            <th>Group</th>
            <th>Brand</th>
            <th>Style Type</th>
            <th>Name</th>
            <th>Finish Material Type</th>
            <th>Finish Material Sqft Cost</th>
            <th>Finish Material Edge Banding Ft Cost</th>
            <th>Wall End Cost</th>
            <th>Base End Cost</th>
            <th>Tall End Cost</th>
            <th>Finished Interior Cost</th>
            <th>Panel 1SSq Ft Cost</th>
            <th>Panel 2SSq Ft Cost</th>
            <th>Wall End List</th>
            <th>Base End List</th>
            <th>Tall End List</th>
            <th>Finished Interior List</th>
            <th>Panel 1SSq Ft List</th>
            <th>Panel 2SSq Ft List</th>
        </tr>
    </thead>
    <tbody>
        @foreach($styles as $style)
        <tr>
            <td>
                @if($style->vendor)
                    {{ $style->vendor->Name }}
                @endif
            </td>
            <td>
                @if($style->group)
                    {{ $style->group->Name }}
                @endif
            </td>
            <td>
                @if($style->brand)
                    {{ $style->brand->Name }}
                @endif
            </td>
            <td>
                {{ $style->StyleType }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Name]", $style->Name) }}
            </td>
            <td>
                {{ Form::select("styles[{$style->ID}][FinishMaterialType]", $finish_material_types, $style->FinishMaterialType) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FinishMaterialSqftCost]", $style->FinishMaterialSqftCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FinishMaterialEdgeBandingFtCost]", $style->FinishMaterialEdgeBandingFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][WallEndCost]", $style->WallEndCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][BaseEndCost]", $style->BaseEndCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][TallEndCost]", $style->TallEndCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FinishedInteriorCost]", $style->FinishedInteriorCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Panel1SSqFtCost]", $style->Panel1SSqFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Panel2SSqFtCost]", $style->Panel2SSqFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][WallEndList]", $style->WallEndList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][BaseEndList]", $style->BaseEndList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][TallEndList]", $style->TallEndList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FinishedInteriorList]", $style->FinishedInteriorList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Panel1SSqFtList]", $style->Panel1SSqFtList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Panel2SSqFtList]", $style->Panel2SSqFtList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}

@stop