<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">
<base href="https://cardinal.corymfg.com">

<?php 

?>
<link rel="stylesheet" href="https://cardinal.corymfg.com/css/dyod/pdf/style.css" type="text/css">
<title>Imperia Cabinet - Design Your Own Door</title>
</head>
<body>

 <div id="print-area">
        <div id="content">
			<div class="header">
            <h1>Design Your Own Door</h1>
			<h1>Style Basis: {{ $inside_profile->brand_name}}</h1>
			</div>
			
			<div class="inner_content">
				<?php 
				// $doorImageEncode = rawurlencode(basename($door->image_url));
				// $doorPath = dirname($door->image_url);
				// $doorImage = $doorPath ."/". $doorImageEncode; 
				// echo $doorImage . "<br>door path:" . $doorPath;
				// print_r($door); 
				
				?>
				<img src="https://cardinal.corymfg.com{{{ $door->image_url }}}" alt="Door Builder Image" />
				
			</div>
			
			<div class="inner_footer">
        
			<div class="footer-top">
				<div class="footer-slab-1">
					<h2>Inside Profile</h2>
					<img src="{{{ $inside_profile->image_url }}}" height="44"  alt="inside profile" />
					<p>Name: {{ $inside_profile->Name}}</p>
					<p>Code: {{ $inside_profile->Code }}</p>
				</div>
				<div class="footer-slab-2">
					<h2>Center Panel</h2>
					<img src="{{{ $center_panel->image_url }}}" height="44"  alt="center panel" />
					<p>Name: {{ $center_panel->Name}}</p>
					<p>Code: {{ $center_panel->Code }}</p>
				</div>
				<div class="footer-slab-3">
					<h2>Outside Profile</h2>
					<img src="{{{ $outside_profile->image_url }}}" height="44"   alt="outside profile" />
					<p>Name: {{ $outside_profile->Name}}</p>
					<p>Code: {{ $outside_profile->Code }}</p>
				</div>
				<div class="footer-slab-4">
					<h2>Stile / Rail</h2>
					<img src="{{{ $inside_profile->image_url }}}" height="44"   alt="stile rail" />
					<p>Name: {{ $stile_rail->VendorCode }}</p>
					<p>Code: {{ $stile_rail->Code }}</p>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="logo">
					<img alt="Cory" src="/css/dyod/pdf/images/logo.png" />
				</div>
				
				<div class="designer_details">
				<p>Designer: {{ $designer }}</p>
				<p>Dealer: {{ $dealer }}</p> 
				<p>Date: {{ $date }}</p>				
				</div>
				<div class="notes_details">
				<p id="notes-paragraph">Notes: {{ $notes }}</p>
				<p>Scale: Door Shown at 12"W x 15"H</p>
				</div>
			</div>
			
		</div>
		</div>
        <div class="external_footer">
			<p class="float-left">343 Manley Street | West Bridgewater, MA 02379	</p>
			<p class="float-right">T: 508.510.3699    F: 508.894.2915    corymfg.com</p>
        </div>
    </div>

</body>
</html>
