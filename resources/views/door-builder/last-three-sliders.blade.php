@include('partials.door-builder.slider', [
    'number'      => 2,
    'label'       => 'Center Panels',
    'input_name'  => 'center_panel_id',
    'options'     => $center_panels,
    'selected_id' => $center_panel_id
])

@include('partials.door-builder.slider', [
    'number'      => 3,
    'label'       => 'Outside Profiles',
    'input_name'  => 'outside_profile_id',
    'options'     => $outside_profiles,
    'selected_id' => $outside_profile_id
])

@include('partials.door-builder.slider', [
    'number'      => 4,
    'label'       => 'Stile and Rail Size',
    'input_name'  => 'stile_rail_id',
    'options'     => $stile_rails,
    'selected_id' => $stile_rail_id
])