<html>
<head>
    <style>
        td{
            padding: 25px;
        }
    </style>
    <link rel="stylesheet" href="/css/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <h1>Send Specs to Dealer</h1>
        <div class="row">
            <div class="col-md-3">
                <h3>Inside Profile</h3>
                <p><img src="{{ $inside_profile->image_url }}"></p>
                <p>Name: {{ $inside_profile->Name}}</p>
                <p>Brand: {{ $inside_profile->brand_name}}</p>
                <p>Code: {{ $inside_profile->Code }}</p>
            </div>
            <div class="col-md-3">
                <h3>Center Panel</h3>
                <p><img src="{{ $center_panel->image_url }}"></p>
                <p>Name: {{ $center_panel->Name}}</p>
                <p>Code: {{ $center_panel->Code }}</p>
            </div>
            <div class="col-md-3">
                <h3>Outside Profile</h3>
                <p><img src="{{ $outside_profile->image_url }}"></p>
                <p>Name: {{ $outside_profile->Name}}</p>
                <p>Code: {{ $outside_profile->Code }}</p>
            </div>
            <div class="col-md-3">
                <h3>Stile / Rail</h3>
                <p><img src="{{ $stile_rail->image_url }}"></p>
                <p>Name: {{ $stile_rail->VendorCode }}</p>
                <p>Code: {{ $stile_rail->Code }}</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <img src="{{ $door->image_url }}" style="width: 100%">
            </div>
            <div class="col-md-6">
                {{ Form::open() }}
                    {{ Form::hidden('inside_profile_id', $inside_profile->ID) }}
                    {{ Form::hidden('outside_profile_id', $outside_profile->ID) }}
                    {{ Form::hidden('center_panel_id', $center_panel->ID) }}
                    {{ Form::hidden('stile_rail_id', $stile_rail->ID) }}
                    {{ Form::hidden('builtfrom', 'purekitchen.com') }}
                    <div class="form-group <?php if($errors->has('first_name')): ?>has-error<?php endif; ?>">
                        <label for="first_name">First Name*</label>
                        {{ Form::text('first_name', null, ['class'=>"form-control"]) }}
                        {{ $errors->first('first_name', '<span class="text-danger">:message</span>') }}
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name*</label>
                        {{ Form::text('last_name', null, ['class'=>"form-control"]) }}
                        {{ $errors->first('last_name', '<span class="text-danger">:message</span>') }}
                    </div>
                    <div class="form-group">
                        <label for="email">Email*</label>
                        {{ Form::text('email', null, ['class'=>"form-control"]) }}
                        {{ $errors->first('email', '<span class="text-danger">:message</span>') }}
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone*</label>
                        {{ Form::text('phone', null, ['class'=>"form-control"]) }}
                        {{ $errors->first('phone', '<span class="text-danger">:message</span>') }}
                    </div>
                    <div class="form-group">
                        <?php //print_r($account_options); ?>
                        <label for="account_id">Dealer*</label>
                        {{ Form::select('account_id', $account_options, null, ['class'=>"form-control"]) }}
                        {{-- {{ Form::select('account_id', $account_options, "hello", ['class'=>"form-control"]) }} --}}
                        {{ $errors->first('account_id', '<span class="text-danger">:message</span>') }}
                    </div>
                    <div class="form-group">
                        <label for="notes">Notes (maximum {{ $notes_max_length }} characters):</label>
                        {{ Form::textarea('notes', null, ['class'=>"form-control", 'style'=>'height: 80px', 'maxlength'=>$notes_max_length]) }}
                        {{ $errors->first('notes', '<span class="text-danger">:message</span>') }}
                    </div>
                    {{ Form::submit('Submit', ['class'=>"btn btn-default"]) }}
                {{ Form::close() }}
            </div>
        </div>
    </div>
</body>
</html>
