<!DOCTYPE html>

<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="Imperia Cabinet - ">

    <link rel="stylesheet" href="/css/dyod/child-theme.css" type="text/css" >
    <link rel="stylesheet" href="/css/dyod/style.css" type="text/css">

    <title>{{config('app.name')}}  Design Your Own Door – Imperia Cabinet</title>
    <base href="https://cardinal.corymfg.com"> 
</head>

<body>

<form id="slider-form">
    <div class="hfeed site" id="page">

        <div class="wrapper" id="full-width-page-wrapper">
            <div class="container" id="content">
                <div class="row">
                    <div class="col-md-12 text-center">
                            <h1 class="title">Design Your Own Door</h1>
                    </div><!-- #primary -->
                </div><!-- .row end -->

                <div class="row">
                
                    <div class="col-md-12 col-lg-6" id="door-image" style="background-size: 446px auto; background-repeat: no-repeat;">
                        <img
                            id="doorStyImgInModal"
                            src="/images/dyod-overlay-v1.png" 
                            style="max-width: 450px"
                        >
						<p class="preview_text">* Preview drawing shown as 15" x 12" door</p>
                    </div>
                    <div class="col-md-12 col-lg-6 the-form-inline">
                        <div class="doorSpec">

                            @include('partials.door-builder.slider', [
                                'number'      => 1,
                                'label'       => 'Inside Door Profile (Door Style)',
                                'input_name'  => 'inside_profile_id',
                                'options'     => $inside_profiles,
                                'selected_id' => $inside_profile_id
                            ])

                            <div id="last-three-sliders">
                            @include('door-builder.last-three-sliders', [
                                'options'     => $center_panels,
                                'selected_id' => $center_panel_id,
                                'options'     => $outside_profiles,
                                'selected_id' => $outside_profile_id,
                                'options'     => $stile_rails,
                                'selected_id' => $stile_rail_id
                            ])
                            </div>
                            
                            <br><br>
							<div class="row text-center">
							<div class="col mb-3">
                            <a
                                class="btn dyod-button"
                                style="padding-top: 17px"
                                id="print-specs-button" 
                                href="#"
                                target="_BLANK"
                            >
                                <i class="fa fa-print" aria-hidden="true"></i>
                                Print Design Specs PDF
                            </a>
                            </div>
							</div>
							<div class="row text-center">
							<div class="col">
                            <a
                                class="btn dyod-button"
                                style="padding-top: 17px"
                                id="send-specs-to-dealer-button" 
                                href=""
                                target="_BLANK"
                            >
                                <i class="fa fa-envelope-open" aria-hidden="true"></i>
                                Send Specs to Local Dealer
                            </a>
							</div>
							</div>
                        
                        </div>
                    </div>
                </div><!-- .row end -->
            </div><!-- Container end -->
        </div><!-- Wrapper end -->
    </div>
</form>
    
<script src="/js/jquery-2.2.4.min.js" ></script>
<script src="/js/sly.min.js"></script>
<script src="/js/dyod.js"></script> 

</body>
</html>