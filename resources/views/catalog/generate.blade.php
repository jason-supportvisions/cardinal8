<html>
    <head>
        <base href="http://<?php echo $_SERVER['SERVER_NAME'];?>">
        <style>

            *{
                font-family: arial;
            }

            td, img {
                /*border: 1px solid red;*/
            }

            td{
                font-size: 12px;
                font-family: arial;
            }

            ul {
                margin-top: 0px;
            }

            .price-wrapper{
                text-align: right;
                width: 45px;
            }

            .item-code-wrapper{
                width: 133px;
            }

            .product-group{
                page-break-inside: avoid;
                border-bottom: 1px solid grey;
                margin-top: 8px;
            }

            .cabinet-image{
                width: 175px;
            }

            .pricing-table{
                border-collapse: collapse;
            }

            .pricing-table thead tr{
                background-color: #BBB;
            }

            .pricing-table tbody tr:nth-child(even) {
               background-color: #DDD;
            }
        </style>
    </head>
    <body>
        <?php
        //for HTML show a title and logo -- the PDF format gets this trhough an include header.blade.php
        if($format != "pdf"){ 
        ?>
        <table width="100%">
            <tr>
                <td align="left">
                    <?php

                        echo "<h1>" . $title . "</h1>";

                     ?> 
                </td>
                <td align="right">
                    <img style="width: 150px" src="/images/logos/catalog-logo.jpg" /> 
                </td>
            </tr>
        </table>
        <?php 
        } //if html format end
        ?>
        <?php 
        //-----------------------------------------------------
        //allow for Modifications to be shown
        //-----------------------------------------------------

        if($category->name == "Mods"){
        ?>
        
        @foreach($category->modificationItemTypes as $modification_item_type)
                @foreach($modification_item_type->ModificationItems as $modification_item)

                <?php //echo print_r($product_item); ?>


                    <div class="product-group">
                    <h3> {{ $modification_item->Code }}</h3>
                        {{ $modification_item->Description }}
                    <table width="100%">
                        <tr valign="top">
                            {{-- <td width="175px">
                                    
                            </td> --}}
                            <td width="320px">
                                <?php
                                //Notes
                                if(isset($modification_item->Note)){
                                    $note =  $modification_item->Note;
                                    $list = explode("\n", $note);
                                ?>

                                    <ul>
                                        
                                        <?php                                        
                                            // Output the items as list elements
                                            foreach ($list as $num => $item) {
                                                if($item != ""){
                                                    $item = nl2br($item);
                                                    //echo "<li>" . utf8_decode($item) . "</li>";
                                                    echo "<br>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $item);
                                                }
                                            }

                                        ?>

                                    </ul>
                                <?php
                                } //if note exists
                                ?>
                                </td>
                                <td valign="top" align="right">
                                    <table class="pricing-table">
                                        
                                        <tbody>

                                            <tr>
                                                <td class="item-code-wrapper">
                                                    {{ $modification_item->Code }}
                                                </td>
                                                    <td class="price-wrapper">
                                                        ${{ $modification_item->List }}
                                                    </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                    <br>

                        
                                </td>
                            </tr>
                        </table>
                    </div> 

                @endforeach
            @endforeach
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        <?php 
        //-----------------------------------------------------
        //allow for accessory ITEM (not group) catalog printing
        //-----------------------------------------------------

        }elseif($category->name == "Accessory"){
        ?>
        
        @foreach($category->accessoryGroups as $product_group)
                @foreach($product_group->items as $product_item)

                <?php //echo print_r($product_item); ?>


                    <div class="product-group">
                   <h3> {{ $product_item->Description }}</h3>
                   {{-- subclass: {{ $product_item->SubClass }}<br>
                   order: {{ $product_item->catalog_order }}<br> --}}
                    <table width="100%">
                        <tr valign="top">
                            <td width="175px">

                                    <?php 
                                    
                                // =================================
                                // IMAGE
                                // =================================

                                //accessory needs the item image and product needs a group image
                                    if($_GET["category_id"] == 11){
                                        $filePath = public_path() . '/images/accessories/' . $product_item->Image_File;                                       
                                        $address = '/images/accessories/' . $product_item->Image_File;
                                    }else{
                                        $filePath = public_path() . '/images/cabinets/' . $product_group->Image_File;
                                        $address = '/images/cabinets/' . $product_group->Image_File;
                                    }
                                
                                //does the image file exist
                                //show image or placeholder
                                    if (@file_exists($filePath) == true) {
                                        // $address = $address; 
                                        echo '<img class="cabinet-image" src="'. $address.'" class="img-responsive" id="prodImg">';
                                    }else{
                                        echo '<img class="cabinet-image" src="/images/coming_soon.png" class="img-responsive" id="prodImg" alt="my alt'.$address.'">';
                                        $address = ''; 

                                    }
                                // =================================     
                                

                                    ?>                                    
                                <?php 
                                //echo "<br>image_address: " . $product_item->image_address;
                                // echo "<br>address: " . $address;
                                ?>
                                {{-- {{ $product_group->GroupCode }} --}}
                            </td>
                            <td width="320px">
                                <ul>
                                <li>{{ $product_item->Note }}</li>

                                <?php
                                //Notes
                                
                                if(isset($product_item->Notes)){
                                    $note =  $product_item->Notes;
                                    $list = explode("\n", $note);
                                ?>

                                    
                                        
                                        <?php                                        
                                            // Output the items as list elements
                                            foreach ($list as $num => $item) {
                                                if($item != ""){
                                                    $item = nl2br($item);
                                                    //echo "<li>" . utf8_decode($item) . "</li>";
                                                    //echo "<li>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $item) . "</li>";
                                                }
                                            }

                                        ?>

                                    </ul>
                                <?php
                                } //if note exists
                                ?>
                                </td>
                                <td valign="top" align="right">
                                    <table class="pricing-table">
                                        <thead>
                                            <tr align="right">
                                                <th>&nbsp;</th>
                                                <th>A</th>
                                                <th>B</th>
                                                <th>C</th>
                                                <th>D</th>
                                                <th>E</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <tr>
                                                <td class="item-code-wrapper">
                                                    {{ $product_item->Code }}
                                                </td>
                                                @foreach($product_item->pricingOptions as $pricing_option)
                                                    <td class="price-wrapper">
                                                        ${{$pricing_option->List}}
                                                    </td>
                                                @endforeach
                                            </tr>

                                        </tbody>
                                    </table>
                                    <br>

                        
                                </td>
                            </tr>
                        </table>
                    </div> 

                @endforeach
            @endforeach

































            <?php
            //else print the regular catalog with only groups
            }else{
            /*
            ->orderBy('ClassBuild','desc') 
            ->orderBy('Category')            
            ->orderBy('Class')
            ->orderBy('SubClass')
            ->orderBy('catalog_order')
            ->orderBy('GroupCode')
            ->orderBy('Type')-> where('active', '=', 1);
            */
            ?>
                
                @foreach($category->productGroups as $product_group)
                
                <?php 
                    $subTitle =  $product_group->Type;
                    //print_r($product_group);
                ?>
                <div class="product-group">
           
                    
                   <h3> {{ $product_group->BookDescription }}</h3>
                   {{-- classbuild: {{ $product_group->ClassBuild }}<br>
                   category: {{ $product_group->Category }}<br>
                   class: {{ $product_group->Class }}<br>
                   subclass: {{ $product_group->SubClass }}<br>
                   order: {{ $product_group->catalog_order }}<br>
                   groupcode: {{ $product_group->GroupCode }}<br>
                    <small>{{ $product_group->GroupCode }}</small> --}}
                   {{-- <div class="alert" style="background-color: #bcd2f5; width: 300px; padding: 10px;">
                   Sorted in this order... <br> --}}
                   <?php 
                    if($product_group->ClassBuild == "Base"){
                        $ClassBuildOut = "Regular" . "->" . $product_group->ClassGroup;
                    }else{
                        $ClassBuildOut = $product_group->ClassBuild . "->" . $product_group->ClassGroup;
                    }
                   ?>
                   {{-- &nbsp;&nbsp;&nbsp;<b>1. ClassBuild:</b> {{ $ClassBuildOut }} <br>
                   &nbsp;&nbsp;&nbsp;<b>2. Category:</b> {{ $product_group->Category }} <br>
                   &nbsp;&nbsp;&nbsp;<b>3. Class:</b> {{ $product_group->Class }} <br>
                   &nbsp;&nbsp;&nbsp;<b>4. Sort Letter:</b> ({{ $product_group->SubClass }})<br> 
                   &nbsp;&nbsp;&nbsp;<b>5. Catalog Order:</b> {{ $product_group->catalog_order }} <br>  
                   &nbsp;&nbsp;&nbsp;<b>7. Type:</b> {{ $product_group->Type }}<br>
                   </div><br><br> --}}
                    <table width="100%">
                        <tr valign="top">
                            <td width="175px">
                            <?php 
                                // =================================
                                // IMAGE
                                // =================================

                                //accessory needs the item image and product needs a group image
                                    if($_GET["category_id"] == 11){
                                        $filePath = public_path() . '/images/accessories/' . $product_group->Image_File;                                       
                                        $address = '/images/accessories/' . $product_group->Image_File;
                                    }else{
                                        $filePath = public_path() . '/images/cabinets/' . $product_group->Image_File;
                                        $address = '/images/cabinets/' . $product_group->Image_File;
                                    }
                                
                                //does the image file exist
                                //show image or placeholder
                                    if (@file_exists($filePath) == true) {
                                        // $address = $address; 
                                        echo '<img class="cabinet-image" src="'. $address.'" class="img-responsive" id="prodImg">';
                                    }else{
                                        echo '<img class="cabinet-image" src="/images/coming_soon.png" class="img-responsive" id="prodImg" alt="my alt'.$address.'">';
                                        $address = ''; 

                                    }
                                // =================================     
                                

                            ?>
                            <?php 
                                /*echo "<br>image_address: " . $product_group->image_address . 
                                " <br>type: " . $product_group->Type . 
                                " <br>group: " . $product_group->GroupCode . 
                                " <br>cat order: " . $product_group->catalog_order;  */
                               ?>
                               
                            </td>
                            <td width="320px">
                            <br /><br />
                                <ul>
                                        <?php 
                                            
                                            $note = $product_group->Notes;

                                            // Get an array of items from lines
                                            $list = explode("\n", $note);

                                            // Output the items as list elements
                                            foreach ($list as $num => $item) {
                                                if($item != ""){
                                                    $item = nl2br($item);
                                                    //echo "<li>" . utf8_decode($item) . "</li>";
                                                    echo "<li>" . iconv("UTF-8", "ISO-8859-1//TRANSLIT", $item) . "</li>";
                                                }
                                            }

                                    ?>
                                </ul>
                            </td>
                            <td valign="top" align="right">
                                <table class="pricing-table">
                                    <thead>
                                        <tr align="right">
                                            <th>&nbsp;</th>
                                            <th>A</th>
                                            <th>B</th>
                                            <th>C</th>
                                            <th>D</th>
                                            <th>E</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($product_group->items as $item)
                                        <tr>
                                            <td class="item-code-wrapper">
                                                {{ $item->Code }}
                                            </td>
                                            @foreach($item->pricingOptions as $pricing_option)
                                                <td class="price-wrapper">
                                                    ${{ $pricing_option->List }}
                                                </td>
                                            @endforeach
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <br>
                                Add to pricing above:

                                @if($product_group->DoorCount)
                                    {{ $product_group->DoorCount }} 
                                    @if($product_group->DoorCount > 1)
                                        Doors
                                    @else
                                        Door
                                    @endif
                                @endif

                                @if($product_group->TallDoorCount)
                                    {{ $product_group->TallDoorCount }}
                                    @if($product_group->DoorCount > 1)
                                        Tall Doors
                                    @else
                                        Tall Door
                                    @endif
                                @endif

                                @if($product_group->FullTallDoorCount)
                                    {{ $product_group->FullTallDoorCount }}
                                    @if($product_group->FullTallDoorCount > 1)
                                        Full Tall Doors
                                    @else
                                        Full Tall Door
                                    @endif
                                @endif

                                @if($product_group->DrawerCount)
                                    {{ $product_group->DrawerCount }}
                                    @if($product_group->DrawerCount > 1)
                                        Drawers
                                    @else
                                        Drawer
                                    @endif
                                @endif

                                @if($product_group->LargeDrawerCount)
                                    {{ $product_group->LargeDrawerCount }}
                                    @if($product_group->LargeDrawerCount > 1)
                                        Large Drawers
                                    @else
                                        Large Drawer
                                    @endif 
                                @endif
                            </td>
                        </tr>
                    </table>
                </div> 
            @endforeach    
            <?php
            } // if accessory end
            ?>
            
        </body>
</html>