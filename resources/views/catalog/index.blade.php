@extends('templates.default')

@section('content')
    {{ Form::open(['method'=>'GET', 'url'=>'catalog/generate','target' => '_blank']) }}
    <h3>
        Select Category to Include 
        <!-- <a href="/backend/catalog/categories">(Edit Categories)</a> -->
    </h3>

    @foreach($categories as $category)
        <?php 
        //disable Mods and Accessories in catalog for now
       // if($category->name == "Mods"){
        //    $disabled = "disabled";
        //}else{
            $disabled = "";
        //}
        ?>
        <input type="radio" name="category_id" value="{{ $category->id }}" <?php echo $disabled; ?>>
        {{ $category->name }}
        <br>
    @endforeach
        <br>
        <input type="radio" name="category_id" value="101">
        Transitional Doors (pdf)
        <br>
        <input type="radio" name="category_id" value="102">
        Traditional Doors (pdf)
        <br>
        <input type="radio" name="category_id" value="103">
        Contemporary Doors (pdf)
        <br>
    
    <br>
    <button type="submit" name="format" value="html">HTML Preview</button>
    <br><br>
    <button type="submit" name="format" value="pdf">PDF Download</button>

    <div class="modal fade" id="ajax-loader" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <!--<div class="loader"></div>-->
                    <div class="saw">
                        <div clas="loader-txt">
                            <b>LOADING PDF... </b><br><br>
                        </div>
                        <img src="/images/loading/saw.gif" class="spin"/>      
                        <br><br>              
                        <div clas="loader-txt">
                            Please allow up to <strong>one minute</strong> to fully create the catalog
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{ Form::close() }}
@stop

@section('scripts')
    <script>
        var setCookie = function(name, value, expiracy) {
            var exdate = new Date();
            exdate.setTime(exdate.getTime() + expiracy * 1000);
            var c_value = escape(value) + ((expiracy == null) ? "" : "; expires=" + exdate.toUTCString());
            document.cookie = name + "=" + c_value + '; path=/';
        };

        var getCookie = function(name) {
            var i, x, y, ARRcookies = document.cookie.split(";");
            for (i = 0; i < ARRcookies.length; i++) {
                x = ARRcookies[i].substr(0, ARRcookies[i].indexOf("="));
                y = ARRcookies[i].substr(ARRcookies[i].indexOf("=") + 1);
                x = x.replace(/^\s+|\s+$/g, "");
                if (x == name) {
                return y ? decodeURI(unescape(y.replace(/\+/g, ' '))) : y;
                }
            }
        };

        $('form').submit(function(e){
            var checked = $("input[type=radio]:checked");
            if(checked.length == 0){
                e.preventDefault();
                return;
            }
            var form =  $(this);
            if (document.activeElement.value === 'pdf') {
                e.preventDefault();
                setCookie('downloadStarted', 0, 100);

                // show the loader
                $("#ajax-loader").modal({
                    backdrop: "static",
                    keyboard: false,
                    show: true
                });
                
                // create an iframe for the download on the background
                var url = form.attr('action') + '?' + form.serialize() + '&format=pdf';
                var iframe = document.createElement('iframe');
                iframe.style.display = 'none';
                document.body.appendChild(iframe);
                iframe.src = url;
                
                // wait for the download cookie from the backend
                var checkDownloadCookie = function() {
                    if (getCookie("downloadStarted") == 1) {
                        setCookie("downloadStarted", "false", 100);
                        $('#ajax-loader').modal('hide');
                    } else {
                        setTimeout(checkDownloadCookie, 1000);
                    }
                };
                checkDownloadCookie();
            }
    
        });
    </script>
@stop

@section('styles')
    <style>
    #ajax-loader .modal-dialog {
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            margin: 0;
    }

    .loader {
        position: relative;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #000;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }
    .saw {
        position: relative;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        /*
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #000;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite; */
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }


    .modal-content {
        border-radius: 0px;
        box-shadow: 0 0 20px 8px rgba(0, 0, 0, 0.3);
    }

    .modal-body {
        min-height: 200px !important;
    }

    .modal-backdrop.show {
        opacity: 0.75;
    }

    .spin {
        -webkit-animation:spin 8s linear infinite;
        -moz-animation:spin 8s linear infinite;
        animation:spin 8s linear infinite;
    }

    transform:rotate(360deg);
        @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
        @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
        @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }

    </style>
@stop