<!doctype html>
<html>
    <head>
        <meta charset='UTF-8'>
        <base href="{{ $http_base_path }}">
        <style>
            *{
                font-family: arial;
            }
        </style>
    </head>
    <body>
        <table width="100%">
            <tr>
                <td align="left">
                   <h2><?php echo $title; ?></h2>
                </td>
                <td align="right">
                    <?php $snappyServer = 'http:///'.$_SERVER['SERVER_NAME'].'/'; ?>
                    <img style="width: 150px" src="<?php echo $snappyServer; ?>images/logos/catalog-logo.jpg" /> 
                </td>
            </tr>
        </table>
    </body>
</html>