@extends('templates.default')

@section('content')
<style>
table { 
    border-spacing: 10px;
    border-collapse: separate;
}
td { 
    padding: 10px;
	white-space: nowrap;
}
</style>

<?php 


function getORDCode($product){
if (!isset($product->accessory_group->GroupCode)){
    return "<b>Missing GROUP relationship</b>";

}else{
    // return($product->accessory_group->GroupCode);

// }
		//vars	
		$debug = "";
		$itemStr    	= "accessory";
		

			
		// if($itemStr == ""){
		// 	$itemStr    	= "product";
		// 	$concatCode 	= $product->product_item->product_group->GroupCode; 
		// 	$customPrefix 	= $product->product_item->product_group->ORDPrefix;
		// 	$ordType		= $product->product_item->product_group->ORDCodeType;
		// 	$ordDimensions 	= $product->product_item->product_group->ORDDimensions;
		// 	$customSuffix 	= $product->product_item->product_group->ORDSuffix;
		// }

		// if($itemStr == "product"){
		// 	$itemStr    	= "product";
		// 	$concatCode 	= $product->product_item->product_group->GroupCode;  
		// 	$customPrefix 	= $product->product_item->product_group->ORDPrefix;
		// 	$ordType		= $product->product_item->product_group->ORDCodeType;
		// 	$ordDimensions 	= $product->product_item->product_group->ORDDimensions;
		// 	$customSuffix 	= $product->product_item->product_group->ORDSuffix;
		// }

		if($itemStr == "accessory"){
			// dd($product->accessory_item->GroupCode);
			$concatCode 	= $product->GroupCode; 
			$customPrefix 	= $product->accessory_group->ORDPrefix;
			$ordType		= $product->accessory_group->ORDCodeType;
			$ordDimensions 	= $product->accessory_group->ORDDimensions;
			$customSuffix 	= $product->accessory_group->ORDSuffix;
		}


		//============================================================

			

		//allow our statements below to be more generic and not conditionally product or accessory	
			$Table              = $itemStr;
			$prodType           = $itemStr;
			$itemType           = $itemStr . "_item";
			$groupType			= $itemStr . "_group";
			$pricingType		= $itemStr . "_pricing";
			$noteType           = $groupType."_notes";
			$groupTable			= $groupType . "s"; // product_group becomes product_groups
			$itemTable 			= $itemType . "s";  //add s to end of object name to use as table name - product_item is 
													// the object call to product_items (see the s) table na
				


			$Width 			= $product->Width; 
			$Height 		= $product->Height; 
			$Depth 			= $product->Depth;
			$Code 			= $product->Code;
			

					//show ITEM or GROUP
			/*
				Flags: 	
					customPrefix x
					prodType (item, group) x
					prodDimensions (enable, disable)
					customSuffix x
			 */
			
			//use item instead of group code
			if($ordType == 0){
				$concatCode 		= $Code;
				$groupCode 		 	= $concatCode;
				$concatGroupCode 	= $groupCode; 
				$needDash 		 	= '-';			
			}else{
				$groupCode 		 	= $concatCode;
				$concatGroupCode 	= $groupCode; 
				$needDash 		 	= '-';	
			}

			//should we append the dimensions?
			if($ordDimensions == 1){
				$showDimensions 	= "Yes";		
			}else{
				$showDimensions 	= NULL;		
			}
			// echo "showDim: $showDimensions";
	
	
			
			$debug 			.=  "</b><br>---------------------------<br>getORDCode('" . $concatCode . "')<br><br>"; 
			// $debug 			.=  "<br> pageType = " . $pageType;
			$debug 			.=  "<br> Width = " . $Width;
			$debug 			.=  "<br> Height = " . $Height;
			$debug 			.=  "<br> Depth = " . $Depth;
			// $concatCode 	= $product->GroupCode;
			
	// 		//Code
 		/*

			[type of cabinet initials]  -  [# of doors]  -  [attribute]

			==========================================
			items
			==========================================

				example item: 		Tall Storage Cabinet - 1 Full Tall Door - Type A - Fixed Shelf 54" Off Floor - 
				example code: 		TS1284-B-FD
				example ord: 		TS12x84x23.875-B-FD

				example item: 		Pie Cut Corner Wall Cabinet - 2 Doors - Bi-fold 
				example code: 		PCW2424-BF
				example ord: 		PCW24x24x24-BF
				
				example item: 		Vanity Drawer Base Suspended Cabinet - 1 Drawer - 
				example code: 		VDBS214.5
				example ord: 		VDBS214.5x21.125-5

				example item: 		Dartmouth Sink Base Cabinet - 2 Full Doors - 
				example code: 		D-SB21-FD(2D)
				example ord: 		D-SB2134.5x23.875-FD 

				example item: 		Tall Drawer Cabinet - 1 Tall Door/1 Drawer/2 Large Drawers - 
				example code: 		TD1884-3D
	 			example ord: 		TD18x84x23.875-3D 

				example item: 		Spectra Blind Corner Base Cabinet - 1 Full Door - Left - Magic Corner 1 - 
				example code: 		S-BCB39-L-MC1-FD
				example ord: 		S-BCB39x34.5x23.875-L-MC1-FD

			==========================================
			accessories
			==========================================				
				
				example item: 		Spectra C-Shape Internal Corner Oil Rubbed Bronze - 
				example code: 		S-CIC-ORB
				example ord: 		SPECTRA_CHANNEL
				
				example item: 		White Melamine Panel - 
				example code: 		WHT-MEL-CARB2
				example ord: 		BOXMATL48x96x0.75
				
				example item: 		Touchup Kit - 
				example code: 		TOUCHUP
				example ord: 		NOTE
				
				example item: 		Spectra J-Shape Closed End Cap White (Pair) - 
				example code: 		S-JCEC-W
				example ord: 		SPECTRA_CHANNEL

			==============================================

			*/

	// 		//look at the item code and group code. Compare all the characters up to '-' in item and '__' in group.
			$codeDashLocation = strcspn($concatCode,"-",0); //echo "<br>dash location: " . $codeDashLocation; //find dash position
			$codeUnderscoreLocation = strcspn($groupCode,"__",0); //echo "<br>underscore location: " . $codeUnderscoreLocation; //find _ position

			    $debug .= "<table border='1' style='padding:6px'>";
			    $debug .= "<tr><td style='padding:6px'><b>Item Code </b></td><td style='padding:6px'>" . $product->Code . "</td></tr>";
				$debug .= "<tr><td style='padding:6px'><b>Description </b></td><td style='padding:6px'>" . $product->Description . "</td></tr>";
			    $debug .= "<tr><td style='padding:6px'><b>Group Code</b></td><td style='padding:6px'>" . $product->GroupCode . "</td></tr>";
			    $debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
			    // $debug .= "</table>";

				$leftParenLocation = strcspn($concatGroupCode,"(",0); //find the )
				$rightParenLocation = strcspn($concatGroupCode,")",0);  //find the (
				$underscoreLocation = strcspn($concatGroupCode,"__",0); //find the __ 

			    $debug .= "<tr><td style='padding:6px'><b>'(' location </b></td><td style='padding:6px'>" . $leftParenLocation . "th</td></tr>";
			    $debug .= "<tr><td style='padding:6px'><b>')' location </b></td><td style='padding:6px'>" . $rightParenLocation . "th</td></tr>";
			    $debug .= "<tr><td style='padding:6px'><b>'_' location </b></td><td style='padding:6px'>" . $underscoreLocation . "th</td></tr>";
			    $debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";

				$plusLocation = strcspn($concatGroupCode,"+",0); //find the +
				$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);//remove the '+' and anything that follows

			    $debug .= "<tr><td style='padding:6px'><b>'+' location </b></td><td style='padding:6px'>" . $plusLocation . "th</td></tr>";

				$parenthesisData = substr($concatGroupCode, $leftParenLocation);
				$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);//remove the (D2) and anything after that
				$initDimension = substr($concatGroupCode, ($underscoreLocation+2), 2); //find the initial dimension that follows the __ in 

			    $debug .= "<tr><td style='padding:6px'><b>remove '$parenthesisData' </b></td><td style='padding:6px'>" . $groupCode . "</td></tr>";
			    $debug .= "<tr><td style='padding:6px'><b>removed result </b></td><td style='padding:6px'>" . $concatGroupCode . "</td></tr>";
			    $debug .= "<tr><td style='padding:6px'><b>init dimension </b></td><td style='padding:6px'> '" . $initDimension . "' </td></tr>";

				$initDimensionNumber = is_numeric($initDimension); //remove the redundant dimension if it's present
				$initDash = strpos($initDimension, "-");//make sure it doesn't start with a dash
				$initDashNumber = is_numeric($initDash); //remove the redundant dimension if it's present

				if($initDimensionNumber == true && $initDashNumber == false){					
					    $debug .= "<tr><td style='padding:6px' colspan='2'>'$initDimension' <b> is</b> a number, so we remove it.</td></tr>";
					$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);    
				}else{
					    $debug .= "<tr><td style='padding:6px' colspan='2'>initial dimension '$initDimension' is <b>NOT</b> a number, so we keep it.</td></tr>"; 
					//is the above dimension really numbers? 
				}	
			
			    $debug .= "<tr><td style='padding:6px'><b>CODE </b></td><td style='padding:6px'>$concatGroupCode</td></tr>";
			    $debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'></td></tr>";
			
				$ORDCodeDimensions = round($Width, 3) . "x" . round($Height, 3)  . "x" . round($Depth, 3) . $needDash; 

			    $debug .= "<tr><td style='padding:6px'><b>Dimensions</b></td><td style='padding:6px'>$ORDCodeDimensions</td></tr>";

				if(stristr($ORDCodeDimensions,"0x0x0")){ //set var to null if the dimension string reads '0x0x0';
					$ORDCodeDimensions = "";	
					    $debug .= "<tr><td style='padding:6px' colspan='2'>found '0x0x0' and zero it out</td></tr>"; //is the above dimension really numbers? 
				}			

			    $debug .= "<tr><td style='padding:6px'></td><td style='padding:6px'>$ORDCodeDimensions</td></tr>";

				//Show dimensions?
				if($showDimensions 	== "Yes"){

					//look for __ and put dimensions if not append dimensions
					if(strpos($concatGroupCode, '__')){
						$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode); 
					}else{
						$ORDCode = $concatGroupCode . $ORDCodeDimensions; 
					}
					$debug .= "<tr><td style='padding:6px'>str_replace('__', $ORDCodeDimensions, $concatGroupCode);</td><td style='padding:6px'>$ORDCodeDimensions</td></tr>";
				
				//Hide Dimensions
				}else{
					// $ORDCode = str_replace("__", "", $concatGroupCode);   
					$ORDCode = ($concatGroupCode);    
				}
				
				// handle any double dash weirdness                         
				$ORDCode = str_replace("--","-",$ORDCode);      

			    $debug .= "<tr><td style='padding:6px'><b>replace '--'</b></td><td style='padding:6px'>$ORDCode</td></tr>";

				//remove any trailing spaces
				$ORDCode = rtrim($ORDCode, "-"); 	

			    $debug .= "<tr><td style='padding:6px'><b>rtrim(code,'-')</b></td><td style='padding:6px'>$ORDCode</td></tr>";
			    $debug .= "</table>"; 
			
			    $debug .= "<br><h2>Final Output: <br /><b><font color=blue>" . $ORDCode . "</font></b></h2><br>";
			//  echo  $debug;
			// dd('functions.php ' . $ORDCode);


			//add both suffix and prefix to output
			if($customPrefix != ""){
				$ORDCode = $customPrefix . $ORDCode;
			}
			if($customSuffix != ""){
				$ORDCode = $ORDCode . $customSuffix;
			}
            // echo $debug;
			return $ORDCode;


	}


}

?>


























{{ Form::open(['method'=>'GET', 'url'=>'/backend/catalog/ord','target' => '_blank']) }}
    <h3>
        Verify Accessory SKU output (ORD)
        <!-- <a href="/backend/catalog/categories">(Edit Categories)</a> -->
    </h3>

    <table width="80%" border="1" cellpadding="50" cellspacing="20" style="padding:15px; margin:15px;">
		<tr style="background:#900C3F;color:white;">
			<td><b>Group, (id)</b></td>			
            <td><B>Item</b></td>
			<td nowrap><b>Show (Code, Dim)</b></td>
			<td><b>WxHxD</b></td>
            <td><b>SKU#</b></td>
		</tr>
		<tr>
			<td colspan="5"><br><br></td>
		</tr>
	<?php 

		//get all the categories into an array of objects
		$accessory_groups = $category->accessory_groups;
	
    ?>
	@foreach($accessory_groups as $accessory_group)
		<?php

			//====================================
			//get data and convert to String
			//====================================

				//Dimensions
				if($accessory_group->ORDDimensions == 1){
					$showDim = "yes"; 
				}else{
					$showDim = "no";
				}

				//Group/Item
				if($accessory_group->ORDCodeType > 0){
					$showType = "group"; 
				}else{
					$showType = "item";
				}
				
				//Suffix
				if($accessory_group->ORDSuffix != ""){
					$showSuffix = $accessory_group->ORDSuffix; 
				}else{
					$showSuffix = "no";
				}
				
				//Prefix
				if($accessory_group->ORDPrefix != ""){
					$showPrefix = $accessory_group->ORDPrefix; 
				}else{
					$showPrefix = "no";
				}

				//round widthxheightxdepth
				$groupW = number_format((float)$accessory_group->Width, 2, '.', '');
				$groupH = number_format((float)$accessory_group->Height, 2, '.', '');
				$groupD = number_format((float)$accessory_group->Depth, 2, '.', '');
		?>
        <tr style="background:#85C1E9  ;color:black;">
			
			<td style=""> <span style="">{{ $accessory_group->GroupCode }}, ({{ $accessory_group->ID }})</span></td>
			<td style=""></td>
			<td style="" nowrap>{{ $showType }}, {{ $showDim }} </td>
			<td nowrap> {{ $groupW }} x {{$groupH }} x {{$groupD}}</td> 
			<td> SKU Output</td>
		</tr>
        @foreach($accessory_group->items as $accessory_item)
        <?php
            if($accessory_item->Code){

				//round widthxheightxdepth
				$itemW = number_format((float)$accessory_item->Width, 2, '.', '');
				$itemH = number_format((float)$accessory_item->Height, 2, '.', '');
				$itemD = number_format((float)$accessory_item->Depth, 2, '.', '');
        ?>
        <tr>
			<td valign="top"> </td>
		<td valign="top"> <a href="/quotes_orders/40450/products/create?product_item_id={{ $accessory_item->ID }}&productType=accessory" target="_blank">{{ $accessory_item->Code }}</a></td>
			<td valign="top">  </td>
			<td valign="top" nowrap> {{$itemW}} x {{$itemH}} x {{$itemD}}</td> 
			<td valign="top"> {{ getORDCode($accessory_item) }} </td>
        </tr>
        <?php
            }else{
        ?>
        <tr>
            <td> no items</td>
            <td> no items </td>
			<td> no items </td>
			<td> no items </td>
        </tr>
        <?php
            }
        ?>
        @endforeach
        </tr>
        <tr>
            <td colspan="5"> &nbsp;</td>
        </tr>
    @endforeach
    </table>
    <br>
    <button type="submit" name="format" value="html">HTML Preview</button>
    <br><br>
    <button type="submit" name="format" value="pdf">PDF Download</button>



    {{ Form::close() }}
@stop



@section('styles')
    <style>
    #ajax-loader .modal-dialog {
            position: fixed;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            margin: 0;
    }

    .loader {
        position: relative;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #000;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite;
    }
    .saw {
        position: relative;
        text-align: center;
        margin: 15px auto 35px auto;
        z-index: 9999;
        display: block;
        /*
        width: 80px;
        height: 80px;
        border: 10px solid rgba(0, 0, 0, .3);
        border-radius: 50%;
        border-top-color: #000;
        animation: spin 1s ease-in-out infinite;
        -webkit-animation: spin 1s ease-in-out infinite; */
    }

    @keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    @-webkit-keyframes spin {
        to {
            -webkit-transform: rotate(360deg);
        }
    }


    .modal-content {
        border-radius: 0px;
        box-shadow: 0 0 20px 8px rgba(0, 0, 0, 0.3);
    }

    .modal-body {
        min-height: 200px !important;
    }

    .modal-backdrop.show {
        opacity: 0.75;
    }



    .spin {
        -webkit-animation:spin 8s linear infinite;
        -moz-animation:spin 8s linear infinite;
        animation:spin 8s linear infinite;
    }

    transform:rotate(360deg);
        @-moz-keyframes spin { 100% { -moz-transform: rotate(360deg); } }
        @-webkit-keyframes spin { 100% { -webkit-transform: rotate(360deg); } }
        @keyframes spin { 100% { -webkit-transform: rotate(360deg); transform:rotate(360deg); } }

    </style>
@stop