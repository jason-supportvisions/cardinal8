@extends('templates.default')

@section('content')
    {{ Form::open(['id'=>'catalog-category-form']) }}

    <h3>Catalog Categories</h3>
    {{ Form::submit('Save') }}

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Order</th>
                <th>Name</th>
                <th>Page Number Prefix</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($categories as $category)
            <tr>
                <td>
                    {{ Form::number("categories[{$category->id}][order]", $category->order, ['class'=>'order']) }}
                </td>
                <td>
                    {{ Form::text("categories[{$category->id}][name]", $category->name, ['class'=>'name']) }}
                </td>
                <td>
                    {{ Form::text("categories[{$category->id}][page_number_prefix]", $category->page_number_prefix, ['class'=>'page_number_prefix']) }}
                </td>
                <td>
                    <a
                        href="/backend/product-images?catalog_category_id={{ $category->id }}"
                    >
                        Edit Content
                    </a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {{ Form::close() }}
@stop

@section('styles')

<style>

input.name, input.page-number-prefix{
    width: 300px;
}

input.order{
    width: 50px;
}

</style>

@stop

@section('scripts')
    <script>
        $('#catalog-category-form').submit(function(e){
            var ans = confirm('Save updates?');
            if(!ans){e.preventDefault();}
        });
    </script>
@stop