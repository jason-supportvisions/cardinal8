@extends('templates.products')

@section('content')

<h1>Fronts Pricing</h1>

{{ Form::open() }}
{{ Form::submit('Save', ['class'=>'btn btn-primary']) }}
<br>
<table class="table table-striped">
    <thead>
        <tr>
            <th>Vendor</th>
            <th>Group</th>
            <th>Brand</th>
            <th>Style Type</th>
            <th>Name</th>
            <th>Multiplier</th>
            <th>Margin</th>
            <th>Per Cost</th>
            <th>Overlay Per Cost</th>
            <th>Sq Ft Cost</th>
            <th>Edge Banding Ft Cost</th>
            <th>Door Cost</th>
            <th>Tall Door Cost</th>
            <th>Full Tall Door Cost</th>
            <th>Drawer Cost</th>
            <th>Large Drawer Cost</th>
            <th>Overlay Cost</th>
            <th>Door Sq Ft Cost</th>
            <th>Drawer Sqft Cost</th>
            <th>Door List</th>
            <th>Tall Door List</th>
            <th>Full Tall Door List</th>
            <th>Drawer List</th>
            <th>Large Drawer List</th>
            <th>Overlay List</th>
            <th>Door Sq Ft List</th>
            <th>Drawer Sq Ft List</th>
        </tr>
    </thead>
    <tbody>
        @foreach($styles as $style)
        <tr>
            <td>
                @if($style->vendor)
                    {{ $style->vendor->Name }}
                @endif
            </td>
            <td>
                @if($style->group)
                    {{ $style->group->Name }}
                @endif
            </td>
            <td>
                @if($style->brand)
                    {{ $style->brand->Name }}
                @endif
            </td>
            <td>
                {{ $style->StyleType }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Name]", $style->Name) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Multiplier]", $style->Multiplier, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][Margin]", $style->Margin, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][PerCost]", $style->PerCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][OverlayPerCost]", $style->OverlayPerCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][SqFtCost]", $style->SqFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][EdgeBandingFtCost]", $style->EdgeBandingFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DoorCost]", $style->DoorCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][TallDoorCost]", $style->TallDoorCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FullTallDoorCost]", $style->FullTallDoorCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DrawerCost]", $style->DrawerCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][LargeDrawerCost]", $style->LargeDrawerCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][OverlayCost]", $style->OverlayCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DoorSqFtCost]", $style->DoorSqFtCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DrawerSqftCost]", $style->DrawerSqftCost, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DoorList]", $style->DoorList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][TallDoorList]", $style->TallDoorList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][FullTallDoorList]", $style->FullTallDoorList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DrawerList]", $style->DrawerList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][LargeDrawerList]", $style->LargeDrawerList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][OverlayList]", $style->OverlayList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DoorSqFtList]", $style->DoorSqFtList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
            <td>
                {{ Form::text("styles[{$style->ID}][DrawerSqFtList]", $style->DrawerSqFtList, ['style'=>'min-width: 80px; width: 80px']) }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
{{ Form::close() }}

@stop