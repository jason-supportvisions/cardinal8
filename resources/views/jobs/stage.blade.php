@php
$Title =  " | Jobs";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 1;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$pageid = "Jobs";

$jobPrint = '';
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');
@endphp
@extends('templates.theme')
@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-UX">
            <div class="panel-heading"><span class="pull-right"></span>Jobs</div>
            <div class="panel-body">
                <table id="table" class="table table-striped table-responsive table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>Account</th>
                            <th class="text-center">Job</th>
                            <th class="text-center">Product Line</th>
                            <th class="text-center">Stage</th>
                            <th class="text-center">Type</th>
                            <th>Name</th>
                            <th class="text-center">Date</th>
                            <th>Selection</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr class="">
                            <td class=" Account text-left" style="width: 15%;">{{$row->AccountName}}</td>
                            <td class="Job text-center" style="width: 10%;">
                                <h4><a data-toggle="modal" class="editmodal" href="" id="modellink" ModalJobID="{{$row->JobID}}">{{$row->JobID}}</a></h4>
                            </td>
                            <td class=" ProductLine text-center" style="width: 10%;">@php echo html_entity_decode($row->Brand); @endphp</td>
                            <td class=" Stage text-center" style="width: 10%;">@php echo html_entity_decode($row->Stage); @endphp</td>
                            <td class=" Type text-center" style="width: 10%;">{{$row->Type}}</td>
                            <td class=" Name text-left" style="width: 10%;">{{$row->Reference}}</td>
                            <td class=" Date text-center" style="width: 10%;">@php echo html_entity_decode($row->ModifiedDate); @endphp</td>
                            <td class=" Buttons text-right" style="width: 15%;">
                                @if($row->Selection == 1)
                                <span class="btn-group">
                                    <a class="btn btn-sm btn-info editmodal" data-toggle="modal" href="" id="modellink" ModalJobID="{{$row->JobID}}"><i class="glyphicon glyphicon-search"></i></a>
                                    <a class="btn btn-sm btn-warning" href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'edit']) }}"><i class="glyphicon glyphicon-edit"></i></a>
                                    <a data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-primary" aria-haspopup="true">Options <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="disabled"><a href="#"><i class="glyphicon glyphicon-ok"></i> Confirm</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyhead']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Header)</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyfull']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Full Job)</a></li>
                                        <li class=""><a href="{{ route('post.update',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID,'active'=>0]) }}"><i class="glyphicon glyphicon-remove"></i> Cancel</a></li>
                                    </ul>
                                </span>
                                @elseif($row->Selection == 2)
                                <span class="btn-group">
                                    <a class="btn btn-sm btn-info editmodal" data-toggle="modal" href="#myModal-',job.JobID,'" id="modellink" ModalJobID="{{$row->JobID}}"><i class="glyphicon glyphicon-search"></i></a>
                                    <a class="btn btn-sm btn-success" href="/quotes_orders/',job.JobID,'/pdf"> <i class="glyphicon glyphicon-print"></i></a>
                                    <a data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-primary" aria-haspopup="true">Options <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="disabled"><a href="#"><i class="glyphicon glyphicon-ok"></i> Confirm</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyhead']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Header)</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyfull']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Full Job)</a></li>
                                        <li class=""><a href="{{ route('post.update',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID,'active'=>0]) }}"><i class="glyphicon glyphicon-remove"></i> Cancel</a></li>
                                    </ul>
                                </span>
                                @elseif($row->Selection == 3)
                                <span class="btn-group">
                                    <a class="btn btn-sm btn-info editmodal" data-toggle="modal" href="#myModal-',job.JobID,'" id="modellink" ModalJobID="{{$row->JobID}}"><i class="glyphicon glyphicon-search"></i></a>
                                    <a class="btn btn-sm btn-success" href="/quotes_orders/',job.JobID,'/pdf"> <i class="glyphicon glyphicon-print"></i></a>
                                    <a data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-primary" aria-haspopup="true">Options <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">
                                        <li class="disabled"><a href="#"><i class="glyphicon glyphicon-ok"></i> Confirm</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyhead']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Header)</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyfull']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Full Job)</a></li>
                                        <li class=""><a href="{{ route('post.update',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID,'active'=>0]) }}"><i class="glyphicon glyphicon-remove"></i> Cancel</a></li>
                                    </ul>
                                </span>
                                @elseif($row->Selection == 4)
                                <span class="btn-group">
                                    <a class="btn btn-sm btn-info editmodal" data-toggle="modal" href="#myModal-',job.JobID,'" id="modellink" ModalJobID="',job.JobID,'"><i class="glyphicon glyphicon-search"></i></a>
                                    <a class="btn btn-sm btn-success" href="/quotes_orders/',job.JobID,'/pdf"> <i class="glyphicon glyphicon-print"></i></a>
                                    <a data-toggle="dropdown" class="dropdown-toggle btn btn-sm btn-primary" aria-haspopup="true">Options <span class="caret"></span></a>
                                    <ul class="dropdown-menu dropdown-menu-right">";
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyhead']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Header)</a></li>
                                        <li class=""><a href="{{ route('post.edit',['jobID'=>$row->JobID,'jobAcct'=>$row->AccountID, 'jobmod'=>'copyfull']) }}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Full Job)</a></li>
                                    </ul>
                                </span>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="myModal" aria-hidden="true"></div>
@endsection
@section('page-script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#table').DataTable({
            ordering: false,
        })
    });
    $(document).on('click', ".editmodal", function() {
        var ModalJobID = $(this).attr("ModalJobID");
        
        $.ajax({
            url: "/jobs/post",
            data: {
                id: ModalJobID
            },
            dataType: 'json',
            success: function(data) {
                $('#myModal').html(unescape(data.html));
                $('#myModal').modal('show');
                
            }

        });
    });
</script>
@stop