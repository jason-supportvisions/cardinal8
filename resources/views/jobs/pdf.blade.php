
@php
$ModalJobID = $data[0];
$jobinfo = $data[1];
foreach ($jobinfo as $row) {
$mjobBrand = $row->Brand;
$logo_image_small = $row->logo_image_small;
$mjobStage = $row->Stage;
$mjobAccountID = $row->AccountID;
$mjobAccountName = $row->AccountName;
$mjobType = $row->Type;
$mjobReference = $row->Reference;
$mjobList = $row->List;
$mjobNet = $row->Net;
$mjobCustomer = $row->Customer;
$mjobCreatedID = $row->CreatedID;
$mjobCreatedDate = $row->CreatedDate;
$mjobModifiedID = $row->ModifiedID;
$mjobModifiedDate = isset($row->ModifiedDate) ? $row->ModifiedDate : 'Not Modified';
$mjobActive = $row->Active;
$mjobEstLeadtime = $row->EstLeadtime;
$mjobDeposit = $row->Deposit;
$mjobBalance = $row->Balance;
$mjobEstOutProduction = $row->EstOutProduction;
$mjobCabinetCount = $row->CabinetCount;
$mjobAccessoryCount = $row->AccessoryCount;
$mjobVolume = $row->Volume;
$mjobWeight = $row->Weight;
}

@endphp


<html>
<head>
    <title>{{config('app.name')}}  | Cabinet Manufacturing Excellence</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="user-scalable=yes,
                   initial-scale=1,
                   minimum-scale=0.2, 
                   maximum-scale=2, 
                   width=device-width" 
          name="viewport"
          id="viewport">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* -->
    <base href="{{ $http_base_path }}"> 
    <link rel="manifest" href="{{ $http_base_path }}/favicon/manifest.json">
    
</head>
<body>
    <div class="container">
       <h3 class="modal-title"># {{$ModalJobID}}</h3>
    <div role="tabpanel" class="tab-pane" id="info" style="margin-left:20%">
        <table class="table table-striped table-condensed">
            <thead>
                <tr>
                    <th class="col-sm-3"></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Brand</td>
                    <td>
                        <img style='height: 40px' src="{{$logo_image_path}}">
                    </td>
                </tr>
                <tr>
                    <td>Stage</td>
                    <td>

                        @if ($mjobStage == 1)
                        Quote
                        @endif
                        @if ($mjobStage == 2)
                        Confirmed
                        @endif
                        @if ($mjobStage == 3)
                        Under Review
                        @endif
                        @if ($mjobStage == 4)
                       Accepted
                        @endif
                        @if ($mjobStage == 5)
                        Shipping
                        @endif
                        @if ($mjobStage == 6)
                        Completed
                        @endif
                        @if ($mjobStage == 7)
                        Cancelled
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Account:</td>
                    <td>
                        {{ $mjobAccountName }}
                    </td>
                </tr>
                <tr>
                    <td>Type:</td>
                    <td>
                        {{ $mjobType }}
                    </td>
                </tr>
                <tr>
                    <td>Reference:</td>
                    <td>
                        {{ $mjobReference }}
                    </td>
                </tr>
                <tr>
                    <td>List Price:</td>
                    <td>
                        $ {{ number_format($mjobList, 2) }}
                    </td>
                </tr>
                <tr>
                    <td>Net Price:</td>
                    <td>
                        $ {{ number_format($mjobNet, 2) }}
                    </td>
                </tr>
                <tr>
                    <td>Customer Price:</td>
                    <td>
                        $ {{ number_format($mjobCustomer, 2) }}
                    </td>
                </tr>
                <tr>
                    <td>Created By / Date:</td>
                    <td>
                        @php
                        $CID = $mjobCreatedID;
                        $Results2 = $data[2];
                        if (is_array($Results2)) {
                        foreach ($Results2 as $row2) {
                        $FullName2 = $row2->FirstName . " " . $row2->LastName;
                        }
                        } else {
                        $FullName2 = "";
                        }
                        echo $FullName2;
                        echo " / ";
                        echo $mjobCreatedDate; @endphp
                    </td>
                </tr>
                <tr>
                    <td>Modified By / Date:</td>
                    <td>
                        @php
                        $Results3 = $data[3];
                        if (is_array($Results3)) {
                        foreach ($Results3 as $row3) {
                        $FullName3 = $row3->FirstName . " " . $row3->LastName;
                        }
                        } else {
                        $FullName3 = "";
                        }
                        echo $FullName3;
                        echo " / ";
                        echo $mjobModifiedDate; @endphp
                    </td>
                </tr>
                <tr>
                    <td>Active</td>
                    <td>
                        @if ($mjobActive == 1)
                        True
                        @if ($mjobStage == 1)
                       
                        @endif
                        @else
                        False
                        
                        @endif
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <style>
        td {
            padding: 15px;
        }
        .modal-title{
            margin-top: 30px;
        }
    </style>

    </div>
</body>
</html>