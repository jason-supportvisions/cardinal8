@php
$ModalJobID = $data[0];
$jobinfo = $data[1];
foreach ($jobinfo as $row) {
$mjobBrand = $row->Brand;
$logo_image_small = $row->logo_image_small;
$mjobStage = $row->Stage;
$mjobAccountID = $row->AccountID;
$mjobAccountName = $row->AccountName;
$mjobType = $row->Type;
$mjobReference = $row->Reference;
$mjobList = $row->List;
$mjobNet = $row->Net;
$mjobCustomer = $row->Customer;
$mjobCreatedID = $row->CreatedID;
$mjobCreatedDate = $row->CreatedDate;
$mjobModifiedID = $row->ModifiedID;
$mjobModifiedDate = isset($row->ModifiedDate) ? $row->ModifiedDate : 'Not Modified';
$mjobActive = $row->Active;
$mjobEstLeadtime = $row->EstLeadtime;
$mjobDeposit = $row->Deposit;
$mjobBalance = $row->Balance;
$mjobEstOutProduction = $row->EstOutProduction;
$mjobCabinetCount = $row->CabinetCount;
$mjobAccessoryCount = $row->AccessoryCount;
$mjobVolume = $row->Volume;
$mjobWeight = $row->Weight;
}
@endphp
<div class="modal-dialog modal-lg">
	<div class="modal-content">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 class="modal-title"># {{$ModalJobID}}</h3>
		</div>
		<div class="modal-body">
			@if ($mjobStage == 1)
			<a class="btn btn-sm btn-warning" href="{{route('post.edit',['jobID'=>$ModalJobID,'jobAcct'=>$mjobAccountID, 'jobmod'=>'edit'])}}"><i class="glyphicon glyphicon-edit"></i> Edit Job</a>
			@endif
			{{ Form::open(['url'=>"/jobs/pdf/{$ModalJobID}"]) }}
				<a class="btn btn-sm btn-default" href="{{route('post.edit',['jobID'=>$ModalJobID,'jobAcct'=>$mjobAccountID, 'jobmod'=>'copyhead'])}}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Header)</a>
				<a class="btn btn-sm btn-default" href="{{route('post.edit',['jobID'=>$ModalJobID,'jobAcct'=>$mjobAccountID, 'jobmod'=>'copyfull'])}}"><i class="glyphicon glyphicon-duplicate"></i> Duplicate (Full Job)</a>
				<a class="btn btn-sm btn-default" href="/quote_orders/summaryPdf/{{$ModalJobID}}" name="type" value="view-summary"><i class="glyphicon glyphicon-print"></i> PDF</a>
				<a class="btn btn-sm btn-default" href="/quote_orders/{{$ModalJobID}}/cabinet-cards"><i class="glyphicon glyphicon-list"></i> Cab Card</a>
			{{ Form::close() }}
	
			<div>
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#info" aria-controls="info" role="tab" data-toggle="tab">Overview</a></li>
					<li role="presentation"><a href="#cabinet" aria-controls="cabinet" role="tab" data-toggle="tab">Cabinet</a></li>
					<li role="presentation"><a href="#files" aria-controls="files" role="tab" data-toggle="tab">Files</a></li>
				</ul>
				<div class="tab-content">
					<div role="tabpanel" class="tab-pane active" id="info">
						<table class="table table-striped table-condensed">
							<thead>
								<tr>
									<th class="col-sm-3"></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Brand</td>
									<td>
										<img style='height: 40px' src="/images/logos/{{ $logo_image_small }}">
									</td>
								</tr>
								<tr>
									<td>Stage</td>
									<td>

										@if ($mjobStage == 1)
										<a class="btn btn-xs btn-info" href="/jobs" style="width:100px;"><strong>Quote</strong></a>
										@endif
										@if ($mjobStage == 2)
										<a class="btn btn-xs btn-warning" href="/jobs" style="width:100px;"><strong>Confirmed</strong></a>
										@endif
										@if ($mjobStage == 3)
										<a class="btn btn-xs btn-danger" href="/jobs" style="width:100px;"><strong>Under Review</strong></a>
										@endif
										@if ($mjobStage == 4)
										<a class="btn btn-xs btn-success" href="/jobs" style="width:100px;"><strong>Accepted</strong></a>
										@endif
										@if ($mjobStage == 5)
										<a class="btn btn-xs btn-default" href="/jobs" style="width:100px;"><strong>Shipping</strong></a>
										@endif
										@if ($mjobStage == 6)
										<a class="btn btn-xs btn-primary" href="/jobs" style="width:100px;"><strong>Completed</strong></a>
										@endif
										@if ($mjobStage == 7)
										<a class="btn btn-xs btn-danger" href="/jobs/cancel" style="width:100px;"><strong>Cancelled</strong></a>
										@endif
									</td>
								</tr>
								<tr>
									<td>Account:</td>
									<td>
										{{ $mjobAccountName }}
									</td>
								</tr>
								<tr>
									<td>Type:</td>
									<td>
										{{ $mjobType }}
									</td>
								</tr>
								<tr>
									<td>Reference:</td>
									<td>
										{{ $mjobReference }}
									</td>
								</tr>
								<tr>
									<td>List Price:</td>
									<td>
										$ {{ number_format($mjobList, 2) }}
									</td>
								</tr>
								<tr>
									<td>Net Price:</td>
									<td>
										$ {{ number_format($mjobNet, 2) }}
									</td>
								</tr>
								<tr>
									<td>Customer Price:</td>
									<td>
										$ {{ number_format($mjobCustomer, 2) }}
									</td>
								</tr>
								<tr>
									<td>Created By / Date:</td>
									<td>
										@php
										$CID = $mjobCreatedID;
										$Results2 = $data[2];
										if (is_array($Results2)) {
										foreach ($Results2 as $row2) {
										$FullName2 = $row2->FirstName . " " . $row2->LastName;
										}
										} else {
										$FullName2 = "";
										}
										echo $FullName2;
										echo " / ";
										echo $mjobCreatedDate; @endphp
									</td>
								</tr>
								<tr>
									<td>Modified By / Date:</td>
									<td>
										@php
										$Results3 = $data[3];
										if (is_array($Results3)) {
										foreach ($Results3 as $row3) {
										$FullName3 = $row3->FirstName . " " . $row3->LastName;
										}
										} else {
										$FullName3 = "";
										}
										echo $FullName3;
										echo " / ";
										echo $mjobModifiedDate; @endphp
									</td>
								</tr>
								<tr>
									<td>Active</td>
									<td>
										@if ($mjobActive == 1)
										True
										@if ($mjobStage == 1)
										<div class="pull-right">
											<a class="btn btn-sm btn-danger" href="{{ route('post.update',['jobID'=>$ModalJobID,'jobAcct'=>$mjobAccountID,'active'=>0]) }}"></i> Cancel Job</a>
										</div>
										@endif
										@else
										False
										<div class="pull-right">
											<a class="btn btn-sm btn-success" href="{{ route('post.update',['jobID'=>$ModalJobID,'jobAcct'=>$mjobAccountID,'active'=>1]) }}"><i class="glyphicon glyphicon-check"></i> Enable Job</a>
										</div>
										@endif
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="cabinet">
						<table class="table table-striped table-condensed">
							<thead>
								<tr>
									<th class="col-sm-3"></th>
									<th>&nbsp;</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Est. Leadtime:</td>
									<td>
										{{ $mjobEstLeadtime }}
									</td>
								</tr>
								<tr>
									<td>Deposit:</td>
									<td>
										$ {{number_format($mjobDeposit, 2)}}
									</td>
								</tr>
								<tr>
									<td>Balance:</td>
									<td>
										$ {{ number_format($mjobBalance, 2)}}
									</td>
								</tr>
								<tr>
									<td>Est. Out of Production:</td>
									<td>
										{{ $mjobEstOutProduction }}
									</td>
								</tr>
								<tr>
									<td>CabinetCount:</td>
									<td>
										{{ $mjobCabinetCount }}
									</td>
								</tr>
								<tr>
									<td>AccessoryCount:</td>
									<td>
										{{ $mjobAccessoryCount }}
									</td>
								</tr>
								<tr>
									<td>Est. Volume (cbft):</td>
									<td>
										{{ $mjobVolume }}
									</td>
								</tr>
								<tr>
									<td>Est. Weight (lbs):</td>
									<td>
										{{ $mjobWeight }}
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div role="tabpanel" class="tab-pane" id="files">
						<div role="tabpanel" class="tab-pane" id="files">
							@php
							$dir = public_path().'/documents/'.$ModalJobID.'/';
							$files = array();
							if(is_dir($dir)){							
							if ($handle = opendir($dir)) {
								while (false !== ($file = readdir($handle))) {
									if ($file != "." && $file != "..") {
										$files[] = $file;
									}
								}
								closedir($handle);
								asort($files);
								@endphp
								<table class="table table-striped table-condensed">
									<thead>
										<tr>
											<th>Filename</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@if ($files)
										<tr>
											<td>No Files Uploaded</td>
										</tr>
										@endif
										@foreach ($files as $file)
											@if (strchr($file, "Cabinet"))
											<tr>
												<td>No Files Uploaded</td>
												<td></td>
											</tr>											
											@else
											<tr>
												<td> <a href={{$dir}}{{$file}} target="_blank">{{ $file }}</a></td>
												<td></td>
											</tr>
											@endif
										@endforeach
									</tbody>
								</table>
							@php } }else{ @endphp								 
							<table class="table table-striped table-condensed">
									<thead>
										<tr>
											<th>Filename</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>No Files Uploaded</td>
											<td></td>
										</tr>				
										
									</tbody>
								</table>
						@php } @endphp 

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	