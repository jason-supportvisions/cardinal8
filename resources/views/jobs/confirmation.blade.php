@php
$Title =  " | Jobs";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 1;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$pageid = "Jobs";

$jobPrint = '';
$jobPrint = 0;
$jobPricing = 0;

$SesUser = session('userid');
$SesAcct = session('accountid');
$SesType = session('accounttype');

@endphp

@extends('templates.theme')
@section('content')

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-UX">
            <div class="panel-heading"><span class="pull-right"></span>Job Duplicator</div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-6 text-center">
                        <h2 style="color: green;"> Success!</h2>
                        <br />
                        <h2>Job# {{ $data->origJobID }} <b>'{{ $data->job_results['Reference'] }}'</b> has had {{ $message }}</h2>
                        <img src="/images/dashboard/copy-job-success.jpg" width="400">
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6 text-center"><h3>{{ $data->cus_results['JobID']  }}</h3></div>
                        <div class="col-md-6 text-center"><h3>{{ $data->job_results['JobID'] }}</h3></div>
                    </div>
                    <div class="col-md-6">
                        <div class="col-md-6 text-center"><a href='/quote_orders/edit/{{ $data->cus_results['JobID'] }}/{{ $SesAcct }}/edit'>
                            <button class="btn btn-success">Go To COPIED Job</button></a>
                        </div>
                        <div class="col-md-6 text-center"><a href='/quote_orders/edit/{{ $data->job_results['JobID'] }}/{{ $SesAcct }}/edit'>
                            <button class="btn btn-warning">RETURN to Source Job</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection


