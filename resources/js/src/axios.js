// axios
import axios from 'axios'

const baseURL = 'http://localhost:8000'
// const baseURL = 'http://localhost:8010'

export default axios.create({
  baseURL
})
