<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\QuoteordersController;
use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\JobsController;
use App\Http\Controllers\OrdersController;
use App\Http\Controllers\QuotesController;
use App\Http\Controllers\PhoneMessageController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\AdministrationController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\DownloadsController;
use App\Http\Controllers\GalleryController;
use App\Http\Controllers\AjaxController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PassresetController;
use App\Http\Controllers\DoorGalleryController;
use App\Http\Controllers\CatalogController;
use App\Http\Controllers\CatalogCategoryController;
use App\Http\Controllers\ToolController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\PricerController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/price-checker', [PricerController::class, 'index']);
Route::get('/price-checker/{jobID}/{code}/{finishedinterior}/{finishedends}', [PricerController::class, 'getItemPrice']);

Route::get('/', [HomeController::class, 'index'])->name('home')->middleware('auth');
Route::get('/dashboard/index', [DashboardController::class, 'index'])->name('dashboard');

Route::get('/quote_orders', [QuoteordersController::class, 'index']);
Route::post('/job', [ApplicationController::class, 'index']); 

Route::post('quote_orders/file_upload', [QuoteordersController::class, 'fileupload']);
Route::get('quote_orders/summary/{jobID}', [QuoteordersController::class, 'summary'])->name('quote_orders.summary');
Route::get('quote_orders/summaryPdf/{jobID}', [QuoteordersController::class, 'summaryPdf'])->name('quote_orders.pdf');
Route::post('quote_orders/summaryPdf/{jobID}', [QuoteordersController::class, 'summaryPdf']);
Route::post('quote_orders/store', [QuoteordersController::class, 'store']);
Route::get('/quote_orders/generalnotes/{jobID}/{jobAcct}', [QuoteordersController::class, 'getgeneralnotes'])->name('quote_orders.notes');
Route::get('/quote_orders/edit/{jobID}/{jobAcct}/{jobmod}', [QuoteordersController::class, 'edit'])->name('post.edit');
Route::get('quote_orders/products/index/{jobID}', [QuoteordersController::class, 'product'])->name('quote_orders.product');
Route::get('quote_orders/products/add/{jobID}/{jobAcct}/{Category}', [QuoteordersController::class, 'addProduct'])->name('add-product');
Route::post('quote_orders/products/search', [QuoteordersController::class, 'searchProduct'])->name('search-product');
Route::get('quote_orders/products/create/{jobID}/{product_item_id}/{productType}', [QuoteordersController::class, 'createProduct'])->name('create-product');
Route::post('quote_orders/products/store/{JobID}', [QuoteordersController::class, 'storeProduct'])->name('store-product');

Route::get('quote_orders/products/show/{jobID}/{job_product_id}', [QuoteordersController::class, 'show'])->name('show-product');
Route::post('quote_orders/products/update/{jobID}/{job_product_id}', [QuoteordersController::class, 'update'])->name('update-product');
Route::post('quote_orders/products/delete/{jobID}/{job_product_id}/{modification_id}', [QuoteordersController::class, 'deleteModification'])->name('delete-modification');

Route::post('quote_orders/products/add', [QuoteordersController::class, 'fastAdd'])->name('fastadd');
Route::get('quote_orders/products/destroy/{JobID}/{product_item_id}', [QuoteordersController::class, 'destroy'])->name('delete-product');
Route::get('quote_orders/products/copy/{jobID}/{productID}', [QuoteordersController::class, 'copy'])->name('copy-product');
Route::post('quote_orders/products/paste', [QuoteordersController::class, 'paste'])->name('paste-product');
Route::post('/quote_orders/products/line', [QuoteordersController::class, 'updateProductLine'])->name('product-line');
Route::get('/quote_orders/{JobID}/cabinet-cards', [QuoteordersController::class, 'cabinetCardsPdf'])->name('cabinet.pdf');
Route::post('quote_orders/upload', [QuoteordersController::class, 'spec_store'])->name('spec.post');
Route::post('quote_orders/save', [QuoteordersController::class, 'final_save']);
Route::get('quote_orders/delete/{id}', [QuoteordersController::class, 'upload_delete'])->name('upload-delete');
Route::get('quote_orders/generaledit/{jobID}/{jobAcct}/{id}', [QuoteordersController::class, 'upload_edit'])->name('upload-edit');
Route::post('quote_orders/autosearch', [QuoteordersController::class, 'autocompletesearch'])->name('autocomplete-search');

Route::prefix('quote_orders/construction')->group(function () {
    Route::get('/index', [QuoteordersController::class, 'consIndex'])->name('cons-first');
    Route::get('/print/{action}/{varDoor}/{varMaterial}/{varStyle}/{varFinish}/{varInside}/{varOutside}/{varCenter}/{varStile}', [QuoteordersController::class, 'pricesheetPrint'])->name('priceprint');
});
 Route::post('/quote_orders/ajaxRequest', [AjaxController::class, 'ajaxRequestPost'])->name('ajaxRequest.post');
 Route::get('/admin/reset', [PassresetController::class, 'index'])->name('reset');
 Route::post('/admin/updatepassword', [PassresetController::class, 'updatePassword'])->name('update.pass');

Route::get('/jobs', [JobsController::class, 'index']);
Route::get('/jobs/post', [JobsController::class, 'show']);
Route::post('/jobs/pdf/{jobid}', [JobsController::class, 'jobPdf']);
Route::get('/jobs/pdf/{jobid}', [JobsController::class, 'jobPdf']);
Route::get('/jobs/cancel', [JobsController::class, 'cancel']);
Route::get('/jobs/stage/{jobStage}', [JobsController::class, 'jobStage'])->name('job-stage');
Route::get('/jobs/update/{jobID}/{jobAcct}/{active}', [JobsController::class, 'update'])->name('post.update');

Route::prefix('crm')->group(function () {

    Route::get('orders', [OrdersController::class, 'inProcess']);
    Route::get('orders/all', [OrdersController::class, 'all']);
    Route::get('orders/{job_stage_slug}', [OrdersController::class, 'stage']);
    Route::get('orders/download/{job_stage_slug}', [OrdersController::class, 'download'])->name('order.download');
    Route::get('orders/{job_id}/edit', [OrdersController::class, 'editBaseOrder']);
    Route::post('orders/{job_id}/edit', [OrdersController::class, 'updateBaseOrder']);
    Route::get('orders/{job_id}/to-cv/preview', [OrdersController::class, 'toCvPreview']);
    Route::get('orders/{job_id}/to-cv/download', [OrdersController::class, 'toCvDownload']);

    Route::get('quotes', [QuotesController::class, 'index'])->name('show.index');

    Route::resource('phone-log', PhoneMessageController::class)->except([ 'destroy']);
    Route::resource('contacts', ContactsController::class);
    Route::resource('users', UsersController::class);
    Route::get('phone-log/edit/{id}', [PhoneMessageController::class, 'edit']);
});

Route::prefix('admin')->group(function () {
    Route::get('/account', [AdministrationController::class, 'getUserAccount']);
    Route::get('/account/add', [AdministrationController::class, 'accountAdd'])->name('add-account');
    Route::get('/edit-account/{id}/{page}', [AdministrationController::class, 'accountShowEdit'])->name('show-edit');
    Route::post('/account/create', [AdministrationController::class, 'accountCreate']);
    Route::get('/user/{id}/{page}', [AdministrationController::class, 'userViewEdit'])->name('view-edit');
    Route::post('/user', [AdministrationController::class, 'userUpdate'])->name('users-update');
    Route::get('users', [UsersController::class, 'index'])->name('all-users');
    Route::get('users/{id}', [UsersController::class, 'destroy'])->name('user-delete');
    Route::post('users/create', [UsersController::class, 'userStore'])->name('user-store');
});
Route::get('/news/readmore/{id}', [NewsController::class, 'getnews'])->name('readmore');
Route::prefix('download')->group(function () {
    Route::get('/', [DownloadsController::class, 'index']);
    Route::get('/show/{id}', [DownloadsController::class, 'show'])->name('downloadinfo');
    Route::get('/edit/{id}', [DownloadsController::class, 'edit'])->name('download.edit');
    Route::post('/update', [DownloadsController::class, 'update']);
    Route::get('/create', [DownloadsController::class, 'create'])->name('download.create');
    Route::post('/store', [DownloadsController::class, 'store']);
    Route::get('/destroy/{id}', [DownloadsController::class, 'destroy'])->name('download.delete');
});

Route::prefix('gallery')->group(function () {
    Route::get('/gallery', [GalleryController::class, 'index']);
    Route::get('/show', [GalleryController::class, 'subcontent']);
    Route::get('/doors', [DoorGalleryController::class, 'show']);
});

Route::prefix('tools')->group(function () {
    Route::get('/abbreviations', [ToolController::class, 'abbreviations']);
    Route::get('/download', [ToolController::class, 'downloads']);
    Route::get('/faq', [ToolController::class, 'faq']);
    Route::get('/glossary', [ToolController::class, 'glossary']);
    Route::get('/news', [ToolController::class, 'news']);
});

Route::prefix('catalog')->group(function () {
    Route::get('/', [CatalogController::class, 'index']);
    Route::get('/generate', [CatalogController::class, 'generate']);
    Route::get('/ord', [CatalogController::class, 'ord']);
});

Route::prefix('administration')->group(function () {
    Route::get('/', [AdministrationController::class, 'index']);
    Route::get('/finishes', [AdministrationController::class, 'getFinishes']);
    Route::post('/finishes', [AdministrationController::class, 'postFinishes']);
    Route::get('/estimation-tuning', [AdministrationController::class, 'getEstimationTuning']);
    Route::post('/estimation-tuning', [AdministrationController::class, 'postEstimationTuning']);
    Route::get('/estimation-test-results', [AdministrationController::class, 'getEstimationTestResults']);
});













