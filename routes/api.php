<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\JsonResponse;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/job/image', 'App\Http\Controllers\JobController@index');
Route::get('/job/style', 'App\Http\Controllers\JobController@getStyle');
Route::get('/job/door', 'App\Http\Controllers\JobController@getDoor');
Route::get('/job/material', 'App\Http\Controllers\JobController@getMaterial');
Route::get('/job/color', 'App\Http\Controllers\JobController@getColor');
Route::get('/job/finish', 'App\Http\Controllers\JobController@getFinish');
Route::get('/job/edge', 'App\Http\Controllers\JobController@getEdge');
Route::get('/job/profile', 'App\Http\Controllers\JobController@getProfile');
