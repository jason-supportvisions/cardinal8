<?php 

require '../includes/config.php'; 
require '../includes/session.php'; 

// Page Settings (1 = True | 0 = False)
$Title = "Administration | Gallery";
$Select2 = 0;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 1;
$jQuery_Validate = 0;
$xCRUD_16 = 1;
$jobPrint = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser = $_SESSION['user']['UserID'];
$SesAcct = $_SESSION['user']['AccountID'];
$SesType = $_SESSION['user']['AccountType'];

include('../xcrud/xcrud.php');

//Gallery Tags
$xcrud1 = Xcrud::get_instance();
$xcrud1->table('gallery_tags');
$xcrud1->order_by('DisplayOrder');
$xcrud1->limit(5);
$xcrud1->limit_list('10,20,50');
$xcrud1->highlight('Active', '=', 0, '#d9534f');
$xcrud1->highlight('Active', '=', 1, '#5cb85c');
$xcrud1->column_width('Active','100px');
$xcrud1->pass_default('Active','1');


//Gallery Images Featured
$xcrud2 = Xcrud::get_instance();
$xcrud2->limit(15);
$xcrud2->table('gallery');
$xcrud2->order_by('DisplayOrder');
$xcrud2->columns('DisplayOrder, Original_Image, Title, Description, Tags,  Active, Featured');
$xcrud2->fields('DisplayOrder, Original_Image, Title, Description, Tags, Location,  Active, Featured');

$path = null;
$timestamp = null;
$timestampnow = time();
/*

Files:
	not_rename - (true/false) - disables auto-renaming of uploaded files
	text - (string) - display custom file name
	path - relative or absolute path to uploads folder
	blob - (true/false) - saves image as binary string in database blob field
	filename - (string) - name of downloadable file
	url - (string) -real url to upload folder (optional, if you want to use real links)

Images:
	not_rename - (true/false) - disables auto-renaming of uploaded files
	path - relative or absolute path to uploads folder
	width, height - (integer) - sets dimensions for image, if both is not set image will not been resized
	crop - (true/false) - image will be cropped (if not set, image will be saved with saving proportions). Both width and height required
	manual_crop - (true/false) -  Allows to crop image manually
	ratio - (float) - cropped area ratio (uses with manual_crop)
	watermark - (string) - relative or absolute path to watermark image
	watermark_position - (array) - array with two elements (left,top), sets watermark offsets in persents (example: array(95,5) - right top corner)
	blob - (true/false) - saves image as binary string in database blob field. Thumbnails creation is not available.
	grid_thumb - (int) - number of thumb, which will be displayed in grid. If not set, original image will be displayed.
	detail_thumb - (int) - number of thumb, which will be displayed in detail view/create/edit . If not set, original image will be displayed.
	url - (string) -real url to upload folder (optional, if you want to use real links)
	thumbs - array of thumb arrays:
	width, height, crop, watermark, watermark_position - see parent
	marker - (string) - thumbnail marker (if you not set marker or folder, the main image will be replaced with thumbnail)
	folder - (string) - thumbnail subfolder, relative to uploads folder (if you not set marker or folder, the main image will be replaced with thumbnail)
All paths are always relative to xcrud's folder

*/
//get dimension of uploaded image

//find out which side is the longest

//constrain that size to 400

//create the thumbnail and upload it
// test writing somecode here with newlaptop 
// database field, type of upload, default, attributes
$xcrud2->change_type('Original_Image', 'image', '', array('thumbs' => 
array(
	array(
		'width'=> '340',
		'crop' => true,
		'marker' => '_300',
		), 
	array(
		'width' => '800',
		'marker' => '_800'),
	
	),'grid_thumb'=>'0','not_rename'=>true,'path'=>''.$WebDir.'/images/gallery/'));

// $imageInfo = getimagesize('../images/gallery/Johnny300.jpg'); 
// print_r($imageInfo);
// echo "<br><br>";
// $files = array_merge(glob("../images/gallery/*.png"), glob("../images/gallery/*.jpg"));
// $files = array_combine($files, array_map("filemtime", $files));
// arsort($files);
// $latest_file = key($files);
// print_r($latest_files);




// $dirname = dirname(__FILE__);
// $dirname = "../images/gallery/";
// $dir = new DirectoryIterator($dirname);
// foreach ($dir as $fileinfo) {
//     if (!$fileinfo->isDot()) {

// 		$fileTimeStamp = $fileinfo->getMTime();

// 		if($fileTimeStamp > $timestampnow){
// 			$fileTimeStamp = "<b>" . $fileTimeStamp . "</b>";
// 		}
// 		echo "<br>getMTime() " . $fileTimeStamp . " vs NOW(" . $timestampnow . ")";

//         if ($fileTimeStamp > $timestampnow) {
//             // current file has been modified more recently
// 			// than any other file we've checked until now

//             $path = $fileinfo->getFilename();
// 			$fileTimeStamp = $fileinfo->getMTime();
// 			echo "<br> path: $path -- timestamp $fileTimeStamp - $timestampnow"; 
//         }
//     }
// }

/*
object(Xcrud)#3 (176) {
  ["demo_mode":"Xcrud":private]=>
  bool(false)
  ["ajax_request":protected]=>
  bool(false)
  ["instance_name"]=>
  string(40) "5bfddd698931e03a287ab51f0c15340a63950791"
  ["instance_count":protected]=>
  int(2)
  ["table":protected]=>
  string(7) "gallery"
  ["table_name":protected]=>
  NULL
  ["primary_key":protected]=>
  NULL
  ["primary_val":protected]=>
  NULL
  ["where":protected]=>
  array(0) {
  }
  ["order_by":protected]=>
  array(1) {
    ["gallery.DisplayOrder"]=>
    string(3) "asc"
  }
  ["relation":protected]=>
  array(0) {
  }
  ["fields":protected]=>
  array(0) {
  }
  ["fields_create":protected]=>
  array(8) {
    ["gallery.DisplayOrder"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(12) "DisplayOrder"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Original_Image"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(14) "Original_Image"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Title"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(5) "Title"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Description"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(11) "Description"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Tags"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(4) "Tags"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Location"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Location"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Active"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(6) "Active"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Featured"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Featured"
      ["tab"]=>
      bool(false)
    }
  }
  ["fields_edit":protected]=>
  array(8) {
    ["gallery.DisplayOrder"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(12) "DisplayOrder"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Original_Image"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(14) "Original_Image"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Title"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(5) "Title"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Description"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(11) "Description"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Tags"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(4) "Tags"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Location"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Location"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Active"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(6) "Active"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Featured"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Featured"
      ["tab"]=>
      bool(false)
    }
  }
  ["fields_view":protected]=>
  array(8) {
    ["gallery.DisplayOrder"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(12) "DisplayOrder"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Original_Image"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(14) "Original_Image"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Title"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(5) "Title"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Description"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(11) "Description"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Tags"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(4) "Tags"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Location"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Location"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Active"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(6) "Active"
      ["tab"]=>
      bool(false)
    }
    ["gallery.Featured"]=>
    array(3) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Featured"
      ["tab"]=>
      bool(false)
    }
  }
  ["fields_list":protected]=>
  array(7) {
    ["gallery.DisplayOrder"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(12) "DisplayOrder"
    }
    ["gallery.Original_Image"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(14) "Original_Image"
    }
    ["gallery.Title"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(5) "Title"
    }
    ["gallery.Description"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(11) "Description"
    }
    ["gallery.Tags"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(4) "Tags"
    }
    ["gallery.Active"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(6) "Active"
    }
    ["gallery.Featured"]=>
    array(2) {
      ["table"]=>
      string(7) "gallery"
      ["field"]=>
      string(8) "Featured"
    }
  }
  ["fields_names":protected]=>
  array(0) {
  }
  ["labels":protected]=>
  array(0) {
  }
  ["columns":protected]=>
  array(0) {
  }
  ["columns_names":protected]=>
  array(0) {
  }
  ["is_create":protected]=>
  bool(true)
  ["is_edit":protected]=>
  bool(true)
  ["is_view":protected]=>
  bool(true)
  ["is_remove":protected]=>
  bool(true)
  ["is_csv":protected]=>
  bool(true)
  ["is_search":protected]=>
  bool(true)
  ["is_print":protected]=>
  bool(true)
  ["is_title":protected]=>
  bool(true)
  ["is_numbers":protected]=>
  bool(true)
  ["is_duplicate":protected]=>
  bool(false)
  ["is_inner":protected]=>
  bool(false)
  ["is_pagination":protected]=>
  bool(true)
  ["is_limitlist":protected]=>
  bool(true)
  ["is_sortable":protected]=>
  bool(true)
  ["is_list":protected]=>
  bool(true)
  ["buttons_position":protected]=>
  string(5) "right"
  ["buttons":protected]=>
  array(0) {
  }
  ["readonly":protected]=>
  array(0) {
  }
  ["disabled":protected]=>
  array(0) {
  }
  ["validation_required":protected]=>
  array(0) {
  }
  ["validation_pattern":protected]=>
  array(0) {
  }
  ["before_insert":protected]=>
  array(0) {
  }
  ["before_update":protected]=>
  array(0) {
  }
  ["before_remove":protected]=>
  array(0) {
  }
  ["after_insert":protected]=>
  array(0) {
  }
  ["after_update":protected]=>
  array(0) {
  }
  ["after_remove":protected]=>
  array(0) {
  }
  ["field_type":protected]=>
  array(1) {
    ["gallery.Original_Image"]=>
    string(5) "image"
  }
  ["field_attr":protected]=>
  array(0) {
  }
  ["defaults":protected]=>
  array(1) {
    ["gallery.Original_Image"]=>
    string(0) ""
  }
  ["limit":protected]=>
  int(15)
  ["limit_list":protected]=>
  array(4) {
    [0]=>
    string(2) "25"
    [1]=>
    string(2) "50"
    [2]=>
    string(3) "100"
    [3]=>
    string(3) "all"
  }
  ["column_cut":protected]=>
  int(50)
  ["column_cut_list":protected]=>
  array(0) {
  }
  ["no_editor":protected]=>
  array(0) {
  }
  ["show_primary_ai_field":protected]=>
  bool(false)
  ["show_primary_ai_column":protected]=>
  bool(false)
  ["url":protected]=>
  NULL
  ["key":protected]=>
  NULL
  ["benchmark":protected]=>
  bool(false)
  ["search_pattern":protected]=>
  array(2) {
    [0]=>
    string(1) "%"
    [1]=>
    string(1) "%"
  }
  ["connection":protected]=>
  bool(false)
  ["start_minimized":protected]=>
  bool(false)
  ["remove_confirm":protected]=>
  bool(true)
  ["upload_folder":protected]=>
  array(0) {
  }
  ["upload_config":protected]=>
  array(1) {
    ["gallery.Original_Image"]=>
    array(4) {
      ["thumbs"]=>
      array(2) {
        [0]=>
        array(3) {
          ["width"]=>
          string(3) "340"
          ["crop"]=>
          bool(true)
          ["marker"]=>
          string(4) "_300"
        }
        [1]=>
        array(2) {
          ["width"]=>
          string(3) "800"
          ["marker"]=>
          string(4) "_800"
        }
      }
      ["grid_thumb"]=>
      string(1) "0"
      ["not_rename"]=>
      bool(true)
      ["path"]=>
      string(45) "C:\xampp\htdocs\snapie\public/images/gallery/"
    }
  }
  ["upload_folder_def":protected]=>
  string(10) "../uploads"
  ["upload_to_save":protected]=>
  array(0) {
  }
  ["upload_to_remove":protected]=>
  array(0) {
  }
  ["binary":protected]=>
  array(0) {
  }
  ["pass_var":protected]=>
  array(0) {
  }
  ["reverse_fields":protected]=>
  array(4) {
    ["list"]=>
    bool(false)
    ["create"]=>
    bool(false)
    ["edit"]=>
    bool(false)
    ["view"]=>
    bool(false)
  }
  ["no_quotes":protected]=>
  array(0) {
  }
  ["join":protected]=>
  array(0) {
  }
  ["inner_where":protected]=>
  array(0) {
  }
  ["inner_table_instance":protected]=>
  array(0) {
  }
  ["condition":protected]=>
  array(0) {
  }
  ["theme":protected]=>
  string(9) "bootstrap"
  ["unique":protected]=>
  array(0) {
  }
  ["fk_relation":protected]=>
  array(0) {
  }
  ["links_label":protected]=>
  array(0) {
  }
  ["emails_label":protected]=>
  array(0) {
  }
  ["sum":protected]=>
  array(0) {
  }
  ["alert_create":protected]=>
  NULL
  ["alert_edit":protected]=>
  NULL
  ["subselect":protected]=>
  array(0) {
  }
  ["subselect_before":protected]=>
  array(0) {
  }
  ["highlight":protected]=>
  array(0) {
  }
  ["highlight_row":protected]=>
  array(0) {
  }
  ["modal":protected]=>
  array(0) {
  }
  ["column_class":protected]=>
  array(0) {
  }
  ["no_select":protected]=>
  array(0) {
  }
  ["primary_ai":protected]=>
  bool(false)
  ["language":protected]=>
  string(2) "en"
  ["subselect_query":protected]=>
  array(0) {
  }
  ["where_pri":protected]=>
  array(0) {
  }
  ["field_params":protected]=>
  array(0) {
  }
  ["mass_alert_create":protected]=>
  array(0) {
  }
  ["mass_alert_edit":protected]=>
  array(0) {
  }
  ["column_callback":protected]=>
  array(0) {
  }
  ["field_callback":protected]=>
  array(0) {
  }
  ["replace_insert":protected]=>
  array(0) {
  }
  ["replace_update":protected]=>
  array(0) {
  }
  ["replace_remove":protected]=>
  array(0) {
  }
  ["send_external_create":protected]=>
  array(0) {
  }
  ["send_external_edit":protected]=>
  array(0) {
  }
  ["locked_fields":protected]=>
  array(0) {
  }
  ["column_pattern":protected]=>
  array(0) {
  }
  ["field_tabs":protected]=>
  array(0) {
  }
  ["field_marker":protected]=>
  array(0) {
  }
  ["field_tooltip":protected]=>
  array(0) {
  }
  ["table_tooltip":protected]=>
  array(0) {
  }
  ["column_tooltip":protected]=>
  array(0) {
  }
  ["search_columns":protected]=>
  array(0) {
  }
  ["search_default":protected]=>
  NULL
  ["column_width":protected]=>
  array(0) {
  }
  ["order_column":protected]=>
  NULL
  ["order_direct":protected]=>
  string(3) "asc"
  ["result_list":protected]=>
  array(0) {
  }
  ["result_row":protected]=>
  array(0) {
  }
  ["result_total":protected]=>
  int(0)
  ["is_get":protected]=>
  bool(false)
  ["after":protected]=>
  NULL
  ["table_info":protected]=>
  NULL
  ["before_upload":protected]=>
  array(0) {
  }
  ["after_upload":protected]=>
  array(0) {
  }
  ["after_resize":protected]=>
  array(0) {
  }
  ["custom_vars":protected]=>
  array(0) {
  }
  ["tabdesc":protected]=>
  array(0) {
  }
  ["column_name"]=>
  array(0) {
  }
  ["search"]=>
  int(0)
  ["hidden_columns":protected]=>
  array(0) {
  }
  ["hidden_fields":protected]=>
  array(0) {
  }
  ["range":protected]=>
  string(0) ""
  ["task":protected]=>
  string(0) ""
  ["column":protected]=>
  bool(false)
  ["phrase":protected]=>
  string(0) ""
  ["inner_value":protected]=>
  bool(false)
  ["fields_output":protected]=>
  array(0) {
  }
  ["hidden_fields_output":protected]=>
  array(0) {
  }
  ["start":protected]=>
  int(0)
  ["before":protected]=>
  string(0) ""
  ["bit_field":protected]=>
  array(0) {
  }
  ["point_field":protected]=>
  array(0) {
  }
  ["float_field":protected]=>
  array(0) {
  }
  ["text_field":protected]=>
  array(0) {
  }
  ["int_field":protected]=>
  array(0) {
  }
  ["grid_condition":protected]=>
  array(0) {
  }
  ["hide_button":protected]=>
  array(0) {
  }
  ["set_lang":protected]=>
  array(0) {
  }
  ["table_ro"]=>
  bool(false)
  ["load_view":protected]=>
  array(4) {
    ["list"]=>
    string(19) "xcrud_list_view.php"
    ["create"]=>
    string(21) "xcrud_detail_view.php"
    ["edit"]=>
    string(21) "xcrud_detail_view.php"
    ["view"]=>
    string(21) "xcrud_detail_view.php"
  }
  ["grid_restrictions":protected]=>
  array(0) {
  }
  ["direct_select_tags":protected]=>
  array(0) {
  }
  ["action":protected]=>
  array(0) {
  }
  ["exception":protected]=>
  bool(false)
  ["exception_fields":protected]=>
  array(0) {
  }
  ["exception_text":protected]=>
  string(0) ""
  ["message":protected]=>
  array(0) {
  }
  ["nested_rendered":protected]=>
  array(0) {
  }
  ["default_tab":protected]=>
  bool(false)
  ["prefix":protected]=>
  string(0) ""
  ["query":protected]=>
  string(0) ""
  ["total_query":protected]=>
  string(0) ""
  ["condition_backup":protected]=>
  array(0) {
  }
  ["is_rtl":protected]=>
  bool(false)
  ["strip_tags":protected]=>
  bool(true)
  ["safe_output":protected]=>
  bool(false)
  ["before_list":protected]=>
  array(0) {
  }
  ["before_create":protected]=>
  array(0) {
  }
  ["before_edit":protected]=>
  array(0) {
  }
  ["before_view":protected]=>
  array(0) {
  }
  ["lists_null_opt":protected]=>
  bool(true)
  ["custom_fields":protected]=>
  array(0) {
  }
  ["date_format":protected]=>
  array(2) {
    ["php_d"]=>
    string(5) "n/j/Y"
    ["php_t"]=>
    string(7) "g:i:s a"
  }
  ["cancel_file_saving":protected]=>
  bool(false)
}
*/

var_dump($xcrud2->fields_list);



// $xcrud2->change_type('Original_Image', 'image', '', array('thumbs' => 
// array(
// 	array(
// 		'ratio' => 0.2,
// 		'crop' => true,
// 		'marker' => '_300',
// 		), 
// 	array(
// 		'width' => '800',
// 		'marker' => '_800'),
	
// 	),'grid_thumb'=>'0','not_rename'=>true,'path'=>''.$WebDir.'/images/gallery/'));

// $xcrud2->change_type('Original_Image', 'image', '', array('ratio' => 0.2, 'manual_crop' => true));

// image upload with manual crop and fixed ratio
// $xcrud2->change_type('Original_Image', 'image', '', array('ratio' => 0.2, 'manual_crop' => true));


//$xcrud2->relation('Tag','gallery_tags','Tags','gallery');

$xcrud2->relation('Tags','gallery_tags','ID',array('Name'),'','','multiselect');
$xcrud2->change_type('Tags','multiselect','','');

$xcrud2->highlight('Active', '=', 0, '#d9534f');
$xcrud2->highlight('Active', '=', 1, '#5cb85c');
$xcrud2->pass_default('Active','1');
$xcrud2->column_width('Active','50px');

$xcrud2->highlight('Featured', '=', 0, '#d9534f');
$xcrud2->highlight('Featured', '=', 1, '#5cb85c');
$xcrud2->column_width('Featured','80px');

$xcrud2->limit(10);
$xcrud2->limit_list('10,20,50'); 

$xcrud2->unset_title();
$xcrud2->unset_numbers();

?>

<?php require("../includes/load_head.php"); ?>

<body>
<div class="container">
	<?php include '../includes/navbar.php'; ?>
</div>

<div class="container">
	
	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-admin">
					<h3 class="panel-title"><strong><?php echo $Title ?></strong></h3>
				</div>
				<div class="panel-body">
					
					<?php echo $xcrud2->render(); ?>
				</div>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-sm-12">
			<div class="panel panel-default">
				<div class="panel-heading panel-heading-admin">
					<h3 class="panel-title"><strong><?php echo $Title ?> Tags</strong></h3>
				</div>
				<div class="panel-body">
					
					<?php echo $xcrud1->render(); ?>
				</div>
			</div>
		</div>
	</div>

</div>

<div class="container">
	<?php include '../includes/footer.php'; ?>
</div>

<?php require("../includes/load_js.php"); ?>

<!-- Placed below scripts loaded above for use -->
<script type="text/javascript">


</script>  

</body>
</html>
<?php  // $conn->close(); ?>