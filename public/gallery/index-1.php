<?php 

require '../includes/config.php'; 
require '../includes/session.php';
include '../includes/functions.php';

// Page Settings (1 = True | 0 = False)
$Title =  " | Gallery";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;
$page = "no results";
$prevpage = 0;
$total_pages =0;
$closeDiv = "";
// $imgCnt = -1;

$SesUser = $_SESSION['user']['UserID'];
$SesAcct = $_SESSION['user']['AccountID'];
$SesType = $_SESSION['user']['AccountType'];


//Search Tags
if(!empty($_POST['Tag']) AND !empty($_POST['Keyword'])){
	
	$tagid = $_POST['Tag'];
	$tagResults = $conn->query("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
	while ($row = $tagResults->fetch_array()) { 
		$tagName = $row['Name'];
		$tagOrder = $row['DisplayOrder'];
	}
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' AND (Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%') ORDER BY DisplayOrder");
	$keyword = $_POST['Keyword'];
	
	} elseif(isset($_POST['Tag']) AND empty($_POST['Keyword'])){
		
		$tagid = $_POST['Tag'];
		$tagResults = $conn->query("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
		while ($row = $tagResults->fetch_array()) { 
			$tagName = $row['Name'];
			$tagOrder = $row['DisplayOrder'];
		}
		$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' ORDER BY DisplayOrder");
		$keyword = "";
		
	} elseif(isset($_POST['Keyword']) AND empty($_POST['Tag'])){
		
		$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%' ORDER BY DisplayOrder");
		$keyword = $_POST['Keyword'];
	} else {
		
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 ORDER BY DisplayOrder");
	$keyword = "";
	$tagid = "";
	$tagName = "";
}

?>
<?php require("../includes/load_head.php"); ?>
    <body>

        <div class="container">
            <?php include '../includes/navbar.php'; ?>
        </div>
	
	<!--  -->
    
	<!-- Search Bar -->
    <div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="well well-sm">
					<div class="col-sm-12">
						<form action="/gallery/" id="Search" method="post"> 
							<div class="form-horizontal">
								<div class="form-group" style="margin-bottom: 0px">
									
									<label class="col-sm-2 control-label" for="Keyword">Search Keyword:</label>
									<div class="col-sm-3">
										<input class="form-control" id="Keyword" name="Keyword" placeholder="Keyword" type="text" value="<?php echo $keyword; ?>"/>
									</div>
									
									<label class="col-sm-2 control-label" for="Tag">Search Tag:</label>
									<div class="col-sm-3">
										<select class="form-control" id="Tag" name="Tag">
											<option></option>
											<?php selected("gallery_tags", $tagid, "", "") ?>
										</select>
									</div>
								</div>
							</div> <!-- end form horiz -->
						</form>
						<div class="col-sm-2 text-right">
							<?php if(!empty($_POST['Tag']) OR !empty($_POST['Keyword'])){ ?>
								<a href="../gallery/"><button type="button" class="btn btn-danger">Clear</button></a>
							<?php } ?>
							<button type="submit" class="btn btn-primary">Search</button>												
						</div>
					</div>
				</div><!-- end well well -->
			</div>
		</div> <!-- end row -->
	</div> <!-- end container -->
	

	<?php
		//Search returns no results
		if (!$Results->num_rows) {
	?>
	
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
				<div class="thumbnail">
					<div class="image-box">
						<h3 class="block-center">', 'No Images Found', '</h3>';
					</div>
				</div>
			</div>
		</div>
	</div>	

	<?php
		//Search returns results
		}else{			
	?>
	<div class="container">
	<?php
		//get page number from url
		(isset($_GET['page'])) ? $page = $_GET['page'] : $page = 1;

		//paging logic
		$num_of_records_per_page = 9;
		$total_records = $Results->num_rows;
		$start = ($page - 1) * $num_of_records_per_page;
		$nextpage = ($page + 1);
		$prevpage = ($page - 1);
		$total_pages = ceil($total_records / $num_of_records_per_page);

		if ($page > $total_pages) {
			$page = $total_pages;
		}
		if ($page < 1) {
			$page = 1;
		}
		
		$i = 1;
				
		//Search SQL
		if(!empty($_POST['Tag']) AND !empty($_POST['Keyword'])){

			$tagid = $_POST['Tag'];
			$tagResults = $conn->query("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
			while ($row = $tagResults->fetch_array()) { 
				$tagName = $row['Name'];
				$tagOrder = $row['DisplayOrder'];
			}
			$query = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' AND (Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%') ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page");
			$keyword = $_POST['Keyword'];
			
		} elseif(isset($_POST['Tag']) AND empty($_POST['Keyword'])){
			
			$tagid = $_POST['Tag'];
			$tagResults = $conn->query("SELECT Name FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
			//while ($row = $tagResults->fetch_array()) { 
			//$tagName = $row['Name'];
			//}
			$query = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page");
			$keyword = "";
			
		} elseif(isset($_POST['Keyword']) AND empty($_POST['Tag'])){
			
			$query = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%' ORDER BY DisplayOrder LIMIT $start, $num_of_records_per_page");
			$keyword = $_POST['Keyword'];
		}else {
						
			$query = $conn->query("SELECT * FROM gallery ORDER BY DisplayOrder ASC LIMIT $start, $num_of_records_per_page");
		}		
	
	?>
	
	<?php

	
	while ($row = $query->fetch_array()) { 
		
			//new query just started
			if(!isset($imgCnt)){
				$imgCnt = 1;
				echo "
				<div class='row'>";
				
			}else{
			// 	// echo "image count before test: $imgCnt";
			// 	//if we've put three images up then make a new row
				if($imgCnt >= 3){
					
					// echo "<div class=\"row\">";	
					echo "</div>";
					echo "<div class=\"row\">";
					$imgCnt = 1;
				}else{
					

					// //close row
					// if($imgCnt >= 3){
					// 	$closeDiv = "</div>";
					// }else{
					// 	$closeDiv = "";
					// }
					$imgCnt = $imgCnt + 1; 
				} 

			}//end query started check
		
		?>
	
			<div class="col-sm-4">
				<div class="thumbnail">
					<div class="image-box">
						<a data-toggle="modal" href="#<?php echo $row['ID']; ?>">
							<img class="img-responsive" style="width: 100%;" src="/images/gallery/<?php echo str_replace(".","_300.",$row['Original_Image']); ?>">
						</a>		
					</div> <!-- end image box -->
				</div><!-- end thumbnail -->
			</div> <!-- col -->
		

		<?php 

				//if we've put three images up then make a new row
				// if($closeDiv == "</div>"){
				// 	echo $closeDiv;
				// 	$closeDiv = ""; 	
				// }else{
				// 	$closeDiv = ""; 	
				// } 
	
		} //end while loop results
		
	}	
?>
         
            
            
            
	

        

	
	<div class="row">
	<div class="col-xs-12 pull-right">
		<ul class="pagination"> 
		 
          
		  <?php
		  if($page != "no results"){
			if($page == 1){
				echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Previous</a></li>";
			} else {
				echo "<li class=\"paginate_button previous\"><a href=\"?page=$prevpage\">Previous</a></li>";
			}
					
					for($i=1; $i <= $total_pages; $i++) {
						if($i == $page){
							echo "<li class=\"active\"><a href='?page=$i'>$i</a></li>";
						} else {
							echo "<li><a href='?page=$i'>$i</a></li>";
						}
					}
					
				if($page == $total_pages){
				echo "<li class=\"paginate_button disabled\" style=\"pointer-events:none;\"><a href=\"#\">Next</a></li>";
				} else {
				echo "<li class=\"paginate_button\"><a href=\"?page=$nextpage\">Next</a></li>";
				}
		  } //page 0
			?>
			
          
        </ul>
	</div>
	</div>
   </div> 
   
    <!-- /.container -->

        <div class="container">
            <?php include '../includes/footer.php'; ?>
        </div>

<?php require("../includes/load_js.php"); ?>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight.js"></script>
<!-- Placed below scripts loaded above for use -->
<!-- <script type="text/javascript">
		$(document).ready(function () {
           
                $('[data-toggle="tooltip"]').tooltip({html:true});
                
                $('[data-toggle="popover"]').popover({html:true}); 
            });

			$('.thumbnail').matchHeight(); /* match height of gallery boxes */
        </script> -->

    </body>
</html>
<?php  // $conn->close(); ?>