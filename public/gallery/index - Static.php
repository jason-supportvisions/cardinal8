<?php 

require '../includes/config.php'; 
require '../includes/session.php'; 

?>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta name="viewport" content="width=device-width">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <meta content="" name="description">
        <meta content="" name="author">
        <link href="favicon.ico" rel="icon">

        <title> <?php echo config('app.name'); ?> | TEMPLATE</title>

        <!-- Bootstrap core CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
        <link href="/css/bootstrap-theme.min.css" rel="stylesheet">

        <!-- Custom styles for this template -->
        <link href="/css/custom.css" rel="stylesheet">
        <link href="/css/bootstrap-select.css" rel="stylesheet">

        <!-- jquery core JavaScript -->
        <script src="/js/jquery-2.1.4.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]> <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script> <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> <![endif]-->

    </head>

    <body>
        <div class="container">
            <?php include '../includes/navbar.php'; ?>
        </div>

      <!-- Page Content -->
    <div class="container">

        <div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
				<div class="thumbnail">
				       <img class="img-responsive" src="/images/gallery/JLaurie_300.png" alt="">
                </a>
				<h4>Ames<br><small>Ames Gloss Laminate, Grey Melamine Box<br>Brooklyn, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/JLaurie_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/JLaurie.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_8066_mod-Sh_300.png" alt="">
                </a>
				<h4>Beil<br><small>Painted Shaker, Plywood Box<br><br>Larchmont, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_8066_mod Sh_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_8066_mod Sh.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/loadingdock5_300.png" alt="">
                </a>
				<h4>Bergstrom Europly<br><small>White Gloss Laminate over Europly Core<br><br>Brooklyn, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/loadingdock5_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/loadingdock5.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_0402m_300.png" alt="">
                </a>
				<h4>Drake <br><small>Rift Cut White Oak w White Glaze, White Satin Paint, Melamine Box<br>Brooklyn, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_0402m_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_0402m.jpg">Original</a></p>
				 </div>
            </div>
		</div>
       <div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_0146_shmod01_300.png" alt="">
                </a>
				<h4>Spectra<br><small>White Satin Paint Integrated handle<br><br>Brooklyn, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_0146_shmod01_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_0146_shmod01.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_3583_300.png" alt="">
                </a>
				<h4>Spectra<br><small>Machi Integrated handle<br><br>Brooklyn, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_3583_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_3583.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/Amber-BB-Gloss-HPL_300.png" alt="">
                </a>
				<h4>Zeuli Amber <br><small>Amber Bamboo, Ames Gloss Laminate, Melamine Box<br>New York, NY</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/Amber BB Gloss HPL_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/Amber BB Gloss HPL.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_5114_mod_01_300.png" alt="">
                </a>
				<h4>Zeuli and Bergstrom<br><small>Amber Bamboo, White Laminte over Amber Bamboo, Melamine Box<br>Weehawken, NJ</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_5114_mod_01_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_5114_mod_01.jpg">Original</a></p>
				 </div>
            </div>
			</div>
        <div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                   <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_5099_300.png" alt="">
                </a>
				<h4>Zeuli and Bergstrom<br><small>Amber Bamboo, White Laminte over Amber Bamboo, Melamine Box<br>Weehawken, NJ</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_5099_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_5099.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
                <a class="" href="#">
                    <img class="img-responsive" src="/images/gallery/IMG_5096_shmod01_300.png" alt="">
                </a>
				<h4>Zeuli and Bergstrom<br><small>Amber Bamboo, White Laminte over Amber Bamboo, Melamine Box<br>Weehawken, NJ</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_5096_shmod01_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_5096_shmod01.jpg">Original</a></p>
				 </div>
            </div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
             <a class="" href="#">
                    
	<img class="img-responsive" src="/images/gallery/IMG_5092_300.png" alt="">
                </a>
				<h4>Zeuli and Bergstrom<br><small>Amber Bamboo, White Laminte over Amber Bamboo, Melamine Box<br>Weehawken, NJ</small></h4>
				 <p class="text-center"><a class="btn btn-sm" href="/images/gallery/IMG_5092_800.jpg">800x600</a> <a class="btn btn-sm" href="/images/gallery/IMG_5092.jpg">Original</a></p>
				 </div>
	</div>
			<div class="col-md-3 col-sm-6 col-xs-12 thumb">
			<div class="thumbnail">
	<a class="" href="#">

	<img class="img-responsive" src="/images/gallery/_MG_6752_mod01_300.png" alt="">
	</a>
				<h4>Zeuli Contour<br><small>Neopolitan Strand Bamboo, Integrated Handle, Plywood Box<br>Bucks County, PA</small></h4>
				<p class="text-center"><a class="btn btn-sm" href="/images/gallery/_MG_6752_mod01_800.jpg">800x600</a><a class="btn btn-sm" href="/images/gallery/_MG_6752_mod01.jpg">Original</a></p>
				</div>
           
            </div>
			
			
        </div>

        <hr>

    </div>
    <!-- /.container -->

        <div class="container">
            <?php include '../includes/footer.php'; ?>
        </div>

        <!-- Bootstrap core JavaScript -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"  crossorigin="anonymous"></script>
        <script src="../js/bootstrap-select.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="/js/ie10-viewport-bug-workaround.js"></script>

        <!-- if using angular uncomment -->
        <!--<script src="../js/angular.min.js"></script>-->
        <!--<script src="../js/ui-bootstrap-tpls-0.10.0.min.js"></script>-->
        <!--<script src="app/app_AddProduct.js"></script>-->
        <!-- change as needed -->

        <!-- Placed below scripts loaded above for use -->
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </body>
</html>
<?php  // $conn->close(); ?>