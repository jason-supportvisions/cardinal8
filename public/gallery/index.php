
<?php //require("../includes/load_head.php"); ?>
    <body style="margin:0px;padding:0px;overflow:hidden">
    
	    <div class="container">
            <?php //include '../includes/navbar.php'; ?>
        </div>

		<!-- start gallery iframe -->
		<iframe 
			src="gallery-iframe.php" 
			name="gallery-iframe" 
			frameborder="0" 
			width="100%" 
			height="1000"	
		></iframe>
		<!-- ./ galler iframe -->


		<?php //require("../includes/load_js.php"); ?>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight.js"></script>

		<!-- Placed below scripts loaded above for use -->
		<script type="text/javascript">
				$(document).ready(function () {
				
						$('[data-toggle="tooltip"]').tooltip({html:true});
						
						$('[data-toggle="popover"]').popover({html:true}); 

						$('.thumbnail').matchHeight(); /* match height of gallery boxes */
					});
        </script>
		</div>
    </body>
</html>
<?php  // $conn->close(); ?>