<?php 

include_once '../includes/config.php'; 
include_once '../includes/session.php';
include_once '../includes/functions.php';

// Page Settings (1 = True | 0 = False)
$Title =  " | Gallery";
$Select2 = 1;
$Select2beta = 0;
$DataTables = 0;
$jQuery_UI = 0;
$jQuery_Validate = 0;
$xCRUD_16 = 0;
$jobPrint = isset($_GET['Print']) ? $_GET['Print'] : 0;
$jobPricing = isset($_GET['Pricing']) ? $_GET['Pricing'] : 0;

$SesUser = $_SESSION['user']['UserID'];
$SesAcct = $_SESSION['user']['AccountID'];
$SesType = $_SESSION['user']['AccountType'];


//Search Tags
if(!empty($_POST['Tag']) AND !empty($_POST['Keyword'])){
	
	$tagid = $_POST['Tag'];
	$tagResults = $conn->query("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
	while ($row = $tagResults->fetch_array()) { 
		$tagName = $row['Name'];
		$tagOrder = $row['DisplayOrder'];
	}
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' AND (Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%') ORDER BY DisplayOrder");
	$keyword = $_POST['Keyword'];
	
} elseif(isset($_POST['Tag']) AND empty($_POST['Keyword'])){
	
	$tagid = $_POST['Tag'];
	$tagResults = $conn->query("SELECT * FROM gallery_tags WHERE ID = $tagid ORDER BY DisplayOrder");
	while ($row = $tagResults->fetch_array()) { 
		$tagName = $row['Name'];
		$tagOrder = $row['DisplayOrder'];
	}
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Tags LIKE '%".$tagid."%' ORDER BY DisplayOrder");
	$keyword = "";
	
} elseif(isset($_POST['Keyword']) AND empty($_POST['Tag'])){
	
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 AND Title LIKE '%".$_POST['Keyword']."%' OR Description LIKE '%".$_POST['Keyword']."%' ORDER BY DisplayOrder");
	$keyword = $_POST['Keyword'];
} else {
	
	$Results = $conn->query("SELECT * FROM gallery WHERE Active = 1 ORDER BY DisplayOrder");

	$keyword 	= "";
	$tagid 		= "";
	$tagName	 = "";
}

?>
    <?php include_once("../includes/load_head.php"); ?>
    <body>
<!-- Search Bar -->
    <div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="well well-sm">
					<div class="col-sm-12">
						<form action="/gallery/" id="Search" method="post"> 
							<div class="form-horizontal">
								<div class="form-group" style="margin-bottom: 0px">
									<!-- <div class="col-sm-3 text-right">
									</div> -->
									<label class="col-sm-1 control-label" for="Keyword">Keyword</label>
									<div class="col-sm-3">
										<input class="form-control" id="Keyword" name="Keyword" placeholder="Keyword" type="text" value="<?php echo $keyword; ?>"/>
									</div>
									
									<label class="col-sm-1 control-label" for="Tag">Type</label>
									<div class="col-sm-3">
										<select class="form-control" id="Tag" name="Tag">
											<option></option>
											<?php selected("gallery_tags", $tagid, "", "") ?>
										</select>
									</div>
									<div class="col-sm-4 text-right">
										<?php if(!empty($_POST['Tag']) OR !empty($_POST['Keyword'])){ ?>
											<a href="../gallery/"><button type="button" class="btn btn-danger">Clear</button></a>
										<?php } ?>
										<button type="submit" class="btn btn-primary">Search</button>												
									</div>
									

								</div>
								
							</div> <!-- end form horiz -->
						
							
						</form>
					</div>
				</div><!-- end well well -->
			</div>
		</div> <!-- end row -->
	</div> <!-- end container -->