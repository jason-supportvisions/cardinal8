$(document).ready(function() {
	
//Door Image Change (Start)
	$(document).on('click', ".DoorInside", function(){
		//Change per section
		var FrontType = 'doors';
		var Input = 'Door';
		var Name = $("#doorSty option:selected").text();
		var ImgID = '#doorStyImg';
		var ImgModal = '#doorStyImgModel';
		var ImgInModal = '#doorStyImgInModal';
		
		// Do Not Change
		var Inside = $(this).attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);

			$(function(){setTimeout(function(){
			$("#lgdrwSty").trigger('change')
			var lgdrwSty = $("#lgdrwSty").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
				
				
				
				
			} else {
				
			}
			$("#drwSty").trigger('change')
			var drwSty = $("#drwSty").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
				
				
			} else {
				
			}
				},300);
			});
	});
	
	$(document).on('click', ".DoorStileRail", function(){
		//Change per section
		var FrontType = 'doors';
		var Input = 'Door';
		var Name = $("#doorSty option:selected").text();
		var ImgID = '#doorStyImg';
		var ImgModal = '#doorStyImgModel';
		var ImgInModal = '#doorStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $(this).attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
		
		$(function(){setTimeout(function(){
			$("#lgdrwSty").trigger('change')
			var lgdrwSty = $("#lgdrwSty").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
				
				
				
				
			} else {
				
			}
			$("#drwSty").trigger('change')
			var drwSty = $("#drwSty").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
				
				
			} else {
				
			}
				},300);
			});
	});
	
	$(document).on('click', ".DoorCenterPanel", function(){
		//Change per section
		var FrontType = 'doors';
		var Input = 'Door';
		var Name = $("#doorSty option:selected").text();
		var ImgID = '#doorStyImg';
		var ImgModal = '#doorStyImgModel';
		var ImgInModal = '#doorStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $(this).attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
		
		$(function(){setTimeout(function(){
			$("#lgdrwSty").trigger('change')
			var lgdrwSty = $("#lgdrwSty").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
				
				
				
				
			} else {
				
			}
			$("#drwSty").trigger('change')
			var drwSty = $("#drwSty").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
				
				
			} else {
				
			}
				},300);
			});
	});
	
	$(document).on('click', ".DoorOutside", function(){
		//Change per section
		var FrontType = 'doors';
		var Input = 'Door';
		var Name = $("#doorSty option:selected").text();
		var ImgID = '#doorStyImg';
		var ImgModal = '#doorStyImgModel';
		var ImgInModal = '#doorStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $(this).attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }

		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
		
		
		$(function(){setTimeout(function(){
			$("#lgdrwSty").trigger('change')
			var lgdrwSty = $("#lgdrwSty").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
				
				
				
				
			} else {
				
			}
			$("#drwSty").trigger('change')
			var drwSty = $("#drwSty").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
				
				
			} else {
				
			}
				},300);
			});
		
		
		
	});					
//Door Image Change (End)
	
//LgDrw Image Change (Start)
	$(document).on('click', ".LgDrwInside", function(){
		var FrontType = 'largedrawers';
		var Input = 'LgDrw';
		var Name = $("#lgdrwSty option:selected").text();
		var ImgID = '#lgdrwStyImg';
		var ImgModal = '#lgdrwStyImgModel';
		var ImgInModal = '#lgdrwStyImgInModal';
		
		// Do Not Change
		var Inside = $(this).attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".LgDrwStileRail", function(){
		//Change per section
		var FrontType = 'largedrawers';
		var Input = 'LgDrw';
		var Name = $("#lgdrwSty option:selected").text();
		var ImgID = '#lgdrwStyImg';
		var ImgModal = '#lgdrwStyImgModel';
		var ImgInModal = '#lgdrwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $(this).attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".LgDrwCenterPanel", function(){
		//Change per section
		var FrontType = 'largedrawers';
		var Input = 'LgDrw';
		var Name = $("#lgdrwSty option:selected").text();
		var ImgID = '#lgdrwStyImg';
		var ImgModal = '#lgdrwStyImgModel';
		var ImgInModal = '#lgdrwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $(this).attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".LgDrwOutside", function(){
		//Change per section
		var FrontType = 'largedrawers';
		var Input = 'LgDrw';
		var Name = $("#lgdrwSty option:selected").text();
		var ImgID = '#lgdrwStyImg';
		var ImgModal = '#lgdrwStyImgModel';
		var ImgInModal = '#lgdrwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $(this).attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});					
//LgDrw Image Change (End)

//Drw Image Change (Start)
	$(document).on('click', ".DrwInside", function(){
		//Change per section
		var FrontType = 'drawers';
		var Input = 'Drw';
		var Name = $("#drwSty option:selected").text();
		var ImgID = '#drwStyImg';
		var ImgModal = '#drwStyImgModel';
		var ImgInModal = '#drwStyImgInModal';
		
		// Do Not Change
		var Inside = $(this).attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".DrwStileRail", function(){
		//Change per section
		var FrontType = 'drawers';
		var Input = 'Drw';
		var Name = $("#drwSty option:selected").text();
		var ImgID = '#drwStyImg';
		var ImgModal = '#drwStyImgModel';
		var ImgInModal = '#drwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $(this).attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".DrwCenterPanel", function(){
		//Change per section
		var FrontType = 'drawers';
		var Input = 'Drw';
		var Name = $("#drwSty option:selected").text();
		var ImgID = '#drwStyImg';
		var ImgModal = '#drwStyImgModel';
		var ImgInModal = '#drwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $(this).attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});
	
	$(document).on('click', ".DrwOutside", function(){
		//Change per section
		var FrontType = 'drawers';
		var Input = 'Drw';
		var Name = $("#drwSty option:selected").text();
		var ImgID = '#drwStyImg';
		var ImgModal = '#drwStyImgModel';
		var ImgInModal = '#drwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $(this).attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';		
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);	
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
	});					
//Drw Image Change (End)

});
