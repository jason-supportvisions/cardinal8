$(window).load(function() {
	//DoorStyle
	var DoorStyle = $("#doorSty option:selected").val();
	if(DoorStyle == ''){	
	} else {
		//Change per section
		var FrontType = 'doors';
		var Input = 'Door';
		var Name = $("#doorSty option:selected").text();
		var ImgID = '#doorStyImg';
		var ImgModal = '#doorStyImgModel';
		var ImgInModal = '#doorStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
			
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);
		$("#doorStyDXF").attr("href", dataStringdxf);
		$("#doorStyPDF").attr("href", dataStringpdf);
		
	
		

	}
	
	
	
	//LgDrwStyle
	//var LgDrwStyle = $("#lgdrwSty option:selected").val();
	var lgdrwSty = $("#lgdrwSty option:selected").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
	} else {
		//Change per section
		var FrontType = 'largedrawers';
		var Input = 'LgDrw';
		var Name = $("#lgdrwSty option:selected").text();
		var ImgID = '#lgdrwStyImg';
		var ImgModal = '#lgdrwStyImgModel';
		var ImgInModal = '#lgdrwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);
		$("#lgdrwStyDXF").attr("href", dataStringdxf);
		$("#lgdrwStyPDF").attr("href", dataStringpdf);
	}
	
	//DrawerStyle
	//var DrwStyle = $("#drwSty option:selected").val();
	var drwSty = $("#drwSty option:selected").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
	} else {
	
	//Change per section
		var FrontType = 'drawers';
		var Input = 'Drw';
		var Name = $("#drwSty option:selected").text();
		var ImgID = '#drwStyImg';
		var ImgModal = '#drwStyImgModel';
		var ImgInModal = '#drwStyImgInModal';
		
		// Do Not Change
		var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
		var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
		var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
		var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
		
		if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
		if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
		if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
		if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
		
		var Code = (Inside + StileRail + CenterPanel + Outside).trim();
		var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
		var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
		var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
		var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
		
		$(ImgID).attr("src", dataStringThumbpng);
		$(ImgModal).attr("src", dataStringpng);
		$(ImgInModal).attr("src", dataStringThumbpng);
		$("#drwStyDXF").attr("href", dataStringdxf);
		$("#drwStyPDF").attr("href", dataStringpdf);
	}
	
	
	// Material/Color/Finish
	var StyleFinish = $("#styMatl option:selected").val();
	if(StyleFinish == ''){	
	} else {
		var FrontstyMatl = $("#styMatl option:selected").text().replace(/\//g, '_');
		var FrontstyColor = $("#styColor option:selected").text().replace(/\//g, '_');
		var FrontstyFinish = $("#styFinish option:selected").text().replace(/\//g, '_');
		//var dataStringpng = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + ' ' + FrontstyFinish + '.jpg';
		
		if (FrontstyColor == "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
			var dataStringjpg = '/images/finish/' + FrontstyMatl + '.jpg';
			$("#styFinishImg").attr("src", dataStringjpg);
		} else if (FrontstyColor != "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
			var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
			$("#styFinishImg").attr("src", dataStringjpg);
		} else if (FrontstyMatl.indexOf("Formica") || FrontstyMatl.indexOf("Wilsonart") || FrontstyFinish == "N/A" || FrontstyFinish == "N_A" || FrontstyFinish.indexOf("Aluminum")){
			var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
			$("#styFinishImg").attr("src", dataStringjpg);
		} else {
			var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + ' ' + FrontstyFinish + '.jpg';
			$("#styFinishImg").attr("src", dataStringjpg);
		}
	}
});