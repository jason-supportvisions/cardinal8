$("a.line-option").click(function () {
   
    var form = $(this).closest('form');
    var input = form.find('input.line-input');
    var new_line = $(this).html();

    input.val(new_line);
    form.submit();

});

$('[data-toggle="tooltip"]').tooltip();


$('#addprodbutton').one("click", function () {
    $('#product_std').DataTable({
        "ordering": true, // Disables control ordering (sorting) abilities
        "processing": true, // Enables control processing indicator.
        "serverSide": true, // Enables control server-side processing mode.
        "pagingType": "simple_numbers", // Pagination button display options
        "autoWidth": false, // Disables automatic column width calculation.
        "responsive": true, // Enables and configure the Responsive extension for table's layout for different screen sizes
        "searching": true, // Enables control search (filtering) abilities
        "searchHighlight": true,
        "lengthChange": true, // Disables control the end user's ability to change the paging display length of the table.
        "Info": true, // Enables control table information display field
        "columnDefs": [ // Column definitions configuration array.
            { "targets": 0, "width": "50px" },
            { "targets": 1, "width": "225px" },
            { "targets": 2, "width": "150px" }
        ],
        "order": [[3, "asc"]],
        "ajax": { // Load data for the table's content from an Ajax source
            url: "/quotes_orders/get_product_std.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function () { $("#product_grid_processing").css("display", "none"); }
        },
        "sDom": '<"top pull-right"l><"top"rt><"bottom col-sm-3"i><"bottom"p><"clear">', // Define the table control elements to appear on the page and in what order
        "_createdRow": function (nRow, aData, iDataIndex) {
            $('td:eq(0)', nRow).append("<a href='/quotes_orders/" + jsJobID + "/products/create?product_item_id=" + aData[12] + "'><button type='button' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Add and Edit'><i class='glyphicon glyphicon-pencil'></i></button></a>");
        },
        get "createdRow"() {
            return this["_createdRow"];
        },
        set "createdRow"(value) {
            this["_createdRow"] = value;
        },
    });

    var Table = $('#product_std').dataTable();

        //Category
            $('#stdcabcat').on('change', function () {
                var Value = $(this).val();
                Table.fnFilter("" + Value + "", 1); //Exact value, column, r
            });

        //Code
            $('#stdcabcode').on('keyup', function () {
                var Value = $(this).val();
                Table.fnFilter("" + Value + "", 2); //Exact value, column, r
            });

        //Description
            $('#stdcabdesc').on('keyup', function () {
                var Value = $(this).val();
                // product_std.fnFilter("" + Value + "", 3); //Exact value, column, r
                // product_std.fnFilter("^" + $Value + "$", 3, false, false); 
                Table
                    .column(3)
                    .search(this.Value)
                    .draw();
            });
    var colIdx = 3;
    $('input', Table.column(colIdx).header()).on('click', function (e) {
        e.stopPropagation();
    });       
});

$('#accessbutton').one("click", function () {
    $('#access_std').DataTable({
        "ordering": false, // Disables control ordering (sorting) abilities 
        "order": [[3, "asc"]],
        "processing": true, // Enables control processing indicator.
        "serverSide": true, // Enables control server-side processing mode.
        "pagingType": "simple_numbers", // Pagination button display options
        "autoWidth": false, // Disables automatic column width calculation.
        "responsive": true, // Enables and configure the Responsive extension for table's layout for different screen sizes
        "searching": true, // Enables control search (filtering) abilities
        "searchHighlight": true,
        "lengthChange": true, // Disables control the end user's ability to change the paging display length of the table.
        "Info": true, // Enables control table information display field
        "columnDefs": [ // Column definitions configuration array.
            { "targets": 0, "width": "50px" },
            { "targets": 1, "width": "225px" },
            { "targets": 2, "width": "150px" }
        ],
        "ajax": { // Load data for the table's content from an Ajax source
            url: "/quotes_orders/get_access_std.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function () { $("#product_grid_processing").css("display", "none"); }
        },
        "sDom": '<"top pull-right"l><"top"rt><"bottom col-sm-3"i><"bottom"p><"clear">', // Define the table control elements to appear on the page and in what order
        "createdRow": function (nRow, aData, iDataIndex) {
            $('td:eq(0)', nRow).append("<a href='/quotes_orders/" + jsJobID + "/products/create?product_item_id=" + aData[12] + "&productType=accessory'><button type='button' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Add and Edit'><i class='glyphicon glyphicon-pencil'></i></button></a>");
        },
    });

    var access_std = $('#access_std').dataTable();

    $('#accesscat').on('change', function () {
        var Value = $(this).val();
        access_std.fnFilter("" + Value + "", 1); //Exact value, column, r
    });

    $('#accesscode').on('keyup', function () {
        var Value = $(this).val();
        access_std.fnFilter("" + Value + "", 2); //Exact value, column, r
    });

    $('#accessdesc').on('keyup', function () {
        var Value = $(this).val();
        access_std.fnFilter("" + Value + "", 3); //Exact value, column, r
    });
});

$('#spectrabutton').one("click", function () {
    $('#spectra_std').DataTable({
        "ordering": true, // Disables control ordering (sorting) abilities 
        "order": [[3, "asc"]],
        "processing": true, // Enables control processing indicator.
        "serverSide": true, // Enables control server-side processing mode.
        "pagingType": "simple_numbers", // Pagination button display options
        "autoWidth": false, // Disables automatic column width calculation.
        "responsive": true, // Enables and configure the Responsive extension for table's layout for different screen sizes
        "searching": true, // Enables control search (filtering) abilities
        "searchHighlight": true,
        "lengthChange": true, // Disables control the end user's ability to change the paging display length of the table.
        "Info": true, // Enables control table information display field
        "columnDefs": [ // Column definitions configuration array.
            { "targets": 0, "width": "50px" },
            { "targets": 1, "width": "225px" },
            { "targets": 2, "width": "150px" }
        ],
        "ajax": { // Load data for the table's content from an Ajax source
            url: "/quotes_orders/get_spectra_std.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function () { $("#product_grid_processing").css("display", "none"); }
        },
        "sDom": '<"top pull-right"l><"top"rt><"bottom col-sm-3"i><"bottom"p><"clear">', // Define the table control elements to appear on the page and in what order
        "createdRow": function (nRow, aData, iDataIndex) {
            $('td:eq(0)', nRow).append("<a href='/quotes_orders/" + jsJobID + "/products/create?product_item_id=" + aData[12] + "&productType=product'><button type='button' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Add and Edit'><i class='glyphicon glyphicon-pencil'></i></button></a>");
        },
    });

    var spectra_std = $('#spectra_std').dataTable();

    $('#spectracat').on('change', function () {
        var Value = $(this).val();
        spectra_std.fnFilter("" + Value + "", 1); //Exact value, column, r
    });

    $('#spectracode').on('keyup', function () {
        var Value = $(this).val();
        spectra_std.fnFilter("" + Value + "", 2); //Exact value, column, r
    });

    $('#spectradesc').on('keyup', function () {
        var Value = $(this).val();
        spectra_std.fnFilter("" + Value + "", 3); //Exact value, column, r 
    });
});

$('#dartmouthbutton').one("click", function () {
    $('#dartmouth_std').DataTable({
        "ordering": true, // Disables control ordering (sorting) abilities 
        "order": [[3, "asc"]],
        "processing": true, // Enables control processing indicator.
        "serverSide": true, // Enables control server-side processing mode.
        "pagingType": "simple_numbers", // Pagination button display options
        "autoWidth": false, // Disables automatic column width calculation.
        "responsive": true, // Enables and configure the Responsive extension for table's layout for different screen sizes
        "searching": true, // Enables control search (filtering) abilities
        "searchHighlight": true,
        "lengthChange": true, // Disables control the end user's ability to change the paging display length of the table.
        "Info": true, // Enables control table information display field
        "columnDefs": [ // Column definitions configuration array.
            { "targets": 0, "width": "50px" },
            { "targets": 1, "width": "225px" },
            { "targets": 2, "width": "150px" }
        ],
        "ajax": { // Load data for the table's content from an Ajax source
            url: "/quotes_orders/get_dartmouth_std.php", // json datasource
            type: "post",  // type of method  ,GET/POST/DELETE
            error: function () { $("#product_grid_processing").css("display", "none"); }
        },
        "sDom": '<"top pull-right"l><"top"rt><"bottom col-sm-3"i><"bottom"p><"clear">', // Define the table control elements to appear on the page and in what order
        "createdRow": function (nRow, aData, iDataIndex) {
            $('td:eq(0)', nRow).append("<a href='/quotes_orders/" + jsJobID + "/products/create?product_item_id=" + aData[12] + "&productType=product'><button type='button' class='btn btn-sm btn-warning' data-toggle='tooltip' title='Add and Edit'><i class='glyphicon glyphicon-pencil'></i></button></a>");
        },
    });

    var dartmouth_std = $('#dartmouth_std').dataTable();

    $('#dartmouthcat').on('change', function () {
        var Value = $(this).val();
        dartmouth_std.fnFilter("" + Value + "", 1); //Exact value, column, r
    });

    $('#dartmouthcode').on('keyup', function () {
        var Value = $(this).val();
        dartmouth_std.fnFilter("" + Value + "", 2); //Exact value, column, r
    });

    $('#dartmouthdesc').on('keyup', function () {
        var Value = $(this).val();
        dartmouth_std.fnFilter("" + Value + "", 3); //Exact value, column, r
    });
});

$(".Price").change(function () {
    if (this.value == "LIST") {
        $(".LIST").removeClass("hide");
        $(".COST").addClass("hide");
        $(".CUSTOMER").addClass("hide");
    }
    if (this.value == "COST") {
        $(".LIST").addClass("hide");
        $(".COST").removeClass("hide");
        $(".CUSTOMER").addClass("hide");
    }
    if (this.value == "CUSTOMER") {
        $(".LIST").addClass("hide");
        $(".COST").addClass("hide");
        $(".CUSTOMER").removeClass("hide");
    }
});

$("#success-alert").fadeTo(10000, 500).slideUp(500, function () {
    $("#success-alert").alert('close');
});