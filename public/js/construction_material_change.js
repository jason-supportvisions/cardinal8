$(document).ready(function() {
	
	$(".styMatl").change(function () {
			var id = $(this).val();
			var dataString = 'Ajax=StyleColor&id=' + id;
			var FrontstyMatl = $("#styMatl option:selected").text();
			var FrontstyColor = 'Natural'
			var FrontstyFinish = $("#styFinish option:selected").text();
			var dataStringjpg = '/images/finish/' + FrontstyMatl.replace(/\//g, '_') + ' ' + FrontstyColor.replace(/\//g, '_') +'.jpg';
			
			$("#styFinishImg").attr("src", dataStringjpg);

			$.ajax({
				type: "POST",
				url: "/quotes_orders/get_construction.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$(".styColor").html(html);
				}
			});
			//$("#styMatlImg").attr("src", dataStringjpg);
			//$("#styFinish").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity,theme: "bootstrap", data: {id:null, text: null}});
			$(function(){setTimeout(function(){
				$(".styColor").trigger('change')
				},300);
			});
		});

		$(".styColor").change(function () {
			var id = $(this).val();
			var materialid = $("#styMatl option:selected").val()
			var dataString = 'Ajax=StyleFinish&id=' + id;
			var FrontstyMatl = $("#styMatl option:selected").text();
			var FrontstyColor = $("#styColor option:selected").text();
			var FrontstyFinish = $("#styFinish option:selected").text();
			var dataStringjpg = '/images/finish/' + FrontstyMatl.replace(/\//g, '_') + ' ' + FrontstyColor.replace(/\//g, '_') + '.jpg';
			
			$("#styFinishImg").attr("src", dataStringjpg);

			$.ajax({
				type: "POST",
				 url: "/quotes_orders/get_construction.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$(".styFinish").html(html);
				}
			});
			//$("#styColorlImg").attr("src", dataStringjpg);
			//$("#styFinish").select();
			//$("#styFinish").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity,theme: "bootstrap", data: {id:null, text: null}});
			$(function(){setTimeout(function(){
				$(".styFinish").trigger('change')
				},300);
			});
		});

		$(".styFinish").change(function () {

			var FrontstyMatl = $("#styMatl option:selected").text().replace(/\//g, '_');
			var FrontstyColor = $("#styColor option:selected").text().replace(/\//g, '_');
			var FrontstyFinish = $("#styFinish option:selected").text().replace(/\//g, '_');
			
			if (FrontstyColor == "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else if (FrontstyColor != "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else if (FrontstyMatl.indexOf("Formica") || FrontstyMatl.indexOf("Wilsonart") || FrontstyFinish == "N/A" || FrontstyFinish == "N_A" || FrontstyFinish.indexOf("Aluminum")){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else {
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + ' ' + FrontstyFinish + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			}
			
			
		var StyleListDoorID = $("#doorSty option:selected").val();
		var StyleListLgDrwID = $("#lgdrwSty option:selected").val();
		var StyleListDrwID = $("#drwSty option:selected").val();
		var StyleListMatl = $("#styMatl option:selected").val();
		var StyleListColor = $("#styColor option:selected").val();
		var StyleListFinish = $("#styFinish option:selected").val();
		var dataStringDoor = 'Ajax=StyleList&Front=Door&Style=' + StyleListDoorID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;
		var dataStringLgDrw = 'Ajax=StyleList&Front=LgDrw&Style=' + StyleListLgDrwID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;
		var dataStringDrw = 'Ajax=StyleList&Front=Drw&Style=' + StyleListDrwID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringDoor,
			cache: false,
			success: function (html) {
				$("#StyleListDoor").html(html);
			}
		});
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringLgDrw,
			cache: false,
			success: function (html) {
				$("#StyleListLgDrw").html(html);
			}
		});
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringDrw,
			cache: false,
			success: function (html) {
				$("#StyleListDrw").html(html);
			}
		});
			
		});
	
});
