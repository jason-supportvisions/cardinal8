$(document).ready(function() {
	
	
function formatLabel (Label) {
	var desc = $(Label.element);
	if(!desc.attr('label') === 'zero'){
		var $Label = $('<span>' + Label.text + '</span><br><span class="small">' + desc.attr('label') + '</span>'); 
	} else {
		var $Label = $('<span>' + Label.text + '</span>');
	}
	return $Label;
};
$("#doorSty").select2({
	templateResult: formatLabel,
	minimumResultsForSearch: Infinity,
	//theme: "bootstrap"
});


function formatMatl (Matl) {
	if (!Matl.id) { return Matl.text; }
	var error = "javascript:this.src='/images/coming_soon.png'";
	var $Matl = $('<span><img src="/images/finish/' + Matl.text.replace(/\//g, '_') + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Matl.text + '</span>');
	return $Matl;
};
$("#styMatl").select2({
	templateResult: formatMatl,
	minimumResultsForSearch: Infinity,
	//theme: "bootstrap"
});

function formatColor (Color) {
	var Material = $("#styMatl option:selected").text().replace(/\//g, '_');
	if (!Color.id) { return Color.text; }
	var error = "javascript:this.src='/images/coming_soon.png'";
	if (Color.text == "Natural"){
		var $Color = $('<span><img src="/images/finish/' + Material + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
	} else {
		var $Color = $('<span><img src="/images/finish/' + Material + ' ' + Color.text.replace(/\//g, '_') + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
	}
	return $Color;
};
$("#styColor").select2({
	templateResult: formatColor,
	minimumResultsForSearch: Infinity,
	//theme: "bootstrap",
  
});

function formatFinish (Finish) {
	var Material = $("#styMatl option:selected").text().replace(/\//g, '_');
	var Color = $("#styColor option:selected").text().replace(/\//g, '_');
	if (!Finish.id) { return Finish.text; }
	var error = "javascript:this.src='/images/coming_soon.png'";
	if (Color == "Natural" && (Finish.text == "15 Sheen" || Finish.text == "40 Sheen" || Finish.text == "15 Sheen (Low VOC)" || Finish.text == "40 Sheen (Low VOC)")){
		var $Finish = $('<span><img src="/images/finish/' + Material + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Finish.text + '</span>');
	} else if (Color != "Natural" && (Finish.text == "15 Sheen" || Finish.text == "40 Sheen" || Finish.text == "15 Sheen (Low VOC)" || Finish.text == "40 Sheen (Low VOC)")){
		var $Finish = $('<span><img src="/images/finish/' + Material + ' ' + Color + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Finish.text + '</span>');
	} else if (Material.includes("Formica") || Material.includes("Wilsonart") || Finish.text == "N/A" || Finish.text == "N_A" || Finish.text.includes("Aluminum")) {
		var $Finish = $('<span><img src="/images/finish/' + Material + ' ' + Color + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Finish.text + '</span>');
	} else {
		var $Finish = $('<span><img src="/images/finish/' + Material + ' ' + Color + ' ' + Finish.text.replace(/\//g, '_') + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Finish.text + '</span>');
	}
	return $Finish;
};
$("#styFinish").select2({
	templateResult: formatFinish,
	minimumResultsForSearch: Infinity,
	//theme: "bootstrap",
  
});

	

	
	//Product Line Selection Change
	$(".brand").change(function () {
		var id = $(this).val();
		var dataString = 'Ajax=StyleGroup&id=' + id;
		var dataStringCabMatl = 'Ajax=CabinetMatl&id=' + id;
		var dataStringCabDrwBox = 'Ajax=CabinetDrwBox&id=' + id;
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataString,
			cache: false,
			success: function (html) {
				$("#styGroup").html(html).select2({placeholder: "Select an option", minimumResultsForSearch: Infinity});
			}
		});

		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringCabMatl,
			cache: false,
			success: function (html) {
				$("#cabMatl").html(html).select2({placeholder: "Select an option", minimumResultsForSearch: Infinity});
			}
		});
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringCabDrwBox,
			cache: false,
			success: function (html) {
				$("#drwBox").html(html).select2({placeholder: "Select an option", minimumResultsForSearch: Infinity});
			}
		});

		//$("#doorSty").html("<option value=\"\">Select (Style Group)</option>");
		//$("#lgdrwSty").html("<option value=\"\">Select (Door Style)</option>");
		//$("#drwSty").html("<option value=\"\">Select (Door Style)</option>");
		//$("#styMatl").html("<option value=\"\">Select (Door Style)</option>");
		//$("#styColor").html("<option value=\"\">Select (Style Material)</option>");
		//$("#styFinish").html("<option value=\"\">Select (Style Color)</option>");
		
		$("#styGroup").html('').select2({placeholder: "Select an option", minimumResultsForSearch: Infinity});
		$("#doorSty").html('').select2({templateResult: formatLabel, placeholder: "Select (Style Group)", minimumResultsForSearch: Infinity});
		$("#lgdrwSty").html('').select2({placeholder: "Select (Door Style)", minimumResultsForSearch: Infinity});
		$("#drwSty").html('').select2({placeholder: "Select (Door Style)", minimumResultsForSearch: Infinity});
		$("#styMatl").html('').select2({placeholder: "Select (Door Style)", minimumResultsForSearch: Infinity});
		$("#styColor").html('').select2({placeholder: "Select (Style Material)", minimumResultsForSearch: Infinity});
		$("#styFinish").html('').select2({placeholder: "Select (Style Color)", minimumResultsForSearch: Infinity});
		
		$("#doorStyImg").attr("src", "/images/no_image.png");
		$("#doorStyImgModel").attr("src", "/images/no_image.png");
		$("#doorStyImgInModal").attr("src", "/images/no_image.png");
		$("#doorStyDXF").attr("href", "#");
		$("#doorStyPDF").attr("href", "#");
		
		$("#lgdrwStyImg").attr("src", "/images/no_image.png");
		$("#lgdrwStyImgModel").attr("src", "/images/no_image.png");
		$("#lgdrwStyImgInModal").attr("src", "/images/no_image.png");
		$("#lgdrwStyDXF").attr("href", "#");
		$("#lgdrwStyPDF").attr("href", "#");
		
		$("#drwStyImg").attr("src", "/images/no_image.png");
		$("#drwStyImgModel").attr("src", "/images/no_image.png");
		$("#drwStyImgInModal").attr("src", "/images/no_image.png");
		$("#drwStyDXF").attr("href", "#");
		$("#drwStyPDF").attr("href", "#");
		
		$("#styFinishImg").attr("src", "/images/no_image.png");
		
	});
	
	//Style Group Selection Change
	$(".styGroup").change(function () {
		var id = $(this).val();
		var brandID = $('input[name=brand]:checked').val();
		var dataString = 'Ajax=DoorStyle&id=' + id + '&brandid=' + brandID;

		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataString,
			cache: false,
			success: function (html) {
				$("#doorSty").html(html).select2({templateResult: formatLabel, placeholder: "Select an option",minimumResultsForSearch: Infinity});
			}
		});
		
		//$("#doorSty").html("<option value=\"\">Select (Door Style)</option>");
		//$("#lgdrwSty").html("<option value=\"\">Select (Door Style)</option>");
		//$("#drwSty").html("<option value=\"\">Select (Door Style)</option>");
		//$("#styMatl").html("<option value=\"\">Select (Door Style)</option>");
		//$("#styColor").html("<option value=\"\">Select (Style Material)</option>");
		//$("#styFinish").html("<option value=\"\">Select (Style Color)</option>");
		
		$("#doorSty").html('').select2({templateResult: formatLabel, placeholder: "Select an option",minimumResultsForSearch: Infinity});		
		$("#lgdrwSty").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity});
		$("#drwSty").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity});
		$("#styMatl").html('').select2({templateResult: formatMatl, placeholder: "Select (Door Style)",minimumResultsForSearch: Infinity});
		$("#styColor").html('').select2({templateResult: formatColor,placeholder: "Select (Style Material)",minimumResultsForSearch: Infinity});
		$("#styFinish").html('').select2({templateResult: formatFinish, placeholder: "Select (Style Color)",minimumResultsForSearch: Infinity});
		
		$("#doorStyImg").attr("src", "/images/no_image.png");
		$("#doorStyImgModel").attr("src", "/images/no_image.png");
		$("#doorStyImgInModal").attr("src", "/images/no_image.png");
		$("#doorStyDXF").attr("href", "#");
		$("#doorStyPDF").attr("href", "#");
		
		$("#lgdrwStyImg").attr("src", "/images/no_image.png");
		$("#lgdrwStyImgModel").attr("src", "/images/no_image.png");
		$("#lgdrwStyImgInModal").attr("src", "/images/no_image.png");
		$("#lgdrwStyDXF").attr("href", "#");
		$("#lgdrwStyPDF").attr("href", "#");
		
		$("#drwStyImg").attr("src", "/images/no_image.png");
		$("#drwStyImgModel").attr("src", "/images/no_image.png");
		$("#drwStyImgInModal").attr("src", "/images/no_image.png");
		$("#drwStyDXF").attr("href", "#");
		$("#drwStyPDF").attr("href", "#");
		
		$("#styFinishImg").attr("src", "/images/no_image.png");
		
		});
	
	//Door Style Selection Change
	$(".doorSty").change(function () {
		var id = $(this).val();
		var dataString = 'Ajax=DrawerStyle&id=' + id;
		var dataStringLg = 'Ajax=LgDrawerStyle&id=' + id;
		var dataStringMatl = 'Ajax=StyleMatl&id=' + id;
		var dataStringDoorSpec = 'Ajax=doorSpec&id=' + id;
		var styGroup = $("#styGroup option:selected").val();

		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataString,
			cache: false,
			success: function (html) {
				$("#drwSty").html(html).select2({placeholder: "Select an option",minimumResultsForSearch: Infinity});
			}
		});
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringLg,
			cache: false,
			success: function (html) {
				$("#lgdrwSty").html(html).select2({placeholder: "Select an option",minimumResultsForSearch: Infinity});
			}
		});
		
		if (styGroup == 11){ 
			//$("#styMatl").html("<option value=\"\">Select (Drawer Style)</option>");
			$("#styMatl").html('').select2({templateResult: formatMatl, placeholder: "Select (Drawer Style)",minimumResultsForSearch: Infinity});
		} else {
			$.ajax({
				type: "POST",
				url: "/quotes_orders/get_construction.php",
				data: dataStringMatl,
				cache: false,
				success: function (html) {
					$("#styMatl").html(html).select2({templateResult: formatMatl, placeholder: "Select an option",minimumResultsForSearch: Infinity});
				}
			});		
		}
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringDoorSpec,
			cache: false,
			success: function (html) {
				$(".doorSpec").html(html);
			}
		});
		
		// Auto Select Images
		$(function(){setTimeout(function(){
			//Change per section
			var FrontType = 'doors';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#doorStyImg';
			var ImgModal = '#doorStyImgModel';
			var ImgInModal = '#doorStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#doorStyDXF").attr("href", dataStringdxf);
			$("#doorStyPDF").attr("href", dataStringpdf);
			
			
			},300);
		});
		
		//$("#styColor").html("<option value=\"\">Select (Material)</option>");
		//$("#styFinish").html("<option value=\"\">Select (Color)</option>");
		$("#styFinishImg").attr("src", "/images/no_image.png");

		$("#styColor").html('').select2({templateResult: formatColor, placeholder: "Select an option",minimumResultsForSearch: Infinity});
		$("#styFinish").html('').select2({templateResult: formatFinish, placeholder: "Select an option",minimumResultsForSearch: Infinity});
		
		
		$(function(){setTimeout(function(){
			$("#lgdrwSty").trigger('change')
			var lgdrwSty = $("#lgdrwSty").text();
			if(lgdrwSty == 'Matching Door'){
				//$("#lgdrwStyImg").attr("src", "/images/no_image.png");
				$("#lgdrwModal").addClass("disabled");
				//$("#lgdrwStyImgModalLink").removeAttr("href");
				
				//Change per section
			var FrontType = 'largedrawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
				
				
				
				
			} else {
				
			}
			$("#drwSty").trigger('change')
			var drwSty = $("#drwSty").text();
			if(drwSty == 'Matching Door'){
				//$("#drwStyImg").attr("src", "/images/no_image.png");
				$("#drwModal").addClass("disabled");
				//$("#drwStyImgModalLink").removeAttr("href");
				
								//Change per section
			var FrontType = 'drawers';
			var Input = 'Door';
			var Name = $("#doorSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
			
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
				
				
			} else {
				
			}
				},300);
			});
		
	});

	$("#lgdrwSty").change(function () {
		var id = $(this).val();
		var dataStringLgDrwSpec = 'Ajax=LgDrwSpec&id=' + id;
		//var dataStringpng = '/images/door/Lg Drawer ' + $("#doorSty option:selected").text() + ' Default.png';
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringLgDrwSpec,
			cache: false,
			success: function (html) {
				$(".lgdrwSpec").html(html);
			}
		});
		
		$(function(){setTimeout(function(){
			//Change per section
			var FrontType = 'largedrawers';
			var Input = 'LgDrw';
			var Name = $("#lgdrwSty option:selected").text();
			var ImgID = '#lgdrwStyImg';
			var ImgModal = '#lgdrwStyImgModel';
			var ImgInModal = '#lgdrwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
			
			if(Name == 'Matching Door'){} else{
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#lgdrwStyDXF").attr("href", dataStringdxf);
			$("#lgdrwStyPDF").attr("href", dataStringpdf);
			$("#lgdrwModal").removeClass("disabled");
			$("#lgdrwStyImgModalLink").attr("href", "#LgDrawerImage");
				}
			},300);
		});
		//$("#styColor").html("<option value=\"\">Select (Material)</option>");
		//$("#styFinish").html("<option value=\"\">Select (Color)</option>");
	});
	
	$("#drwSty").change(function () {
			var id = $(this).val();
			var dataStringDrwSpec = 'Ajax=drwSpec&id=' + id;
			var dataStringMatl = 'Ajax=StyleMatl&id=' + id;
			var styGroup = $("#styGroup option:selected").val();
			$.ajax({
				type: "POST",
				url: "/quotes_orders/get_construction.php",
				data: dataStringDrwSpec,
				cache: false,
				success: function (html) {
					$(".drwSpec").html(html);
				}
			});
			if (styGroup == 11){	
			$.ajax({
				type: "POST",
				url: "/quotes_orders/get_construction.php",
				data: dataStringMatl,
				cache: false,
				success: function (html) {
					$("#styMatl").html(html).select2({templateResult: formatMatl, placeholder: "Select an option",minimumResultsForSearch: Infinity});
				}
			});
			} else {
			}
			
			$(function(){setTimeout(function(){
			//Change per section
			var FrontType = 'drawers';
			var Input = 'Drw';
			var Name = $("#drwSty option:selected").text();
			var ImgID = '#drwStyImg';
			var ImgModal = '#drwStyImgModel';
			var ImgInModal = '#drwStyImgInModal';
			
			// Do Not Change
			var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
			var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
			var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
			var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
			
			if(Inside){ var Inside = " " + Inside; } else { var Inside = ""; }
			if(StileRail){ var StileRail = " " + StileRail; } else { var StileRail = ""; }
			if(CenterPanel){ var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
			if(Outside){ var Outside = " " + Outside; } else { var Outside = ""; }
			
			var Code = (Inside + StileRail + CenterPanel + Outside).trim();
			var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';		
			var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
			var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
			var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
			
			if(Name == 'Matching Door'){} else{
			$(ImgID).attr("src", dataStringThumbpng);
			$(ImgModal).attr("src", dataStringpng);
			$(ImgInModal).attr("src", dataStringThumbpng);
			$("#drwStyDXF").attr("href", dataStringdxf);
			$("#drwStyPDF").attr("href", dataStringpdf);
			$("#drwModal").removeClass("disabled");
			$("#drwStyImgModalLink").attr("href", "#DrawerImage");
			}
			
			
			},300);
		});
		//$("#styColor").html("<option value=\"\">Select (Material)</option>");
		//$("#styFinish").html("<option value=\"\">Select (Color)</option>");
		//$("#styFinishImg").attr("src", "/images/no_image.png");
		
		$("#styColor").html('').select2({templateResult: formatColor, placeholder: "Select (Style Material)",minimumResultsForSearch: Infinity});
		$("#styFinish").html('').select2({templateResult: formatFinish, placeholder: "Select (Style Color)",minimumResultsForSearch: Infinity});
		
	});
	
	
	
	
});
