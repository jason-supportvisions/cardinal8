$(document).ready(function() {
    $(function() {
        $('#datetimepicker1').datetimepicker({
            format: 'DD-MM-YYYY LT'
        });
       

    });
    $(".app_add_btn").click(function() {
        $(".app_table_div").hide();
        $(".app_add_div").show();
    });
    $(".app_add_cancel").click(function() {
        $(".app_table_div").show();
        $(".app_add_div").hide();
    });
});
const realFileBtn = document.getElementById("real-file");
const customBtn = document.getElementById("custom-button");
const customTxt = document.getElementById("custom-text");

customBtn.addEventListener("click", function() {
    realFileBtn.click();
    
});

realFileBtn.addEventListener("change", function() {
    if (realFileBtn.value) {
        customTxt.innerHTML = realFileBtn.value.match(
            /[\/\\]([\w\d\s\.\-\(\)]+)$/
        )[1];
    } else {
        customTxt.innerHTML = "No file";
    }
});
