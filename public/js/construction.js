$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
        
    });

    if(typeof jQuery!=='undefined'){
        console.log('jQuery Loaded');
    }
    else{
        console.log('not loaded yet');
    }



    //=============== Brand =====================

    
        var id                  = $('input[name="brand"]:checked').val(); 
        // var id                  = $(".brand").val(); if(!id){ id = 1; } // default to imperia
        var dataString          = 'Ajax=StyleGroup&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
        var dataStringCabMatl   = 'Ajax=CabinetMatl&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
        var dataStringCabDrwBox = 'Ajax=CabinetDrwBox&id=' + id + '&jobId=' + $('#myJobId').val();
        var dataStringHinges    = 'Ajax=Hinges&id=' + id;
        // console.log("--------------------  CabMaterialString:" + dataStringCabMatl);
        // console.log("--------------------  TOP brandID:" + id);
        // console.log("id:"+id);
        // console.log("dataString:"+dataString);
        // console.log("dataStringCabMatl:"+dataStringCabMatl);
        // console.log("dataStringCabDrwBox:"+dataStringCabDrwBox);
        // console.log("dataStringHinges:"+dataStringHinges);
        // console.log("dataStringCabDrwRunner:"+dataStringCabDrwRunner);

        //Set Cabinet Material
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringCabMatl,
            cache: false,
            success: function (html) {
                $("#cabMatl").html(html).select2({ placeholder: "Select (Cabinet Material)", minimumResultsForSearch: Infinity });
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });
        
        //Set Style Group
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataString,
            cache: false,
            success: function (html) {
                $("#styGroup").html(html).select2({ placeholder: "Select (Style Group)", minimumResultsForSearch: Infinity });
                $(".styGroup").trigger('change');
            },
           error: function(xhr, status, error){
               var errorMessage = xhr.status + ': ' + xhr.statusText
               alert('Error - ' + errorMessage);
           }
        }); 

        //Set Drawer Box
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringCabDrwBox,
            cache: false,
            success: function (html) {
                
                $("#drwBox").html(html).select2({ placeholder: "Select (Drawer Box Style)", minimumResultsForSearch: Infinity });
                loadDrwRunner();

            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });




        //Set Hinges
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringHinges,
            cache: false,
            success: function (html) {
                $("#hinge").html(html).select2({ placeholder: "Select (Hinges)", minimumResultsForSearch: Infinity });
            
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }

 
        });

        

    //hide door buttons unless door selected
    $("#doorButtons").hide();


    $("DoorHardware").click(function() {
        $(this).next().prop("checked", "checked");
    });

    var id                  = $('input[name="brand"]:checked').val(); if(!id){ id = 1; } 
    // var id                  = $(".brand").val(); if(!id){ id = 1; } // default to imperia
    var dataString          = 'Ajax=StyleGroup&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
    var dataStringCabMatl   = 'Ajax=CabinetMatl&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
    var dataStringCabDrwBox = 'Ajax=CabinetDrwBox&id=' + id + '&jobId=' + $('#myJobId').val();
    var dataStringHinges    = 'Ajax=Hinges&id=' + id;
    // console.log("--------------------  CabMaterialString:" + dataStringCabMatl);
 
    // loadSpecImages();

    if(id != 0){ //brand not 0

        // console.log("id!=0");
        var dataStringDoorSpec  = 'Ajax=doorSpec&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
        $("#doorButtons").show();
        // console.log("dataStringDoorSpec:" + dataStringDoorSpec);
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringDoorSpec,
            cache: false,
            success: function (html) {
                $(".doorSpec").html(html);
                // console.log("right bar: doorSpec.html() called");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });

        // loadSpecImages();
        // Build Images for Door Profile Options
                // //Change per section
                // var FrontType = 'doors';
                // var Input = 'Door';
                // var Name = $("#doorSty option:selected").text();
                // var ImgID = '#doorStyImg';
                // var ImgModal = '#doorStyImgModel';
                // var ImgInModal = '#doorStyImgInModal';
                // var ImgOnRight = '#doorStyImgInModal';

                // // Do Not Change
                // var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                // var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                // var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                // var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
                // var Hardware = $('input[name=' + Input + 'Hardware]:checked').attr("code");

                // // console.log("Inside: " + Inside);
                // // console.log("CenterPanel: " + CenterPanel);
                // // console.log("Outside: " + Outside);
                // // console.log("StileRail: " + StileRail);
                // // console.log("Hardware: " + Hardware);

                // if (Inside != undefined) {
                //     $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                //     $("#construction_inside_label").html(Inside.trim());
                // } else {
                //     $("#construction_inside").attr("src", "/images/no_image.png");
                //     $("#construction_inside_label").html("");
                // }

                // if (CenterPanel != undefined) {
                //     $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                //     $("#construction_center_label").html(CenterPanel.trim());
                // } else {
                //     $("#construction_center").attr("src", "/images/no_image.png");
                //     $("#construction_center_label").html("");
                // }

                // if (Outside != undefined) {
                //     $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                //     $("#construction_outside_label").html(Outside.trim());
                // } else {
                //     $("#construction_outside").attr("src", "/images/no_image.png");
                //     $("#construction_outside_label").html("");
                // }

                // if (StileRail != undefined) {
                //     $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                //     $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                // } else {
                //     $("#construction_siterail").attr("src", "/images/no_image.png");
                //     $("#construction_siterail_label").html("");
                // }

                // if (Hardware != undefined) {
                //     $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                //     if(Hardware != "None"){
                //         $("#construction_hardware_label").html( Hardware.trim());
                //     }
                // } else {
                //     $("#construction_hardware").attr("src", "/images/no_image.png");
                //     $("#construction_hardware_label").html("");
                // }


                // //add a space for image file path creation
                // if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                // if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                // if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                // if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }

                // var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                // // ////console.log(' -- Code: ' + Code);

                // var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';
                // var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                // var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                // var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
                // var dataStringProfile = '/images/inside/' + $.trim(Inside) + '.png';

                // $(ImgID).attr("src", dataStringThumbpng);
                // $(ImgModal).attr("src", dataStringpng);
                // $(ImgInModal).attr("src", dataStringThumbpng);
                // $(ImgOnRight).attr("src", dataStringProfile);
                // $("#doorStyDXF").attr("href", dataStringdxf);
                // $("#doorStyPDF").attr("href", dataStringpdf);
    
                // console.log("Inside: " + Inside);
                // console.log("CenterPanel: " + CenterPanel);
                // console.log("Outside: " + Outside);
                // console.log("StileRail: " + StileRail);
                // console.log("Hardware: " + Hardware);
                
                // loadSpecImages();
        
    }else{
        // console.log("No Door Selected Yet");

        //========= Style Group ===========
        var id = $(".brand").val();
        
        var dataString = 'Ajax=StyleGroup&id=' + id + '& jobId=' + $('#myJobId').val();
        // console.log('BrandID = ' + id);
        // console.log('JobID = ' + $('#myJobId').val());

        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataString,
            cache: false,
            success: function (html) {
                $("#styGroup").html(html).select2({ placeholder: "Select (Style Group)", minimumResultsForSearch: Infinity });
                $(".styGroup").trigger('change');
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }

        });

        // loadSpecImages();
    //==========================

    }


    // =============== PDF Spec Button ===============
        $("#doorPDF").click(function () {
            var DoorName = $("#doorSty option:selected").text();
            var FileName = '/downloads/doors/specs/' + DoorName.trim() + '.pdf';
            window.open(FileName, '_blank');
            // ////console.log('STYLE - doorSty() - DoorName: ' + DoorName + " -- FileName: " + FileName);
        });
	// ===============================================


    // ====== Door Gallery Spec Button ===============
    $("#doorAll").click(function () {
        var PageURL = '/gallery/doors';
        window.open(PageURL, '_blank');
    });
	// ===============================================


    //============= Price Sheet ======================
    $('#PriceSheetBtn').click(function () {
        Material = $('#styMatl').val();
        Style = $('#styColor').val();
        Finish = $('#styFinish').val();
        Door = $('#doorSty').val();
        DoorInside = $("input[name='DoorInside']:checked").val();
        DoorOutside = $("input[name='DoorOutside']:checked").val();
        DoorCenter = $("input[name='DoorCenterPanel']:checked").val();
        DoorStile = $("input[name='DoorStileRail']:checked").val();
        DoorHardware = $("input[name='DoorHardware']:checked").val();

        $.post("/quote_orders/ajaxRequest", {
            Ajax: 'PriceSheetModal',
            varMaterial: Material,
            varStyle: Style,
            varFinish: Finish,
            varDoor: Door,
            varInside: DoorInside,
            varOutside: DoorOutside,
            varCenter: DoorCenter,
            varStile: DoorStile,
            varHardware: DoorHardware
        }, function (data) {
            $('#priceModalTable').html(data);
        })

    });
    //============= Price Sheet ======================
    

    //=============== Brand ==========================
    $(".brand").change(function () {
       
        var id                  = $('input[name="brand"]:checked').val(); if(!id){ id = 1; } 
        // var id                  = $(".brand").val(); if(!id){ id = 1; } // default to imperia
        var dataString          = 'Ajax=StyleGroup&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
        var dataStringCabMatl   = 'Ajax=CabinetMatl&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs
        var dataStringCabDrwBox = 'Ajax=CabinetDrwBox&id=' + id + '&jobId=' + $('#myJobId').val();
        var dataStringHinges    = 'Ajax=Hinges&id=' + id;

        //Set Style Group
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataString,
            cache: false,
            success: function (html) {
                $("#styGroup").html(html).select2({ placeholder: "Select (Style Group)", minimumResultsForSearch: Infinity });
                $(".styGroup").trigger('change');
                // console.log('[on-load] #styGroup.html() called. BrandID = ' + id);
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        }); 

        //Set Cabinet Material
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringCabMatl,
            cache: false,
            success: function (html) {
                $("#cabMatl").html(html).select2({ placeholder: "Select (Cabinet Material)", minimumResultsForSearch: Infinity });
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });

        //Set Drawer Box
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringCabDrwBox,
            cache: false,
            success: function (html) {
                $("#drwBox").html(html).select2({ placeholder: "Select (Drawer Box Style)", minimumResultsForSearch: Infinity });
                loadDrwRunner();
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });

        //Set Hinges
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringHinges,
            cache: false,
            success: function (html) {
                $("#hinge").html(html).select2({ placeholder: "Select (Hinges)", minimumResultsForSearch: Infinity });
            
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                alert('Error - ' + errorMessage);
            }
        });

        //Right Bar Images
        ////console.log("[on-load] setting /images/no_image.png");
        // $("#doorStyImg").attr("src", "/images/no_image.png"); //Door
        // $("#drwStyImg").attr("src", "/images/no_image.png"); //Drawer
        // $("#lgdrwStyImg").attr("src", "/images/no_image.png"); //LG Drawer
        // $("#styFinishImg").attr("src", "/images/no_image.png"); //Finish
        // $("#construction_inside").attr("src", "/images/no_image.png"); //Inside Profile
        // $("#construction_center").attr("src", "/images/no_image.png"); //Center Panel
        // $("#construction_outside").attr("src", "/images/no_image.png"); //Outside Profile
        // $("#construction_siterail").attr("src", "/images/no_image.png"); //Stile Rails

        // $("#doorStyImg").attr("src", "/images/no_image.png");
        // $("#doorStyImgModel").attr("src", "/images/no_image.png");
        // $("#doorStyImgInModal").attr("src", "/images/no_image.png");
        // $("#doorStyDXF").attr("href", "#");
        // $("#doorStyPDF").attr("href", "#");

        // $("#lgdrwStyImg").attr("src", "/images/no_image.png");
        // $("#lgdrwStyImgModel").attr("src", "/images/no_image.png");
        // $("#lgdrwStyImgInModal").attr("src", "/images/no_image.png");
        // $("#lgdrwStyDXF").attr("href", "#");
        // $("#lgdrwStyPDF").attr("href", "#");

        // $("#drwStyImg").attr("src", "/images/no_image.png");
        // $("#drwStyImgModel").attr("src", "/images/no_image.png");
        // $("#drwStyImgInModal").attr("src", "/images/no_image.png");
        // $("#drwStyDXF").attr("href", "#");
        // $("#drwStyPDF").attr("href", "#");
        // loadSpecImages();
});
//================= * Brand ==========================



//=============== Style Group =====================

$(".styGroup").change(function () {
    
    // console.log("[onChange] #styGroup called");

    // var id = $("#styGroup").val();
    // var id = $("#styGroup").find('option:selected').val();
    
    var id = $("select#styGroup option:selected").val().trim();
    var groupName = $("#styGroup").find('option:selected').html();
    var brandID = $('input[name=brand]:checked').val(); //get brand
    var jobID = $('#myJobId').val(); 
    var dataString = 'Ajax=DoorStyle&id=' + id +  '&brandid=' + brandID + '&jobId=' + jobID; //build query string
    
    // console.log("------+-------");
    // console.log("groupName: " + typeof groupName);
    // console.log("brand.type: " + typeof brandID);
    // console.log("id.type:  "    + typeof id);
    // console.log("id.jason:  "    + typeof jason);
    // console.log("styGroup Name = " + groupName);
    // console.log("styGroupID = " + id);
    // console.log("brandID = " + brandID);
    // console.log("dataString(1) = " + dataString);
    // console.log("myJobID.val = " + $('#myJobId').val());
    // console.log("jason = " + jason);
    // console.log("-------------");
    // console.log("dataString(2) = " + dataString);
    // console.log("-------------");


    //Set DoorStyles
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataString,
        cache: false,
        success: function (html) {
            $("#doorSty").html(html).select2({placeholder: "Select (Door Style)",minimumResultsForSearch: Infinity});
            $(".doorSty").trigger("change");
            // console.log('[on-load] #doorSty ' + dataString);
            // loadSpecImages();
        },
        error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText
            alert('Error - ' + errorMessage);
        }
    });

    // set placeholders
    $("#lgdrwSty").html('').select2({ placeholder: "Select (Large Drawer Style)", minimumResultsForSearch: Infinity });
    $("#drwSty").html('').select2({ placeholder: "Select (Drawer Style)", minimumResultsForSearch: Infinity });
    $("#styMatl").html('').select2({ templateResult: formatMatl, placeholder: "Select (Style Material)", minimumResultsForSearch: Infinity });
    $("#styColor").html('').select2({ templateResult: formatColor, placeholder: "Select (Style Color)", minimumResultsForSearch: Infinity });
    $("#styFinish").html('').select2({ templateResult: formatFinish, placeholder: "Select (Style Finish)", minimumResultsForSearch: Infinity });
    $("#cabFinEndColor").html('').select2({ placeholder: "Select a door above", minimumResultsForSearch: Infinity });
    
});


//================= * Style Group ==============================


//======================= Door =========================


$(".doorSty").change(function () {
    setTimeout(function () {
        // console.log ("waiting 200ms");
        }, 1000);

    console.log("[onChange] #doorSty called");
    
    // var brandId             = $(".brand").val(); 
    var brandId             = $('input[name="brand"]:checked').val();
    var id                  = $("#doorSty").val();
    // console.log('brandID: ' + brandId);

    var DoorName            = $("#doorSty").find('option:selected').html();
    var DoorName            = $("#doorSty").find('option:selected').html();
    var dataString          = 'Ajax=DrawerStyle&brandId=' + brandId + '&id=' + id + '&DoorName=' + DoorName + '&jobId=' + $('#myJobId').val(); //drawer qs
    var dataStringLg        = 'Ajax=LgDrawerStyle&brandId=' + brandId + '&id=' + id + '&DoorName=' + DoorName + '&jobId=' + $('#myJobId').val(); //large drawer qs
    var dataStringMatl      = 'Ajax=StyleMatl&brandId=' + brandId + '&id=' + id + '&jobId=' + $('#myJobId').val(); //material qs
    var dataStringCabMatl   = 'Ajax=StyleCabMatl&brandId=' + brandId + '&id=' + id + '&jobId=' + $('#myJobId').val(); //material qs
    var dataStringDoorSpec  = 'Ajax=doorSpec&brandId=' + brandId + '&id=' + id + '&jobId=' + $('#myJobId').val(); //door spec qs

    // console.log("#doorSty variables created");
    // console.log('doorStyID: ' + id);
    // console.log('DoorName: ' + DoorName);
    // console.log('dataString: ' + dataString);
    // console.log('dataStringLg: ' + dataStringLg);
    // console.log('dataStringMatl: ' + dataStringMatl);
    // console.log('dataStringDoorSpec: ' + dataStringDoorSpec);

    if(id){ //door is selected        
        $("#doorButtons").show();//show door buttons
    }else{
        $("#doorButtons").hide();
    }

    //Get Drawers
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataString,
        cache: false,
        success: function (html) {
            $("#drwSty").html(html).select2({ placeholder: "Select (Drawer Style)", minimumResultsForSearch: Infinity });
        }
    });

    //Get Large Drawers
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringLg,
        cache: false,
        success: function (html) {
            $("#lgdrwSty").html(html).select2({ placeholder: "Select (Large Drawer Style)", minimumResultsForSearch: Infinity });
        }
    });


    //Get Materials
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringMatl,
        cache: false,
        success: function (html) {
            $("#styMatl").html(html).select2({ templateResult: formatMatl, placeholder: "Select (Material Style)", minimumResultsForSearch: Infinity });
            $('.styMatl').trigger('change');
            $("#cabFinEndMat").html(html).select2({ templateResult: formatMatl, placeholder: "Select a door above", minimumResultsForSearch: Infinity });

        }
    });

    //Door Options Modal
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringDoorSpec,
        cache: false,
        success: function (html) {
            $(".doorSpec").html(html);
            //console.log(" -- ajax_construction: get doorSpec");

        }
    });

    setTimeout(function () {
        // console.log("waiting 200ms");
        //load right side images
        loadSpecImages();
    }, 3000);

});



//======================= Drawers =======================

$("#lgdrwSty").change(function () {
    var id = $(this).val();
    var dataStringLgDrwSpec = 'Ajax=LgDrwSpec&id=' + id;
    // console.log("[onChange] #lgdrwSty called");
    //console.log("lgdrwSty.change.called: ");
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringLgDrwSpec,
        cache: false,
        success: function (html) {
            $(".lgdrwSpec").html(html);
            //console.log (".lgdrwSpec called");
        }
    });

    $(function () {
        setTimeout(function () {
            //Change per section
            var FrontType = 'largedrawers';
            var ImgInput = 'LgDrw'; //get door options instead of drawer options
            var Input = 'Door'; //get door options instead of drawer options
            var Name = $("#lgdrwSty option:selected").text();
            Name = Name.replace(" - SM", "");
            Name = Name.replace(" - LM", "");
            var ImgID = '#lgdrwStyImg';
            var ImgModal = '#lgdrwStyImgModel';
            var ImgInModal = '#lgdrwStyImgInModal';

            // Do Not Change
            var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
            var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
            var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
            var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");

            if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
            if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
            if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
            if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }

            //change file path for DRW - drawers
            if (Name.search("DRW") != -1) {
                var Code = (Outside).trim();
                // console.log("search outcome:" + Name.search("DRW"));
            } else {
                var Code = (Inside + StileRail + CenterPanel + Outside).trim();
            }


            var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' Thumbnail ' + Code + '.png';
            var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.png';
            var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.dxf';
            var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.pdf';
            //console.log("LGdrwStyImg = " + dataStringThumbpng);

            // console.log(' LG Drawer.change called: ' + Input);
            // console.log(' -- Input: ' + Input);
            // console.log(' -- Inside: ' + Inside);
            // console.log(' -- Outside: ' + Outside);
            // console.log(' -- CenterPanel: ' + CenterPanel);
            // console.log(' -- StileRail: ' + StileRail);
            // console.log(' -- Code: ' + Code);
            // console.log(' --  --');
            // console.log(' -- Name: ' + Name);
            // console.log(' -- LgdrwImg: ' + dataStringThumbpng);
            // console.log(' -- ImgInModal: ' + ImgInModal);
            // console.log(' -- ImgModal: ' + ImgModal);


            if (Name.search("DRW") != -1) { 

                //ignore updating images if a slab style drawer

            } else {
                $(ImgID).attr("src", dataStringThumbpng);
                $(ImgModal).attr("src", dataStringpng);
                $(ImgInModal).attr("src", dataStringThumbpng);
                $("#lgdrwStyDXF").attr("href", dataStringdxf);
                $("#lgdrwStyPDF").attr("href", dataStringpdf);
                $("#lgdrwModal").removeClass("disabled");
                $("#lgdrwStyImgModalLink").attr("href", "#LgDrawerImage");
            }
        }, 300);
    });

    //load right side images
    loadSpecImages();
    

});


$("#drwSty").change(function () {
    // console.log("[onChange] #drwSty called");
    var id = $(this).val();
    // console.log("drawer id: " + id);
    var id = $("#doorSty option:selected").val();
    // console.log("door id: " + id);
//because we are using the door's option choices we can avoid lots of recoding by simply passing the door ID instead of the Drawer.
    var dataStringDrwSpec = 'Ajax=drwSpec&id=' + id + '&jobId=' + $('#myJobId').val(); 
    // console.log("jobid: " + $('#myJobId').val());
    var dataStringMatl = 'Ajax=StyleMatl&id=' + id;
    var styGroup = $("#styGroup option:selected").val();
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringDrwSpec,
        cache: false,
        success: function (html) {
            $(".drwSpec").html(html);
        }
    });
    // if (styGroup == 11) {
    //     $.ajax({
    //         type: "POST",
    //         url: "/quote_orders/ajaxRequest",
    //         data: dataStringMatl,
    //         cache: false,
    //         success: function (html) {
    //             $("#styMatl").html(html).select2({ templateResult: formatMatl, placeholder: "Select an option", minimumResultsForSearch: Infinity });
    //         }
    //     });
    // } else {
    // }

    $(function () {
        setTimeout(function () {
            //Change per section
            var FrontType = 'drawers';
            var ImgInput = 'Drw';
            var Input = 'Door'; //get door options instead of drawer options
            var Name = $("#drwSty option:selected").text();
            Name = Name.replace(" - SM", "");
            Name = Name.replace(" - LM", "");
            var ImgID = '#drwStyImg';
            var ImgModal = '#drwStyImgModel';
            var ImgInModal = '#drwStyImgInModal';

            // Do Not Change
            var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
            var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
            var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
            var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");

            if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
            if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
            if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
            if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }

            var Code = (Inside + StileRail + CenterPanel + Outside).trim();


            //change file path for DRW - drawers
            if (Name.search("DRW") != -1) {
                var Code = (Outside).trim();
                // console.log("search outcome drawer:" + Name.search("DRW"));
            } else {
                var Code = (Inside + StileRail + CenterPanel + Outside).trim();
            }

            var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' Thumbnail ' + Code + '.png';
            var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.png';
            var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.dxf';
            var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + ImgInput + ' ' + Name + ' ' + Code + '.pdf';

                // console.log(' Drawer.change called: ' + Input);
                // console.log(' -- ImgInput: ' + ImgInput);
                // console.log(' -- Inside: ' + Inside);
                // console.log(' -- Outside: ' + Outside);
                // console.log(' -- CenterPanel: ' + CenterPanel);
                // console.log(' -- StileRail: ' + StileRail);
                // console.log(' -- Code: ' + Code);
                // console.log(' --  --');
                // console.log(' -- Name: ' + Name);
                // console.log(' -- drwImg: ' + dataStringThumbpng);

            if (Name.search("DRW") != -1)  { 
                //if slab drawer etc just avoid updating image
            } else {
                $(ImgID).attr("src", dataStringThumbpng);
                $(ImgModal).attr("src", dataStringpng);
                $(ImgInModal).attr("src", dataStringThumbpng);
                $("#drwStyDXF").attr("href", dataStringdxf);
                $("#drwStyPDF").attr("href", dataStringpdf);
                $("#drwModal").removeClass("disabled");
                $("#drwStyImgModalLink").attr("href", "#DrawerImage");
            }


        }, 3000);
    });

    //load right side images
    loadSpecImages();

    //$("#styColor").html("<option value=\"\">Select (Material)</option>");
    //$("#styFinish").html("<option value=\"\">Select (Color)</option>");
    //$("#styFinishImg").attr("src", "/images/no_image.png");

    // $("#styColor").html('').select2({ templateResult: formatColor, placeholder: "Select (Style Material)", minimumResultsForSearch: Infinity });
    // $("#styFinish").html('').select2({ templateResult: formatFinish, placeholder: "Select (Style Color)", minimumResultsForSearch: Infinity });

});


$("#drwBox").change(function () {
    loadDrwRunner();
});



//=============== Style  Material =====================

$(".styMatl").change(function () {
    // ////console.log("#styMatl.change() called");
    // console.log("[onChange] #styMatl called");

    var id              = $(this).find('option:selected').val();
    var MaterialName    = $(this).find('option:selected').html();
    var brandId         = $('input[name="brand"]:checked').val();
    var DoorName        = $("#doorSty").find('option:selected').html();
    var dataStringColor = 'Ajax=StyleColor&brandId=' + brandId + '&id=' + id + '&jobId=' + $('#myJobId').val(); //Color qs
    // console.log("brandid: " + id);
    ////console.log("dataStringColor: " + dataStringColor);

    if(id == null){
        // console.log('found null ID: ' + id );
    }
    if (MaterialName == null){
        // console.log('found null MaterialName: ' + MaterialName);
    }

    // Set Material to above choice
    if(id == 0 || id == null || MaterialName == null){
        $('#cabFinEndMat').html('<option value="0" selected placeholder="Select Material Above"></option>');
    //     ////console.log('cabFinEndMat.html - called because material NOT set above');
    }else{
        $('#cabFinEndMat').html('<option value="' + id + '" selected>' + MaterialName + '</option>');
    //     ////console.log('cabFinEndMat.html - material selected');
    }

    //Set Color
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringColor,
        cache: false,
        success: function (html) {
            $("#styColor").html(html).select2({ templateResult: formatColor, placeholder: "Select (Style Color)", minimumResultsForSearch: Infinity });
            $('.styColor').trigger('change');
            // ////console.log(" -- ajax_construction: get color");

        }
    });

    //load right side images
    loadSpecImages();

});
// ===============================================




//=============== Style  Color =====================

$(".styColor").change(function () {
    // console.log("#styColor called");

    var id = $("#styColor").val();
    var ColorName = $("#styColor").find('option:selected').html();
    var ColorID = $("#styColor").find('option:selected').val();
    var DoorName = $("#doorSty").find('option:selected').html();
    var MaterialID = $("#styMatl").find('option:selected').val();
    var dataStringFinish = 'Ajax=StyleFinish&id=' + ColorID + '&jobId=' + $('#myJobId').val(); //Color qs
    var dataStringEdge = 'Ajax=CabinetEdge&id=' + ColorID + '&MaterialID=' + MaterialID +'&jobId=' + $('#myJobId').val(); //Color qs

    // console.log('dataStringEdge = ' + dataStringEdge);

    //set Cabinet Finish color to match the color chosen in styColor
    if(ColorName != undefined || ColorID != undefined){
        // ////console.log('ColorName fail: ' + ColorName);
        // ////console.log('ColorID fail: ' + ColorID);
        $('#cabFinEndColor').html('<option value="' + ColorID +'" selected>'+ColorName+'</option>');
    }else{
        // ////console.log('ColorName: ' + ColorName);
        // ////console.log('ColorID: ' + ColorID);
        // $('#cabFinEndColor').html('<option>asdf</option>');

    }

    //Set Finish
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringFinish,
        cache: false,
        success: function (html) {
            $("#styFinish").html(html).select2({ templateResult: formatFinish, placeholder: "Select (Style Finish)", minimumResultsForSearch: Infinity });
            $('.styFinish').trigger('change');
            // ////console.log(" -- ajax_construction: get finish");

        }
    });


    //Set EdgeBanding
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringEdge,
        cache: false,
        success: function (html) {
            $("#cabFinEndEdge").html(html).select2({ templateResult: formatLabel, placeholder: "Select (Edge Banding)", minimumResultsForSearch: Infinity });
            $('.cabFinEndEdge').trigger('change');
            // console.log(" -- ajax_construction: get edge banding");

        }
    });

    

    //============ Stella - HG rule  =============
    if (DoorName == 'Stella - HG') {
        // console.log('styMatl (Door) RULE Activated - DoorName: ' + DoorName + ', ID: ' + id);

        /*



        Finished End Material = Paints – Solvent Based [19]
        Finished End Color =       Custom Paint – SB [1973]
        Finished End Finish =      40 Sheen Paint – SB [87]
        Edge Banding =                 Default – See Header Notes [13] = $0.00,   Opt 1  Paintable PVC = $175


        If Door Style =              Stella – HG,
            And if                	Material = Gloss Lacquer (this is always true)
            Then                  	Finished End Material = Formica – Solid Colors – Gloss
            
        */
        $('#cabFinEndMat').find('option').remove();
        $('#cabFinEndMat').html('<option value="233" selected>Formica – Solid Colors – Gloss</option><option value="19" selected>Paints – Solvent Based</option>');
        $('#cabFinEndMat').trigger('change');

        $('#cabFinEndColor').find('option').remove();
        $('#cabFinEndColor').html('<option value="1973" selected>Custom Paint – SB</option>');
        $('#cabFinEndColor').trigger('change');

        $('#cabFinEndFin').find('option').remove();
        $('#cabFinEndFin').html('<option value="87" selected>40 Sheen Paint – SB</option>');
        $('#cabFinEndFin').trigger('change');

        $('#cabFinEndEdge').find('option').remove();
        $('#cabFinEndEdge').html('<option value="13" selected>See Header Notes</option><option value="2" selected>Paintable PVC</option>');
        $('#cabFinEndEdge').trigger('change');

        /*
        
        
        My understanding is that the unique finished end materials are now hard coded – so I guess this will need to be added to the list –

        If door is                              Stella HG
        Then, If Color =                  *Custom Paint – HG Doors [1998]
        Then, if Finish =                 90 Sheen Drs – 40 Sheen Pts [95]

        Then

        */

        /*

        To determine Finished End Color
            Then if              	Color/Pattern = F-___ - ( color name ) – Match
            Then                  	Finished End Color = F-___-90 – ( color name ) - GEB

        */


            switch (ColorID) {
                case "1998":
                    FinishEndName = "90 Sheen Drs - 40 Sheen Pts";
                    FinishEndID = 95;
                    // ////console.log("styColor (Color) RULE Activated - " + FinishEndName + " / " + FinishEndID);
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    break;
                case "1587":
                    FinishEndName = "F-459-90 - Brite White - GEB";
                    FinishEndID = 1602;
                    // ////console.log("styColor (Color) RULE Activated - " + FinishEndName + " / " + FinishEndID);
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    break;
                case "1588":
                    FinishEndName = "F-463-90 - Sail White - GEB";
                    FinishEndID = 622;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1589":
                    FinishEndName = "F-7197-90 - Dover White - GEB";
                    FinishEndID = 1603;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1590":
                    FinishEndName = "F-837-90 - Graphite - GEB";
                    FinishEndID = 1604;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1591":
                    FinishEndName = "F-845-90 - Spectrum Red - GEB";
                    FinishEndID = 745;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1592":
                    FinishEndName = "F-902-90 - Platinum - GEB";
                    FinishEndID = 780;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1023":
                    FinishEndName = "F-909-90 - Black - GEB";
                    FinishEndID = 1605;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1593":
                    FinishEndName = "F-912-90 - Storm - GEB";
                    FinishEndID = 1606;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1594":
                    FinishEndName = "F-923-90 - Surf - GEB";
                    FinishEndID = 788;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1595":
                    FinishEndName = "F-927-90 - Folkstone - GEB";
                    FinishEndID = 1977;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "564":
                    FinishEndName = "F-949-90 - White - GEB";
                    FinishEndID = 1607;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1597":
                    FinishEndName = "F-961-90 - Fog - GEB";
                    FinishEndID = 1608;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                
            }// end switch



    } else {
        // console.log('No STELLA Door rule located');
    }
    // ==================== END Stella RULES ===========================

    //============ Lexington rule -- to be replaced by rules engine =============

    if (DoorName == 'Lexington' || DoorName == 'Lexington - 90') {

        // ////console.log('styColor (Door) RULE Activated: DoorName: ' + DoorName + ', ID: ' + id + ', ColorName: ' + ColorName);

        if (MaterialID == 129) { 
            // ////console.log('styColor (Material) RULE Activated - MaterialID 129: ' + MaterialID);

            switch (ColorID) {
                case "1678":
                    FinishEndName = "Formica 8830-90 – Elemental Concrete";
                    FinishEndID = 1991;
                    // ////console.log("styColor (Color) RULE Activated - " + FinishEndName + " / " + FinishEndID);
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    break;
                case "1679":
                    FinishEndName = "F-837-58 - Graphite";
                    FinishEndID = 743;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1680":
                    FinishEndName = "Wilsonart 4796-60 – Burnished Chestnut";
                    FinishEndID = 1993;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1681":
                    FinishEndName = "Formica 3690-58 – Basalt Slate";
                    FinishEndID = 1994;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
            }// end switch

//Door Ashfield Thumbnail OGEE 2323 P-RGC38 SR.png
//http://snapielegacy.corymfg.com/images/doors/Ashfield/Door Ashfield Thumbnail OGE 2323 P-RGC38 SR.png

        }

        // If Material = 128   OR  130 
        // LPL(TFL) - BRI - Patterns Gloss
        if (MaterialID == 130 || MaterialID == 128) {
            // ////console.log('styColor (Material) RULE Activated - MaterialID 128 or 130: ' + MaterialID);
            // ////console.log("look for colors that match ColorID: " + ColorID);

            switch (ColorID) {
                case "1660":
                    FinishEndName = "Formica 934-90 - Pearl";
                    FinishEndID = 1980;
                    // ////console.log("styColor (Color) RULE Activated - " + FinishEndName + " / " + FinishEndID);
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    break;
                case "1071":
                    FinishEndName = "Formica 932-90 – Antique White";
                    FinishEndID = 1981;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "566":
                    FinishEndName = "Formica 858-90 - Pumice";
                    FinishEndID = 1982;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1661":
                    FinishEndName = "Formica 927-90 - Folkstone";
                    FinishEndID = 1983;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1662":
                    FinishEndName = "Formica 5342-90 - Earth";
                    FinishEndID = 1984;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1663":
                    FinishEndName = "Formica 2297-90 - Terril";
                    FinishEndID = 0;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1664":
                    FinishEndName = "Formica 839-90 – Stop Red";
                    FinishEndID = 0;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1072":
                    FinishEndName = "Formica 909-90 - Black";
                    FinishEndID = 0;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1666":
                    FinishEndName = "Formica 858-90 - Pumice";
                    FinishEndID = 1982;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1667":
                    FinishEndName = "Formica 912-90 - Storm";
                    FinishEndID = 1988;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1668":
                    FinishEndName = "Wilsonart 4861K-01 – Gold Alchemy";
                    FinishEndID = 1989;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1669":
                    FinishEndName = "Formica 7812-90 – MDF Solidz";
                    FinishEndID = 1990;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1670":
                    FinishEndName = "Formica 8830-90 – Elemental Concrete";
                    FinishEndID = 1991;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1979":
                    FinishEndName = "Formica 837-90 - Graphite";
                    FinishEndID = 1992;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1671":
                    FinishEndName = "Wilsonart 4796-01 – Burnished Chestnut";
                    FinishEndID = 1993;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1672":
                    FinishEndName = "Formica 3690-90 – Basalt Slate";
                    FinishEndID = 1994;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1673":
                    FinishEndName = "Formica 5481-90 – Oiled Olivewood";
                    FinishEndID = 1995;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1674":
                    FinishEndName = "Formica 6209-90 – Prestige Walnut";
                    FinishEndID = 1996;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1675":
                    FinishEndName = "Formica 6209-90 – Prestige Walnut";
                    FinishEndID = 1996;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1676":
                    FinishEndName = "Formica 6414-90 – Black Riftwood";
                    FinishEndID = 1997;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                case "1677":
                    FinishEndName = "Formica 837-90 – Graphite";
                    FinishEndID = 1992;
                    $('#cabFinEndColor').find('option').remove(); //remove all options from select
                    $('#cabFinEndColor').html('<option value="' + FinishEndID + '" selected>' + FinishEndName + '</option>');
                    $('#cabFinEndColor').trigger('change');
                    // ////console.log("styColor (Color) RULE Activated - - " + FinishEndName + "/" + FinishEndID);
                    break;
                default:
                    // ////console.log("No color conditions met");
            }
            // CabFinEndColorID = $('#cabFinEndColor').val();
            // ////console.log("CabFinEndColorID: " + CabFinEndColorID);

            

        }//material if
        // ////console.log('RULE Activated - ColorName: ' + ColorName);

    } else {
        // ////console.log('No Lexington : ' + DoorName + ', ID: ' + id);
    }
    // ================== END Lexington Rules ==============================

    //load right side images
    loadSpecImages();

});
// ===============================================




// ================== Style Finish ===================

$(".styFinish").change(function () {


    var DoorName = $("#doorSty").find('option:selected').html();
    var DoorID = $("#doorSty").find('option:selected').val();

    var FinishName = $("#styFinish").find('option:selected').html();
    var FinishID = $("#styFinish").find('option:selected').val();

    // ////console.log("styFinish -- FinishName: " + FinishName);
    
    var FrontstyMatl = $("#styMatl option:selected").text().replace(/\//g, '_');
    var FrontstyColor = $("#styColor option:selected").text().replace(/\//g, '_');
    var FrontstyFinish = $("#styFinish option:selected").text().replace(/\//g, '_');


    var dataStringDrw = 'Ajax=CabinetFinishedEndFinish&jobStyleColor=' + StyleListColor + '&jobStyleFinish=' + StyleListFinish + '&jobId=' + $('#myJobId').val();

    //JAy - reimplement in proper order as to not overwrite CabFinEndFin
    
    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringDrw,
        cache: false,
        success: function (html) {
            $(".cabFinEndFin").html(html);
        }
    });
    var dataStringDrw = 'Ajax=CabinetBoxEdge&jobStyleFinish=' + StyleListFinish;

    $.ajax({
        type: "POST",
        url: "/quote_orders/ajaxRequest",
        data: dataStringDrw,
        cache: false,
        success: function (html) {
            $(".cabFinEndEdge").html(html);
        }
    });

    /*




                To determine Finished End Finish
                    Then if              	Finish = 90 Sheen – Hand Polished
                    Then                  	Finished End Finish = F – High Gloss – 90
                */
    
    if (DoorName == 'Stella - HG'){
        $('#cabFinEndFin').find('option').remove();
        $('#cabFinEndFin').html('<option value="63" selected>F – High Gloss – 90</option>');
        $('#cabFinEndFin').trigger('change');
        // console.log('FrontstyMatl ' + FrontstyMatl);
    }





    // $("#styFinish").find('option:selected').html();

//only do this if a rule hasn't been activated above for finish
// if($RuleFinish != "yes"){
//     //set Cabinet Finish color to match the color chosen in styColor
    if (FinishName != undefined || FinishID != undefined) {
//         // ////console.log('ColorName fail: ' + ColorName);
//         // ////console.log('ColorID fail: ' + ColorID);
        $('#cabFinEndFin').html('<option value="'+FinishID+'" selected>'+FinishName+'</option>');
    } else {
//         // ////console.log('ColorName: ' + ColorName);
//         // ////console.log('ColorID: ' + ColorID);
//         // $('#cabFinEndColor').html('<option>asdf</option>');

    }
//     //set Cabinet Finish to match choice above
// }
    
//Natural
    if (FrontstyColor.indexOf("Natural") && 
        (FrontstyFinish == "15 Sheen Paint - SB" || 
        FrontstyFinish == "40 Sheen Paint - SB" || 
        FrontstyFinish == "15 Sheen Paint - WB - Low VOC" || 
        FrontstyFinish == "40 Sheen Paint - WB - Low VOC")) {
        var dataStringjpg = '/images/finish/' + FrontstyMatl + '.jpg';
        $("#styFinishImg").attr("src", dataStringjpg);
        // ////console.log("styFinish: NATURAL: " + dataStringjpg);

    //NOT Natural
    } else if (!FrontstyColor.indexOf("Natural") && 
        (FrontstyFinish == "15 Sheen Paint - SB" ||
        FrontstyFinish == "40 Sheen Paint - SB" ||
        FrontstyFinish == "15 Sheen Paint - WB - Low VOC" ||
        FrontstyFinish == "40 Sheen Paint - WB - Low VOC")) {
        var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
        $("#styFinishImg").attr("src", dataStringjpg);
        // ////console.log("styFinish: NOT NATURAL: " + dataStringjpg);

    
    //Formica / WilsonArt
    } else if (FrontstyMatl.indexOf("Formica") || 
        FrontstyMatl.indexOf("Wilsonart") || 
        FrontstyFinish == "N/A" || 
        FrontstyFinish == "N_A" || 
        FrontstyFinish.indexOf("Aluminum")) {
        var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
        $("#styFinishImg").attr("src", dataStringjpg);
        // ////console.log("styFinish: FORMICA: " + dataStringjpg);

    
    //Else
    } else {
        var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + ' ' + FrontstyFinish + '.jpg';
        $("#styFinishImg").attr("src", dataStringjpg);
        // ////console.log("styFinish: ELSE: " + dataStringjpg);

    }
    
    $("#styFinishImg").attr("title", dataStringjpg);//set tooltip


    var StyleListDoorID     = $("#doorSty option:selected").val();
    var StyleListLgDrwID    = $("#lgdrwSty option:selected").val();
    var StyleListDrwID      = $("#drwSty option:selected").val();
    var StyleListMatl       = $("#styMatl option:selected").val();
    var StyleListColor      = $("#styColor option:selected").val();
    var StyleListFinish     = $("#styFinish option:selected").val();
    // var dataStringDoor      = 'Ajax=StyleList&Front=Door&Style=' + StyleListDoorID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;
    // var dataStringLgDrw     = 'Ajax=StyleList&Front=LgDrw&Style=' + StyleListLgDrwID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;
    // var dataStringDrw       = 'Ajax=StyleList&Front=Drw&Style=' + StyleListDrwID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish;

    // $.ajax({
    //     type: "POST",
    //     url: "/quote_orders/ajaxRequest",
    //     data: dataStringDoor,
    //     cache: false,
    //     success: function (html) {
    //         $("#StyleListDoor").html(html);
    //     }
    // });
    // $.ajax({
    //     type: "POST",
    //     url: "/quote_orders/ajaxRequest",
    //     data: dataStringLgDrw,
    //     cache: false,
    //     success: function (html) {
    //         $("#StyleListLgDrw").html(html);
    //     }
    // });
    // $.ajax({
    //     type: "POST",
    //     url: "/quote_orders/ajaxRequest",
    //     data: dataStringDrw,
    //     cache: false,
    //     success: function (html) {
    //         $("#StyleListDrw").html(html);
    //     }
    // });




    var FinishName = $("#styFinish").find('option:selected').html();
    // ////console.log("FinishName: " + FinishName);

    if (FinishName == "High Gloss - BRI") {
        // ////console.log('styFinish (Finish) RULE Activated: ' + FinishName);
        $('#cabFinEndFin').find('option').remove();
        $('#cabFinEndFin').html('<option value="88" selected>High Gloss Laminate</option>');
        $('#cabFinEndFin').trigger('change');
        $RuleFinish = "yes";
    } else {
        // ////console.log('styColor (Finish) RULE --NOT-- Activated: ' + FinishName);
    }

});

});
// end ============== Style Finish =======================






    //Functions =======================================
    
    function formatLabel(Label) {
        var desc = $(Label.element);
        if (!desc.attr('label') === 'zero') {
            var Label = $('<span style="label">' + Label.text + '</span><br><span class="small">' + desc.attr('label') + '</span>');
        } else {
            var Label = $('<span style="nolabel">' + Label.text + '</span>');
        }
        ////console.log("Label called");
        return Label;
    };

    function formatMatl(Matl) {
        if (!Matl.id) { return Matl.text; }
        var error = "javascript:this.src='/images/coming_soon.png'";
        var $Matl = $('<span><img src="/images/finish/' + Matl.text.replace(/\//g, '_') + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Matl.text + '</span>');
        // ////console.log("Material: " + Matl.text);
        return $Matl;
    };

    function formatColor(Color) {
        var Material = $("#styMatl option:selected").text().replace(/\//g, '_');
        if (!Color.id) { return Color.text; }
        var error = "javascript:this.src='/images/coming_soon.png'";
        if (Color.text == "Natural" || Color.text == "Natural - SB" || Color.text == "Natural - WB") {
            var $Color = $('<span><img src="/images/finish/' + Material + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
        } else if (Color.text == "Standard" || Color.text == "Standard - FFG") { //for glass modal
            var $Color = $('<span><img src="/images/finish/' + Material + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
        } else {
            var $Color = $('<span><img src="/images/finish/' + Material + ' ' + Color.text.replace(/\//g, '_') + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
        }
        // console.log($Color);
        return $Color;
    };

    function formatDrawerBox(Color) {
        var Material = $("#styMatl option:selected").text().replace(/\//g, '_');
        if (!Color.id) { return Color.text; }
        var error = "javascript:this.src='/images/coming_soon.png'";
        var $Color = $('<span><img src="/images/finish/' + Color.text + '.jpg" width="50" height="50" onerror=' + error + ' /> ' + Color.text + '</span>');
        return $Color;
    };

    $("#drwBox").select2({
        templateResult: formatDrawerBox,
        minimumResultsForSearch: Infinity
    });

    function formatFinish(Finish) {
        var Material = $("#styMatl option:selected").text().replace(/\//g, '_');
        var Color = $("#styColor option:selected").text().replace(/\//g, '_');
        var $Finish = $('<span>' + Finish.text + '</span>');
        return $Finish;
    };

    $("#styFinish").select2({
        templateResult: formatFinish,
        minimumResultsForSearch: Infinity
    });

    //Right Bar Images
    function loadSpecImages(){

        setTimeout(function () {
            console.log("waiting 1000ms");
        // }, 1000);
    
        // Build Images for Door Options
        // $(function () {
            // setTimeout(function () {
                
                // <input type="radio" class="imagebtn DoorOutside" 
                //name="DoorOutside" code="VBE" id="Door_VBE" 
                //value="11" checked="" style="customer">
                
                // var DoorInside, DoorOutside, DoorCenter, DoorHardware, DoorStile;
                // var DoorInside = $("input[name='DoorInside']:checked").val();
                // var DoorOutside = $("input[name='DoorOutside']:checked").val();
                // var DoorInside = document.querySelector('[name="DoorInside"]');
                
                // var DoorOutside = $("input[name='DoorOutside']").text();

                // var DoorOutside     = document.querySelector('[name="DoorOutside"]').id();
                // // var DoorOutside = document.querySelector('.Door_VBE');
                
                // // console.log(DoorOutside);

                Input = "Door";
                // Do Not Change
                var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
                var Hardware = $('input[name=' + Input + 'Hardware]:checked').attr("code");
                
                // //'input[name="pwd"]'
                // var DoorCenter = $("input[name='DoorCenterPanel']:checked").val();
                // var DoorCenter = document.querySelector('input[code="DoorCenterPanel"]');
                // var DoorStile = $("input[name='DoorStileRail']:checked").val();
                // var DoorStile = document.querySelector('input[code="DoorStileRail"]');
                // var DoorHardware = $("input[name='DoorHardware']:checked").val();
                // var DoorHardware = document.querySelector('input[code="DoorHardware"]');
                // var doorName = document.querySelector('input[code="DoorHardware"]').id;
                var doorStyID = document.querySelector('.doorSty');
                // var doorStyID = document.querySelector('.Door_VBE');
                // console.log('Door selectValue ' + doorStyID.id);
                // console.group("loadSpecImages("+ Code +")");        

                // console.log('Doors ----- ');
                // console.log('door style ' +  doorStyID);
                // console.log(' -- DoorInside: ' + Inside);
                // console.log(' -- DoorOutside: ' + Outside);
                // console.log(' -- DoorCenter: ' + CenterPanel);
                // console.log(' -- DoorStile: ' + StileRail);
                // console.log(' -- DoorHardware: ' + Hardware);


                // ////console.log('------ ');
                // console.log('Building Door');
                // var FormData = $('form').serialize();

                // var formData = new FormData(document.querySelector('form'));
                // console.log('FormData ' + FormData);
                // console.log('formData ' + formData);

                //Change per section
                var FrontType = 'doors';
                var Input = 'Door';
                var Name = $(".doorSty option:selected").text();
                var DrawerName = $("#drwSty option:selected").text();
                var ImgID = '#doorStyImg';
                var ImgModal = '#doorStyImgModel';
                var ImgInModal = '#doorStyImgInModal';
                var ImgOnRight = "#construction_inside";
                
                // console.log('FrontType: ' +FrontType);
                // console.log('Input: ' +Input);
                // console.log('Name: ' +Name);
                // console.log('DrawerName: ' +DrawerName);
                // console.log('ImgID: ' +ImgID);
                // console.log('ImgModal: ' +ImgModal);
                // console.log('ImgInModal: ' +ImgInModal);
                // console.log('ImgOnRight: ' +ImgOnRight);
                
                $("#construction_door_label").html(Name);
    
                // Do Not Change
                var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
                var Hardware = $('input[name=' + Input + 'Hardware]:checked').attr("code");
    
    
                $("#construction_inside_title").html("Inside Profile");
                $("#construction_center_title").html("Center Panel");
                $("#construction_outside_title").html("Outside Profile");
                $("#construction_siterail_title").html("Stile/Rails");
                $("#construction_hardware_title").html("Hardware");
                $("#construction_door_title").html("Door");
                $("#construction_drawer_title").html("SM Drawer");
                $("#construction_lgdrawer_title").html("LG Drawer");
    
    
    
                if (Inside != undefined) {
                    $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                    $("#construction_inside_label").html(Inside.trim());
                    // ////console.log("Inside: /images/inside/" + Inside.trim() + ".png");
                } else {
                    $("#construction_inside").attr("src", "/images/no_image_inside.png");
                    $("#construction_inside_label").html("");
                    // ////console.log("Inside: /images/no_image.png");
                }
    
                if (CenterPanel != undefined) {
                    $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                    $("#construction_center_label").html(CenterPanel.trim());
                    // ////console.log("Center: /images/centerpanel/" + CenterPanel.trim() + ".png");
                } else {
                    $("#construction_center").attr("src", "/images/no_image_center.png");
                    $("#construction_center_label").html("");
                    // ////console.log("Center: /images/no_image.png");
                }
    
                if (Outside != undefined) {
                    $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                    $("#construction_outside_label").html(Outside.trim());
                    // ////console.log("Outside: /images/outside/" + Outside.trim() + ".png");
                } else {
                    $("#construction_outside").attr("src", "/images/no_image_outside.png");
                    $("#construction_outside_label").html("");
                    // ////console.log("Outside: /images/no_image.png");
                }
    
                if (StileRail != undefined) {
                    $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                    $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                    // ////console.log("StileRail: /images/stilerail/" + StileRail.trim() + ".png");
                } else {
                    $("#construction_siterail").attr("src", "/images/no_image_stile.png");
                    $("#construction_siterail_label").html("");
                    // ////console.log("StileRail: /images/no_image.png");
                }
    
                if (Hardware != undefined) {
                    $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                    if (Hardware != "None"){
                        $("#construction_hardware_label").html(Hardware.trim());
                    }
                } else {
                    $("#construction_hardware").attr("src", "/images/no_image.png");
                    $("#construction_hardware_label").html("");
                }
    
                
    
                //add a space for image file path creation
                if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
                
                // //console.log('Doors --2--- ');
                // console.log(' -- Inside: ' + Inside);
                // console.log(' -- Outside: ' + Outside);
                // console.log(' -- CenterPanel: ' + CenterPanel);
                // console.log(' -- StileRail: ' + StileRail);
                
                var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';
                var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
    
    
                $(ImgID).attr("src", dataStringThumbpng);
                $(ImgModal).attr("src", dataStringpng);
                $(ImgInModal).attr("src", dataStringThumbpng);
                $("#doorStyDXF").attr("href", dataStringdxf);
                $("#doorStyPDF").attr("href", dataStringpdf);
    
                //update both drawers if door options are changed
                var dataStringDrwThumbpng = '/images/drawers/' + Name + '/' + 'Drw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                var dataStringLgDrwThumbpng = '/images/largedrawers/' + Name + '/' + 'LgDrw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                
                if (DrawerName.search("DRW") != -1) {
                    // console.log("DRW found in name - ignore updating the drawer images");
                } else {
                    $("#drwStyImg").attr("src", dataStringDrwThumbpng);
                    $("#lgdrwStyImg").attr("src", dataStringLgDrwThumbpng);
                }
    
                //console.log(' -- Door-ImgID: ' + dataStringThumbpng);
                $('input[name=DoorInside]').change(function () {
    
                    //Change per section
                    var FrontType = 'doors';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var DrawerName = $("#drwSty option:selected").text();
    
                    // var Name = DoorName; 
                    var ImgID = '#doorStyImg';
                    var ImgModal = '#doorStyImgModel';
                    var ImgInModal = '#doorStyImgInModal';
                    var ImgOnRight = "#construction_inside";
    
                    // Do Not Change
                    var Inside = $(this).attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    $("#construction_inside_title").html("Inside Profile");
                    $("#construction_center_title").html("Center Panel");
                    $("#construction_outside_title").html("Outside Profile");
                    $("#construction_siterail_title").html("Stile/Rails");
                    $("#construction_hardware_title").html("Hardware");
                    $("#construction_door_title").html("Door");
                    $("#construction_drawer_title").html("SM Drawer");
                    $("#construction_lgdrawer_title").html("LG Drawer");    
    
                    if (Inside != undefined) {
                        $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                        $("#construction_inside_label").html(Inside.trim());
                        // ////console.log("Inside: /images/inside/" + Inside.trim() + ".png");
                    } else {
                        $("#construction_inside").attr("src", "/images/no_image_inside.png");
                        $("#construction_inside_label").html("");
                        // ////console.log("Inside: /images/no_image.png");
                    }
    
                    if (CenterPanel != undefined) {
                        $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                        $("#construction_center_label").html(CenterPanel.trim());
                        // ////console.log("Center: /images/centerpanel/" + CenterPanel.trim() + ".png");
                    } else {
                        $("#construction_center").attr("src", "/images/no_image_center.png");
                        $("#construction_center_label").html("");
                        // ////console.log("Center: /images/no_image.png");
                    }
    
                    if (Outside != undefined) {
                        $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                        $("#construction_outside_label").html(Outside.trim());
                        // ////console.log("Outside: /images/outside/" + Outside.trim() + ".png");
                    } else {
                        $("#construction_outside").attr("src", "/images/no_image_outside.png");
                        $("#construction_outside_label").html("");
                        // ////console.log("Outside: /images/no_image.png");
                    }
    
                    if (StileRail != undefined) {
                        $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                        $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                        // ////console.log("StileRail: /images/stilerail/" + StileRail.trim() + ".png");
                    } else {
                        $("#construction_siterail").attr("src", "/images/no_image_stile.png");
                        $("#construction_siterail_label").html("");
                        // ////console.log("StileRail: /images/no_image.png");
                    }
    
                    if (Hardware != undefined) {
                        $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                        if (Hardware != "None") {
                            $("#construction_hardware_label").html(Hardware.trim());
                        }
                    } else {
                        $("#construction_hardware").attr("src", "/images/no_image.png");
                        $("#construction_hardware_label").html("");
                    }
    
                    var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png'
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
                    var dataStringProfile = '/images/inside/' + $.trim(Inside) + '.png';
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $(ImgOnRight).attr("src", dataStringProfile);
                    $("#doorStyDXF").attr("href", dataStringdxf);
                    $("#doorStyPDF").attr("href", dataStringpdf);
    
                    //update both drawers if door options are changed
                    var dataStringDrwThumbpng = '/images/drawers/' + Name + '/' + 'Drw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringLgDrwThumbpng = '/images/largedrawers/' + Name + '/' + 'LgDrw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
    
                    if (DrawerName.search("DRW") != -1) {
                        // console.log("DRW found in name - ignore updating the drawer images");
                    } else{
                        $("#drwStyImg").attr("src", dataStringDrwThumbpng);
                        $("#lgdrwStyImg").attr("src", dataStringLgDrwThumbpng);
                    }
                    //console.log("dataStringDrwThumbpng: " + dataStringDrwThumbpng);
                    //console.log("dataStringLgDrwThumbpng: " + dataStringLgDrwThumbpng);
    
                    // ////console.log("----- INSIDE ----");
                    // ////console.log("Inside: " + Inside);
                    // ////console.log("StileRail: " + StileRail);
                    // ////console.log("CenterPanel: " + CenterPanel);
                    // ////console.log("Outside: " + Outside);
                    // ////console.log("dataStringThumbpng: " + dataStringThumbpng);
                    // ////console.log("dataStringpng: " + dataStringpng);
                    // ////console.log("dataStringProfile: " + dataStringProfile);
                    // ////console.log("----- * INSIDE ----");
                    // ////console.log("");
                    // ////console.log("");
    
                });
    
                $('input[name=DoorCenterPanel]').change(function () {
    
                    //Change per section
                    var FrontType = 'doors';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var DrawerName = $("#drwSty option:selected").text();
                    // var Name = DoorName;
                    var ImgID = '#doorStyImg';
                    var ImgModal = '#doorStyImgModel';
                    var ImgInModal = '#doorStyImgInModal';
                    var ImgOnRight = "#construction_center";
    
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    $("#construction_inside_title").html("Inside Profile");
                    $("#construction_center_title").html("Center Panel");
                    $("#construction_outside_title").html("Outside Profile");
                    $("#construction_siterail_title").html("Stile/Rails");
                    $("#construction_hardware_title").html("Hardware");
                    $("#construction_door_title").html("Door");
                    $("#construction_drawer_title").html("SM Drawer");
                    $("#construction_lgdrawer_title").html("LG Drawer");
    
    
                    if (Inside != undefined) {
                        $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                        $("#construction_inside_label").html(Inside.trim());
                        // ////console.log("Inside: /images/inside/" + Inside.trim() + ".png");
                    } else {
                        $("#construction_inside").attr("src", "/images/no_image_inside.png");
                        $("#construction_inside_label").html("");
                        // ////console.log("Inside: /images/no_image.png");
                    }
    
                    if (CenterPanel != undefined) {
                        $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                        $("#construction_center_label").html(CenterPanel.trim());
                        // ////console.log("Center: /images/centerpanel/" + CenterPanel.trim() + ".png");
                    } else {
                        $("#construction_center").attr("src", "/images/no_image_center.png");
                        $("#construction_center_label").html("");
                        // ////console.log("Center: /images/no_image.png");
                    }
    
                    if (Outside != undefined) {
                        $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                        $("#construction_outside_label").html(Outside.trim());
                        // ////console.log("Outside: /images/outside/" + Outside.trim() + ".png");
                    } else {
                        $("#construction_outside").attr("src", "/images/no_image_outside.png");
                        $("#construction_outside_label").html("");
                        // ////console.log("Outside: /images/no_image.png");
                    }
    
                    if (StileRail != undefined) {
                        $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                        $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                        // ////console.log("StileRail: /images/stilerail/" + StileRail.trim() + ".png");
                    } else {
                        $("#construction_siterail").attr("src", "/images/no_image_stile.png");
                        $("#construction_siterail_label").html("");
                        // ////console.log("StileRail: /images/no_image.png");
                    }
    
                    if (Hardware != undefined) {
                        $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                        if (Hardware != "None") {
                            $("#construction_hardware_label").html(Hardware.trim());
                        }
                    } else {
                        $("#construction_hardware").attr("src", "/images/no_image.png");
                        $("#construction_hardware_label").html("");
                    }
    
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $(this).attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
                    var dataStringProfile = '/images/centerpanel/' + $.trim(CenterPanel) + '.png';
    
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $(ImgOnRight).attr("src", dataStringProfile);
                    $("#doorStyDXF").attr("href", dataStringdxf);
                    $("#doorStyPDF").attr("href", dataStringpdf);
    
                    //update both drawers if door options are changed
                    var dataStringDrwThumbpng = '/images/drawers/' + Name + '/' + 'Drw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringLgDrwThumbpng = '/images/largedrawers/' + Name + '/' + 'LgDrw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    
                    if (DrawerName.search("DRW") != -1) {
                        // console.log("DRW found in name - ignore updating the drawer images");
                    } else {
                        $("#drwStyImg").attr("src", dataStringDrwThumbpng);
                        $("#lgdrwStyImg").attr("src", dataStringLgDrwThumbpng);
                    }
    
                    // ////console.log("----- CENTER ----");
                    // ////console.log("Inside: " + Inside);
                    // ////console.log("StileRail: " + StileRail);
                    // ////console.log("CenterPanel: " + CenterPanel);
                    // ////console.log("Outside: " + Outside);
                    // ////console.log("dataStringThumbpng: " + dataStringThumbpng);
                    // ////console.log("dataStringpng: " + dataStringpng);
                    // ////console.log("dataStringProfile: " + dataStringProfile);
                    // ////console.log("----- * CENTER ----");
                    // ////console.log("");
                    // ////console.log("");
    
    
                });
    
                $('input[name=DoorOutside]').change(function () {
    
                    //Change per section
                    var FrontType = 'doors';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var DrawerName = $("#drwSty option:selected").text();
                    // var Name = DoorName;
                    var ImgID = '#doorStyImg';
                    var ImgModal = '#doorStyImgModel';
                    var ImgInModal = '#doorStyImgInModal';
                    var ImgOnRight = '#construction_outside';
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $(this).attr("code");
    
    
    
                    if (Inside != undefined) {
                        $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                        $("#construction_inside_label").html(Inside.trim());
                        // ////console.log("Inside: /images/inside/" + Inside.trim() + ".png");
                    } else {
                        $("#construction_inside").attr("src", "/images/no_image_inside.png");
                        $("#construction_inside_label").html("");
                        // ////console.log("Inside: /images/no_image.png");
                    }
    
                    if (CenterPanel != undefined) {
                        $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                        $("#construction_center_label").html(CenterPanel.trim());
                        // ////console.log("Center: /images/centerpanel/" + CenterPanel.trim() + ".png");
                    } else {
                        $("#construction_center").attr("src", "/images/no_image_center.png");
                        $("#construction_center_label").html("");
                        // ////console.log("Center: /images/no_image.png");
                    }
    
                    if (Outside != undefined) {
                        $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                        $("#construction_outside_label").html(Outside.trim());
                        // ////console.log("Outside: /images/outside/" + Outside.trim() + ".png");
                    } else {
                        $("#construction_outside").attr("src", "/images/no_image_outside.png");
                        $("#construction_outside_label").html("");
                        // ////console.log("Outside: /images/no_image.png");
                    }
    
                    if (StileRail != undefined) {
                        $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                        $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                        // ////console.log("StileRail: /images/stilerail/" + StileRail.trim() + ".png");
                    } else {
                        $("#construction_siterail").attr("src", "/images/no_image_stile.png");
                        $("#construction_siterail_label").html("");
                        // ////console.log("StileRail: /images/no_image.png");
                    }
    
                    if (Hardware != undefined) {
                        $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                        if (Hardware != "None") {
                            $("#construction_hardware_label").html(Hardware.trim());
                        }
                    } else {
                        $("#construction_hardware").attr("src", "/images/no_image.png");
                        $("#construction_hardware_label").html("");
                    }
    
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png'
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
                    var dataStringProfile = '/images/outside/' + $.trim(Outside) + '.png';
    
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $(ImgOnRight).attr("src", dataStringProfile);
                    $("#doorStyDXF").attr("href", dataStringdxf);
                    $("#doorStyPDF").attr("href", dataStringpdf);
    
    
                    //update both drawers if door options are changed
                    var dataStringDrwThumbpng = '/images/drawers/' + Name + '/' + 'Drw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringLgDrwThumbpng = '/images/largedrawers/' + Name + '/' + 'LgDrw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    
                    if (DrawerName.search("DRW") != -1) {
                        // console.log("DRW found in name - ignore updating the drawer images");
                    } else {
                        $("#drwStyImg").attr("src", dataStringDrwThumbpng);
                        $("#lgdrwStyImg").attr("src", dataStringLgDrwThumbpng);
                    }
    
                    // ////console.log("----- OUTSIDE ----");
                    // ////console.log("Inside: " + Inside);
                    // ////console.log("StileRail: " + StileRail);
                    // ////console.log("CenterPanel: " + CenterPanel);
                    // ////console.log("Outside: " + Outside);
                    // ////console.log("dataStringThumbpng: " + dataStringThumbpng);
                    // ////console.log("dataStringpng: " + dataStringpng);
                    // ////console.log("ImgInModal: " + ImgInModal);
                    // ////console.log("dataStringProfile: " + dataStringProfile);
                    // ////console.log("----- * OUTSIDE ----");
                    // ////console.log("");
                    // ////console.log("");
    
                });
    
                $('input[name=DoorStileRail]').change(function () {
    
                    //Change per section
                    var FrontType = 'doors';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var DrawerName = $("#drwSty option:selected").text();
                    // var Name = DoorName;
                    var ImgID = '#doorStyImg';
                    var ImgModal = '#doorStyImgModel';
                    var ImgInModal = '#doorStyImgInModal';
                    var ImgOnRight = '#construction_siterail';
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $(this).attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
    
                    if (Inside != undefined) {
                        $("#construction_inside").attr("src", "/images/inside/" + Inside.trim() + ".png");
                        $("#construction_inside_label").html(Inside.trim());
                        // ////console.log("Inside: /images/inside/" + Inside.trim() + ".png");
                    } else {
                        $("#construction_inside").attr("src", "/images/no_image_inside.png");
                        $("#construction_inside_label").html("");
                        // ////console.log("Inside: /images/no_image.png");
                    }
    
                    if (CenterPanel != undefined) {
                        $("#construction_center").attr("src", "/images/centerpanel/" + CenterPanel.trim() + ".png");
                        $("#construction_center_label").html(CenterPanel.trim());
                        // ////console.log("Center: /images/centerpanel/" + CenterPanel.trim() + ".png");
                    } else {
                        $("#construction_center").attr("src", "/images/no_image_center.png");
                        $("#construction_center_label").html("");
                        // ////console.log("Center: /images/no_image.png");
                    }
    
                    if (Outside != undefined) {
                        $("#construction_outside").attr("src", "/images/outside/" + Outside.trim() + ".png");
                        $("#construction_outside_label").html(Outside.trim());
                        // ////console.log("Outside: /images/outside/" + Outside.trim() + ".png");
                    } else {
                        $("#construction_outside").attr("src", "/images/no_image_outside.png");
                        $("#construction_outside_label").html("");
                        // ////console.log("Outside: /images/no_image.png");
                    }
    
                    if (StileRail != undefined) {
                        $("#construction_siterail").attr("src", "/images/stilerail/" + StileRail.trim() + ".png");
                        $("#construction_siterail_label").html("Stile/Rail <br>" + StileRail.trim());
                        // ////console.log("StileRail: /images/stilerail/" + StileRail.trim() + ".png");
                    } else {
                        $("#construction_siterail").attr("src", "/images/no_image_stile.png");
                        $("#construction_siterail_label").html("");
                        // ////console.log("StileRail: /images/no_image.png");
                    }
    
                    if (Hardware != undefined) {
                        $("#construction_hardware").attr("src", "/images/hardware/" + Hardware.trim() + ".png");
                        if (Hardware != "None") {
                            $("#construction_hardware_label").html(Hardware.trim());
                        }
                    } else {
                        $("#construction_hardware").attr("src", "/images/no_image.png");
                        $("#construction_hardware_label").html("");
                    }
    
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/' + Input + ' ' + Name + ' ' + Code + '.pdf';
                    var dataStringProfile = '/images/stilerail/' + $.trim(StileRail) + '.png';
    
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $(ImgOnRight).attr("src", dataStringProfile);
                    $("#doorStyDXF").attr("href", dataStringdxf);
                    $("#doorStyPDF").attr("href", dataStringpdf);
    
    
                    //update both drawers if door options are changed
                    var dataStringDrwThumbpng = '/images/drawers/' + Name + '/' + 'Drw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringLgDrwThumbpng = '/images/largedrawers/' + Name + '/' + 'LgDrw' + ' ' + Name + ' Thumbnail ' + Code + '.png';
    
                    if (DrawerName.search("DRW") != -1) {
                        // console.log("DRW found in name - ignore updating the drawer images");
                    } else {
                        $("#drwStyImg").attr("src", dataStringDrwThumbpng);
                        $("#lgdrwStyImg").attr("src", dataStringLgDrwThumbpng);
                    }
    
    
                    // ////console.log("----- STILE ----");
                    // ////console.log("Inside: " + Inside);
                    // ////console.log("StileRail: " + StileRail);
                    // ////console.log("CenterPanel: " + CenterPanel);
                    // ////console.log("Outside: " + Outside);
                    // ////console.log("dataStringThumbpng: " + dataStringThumbpng);
                    // ////console.log("dataStringpng: " + dataStringpng);
                    // ////console.log("dataStringProfile: " + dataStringProfile);
                    // ////console.log("----- * STILE ----");
                    // ////console.log("");
                    // ////console.log("");
    
                });
    
                // $("#DoorInsideBox input:radio").prop("checked", true).trigger("click");
                // $("#DoorCenterBox input:radio").prop("checked", true).trigger("click");
                // $("#DoorStileBox input:radio").prop("checked", true).trigger("click");
                // $("#DoorOutsideBox input:radio").prop("checked", true).trigger("click");
    
                // $("#DoorInsideBox input:radio").trigger("click");
                // $("#DoorCenterBox input:radio").trigger("click");
                // $("#DoorStileBox input:radio").trigger("click");
                // $("#DoorOutsideBox input:radio").trigger("click");
    
    
    
    
            // }, 400);
        // });
    
        //Drawer Image Builder
        // $(function () {
            // setTimeout(function () {
                ////console.log("SM drwSty image builder called");
                var drwSty = $("#drwSty option:selected").text();
                var drwStySuff = drwSty.slice(drwSty.length - 2);
                //console.log("drwSty.slice -2 " + drwSty.slice(drwSty.length - 2));
    
                // If Large Drawer DOES NOT match Door
                if (drwStySuff != 'LM' || drwStySuff != 'SM') {
                    $("#drwModal").addClass("disabled");
                    ////console.log('Building Drawer Image - no LM or SM detected');
                    ////console.log('drwSty = ' + drwSty);
                    //Change per section
                    var FrontType = 'drawers';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var ImgID = '#drwStyImg';
                    var ImgModal = '#drwStyImgModel';
                    var ImgInModal = '#drwStyImgInModal';
    
                    //update right bar labels
                    $("#construction_drawer_label").html(Name);
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    //console.log('Drawers ------');
                    //console.log(' -- Inside: ' + Inside);
                    //console.log(' -- Outside: ' + Outside);
                    //console.log(' -- CenterPanel: ' + CenterPanel);
                    //console.log(' -- StileRail: ' + StileRail);
    
                    //change file path for DRW - drawers
                    if (drwSty.search("DRW") != -1 ) {
                        var Code = (Outside).trim();
                        Name = drwSty;
                        // console.log("search outcome drawer is DRW: " + drwSty + ", " + drwSty.search("DRW") + ", code: " + Code);
                    } else {
                        var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                        // console.log("search outcome drawer is DRW: " + drwSty + ", " + drwSty.search("DRW") + ", code: " + Code);
                    }
    
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/Drw ' + Name + ' ' + Code + '.pdf';
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $("#drwStyDXF").attr("href", dataStringdxf);
                    $("#drwStyPDF").attr("href", dataStringpdf);
    
                } else {
                    var FrontType = 'drawers';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var ImgID = '#drwStyImg';
                    var ImgModal = '#drwStyImgModel';
                    var ImgInModal = '#drwStyImgInModal';

                    $("#construction_drawer_label").html(Name);
                }
            // }, 400);
        // });
    
        //Large Drawer Image Builder
        // $(function () {
            // setTimeout(function () {
                var lgdrwSty = $("#lgdrwSty option:selected").text();
                var lgdrwStySuff = lgdrwSty.slice(lgdrwSty.length - 2);
    
                // If Large Drawer DOES NOT match Door
                if (lgdrwStySuff != 'LM' || lgdrwStySuff != 'SM' ) {
                    $("#lgdrwModal").addClass("disabled");
    
                    //Change per section
                    var FrontType = 'largedrawers';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var ImgID = '#lgdrwStyImg';
                    var ImgModal = '#lgdrwStyImgModel';
                    var ImgInModal = '#lgdrwStyImgInModal';
    
                    //update right bar labels
                    $("#construction_lgdrawer_label").html(Name);
    
                    // Do Not Change
                    var Inside = $('input[name=' + Input + 'Inside]:checked').attr("code");
                    var StileRail = $('input[name=' + Input + 'StileRail]:checked').attr("code");
                    var CenterPanel = $('input[name=' + Input + 'CenterPanel]:checked').attr("code");
                    var Outside = $('input[name=' + Input + 'Outside]:checked').attr("code");
    
                    if (Inside) { var Inside = " " + Inside; } else { var Inside = ""; }
                    if (StileRail) { var StileRail = " " + StileRail; } else { var StileRail = ""; }
                    if (CenterPanel) { var CenterPanel = " " + CenterPanel; } else { var CenterPanel = ""; }
                    if (Outside) { var Outside = " " + Outside; } else { var Outside = ""; }
    
                    //change file path for DRW - drawers
                    if (lgdrwSty.search("DRW") != -1) {
                        var Code = (Outside).trim();
                        Name = lgdrwSty;
                        // console.log("search outcome:" + lgdrwSty.search("DRW"));
                    } else {
                        var Code = (Inside + StileRail + CenterPanel + Outside).trim();
                    }
    
                    var dataStringThumbpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' Thumbnail ' + Code + '.png';
                    var dataStringpng = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.png';
                    var dataStringdxf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.dxf';
                    var dataStringpdf = '/images/' + FrontType + '/' + Name + '/LgDrw ' + Name + ' ' + Code + '.pdf';
    
                    $(ImgID).attr("src", dataStringThumbpng);
                    $(ImgModal).attr("src", dataStringpng);
                    $(ImgInModal).attr("src", dataStringThumbpng);
                    $("#lgdrwStyDXF").attr("href", dataStringdxf);
                    $("#lgdrwStyPDF").attr("href", dataStringpdf);
    
                } else {
                    var FrontType = 'largedrawers';
                    var Input = 'Door';
                    var Name = $("#doorSty option:selected").text();
                    var ImgID = '#lgdrwStyImg';
                    var ImgModal = '#lgdrwStyImgModel';
                    var ImgInModal = '#lgdrwStyImgInModal';
                    // Name = Name.trim();
                    $("#construction_lgdrawer_label").html(Name);
                }                
            // }, 400);
        // });
        ////console.log("[on-load] setting /images/no_image.png");
        // $("#doorStyImg").attr("src", "/images/no_image.png"); //Door
        // $("#drwStyImg").attr("src", "/images/no_image.png"); //Drawer
        // $("#lgdrwStyImg").attr("src", "/images/no_image.png"); //LG Drawer
        // $("#styFinishImg").attr("src", "/images/no_image.png"); //Finish
        // $("#construction_inside").attr("src", "/images/no_image.png"); //Inside Profile
        // $("#construction_center").attr("src", "/images/no_image.png"); //Center Panel
        // $("#construction_outside").attr("src", "/images/no_image.png"); //Outside Profile
        // $("#construction_siterail").attr("src", "/images/no_image.png"); //Stile Rails

        // $("#doorStyImg").attr("src", "/images/no_image.png");
        // $("#doorStyImgModel").attr("src", "/images/no_image.png");
        // $("#doorStyImgInModal").attr("src", "/images/no_image.png");
        // $("#doorStyDXF").attr("href", "#");
        // $("#doorStyPDF").attr("href", "#");

        // $("#lgdrwStyImg").attr("src", "/images/no_image.png");
        // $("#lgdrwStyImgModel").attr("src", "/images/no_image.png");
        // $("#lgdrwStyImgInModal").attr("src", "/images/no_image.png");
        // $("#lgdrwStyDXF").attr("href", "#");
        // $("#lgdrwStyPDF").attr("href", "#");

        // $("#drwStyImg").attr("src", "/images/no_image.png");
        // $("#drwStyImgModel").attr("src", "/images/no_image.png");
        // $("#drwStyImgInModal").attr("src", "/images/no_image.png");
        // $("#drwStyDXF").attr("href", "#");
        // $("#drwStyPDF").attr("href", "#");
        console.groupEnd("loadSpecImages()");
    }, 3000);
    };

    function loadDrwRunner(){
        var drwBoxID               = $("#drwBox").val(); 
        var dataStringCabDrwRunner = 'Ajax=CabinetDrwRunner&id=' + drwBoxID + '&jobId=' + $('#myJobId').val();
            
            // console.group("HTML return: ");
            // console.log(dataStringCabDrwRunner);
            // console.groupEnd();
        $.ajax({
            type: "POST",
            url: "/quote_orders/ajaxRequest",
            data: dataStringCabDrwRunner,
            cache: false,
            success: function (html) {
                // console.group("HTML return runner: ");
                // console.log(html);
                // console.groupEnd();
                
                $("#drwRunner").html(html).select2({ 
                    placeholder: "Select (Drawer Box Runner)", 
                    minimumResultsForSearch: Infinity 
                });
                
            }
        });
    };

  
    loadSpecImages();
    
    //=====================================





