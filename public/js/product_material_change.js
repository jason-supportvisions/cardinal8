$(document).ready(function() {
	
	$(".styMatl").change(function () {
			var id = $(this).val();
			var dataString = 'Ajax=StyleColor&id=' + id;
			var FrontstyMatl = $("#styMatl option:selected").text();
			var FrontstyColor = 'Natural'
			var FrontstyFinish = $("#styFinish option:selected").text();
			var dataStringjpg = '/images/finish/' + FrontstyMatl.replace(/\//g, '_') + ' ' + FrontstyColor.replace(/\//g, '_') +'.jpg';
			
			$("#styFinishImg").attr("src", dataStringjpg);

			$.ajax({
				type: "POST",
				url: "/quotes_orders/get_construction.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$(".styColor").html(html);
				}
			});
			//$("#styMatlImg").attr("src", dataStringjpg);
			//$("#styFinish").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity,theme: "bootstrap", data: {id:null, text: null}});
			$(function(){setTimeout(function(){
				$(".styColor").trigger('change')
				},300);
			});
		});

		$(".styColor").change(function () {
			var id = $(this).val();
			var materialid = $("#styMatl option:selected").val()
			var dataString = 'Ajax=StyleFinish&id=' + id;
			var FrontstyMatl = $("#styMatl option:selected").text();
			var FrontstyColor = $("#styColor option:selected").text();
			var FrontstyFinish = $("#styFinish option:selected").text();
			var dataStringjpg = '/images/finish/' + FrontstyMatl.replace(/\//g, '_') + ' ' + FrontstyColor.replace(/\//g, '_') + '.jpg';
			
			$("#styFinishImg").attr("src", dataStringjpg);

			$.ajax({
				type: "POST",
				 url: "/quotes_orders/get_construction.php",
				data: dataString,
				cache: false,
				success: function (html) {
					$(".styFinish").html(html);
				}
			});
			//$("#styColorlImg").attr("src", dataStringjpg);
			//$("#styFinish").select();
			//$("#styFinish").html('').select2({placeholder: "Select an option",minimumResultsForSearch: Infinity,theme: "bootstrap", data: {id:null, text: null}});
			$(function(){setTimeout(function(){
				$(".styFinish").trigger('change')
				},300);
			});
		});

		$(".styFinish").change(function () {

			var FrontstyMatl = $("#styMatl option:selected").text().replace(/\//g, '_');
			var FrontstyColor = $("#styColor option:selected").text().replace(/\//g, '_');
			var FrontstyFinish = $("#styFinish option:selected").text().replace(/\//g, '_');
			
			if (FrontstyColor == "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else if (FrontstyColor != "Natural" && (FrontstyFinish == "15 Sheen" || FrontstyFinish == "40 Sheen" || FrontstyFinish == "15 Sheen (Low VOC)" || FrontstyFinish == "40 Sheen (Low VOC)")){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else if (FrontstyMatl.includes("Formica") || FrontstyMatl.includes("Wilsonart") || FrontstyFinish == "N/A" || FrontstyFinish == "N_A"){
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			} else {
				var dataStringjpg = '/images/finish/' + FrontstyMatl + ' ' + FrontstyColor + ' ' + FrontstyFinish + '.jpg';
				$("#styFinishImg").attr("src", dataStringjpg);
			}
			
		var frontSelected = $("#frontSel option:selected").val();
			
		//if (frontSelected === 'All'){
		var StyleListDoorID = $("#doorSty option:selected").val();
		var StyleListMatl = $("#styMatl option:selected").val();
		var StyleListColor = $("#styColor option:selected").val();
		var StyleListFinish = $("#styFinish option:selected").val();
		
		var GlassWidth = parseFloat($("#prodWidth").val()) || 0;
		var GlassHeight = parseFloat($("#prodHeight").val()) || 0;
		
		
		if (frontSelected === 'All Fronts'){
			var GlassSqFt = parseFloat($("#TotalS").val()) || 0;
		} else if (frontSelected === 'Upper Fronts'){
			var GlassSqFt = parseFloat($("#TotalS").val()/2) || 0;
		} else if (frontSelected === 'Lower Fronts'){
			var GlassSqFt = parseFloat($("#TotalS").val()/2) || 0;
		} else if (frontSelected === 'Left Upper Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/4) || 0;
		} else if (frontSelected === 'Right Upper Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/4) || 0;
		} else if (frontSelected === 'Left Lower Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/4) || 0;
		} else if (frontSelected === 'Right Lower Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/4) || 0;
		} else if (frontSelected === 'Left Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/2) || 0;
		} else if (frontSelected === 'Right Front'){
			var GlassSqFt = parseFloat($("#TotalS").val()/2) || 0;
		} else {
			var GlassSqFt = parseFloat($("#TotalS").val()) || 0;
		}
		

		
		
		var dataStringDoor = 'Ajax=StyleList&Front=Glass&Style=' + StyleListDoorID + '&Material=' + StyleListMatl + '&Color=' + StyleListColor + '&Finish=' + StyleListFinish + '&Width=' + GlassWidth + '&Height=' + GlassHeight + '&SqFt=' + GlassSqFt;
		
		$.ajax({
			type: "POST",
			url: "/quotes_orders/get_construction.php",
			data: dataStringDoor,
			cache: false,
			success: function (html) {
				$("#StyleListDoor").html(html);
			}
		});
		//}
			
		});
	
});
