$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    
    $("#autocomplete").autocomplete({
        source: function (request, response) {
            $.ajax({
                type: "POST",
                url: "/quote_orders/autosearch",
                data: {
                    term: request.term
                },
                dataType: "json",
                cache: false,
                success: function (data) {
                    response(data);
                    if (data) {
                        $('.add_result').hide();
                        $('.search_result_div').show();
                        $(".search_result").empty();
                        var jobid = $('#jobid').val();
                        if (data.length > 0) {
                            var rows = "";
                            rows += "<div class=\"panel-body\">";
                            for (var i = 0; i < data.length; i++) {
                                if (data[i].category == "Accessory") {
                                    var imagepath = "accessories";
                                } else {
                                    var imagepath = "cabinets";
                                }
                                
                                rows += "<div class=\"row\" style=\"background-color: #ffffff;\">";
                                rows +=     "<div class=\"col-sm-12\">";
                                rows +=         "<h4>" + data[i].book + "</h4>";
                                rows +=     "</div>";
                                rows +=     "<div class=\"col-sm-4\" style=\"text-align:center;\"><img src=\"/images/" + imagepath + "/" + data[i].image_file + "\" width=\"200\" height=\"200\" alt=\""+ data[i].image_file + "\">";
                                rows +=     "<br/>" + data[i].groupcode;
                                rows +=     "</div>";
                                rows +=     "<div class=\"col-sm-8\"><br /><br />";
                                if (data[i].notes){
                                    var notes = data[i].notes;
                                }else{
                                    var notes = " ";
                                }
                                rows +=         "<ul>" + notes + "</ul ><br /><br />";
                                rows +=         "<div class=\"col-sm-12\">";
                                rows +=             "<div class=\"col-sm-5\"><strong> Code</strong></div>";
                                rows +=             "<div class=\"col-sm-2\"><strong> Size</strong></div>";
                                rows +=             "<div class=\"col-sm-5\"><strong> </strong></div>";
                                rows +=         "</div><br /><br />";
                                for (var j = 0; j < data[i].items.length; j++) {
                                    rows +=     "<div class=\"col-sm-12\" >";
                                    rows +=         "<div class=\"col-sm-5\">" + data[i].items[j].code + "</div>";
                                    rows +=         "<div class=\"col-sm-2\">" + Math.round(data[i].items[j].width * 100) / 100 + "</div>";
                                    rows +=         "<div class=\"col-sm-5\">";
                                    rows +=             "<a href = \"/quote_orders/products/create/" + jobid + "/" + data[i].items[j].id + "/" + data[i].items[j].type+"\">";
                                    rows +=             "<button type=\"submit\" class=\"btn btn-success btn-xs\">Add</button></a>";
                                    rows +=         "</div>";
                                    rows +=      "</div><br /><br />";
                                }
                                rows += "</div>";
                                rows += "</div><hr>";
                                
                            }
                            rows += "</div>";
                            $(".search_result").append(rows);
                        }
                    }
                    
                }

            }); 
        },
        select: function (event, ui) {
            // Set selection
            $('#autocomplete').val(ui.item.label); // display the selected text
            return false;
        }
    });
    
    // $("#autocomplete").onchange(function () {
    //     var show_product = $("#autocomplete").val();
    //     alert(show_product);
    //     if (show_product == '') {
    //         $('.add_result').show();
    //     }
    // });
});

function empty() {
    var searchDescription, searchItemCode;
    searchItemCode = document.getElementById("searchItemCode").value;
    searchDescription = document.getElementById("searchDescription").value;
    if (searchItemCode == "" && searchDescription == "") {
        alert("Enter a value to search");
        return false;
    };
}

function input_empty() {
    var autosearchvalue;
    autosearchvalue = document.getElementById("autocomplete").value;
    if (autosearchvalue == '') {
        alert("Enter a value to search");
        return false;
    }
}


