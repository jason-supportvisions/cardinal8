<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductItem extends Model {

    protected $table        = 'product_items';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];
    public $timestamps      = false;

    public function product_group()
    {
        return $this->belongsTo('App\Model\ProductGroup', 'product_group_id', 'ID');
    }

    public function pricingOptions()
    {
        return $this->hasMany('App\Model\ProductItemPricingOption', 'Code', 'Code')
                ->orderBy('MaterialGroup');
    }

}
