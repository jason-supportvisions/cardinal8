<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JobSubstage extends Model {

    public $timestamps      = false;

    public function jobStage()
    {
        return $this->belongsTo('App\Model\JobStage', 'stage_id', 'ID');
    }

}