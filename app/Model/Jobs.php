<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Jobs extends Model
{
    use HasFactory;

    public static function getJobs($SesAcct, $SesType, $SesUser){
 
        if($SesType == -1){
        $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                'job.CreatedDate AS CreatedDate',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Active', '=', 1)
            ->orderByDesc('job.ModifiedDate')
            ->get();
        }elseif($SesType == 1){
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                'job.CreatedDate AS CreatedDate',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Active', '=', 1)
            ->orderByDesc('job.ModifiedDate')
            ->having('job.AccountID', '=', $SesAcct)
            ->get();
        }elseif($SesType == 2){
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                 DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                'job.CreatedDate AS CreatedDate',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Active', '=', 1)
            ->orderByDesc('job.ModifiedDate')
            ->where('job.AccountID', '=', $SesAcct)
            ->where('job.CreatedID', '=', $SesUser)
            ->get();
        }
            return $jobsinfos;
    }

    public static function getjobinfo($jobID){
        $getdata = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('style_brand', 'job.Brand', '=', 'style_brand.ID')
            ->leftJoin('job_stages', 'job.Stage', '=', 'job_stages.ID')
            ->leftJoin('job_types', 'job.Type', '=', 'job_types.ID')
            ->select(
                'job.Brand', 
                'job.Stage',
                'job.AccountID',
                'account.Name AS AccountName',
                'job_types.Name AS Type',
                'job.Reference',
                'job.List',
                'job.Net',
                'job.Customer',
                'job.CreatedID',
                DB::raw("DATE_FORMAT(job.CreatedDate,'%m/%d/%Y %h:%i %p') AS CreatedDate"),
                'job.ModifiedID',
                DB::raw("DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y %h:%i %p') AS ModifiedDate"),
                'job.Active',
                'job.Type',
                'job.EstLeadtime',
                'job.Deposit',
                'job.Balance',
                'job.EstOutProduction',
                'job.CabinetCount',
                'job.AccessoryCount',
                'job.Volume',
                'job.Weight',
                'style_brand.logo_image_small'
            )
            ->where('JobID', '=', $jobID)
            ->get();
        if($getdata[0]->CreatedID){
            $cid = $getdata[0]->CreatedID;
        }else{
            $cid = '';
        }
        
        if($cid){
            $jobCreatedID = DB::table('users')
            ->select('*')
            ->distinct()
            ->where('UserID', '=', $cid)
            ->get();
        }else{
            $jobCreatedID = '';
        }
        if($getdata[0]->ModifiedID){
            $mid = $getdata[0]->ModifiedID;
        }else{
            $mid = '';
        }
        if($mid){
            $jobModifiedID = DB::table('users')
            ->select('*')
            ->where('UserID', '=', $mid)
            ->get();
        }else{
            $jobModifiedID = '';
        }
        $jobsinfo = array($jobID, $getdata,$jobCreatedID, $jobModifiedID);
        return $jobsinfo;
    }

    public static function getcancelledjobs($SesAcct, $SesType){
        if($SesType == -1){
            $jobsinfos = DB::table('job')
                ->Join('account', 'job.AccountID', '=', 'account.ID')
                ->Join('job_types', 'job.Type', '=', 'job_types.ID')
                ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
                ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
                ->select(
                    'account.Name AS AccountName',
                    'job.JobID AS JobID',
                     DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                    'job.Stage AS Stage',
                    DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs/\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                    'job_types.Name AS Type',
                    'job.Reference AS Reference',
                    DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                    DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                    'job.AccountID AS AccountID',
                    'uc.FirstName AS CreatedBy',
                    'um.FirstName AS ModifiedBy'
                )
                ->where('job.Active', '=', 0)
                ->orderByDesc('job.ModifiedDate')
                ->get();
        }elseif($SesType == 1 || $SesType == 2){
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                 DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Active', '=', 0)
            ->having('job.AccountID', '=', $SesAcct)
            ->orderByDesc('job.ModifiedDate')
            ->get();
        }else{
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                 DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 ELSE 3 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Active', '=', 0)
            ->having('job.AccountID', '=', 0)
            ->orderByDesc('job.ModifiedDate')
            ->get();
        }
            return $jobsinfos;
    }

    public static function getstagejobs($SesAcct, $SesType, $jobstage){
        if($SesType == -1){
            $jobsinfos = DB::table('job')
                ->Join('account', 'job.AccountID', '=', 'account.ID')
                ->Join('job_types', 'job.Type', '=', 'job_types.ID')
                ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
                ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
                ->select(
                    'account.Name AS AccountName',
                    'job.JobID AS JobID',
                     DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                    'job.Stage AS Stage',
                    DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs/\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                    'job_types.Name AS Type',
                    'job.Reference AS Reference',
                    DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                    DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                    'job.AccountID AS AccountID',
                    'uc.FirstName AS CreatedBy',
                    'um.FirstName AS ModifiedBy'
                )
                ->where('job.Stage', '=', $jobstage)
                ->orderByDesc('job.ModifiedDate')
                ->get();
        }elseif($SesType == 1 || $SesType == 2){
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                 DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 WHEN job.AccessoryCount <> 0 THEN 3 ELSE 4 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Stage', '=', $jobstage)
            ->having('job.AccountID', '=', $SesAcct)
            ->orderByDesc('job.ModifiedDate')
            ->get();
        }else{
            $jobsinfos = DB::table('job')
            ->Join('account', 'job.AccountID', '=', 'account.ID')
            ->Join('job_types', 'job.Type', '=', 'job_types.ID')
            ->leftJoin('users AS uc', 'job.CreatedID', '=', 'uc.UserID')
            ->leftJoin('users AS um', 'job.ModifiedID', '=', 'um.UserID')
            ->select(
                'account.Name AS AccountName',
                'job.JobID AS JobID',
                 DB::raw("CASE WHEN job.Brand = 1 THEN '<img src=\"/images/logos/imperia.png\">' WHEN job.Brand = 2 THEN '<img src=\"/images/logos/purekitchen.png\">' WHEN job.Brand = 3 THEN '<img src=\"/images/logos/nkd.svg\" width=\"50\">' END AS Brand"),
                'job.Stage AS Stage',
                DB::raw("CASE WHEN job.Stage = 1 THEN '<a class=\"btn btn-xs btn-info\" href=\"/jobs\" style=\"width:100px;\"><strong>Quote</strong></a>' WHEN job.Stage = 2 THEN '<a class=\"btn btn-xs btn-warning\" href=\"../quotes_orders/?JobStage=2\" style=\"width:100px;\"><strong>Confirmed</strong></a>' WHEN job.Stage = 3 THEN '<a class=\"btn btn-xs btn-danger\" href=\"../quotes_orders/?JobStage=3\" style=\"width:100px;\"><strong>Under Review</strong></a>' WHEN job.Stage = 4 THEN '<a class=\"btn btn-xs btn-success\" href=\"../quotes_orders/?JobStage=4\" style=\"width:100px;\"><strong>Accepted</strong></a>' WHEN job.Stage = 5 THEN '<a class=\"btn btn-xs btn-default\" href=\"../quotes_orders/?JobStage=5\" style=\"width:100px;\"><strong>Shipping</strong></a>' WHEN job.Stage = 6 THEN '<a class=\"btn btn-xs btn-primary\" href=\"../quotes_orders/?JobStage=6\" style=\"width:100px;\"><strong>Completed</strong></a>' WHEN job.Stage = 7 THEN '<a class=\"btn btn-xs btn-danger\" href=\"#\" style=\"width:100px;\"><strong>Cancelled</strong></a>' END AS Stage"),
                'job_types.Name AS Type',
                'job.Reference AS Reference',
                DB::raw("CASE WHEN job.ModifiedDate = 'NULL' THEN DATE_FORMAT(job.CreatedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p')  ELSE DATE_FORMAT(job.ModifiedDate,'%m/%d/%Y <br class=\"hidden-xs hidden-sm\">%h:%i %p') END AS ModifiedDate"),
                DB::raw("CASE WHEN job.Stage = 1 THEN 1 WHEN job.CabinetCount <> 0 THEN 2 ELSE 3 END AS Selection"),
                'job.AccountID AS AccountID',
                'uc.FirstName AS CreatedBy',
                'um.FirstName AS ModifiedBy'
            )
            ->where('job.Stage', '=', $jobstage)
            ->having('job.AccountID', '=', 0)
            ->orderByDesc('job.ModifiedDate')
            ->get();
        }
            return $jobsinfos;
    }

    public static function updateActive($jobID, $jobAcct,$active){
        $result = DB::table('job')
            ->where('job.JobID', '=', $jobID)
            ->where('job.AccountID', '=', $jobAcct)
            ->update(['job.Active' => 0,'job.Stage' => 7]);
        return 1 ;


    }
    
}