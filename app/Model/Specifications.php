<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Specifications extends Model
{
    use HasFactory;
    public static function getSpecifications($jobID, $jobAcct)
    {
        $LargeDrawerName            = "n/a";
        $DrawerName                 = "n/a";
        $DoorName                   = "n/a";
        $DrawerStyleID              = 0;
        $LargeDrawerStyleID         = 0;
        $DoorStyleID                = 0;
        $StyleMaterialID            = 0;
        $StyleColorID               = 0;
        $StyleFinishID              = 0;
        $CabinetDrawerBoxID         = 0;
        $CabinetBoxMaterialID       = 0;
        $CabinetToeKickID           = 0;
        $CabinetExteriorEdgeID      = 0;
        $CabinetExteriorColorID     = 0;
        $CabinetExteriorFinishID    = 0;
        $CabinetExteriorMaterialID  = 0;
        $DoorInside                 = 0;
        $DoorCenterPanel            = "";
        $DoorOutside                = "";
        $DoorStileRail              = "";
        $CabinetInteriorName        = "";
        $CabinetInteriorMatGroup    = "";
        $CabinetInteriorBrand       = "";
        $CabinetInteriorEdgeID      = 0;
        $BrandName                  = "";
        $BrandCabCard               = "";
        $BrandWhiteLabel            = "";
        $DrawerBoxName              = "";
        $DoorPull                   = 0;
        $AdminNote                  = "";
        $constructionNote           = "";
        $BrandID                    = 0;
        $GroupID                    = 0;
        $DoorMaterial               = "n/a";
        $DoorColor                  = "n/a";
        $DoorFinish                 = "n/a";
        $CabinetExteriorName        = "n/a";
        $CabinetExteriorMatGroup    = "n/a";
        $CabinetExteriorBrand       = "n/a";
        $CabinetExteriorName        = "n/a";
        $CabinetExteriorEdge        = "n/a";
        $CabinetExteriorColor       = "n/a";
        $CabinetExteriorFinish      = "n/a";
        $StyleType                  = 0;
        $StyleMaterialCustom        = "";
        $StyleColorCustom           = "";
        $StyleFinishCustom          = "";
        $CabinetToeKickName         = "n/a";

        $AcctResults = DB::table('account')
            ->select('*')
            ->where('ID', '=', $jobAcct)
            ->get();

        foreach ($AcctResults as $row) {
            $acctname = $row->Name;
            if (empty($row->AddressLine2)) {
                $acctaddress        = $row->AddressLine1 . "<br>" . $row->City . " " . $row->State . ", " . $row->Zip;
                $acctphone          = $row->Phone;
                $acctemail          = $row->Email;
            } else {
                $acctaddress        = $row->AddressLine1 . " - " . $row->AddressLine2 . "<br>" . $row->City . " " . $row->State . ", " . $row->Zip;
                $acctphone          = $row->Phone;
                $acctemail          = $row->Email;
            }
        }

        $jobResults = DB::table('job')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->get();

        foreach ($jobResults as $row) {
            $jobAcct            = $row->AccountID;
            $jobType            = $row->Type;
            $jobRef             = $row->Reference;
            $jobStage           = $row->Stage;
            $AcctMultDis        = $row->multiplier_discount;
            $AcctMultCus        = $row->multiplier_customer;
        }

        $jobstg = DB::table('job_stages')
            ->select('*')
            ->distinct()
            ->where('ID', '=', $jobStage)
            ->get();

        foreach ($jobstg as $row) {
            $jobStageName       = $row->Name;
        }

        $jobtyp = DB::table('job_types')
            ->select('*')
            ->distinct()
            ->where('ID', '=', $jobType)
            ->get();

        foreach ($jobtyp as $row) {
            $jobTypeName        = $row->Name;
        }

        $cusResults = DB::table('job_header')
            ->select(
                'ContactName',
                'AddressLine1',
                'AddressLine2',
                'City',
                'State',
                'PostalCode',
                'PhoneNumber',
                'EmailAddress',
                'Notes',
            )
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Customer')
            ->get();

        foreach ($cusResults as $row) {
            $cusname            = $row->ContactName;
            $cusnotes           = $row->Notes;
            if (empty($row->AddressLine2)) {
                $cusaddress         = $row->AddressLine1 . "<br>" . $row->City . " " . $row->State . ", " . $row->PostalCode;
                $cusphone           = $row->PhoneNumber;
                $cusemail           = $row->EmailAddress . "<br>&nbsp;";
            } else {
                $cusaddress         = $row->AddressLine1 . "<br>" . $row->AddressLine2 . "<br>" . $row->City . " " . $row->State . ", " . $row->PostalCode;
                $cusphone           = $row->PhoneNumber;
                $cusemail           = $row->EmailAddress;
            }
        }

        $shipResults = DB::table('job_header')
            ->select(
                'ContactName',
                'AddressLine1',
                'AddressLine2',
                'City',
                'State',
                'PostalCode',
                'PhoneNumber',
                'EmailAddress',
                'Notes',
            )
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Shipping')
            ->get();

        $conDebug = "";

        $jobConst = DB::table('job_construction')
            ->select('*')
            ->distinct()
            ->where('JobID', '=', $jobID)
            ->get();

        foreach ($jobConst as $row) {
            $BrandID                        = $row->BrandID;
            $StyleGroupID                   = $row->StyleGroupID;
            $DoorStyleID                    = $row->DoorStyleID;
            $DoorOutsideProfileID           = $row->DoorOutsideProfileID;
            $DoorStileRailID                = $row->DoorStileRailID;
            $DoorPegID                      = $row->DoorPegID;
            $DoorInsideProfileID            = $row->DoorInsideProfileID;
            $DoorCenterPanelID              = $row->DoorCenterPanelID;
            $DoorHardwareID                 = $row->DoorHardwareID;
            $DoorHardwareLocationID         = $row->DoorHardwareLocationID;
            $DrawerStyleID                  = $row->DrawerStyleID;
            $DrawerOutsideProfileID         = $row->DrawerOutsideProfileID;
            $DrawerStileRailID              = $row->DrawerStileRailID;
            $DrawerPegID                    = $row->DrawerPegID;
            $DrawerInsideProfileID          = $row->DrawerInsideProfileID;
            $DrawerCenterPanelID            = $row->DrawerCenterPanelID;
            $DrawerHardwareID               = $row->DrawerHardwareID;
            $DrawerHardwareLocationID       = $row->DrawerHardwareLocationID;
            $LargeDrawerStyleID             = $row->LargeDrawerStyleID;
            $LargeDrawerOutsideProfileID    = $row->LargeDrawerOutsideProfileID;
            $LargeDrawerStileRailID         = $row->LargeDrawerStileRailID;
            $LargeDrawerPegID               = $row->LargeDrawerPegID;
            $LargeDrawerInsideProfileID     = $row->LargeDrawerPegID;
            $LargeDrawerCenterPanelID       = $row->LargeDrawerCenterPanelID;
            $LargeDrawerHardwareID          = $row->LargeDrawerHardwareID;
            $LargeDrawerHardwareLocationID  = $row->LargeDrawerHardwareLocationID;

            $StyleMaterialID                = $row->StyleMaterialID;
            $StyleMaterialGroupID           = $row->StyleMaterialGroupID;
            $StyleMaterialCustom            = $row->StyleMaterialCustom;
            $StyleColorID                   = $row->StyleColorID;
            $StyleColorGroupID              = $row->StyleColorGroupID;
            $StyleColorCustom               = $row->StyleColorCustom;
            $StyleFinishID                  = $row->StyleFinishID;
            $StyleFinishGroupID             = $row->StyleFinishGroupID;
            $StyleFinishCustom              = $row->StyleFinishCustom;

            $CabinetBoxMaterialID           = $row->CabinetBoxMaterialID;
            $CabinetBoxMaterialGroup        = $row->CabinetBoxMaterialGroup;
            $CabinetDrawerBoxID             = $row->CabinetDrawerBoxID;
            $CabinetHingeID                 = $row->CabinetHingeID;

            $CabinetExteriorMaterialID      = $row->CabinetExteriorMaterialID;
            $CabinetExteriorColorID         = $row->CabinetExteriorColorID;
            $CabinetExteriorFinishID        = $row->CabinetExteriorFinishID;
            $CabinetExteriorEdgeID          = $row->CabinetExteriorEdgeID;

            $CabinetInteriorMaterialID      = $row->CabinetInteriorMaterialID;
            $CabinetInteriorColorID         = $row->CabinetInteriorColorID;
            $CabinetInteriorFinishID        = $row->CabinetInteriorFinishID;
            $CabinetInteriorEdgeID          = $row->CabinetInteriorEdgeID;

            $CabinetToeHeightID             = $row->CabinetToeHeightID;
            $CabinetToeKickID               = $row->CabinetToeKickID;
            $AdminNote                      = $row->AdminNote;
        }

        $jobDoorFronts = DB::table('styles')
            ->crossJoin('style_material')
            ->crossJoin('style_color')
            ->crossJoin('style_finish')
            ->select(
                'styles.ID AS StyleID',
                'styles.GroupID AS GroupID',
                'styles.StyleType AS StyleType',
                'styles.Name AS StyleName',
                'style_material.ID AS MaterialID',
                'style_material.Name AS MaterialName',
                'style_color.ID AS ColorID',
                'style_color.Name AS ColorName',
                'style_finish.ID AS FinishID',
                'style_finish.Name AS FinishName',
                DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
            )
            ->where('styles.ID', '=', $DoorStyleID)
            ->where('style_material.ID', '=', $StyleMaterialID)
            ->where('style_color.ID', '=', $StyleColorID)
            ->where('style_finish.ID', '=', $StyleFinishID)
            ->get();

        foreach ($jobDoorFronts as $row) {

            $DoorList               = $row->DoorList;
            $TallDoorList           = $row->TallDoorList;
            $FullTallDoorList       = $row->FullTallDoorList;
            $OverlayList            = $row->OverlayList;
            $DoorSqFtList           = $row->DoorSqFtList;
            $GroupID                = $row->GroupID;
            $DoorName               = $row->StyleName;
            $DoorMaterial           = $row->MaterialName;
            $DoorColor              = $row->ColorName;
            $DoorFinish             = $row->FinishName;
            $StyleType              = $row->StyleType;
        }
        $jobLgDrwFronts = DB::table('styles')
            ->crossJoin('style_material')
            ->crossJoin('style_color')
            ->crossJoin('style_finish')
            ->select(
                'styles.ID AS StyleID',
                'styles.Name AS StyleName',
                'style_material.ID AS MaterialID',
                'style_material.Name AS MaterialName',
                'style_color.ID AS ColorID',
                'style_color.Name AS ColorName',
                'style_finish.ID AS FinishID',
                'style_finish.Name AS FinishName',
                DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
            )
            ->where('styles.ID', '=', $LargeDrawerStyleID)
            ->where('style_material.ID', '=', $StyleMaterialID)
            ->where('style_color.ID', '=', $StyleColorID)
            ->where('style_finish.ID', '=', $StyleFinishID)
            ->get();

        foreach ($jobLgDrwFronts as $row) {
            $LargeDrawerList            = $row->LargeDrawerList;
            $LargeDrawerSqFtList        = $row->DrawerSqFtList;
            $LargeDrawerName            = $row->StyleName;
            $LargeDrawerMaterial        = $row->MaterialName;
            $LargeDrawerColor           = $row->ColorName;
            $LargeDrawerFinish          = $row->FinishName;
        }

        $jobDrwFronts = DB::table('styles')
            ->crossJoin('style_material')
            ->crossJoin('style_color')
            ->crossJoin('style_finish')
            ->select(
                'styles.ID AS StyleID',
                'styles.Name AS StyleName',
                'style_material.ID AS MaterialID',
                'style_material.Name AS MaterialName',
                'style_color.ID AS ColorID',
                'style_color.Name AS ColorName',
                'style_finish.ID AS FinishID',
                'style_finish.Name AS FinishName',
                DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
            )
            ->where('styles.ID', '=', $DrawerStyleID)
            ->where('style_material.ID', '=', $StyleMaterialID)
            ->where('style_color.ID', '=', $StyleColorID)
            ->where('style_finish.ID', '=', $StyleFinishID)
            ->get();

        foreach ($jobDrwFronts as $row) {
            $DrawerList             = $row->DrawerList;
            $LargeDrawerList        = $row->LargeDrawerList;
            $DrawerSqFtList         = $row->DrawerSqFtList;
            $DrawerName             = $row->StyleName;
            $DrawerMaterial         = $row->MaterialName;
            $DrawerColor            = $row->ColorName;
            $DrawerFinish           = $row->FinishName;
        }

        $jobDrwBox = DB::table('cabinet_drwbox')
            ->select('*')
            ->distinct()
            ->where('ID', '=', $CabinetDrawerBoxID)
            ->get();

        foreach ($jobDrwBox as $row) {
            $DrawerBoxList          = $row->SmallAvgList;
            $LargeDrawerBoxList     = $row->LargeAvgList;
            $DrawerBoxName          = $row->Name;
        }

        $jobCabinetInterior = DB::table('cabinet_material')
            ->select('*')
            ->distinct()
            ->where('ID', '=', $CabinetBoxMaterialID)
            ->get();

        foreach ($jobCabinetInterior as $row) {
            $CabinetInteriorName        = $row->Name;
            $CabinetInteriorMatGroup    = $row->MaterialGroup;
            $CabinetInteriorBrand       = $row->BrandID;
        }

        $jobCabinetToeKick = DB::table('cabinet_toekick')
            ->select('*')
            ->distinct()
            ->where('ID', '=', $CabinetToeKickID)
            ->get();

        foreach ($jobCabinetToeKick as $row) {
            $CabinetToeKickName        = $row->Name;
            $CabinetToeKickCode        = $row->Code;
            $CabinetToeKickID          = $row->ID;
        }

        $jobCabinetExtBanding = DB::table('edge_banding')
            ->select('*')
            ->where('ID', '=', $CabinetExteriorEdgeID)
            ->get();

        foreach ($jobCabinetExtBanding as $row) {
            $CabinetExtEdgeID           = $row->ID;
            $CabinetExtEdgeName         = $row->Name;
            $CabinetExtEdgeCode         = $row->Code;
        }

        $jobBrandResults = DB::table('style_brand')
            ->select('*')
            ->where('ID', '=', $BrandID)
            ->get();

        foreach ($jobBrandResults as $row) {
            $BrandID                    = $row->ID;
            $BrandName                  = $row->Name;
            $BrandCabCard               = $row->CabCardFooter;
            $BrandWhiteLabel            = $row->WhiteLabel;
            // if ($BrandWhiteLabel == 1) {
            //     $BrandID = 1;
            //     echo "BrandID: $BrandID";
            // }
        }

        $jobStyleGroupResults = DB::table('style_group')
            ->select('Name')
            ->where('ID', '=', $GroupID)
            ->get();

        foreach ($jobStyleGroupResults as $row) {
            $StyleGroupName             = $row->Name;
        }

        $jobCabExteriorAll = DB::table('style_material')
            ->crossJoin('edge_banding')
            ->crossJoin('style_color')
            ->crossJoin('style_finish')
            ->select(
                'style_material.ID AS CabExtID',
                'style_material.Name AS CabExtMat',
                'style_color.Name AS CabExtColor',
                'style_finish.Name AS CabExtFinish',
                'edge_banding.Name AS CabExtEdge'
            )
            ->where('style_material.ID', '=', $CabinetExteriorMaterialID)
            ->where('edge_banding.ID', '=', $CabinetExteriorEdgeID)
            ->where('style_color.ID', '=', $CabinetExteriorColorID)
            ->where('style_finish.ID', '=', $CabinetExteriorFinishID)
            ->get();

        foreach ($jobCabExteriorAll as $row) {
            $CabinetExteriorName        = $row->CabExtMat;
            $CabinetExteriorEdge        = $row->CabExtEdge;
            $CabinetExteriorColor       = $row->CabExtColor;
            $CabinetExteriorFinish      = $row->CabExtFinish;
        }

        $DoorProfile = DB::table('job_construction')
            ->leftJoin('style_outsideprofile', 'job_construction.DoorOutsideProfileID', '=', 'style_outsideprofile.ID')
            ->leftJoin('style_centerpanel', 'job_construction.DoorCenterPanelID', '=', 'style_centerpanel.ID')
            ->leftJoin('style_insideprofile', 'job_construction.DoorInsideProfileID', '=', 'style_insideprofile.ID')
            ->leftJoin('style_stilerail', 'job_construction.DoorStileRailID', '=', 'style_stilerail.ID')
            ->leftJoin('style_hardware', 'job_construction.DoorHardwareID', '=', 'style_hardware.ID')
            ->select(
                'style_outsideprofile.Code AS Outside',
                'style_insideprofile.Code AS Inside',
                'style_stilerail.Code AS StileRail',
                'style_centerpanel.Code AS CenterPanel',
                'style_hardware.Code AS Pull',
                'job_construction.DoorHardwareLocationID AS PullLocation'
            )
            ->distinct()
            ->where('job_construction.JobID', '=', $jobID)
            ->get();

        foreach ($DoorProfile as $row) {
            $DoorOutside            = $row->Outside;
            $DoorInside             = $row->Inside;
            $DoorStileRail          = $row->StileRail;
            $DoorCenterPanel        = $row->CenterPanel;
            $DoorPull               = $row->Pull;
            $DoorPullLocation       = $row->PullLocation;
        }

        $LargeDrawerProfile = DB::table('job_construction')
            ->leftJoin('style_outsideprofile', 'job_construction.LargeDrawerOutsideProfileID', '=', 'style_outsideprofile.ID')
            ->leftJoin('style_centerpanel', 'job_construction.LargeDrawerCenterPanelID', '=', 'style_centerpanel.ID')
            ->leftJoin('style_insideprofile', 'job_construction.LargeDrawerInsideProfileID', '=', 'style_insideprofile.ID')
            ->leftJoin('style_stilerail', 'job_construction.LargeDrawerStileRailID', '=', 'style_stilerail.ID')
            ->select(
                'style_outsideprofile.Code AS Outside',
                'style_insideprofile.Code AS Inside',
                'style_stilerail.Code AS StileRail',
                'style_centerpanel.Code AS CenterPanel'
            )
            ->distinct()
            ->where('job_construction.JobID', '=', $jobID)
            ->get();

        foreach ($LargeDrawerProfile as $row) {
            $LargeDrawerOutside         = $row->Outside;
            $LargeDrawerInside          = $row->Inside;
            $LargeDrawerStileRail       = $row->StileRail;
            $LargeDrawerCenterPanel     = $row->CenterPanel;
        }

        $DrawerProfile = DB::table('job_construction')
            ->leftJoin('style_outsideprofile', 'job_construction.DrawerOutsideProfileID', '=', 'style_outsideprofile.ID')
            ->leftJoin('style_centerpanel', 'job_construction.DrawerCenterPanelID', '=', 'style_centerpanel.ID')
            ->leftJoin('style_insideprofile', 'job_construction.DrawerInsideProfileID', '=', 'style_insideprofile.ID')
            ->leftJoin('style_stilerail', 'job_construction.DrawerStileRailID', '=', 'style_stilerail.ID')
            ->select(
                'style_outsideprofile.Code AS Outside',
                'style_insideprofile.Code AS Inside',
                'style_stilerail.Code AS StileRail',
                'style_centerpanel.Code AS CenterPanel'
            )
            ->distinct()
            ->where('job_construction.JobID', '=', $jobID)
            ->get();

        foreach ($DrawerProfile as $row) {
            $DrawerOutside          = $row->Outside;
            $DrawerInside           = $row->Inside;
            $DrawerStileRail        = $row->StileRail;
            $DrawerCenterPanel      = $row->CenterPanel;
        }

        $jobResults = DB::table('job_product')
            ->select(
                'ID',
                'Type',
                'Line',
                'Qty',
                'Code',
                'OrigCode',
                'Hinging',
                'FinishedEnd',
                'Description',
                'Notes',
                DB::raw("TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from Width)) AS Width"),
                DB::raw("TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from Height)) AS Height"),
                DB::raw("TRIM(TRAILING '.' FROM TRIM(TRAILING '0' from Depth)) AS Depth"),
                'ListPer',
                'ListQty',
                'CabinetBoxOverride'
            )
            ->distinct()
            ->where('JobID', '=', $jobID)
            ->orderBy(DB::raw("'Line' + 0"))
            ->get();

        $jobTotals = DB::table('job_product')
            ->select(
                DB::raw('Sum(job_product.ListQty) AS ListQty'),
                DB::raw("Sum(ROUND(ListQty * $AcctMultDis , 2)) AS CostQty"),
                DB::raw("Sum(ROUND(ROUND(ListQty * $AcctMultDis , 2) * $AcctMultCus, 2)) AS CustomerQty"),
                DB::raw('Sum(job_product.OrigList) AS OrigList'),
                DB::raw('Sum(job_product.FrontList) AS FrontList'),
                DB::raw('Sum(job_product.DrwBoxList) AS DrwBoxList'),
                DB::raw('Sum(job_product.FinEndList) AS FinEndList'),
                DB::raw('Sum(job_product.FinIntList) AS FinIntList'),
                DB::raw('Sum(job_product.ModList) AS ModList'),
                DB::raw('Sum(job_product.DoorCount) AS DoorCount'),
                DB::raw('Sum(job_product.TallDoorCount) AS TallDoorCount'),
                DB::raw('Sum(job_product.FullTallDoorCount) AS FullTallDoorCount'),
                DB::raw('Sum(job_product.DrawerCount) AS DrawerCount'),
                DB::raw('Sum(job_product.LargeDrawerCount) AS LargeDrawerCount'),
                DB::raw('Sum(job_product.OverlayCount) AS OverlayCount'),
                DB::raw('Sum(job_product.DoorCount+job_product.TallDoorCount+job_product.FullTallDoorCount+job_product.DrawerCount+job_product.LargeDrawerCount+job_product.OverlayCount) AS FrontCount'),
                DB::raw('Sum(job_product.DrawerBoxCount) AS DrawerBoxCount'),
                DB::raw('Sum(job_product.LargeDrawerBoxCount) AS LargeDrawerBoxCount'),
                DB::raw('Sum(job_product.AdjShelfCount) AS AdjShelfCount'),
                DB::raw('Sum(job_product.FixedShelfCount) AS FixedShelfCount'),
                DB::raw('Sum(job_product.PartitionCount) AS PartitionCount'),
                DB::raw('Sum(job_product.DividerCount) AS DividerCount'),
                DB::raw('Sum(job_product.HingeCount) AS HingeCount'),
                DB::raw('Sum(job_product.HandleCount) AS HandleCount'),
                DB::raw('Sum(job_product.FinishedInteriorSqFt) AS FinishedInteriorSqFt'),
                DB::raw('Sum(job_product.Volume) AS Volume'),
                DB::raw('Sum(job_product.Weight) AS Weight'),
                DB::raw('Sum(job_product.Qty) AS Qty'),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' THEN 1 *job_product.Qty END) AS QtyFinEnds"),
                DB::raw("Sum(if(job_product.Category = 'Wall',job_product.Qty,0)) AS Walls"),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Wall' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Wall' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Wall' THEN 1 *job_product.Qty END) AS WallFinEnds"),
                DB::raw("Sum(if(job_product.Category = 'Base',job_product.Qty,0)) AS Bases"),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Base' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Base' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Base' THEN 1 *job_product.Qty END) AS BaseFinEnds"),
                DB::raw("Sum(if(job_product.Category = 'Tall',job_product.Qty,0)) AS Talls"),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Tall' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Tall' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Tall' THEN 1 *job_product.Qty END) AS TallFinEnds"),
                DB::raw("Sum(if(job_product.Category = 'Vanity',job_product.Qty,0)) AS Vanities"),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Vanity' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Vanity' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Vanity' THEN 1 *job_product.Qty END) AS VanityFinEnds"),
                DB::raw("Sum(if(job_product.Category = 'Accessory',job_product.Qty,0)) AS Accessories"),
                DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Accessory' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Accessory' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Accessory' THEN 1 *job_product.Qty END) AS AccessoryFinEnds")
            )
            ->distinct()
            ->where('JobID', '=', $jobID)
            ->get();

        foreach ($jobTotals as $row) {

            $jobListSum             = $row->ListQty ? $row->ListQty : 'null';
            $jobCostSum             = $row->CostQty ? $row->CostQty : 'null';
            $jobCustSum             = $row->CustomerQty;
            $jobOrigList            = $row->OrigList;
            $jobFrontList           = $row->FrontList;
            $jobDrwBoxList          = $row->DrwBoxList;
            $jobFinEndList          = $row->FinEndList;
            $jobModList             = $row->ModList;

            if ($DoorStyleID == 186) {
                $jobDoorCount           = 0;
                $jobTallDoorCount       = 0;
                $jobFullTallDoorCount   = 0;
                $jobDrawerCount         = 0;
                $jobLargeDrawerCount    = 0;
                $jobOverlayCount        = 0;
                $jobFrontCount          = 0;
            } else {
                $jobDoorCount           = $row->DoorCount;
                $jobTallDoorCount       = $row->TallDoorCount;
                $jobFullTallDoorCount   = $row->FullTallDoorCount;
                $jobDrawerCount         = $row->DrawerCount;
                $jobLargeDrawerCount    = $row->LargeDrawerCount;
                $jobOverlayCount        = $row->OverlayCount;
                $jobFrontCount          = $row->FrontCount;
            }
            $jobDrawerBoxCount       = $row->DrawerBoxCount;
            $jobLargeDrawerBoxCount  = $row->LargeDrawerBoxCount;
            $jobAdjShelfCount        = $row->AdjShelfCount;
            $jobFixedShelfCount      = $row->FixedShelfCount;
            $jobPartitionCount       = $row->PartitionCount;
            $jobDividerCount         = $row->DividerCount;
            $jobHingeCount           = $row->HingeCount;
            $jobHandleCount          = $row->HandleCount;
            $jobFinishedInteriorSqFt = $row->FinishedInteriorSqFt;
            $jobVolume               = $row->Volume ? $row->Volume : 'null';
            $jobWeight               = $row->Weight ? $row->Weight : 'null';
            $jobQtyTotal             = $row->Qty;
            $jobQtyFinEnds           = $row->QtyFinEnds;
            $jobWalls                = $row->Walls;
            $jobWallFinEnds          = $row->WallFinEnds;
            $jobBases                = $row->Bases;
            $jobBaseFinEnds          = $row->BaseFinEnds;
            $jobTalls                = $row->Talls;
            $jobTallFinEnds          = $row->TallFinEnds;
            $jobVanities             = $row->Vanities;
            $jobVanityFinEnds        = $row->VanityFinEnds;
            $jobCabinets             = $row->Walls + $row->Bases + $row->Talls + $row->Vanities;
            $jobAccessories          = $row->Accessories ? $row->Accessories : 'null';
            $jobAccessoryFinEnds     = $row->AccessoryFinEnds;
            $jobQty                  = $row->Walls + $row->Bases + $row->Talls + $row->Vanities + $row->Accessories;
        }

        $logo_image_path        = "/images/coming_soon.png";
        $brand_results =  DB::table('style_brand')
            ->select('logo_image_large')
            ->where('ID', '=', $BrandID)
            ->get();
        foreach ($brand_results as $brand_row) {
            $logo_image_path        = "/images/logos/" . $brand_row->logo_image_large;
        }


        $specifications = (object) [

            "door" => (object) [
                'name'          => $DoorName,
                'inside'        => $DoorInside,
                'center_panel'  => $DoorCenterPanel,
                'outside'       => $DoorOutside,
                'style_rail'    => $DoorStileRail,
                'style_type'    => $StyleType
            ],
            "large_drawer" => (object) [
                'name'          => $LargeDrawerName,
                'inside'        => $DoorInside,
                'center_panel'  => $DoorCenterPanel,
                'outside'       => $DoorOutside,
                'style_rail'    => $DoorStileRail
            ],
            "drawer" => (object) [
                'name'          => $DrawerName,
                'inside'        => $DoorInside,
                'center_panel'  => $DoorCenterPanel,
                'outside'       => $DoorOutside,
                'style_rail'    => $DoorStileRail
            ],
            "style" => (object) [
                'material'      => $DoorMaterial,
                'color'         => $DoorColor,
                'finish'        => $DoorFinish,
                'edge'          => $CabinetExteriorEdgeID,
                'material_note' => $StyleMaterialCustom,
                'color_note'    => $StyleColorCustom,
                'finish_note'   => $StyleFinishCustom
            ],
            "style_group" => (object) [
                'name'          => $StyleGroupName
            ],
            "cabinet_interior"  => (object) [
                'name'          => $CabinetInteriorName,
                'material_group' => $CabinetInteriorMatGroup,
                'brand'         => $CabinetInteriorBrand,
                'edge'          => $CabinetInteriorEdgeID
            ],
            "cabinet_exterior"  => (object) [
                'name'          => $CabinetExteriorName,
                'finish'        => $CabinetExteriorFinish,
                'color'         => $CabinetExteriorColor,
                'edge'          => $CabinetExteriorEdge
            ],
            "account_info" => (object) [
                'name'          => $acctname,
                'address'       => $acctaddress,
                'phone'         => $acctphone,
                'email'         => $acctemail
            ],
            "brand" => (object) [
                'id'            => $BrandID,
                'name'          => $BrandName,
                'cabcard'       => $BrandCabCard,
                'whitelabel'    => $BrandWhiteLabel
            ],
            "customer_info" => (object) [
                'name'          => $acctname,
                'address'       => $acctaddress,
                'phone'         => $acctphone,
                'email'         => $acctemail
            ],
            "shipping_info" => (object) [
                'name'          => $acctname,
                'address'       => $acctaddress,
                'phone'         => $acctphone,
                'email'         => $acctemail
            ],
            "drawer_box_name"   => $DrawerBoxName,
            "door_pull"         => $DoorPull,
            "toe_kick"          => $CabinetToeKickName,
            "logo_image_path"   => $logo_image_path,
            "admin_note"        => $AdminNote
        ];
        return $specifications;
    }
}
