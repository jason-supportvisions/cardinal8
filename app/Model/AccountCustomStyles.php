<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AccountCustomStyles extends Model {

    protected $table        = 'acct_custom_styles';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;

  
}
