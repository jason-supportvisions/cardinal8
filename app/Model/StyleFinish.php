<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class StyleFinish extends Model {

    protected $table        = 'style_finish';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;

}