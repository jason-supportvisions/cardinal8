<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Download extends Model
{
    use HasFactory;
    protected $table        = 'downloads';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;

    
}