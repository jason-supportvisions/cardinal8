<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductItemPricingOption extends Model {

    protected $table        = 'product_pricing';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];
    public $timestamps      = false;

    public function productItem()
    {
        return $this->belongsTo('App\Model\ProductItem', 'Code', 'Code');
    }

}
