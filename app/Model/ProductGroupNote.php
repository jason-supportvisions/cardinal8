<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductGroupNote extends Model {

    protected $table        = 'product_group_notes';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];
    public $timestamps      = false;

    public function productGroup()
    {
        return $this->belongsTo('App\Model\ProductGroup', 'GroupCode', 'GroupCode');
    }

}
