<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Specifications;
use App\Model\EstimationParameters;
use App\Model\JobSubstage;
use App\Model\JobHistory;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\JobProduct;



class Job extends Model
{

    protected $table        = 'job';

    protected $primaryKey   = 'JobID';

    protected $dates        = ['CreatedDate', 'EstOutProduction', 'AcceptedDate'];
    
    protected $appends      = [
        'summary_link',
        'job_number_link',
        'Net',
        'Fin',
        'finish_major_category',
        'EstOutProduction',
        'AcceptedDate',
        'rso_date'
    ];

    public $timestamps      = false;

    public static $rules    = [
        'Brand'             => 'required',
        'AccountID'         => 'required',
        'List'              => 'numeric',
        'EstOutProduction'  => 'date_format:m/d/Y',
        'AcceptedDate'      => 'date_format:m/d/Y',
        'Stage'             => 'required'
    ];

    public static $messages = [
        'date_format' => 'Dates must be in the format mm/dd/yyyy'
    ];

    public function getSpecificationsAttribute(){
        $job = $this;
        $jobID = $this->JobID;
        $jobAcct = $job->AccountID;
        return Specifications::getSpecifications($jobID, $jobAcct);
    }

    public static function getSystemAlert(){

        $AlertString = "";
        $SystemAlert = DB::table('app_alerts')
        ->select('*')
        ->distinct()
        ->where('Active', '=', 1)
        ->get();



        if (is_array($SystemAlert)) {

            foreach($SystemAlert as $row) {
                $Alert           = $row['Alert'];
                $Server          = $row['Server'];
                $ModifiedDate    = $row['ModifiedDate'];
                if ($_SERVER['SERVER_NAME'] == $Server) {
                    $AlertString .= '<br/><div class="alert alert-danger">';
                    $AlertString .= '<strong>** Application Alert ** </strong>: ';
                    $AlertString .= $Alert;
                    $AlertString .= '<div class="pull-right"> ' . date('F j, Y, g:i a', strtotime($ModifiedDate)) . '</div>';
                    $AlertString .= '</div>';
                }
            }
            return $AlertString;
        }
    }

    public function getMultiplierListToCustomerAttribute(){
        return $this->multiplier_discount * $this->multiplier_customer;
    }

    public function getProductCategoriesAttribute(){
        $cats = ['Wall', 'Base', 'Tall', 'Vanity', 'Cabinet', 'Accessory', 'Total', 'Custom'];
        $response = [];
        foreach ($cats as $cat) {
            $response[$cat] = ["Qty" => "0", "FinishedEnds" => "0"];
        }

        foreach ($this->products as $product) {

            if ($product->Type == "accessory") {
                $groupType = "accessory_group";
                $itemType = "accessory_item";
            } else {
                $groupType = "product_group";
                $itemType = "product_item";
            }
            //  print_r($product);
            // $response[$product->$itemType->Category]['Qty'] += $product->Qty;
            // $response[$product->$itemType->Category]['FinishedEnds'] += $product->finished_ends_count;
            // $response['Total']['Qty'] += $product->Qty;
            // $response['Total']['FinishedEnds'] += $product->finished_ends_count;
        }

        $response['Cabinet']['Qty'] = $response['Wall']['Qty']
            + $response['Base']['Qty']
            + $response['Tall']['Qty']
            + $response['Vanity']['Qty'];

        $response['Cabinet']['FinishedEnds'] = $response['Wall']['FinishedEnds']
            + $response['Base']['FinishedEnds']
            + $response['Tall']['FinishedEnds']
            + $response['Vanity']['FinishedEnds'];

        foreach ($response as $cat => $r) {
            $r['Qty'] = $r['Qty'] ? $r['Qty'] : '';
            $r['FinishedEnds'] = $r['FinishedEnds'] ? $r['FinishedEnds'] : '';
            $response[$cat] = (object) $r;
        }

        return (object) $response;
    }

    public static function validator($data){
        return Validator::make($data, self::$rules, self::$messages);
    }

    public function updateList(){
        $this->List     = 0;
        $this->Net      = 0;
        $this->Customer = 0;

        if(isset($this->products)){
            foreach ($this->products as $product) {

                //update NET
                $productNetPer        = $product->cost_per;
                $productNetTotal      = $product->cost_qty;
                $this->Net           += $productNetTotal;

                //update LIST
                $productListPer       = $product->list_per;
                $productListTotal     = $product->list_qty;
                $this->List          += $productListTotal;

                //update CUSTOMER
                $productCustomerPer   = $product->customer_per;
                $productCustomerTotal = $product->customer_qty;
                $this->Customer      += $productCustomerTotal;
            }
        }
        $this->save();
    }

    public function totalCount($property){
        $total = 0;

        foreach ($this->products as $product) {
            $groupType = "product_group";
            $productQty = $product->Qty;
            if($product->product_item){
                $total += ($product->{$groupType}->{$property} * $productQty);
            }
        }
        return $total ? $total : '';
    }

    public function getTotalAllAttribute(){
        return 5;
    }

    public function getProfitAttribute(){
        return $this->Customer - $this->Net;
    }

    public function getNetAttribute(){
        return $this->List * $this->multiplier_discount;
    }

    public function getCustomerAttribute(){
        return $this->List * $this->multiplier_list_to_customer;
    }

    public static function getReportArrayByStageId($stage_ids){
        $collection = self::getExpandedSetByStageId($stage_ids);

        $arr = $collection->map(function ($row) {
            return [
                'Status'    => $row->compound_stage_name,
                'Job ID'    => $row->JobID,
                'Reference' => $row->Reference,
                'Acct'      => $row->account->Name,
                'Brnd'      => $row->brand->abbreviation,
                'Fin'       => $row->Fin,
                'Net'       => $row->Net,
                'Deposit'   => $row->Deposit ? 'Yes' : 'No',
                'RSO'       => $row->rso_date,
                'Ship'      => $row->EstOutProduction
            ];
            
        });
        // dd($arr);
        return $arr;
    }

    public function getCompoundStageNameAttribute(){
        $name = $this->stage->Name;

        if ($substage = $this->substage) {
            $name .= ' - ' . $substage->name;
        }

        return $name;
    }

    public static function getExpandedSetByStageId($stage_ids){
        // convert to array if came in as single value
        if (!is_array($stage_ids)) {
            $stage_ids = [$stage_ids];

        }

        $set = self::orderBy('Deposit', 'DESC')
            // ->whereIn('Stage', $stage_ids)
            ->with('stage')
            ->with('substage')
            ->with('brand')
            ->with('account')
            ->with('door')
            ->with('type')
            ->with('history')
            ->with('construction.cabinetBoxMaterial')
            ->with('construction.cabinetDrawerBox')
            ->with('construction.styleGroup')
            ->get();

        return $set;
    }

    public function products(){
        return $this->hasMany('App\Model\JobProduct', 'JobID', 'JobID')->orderBy('Line');
    }

    public function history(){
        return $this->hasMany('App\Model\JobHistory', 'job_id', 'JobID');
    }

    public function stage(){
        return $this->belongsTo('App\Model\JobStage', 'Stage', 'ID');
    }

    public function substage(){
        return $this->belongsTo('App\Model\JobSubstage', 'substage_id', 'id');
    }
    
    public function brand(){
        return $this->belongsTo('App\Model\Brand', 'Brand', 'ID');
    }

    public function type(){
        return $this->belongsTo('App\Model\JobType', 'Type', 'ID');
    }

    public function door(){
        return $this->hasOne('App\Model\JobDoor', 'JobID', 'JobID');
    }

    private function getDoorField($field){

        if ($door = $this->door) {
            return $door->{$field};
        }
    }

    public function construction(){
        return $this->hasOne('App\Model\JobConstruction', 'JobID', 'JobID');
    }

    public function getGuideMaterialAttribute(){
        $name = $this->construction->cabinet_drawer_box->Name;

        $parts = explode('-', $name);

        return trim($parts[0]);
    }

    private function getPlywoodOrLaminate(){
        $value = $this->construction->cabinetDrawerBox->Name;
        return explode(' ',  $value)[0];
    }

    public function getBaseCabinetMaterialAttribute(){
        return $this->getPlywoodOrLaminate() == "Plywood" ?
            "Veneer/Prefinished Interior Generic" :
            "HPL/LPL Std Interior";
    }

    public function resetProductLines(){
        foreach ($this->products as $i => $product) {
            $product->Line = $i + 1;
            $product->save();
        }
    }

    public function getWallCabinetMaterialAttribute(){
        return $this->base_cabinet_material;
    }

    public function getBaseExposedCabinetMaterialAttribute(){
        return $this->getPlywoodOrLaminate() == "Plywood" ?
            "Veneer/Veneer Interior Generic" :
            "HPL/HPL Exposed Interior";
    }

    public function getWallExposedCabinetMaterialAttribute(){
        return $this->base_exposed_cabinet_material;
    }

    public function getRsoDateAttribute(){
        foreach ($this->history as $history) {
            if ($history->substage_id == 3) {
                return $history->created_at;
            }
        }
    }

    // public function getEstOutProductionAttribute($value){

    //     //  return $this->EstOutProduction;
    // }

    public function UpdateEstOutProduction(){

        $parameters         = EstimationParameters::retrieve();

        $parameters_array   = $parameters->toArray();

        $job_array          = $this->toArray();

        $estimation_values  = self::getEstimationValues($job_array, $parameters_array);

        if ($previous_job = $this->getPreviousJob()) {
            $work_start = max($previous_job->nextJobStart, date('Y-m-d H:m:s'));
        } else {
            $work_start = date('Y-m-d H:m:s');
        }

        $next_job_start_stamp = strtotime("{$work_start} + " . $estimation_values['total_blocking_days'] . "days");
        $est_out_production_stamp = strtotime("{$work_start} + " . $estimation_values['total_total_days'] . "days");

        $this->WorkStart    = $work_start;
        $this->NextJobStart = date('Y-m-d', $next_job_start_stamp);
        $this->EstOutProduction = date('Y-m-d', $est_out_production_stamp);
        $this->save();

        return $this;
    }

    public function getPreviousJob(){
        return Job::where('JobID', '<', $this->JobID)
            ->orderBy('JobID', 'DESC')
            ->first();
    }

    public static function getEstimationValues($job_array, $parameters){
        $finish_major_category = $job_array['finish_major_category'] ?
            $job_array['finish_major_category'] :
            'laminate';

        $normalized_boxes = $job_array['CabinetCount'] +
            $job_array['AccessoryCount'] / $parameters['accessories_per_box_equivalent'];

        // blocking
        $blocking_weeks_boxes = $parameters['blocking_boxes_weight'] *
            $normalized_boxes / $parameters['boxes_per_week'];

        $blocking_weeks_list = $parameters['blocking_list_value_weight'] *
            $job_array['List'] / $parameters['list_value_per_week'];

        $blocking_weeks_flat = $parameters["additional_blocking_weeks_for_{$finish_major_category}"];

        $total_blocking_weeks = round(($blocking_weeks_boxes +
            $blocking_weeks_list +
            $blocking_weeks_flat), 1);

        $total_blocking_days = round($total_blocking_weeks * 7);

        $total_blocking_equation = $parameters['blocking_boxes_weight'] . ' x '
            . '(' . $job_array['CabinetCount'] . ' + '
            . '(' . $job_array['AccessoryCount'] . ' / '
            . $parameters['accessories_per_box_equivalent'] . '))'
            . ' / ' . $parameters['boxes_per_week'] . ') + '
            . $parameters['blocking_list_value_weight'] . ' x '
            . '(' . $job_array['List'] . ' / '
            . $parameters['list_value_per_week'] . ') + '
            . $blocking_weeks_flat;

        // total
        $total_weeks_boxes = $parameters['total_boxes_weight'] *
            $normalized_boxes / $parameters['boxes_per_week'];

        $total_weeks_list = $parameters['total_list_value_weight'] *
            $job_array['List']  / $parameters['list_value_per_week'];

        $total_weeks_flat = $parameters['additional_total_weeks_for_' . $job_array['finish_major_category']];

        $total_total_weeks = round(($total_weeks_boxes +
            $total_weeks_list +
            $total_weeks_flat), 1);

        $total_total_days = round($total_total_weeks * 7);

        $total_total_equation = $parameters['total_boxes_weight'] . ' x '
            . '(' . $job_array['CabinetCount'] . ' + '
            . '(' . $job_array['AccessoryCount'] . ' / '
            . $parameters['accessories_per_box_equivalent'] . '))'
            . ' / ' . $parameters['boxes_per_week'] . ') + '
            . $parameters['total_list_value_weight'] . ' x '
            . '(' . $job_array['List'] . ' / '
            . $parameters['list_value_per_week'] . ') + '
            . $total_weeks_flat;

        return compact(
            'normalized_boxes',
            'blocking_weeks_boxes',
            'blocking_weeks_list',
            'blocking_weeks_flat',
            'total_blocking_weeks',
            'total_blocking_days',
            'total_blocking_equation',
            'total_weeks_boxes',
            'total_weeks_list',
            'total_weeks_flat',
            'total_total_weeks',
            'total_total_days',
            'total_total_equation'
        );
    }

    public function getAcceptedDateAttribute($value){
        // return date($this->getOriginal('AcceptedDate'));
    }

    public function getJobNumberLinkAttribute(){
    }

    public function getFinAttribute(){
        $response = "";
        if ($construction = $this->construction) {
            if ($style_group = $construction->style_group) {
                $response .= $style_group->Abbreviation;
            }
            $response .= '/';
            if ($style_finish = $construction->style_finish) {
                $response .= $style_finish->Abbreviation;
            }
        }
        return $response;
    }

    public function getFinishMajorCategoryAttribute(){
        if ($construction = $this->construction) {
            if ($style_group = $construction->style_group) {
                return $style_group->MajorCategory;
            }
        }
    }

    public function getCabinetBoxMaterialNameAttribute(){
        if ($construction = $this->construction) {
            if ($cabinet_box_material = $construction->cabinet_box_material) {
                return $cabinet_box_material->Name;
            }
        }
    }

    public function getMaterialGroupAttribute(){
        if ($construction = $this->construction) {
            return $construction->CabinetBoxMaterialGroup;
        }
    }

    public function getCabinetDrawerBoxNameAttribute(){
        if ($construction = $this->construction) {
            if ($cabinet_drawer_box = $construction->cabinet_drawer_box) {
                return $cabinet_drawer_box->Name;
            }
        }
    }

    public function getDoorStyleNameAttribute(){
        return $this->getDoorField('DoorStyleName');
    }

    public function getDoorMaterialNameAttribute(){
        return $this->getDoorField('StyleMaterialName');
    }

    public function getDoorColorNameAttribute(){
        return $this->getDoorField('StyleColorName');
    }

    public function getDoorFinishNameAttribute(){
        return $this->getDoorField('StyleFinishName');
    }

    public function account(){
        return $this->belongsTo('App\Model\Account', 'AccountID', 'ID');
    }

    public function user(){
        return $this->belongsTo('App\Model\User', 'CreatedID', 'UserID');
    }

    // todo: change this to call a partial
    public function getSummaryLinkAttribute() {
        $link = "<span class='job_JobID'><span>"
            . "<a target='_blank'"
            . "class='ewTooltipLink'"
            . "data-tooltip-id='job_{$this->JobID}'"
            . "data-tooltip-width='500'"
            . "data-placement='right'"
            . "data-container='body'"
            . "data-original-title=''"
            . "title=''"
            . "href='/quotes_orders/{$this->JobID}/pdf'>{$this->JobID}</a>"
            . "<span id='job_{$this->JobID}' style='display: none'>"
            . "    <strong>Door/Drawer Style: </strong> {$this->door_style_name} <br>"
            . "    <strong>Material: </strong> {$this->door_material_name} <br>"
            . "    <strong>Color: </strong> {$this->door_color_name} <br>"
            . "    <strong>Finish: </strong> {$this->door_finish_name} <br>"
            . "    <strong>Cabinet Interior: </strong> {$this->cabinet_box_material_name}<br>"
            . "    <strong>Drawer Box: </strong> {$this->cabinet_drawer_box_name}<br>"
            . "    <strong>Total Cabinets: </strong> {$this->CabinetCount} <br>"
            . "    <strong>Total Accessories: </strong> {$this->AccessoryCount}"
            . "</span></span></span>";

        return $link;
    }

    // todo: change this to call a partial
    public function getEditLinkAttribute() {
        $link = "/quotes_orders/header.php?"
            . "JobID={$this->JobID}&"
            . "JobAcct={$this->account->ID}";

        return $link;
    }

    public function save(array $options = array()){
        $original = $this->getOriginal();

        if (!$this->substage_id) {
            $this->substage_id = null;
        }

        // null out substage if it doesn't agree with stage
        if ($this->substage_id) {
            $substage = JobSubstage::find($this->substage_id);
            if ($substage->stage_id != $this->Stage) {
                $this->substage_id = null;
            }
        }

        parent::save();
        if ($this->Stage != $original['Stage'] || $this->substage_id != $original['substage_id']) {
            JobHistory::create([
                'job_id' => $this->JobID,
                'stage_id' => $this->Stage,
                'substage_id' => $this->substage_id,
                'user_id' => Auth::user()->UserID
            ]);
        }
        return $this;
    }
}
