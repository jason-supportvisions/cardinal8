<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AccessoryGroupNote extends Model {

    protected $table        = 'accessory_group_notes';
    protected $primaryKey   = 'id';
    protected $guarded      = ['id'];
    public $timestamps      = false;

    public function accessory_group()
    {
        return $this->belongsTo('App\Model\AccessoryGroup', 'GroupCode', 'GroupCode');
    }

}
