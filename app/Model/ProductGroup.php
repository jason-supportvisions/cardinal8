<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductGroup extends Model {


    protected $table        = 'product_groups';
    protected $primaryKey   = 'ID';
    protected $appends      = array('image_address');
    public $timestamps      = false;
    protected $dates        = ['deleted_at'];
    protected $guarded      = ['ID'];

    private $front_counts = [
        'DoorCount',
        'TallDoorCount',
        'FullTallDoorCount',
        'DrawerCount',
        'LargeDrawerCount',
        'OverlayCount',
        'PanelCount'
    ];

    

    public function update(array $attributes = Array(), array $options = [])
    {        
        unset($attributes['note_lines']);

        parent::update($attributes);
    }

        public function delete(array $attributes = Array())
    {        
    

        parent::update($attributes);
    }

    public function getTotalFrontCountAttribute()
    {   
        $total = 0;
        // dd($this);
        foreach($this->front_counts as $front_count){
            $total += $this->{$front_count};
        }
        return $total;
    }

      public function getHingeOptionsAttribute()
    {
        $all_options = ["Split", "Split (Left)", "Split (Right)", "Right", "Left", "Right/Right", "Left/Left", "Pull", "Lift", "None"];
        $these_options = explode(',', $this->HingeOption);
        $response = [];
        foreach($all_options as $all_option){
            if(in_array($all_option, $these_options)){
                $response[$all_option] = $all_option;
            }
        }
        return $response;
    }

    public function getFinishedEndOptionsAttribute()
    {
        $all_options = ["None", "Left", "Right", "Both"];
        $these_options = explode(',', $this->FinishedEndOption);
        $response = [];
        foreach($all_options as $all_option){
            if(in_array($all_option, $these_options)){
                $response[$all_option] = $all_option;
            }
        }
        // echo "<br>productGroup.php: finisedends $response";
        // print_r($response);
        return $response;
    }

    public function catalogCategory()
    {
        return $this->belongsTo('App\Model\CatalogCategory');
    }

    public function notes()
    {
        return $this->hasMany('App\Model\ProductGroupNote', 'GroupCode', 'GroupCode');
    }

    public function getNoteLinesAttribute()
    {
        $notes_array = $this->notes->map(function($note){return $note->Note;})->toArray();
        return implode("\n", $notes_array);
    }

    public function getExtendedNotesAttribute()
    {
        $notes = [];

        if($this->AdjShelfCount){
            $entry = $this->AdjShelfCount . ' ';
            $entry .= $this->AdjShelfCount > 1 ?
                     "Adjustable Shelves" :
                     "Adjustable Shelf";
            $notes[] = $entry;
        }

        if($this->DrawerBoxCount){
            $entry = $this->DrawerBoxCount . ' ';
            $entry .= $this->DrawerBoxCount > 1 ?
                     "Drawer Boxes" :
                     "Drawer Box";
            $notes[] = $entry;
        }

        if($this->LargeDrawerBoxCount){
            $entry = $this->LargeDrawerBoxCount . ' ';
            $entry .= $this->LargeDrawerBoxCount > 1 ?
                     "Large Drawer Boxes" :
                     "Large Drawer Box";
            $notes[] = $entry;
        }

        if($this->FixedShelfCount){
            $entry = $this->FixedShelfCount . ' ';
            $entry .= $this->FixedShelfCount > 1 ?
                     "Fixed Shelves" :
                     "Fixed Shelf";
            $notes[] = $entry;
        }

        if($this->PartitionCount){
            $entry = $this->PartitionCount . ' ';
            $entry .= $this->PartitionCount > 1 ?
                     "Partitions" :
                     "Partition";
            $notes[] = $entry;
        }

        if($this->Depth){
            if($this->Depth != 0){
                $notes[] = "Standard Depth " . ($this->Depth);
            }
        }

        foreach($this->notes as $note){
            $notes[] = htmlentities($note->Note);
        }

        return $notes;
    }

    public function items()
    {
        return $this->hasMany('App\Model\ProductItem', 'product_group_id', 'ID');
    }

    public function getCatalogImageAddressAttribute()
    {
        if($svg = $this->getExternalPathToFile('svg')){
            return $svg;
        }else {
            return $this->getExternalPathToFile('png');
        }
    }

    public function getImageAddressAttribute()
    {
        return $this->getExternalPathToFile('png');
    }

    public function getEmfAddressAttribute()
    {
        return $this->getExternalPathToFile('emf');
    }

    public function getSvgAddressAttribute()
    {
        if($address = $this->getExternalPathToFile('svg')){
            return $address;
        }
    }

    private function getExternalPathToFile($extension)
    {
        $external_path = "/images/cabinets/" . $this->GroupCode . "." . $extension;
        $internal_path = base_path() . "/public" . $external_path;

        $alternative_code = str_replace("(","_", $this->GroupCode);
        $alternative_code = str_replace(")","_", $alternative_code);
        $alternative_code = str_replace(" ","_", $alternative_code);
        $alternative_code = str_replace("+","-", $alternative_code);
        $alternative_external_path = "/images/cabinets/" . $alternative_code. "." . $extension;
        $alternative_internal_path = base_path() . "/public" . $alternative_external_path;

        if(file_exists($internal_path))
        {
            return $external_path;
        }elseif(file_exists($alternative_internal_path)){
            return $alternative_external_path;
        }
    }
}
