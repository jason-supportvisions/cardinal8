<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rules extends Model
{
    use HasFactory;
    protected $table        = 'rules';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;
    // public $incrementing    = false;
    // protected $keyType      = 'string';
}
