<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\DB;


class CatalogCategory extends Model {

    use HasFactory;

    public $timestamps      = false;
    protected $guarded      = ['id'];

    public function users()
    {
        return $this->hasMany('App\Model\User', 'AccountID', 'ID');
    }


    public static function all($columns = Array())
    {
        return self::orderBy('order')->get();
    }

    public function productGroups()
    {
        

        return $this->hasMany('App\Model\ProductGroup')
            ->orderBy('ClassBuild','desc') 
            ->orderBy('Category')            
            ->orderBy('Class')
            ->orderBy('SubClass')
            ->orderBy('catalog_order')
            ->orderBy('GroupCode')
            ->orderBy('Type')-> where('active', '=', 1);
        
            //make sure only active show up
    }
    
    public function accessoryGroups()
    {
        return $this->hasMany('App\Model\AccessoryGroup')
             ->orderBy('ClassBuild','desc') 
            ->orderBy('Category')            
            ->orderBy('Class')
            ->orderBy('SubClass')
            ->orderBy('catalog_order')
            ->orderBy('GroupCode')
            ->orderBy('Type')-> where('active', '=', 1);
    }

    public function modificationItemTypes()
    {
        return $this->hasMany('App\Model\ModificationItemType')
            ->orderBy('order')
            ->orderBy('type');
    }

}