<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
class JobHistory extends Model {

    public $timestamps      = true;

    protected $guarded      = ['id'];

    public function job()
    {
        return $this->belongsTo('App\Model\Job', 'job_id', 'JobID');
    }

    public function stage()
    {
        return $this->belongsTo('App\Model\JobStage', 'stage_id', 'ID');
    }

    public function substage()
    {
        return $this->belongsTo('App\Model\JobSubstage', 'substage_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id', 'UserID');
    }

}