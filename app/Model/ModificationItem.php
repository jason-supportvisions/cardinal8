<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class ModificationItem extends Model {

    protected $table        = 'modification_items';
    protected $primaryKey   = 'ID';
    public $guarded         = ['ID'];

    public $timestamps      = false;

    public static $formats  = [
        'bool' => 'bool',
        'meas' => 'meas',
        'text' => 'text',
        'int' => 'int'
    ];



}