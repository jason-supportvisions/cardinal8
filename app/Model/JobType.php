<?php
namespace APP\Model;
use Illuminate\Database\Eloquent\Model;

class JobType extends Model {

    protected $table        = 'job_types';
    protected $primaryKey   = 'ID';

    public $timestamps      = false;

}