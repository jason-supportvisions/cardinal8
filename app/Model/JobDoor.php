<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class JobDoor extends Model {

    protected $table        = 'job_door';
    protected $primaryKey   = 'JobID';

    public $timestamps      = false;

}