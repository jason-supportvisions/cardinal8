<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;



class Input extends Model
{
    /**
     * Get an item from the input data.
     *
     * This method is used for all request verbs (GET, POST, PUT, and DELETE)
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    public static function get($key = null, $default = null)
    {
        return request()->get($key, $default);
    }

    public static function get_all()
    {
        return request()->all();
    }
}