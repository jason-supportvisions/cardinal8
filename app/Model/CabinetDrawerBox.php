<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class CabinetDrawerBox extends Model {

    protected $table        = 'cabinet_drwbox';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;

}