<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;


class Account extends Model {

    protected $table        = 'account';
    protected $primaryKey   = 'ID';

    public $timestamps      = false;

    public function users()
    {
        return $this->hasMany('App\Model\User', 'AccountID', 'ID');
    }


    public function brands()
    {
        return $this->belongsToMany('App\Model\Brand', 'account_style_brand', 'account_id', 'style_brand_id');
    }

    public static $rules    = [
        'CompanyName' => 'required',
        'XDiscount' => 'numeric',
        'brand' => 'required'
    ];

    public static $messages = [
        'date_format' => 'Dates must be in the format mm/dd/yyyy'
    ];
    
    public static function validator($data)
    {
        return Validator::make($data, self::$rules, self::$messages);
    }

}