<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class CabinetMaterial extends Model {

    protected $table        = 'cabinet_material';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;

}