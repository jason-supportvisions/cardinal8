<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AccessoryItemPricingOption extends Model { 

    protected $table        = 'accessory_pricing';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];
    public $timestamps      = false;

    public function accessoryItem()
    {
        return $this->belongsTo('App\Model\AccessoryItem', 'Code', 'Code');
    }

}
