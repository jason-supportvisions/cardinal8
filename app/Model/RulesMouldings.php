<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RulesMouldings extends Model
{
    use HasFactory;
    protected $table        = 'rules_mouldings';
    protected $primaryKey   = 'ID';
    public $timestamps      = false;
    // public $incrementing    = false;
    // protected $keyType      = 'string';
}
