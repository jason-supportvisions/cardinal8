<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class JobStage extends Model {

    protected $table        = 'job_stages';
    protected $primaryKey   = 'ID';

    public $timestamps      = false;

    public function substages()
    {
        return $this->hasMany('App\Model\JobSubstage', 'stage_id', 'ID');
    }

    public function getSubstagesArray()
    {
        $substages = $this->substages->pluck('name', 'id');
     
        // if the substage set is empty, return null
        if(isset($substages)){return null;}
        
        return [''=>''] + $substages;
    }

}