<?php


namespace App\Model;

use Illuminate\Database\Eloquent\Model;


class Brand extends Model {

    protected $table        = 'style_brand';
    protected $primaryKey   = 'ID';

    public $timestamps      = false;

    public function getLogoImageSmallPathAttribute()
    {
        $path = "/images/logos/" . $this->logo_image_small;

        return $path;
    }

    public function getLogoImageLargePathAttribute()
    {
        $path = "/images/logos/" . $this->logo_image_large;

        return $path;
    }

    public function accounts()
    {
        return $this->belongsToMany('App\Model\Account');
    }

}