<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JobProductModification extends Model {

    protected $table        = 'job_product_modification';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;
    protected $fillable = [
        'Qty', 'Code', 'Value1', 'Value2', 'Description', 'Notes', 'OrigList', 'ModList', 'ListPer', 'CreatedBy', 'ModifiedBy', 'modification_item_id','job_product_id'
    ];
    
    public function job()
    {
        return $this->belongsTo('App\Model\Job', 'JobID', 'JobID');
    }

    public function jobProduct()
    {
        return $this->belongsTo('App\Model\JobProduct', 'job_product_id', 'ID');
    }

    public function modificationItem()
    {
        return $this->belongsTo('App\Model\ModificationItem', 'modification_item_id', 'ID');
    }

    public static function insert($data){
        $data = json_decode(json_encode($data), true);
        DB::table('job_product_modification')->insert($data);
    }

    public function getSnapieValueAttribute()
    {
        if($this->modificationItem->Value1Type == "bool"){
            return $this->modificationItem->bool_val;
        }else{
            return $this->Value1;
        }
    }

    public function getCvValueAttribute()
    {
        $val = $this->snapie_value;

        if($this->modificationItem->cv_type == "text"){
            return  "\"" . addslashes($val) . "\"";
        }else{
            return $val;
        }
    }

    public function getQuantityAttribute()
    {
        return $this->modificationItem->Value1Type == "int" ?
               $this->Value1 :
               1;
    }

    public function getAccountAttribute()
    {
        return $this->job_product->job->Account;
    }

    // public function getListPerAttribute()
    // {
    //     // echo "<br> adminList: $this->AdminList ";
    //     // echo "<br> ListPer: $this->ListPer ";
    //     // if($this->AdminList >= 1){
    //     //     return $this->AdminList;
    //     // }else{
    //         return $this->ListPer;
    //     // }
    // }

    public function getListModAttribute()
    {

        if($this->AdminList > 0){
            $total = $this->AdminList;
        }else{
            $total = $this->List;
        }
    }

    public function getListQtyAttribute()
    {
        if($this->AdminList != 0){
            return $this->AdminList * $this->Quantity;
        }else{
            return $this->ListPer * $this->Quantity;
        }
    }

    public function getCostPerAttribute()
    {
        return $this->ListPer * $this->jobProduct->job->multiplier_discount;
    }

    public function getCostQtyAttribute()
    {
        return $this->ListQty * $this->jobProduct->job->multiplier_discount;
    }

    public function getCustomerPerAttribute()
    {
        return $this->ListPer * $this->jobProduct->job->multiplier_list_to_customer;
    }

    public function getCustomerQtyAttribute()
    {
        return $this->ListQty * $this->jobProduct->job->multiplier_list_to_customer;
    }

    public function getCvOutputAttribute()
    {
        $raw = $this->cv_code;

        // if it's not a boolean, tack on the user supplied value
        if($this->modificationItem->Value1Type != "bool"){
            $raw .= $this->Value1;
        }

        $pos1 = strpos($raw, '"');
        $pos2 = strpos($raw, '"', $pos1 + 1);

        $described = $this->Description ?
                     substr_replace($raw, ' - '.$this->Description, $pos2, 0) :
                     $raw;

        return $described;
    }

    public function getCodeAttribute()
    {
        return $this->modificationItem->Code;
    }

    public function getModOrderAttribute()
    {
        return $this->modificationItem->mod_order;
    }
    
    public function getDescriptionAttribute()
    {
        return $this->modificationItem->Description;
    }

    public function updatePrice()
    {
        $this->ListQty = $this->ListPer * $this->Qty;
        $this->save();
    }
    

}