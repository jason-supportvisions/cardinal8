<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Quoteorders extends Model
{
    use HasFactory;

    public static function initData()
    {
        $job_account = DB::table('account')
            ->select('ID', 'Name')
            ->get();
        $job_stages = DB::table('job_stages')
            ->select('ID', 'Name')
            ->get();
        $job_types = DB::table('job_types')
            ->select('ID', 'Name')
            ->get();
        $states = DB::table('states')
            ->select('ID', 'Name')
            ->get();
        $value = array($job_stages, $job_types, $states, $job_account);
        return $value;
    }


    public static function getCopyJobData($jobID, $jobAcct){
        
        $jobResults = DB::table('job')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->where('AccountID', '=', $jobAcct)
            ->first();
        $cusResults = DB::table('job_header')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Customer')
            ->first();
        $shipResults = DB::table('job_header')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Shipping')
            ->first();  
        $specResults = DB::table('job_construction')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->first();  
        $historyResults = DB::table('job_histories')
            ->select('*')
            ->where('job_id', '=', $jobID)
            ->first();  
        
        $data = array(
            'cus_results'   => $cusResults,
            'ship_results'  => $shipResults,
            'job_histories' => $historyResults,
            'spec_results'  => $specResults,  
            'job_results'   => $jobResults,            
            'spec_results'  => $specResults,
            'origJobID'     => $jobID  
        );
        
        return $data;

    }

    public static function insertCopyData($data)
    {
        if(!isset($data->cus_results)){ 
            DB::table('job_header')->insert($data[0]);
            DB::table('job_header')->insert($data[1]);
            DB::table('job_histories')->insert($data[2]);
            DB::table('job_construction')->insert($data[3]);   
        }else{
            DB::table('job_header')->insert($data->cus_results);
            DB::table('job_header')->insert($data->ship_results);
            DB::table('job_histories')->insert($data->job_histories);
            DB::table('job_construction')->insert($data->spec_results);   
        }   
    }
    
    public static function insertData($data)
    {
        if(!isset($data->cus_results)){ 
            DB::table('job_header')->insert($data[0]);
            DB::table('job_header')->insert($data[1]);
            DB::table('job_histories')->insert($data[2]);
            // DB::table('job_construction')->insert($data[3]);   
        }else{
            DB::table('job_header')->insert($data->cus_results);
            DB::table('job_header')->insert($data->ship_results);
            DB::table('job_histories')->insert($data->job_histories);
            DB::table('job_construction')->insert($data->spec_results);   
        }
    }

    public static function getData($jobID, $jobAcct, $jobmod)
    {
        $job_account = DB::table('account')
            ->select('ID', 'Name')
            ->get();
        $job_stages = DB::table('job_stages')
            ->select('ID', 'Name')
            ->get();
        $job_types = DB::table('job_types')
            ->select('ID', 'Name')
            ->get();
        $states = DB::table('states')
            ->select('ID', 'Name')
            ->get();
        $jobResults = DB::table('job')
            ->select('AccountID', 'Type', 'Reference', 'Stage')
            ->where('JobID', '=', $jobID)
            ->where('AccountID', '=', $jobAcct)
            ->first();
        $cusResults = DB::table('job_header')
            ->select('ContactName', 'Company', 'AddressLine1', 'AddressLine2', 'City', 'State', 'PostalCode', 'PhoneNumber', 'EmailAddress', 'Notes')
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Customer')
            ->first();
        $shipResults = DB::table('job_header')
            ->select('ContactName', 'Company', 'AddressLine1', 'AddressLine2', 'City', 'State', 'PostalCode', 'PhoneNumber', 'EmailAddress', 'Notes')
            ->where('JobID', '=', $jobID)
            ->where('AddressType', '=', 'Shipping')
            ->first();            
   
             $value = array(
                'job_account' => $job_account,
                'job_stages'  => $job_stages,
                'job_types'   => $job_types,
                'states'      => $states,
                'jobID'       => $jobID,    
                'jobResults'  => $jobResults,
                'cusResults'  => $cusResults,
                'shipResults' => $shipResults,
                'smod'        => $jobmod
            );
        
        
        return $value;
    }

    public static function getItems($fastAddCode){
        $itemSQL    = '(SELECT ID, Type FROM accessory_items WHERE Code = "' . $fastAddCode . '") UNION (SELECT ID, Type FROM product_items WHERE Code = "' . $fastAddCode . '")';
        $results = DB::select(DB::raw($itemSQL));
        return $results;
    }

    public static function insertJob($data)
    {
        DB::table('job')->insert($data);
        return DB::table('job')->latest('jobID')->first();
    }

    public static function updateJob($jobID, $data)
    {
        DB::table('job') ->where('JobID', $jobID) ->limit(1) ->update($data);
        return 1;
    }

    public static function find($jobID)
    {
        $jobID = DB::table('job')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->get();
        return $jobID;
    }

    public static function getjobID($ref, $acct)
    {
        $jobID = DB::table('job')
            ->select('JobID')
            ->where('Reference', '=', $ref)
            ->where('AccountID', '=', $acct)
            ->first();
        return $jobID;
    } 

    public static function updateData($jobID, $data)
    {
        DB::table('job_header')->where([['JobID', '=', $jobID], 
                                        ['AddressType', '=', 'Customer'],
                                        ])->limit(1) ->update($data[0]);
        DB::table('job_header')->where([['JobID', '=', $jobID], 
                                        ['AddressType', '=', 'Shipping'],
                                        ])->limit(1) ->update($data[1]);
        
        DB::table('job_histories')->insert($data[2]);
    }

    public static function insertFiles($job_file)
    {
        DB::table('job_files')->insert($job_file);       
        return 1;
    }

    public static function updateFiles($id, $job_file)
    {
        DB::table('job_files')->where('ID', '=', $id)->update($job_file);       
        return 1;
    }

    public static function returnData($jobID, $jobAcct, $action)
    {
        $brand = DB::table('style_brand')
            ->select('*')
            ->where('active', '=', '1')
            ->get();
        $jobResults = DB::table('job_construction')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->get();
        $headernote = DB::table('job_construction')
            ->select('HeaderNote')
            ->where('JobID', '=', $jobID)
            ->get();
        $adminnote = DB::table('job_construction')
            ->select('AdminNote')
            ->where('JobID', '=', $jobID)
            ->get();
        $tipResults = DB::table('tips')
            ->select('Tip')
            ->orderByDesc('ID')
            ->limit(1)
            ->get();
        $data = array($jobID, $jobAcct, $brand, $jobResults, $headernote, $adminnote, $tipResults, $action);
        return $data;
    }

    public static function getnotes($jobID, $jobAcct){
        $results = DB::table('job_files')
            ->select('ID','Type','Title','File', 'FilePath', 'Note', 'Date')
            ->where('JobID', '=', $jobID)
            ->where('AccountID', '=', $jobAcct)
            ->get();
        return $results;
    }

    public static function geteditnotes($jobID, $jobAcct, $id){
        $results = DB::table('job_files')
            ->select('ID','Type','Title','File', 'Note', 'Date', 'FilePath')
            ->where('JobID', '=', $jobID)
            ->where('AccountID', '=', $jobAcct)
            ->where('ID', '=', $id)
            ->first();
        return $results;
    }

    public static function getsummary($jobID, $jobAcct)
    {

        $acctResults = DB::table('account')
            ->select('*')
            ->where('ID', '=', $jobAcct)
            ->get();
        if (is_array($acctResults)) {
            $AcctMultDis = $acctResults[0]->MultiplierDiscount;
        } else {
            $AcctMultDis = 1;
        }


        $jobResults = DB::table('job')
            ->select('*')
            ->where('JobID', '=', $jobID)
            ->get();
        $jobStage = $jobResults[0]->Stage;
        $jobtypes = $jobResults[0]->Type;
        $AcctMultCus = $jobResults[0]->multiplier_customer;
        if ($jobStage) {
            $jobstg = DB::table('job_stages')
                ->select('*')
                ->distinct()
                ->where('ID', '=', $jobStage)
                ->get();
        }
        if ($jobtypes) {
            $jobtype = DB::table('job_types')
                ->select('*')
                ->distinct()
                ->where('ID', '=', $jobtypes)
                ->get();
        }

        $cusResults = DB::table('job_header')
            ->select(
                'Company',
                'ContactName',
                'AddressLine1',
                'AddressLine2',
                'City',
                'State',
                'PostalCode',
                'PhoneNumber',
                'EmailAddress',
                'Notes'
            )
            ->where('JobID', '=', $jobID)
            ->where(function ($query) {
                $query->where('AddressType', '=', 'Customer');
            })
            ->get();

        $shipResults = DB::table('job_header')
            ->select(
                'Company',
                'ContactName',
                'AddressLine1',
                'AddressLine2',
                'City',
                'State',
                'PostalCode',
                'PhoneNumber',
                'EmailAddress',
                'Notes'
            )
            ->where('JobID', '=', $jobID)
            ->where(function ($query) {
                $query->where('AddressType', '=', 'Shipping');
            })
            ->get();

        $jobConst = DB::table('job_construction')
            ->select('*')
            ->distinct()
            ->where('JobID', '=', $jobID)
            ->where('AccountID', '=', $jobAcct)
            ->get();

        if (is_array($jobConst)) {
            $DoorStyleID        = $jobConst[0]->DoorStyleID;
            $StyleMaterialID    = $jobConst[0]->StyleMaterialID;
            $StyleColorID       = $jobConst[0]->StyleColorID;
            $StyleFinishID      = $jobConst[0]->StyleFinishID;

            if ($DoorStyleID && $StyleMaterialID && $StyleMaterialID && $StyleMaterialID) {
                $jobDoorFronts = DB::table('styles')
                    ->crossJoin('style_material')
                    ->crossJoin('style_color')
                    ->crossJoin('style_finish')
                    ->select(
                        'styles.ID AS StyleID',
                        'styles.Name AS StyleName',
                        'style_material.ID AS MaterialID',
                        'style_material.Name AS MaterialName',
                        'style_color.ID AS ColorID',
                        'style_color.Name AS ColorName',
                        'style_finish.ID AS FinishID',
                        'style_finish.Name AS FinishName',
                        DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                        DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                        DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                        DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                        DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                        DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                        DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                        DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
                    )
                    ->where('style_material.ID', '=', $StyleMaterialID)
                    ->where('style_color.ID', '=', $StyleColorID)
                    ->where('style_finish.ID', '=', $StyleFinishID)
                    ->where('styles.ID', '=', $DoorStyleID)
                    ->get();
            } else {
                $jobDoorFronts = '';
            }
            if (is_array($jobDoorFronts)) {
                $GroupID = $jobDoorFronts[0]->GroupID;
                $jobDoorStyleGroup = DB::table('style_group')
                    ->select('Name')
                    ->where('ID', '=', $GroupID)
                    ->get();
            } else {
                $jobDoorStyleGroup = '';
            }

            $LargeDrawerStyleID = $jobConst[0]->LargeDrawerStyleID;
            if ($LargeDrawerStyleID && $StyleMaterialID && $StyleMaterialID && $StyleMaterialID) {
                $jobLgDrwFronts = DB::table('styles')
                    ->crossJoin('style_material')
                    ->crossJoin('style_color')
                    ->crossJoin('style_finish')
                    ->select(
                        'styles.ID AS StyleID',
                        'styles.Name AS StyleName',
                        'style_material.ID AS MaterialID',
                        'style_material.Name AS MaterialName',
                        'style_color.ID AS ColorID',
                        'style_color.Name AS ColorName',
                        'style_finish.ID AS FinishID',
                        'style_finish.Name AS FinishName',
                        DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                        DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                        DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                        DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                        DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                        DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                        DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                        DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
                    )
                    ->where('styles.ID', '=', $LargeDrawerStyleID)
                    ->where('style_material.ID', '=', $StyleMaterialID)
                    ->where('style_color.ID', '=', $StyleColorID)
                    ->where('style_finish.ID', '=', $StyleFinishID)
                    ->get();
            } else {
                $jobLgDrwFronts = '';
            }
            $DrawerStyleID = $jobConst[0]->DrawerStyleID;
            if ($LargeDrawerStyleID && $StyleMaterialID && $StyleMaterialID && $StyleMaterialID) {
                $jobDrwFronts = DB::table('styles')
                    ->crossJoin('style_material')
                    ->crossJoin('style_color')
                    ->crossJoin('style_finish')
                    ->select(
                        'styles.ID AS StyleID',
                        'styles.Name AS StyleName',
                        'style_material.ID AS MaterialID',
                        'style_material.Name AS MaterialName',
                        'style_color.ID AS ColorID',
                        'style_color.Name AS ColorName',
                        'style_finish.ID AS FinishID',
                        'style_finish.Name AS FinishName',
                        DB::raw('(((styles.DoorList + style_material.DoorList) + style_color.DoorList) + style_finish.DoorList) AS DoorList'),
                        DB::raw('(((styles.TallDoorList + style_material.TallDoorList) + style_color.TallDoorList) + style_finish.TallDoorList) AS TallDoorList'),
                        DB::raw('(((styles.FullTallDoorList + style_material.FullTallDoorList) + style_color.FullTallDoorList) + style_finish.FullTallDoorList) AS FullTallDoorList'),
                        DB::raw('(((styles.DrawerList + style_material.DrawerList) + style_color.DrawerList) + style_finish.DrawerList) AS DrawerList'),
                        DB::raw('(((styles.LargeDrawerList + style_material.LargeDrawerList) + style_color.LargeDrawerList) + style_finish.LargeDrawerList) AS LargeDrawerList'),
                        DB::raw('(((styles.OverlayList + style_material.OverlayList) + style_color.OverlayList) + style_finish.OverlayList) AS OverlayList'),
                        DB::raw('(((styles.DoorSqFtList + style_material.DoorSqFtList) + style_color.DoorSqFtList) + style_finish.DoorSqFtList) AS DoorSqFtList'),
                        DB::raw('(((styles.DrawerSqFtList + style_material.DrawerSqFtList) + style_color.DrawerSqFtList) + style_finish.DrawerSqFtList) AS DrawerSqFtList')
                    )
                    ->where('styles.ID', '=', $DrawerStyleID)
                    ->where('style_material.ID', '=', $StyleMaterialID)
                    ->where('style_color.ID', '=', $StyleColorID)
                    ->where('style_finish.ID', '=', $StyleFinishID)
                    ->get();
            } else {
                $jobDrwFronts = '';
            }
            $CabinetDrawerBoxID = $jobConst[0]->CabinetDrawerBoxID;
            if ($CabinetDrawerBoxID) {
                $jobDrwBox = DB::table('cabinet_drwbox')
                    ->select('*')
                    ->distinct()
                    ->where('ID', '=', $CabinetDrawerBoxID)
                    ->get();
            } else {
                $jobDrwBox = '';
            }
            $CabinetBoxMaterialID = $jobConst[0]->CabinetBoxMaterialID;
            if ($CabinetBoxMaterialID) {
                $jobCabinetInterior = DB::table('cabinet_material')
                    ->select('*')
                    ->distinct()
                    ->where('ID', '=', $CabinetBoxMaterialID)
                    ->get();
            } else {
                $jobCabinetInterior = '';
            }
            $DoorProfile = DB::table('job_construction')
                ->leftJoin('style_outsideprofile', 'job_construction.DoorOutsideProfileID', '=', 'style_outsideprofile.ID')
                ->leftJoin('style_centerpanel', 'job_construction.DoorCenterPanelID', '=', 'style_centerpanel.ID')
                ->leftJoin('style_insideprofile', 'job_construction.DoorInsideProfileID', '=', 'style_insideprofile.ID')
                ->leftJoin('style_stilerail', 'job_construction.DoorStileRailID', '=', 'style_stilerail.ID')
                ->leftJoin('style_hardware', 'job_construction.DoorHardwareID', '=', 'style_hardware.ID')
                ->select(
                    'style_outsideprofile.Code AS Outside',
                    'style_insideprofile.Code AS Inside',
                    'style_stilerail.Code AS StileRail',
                    'style_centerpanel.Code AS CenterPanel',
                    'style_hardware.Code AS Pull',
                    'job_construction.DoorHardwareLocationID AS PullLocation'
                )
                ->distinct()
                ->where('job_construction.JobID', '=', $jobID)
                ->get();

            $LargeDrawerProfile = DB::table('job_construction')
                ->leftJoin('style_outsideprofile', 'job_construction.LargeDrawerOutsideProfileID', '=', 'style_outsideprofile.ID')
                ->leftJoin('style_centerpanel', 'job_construction.LargeDrawerCenterPanelID', '=', 'style_centerpanel.ID')
                ->leftJoin('style_insideprofile', 'job_construction.LargeDrawerInsideProfileID', '=', 'style_insideprofile.ID')
                ->leftJoin('style_stilerail', 'job_construction.LargeDrawerStileRailID', '=', 'style_stilerail.ID')
                ->select(
                    'style_outsideprofile.Code AS Outside',
                    'style_insideprofile.Code AS Inside',
                    'style_stilerail.Code AS StileRail',
                    'style_centerpanel.Code AS CenterPanel'
                )
                ->distinct()
                ->where('job_construction.JobID', '=', $jobID)
                ->get();
            $DrawerProfile = DB::table('job_construction')
                ->leftJoin('style_outsideprofile', 'job_construction.DrawerOutsideProfileID', '=', 'style_outsideprofile.ID')
                ->leftJoin('style_centerpanel', 'job_construction.DrawerCenterPanelID', '=', 'style_centerpanel.ID')
                ->leftJoin('style_insideprofile', 'job_construction.DrawerInsideProfileID', '=', 'style_insideprofile.ID')
                ->leftJoin('style_stilerail', 'job_construction.DrawerStileRailID', '=', 'style_stilerail.ID')
                ->select(
                    'style_outsideprofile.Code AS Outside',
                    'style_insideprofile.Code AS Inside',
                    'style_stilerail.Code AS StileRail',
                    'style_centerpanel.Code AS CenterPanel'
                )
                ->distinct()
                ->where('job_construction.JobID', '=', $jobID)
                ->get();
        } else {
            $jobDoorFronts      = '';
            $jobDoorStyleGroup  = '';
            $jobLgDrwFronts     = '';
            $jobDrwFronts       = '';
            $jobDrwBox          = '';
            $jobCabinetInterior = '';
            $DoorProfile        = '';
            $LargeDrawerProfile = '';
            $DrawerProfile      = '';
        }
        if (is_array($acctResults)) {
            $jobTotals = DB::table('job_product')
                ->select(
                    DB::raw('Sum(job_product.ListQty) AS ListQty'),
                    DB::raw("Sum(ROUND(ListQty * $AcctMultDis , 2)) AS CostQty"),
                    DB::raw("Sum(ROUND(ROUND(ListQty * $AcctMultDis , 2) * $AcctMultCus, 2)) AS CustomerQty"),
                    DB::raw('Sum(job_product.OrigList) AS OrigList'),
                    DB::raw('Sum(job_product.FrontList) AS FrontList'),
                    DB::raw('Sum(job_product.DrwBoxList) AS DrwBoxList'),
                    DB::raw('Sum(job_product.FinEndList) AS FinEndList'),
                    DB::raw('Sum(job_product.FinIntList) AS FinIntList'),
                    DB::raw('Sum(job_product.ModList) AS ModList'),
                    DB::raw('Sum(job_product.DoorCount) AS DoorCount'),
                    DB::raw('Sum(job_product.TallDoorCount) AS TallDoorCount'),
                    DB::raw('Sum(job_product.FullTallDoorCount) AS FullTallDoorCount'),
                    DB::raw('Sum(job_product.DrawerCount) AS DrawerCount'),
                    DB::raw('Sum(job_product.LargeDrawerCount) AS LargeDrawerCount'),
                    DB::raw('Sum(job_product.OverlayCount) AS OverlayCount'),
                    DB::raw('Sum(job_product.DoorCount+job_product.TallDoorCount+job_product.FullTallDoorCount+job_product.DrawerCount+job_product.LargeDrawerCount+job_product.OverlayCount) AS FrontCount'),
                    DB::raw('Sum(job_product.DrawerBoxCount) AS DrawerBoxCount'),
                    DB::raw('Sum(job_product.LargeDrawerBoxCount) AS LargeDrawerBoxCount'),
                    DB::raw('Sum(job_product.AdjShelfCount) AS AdjShelfCount'),
                    DB::raw('Sum(job_product.FixedShelfCount) AS FixedShelfCount'),
                    DB::raw('Sum(job_product.PartitionCount) AS PartitionCount'),
                    DB::raw('Sum(job_product.DividerCount) AS DividerCount'),
                    DB::raw('Sum(job_product.HingeCount) AS HingeCount'),
                    DB::raw('Sum(job_product.HandleCount) AS HandleCount'),
                    DB::raw('Sum(job_product.FinishedInteriorSqFt) AS FinishedInteriorSqFt'),
                    DB::raw('Sum(job_product.Volume) AS Volume'),
                    DB::raw('Sum(job_product.Weight) AS Weight'),
                    DB::raw('Sum(job_product.Qty) AS Qty'),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' THEN 1 *job_product.Qty END) AS QtyFinEnds"),
                    DB::raw("Sum(if(job_product.Category = 'Wall',job_product.Qty,0)) AS Walls"),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Wall' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Wall' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Wall' THEN 1 *job_product.Qty END) AS WallFinEnds"),
                    DB::raw("Sum(if(job_product.Category = 'Base',job_product.Qty,0)) AS Bases"),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Base' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Base' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Base' THEN 1 *job_product.Qty END) AS BaseFinEnds"),
                    DB::raw("Sum(if(job_product.Category = 'Tall',job_product.Qty,0)) AS Talls"),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Tall' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Tall' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Tall' THEN 1 *job_product.Qty END) AS TallFinEnds"),
                    DB::raw("Sum(if(job_product.Category = 'Vanity',job_product.Qty,0)) AS Vanities"),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Vanity' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Vanity' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Vanity' THEN 1 *job_product.Qty END) AS VanityFinEnds"),
                    DB::raw("Sum(if(job_product.Category = 'Accessory',job_product.Qty,0)) AS Accessories"),
                    DB::raw("Sum(CASE WHEN job_product.FinishedEnd = 'Both' AND job_product.Category = 'Accessory' THEN 2 * job_product.Qty WHEN job_product.FinishedEnd = 'Left' AND job_product.Category = 'Accessory' THEN 1 * job_product.Qty WHEN job_product.FinishedEnd = 'Right' AND job_product.Category = 'Accessory' THEN 1 *job_product.Qty END) AS AccessoryFinEnds")
                )
                ->distinct()
                ->where('JobID', '=', $jobID)
                ->get();
        } else {
            $jobTotals = '';
        }
        $value = array($jobID, $jobAcct, $acctResults, $jobResults, $jobstg, $jobtype, $cusResults, $shipResults, $jobConst, $jobDoorFronts, $jobDoorStyleGroup, $jobLgDrwFronts, $jobDrwFronts, $jobDrwBox, $jobCabinetInterior, $DoorProfile, $LargeDrawerProfile, $DrawerProfile, $jobTotals);
        return $value;
    }
}
