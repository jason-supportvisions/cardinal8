<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class JobProductPart extends Model {

    protected $table        = 'job_product_parts';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;
    protected $fillable = [
        'JobID', 'AccountID', 'Line', 'Code', 'OrigCode', 'Class', 'Type', 'Description', 'Width', 'Height', 'Depth', 'Qty', 'UOM', 'UOMPer', 'UOMQty', 'ListPer', 'ListQty',  'CreatedBy', 'StyleID', 'StyleMaterialID', 'StyleColorID', 'StyleFinishID', 'CabinetBoxMaterialGroup', 'CabinetBoxMaterialID', 'CabinetDrawerBoxID', 'StyleOverride', 'CabinetBoxOverride', 'CabinetDrawerBoxOverride', 'ProdID'
    ];    

}