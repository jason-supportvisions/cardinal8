<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Model\Component;
use App\Model\JobProductModification;
use App\Model\ProductItemPricingOption;
use App\Model\AccessoryItemPricingOption;
use App\Model\ModificationItem;
use App\Model\Input; 
use App\Model\User; 
use Illuminate\Support\Facades\Auth;



class JobProduct extends Model {
    use HasFactory;
    
    protected $table        = 'job_product';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    protected $fillable = [
        'JobID', 'product_item_id', 'AccountID', 'Type', 'Line', 'Qty', 'UOM', 'Code', 'OrigCode', 'Width', 'Height', 'Depth', 'Hinging', 'FinishedEnd', 'Description', 'Notes', 'OrigList', 'FrontList', 'DrwBoxList', 'FinEndList', 'FinIntList', 'ModList', 'GlassList', 'FinList', 'HandleList', 'MiscList', 'ListPer', 'ListQty', 'Fronts', 'Internal', 'DoorCount', 'TallDoorCount', 'FullTallDoorCount', 'DrawerCount', 'LargeDrawerCount', 'OverlayCount', 'FrameCount', 'DrawerBoxCount', 'LargeDrawerBoxCount', 'AdjShelfCount', 'FixedShelfCount', 'PartitionCount', 'DividerCount', 'HingeCount', 'HandleCount', 'FinishedInterior', 'FinishedInteriorSqFt', 'Volume', 'Weight', 'Category', 'StyleID', 'DoorStyleID', 'DoorHardwareID', 'LargeDrawerStyleID', 'LargeDrawerHardwareID', 'DrawerStyleID', 'DrawerHardwareID', 'StyleMaterialID', 'StyleMaterialGroupID', 'StyleColorID', 'StyleColorGroupID', 'StyleFinishID', 'StyleFinishGroupID', 'CabinetBoxMaterialGroup', 'CabinetBoxMaterialID', 'CabinetDrawerBoxID', 'StyleOverride', 'CabinetBoxOverride', 'CabinetDrawerBoxOverride', 'FrontLabel', 'FrontWidth', 'FrontHeight', 'FrontType','refnumber',
    ];

    public $timestamps      = false;
    public static $cab_cards_initial_mods_num = 6;
    public static $cab_cards_overflow_mods_num = 12;

    public function job()
    {
        return $this->belongsTo('App\Model\Job', 'JobID', 'JobID');
    }

    public function product_item()
    {
        return $this->belongsTo('App\Model\ProductItem', 'product_item_id', 'ID');
    }

    public function accessory_item()
    {
        return $this->belongsTo('App\Model\AccessoryItem', 'product_item_id', 'ID');
    }

    public function getHasFinishedInteriorAttribute()
    {
        return $this->FinishedInterior == 'Yes';
    }

    public function getFinishedEndsCountAttribute()
    {
        if($this->FinishedEnd == 'Left' || $this->FinishedEnd == 'Right'){
            return 1 * $this->Qty;
        }else if($this->FinishedEnd == 'Both'){
            return 2 * $this->Qty;
        }else {
            return 0;
        }
    }

       public function getCodeAttribute()
    {
        // dd($this);
        if($this->Type == "accessory"){           
                return (string)$this->accessory_item->Code;
        }elseif($this->Type == "product"){
                //return (string)$this->product_item->Code;            
        }elseif($this->product_type == "product"){
            //dd($this);
            if($this->product_item){
                return (string)$this->product_item->Code;
            }elseif($this->accessory_item){
                $this->product_type = "accessory";
                return (string)$this->accessory_item->Code;
            }

        }elseif($this->product_type == "accessory"){
                return (string)$this->accessory_item->Code;
        }
    }

    public function getGroupCodeAttribute()
    { 
        $itemType = Input::get('productType');
        if($itemType == ""){
            $itemType = $this->Type;            
        }
        if($itemType == "accessory"){
            return $this->accessory_group->GroupCode;
        }else{          
            return $this->product_group->GroupCode;
        }            
    }
    public function getComponent($component_slug)
    {
        foreach($this->components as $component){
            if($component->slug == $component_slug){
                return $component;
            }
        }
    }

    public function getComponentPrice($component_slug, $prop, $multiplier)
    {
        if($component = $this->getComponent($component_slug)){
            $list = $component->{$prop};
            if($multiplier == 'MultiplierDiscount'){
                return $list * $this->job->multiplier_discount;
            } elseif($multiplier == 'MultiplierCustomer'){
                return $list * $this->job->multiplier_customer;
            } else {
                return $list;
            }
        } 
    }

    public function getModificationsTotal()
    {
        
        $value = 0;
        foreach($this->modifications as $modification){
            $value += $modification->ListQty;
        }
        return $value;
    }

    public function getListPerAttribute()
    {
        
        if($this->AdminList > 0 ){
            //override any stored / calculated price
            $TotalList = $this->AdminList;
            
        }else{
            //recalculate price through component
            $TotalList = $this->getComponentsTotal();
        }
        
        //add in any modifications
            $ModTotal = $this->getModificationsTotal();
            $TotalList = $TotalList + $ModTotal;
        
        return $TotalList;
    }

    public function getComponentsTotal()
    {
        $value = 0;
        $i = 0;
        $qty = 0;
        $list = 0;
        $components = Component::getComponents($this);
        foreach($components as $component){           
            if($component->Qty > 0 && $component->ListPer > 0){                
                $value +=  $component->ListPer * $component->Qty; 
            }          
        } 
        return $value;
    }

    public function getComponentsAttribute()
    {
        $job_product = $this;
        return Component::getComponents($job_product);
    }

    public function getListQtyAttribute()
    {
        return $this->Qty * $this->ListPer;
    }    
    
    public function getCabfinextTextAttribute()
    {
        return "Material: {$this->job->specifications->style->material}, "
              ."Color: {$this->job->specifications->style->color}, "
              ."Finish: {$this->job->specifications->style->finish}";
    }
    
    public function getCostPerAttribute()
    {
        return $this->ListPer * $this->job->multiplier_discount;
    }

    public function getCostQtyAttribute()
    {
        return $this->ListQty * $this->job->multiplier_discount;
    }

    public function getCustomerPerAttribute()
    {
        return $this->ListPer * $this->job->multiplier_list_to_customer;
    }

    public function getCustomerQtyAttribute()
    {
        return $this->ListQty * $this->job->multiplier_list_to_customer;
    }

    public static function create(array $attributes)
    {
        $p = new JobProduct;
        foreach ($attributes as $key=>$value){
            $p->$key = $value;
        }
        $highest = JobProduct::where('JobID', $p->JobID)
                        ->orderBy('Line', 'DESC')
                        ->first();
        $p->Line = $highest ? $highest->Line + 1 :1;
        $p->save();
        return $p;
    }

    public function update(array $attributes = Array(), array $options = [])
    {
        // dd($attributes);

        $this->Type                     = isset($attributes['prodType']) ? $attributes['prodType'] : "product";
        $this->Qty                      = isset($attributes['Qty']) ? $attributes['Qty'] : 1;
        $this->UOM                      = isset($attributes['prodUOM']) ? $attributes['prodUOM'] : "";
        $this->Code                     = isset($attributes['Code']) ? $attributes['Code'] : "";
        $this->OrigCode                 = isset($attributes['Code']) ? $attributes['Code'] : "";
        $this->Width                    = isset($attributes['Width']) ? $attributes['Width'] : "";
        $this->Height                   = isset($attributes['Height']) ? $attributes['Height'] : "";
        $this->Depth                    = isset($attributes['Depth']) ? $attributes['Depth'] : "";
        $this->Hinging                  = isset($attributes['Hinging']) ? $attributes['Hinging'] : "";
        $this->Description              = isset($attributes['prodDesc']) ? $attributes['prodDesc'] : "";
        $this->AdminDescription         = isset($attributes['adminDescription']) ? $attributes['adminDescription'] : "";
        $this->AdminCode                = isset($attributes['adminCode']) ? $attributes['adminCode'] : "";
        $this->Notes                    = isset($attributes['Notes']) ? $attributes['Notes'] : "";
        $this->DoorCount                = isset($attributes['DoorCount']) ? $attributes['DoorCount'] : "";
        $DoorCount                      = isset($attributes['DoorCount']) ? $attributes['DoorCount'] : "";
        $this->TallDoorCount            = isset($attributes['TallDoorCount']) ? $attributes['TallDoorCount'] : "";
        $TallDoorCount                  = isset($attributes['TallDoorCount']) ? $attributes['TallDoorCount'] : "";
        $this->FullTallDoorCount        = isset($attributes['FullTallDoorCount']) ? $attributes['FullTallDoorCount'] : "";
        $FullTallDoorCount              = isset($attributes['FullTallDoorCount']) ? $attributes['FullTallDoorCount'] : "";
        $this->DrawerCount              = isset($attributes['DrawerCount']) ? $attributes['DrawerCount'] : "";
        $DrawerCount                    = isset($attributes['DrawerCount']) ? $attributes['DrawerCount'] : "";
        $this->LargeDrawerCount         = isset($attributes['LargeDrawerCount']) ? $attributes['LargeDrawerCount'] : "";
        $LargeDrawerCount               = isset($attributes['LargeDrawerCount']) ? $attributes['LargeDrawerCount'] : "";
        $this->OverlayCount             = isset($attributes['OverlayCount']) ? $attributes['OverlayCount'] : "";
        $OverlayCount                   = isset($attributes['OverlayCount']) ? $attributes['OverlayCount'] : "";
        $this->FrameCount               = isset($attributes['FrameCount']) ? $attributes['FrameCount'] : "";
        $FrameCount                     = isset($attributes['FrameCount']) ? $attributes['FrameCount'] : "";
        $this->DrawerBoxCount           = isset($attributes['DrawerBoxCount']) ? $attributes['DrawerBoxCount'] : "";
        $DrawerBoxCount                 = isset($attributes['DrawerBoxCount']) ? $attributes['DrawerBoxCount'] : "";
        $this->LargeDrawerBoxCount      = isset($attributes['LargeDrawerBoxCount']) ? $attributes['LargeDrawerBoxCount'] : "";
        $LargeDrawerBoxCount            = isset($attributes['LargeDrawerBoxCount']) ? $attributes['LargeDrawerBoxCount'] : "";
        $this->AdminList                = isset($attributes['AdminList']) ? $attributes['AdminList'] : "";
        $AdminList                      = isset($attributes['AdminList']) ? $attributes['AdminList'] : "";
        $this->CustomList               = isset($attributes['adminCustomList']) ? $attributes['adminCustomList'] : "";
        $CustomList                     = isset($attributes['adminCustomList']) ? $attributes['adminCustomList'] : "";


        $adminDoorCount                 = isset($attributes['adminDoorCount']) ? $attributes['adminDoorCount'] : 0;
        $adminTallDoorCount             = isset($attributes['adminTallDoorCount']) ? $attributes['adminTallDoorCount'] : "";
        $adminFullTallDoorCount         = isset($attributes['adminFullTallDoorCount']) ? $attributes['adminFullTallDoorCount'] : "";
        $adminDrawerCount               = isset($attributes['adminDrawerCount']) ? $attributes['adminDrawerCount'] : "";
        $adminLargeDrawerCount          = isset($attributes['adminLargeDrawerCount']) ? $attributes['adminLargeDrawerCount'] : "";
        $adminOverlayCount              = isset($attributes['adminOverlayCount']) ? $attributes['adminOverlayCount'] : "";
        $adminFrameCount                = isset($attributes['adminFrameCount']) ? $attributes['adminFrameCount'] : "";
        $adminDrawerBoxCount            = isset($attributes['adminDrawerBoxCount']) ? $attributes['adminDrawerBoxCount'] : "";
        $adminLargeDrawerBoxCount       = isset($attributes['adminLargeDrawerBoxCount']) ? $attributes['adminLargeDrawerBoxCount'] : "";
        $adminCabinetList               = isset($attributes['AdminList']) ? $attributes['AdminList'] : "";
        $adminHandlePullCount           = isset($attributes['adminHandlePullCount']) ? $attributes['adminHandlePullCount'] : "";

        if($adminDrawerCount > $this->DrawerBoxCount){
            $drawerBoxDiff                  = $adminDrawerCount - $this->DrawerBoxCount;
            $this->DrawerBoxCount           = $this->DrawerBoxCount + $drawerBoxDiff;
        }

        if($adminLargeDrawerCount > $this->LargeDrawerBoxCount){
            $lgDrawerBoxDiff                = $adminLargeDrawerCount - $this->LargeDrawerBoxCount;
            $this->LargeDrawerBoxCount      = $this->LargeDrawerBoxCount + $lgDrawerBoxDiff;
        }

        $this->DoorCount            = $this->DoorCount;     
        $this->TallDoorCount        = $this->TallDoorCount;
        $this->FullTallDoorCount    = $this->FullTallDoorCount;  
        $this->DrawerCount          = $this->DrawerCount;    
        $this->LargeDrawerCount     = $this->LargeDrawerCount;    
        $this->OverlayCount         = $this->OverlayCount;      
        $this->FrameCount           = $this->FrameCount;      
        $this->DrawerBoxCount       = $this->DrawerBoxCount;     
        $this->LargeDrawerBoxCount  = $this->LargeDrawerBoxCount;   
        $this->HandleCount          = $this->HandleCount;   
        
        $this->AdjShelfCount        = isset($attributes['AdjShelfCount']) ? $attributes['AdjShelfCount'] : "";
        $this->FixedShelfCount      = isset($attributes['FixedShelfCount']) ? $attributes['FixedShelfCount'] : "";
        $this->PartitionCount       = isset($attributes['PartitionCount']) ? $attributes['PartitionCount'] : "";
        $this->DividerCount         = isset($attributes['DividerCount']) ? $attributes['DividerCount'] : "";
        $this->HingeCount           = isset($attributes['HingeCount']) ? $attributes['HingeCount'] : "";
        $this->HandleCount          = isset($attributes['HandleCount']) ? $attributes['HandleCount'] : "";   
           
        //List Pricing
        $this->OrigList             = isset($attributes['prodOrigList']) ? $attributes['prodOrigList'] : "";
        $this->FrontList            = isset($attributes['prodFrontList']) ? $attributes['prodFrontList'] : "";
        $this->GlassList            = isset($this->getComponent('Glass')->ListPer) ? $this->getComponent('Glass')->ListPer : "";
        $this->DrwBoxList           = isset($attributes['prodDrwBoxList']) ? $attributes['prodDrwBoxList'] : "";
        $this->FinEndList           = isset($attributes['prodFinEndList']) ? $attributes['prodFinEndList'] : "";
        $this->FinIntList           = isset($this->getComponent('FinishedInterior')->ListQty) ? $this->getComponent('FinishedInterior')->ListQty : "";
        $this->ListPer              = isset($attributes['listTotal']) ? $attributes['listTotal'] : "";
        $this->ComponentList        = isset($attributes['componentList']) ? $attributes['componentList'] : "";    
        $this->listQty              = isset($attributes['listTotal']) ? $attributes['listTotal'] : "";
        
        //User
        $this->CreatedBy            = isset($attributes['prodCreatedBy']) ? $attributes['prodCreatedBy'] : "";
        $this->ModifiedBy           = isset($attributes['prodCreatedBy']) ? $attributes['prodCreatedBy'] : "";

        //Styles
        $this->Category             = isset($attributes['prodCat']) ? $attributes['prodCat'] : "";
        $this->DrawerStyleID        = isset($attributes['prodDrwStyID']) ? $attributes['prodDrwStyID'] : "";
        $this->DoorStyleID          = isset($attributes['prodDoorStyID']) ? $attributes['prodDoorStyID'] : "";
        $this->DoorHardwareID       = isset($attributes['prodDoorHdwID']) ? $attributes['prodDoorHdwID'] : "";
        $this->LargeDrawerStyleID   = isset($attributes['prodLgDrwStyID']) ? $attributes['prodLgDrwStyID'] : "";
        $this->LargeDrawerHardwareID = isset($attributes['prodLgDrwHdwID']) ? $attributes['prodLgDrwHdwID'] : "";
        $this->DrawerStyleID        = isset($attributes['prodDrwStyID']) ? $attributes['prodDrwStyID'] : "";
        $this->DrawerHardwareID     = isset($attributes['prodDrwHdwID']) ? $attributes['prodDrwHdwID'] : "";
        $this->StyleMaterialID      = isset($attributes['prodStyMaterialID']) ? $attributes['prodStyMaterialID'] : "";
        $this->StyleFinishID        = isset($attributes['prodStyFinishID']) ? $attributes['prodStyFinishID'] : "";
        $this->StyleColorID         = isset($attributes['prodStyColorID']) ? $attributes['prodStyColorID'] : "";
        $this->StyleFinishGroupID   = isset($attributes['prodStyFinishGroup']) ? $attributes['prodStyFinishGroup'] : "";
        $this->CabinetBoxMaterialGroup = isset($attributes['prodCabinetBoxMaterialGroup']) ? $attributes['prodCabinetBoxMaterialGroup'] : "";
        $this->CabinetBoxMaterialID = isset($attributes['prodCabinetBoxMaterialID']) ? $attributes['prodCabinetBoxMaterialID'] : "";
        $this->CabinetDrawerBoxID   = isset($attributes['prodCabinetDrawerBoxID']) ? $attributes['prodCabinetDrawerBoxID'] : "";
        $this->FrontType            = isset($attributes['prodFrontType']) ? $attributes['prodFrontType'] : "";
        $this->FrontLabel           = isset($attributes['prodFrontLabel']) ? $attributes['prodFrontLabel'] : "";
        $this->FrontWidth           = isset($attributes['prodFrontWidth']) ? $attributes['prodFrontWidth'] : "";
        $this->FrontHeight          = isset($attributes['prodFrontHeight']) ? $attributes['prodFrontHeight'] : "";
        $this->FinishedEnd          = isset($attributes['FinishedEnd']) ? $attributes['FinishedEnd'] : "";
        $this->FinishedInterior     = isset($attributes['FinishedInterior']) ? $attributes['FinishedInterior'] : "";
 
        $this->FrontSelection             = isset($attributes['frontSel']) ? $attributes['frontSel'] : 0;
        $this->FrontSelectionDoor         = isset($attributes['doorSty']) ? $attributes['doorSty'] : 0;

        // $this->FrontSelectionDoor         = $attributes['FrontSelectionDoor']) ? $attributes[''] : "";
        $this->FrontSelectionMaterial     = isset($attributes['styMatl']) ? $attributes['styMatl'] : 0;
        $this->FrontSelectionColor        = isset($attributes['styColor']) ? $attributes['styColor'] : 0;
        $this->FrontSelectionFinish       = isset($attributes['styFinish']) ? $attributes['styFinish'] : 0;
        
        $this->MiscList = $attributes['prodEdgeBandingList'];  
        $this->HandleList = $attributes['prodHandleList'];
        $this->save();
        $this->updateModifications($attributes['modification_items']);  
    }

    public function updateLine($new_line)
    {
        $old_line = $this->Line;
        $new_line = (int) $new_line;

        if($new_line > $old_line){
            $above_destination = self::where('JobID', $this->JobID)
                        ->where('Line', '>', $new_line)
                        ->orderBy('Line', 'DESC')
                        ->get();

            // bump up all above
            foreach($above_destination as $product){
                $product->Line += 1;
                $product->save();
            }

            $this->Line = $new_line+1;
            $this->save();
        } else {
            $above_destination = self::where('JobID', $this->JobID)
                        ->where('Line', '>=', $new_line)
                        ->orderBy('Line', 'DESC')
                        ->get();

            // bump up all above
            foreach($above_destination as $product){
                $product->Line += 1;
                $product->save();
            }

            $this->Line = $new_line;
            $this->save();
        }
        $this->job->resetProductLines();
    }

    public function getImageAddressAttribute()
    {
        //accessory
        if($product_item = $this->accessory_item){
            if($product_group = $product_item->accessory_group){
                return $product_group->image_address;
            }
        }else{
            $product_item = $this->product_item;
            if($product_group = $product_item->product_group){
                return $product_group->image_address;
            }
        }
    }

    public function getModificationsAttribute()
    {
        $modifications = JobProductModification::whereNotNull('job_product_id')
                            ->where('job_product_id', $this->ID)
                            ->get();

        foreach($modifications as $i => $modification){
            $modification->Line = $this->Line . "." . ($i+1);
        }
        return $modifications;
    }

    private function cabCardsInitialNumActual()
    {
        $num = self::$cab_cards_initial_mods_num;
        if($this->has_finished_interior){
            return $num -= 1;
        }
        return $num;
    }

    public function getCabCardInitialModificationsAttribute()
    {
        $mods = $this->modifications->slice(0, $this->cabCardsInitialNumActual());
        return $mods;
    }

    public function getCabCardOverflowModificationSetsAttribute()
    {
        $overflow_sets = [];
        $start = $this->cabCardsInitialNumActual();
        $length = self::$cab_cards_overflow_mods_num;
        while($start < $this->modifications->count()){
            $overflow_sets[] = $this->modifications->slice($start, $length);
            $start += $length;
        }
        return $overflow_sets;
    }

    public function getProductGroupAttribute()
    {
        if($product_item = $this->product_item){
            return $product_item->product_group;
        }
    
        if($product_item = $this->accessory_item){
            return $product_item->accessory_group;
        }
    }

    public function getPricingOptionAttribute()
    {
        if($product_item = $this->product_item)
        {
            $code = $product_item->Code;
            $material_group = $this->job->material_group;
            $pricing_option = ProductItemPricingOption::where('Code', $code)
                                ->where('MaterialGroup', $material_group)
                                ->first();
            return $pricing_option;
        }

        if($product_item = $this->accessory_item)
        {            
            $code = $product_item->Code;
            $pricing_option = AccessoryItemPricingOption::where('Code', $code)->first();
            return $pricing_option;
        }
    }

    public function getHasModsAttribute()
    {
        return $this->modifications->count() ||
               $this->FinishedInterior == 'Yes';
    }

    public function getCvCodeAttribute()
    {
        if($product_item = $this->product_item){
            if($product_group = $product_item->product_group){
                return $product_group->CVCode;
            }
        }
        if($product_item = $this->accessory_item){
            if($product_group = $product_item->accessory_group){
                return $product_group->CVCode;
            }
        }
    }

    public function updateModifications($modifications)
    {
        
        foreach($modifications as $modification_item_id => $props){

            $modification_item = ModificationItem::findOrFail($modification_item_id);
            $mod = JobProductModification::firstOrNew([
                'job_product_id' => $this->ID,
                'modification_item_id' => $modification_item_id
            ]);
            if(!isset($props['Value1']) || !$props['Value1']){
                $mod->delete();
                continue;
            }

            // these are the only things you can actually update
            $mod->Value1        = $props['Value1'];
            $mod->Notes         = $props['Notes'];
            $mod->ModifiedBy    = session('userid');
            $mod->ModifiedDate  = date("Y-m-d H:i:s");
            if(Auth::user()->canCustomizeModificationPrice($modification_item)){     
                $mod->ListPer = $props['ListPer'];
            } elseif(!$mod->id){
                $mod->ListPer = $modification_item->List;
            }
            if($modification_item->List == $props["ListPer"]){
                $mod->AdminList = 0.00;
            }else{
                $mod->AdminList = $props["ListPer"];
            }             
            $mod->save();
        }
    }
    
    public function getEndTypeCodeAttribute()
    {
        return $this->FinishedEnd ? $this->FinishedEnd[0] : '*';    
    }

     public function getHingeCodeAttribute()
    {
        $mapping = [
            'Right' => 'R',
            'Left'  => 'L',
            'Top'   => 'T',
            'Bottom' => 'B',
            'Split'  => 'P'
        ];

        return isset($mapping[$this->Hinging]) ? $mapping[$this->Hinging] : '*';
    }
}