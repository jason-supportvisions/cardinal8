<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AccessoryItem extends Model {

    protected $table        = 'accessory_items';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];
    public $timestamps      = false;

    public function accessory_group()
    {
        // return $this->belongsTo('AccessoryGroup', 'accessory_group_id', 'ID');
        return $this->belongsTo('App\Model\AccessoryGroup', 'GroupCode', 'GroupCode');
    }

    public function pricingOptions()
    {
        return $this->hasMany('App\Model\AccessoryItemPricingOption', 'Code', 'Code')
                ->orderBy('MaterialGroup');
    }

    public function getCatalogImageAddressAttribute()
    {
            return $this->getExternalPathToFile('png');
    }

    public function getImageAddressAttribute()
    {
        $external_path = "/images/accessories/" . $this->Code . ".png";
        if(file_exists($external_path)){
            return $external_path;
        }else{
            return $external_path;
        }
    }

    public function getEmfAddressAttribute()
    {
        return $this->getExternalPathToFile('emf');
    }

    public function getSvgAddressAttribute()
    {
        if($address = $this->getExternalPathToFile('svg')){
            return $address;
        }
    }

    private function getExternalPathToFile($extension)
    {
        $external_path = "/images/accessories/" . $this->Code . ".png";
        if(file_exists($external_path)){
            return $external_path;
        }
    }

}
