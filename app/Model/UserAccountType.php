<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class UserAccountType extends Model 
{

    protected $table        = 'user_account_type';
    protected $primaryKey   = 'ID';
    
    public $timestamps = false;

    public function user()
    {
        return $this->hasMany('User', 'AccountType', 'ID');
    }


}
