<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;

class EstimationParameters extends Model {

    protected $guarded      = ['id'];

    public static $rules    = [
        // business numbers
        'boxes_per_week' => 'required|integer',
        'accessories_per_box_equivalent' => 'required|integer',
        'list_value_per_week' => 'required|integer',

        'blocking_boxes_weight' => 'required|numeric',
        'blocking_list_value_weight' => 'required|numeric',
        'additional_blocking_weeks_for_laminate' => 'required|numeric',
        'additional_blocking_weeks_for_veneer' => 'required|numeric',
        'additional_blocking_weeks_for_paint' => 'required|numeric',

        'total_boxes_weight' => 'required|numeric',
        'total_list_value_weight' => 'required|numeric',
        'additional_total_weeks_for_laminate' => 'required|numeric',
        'additional_total_weeks_for_veneer' => 'required|numeric',
        'additional_total_weeks_for_paint' => 'required|numeric',
    ];

    public static $messages = [];

    public static function validator($data)
    {
        return Validator::make($data, self::$rules, self::$messages);
    }

    public static function retrieve()
    {
        return self::first();
    }

}