<?php

namespace App\Model;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $timestamps      = false;
    protected $fillable = [
        'username',
        'email',
        'password',
        'accounttype',
        'accountid',
        'firstname',
        'lastname',
        'addressline1',
        'addressline2',
        'state',
        'zipcode',      
        'phone',     
        'xcustomer',
        'includeinphonelog',
        'receive_door_spec_emailsreceive_door_spec_emails',
        'active',
        'ChatSupport',
        'AccountType'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table        = 'users';
    protected $primaryKey   = 'UserID';

    public function account() 
    {
        return $this->belongsTo('App\Model\Account', 'AccountID');
    }

    public function usermessage() 
    {
        return $this->belongsTo('App\Model\PhoneMessage', 'phone_message_id', 'UserID');
    }

    public function setPasswordAttribute($pass){

        $this->attributes['password'] = Hash::make($pass);
        
    }
    public function canCustomizeModificationPrice($modification_item)
    {
        return $modification_item->allow_price_customization &&
               $this->hasAccountType('master-administrator');
    }
    public function accountType()
    {
        return $this->belongsTo('App\Model\UserAccountType', 'AccountType', 'ID');
    }
    public function hasAccountType($account_type_slug)
    {   
        $account_type = $this->accountType;
        return $account_type_slug == $account_type->slug;
    }
}
