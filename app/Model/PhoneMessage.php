<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;

class PhoneMessage extends Model {

    public      $timestamps = true;
    protected   $fillable   = array('created_by', 'datetime', 'content', 'call_back_number');
    protected   $dates      = ['datetime'];
    protected   $appends    = array('assigned_to_list');

    public function getDatetimeAttribute($val)
    {
        return Carbon::parse($val)->format('m/d/Y g:ia');
        
    }

    public function setAll($values)
    {
        $old_assignee_ids = $this->assignees->pluck('UserID');
        $new_assignee_ids = isset($values['assignee_ids']) ?
                            $values['assignee_ids'] :
                            array();
        $values['datetime'] = isset($values['datetime']) ?
                              strtotime($values['datetime']) :
                              time();
                           
        $this->update($values);
       
            
       
        $this->assignees()->sync($new_assignee_ids);

        $this->alertNewAssignees($new_assignee_ids, $old_assignee_ids);        
        
    }

    public function getAssignedToListAttribute()
    {
        $set = $this->assignees->pluck('email');
        $str =$set->implode("<br>");
        return $str;
    }

    private function alertNewAssignees($new_assignee_ids, $old_assignee_ids)
    {
        // send emails to everyone that is attached now, and wasn't before
        foreach($new_assignee_ids as $new_assignee_id){
            if(!in_array($new_assignee_id, $old_assignee_ids->toArray())){
                $assignee = User::find($new_assignee_id);

                $phone_message = PhoneMessage::with('assigner')->where('id', '=', $this->id)->get();
                
                //$assignee->Mail::send("Phone Message {$this->datetime}", 'emails.phone-message', ['phone_message'=>$this], $this->assigner);
            }
        }
    }

    public static $rules = array(
        'datetime' => 'required|date_format:m/d/Y g:ia',
        'content' => 'required',
    );

    private static $messages = [
        'datetime.date_format' => 'Datetime must be formatted: mm/dd/yyyy hh:mm[am or pm]',
    ];

    public static function validator($data)
    {
        return Validator::make($data, self::$rules, self::$messages);
    }

    public function assigner()
    {
        return $this->belongsTo('App\Model\User', 'created_by', 'UserID');
    }

    public function assignees()
    {
        return $this->belongsToMany('App\Model\User', 'phone_message_user', 'phone_message_id', 'UserID');
    }



}