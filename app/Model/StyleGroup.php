<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class StyleGroup extends Model {

    protected $table        = 'style_group';
    protected $primaryKey   = 'ID';
    protected $guarded      = ['ID'];

    public $timestamps      = false;


}