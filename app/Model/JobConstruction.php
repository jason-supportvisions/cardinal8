<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;


class JobConstruction extends Model {

    protected $table        = 'job_construction';
    protected $primaryKey   = 'ID';

    public $timestamps      = false;

    public function job()
    {
        return $this->belongsTo('App\Model\Job', 'JobID', 'JobID');
    }

    public function cabinetBoxMaterial()
    {
        return $this->belongsTo('App\Model\CabinetMaterial', 'CabinetBoxMaterialID', 'ID');
    }

    public function cabinetDrawerBox()
    {
        return $this->belongsTo('App\Model\CabinetDrawerBox', 'CabinetDrawerBoxID', 'ID');
    }

    public function styleGroup()
    {
        return $this->belongsTo('App\Model\StyleGroup', 'StyleGroupID', 'ID');
    }

    public function styleFinish()
    {
        return $this->belongsTo('App\Model\StyleFinish', 'StyleFinishID', 'ID');
    }

}