<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Dashboard extends Model
{
    use HasFactory;
    
    public static function getData()
    {
        $sesAcct = session('accountid');
        $sesUser = session('userid');
        if(session('accounttype') == -1){
            $jobsAct = DB::table('job')
            ->join('job_stages', 'job.Stage', '=', 'job_stages.ID')
            ->join('job_types', 'job.Type', '=', 'job_types.ID')
            ->join('users', 'job.CreatedID', '=', 'users.UserID')
            ->join('account', 'job.AccountID', '=', 'account.ID')
            ->join('job_construction', 'job.JobID', '=', 'job_construction.JobID')
            ->join('styles', 'job_construction.DoorStyleID', '=', 'styles.ID')
            ->join('style_material', 'job_construction.StyleMaterialID', '=', 'style_material.ID')
            ->join('style_color', 'job_construction.StyleColorID', '=', 'style_color.ID')
            ->join('style_finish', 'job_construction.StyleFinishID', '=', 'style_finish.ID')
            ->join('cabinet_material', 'job_construction.CabinetBoxMaterialID', '=', 'cabinet_material.ID')
            ->join('cabinet_drwbox', 'job_construction.CabinetDrawerBoxID', '=', 'cabinet_drwbox.ID')
            ->select(
                'job.*',
                'users.*',
                'job_construction.*',
                'account.name',
                'job.AccountID',
                'job.Reference',
                'job.Type',
                'job_types.Name AS TypeName',
                DB::raw('DATE_FORMAT( IFNULL(job.ModifiedDate, job.CreatedDate), "%m/%d/%Y %h:%i %p") AS Date'),
                'job.Stage',
                'job_stages.Name AS StageName',
                'job.List',
                'job.Net',
                'job.EstLeadtime',
                'job.Deposit',
                'job.Balance',
                'job.EstOutProduction',
                'styles.Name AS StyleName',
                'style_material.Name AS StyleMaterialName',
                'style_color.Name AS StyleColorName',
                'style_finish.Name AS StyleFinishName',
                'cabinet_material.Name AS CabinetMaterialName',
                'cabinet_drwbox.Name AS CabinetDrwBoxName'
            )
            ->where('job.Active', '=', 1)
            ->orderByDesc('job.ModifiedDate')
            ->limit(6)
            ->get();

        $jobsDB = DB::table('job')
            ->join('job_stages', 'job.Stage', '=', 'job_stages.ID')
            ->join('job_types', 'job.Type', '=', 'job_types.ID')
            ->select(
                'job.JobID',
                'job.AccountID',
                'job.Reference',
                'job.Type',
                'job_types.Name AS TypeName',
                DB::raw('DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),"%m/%d/%Y %h:%i %p") AS Date'),
                'job.Stage',
                'job_stages.Name AS StageName',
                'job.List',
                'job.Net',
                'job.EstLeadtime',
                'job.Deposit',
                'job.Balance',
                'job.EstOutProduction'
            )
            ->where('job.Active', '=', 1)
            ->orderByDesc('job.ModifiedDate')
            ->limit(6)
            ->get();

        $jobsShip = DB::table('job')
            ->join('job_stages', 'job.Stage', '=', 'job_stages.ID')
            ->join('job_types', 'job.Type', '=', 'job_types.ID')
            ->select(
                'job.JobID',
                'job.AccountID',
                'job.Reference',
                'job.Type',
                'job_types.Name AS TypeName',
                DB::raw('DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),"%m/%d/%Y %h:%i %p") AS Date'),
                'job.Stage',
                'job_stages.Name AS StageName',
                'job.List',
                'job.Net',
                'job.EstLeadtime',
                'job.Deposit',
                'job.Balance',
                'job.EstOutProduction',
                'CabinetCount',
                'AccessoryCount',
                'Volume',
                'Weight'
            )
            ->where(function ($query) {
                $query->where('job.Stage', '<>', 1)
                    ->orwhere('job.Stage', '=', 7);
            })
            ->orderByDesc('job.ModifiedDate')
            ->limit(6)
            ->get();

        }elseif(session('accounttype') == 1){
            $jobsAct = DB::select(DB::raw("SELECT job.*, users.*, account.name, job.AccountID, job.Reference, job.Type, job_construction.*, 
                job_types.`Name` AS TypeName, 
                DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, 
                job.Stage, 
                job_stages.`Name` AS StageName, 
                job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction,
                styles.Name AS StyleName, 
                style_material.Name AS StyleMaterialName, 
                style_color.Name AS StyleColorName, 
                style_finish.Name AS StyleFinishName, 
                cabinet_material.Name AS CabinetMaterialName, 
                cabinet_drwbox.Name AS CabinetDrwBoxName 
                FROM job 						
                INNER JOIN job_stages ON job.Stage = job_stages.ID 
                INNER JOIN job_types ON job.Type = job_types.ID 
                INNER JOIN users ON job.CreatedID = users.UserID  
                INNER JOIN account ON job.AccountID = account.ID
                INNER JOIN job_construction on job.JobID = job_construction.JobID   
                INNER JOIN styles ON job_construction.DoorStyleID = styles.ID 
                INNER JOIN style_material ON job_construction.StyleMaterialID = style_material.ID 
                INNER JOIN style_color ON job_construction.StyleColorID = style_color.ID 
                INNER JOIN style_finish ON job_construction.StyleFinishID = style_finish.ID 
                INNER JOIN cabinet_material ON job_construction.CabinetBoxMaterialID = cabinet_material.ID 
                INNER JOIN cabinet_drwbox ON job_construction.CabinetDrawerBoxID = cabinet_drwbox.ID 
                WHERE job.AccountID = $sesAcct 
                AND job.Active = 1 
                ORDER BY job.ModifiedDate 
                DESC LIMIT 6
            ")); //ORDER BY IFNULL(job.ModifiedDate,job.CreatedDate) 

            $jobsDB = DB::select(DB::raw("SELECT job.*, job.AccountID, job.Reference, job.Type, job_types.`Name` AS TypeName, DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, job.Stage, job_stages.`Name` AS StageName, job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction FROM job INNER JOIN job_stages ON job.Stage = job_stages.ID INNER JOIN job_types ON job.Type = job_types.ID WHERE (job.Stage <> 1 OR job.Stage = 7) AND job.AccountID = $sesAcct AND job.Active = 1 ORDER BY job.EstOutProduction DESC LIMIT 6"));
            
            $jobsShip = DB::select(DB::raw("SELECT job.*, job.AccountID, job.Reference, job.Type, job_types.`Name` AS TypeName, DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, job.Stage, job_stages.`Name` AS StageName, job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction, CabinetCount, AccessoryCount, Volume, Weight FROM job INNER JOIN job_stages ON job.Stage = job_stages.ID INNER JOIN job_types ON job.Type = job_types.ID WHERE (job.Stage <> 1 OR job.Stage = 7) AND  (job.Stage <> 1 OR job.Stage = 7) AND job.AccountID = $sesAcct AND job.Active = 1 ORDER BY job.EstOutProduction DESC LIMIT 6"));
        }else{
            $jobsAct = DB::select(DB::raw("SELECT job.*, users.*, account.name, job.AccountID, job.Reference, job.Type, job_construction.*, 
                job_types.`Name` AS TypeName, 
                DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, 
                job.Stage, 
                job_stages.`Name` AS StageName, 
                job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction,
                styles.Name AS StyleName, 
                style_material.Name AS StyleMaterialName, 
                style_color.Name AS StyleColorName, 
                style_finish.Name AS StyleFinishName, 
                cabinet_material.Name AS CabinetMaterialName, 
                cabinet_drwbox.Name AS CabinetDrwBoxName 
                FROM job 						
                INNER JOIN job_stages ON job.Stage = job_stages.ID 
                INNER JOIN job_types ON job.Type = job_types.ID 
                INNER JOIN users ON job.CreatedID = users.UserID  
                INNER JOIN account ON job.AccountID = account.ID
                INNER JOIN job_construction on job.JobID = job_construction.JobID   
                INNER JOIN styles ON job_construction.DoorStyleID = styles.ID 
                INNER JOIN style_material ON job_construction.StyleMaterialID = style_material.ID 
                INNER JOIN style_color ON job_construction.StyleColorID = style_color.ID 
                INNER JOIN style_finish ON job_construction.StyleFinishID = style_finish.ID 
                INNER JOIN cabinet_material ON job_construction.CabinetBoxMaterialID = cabinet_material.ID 
                INNER JOIN cabinet_drwbox ON job_construction.CabinetDrawerBoxID = cabinet_drwbox.ID 
                WHERE job.AccountID = $sesAcct 
                AND job.Active = 1 
                AND job.CreatedID = $sesUser 
                ORDER BY job.ModifiedDate 
                DESC LIMIT 6
            "));

            $jobsDB = DB::select(DB::raw("SELECT job.*, job.AccountID, job.Reference, job.Type, job_types.`Name` AS TypeName, DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, job.Stage, job_stages.`Name` AS StageName, job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction FROM job INNER JOIN job_stages ON job.Stage = job_stages.ID INNER JOIN job_types ON job.Type = job_types.ID WHERE (job.Stage <> 1 OR job.Stage = 7) AND job.AccountID = $sesAcct AND job.Active = 1 AND job.CreatedID = $sesUser ORDER BY job.EstOutProduction DESC LIMIT 6"));
            
            $jobsShip = DB::select(DB::raw("SELECT job.*, job.AccountID, job.Reference, job.Type, job_types.`Name` AS TypeName, DATE_FORMAT( IFNULL(job.ModifiedDate,job.CreatedDate),'%m/%d/%Y %h:%i %p') AS Date, job.Stage, job_stages.`Name` AS StageName, job.List, job.Net, job.EstLeadtime, job.Deposit, job.Balance, job.EstOutProduction, CabinetCount, AccessoryCount, Volume, Weight FROM job INNER JOIN job_stages ON job.Stage = job_stages.ID INNER JOIN job_types ON job.Type = job_types.ID WHERE (job.Stage <> 1 OR job.Stage = 7) AND  (job.Stage <> 1 OR job.Stage = 7) AND job.AccountID = $sesAcct AND job.Active = 1 AND job.CreatedID = $sesUser ORDER BY job.EstOutProduction DESC LIMIT 6"));
        }

        if(session('accounttype') == -1){
            $jobResults = DB::table('job')
                ->select(
                    DB::raw('SUM(IF(job.Stage = 1,1,0)) AS  Quote'),
                    DB::raw('SUM(IF(job.Stage = 2,1,0)) AS Confirmed'),
                    DB::raw('SUM(IF(job.Stage = 3,1,0)) AS UnderReview'),
                    DB::raw('SUM(IF(job.Stage = 4,1,0)) AS Accepted'),
                    DB::raw('SUM(IF(job.Stage = 5,1,0)) AS Shipping'),
                    DB::raw('SUM(IF(job.Stage = 6,1,0)) AS Completed'),
                    DB::raw('SUM(IF(job.Stage = 7, 1, 0)) AS Cancelled'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 6, 1, 0)) AS TotalJobs'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 7, 1, 0)) AS Total')
                )
                ->get();

        }elseif(session('accounttype') == 1){
            $jobResults = DB::table('job')
                ->select(
                    DB::raw('SUM(IF(job.Stage = 1,1,0)) AS  Quote'),
                    DB::raw('SUM(IF(job.Stage = 2,1,0)) AS Confirmed'),
                    DB::raw('SUM(IF(job.Stage = 3,1,0)) AS UnderReview'),
                    DB::raw('SUM(IF(job.Stage = 4,1,0)) AS Accepted'),
                    DB::raw('SUM(IF(job.Stage = 5,1,0)) AS Shipping'),
                    DB::raw('SUM(IF(job.Stage = 6,1,0)) AS Completed'),
                    DB::raw('SUM(IF(job.Stage = 7, 1, 0)) AS Cancelled'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 6, 1, 0)) AS TotalJobs'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 7, 1, 0)) AS Total')
                )
                ->where('AccountID', '=', $sesAcct)
                ->get();
        }elseif(session('accounttype') == 2){
            $jobResults = DB::table('job')
                ->select(
                    DB::raw('SUM(IF(job.Stage = 1,1,0)) AS  Quote'),
                    DB::raw('SUM(IF(job.Stage = 2,1,0)) AS Confirmed'),
                    DB::raw('SUM(IF(job.Stage = 3,1,0)) AS UnderReview'),
                    DB::raw('SUM(IF(job.Stage = 4,1,0)) AS Accepted'),
                    DB::raw('SUM(IF(job.Stage = 5,1,0)) AS Shipping'),
                    DB::raw('SUM(IF(job.Stage = 6,1,0)) AS Completed'),
                    DB::raw('SUM(IF(job.Stage = 7, 1, 0)) AS Cancelled'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 6, 1, 0)) AS TotalJobs'),
                    DB::raw('SUM(IF(job.Stage >= 1 AND job.Stage <= 7, 1, 0)) AS Total')
                )
                ->where('AccountID', '=', $sesAcct)
                ->where('CreatedID', '=', $sesUser)
                ->get();
        }
                 

        $down = DB::table('downloads')
            ->select('*');
        $ndResults = DB::table('news')
            ->select('*')
            ->union($down)
            ->orderByDesc('Date')
            ->orderByDesc('Subject')
            ->limit(5)
            ->get();

        $randGallery = DB::table('gallery')
            ->select('gallery.Original_Image',
            'gallery.Title'
            )
            ->where('Featured', '=', '1')
            ->where(function ($query) {
                $query->where('Active', '=', '1');
            })
            ->orderByRaw("RAND()")
            ->limit(5)
            ->get();
            
        $value = array($jobsAct, $jobsDB, $jobsShip, $jobResults, $ndResults, $randGallery);
        return $value;
    }
}
