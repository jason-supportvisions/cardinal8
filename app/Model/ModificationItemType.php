<?php
namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ModificationItemType extends Model {

    protected $table        = 'modification_item_types';
    protected $primaryKey   = 'id';

    public $timestamps      = false;

    public function ModificationItems()
    {
        return $this->hasMany('App\Model\ModificationItem', 'Type', 'type')->orderBy('mod_order');
    }

    public function getActiveItemsAttribute()
    {
        $all = $this->ModificationItems;

        $active = $all->filter(function($item){
            return $item->Active;
        });

        return $active;
    }

    public static function all($columns = Array())
    {
        return self::orderBy('order')
            ->get();
    }

}