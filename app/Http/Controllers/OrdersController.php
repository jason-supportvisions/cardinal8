<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\JobStage;
use App\Model\Job;
use App\Model\Input;
use App\Model\Brand;
use App\Model\Account;
use Illuminate\Support\Facades\Response;
// use Excel

class OrdersController extends Controller
{       
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function inProcess()
    {
        // dd("orders controller in process");
        if(session('username')){
        
            $stage_ids = JobStage::where('in_process', 1)
                ->orderBy('order')
                ->get()
                ->pluck('ID');

            $page_title = "Orders - Overview";
            // $jobs = Job::getExpandedSetByStageId($stage_ids); 
            $jobs = Job::getExpandedSetByStageId("2,4"); 
            $job_stage_slug = 'Shipping';
            // $quote_stage = JobStage::where('slug', 'quote')->first();
            // $quote_substages = $quote_stage->substages;
  

            // return view('orders/index', ['page_title' => $page_title, 'stage_ids_list' => $stage_ids, 'job_stage_slug' => $job_stage_slug, 'jobs' => $jobs]);
            return view('orders/index')->with('page_title', $page_title)
                ->with('job_stage_slug', $job_stage_slug)
                ->with('stage_ids_list', $stage_ids)
                ->with('substages', $stage_ids)
                ->with('jobs', $jobs)
                ->with('initial_ordered_column', 0);
        }else{
            return redirect()->route('home');
        }
    }

    public function stage($job_stage_slug)
	{      
        $job_stage  = JobStage::where('slug', $job_stage_slug)->first();
        $page_title = "Orders - {$job_stage->Name}";
        $jobs       = Job::getExpandedSetByStageId($job_stage->ID);
        return view('orders/index', ['page_title' => $page_title, 'job_stage_slug' => $job_stage_slug, 'stage_ids_list' => $job_stage->ID, 'jobs' => $jobs]);
    }
    

    public function overview()
    {
        // $stage_ids = [2,4,5]; //shipped, accepted, confirmed
        $stage_ids = JobStage::where('in_process', 1)->get()->pluck('ID');
        // dd($stage_ids);
        $page_title = "Orders - Overview";
        $jobs = Job::getExpandedSetByStageId($stage_ids);
        $job_stage_slug = 'Overview';
        return view('orders/index', ['page_title' => $page_title,'job_stage_slug' => $job_stage_slug, 'stage_ids_list' => $stage_ids, 'jobs' => $jobs]);
    }

    public function all()
    {
        $stage_ids = JobStage::all()->pluck('ID');
        $page_title = "Orders - All";
        $jobs = Job::getExpandedSetByStageId($stage_ids);
        $job_stage_slug = 'All';
        // dd($jobs);
        return view('orders/index', ['page_title' => $page_title,'job_stage_slug' => $job_stage_slug, 'stage_ids_list' => $stage_ids, 'jobs' => $jobs]);
    }

    public function editBaseOrder($job_id)
    {
        return $this->edit($job_id, 'orders/edit');
    }

    public function updateBaseOrder($job_id, Request $request)
    {
        
        return $this->update($job_id, "/crm/orders", $request);
    }

    private function edit($job_id, $view)
    {
        $return_page = URL()->previous();

        $brands_list = Brand::all()->pluck('Name', 'ID');

        $accounts_list = Account::all()->pluck('Name', 'ID');

        $stages_list = JobStage::orderBy('order')->get()->pluck('Name', 'ID');

        $job = Job::where('JobID', $job_id)
                    ->with('history')
                    ->with('history.stage')
                    ->with('history.user')
                    ->first();
        return view('orders/edit', ['return_page' => $return_page, 'brands_list' => $brands_list, 'accounts_list' => $accounts_list, 'stages_list' => $stages_list, 'job' => $job]);
    }

    public function update($job_id, $return_url, $request)
    {
         $all = date('m/d/Y',strtotime($request->get('AcceptedDate') ?? time()));
         $acceptedDate = $request->get('AcceptedDate') ? strtotime($request->get('AcceptedDate')) : time();
        $job = Job::find($job_id);        
        // // $validator = Job::Validator($request->all());
        // if ($validator->fails()) {
        //     return Redirect()->back()->withErrors($validator)->withInput();
        // }

        $acceptedStageID        = JobStage::where('Name','=','Accepted')->firstOrFail();
        $job->Brand             = Input::get('Brand');
        $job->AccountID         = Input::get('AccountID');
        $job->Reference         = Input::get('Reference');
        $job->updateList();
        $job->List              = (Input::get('List') === '') ? null : Input::get('List');
        $job->Net               = (Input::get('Net') === '') ? null : Input::get('Net');
        
        // echo $request->get('Stage') . " = " .  $acceptedStageID->ID;
        // die();

        //if accepted
        if((int)$request->get('Stage') === $acceptedStageID->ID || $request->get('AcceptedDate')){
            $job->AcceptedDate      = date('m/d/Y',$acceptedDate);  
        }elseif(!$request->get('AcceptedDate')){
            $job->AcceptedDate      = null;
        }

        
        
        $job->EstOutProduction  = date('m/d/Y',strtotime(Input::get('EstOutProduction')));
        $job->Stage             = Input::get('Stage');
        $job->substage_id       = Input::get('substage_id');
        $job->Deposit           = Input::get('Deposit');        
        $job->save();

        return Redirect()->to($return_url)->with('success', "Order {$job->JobID} updated.");
    }

    public function toCvPreview($JobID)
    {
        $job = Job::find($JobID);
        return view('orders/to-cv-preview')->with('job', $job);
    }

    public function toCvDownload($JobID)
    {
        $job = Job::find($JobID);
        $content = view('orders.to-cv-download', ['job' => $job]);
        // convert to have windows style line endings
        // $content = preg_replace('~\R~u', "/\r/\n", $content);
        // $content = htmlspecialchars_decode($content);

        return Response::make($content, '200', array(
            'Content-Type' => 'application/octet-stream',
            'Content-Disposition' => "attachment; filename=\"{$JobID}.ord\""
        ));
    }

}
