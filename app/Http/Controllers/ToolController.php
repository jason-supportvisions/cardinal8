<?php
namespace App\Http\Controllers;

class ToolController extends Controller 
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		if(session('username')){
			
			return view('tools/index');

        }else{
            return redirect()->route('home');
        }
		
	}

	public function abbreviations()
	{
		return view('tools/abbreviations');
    }
    
    public function downloads()
	{
		return view('tools/downloads');
    }
    
    public function faq()
	{
		return view('tools/faq');
	}

    public function glossary()
	{
		return view('tools/glossary');
	}

    public function news()
	{
		return view('tools/news');
	}

 
}
