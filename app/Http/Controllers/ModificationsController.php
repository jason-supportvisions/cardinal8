<?php
namespace App\Http\Controllers;

use App\Model\ModificationItem;
use App\Model\ModificationItemType;
use App\Model\Input;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ModificationsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
        if(!Auth::user()->hasAccountType('master-administrator')){
            abort(403, 'Unauthorized action.');
        }
        $mods = ModificationItem::orderBy('mod_order')->orderBy('catalog_category_id')->get();

        $mod_types = [''=>''] + DB::table('modification_item_types')->orderBy('order', 'ASC')->get()->pluck('type','type')->toArray();

        $formats = [''=>''] + ModificationItem::$formats;

        $bool_vals = [''=>'', '0'=>'0', '1'=>'1'];
        return view('modifications/index')->with(compact(
			'mods', 
			'mod_types',
			'formats',
			'bool_vals'
		));

	}

    public function store()
    {
        foreach(Input::get('mods') as $id => $mod_data){

            if(!isset($mod_data['Active'])){
                $mod_data['Active'] = 0;
            }
            
            ModificationItem::find($id)
                ->update($mod_data);
        }

        return redirect()->back()->with('success', 'Modifications Updated');

    }

   

}
