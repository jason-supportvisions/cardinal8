<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Model\Input;
// use PHPMaker2020\managerCOLOR\edge_banding;
use App\Model\Edge\Edge_banding;

class AjaxController extends Controller
{
    public function ajaxRequestPost(Request $request)
    {
        date_default_timezone_set('america/new_york');

        $brandList = "1,2,3";
        $Inside = "";
        $CenterPanel = "";
        $Outside = "";
        $StileRail = "";
        $Hardware = "";
        $postAjax       = Input::get('Ajax');
        $postJobID      = Input::get('jobId');
        $postID         = Input::get('id');
        $postBrandID    = Input::get('brandId');
        $defaultSelected = "";

        if ($postAjax == 'StyleGroup') {

            $html           =  "";
            $jobid          = isset($postJobID) ? $postJobID : 10000;
            $id             = isset($postID) ? $postID : 1;
            $SpecDefaultID  = $this->getDefaultSpec("style_brand", $id, "SpecDefault", "StyleGroup");
            $useSpecDefault = "yes  ";
            // echo "<script>console.log('SpecDefaultID:" . $SpecDefaultID . "');</script>";   
            
            
            
            $Results = DB::select(DB::raw("SELECT StyleGroupID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionID = "";
            foreach ($Results as $row) {
                $ConstructionID = $row->StyleGroupID;
            }
            // echo "<script>console.log('constructionID = " . $ConstructionID . "');</script>";       
            
            
            
            
            $doorStyleResults = DB::select(DB::raw("SELECT ID, Name FROM style_group WHERE Active = 1 ORDER BY `ListOrder` ASC"));
            foreach ($doorStyleResults as $row) {


                //EXISTING JOB
                if($ConstructionID != ""){

                     //if it's already been selected then override default and set this to selected
                    if ($ConstructionID == $row->ID) {
                        // $html .= "<script>console.log('ALREADY SAVED AS ConstructionID == $row->ID');</script>";
                        $defaultSelected = " selected";
                        $useSpecDefault = "no";
                    }else{
                        // $html .= "<script>console.log('$ConstructionID != $row->ID)');</script>";
                        $defaultSelected = "";
                        
                    }

                //NEW JOB w/DEFAULT
                }elseif($useSpecDefault != "no"){

                    if ($row->ID == $SpecDefaultID) {
                        // $html .= "<script>console.log('SpecDefault, $SpecDefaultID == $row->ID');</script>";
                            $defaultSelected = " selected";
                    } else {
                        // $html .= "<script>console.log('row ID != specdefault && Construction is new');</script>";
                        $defaultSelected = "";
                    }

                }

                $html .= '<option value="'.$row->ID.' " '.$defaultSelected.'>' . $row->Name . '</option>';
            }

            echo $html;
        }


        if ($postAjax == 'CabinetMatl') {
            
            $html               = '';
            $id                 = isset($postID) ? $postID : 1; //default to imperia
            $jobId              = isset($postJobID) ? $postJobID : 1; 
            $ConstructionID     = 0;



            //do you already have a selection in job construction
            $ConstructionResults = DB::select(DB::raw("SELECT CabinetBoxMaterialID FROM job_construction WHERE JobID = $jobId"));
            foreach ($ConstructionResults as $row) {
                $ConstructionID = $row->CabinetBoxMaterialID;
            }
            // $html .= "<script>console.log('Brand ID after sent to ajax: " . $id . "');</script>";
            // $html .= "<script>console.log('Job ID: " . $jobId . "');</script>";
            // $html .= "<script>console.log('construction ID: " . $ConstructionID . "');</script>";
            $doorStyleResults = DB::select(DB::raw("SELECT * FROM cabinet_material WHERE BrandID LIKE '%$id%' and Active = 1 ORDER BY Name ASC"));
            
            foreach ($doorStyleResults as $row) {

                if ($row->ID == $ConstructionID) {
                    // $html .= "<script>console.log('does ID = ConstructionID? " . $ConstructionID . " = ". $row->ID  ."');</script>";                   
                    $html .= "<option value=\"" . $row->ID . "\" selected>" . $row->Name . "</option>";
                } else {
                    // $html .= "<script>console.log('does ID = ConstructionID? " . $ConstructionID . " = ". $row->ID  ."');</script>";                   
                    $html .= "<option value=\"" . $row->ID . "\">" . $row->Name . "</option>";
                }
            }
            echo $html;
        }

        if ($postAjax == 'CabinetDrwBox') {

            $jobid     = isset($postJobID) ? $postJobID : 0;
            $id     = isset($postID) ? $postID : 1; //default to imperia
            $html = '';
            $html .= "<option></option>";
            // $brandID    = isset($postBrandID) ? $postBrandID : 1;



            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT CabinetDrawerBoxID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionID = "";

            foreach ($Results as $row) {
                $ConstructionID = $row->CabinetDrawerBoxID;
            }

            //get the brand's default drawerbox
            $Results = DB::select(DB::raw("SELECT DrwBoxDefault FROM style_brand WHERE ID = $id AND Active =1"));
            $DrwBoxDefaultID = "";

            foreach ($Results as $row) {
                $DrwBoxDefaultID = $row->DrwBoxDefault;
            } 
            
            $constructionUsed = "";
            $defaultUsed = "";

            // //user imperia 1 for all other brands
            // if($id == 3 || $id == 4 || $id == 5){
            //     $id = 1; //make it use imperia
            // }
            
            $doorStyleResults = DB::select(DB::raw("SELECT ID, Name, BrandID FROM cabinet_drwbox WHERE Active = 1 AND BrandID LIKE '%$id%' ORDER BY Name ASC"));
            foreach ($doorStyleResults as $row) {


                //construction spec?
                if ($ConstructionID == $row->ID) {
                    $defaultSelected = " selected";
                    $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                    $constructionUsed = TRUE;
                }elseif($constructionUsed != TRUE && $DrwBoxDefaultID == $row->ID){
                    $defaultSelected = " selected";
                    $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                    $defaultUsed = TRUE;
                }else{
                    $html .= "<option value=\"" . $row->ID . "\">" . $row->Name . "</option>";
                }
               
            }

            echo $html;
        }

        if ($postAjax == 'Hinges') {


            //get vars from construction.php call
            $jobid     = isset($postJobID) ? $postJobID : 0;
            $id     = isset($postID) ? $postID : 1; //default to imperia
            $html   = '';
            //why blank option
            $html .= "<option></option>";

            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT CabinetHingeID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionID = "";

            foreach ($Results as $row) {
                $ConstructionID = $row->CabinetHingeID;
            }

            //get all options
            $doorStyleResults = DB::select(DB::raw("SELECT ID, Name, BrandID FROM cabinet_hinge ORDER BY Name ASC"));

            foreach ($doorStyleResults as $row) {

                //set this to selected
                if ($ConstructionID == $row->ID || $row->BrandID == 0) {
                    $defaultSelected = " selected";
                } else {
                    $defaultSelected = "";
                }

                if ($id == $row->BrandID || $row->BrandID == 0) {
                    $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                }
            }
            echo $html;
        }

        if ($postAjax == 'CabinetDrwRunner') {
            //get vars from construction.php call
            $jobid     = isset($postJobID) ? $postJobID : 0;
            $id     = isset($postID) ? $postID : 1; //drwbox ID above
            $html   = '';

            $html .= "<option></option>";

             //get all the guide material IDs
            $ResultsGuides = DB::select(DB::raw("SELECT guide_material FROM cabinet_drwbox WHERE ID = $id"));
            foreach ($ResultsGuides as $row) {
                $allGuides = $row->guide_material;
            }

            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT DrwRunnerID FROM job_construction WHERE JobID = $jobid"));
            if (count($Results)) {
                foreach ($Results as $row) {
                    $ConstructionID   = $row->DrwRunnerID;
                }
            } else {
                    $ConstructionID = 0;
            }

            
            // echo "<script>console.log('ConstructionID Runner: '  . $ConstructionID);</script>";

            //get all options
            $doorStyleResults = DB::select(DB::raw("SELECT ID, Name FROM `cabinet_drwrunner` WHERE ID IN ($allGuides) ORDER BY Name ASC"));

            foreach ($doorStyleResults as $row) {

                //set this to selected
                if ($ConstructionID  == $row->ID || $ConstructionID == 0) {
                    $defaultSelected = " selected";
                } else {
                    $defaultSelected = "";
                }

                // if ($id == $row->BrandID || $row->BrandID == 0) {
                    $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                // }
            }
            echo $html;
        }

        if ($postAjax == 'AdminNote') {


            //get vars from construction.php call
            $jobid     = isset($postJobID) ? $postJobID : 0;
            $id     = isset($postID) ? $postID : 1; //default to imperia

            $Results = DB::select(DB::raw("SELECT AdminNote FROM job_construction WHERE JobID = $jobid"));
            $AdminNote = "";

            foreach ($Results as $row) {
                $AdminNote .= $row->AdminNote;
            }

            echo $AdminNote;
        }

        if ($postAjax == 'CabinetEdge') {
            // echo "<option>CabinetEdge Called()</option>";
            $ColorID        = isset($postID) ? $postID : 0;
            $materialid     = Input::get('MaterialID');
            $MaterialID     = isset($materialid ) ? $materialid : 0;
            $jobid          = isset($postJobID) ? $postJobID : 0;
            $html           = '';

            //do you already have a value saved?
            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT CabinetExteriorEdgeID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionEdgeID = 0;

            if(count($Results)){
                foreach ($Results as $row) {
                    $ConstructionEdgeID = $row->CabinetExteriorEdgeID;  
                    // echo "<option>Job_construction entry found</option>";

                }
            }           

            //if It's a wood species it will show up in this result
            $edgeWoodResults = DB::select(DB::raw("SELECT EdgeBandingID FROM `edge_banding_wood` WHERE MaterialID = $MaterialID AND ColorID = $ColorID"));
            $mysqlNumCheckWood = count($edgeWoodResults);

            //if it is wood 
            if ($mysqlNumCheckWood != 0) {
            // echo "<option>Wood = TRUE</option>";
                foreach ($edgeWoodResults as $row) {
                    $EdgeID                = $row->EdgeBandingID;
                    $edgeResults     = DB::select(DB::raw("SELECT  e.ID as EdgeID,
													e.Name as EdgeName,
                                                    e.Option1 as Option1ID,
                                                    e.Option2 as Option2ID
										FROM edge_banding e 
										WHERE e.ID = $EdgeID AND e.Active = 1 
										ORDER BY e.Name ASC
										"));
                    if (count($edgeResults)) {
                    // echo "<option>Edge Found</option>";

                        foreach($edgeResults as $subrow) {
                            $name         = $subrow->EdgeName;
                            $id         = $subrow->EdgeID;
                            $op1ID         = $subrow->Option1ID;
                            $op2ID         = $subrow->Option2ID;

                            if ($id == $ConstructionEdgeID) {
                                $html .= "<option class=\"IDeqConstructionID\" value=\"" . $id . "\" selected>" . $name . "</option>";
                                
                            } else {
                                $html .= "<option class=\"IDnoteqConstructionID\" value=\"" . $id . "\">" . $name . "</option>";
                         
                            }

                            $html .= "Edge Option IDS: ". $id ." ".  $op1ID . " " . $op2ID;
                            
                            //do i have options?
                            if ($op1ID != 0 || $op2ID != 0) {

                                //have 1 option
                                if ($op1ID != 0) {
                                    $OptionsSQL = DB::select(DB::raw("SELECT * FROM edge_banding_options WHERE ID = $op1ID")); //option ID get the options
                                    foreach ( $OptionsSQL as $row1) {
                                        $op1Name = $row1->Name;
                                        $op1List = $row1->List;
                                    }

                                    if ($op1ID == $ConstructionEdgeID) {
                                        $html .= "<option class=\"OP1IDeqConstructionID\" value=\"" . $op1ID . "\" selected>" . $op1Name . " (+" . $op1List . ")</option>";
                                        // echo "<option>Option 1 equals Construction Edge</option>";
                                    } else {
                                        $html .= "<option class=\"OP1IDnoteqConstructionID\" value=\"" . $op1ID . "\">" . $op1Name . " (+" . $op1List . ")</option>";
                                        // echo "<option>Option 1 NOT equal Construction Edge</option>";
                                    }
                                }


                                //have 2 option
                                if ($op2ID != 0) {
                                    $OptionsSQL = DB::select(DB::raw("select * from edge_banding_options where ID = $op2ID")); //option ID get the options
                                    foreach($OptionsSQL as $row1) {
                                        $op2Name = $row1->Name;
                                        $op2List = $row1->List;
                                    }

                                    if ($op2ID == $ConstructionEdgeID) {
                                        $html .= "<option class=\"OP2IDeqConstructionID\" value=\"" . $op2ID . "\" selected>" . $op2Name . " (+" . $op2List . ")</option>";
                                        // echo "<option>Option 2 equals Construction Edge</option>";
                                    } else {
                                        $html .= "<option class=\"OP2IDnoteqConstructionID\" value=\"" . $op2ID . "\">" . $op2Name . " (+" . $op2List . ")</option>";
                                        // echo "<option>Option 2 NOT equal Construction Edge</option>";
                                    }
                                }
                            } //end options


                            // $html .= "<option value=\"3\">Veneer (+14%)</option>";
                     

                        }
                    } else {
    
                        $html .= "<option class=\"IDnotfound\" value=\"0\">Matching edge not found</option>";
                        // $html .= "<option class=\"IDnotfound\" value=\"3\">Veneer</option>";
              
                    }
                }
            } else {
                //not wood do regular edge banding look up via color defaults
                // echo "<option>NOT WOOD Edge Type</option>";
                //if Color ID is valid
                $colorResults     = DB::select(DB::raw("SELECT EdgeBandingDefaultID FROM style_color WHERE ID = $ColorID and Active = 1 ORDER BY Name ASC"));
                $mysqlNumCheckColor = count($colorResults);

                if ($mysqlNumCheckColor != 0) {
                          $TypeCheck = gettype($colorResults);
                     if ($TypeCheck == "object" || $TypeCheck == "array") {
                                if (count($colorResults)) {
                                  foreach ($colorResults as $Trow) {

                                    $EdgeID = $Trow->EdgeBandingDefaultID;
                                    $edgeResults = Edge_banding::where('ID', $EdgeID)->get();

                                
                                if (count($edgeResults)) {
                                // echo "<option>Got Options List</option>";

                                    foreach ($edgeResults as $row) {
                                        $name           = $row->Name;
                                        $id             = $row->ID;
                                        $op1ID          = $row->Option1;
                                        $op2ID          = $row->Option2;



                                        if ($id == $ConstructionEdgeID) {
     
                                            $html .= "<option class=\"IDeqConstructionID\" value=\"" . $id . "\" selected>" . $name . "</option>";
                                        } else {

                                            $html .= "<option class=\"IDnoteqConstructionID\" value=\"" . $op1ID ." ". $op2ID ." ". $id ." ". $EdgeID . "\">" . $name . "</option>";
                                        }
                                        
                                        // $html .= "Edge Option IDS: " . $op1ID . " " . $op2ID;

                                        //do i have options?
                                        if ($op1ID != 0 || $op2ID != 0) {


                                            //have 1 option
                                            if ($op1ID != 0) {
                                                $OptionsSQL = DB::select(DB::raw("SELECT * FROM edge_banding_options WHERE ID = $op1ID")); //option ID get the options
                                                foreach ($OptionsSQL as $row) {
                                                    $op1Name = $row->Name;
                                                    $op1List = $row->List;
                                                }

                                                if ($op1ID == $ConstructionEdgeID) {
                                                    $html .= "<option  class=\"OP1IDeqConstructionID\" value=\"" . $op1ID . "\" selected>" . $op1Name . " (+" . $op1List . ")</option>";
                                                } else {
                                                    $html .= "<option class=\"OP1IDnoteqConstructionID\" value=\"" . $op1ID . "\">" . $op1Name . " (+" . $op1List . ")</option>";
                                                }
                                            }


                                            //have 2 option
                                            if ($op2ID != 0) {
                                                $OptionsSQL = DB::select(DB::raw("select * from edge_banding_options where ID = $op2ID")); //option ID get the options
                                                foreach ($OptionsSQL as $row) {
                                                    $op2Name = $row->Name;
                                                    $op2List = $row->List;
                                                }

                                                if ($op2ID == $ConstructionEdgeID) {
                                                    $html .= "<option class=\"OP20IDeqConstructionID\"  value=\"" . $op2ID . "\" selected>" . $op2Name . " (+" . $op2List . ")</option>";
                                                } else {
                                                    $html .= "<option class=\"OP2IDnoteqConstructionID\"  value=\"" . $op2ID . "\">" . $op2Name . " (+" . $op2List . ")</option>";
                                                }
                                            }
                                        } //end options

                                        // $html .= "<option value=\"3\">Veneer (+14%)</option>";

                                    }
                                } else {
                                                        
                                    $html .= "<option value=\"0\">Matching edge not found</option>";
                                    // $html .= "<option value=\"3\">Veneer</option>";
                                }

                                //look up the edge band and see if options should exist

                            }

                        } else {
                            // $html .= "<option value=\"0\">No Match Available</option>";
                            // $html .= "<option value=\"3\">Veneer</option>";

                        }
                    }
                }else{
                    echo "<option>NO Default Edge Found</option>";

                }
            }
            echo $html;
        }

        if ($postAjax == 'DoorStyle') {


            $ajax = $postAjax;
            $id = $postID; //default to imperia
            $brandID = Input::get('brandid');
            $jason = Input::get('jason');
            $jobid     = $postJobID;
            $html = '';
       
            //does the parent of this group have a default?
            $SpecDefaultID = $this->getDefaultSpec("style_group", $id, "SpecDefault", "DoorStyle");

            $Results = DB::select(DB::raw("SELECT DoorStyleID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionID = "";
            foreach ($Results as $row) {
                $ConstructionID = $row->DoorStyleID;
            }

            //get all options
            // $doorStyleResults = DB::select(DB::raw("SELECT ID, Name FROM styles WHERE GroupID = $id and StyleType = 'Door' and BrandID = $brandID and Active = 1 ORDER BY Name ASC"));
            // $doorStyleResults = DB::select(DB::raw("SELECT ID, Name FROM styles WHERE GroupID = $id and StyleType = 'Door' and Active = 1 ORDER BY Name ASC"));
            $doorStyleResults = DB::select(DB::raw("SELECT ID, Name, BrandID FROM styles WHERE GroupID = $id and StyleType = 'Door' and BrandID LIKE '%$brandID%' and Active = 1 ORDER BY Name ASC"));
             
            
            foreach ($doorStyleResults as $row) {
                //if it has a default, use it
                if ($row->ID == $SpecDefaultID && $ConstructionID == "") {
                    $defaultSelected = " selected";
                } else {
                    $defaultSelected = "";
                }

                //if it's already been selected then override default and set this to selected
                if ($ConstructionID == $row->ID) {
                    $defaultSelected = " selected";
                }

                $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
            }
            echo $html;
        }

        if ($postAjax == 'DrawerStyle') {

            //get vars from construction.php call
            $jobid          = isset($postJobID) ? $postJobID : 0;
            $id             = isset($postID) ? $postID : 1;
            $doorname       = Input::get('DoorName');
            $doorStyleID    = $id;
            $DoorName       = isset($doorname) ? $doorname : 1; //default to imperia
            $SpecDefaultID  = 0;
            $ConstructionID = 0;
            $firstEntry     = 0;
            $html           = '';
            // $html           .= '<script>console.log(drwSty called);</script>';
            $SpecDefaultID     = $this->getDefaultSpec("styles", $id, 'DefaultDrawer', 'DrawerStyle');

            $defaultSelected = "";
            $jobConstruction = false;
            $ConstructionID = 0;
            $ConstructionDoorID = 0;
            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT DrawerStyleID, DoorStyleID FROM job_construction WHERE JobID = $jobid"));

            foreach ($Results as $row) {
                $ConstructionID = $row->DrawerStyleID;
                $ConstructionDoorID = $row->DoorStyleID;
            }
            //dd($Results);
            //get DrawerStyle IDs available to this door
            if ($id != 0) {
                $doorStyleResults = DB::select(DB::raw("SELECT DrawerStyleID FROM styles WHERE ID = $id ORDER BY Name ASC LIMIT 1"));
                // echo "<script>console.log('SELECT DrawerStyleID FROM styles WHERE ID = $id ORDER BY Name ASC LIMIT 1');</script>";
                foreach ($doorStyleResults as $row) {
                    $ValNull = $row->DrawerStyleID;
                }

                $drwArray = explode(',', $ValNull);
                list($first) = explode(',', $ValNull);
                reset($drwArray);
  
                //Loop through and build an array of options
                foreach ($drwArray as $key => $value) {
                    $StyleMatlResults2 = DB::select(DB::raw("SELECT ID, Name FROM styles WHERE ID = $value and Active = 1 ORDER BY Name ASC"));

                    //decide if it needs -SM because it's a door match
                    foreach ($StyleMatlResults2 as $row) {
                        if ($row->Name == $DoorName) {
                            $sort[$row->ID] = $row->Name . " - SM";
                        } else {
                            $sort[$row->ID] = $row->Name;
                        }
                    }
                    asort($sort);
                }

                //does the current door selected match the customer's saved choice?
                if ($doorStyleID == $ConstructionDoorID) {
                    $CustomerSavedDoor = 1;
                } else {
                    $CustomerSavedDoor = 0;
                }

                // dd($CustomerSavedDoor);

                //--

                //Loop through and find selected
                foreach ($sort as $key => $value) {

                    if ($SpecDefaultID  == 0 && $ConstructionID == 0 && $firstEntry == 0 && $CustomerSavedDoor == 0) {

                        //only do this for the first one
                        $firstEntry = " checked style='first'";
                        $html .= "<option value=\"" . $key . "\" $firstEntry>" . $value . "</option>";
                    } else {

                        //if it has a default, use it
                        if ($key == $SpecDefaultID  && $CustomerSavedDoor == 0) {
                            $defaultSelected = " selected style='default'";
                        } else {
                            $defaultSelected = "";
                        }

                        //if it's already been selected then override default and set this to selected
                        if ($ConstructionID == $key && $CustomerSavedDoor == 1) {
                            $defaultSelected = " selected style='customer'";
                        }

                        $html .= "<option value=\"" . $key . "\" $defaultSelected>" . $value . "</option>";
                    }
                }
            }

            echo $html;
        }

        if ($postAjax == 'LgDrawerStyle') {

            //get vars from construction.php call
            $jobid             = isset($postJobID) ? $postJobID : 0;
            $id             = isset($postID) ? $postID : 0;
            $doorname       = Input::get('DoorName');
            $DoorName         = isset($doorname) ? $doorname : 1; //default to imperia
            $SpecDefaultID  = $this->getDefaultSpec("styles", $id, 'DefaultLgDrawer', 'LgDrawerStyle');

            $jobConstruction = false;
            $defaultSelected = "";
            $html            = '';
            //get selected job construction value if it exists
            $Results = DB::select(DB::raw("SELECT LargeDrawerStyleID FROM job_construction WHERE JobID = $jobid"));
            $ConstructionID = "";
            foreach ($Results as $row) {
                $ConstructionID = $row->LargeDrawerStyleID;
            }

            //get DrawerStyle IDs available to this door
            if ($id != 0) {
                $doorStyleResults = DB::select(DB::raw("SELECT LgDrawerStyleID FROM styles WHERE ID = $id ORDER BY Name ASC LIMIT 1"));
                foreach ($doorStyleResults as $row) {
                    $ValNull = $row->LgDrawerStyleID;
                }

                $drwArray = explode(',', $ValNull);
                list($first) = explode(',', $ValNull);
                reset($drwArray);

                // echo "<option></option>";

                //Loop through and build an array of options
                foreach ($drwArray as $key => $value) {
                    $StyleMatlResults2 = DB::select(DB::raw("SELECT ID, Name FROM styles WHERE ID = $value and Active = 1 ORDER BY Name ASC"));

                    //decide if it needs -SM because it's a door match
                    foreach ($StyleMatlResults2 as $row) {
                        if ($row->Name == $DoorName) {
                            $sort[$row->ID] = $row->Name . " - LM";
                        } else {
                            $sort[$row->ID] = $row->Name;
                        }
                    }
                    asort($sort);
                }

                //Loop through and find selected
                foreach ($sort as $key => $value) {

                    // job construction has an ID, use it
                    if ($key == $ConstructionID) {
                        $defaultSelected = " selected";
                        $jobConstruction = true;

                        //if it has a default, use it
                    } elseif ($key == $SpecDefaultID && $jobConstruction != true) {
                        $defaultSelected = " selected";
                    }

                    //just print it
                    $html .= "<option value=\"" . $key . "\" $defaultSelected>" . $value . "</option>";
                    $defaultSelected = "";
                }
            }
            echo $html;
        }


        if ($postAjax == 'StyleMatl') {
            $id             = $postID;
            $brandID        = isset($postBrandID) ? $postBrandID : 1;
            $html           = '';
            $jobid          = isset($postJobID) ? $postJobID : 0;
            
            if ($id != 0) {

                //get default material
                $SpecDefaultID = $this->getDefaultSpec("styles", $id, 'DefaultMaterial', 'StyleMatl');
                echo "<script>console.log('matl.specdefaultid = ' + ".$SpecDefaultID.");</script>";
  
                //get material already saved in system for this job
                $Results = DB::select(DB::raw("SELECT StyleMaterialID FROM job_construction WHERE JobID = $jobid"));
                $ConstructionID = "";
                foreach ($Results as $row) {
                    $ConstructionID = $row->StyleMaterialID;
                }

                //get all materials for this door
                $StyleMatlResults = DB::select(DB::raw("SELECT MaterialID FROM styles WHERE ID = $id LIMIT 1"));
                foreach ($StyleMatlResults as $row) {
                    $MaterialIDs = $row->MaterialID;
                }
                $MatlArray = explode(',', $MaterialIDs);
                reset($MatlArray);

                foreach ($MatlArray as $key => $value) {
                    
                    $StyleMatlResults2 = DB::select(DB::raw("SELECT ID, Name FROM style_material WHERE ID = $value and Active = 1 ORDER BY Name ASC"));
                    foreach ($StyleMatlResults2 as $row) {

                        //if it has a default, use it
                        if ($row->ID == $SpecDefaultID && $ConstructionID == "") {
                            $defaultSelected = " selected";
                        } else {
                            $defaultSelected = "";
                        }
        
                        //if it's already been selected then override default and set this to selected
                        if ($ConstructionID == $row->ID) {
                            $defaultSelected = " selected";
                        }
        
                        $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                    }
                }

            //if no materials found
            } else {

                $html .= "<option></option>";
            }

            echo $html;
        }

       
        if ($postAjax == 'StyleColor') {
            $id         = isset($postID) ? $postID : 0;
            $brandID    = isset($postBrandID) ? $postBrandID : 1;
            $html       = '';
            $jobid      = isset($postJobID) ? $postJobID : 0;

            if ($id != 0) {

                //get default color
                $SpecDefaultID = $this->getDefaultSpec("style_material", $id, 'SpecDefault', 'StyleColor');
                echo "<script>console.log('color.specdefaultid = ' + ".$SpecDefaultID.");</script>";

                //get color already saved in system
                $StyleColorJobResult = DB::select(DB::raw("SELECT StyleColorID FROM job_construction WHERE JobID =$jobid limit 1"));
                $ConstructionID = "";
                foreach ($StyleColorJobResult as $row) {
                        $ConstructionID   = $row->StyleColorID;
                }

                //get all colors for this material
                $StyleMatlResults   = DB::select(DB::raw("SELECT ColorID FROM style_material WHERE ID = $id LIMIT 1"));
                foreach ($StyleMatlResults as $row) {
                    $AllValue   = $row->ColorID;
                }
                $ColorArray = explode(',', $AllValue);
                reset($ColorArray);

                foreach ($ColorArray as $key => $value) {
                
                    $StyleMatlResults2 = DB::select(DB::raw("SELECT ID, Name FROM style_color WHERE ID = $value and Active = 1 ORDER BY Name ASC"));
                    foreach ($StyleMatlResults2 as $row) {

                        //if it has a default, use it
                        if ($row->ID == $SpecDefaultID && $ConstructionID == "") {
                            $defaultSelected = " selected";
                        } else {
                            $defaultSelected = "";
                        }
        
                        //if it's already been selected then override default and set this to selected
                        if ($ConstructionID == $row->ID) {
                            $defaultSelected = " selected";
                        }
        
                        $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                    }
                
            }
            
            //if no colors found
            } else {

                $html .= "<option></option>";
            }

            echo $html;
        }


        if ($postAjax == 'CabColor') {
            // echo "<script>console.log('AJAX = CabColor');</script>";

            $id        =  isset($postID) ? $postID : 0;
            $html   = '';
            $StyleMatlResults     = DB::select(DB::raw("SELECT ColorID FROM style_material WHERE ID = $id LIMIT 1"));
            foreach ($StyleMatlResults as $row) {
                $AllValue             = $row->ColorID;
            }

            $jobid                 = $postJobID;
            $StyleColorJobResult = DB::select(DB::raw("SELECT CabinetExteriorColorID FROM job_construction WHERE JobID =$jobid limit 1"));
            if (count($StyleColorJobResult)) {
                foreach ($StyleColorJobResult as $row) {
                    $ColorArrayResult     = $row->CabinetExteriorColorID;
                }
            } else {
                $ColorArrayResult = '';
            }


            $ColorArray = explode(',', $AllValue);
            reset($ColorArray);

            foreach ($ColorArray as $key => $value) {
                $StyleMatlResults2 = DB::select(DB::raw("SELECT * FROM style_color WHERE ID = $value and Active = 1 ORDER BY Name ASC"));
                foreach ($StyleMatlResults2 as $row) {

                    //remove *, /, from Name
                    $name = str_replace("* ", "", $row->Name);
                    $name = str_replace(" / ", " ", $name);
                    $sort[$row->ID] = $name;
                }
                asort($sort);
            }

            foreach ($sort as $key => $value) {
                $strselected = "";
                if ($ColorArrayResult == $key) {
                    $strselected = "selected";
                }
                $html .= "<option $strselected value=\"" . $key . "\">" . $value . "</option>";
            }
        }

        if ($postAjax == 'StyleFinish') {

            $id          = isset($postID) ? $postID : 0;
            $brandID    = isset($postBrandID) ? $postBrandID : 1;
            $jobid       = $postJobID;
            $html        = '';

            if ($id != 0) {

                //get default finish
                $SpecDefaultID = $this->getDefaultSpec("style_color", $id, 'SpecDefault', 'StyleFinish');
                echo "<script>console.log('finish.specdefaultid = ' + ".$SpecDefaultID.");</script>";

                //get color already saved in system
                $StyleColorJobResult = DB::select(DB::raw("SELECT StyleFinishID FROM job_construction WHERE JobID =$jobid limit 1"));
                $ConstructionID = "";
                foreach ($StyleColorJobResult as $row) {
                        $ConstructionID   = $row->StyleFinishID;
                }
                
                 //get all finishes for this color
                 $StyleMatlResults   = DB::select(DB::raw("SELECT FinishID FROM style_color WHERE ID = $id LIMIT 1"));
                 foreach ($StyleMatlResults as $row) {
                     $AllValue   = $row->FinishID;
                 }
                 $ColorArray = explode(',', $AllValue);
                 reset($ColorArray);

                 foreach ($ColorArray as $key => $value) {
                
                    $StyleMatlResults2 = DB::select(DB::raw("SELECT ID, Name FROM style_finish WHERE ID = $value and Active = 1 ORDER BY Name ASC"));
                    foreach ($StyleMatlResults2 as $row) {

                        //if it has a default, use it
                        if ($row->ID == $SpecDefaultID && $ConstructionID == "") {
                            $defaultSelected = " selected";
                        } else {
                            $defaultSelected = "";
                        }
        
                        //if it's already been selected then override default and set this to selected
                        if ($ConstructionID == $row->ID) {
                            $defaultSelected = " selected";
                        }
        
                        $html .= "<option value=\"" . $row->ID . "\" $defaultSelected>" . $row->Name . "</option>";
                    }
                
            }

            //if no finishes found
            } else {

                $html .= "<option></option>";
            }

            echo $html;
        }



               

        if ($postAjax == 'GlassStyle') {
            // echo "<script>console.log('AJAX = GlassStyle');</script>";

            $id         = isset($postID) ? $postID : 0;
            // $id			= isset($postID) ? $postID : 0;
            $html       = '';
            $brandID     = Input::get('brandid');

            $html = "<option></option>";

            // $doorStyleResults = DB::select(DB::raw("SELECT * FROM styles WHERE GroupID = $id and StyleType = 'Glass' and BrandID = $brandID and Active = 1 ORDER BY Name ASC"));
            $doorStyleResults = DB::select(DB::raw("SELECT * FROM styles WHERE GroupID = $id and StyleType = 'Glass'  and Active = 1 ORDER BY Name ASC"));
            foreach ($doorStyleResults as $row) {
                if (!$row->Label) {
                    $Label = "";
                } else {
                    $Label = "(" . $row->Label . ")";
                }
                $html .= "<option value=\"" . $row->ID . "\">" . $row->Name . "</option>";
            }
            echo $html;
        }


        if ($postAjax == 'doorSpec') {
            // echo "<script>console.log('right bar: doorSpec.html() called - AJAX');</script>";
            
            $id         = isset($postID) ? $postID : 0;
            $jobId      = isset($postJobID) ? $postJobID : "";
            $doorStyleID = $id;

            $DefaultHardware = 0;
            $DefaultCenter = 0;
            $DefaultInside = 0;
            $DefaultOutside = 0;
            $DefaultStile = 0;
            $doorID       = 0;
            $jobDoorHardware = 0;
            $doorinside = 0;
            $doorcenter = 0;
            $dooroutside = 0;
            $jobsiterail = 0;
            $html = '';
            if ($id != 0) {
                $Results = DB::select(DB::raw("SELECT * FROM styles WHERE ID = $id LIMIT 1"));
                
                foreach ($Results as $row) {
                    $DoorName             = $row->Name;
                    $Inside                = $row->InsideProfileID;
                    $Outside            = $row->OutsideProfileID;
                    $StileRail          = $row->StileRailID;
                    $Peg                = $row->PegID;
                    $CenterPanel        = $row->CenterPanelID;
                    $Hardware           = $row->HardwareID;
                    $Location           = $row->LocationID;
                    $DefaultCenter      = $row->DefaultCenter;
                    $DefaultInside      = $row->DefaultInside;
                    $DefaultOutside     = $row->DefaultOutside;
                    $DefaultStile       = $row->DefaultStile;
                    $DefaultHardware       = $row->DefaultHardware;
                }
            }

            
            $job_construction = DB::select(DB::raw("SELECT * FROM job_construction where JobID = '" . $jobId . "'"));
            
            if (count($job_construction)) {
                foreach ($job_construction as $jcdata) {
                    $doorID             = $jcdata->DoorStyleID;
                    $doorinside         = $jcdata->DoorInsideProfileID;
                    $doorcenter         = $jcdata->DoorCenterPanelID;
                    $dooroutside        = $jcdata->DoorOutsideProfileID;
                    $jobsiterail        = $jcdata->DoorStileRailID;
                    $jobDoorHardware    = $jcdata->DoorHardwareID;
                }
            }

            
            //does the current door selected match the customer's saved choice?
            if ($doorStyleID == $doorID) {
                $CustomerSavedDoor = 1;
            } else {
                $CustomerSavedDoor = 0;
            }

            if (is_null($Inside) or empty($Inside)) {
            } else {
                
                $html .= "<div>";
                $html .= "<label class=\"h4\"><strong>Inside Profiles</strong></label><br>";
                //list($first) = explode(',', $Inside);

                $defaultSelected = "";
               
                $inResults = DB::select(DB::raw("SELECT ID, Name, Code FROM style_insideprofile WHERE ID = $Inside ORDER BY Name"));
                $firstEntry = 0;
                foreach ($inResults as $row) {

                    $ID     = $row->ID;
                    $Name     = $row->Name;
                    $Code     = $row->Code;

                    $html .= '<div class="radio" style="margin: 4px;">';




                    //--



                    if ($DefaultInside == 0 && $doorinside == 0 && $firstEntry == 0) {

                        //only do this for the first one
                        $firstEntry = 1;
                        $html .= "<input type='radio'  class='imagebtn DoorInside' name='DoorInside' code='$Code' id='Door_$Name' value='$ID' $firstEntry>";
                    } else {

                        //if it has a default, use it
                        if ($row->ID == $DefaultInside  && $CustomerSavedDoor == 0) {
                            $defaultSelected = " checked style='default'";
                        } else {
                            $defaultSelected = "";
                        }

                        //if it's already been selected then override default and set this to selected and it's the same door
                        if ($doorinside == $row->ID && $CustomerSavedDoor == 1) {
                            $defaultSelected = " checked style='customer'";
                        }

                        $html .= "<input type='radio'  class='imagebtn DoorInside' name='DoorInside' code='$Code' id='Door_$Name' value='$ID' $defaultSelected>";
                    }

                    
                    //--

                    $html .= "<label for='Door_$Code'>";
                    $html .= "<img src='/images/inside/$Code.png'>";
                    $html .= "<br>&nbsp;$Name";
                    $html .= "</label>";
                    $html .= "</div>";
                }
                
                $html .= "</div>";
            }




            // ===================== CENTERPANEL =====================
            if (is_null($CenterPanel) or empty($CenterPanel)) {
                // echo "<span style='color:grey;'>NO Center: $CenterPanel </span><br>";
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\"><strong>Center Panels</strong></label><br>";
                list($first) = explode(',', $CenterPanel);


                foreach (explode(',', $CenterPanel) as $value) {
                    $centerResults = DB::select(DB::raw("SELECT ID, Code, Name FROM style_centerpanel WHERE ID = $value ORDER BY Name"));
                    $firstEntry = 0;
                    foreach ($centerResults as $row) {

                        $ID     = $row->ID;
                        $Name     = $row->Name;
                        $Code     = $row->Code;

                        $html .= "<div id=\"DoorCenterBox\" class=\"radio\" style=\"margin: 4px;\">";

                        if ($DefaultCenter == 0 && $doorcenter == 0 && $firstEntry == 0) {

                            //only do this for the first one
                            $firstEntry = 1;
                            $html .= "<input type='radio' class='imagebtn DoorCenterPanel' name='DoorCenterPanel' code='$Code' id='Door_$Code'  value='$ID' $firstEntry>";
                        } else {

                            //if it has a default, use it
                            if ($row->ID == $DefaultCenter  && $CustomerSavedDoor == 0) {
                                $defaultSelected = " checked style='default'";
                            } else {
                                $defaultSelected = "";
                            }

                            //if it's already been selected then override default and set this to selected
                            if ($doorcenter == $row->ID && $CustomerSavedDoor == 1) {
                                $defaultSelected = " checked style='customer'";
                            }

                            $html .= "<input type='radio' class='imagebtn DoorCenterPanel' name='DoorCenterPanel' code='$Code' id='Door_$Code'  value='$ID' $defaultSelected>";
                        }

                        $html .= "<label for='Door_$Code'>";
                        $html .= "<img src=\"/images/centerpanel/" . $row->Code . ".png\">";
                        $html .= "<br>&nbsp;" . $row->Name;
                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }



            // ===================== OUTSIDE =====================
            if (is_null($Outside) or empty($Outside)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\"><strong>Outside Profiles</strong></label><br>";
                list($first) = explode(',', $Outside);


                foreach (explode(',', $Outside) as $value) {
                    $outResults = DB::select(DB::raw("SELECT ID, Code, Name FROM style_outsideprofile WHERE ID = $value ORDER BY Name"));
                    $firstEntry = 0;

                    foreach ($outResults as $row) {

                        $ID     = $row->ID;
                        $Name     = trim($row->Name);
                        $Code     = trim($row->Code);

                        $html .= "<div id=\"DoorOutsideBox\" class=\"radio\" style=\"margin: 4px;\">";
                        if ($DefaultOutside == 0 && $dooroutside == 0 && $firstEntry == 0 && $CustomerSavedDoor == 0) {

                            //only do this for the first one
                            $firstEntry = 1;
                            $html .= "<input type='radio' class='imagebtn DoorOutside' name='DoorOutside' code='$Code' id='Door_$Code'  value='$ID' $firstEntry>";
                        } else {

                            //if it has a default, use it
                            if ($row->ID == $DefaultOutside && $CustomerSavedDoor == 0) {
                                $defaultSelected = " checked style='default'";
                            } else {
                                $defaultSelected = "";
                            }

                            //if it's already been selected then override default and set this to selected
                            if ($dooroutside == $row->ID && $CustomerSavedDoor == 1) {
                                $defaultSelected = " checked style='customer'";
                            }

                            $html .= "<input type='radio' class='imagebtn DoorOutside' name='DoorOutside' code='$Code' id='Door_$Code'  value='$ID' $defaultSelected>";
                        }
                        $html .= "<label for='Door_$Code'><img src='/images/outside/$Code.png'><br>$Name</label></div>";
                    }
                }
                $html .= "</div>";
            }
            // ===================== * Outside =====================



            // ===================== StileRail =====================
            if (is_null($StileRail) or empty($StileRail)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\"><strong>Stiles and Rails</strong></label><br>";
                list($first) = explode(',', $StileRail);



                foreach (explode(',', $StileRail) as $value) {
                    $outResults = DB::select(DB::raw("SELECT ID, Name, Code FROM style_stilerail WHERE ID = $value ORDER BY Name"));
                    $jobRadioChecked = false;
                    $firstEntry = 0;
                    $defaultSelected = 0;

                    foreach ($outResults as $row) {

                        $ID     = $row->ID;
                        $Name     = $row->Name;
                        $Code     = $row->Code;

                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";
                        //--

                        if ($DefaultStile == 0 && $jobsiterail == 0 && $firstEntry == 0) {

                            //only do this for the first one
                            $firstEntry = 1;
                            $html .= "<input type='radio' class='imagebtn DoorStileRail' name='DoorStileRail' code='$Code' id='Door_$Code' value='$ID' $firstEntry>";
                        } else {

                            //if it has a default, use it
                            if ($row->ID == $DefaultStile && $CustomerSavedDoor == 0) {
                                $defaultSelected = " checked style='default'";
                            } else {
                                $defaultSelected = "";
                            }

                            //if it's already been selected then override default and set this to selected
                            if ($jobsiterail == $row->ID && $CustomerSavedDoor == 1) {
                                $defaultSelected = " checked style='customer'";
                            }

                            $html .= "<input type='radio' class='imagebtn DoorStileRail' name='DoorStileRail' code='$Code' id='Door_$Code' value='$ID' $defaultSelected>";
                        }
                        //--

                        $html .= "<label for='Door_$Code'>";
                        $html .= "<img src=\"/images/stilerail/" . $row->Code . ".png\">";
                        $html .= "<br>&nbsp;" . $row->Name;
                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }
            // ===================== * STILERAIL =====================





            // ===================== HARDWARE =====================
            if (is_null($Hardware) or empty($Hardware)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\"><strong>Handle/Pulls</strong></label><br>";
                list($first) = explode(',', $Hardware);


                $counter = 0;
                $html .= "<table border='0'><tr>";

                foreach (explode(',', $Hardware) as $value) {
                    $outResults = DB::select(DB::raw("SELECT ID, Code, Name FROM style_hardware WHERE ID = $value ORDER BY Name"));
                    $jobRadioChecked = false;
                    $firstEntry = 0;
                    $defaultSelected = 0;
                    
                    foreach ($outResults as $row) {
                        $counter = $counter + 1; 
                        $ID       = $row->ID;
                        $Name     = $row->Name;
                        $Code     = $row->Code;
                        
                        $html .= "<td><div id=\"DoorHardware\" class=\"radio\" style=\"margin: 30px; text-align: center; border-style: solid; border-color: #efefef; font-size: 10pt; \">";

                        if ($DefaultHardware == 0 && $jobDoorHardware == 0 && $firstEntry == 0) {

                            //only do this for the first one
                            $firstEntry = 1;
                            $html .= "<input type='radio' name='DoorHardware' code='$Code' id='DoorHardware_$Name' value='$ID' $firstEntry>";
                        } else {

                            //if it has a default, use it
                            if ($row->ID == $DefaultHardware && $CustomerSavedDoor == 0) {
                                $defaultSelected = " checked style='default'";
                            } else {
                                $defaultSelected = "";
                            }

                            //if it's already been selected then override default and set this to selected
                            if ($jobDoorHardware == $row->ID && $CustomerSavedDoor == 1) {
                                $defaultSelected = " checked style='customer'";
                            }

                            $html .= "<input type='radio' name='DoorHardware' code='$Code' id='DoorHardware_$Name' value='$ID' $defaultSelected>";
                        }


                        $html .= "<label for=\"Door_" . $row->Code . "\">";
                        $html .= "<img src=\"/images/hardware/" . $row->Code . ".png\">";
                        $html .= "<br><br>&nbsp;" . $row->Name;
                        $html .= "</label>";
                        $html .= "</div></td>";
                    }

                    if($counter >= 3){
                        $html .= "</tr><tr>";
                        $counter = 0;
                    }
                }
                $html .= "</div>";
            }

            echo $html;
        }


        if ($postAjax == 'LgDrwSpec') {

            $id = isset($postID) ? $postID : 0;
            $html = '';

            $Results = DB::select(DB::raw("SELECT * FROM styles WHERE ID = $id LIMIT 1"));

            foreach ($Results as $row) {
                $Inside = $row->InsideProfileID;
                $Outside = $row->OutsideProfileID;
                $StileRail = $row->StileRailID;
                $Peg = $row->PegID;
                $CenterPanel = $row->CenterPanelID;
                $Hardware = $row->HardwareID;
                $Location = $row->LocationID;
            }


            if (is_null($Inside) or empty($Inside)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Inside Profiles</label><br>";
                list($first) = explode(',', $Inside);
                foreach (explode(',', $Inside) as $value) {
                    $inResults = DB::select(DB::raw("SELECT * FROM style_insideprofile WHERE ID = $value"));
                    foreach ($inResults as $row) {
                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";

                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\"  Class=\"imagebtn LgDrwInside\" name=\"LgDrwInside\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" Class=\"imagebtn LgDrwInside\"  name=\"LgDrwInside\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"LgDrw_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/inside/" . $row->Code . ".png\"><br>";
                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }

            if (is_null($CenterPanel) or empty($CenterPanel)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Center Panels</label><br>";
                list($first) = explode(',', $CenterPanel);
                foreach (explode(',', $CenterPanel) as $value) {
                    $outResults = DB::select(DB::raw("SELECT * FROM style_centerpanel WHERE ID = $value"));
                    foreach ($outResults as $row) {

                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";

                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwCenterPanel\" name=\"LgDrwCenterPanel\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwCenterPanel\" name=\"LgDrwCenterPanel\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"LgDrw_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/centerpanel/" . $row->Code . ".png\"><br>";

                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }

            if (is_null($Outside) or empty($Outside)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Outside Profiles</label><br>";
                list($first) = explode(',', $Outside);
                foreach (explode(',', $Outside) as $value) {
                    $outResults = DB::select(DB::raw("SELECT * FROM style_outsideprofile WHERE ID = $value"));
                    foreach ($outResults as $row) {

                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";

                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwOutside\" name=\"LgDrwOutside\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwOutside\" name=\"LgDrwOutside\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"LgDrw_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/outside/" . $row->Code . ".png\"><br>";

                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }

            if (is_null($StileRail) or empty($StileRail)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Stiles and Rails</label><br>";
                list($first) = explode(',', $StileRail);
                foreach (explode(',', $StileRail) as $value) {
                    $outResults = DB::select(DB::raw("SELECT * FROM style_stilerail WHERE ID = $value"));
                    foreach ($outResults as $row) {

                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";

                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwStileRail\" name=\"LgDrwStileRail\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwStileRail\" name=\"LgDrwStileRail\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"LgDrw_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/stilerail/" . $row->Code . ".png\"><br>";

                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }

            if (is_null($Peg) or empty($Peg)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Pegs</label><br>";
                list($first) = explode(',', $Peg);
                foreach (explode(',', $Peg) as $value) {
                    $outResults = DB::select(DB::raw("SELECT * FROM style_peg WHERE ID = $value"));
                    foreach ($outResults as $row) {


                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";
                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwPeg\" name=\"LgDrwPeg\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwPeg\" name=\"LgDrwPeg\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"LgDrw_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/peg/" . $row->Code . ".png\"><br>";

                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }

            if (is_null($Hardware) or empty($Hardware)) {
            } else {
                $html .= "<div>";
                $html .= "<label class=\"h4\">Hardware</label><br>";
                list($first) = explode(',', $Hardware);
                foreach (explode(',', $Hardware) as $value) {
                    $outResults = DB::select(DB::raw("SELECT * FROM style_hardware WHERE ID = $value"));
                    foreach ($outResults as $row) {

                        $html .= "<div class=\"radio\" style=\"margin: 4px;\">";

                        if ($row->ID == $first) {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwHardware\" name=\"LgDrwHardware\" code=\"" . $row->Code . "\" id=\"LgDrw_" . $row->Code . "\" value=\"" . $row->ID . "\" checked>";
                        } else {
                            $html .= "<input type=\"radio\" class=\"imagebtn LgDrwHardware\" name=\"LgDrwHardware\" code=\"" . $row->Code . "\" id=\"v_" . $row->Code . "\" value=\"" . $row->ID . "\">";
                        }
                        $html .= "<label for=\"Door_" . $row->Code . "\">";
                        $html .= "<img src=\"..//images/hardware/" . $row->Code . ".png\"><br>";

                        $html .= "</label>";
                        $html .= "</div>";
                    }
                }
                $html .= "</div>";
            }
            echo $html;
        }



        if($postAjax=='PriceSheetModal'){
            $response = '';
            $response .= '<style>td.priceStyle {
                    text-align: center;
                    background-color: #c9daf8;
                    color: black;
                    }</style>
                <table class="table table-striped">
                    <tH>
                        <td><b>Door</b></td>
                        <td><b>Tall</b></td>
                        <td><b>Full</b></td>
                        <td><b>Drw</b></td>
                        <td><b>LgDrw</b></td>
                        <td><b>Over</b></td>
                        <td><b>DrSQ</b></td>
                        <td><b>DrwSQ</b></td>
                        <td><b>WallE</b></td>
                        <td><b>BaseE</b></td>
                        <td><b>TallE</b></td>
                        <td><b>P1sq</b></td>
                        <td><b>P2sq</b></td>
                        <td><b>FinInts</b></td>
                    </tH>';  

                //get ids from construction.js page
                $jobdoor 			= Input::get('varDoor');
                $jobstylematerial 	= Input::get('varMaterial');
                $jobstylecolor 		= Input::get('varStyle');
                $jobstylefinish	 	= Input::get('varFinish'); 
                $jobdoorinside	 	= Input::get('varInside');
                $jobdooroutside	 	= Input::get('varOutside');
                $jobdoorcenter	 	= Input::get('varCenter'); 
                $jobdoorstile	 	= Input::get('varStile'); 
                $jobdoorhardware	= Input::get('varHardware');

                $jobDoor 			= isset($jobdoor) ? $jobdoor : 0;
                $jobStyleMaterial 	= isset($jobstylematerial) ? $jobstylematerial : 0;
                $jobStyleColor 		= isset($jobstylecolor) ? $jobstylecolor : 0;
                $jobStyleFinish	 	= isset($jobstylefinish) ? $jobstylefinish : 0;
                $jobDoorInside	 	= isset($jobdoorinside) ? $jobdoorinside : 0;
                $jobDoorOutside	 	= isset($jobdooroutside) ? $jobdooroutside : 0;
                $jobDoorCenter	 	= isset($jobdoorcenter) ? $jobdoorcenter : 0;
                $jobDoorStile	 	= isset($jobdoorstile) ? $jobdoorstile : 0;
                $jobDoorHardware	= isset($jobdoorhardware) ? $jobdoorhardware : 1; //None

            
                //for totalling
                $totalDoor 		= 0;
                $totalTDoor 	= 0;
                $totalFDoor 	= 0;
                $totalDrw 		= 0;
                $totalLgDrw 	= 0;
                $totalOver 		= 0;
                $totalDSq 		= 0;
                $totalDrwSq 	= 0;
                $totalFinInt 	= 0;
                $totalP1 		= 0;
                $totalP2 		= 0;
                $totalWE 		= 0;
                $totalTE 		= 0;
                $totalBE 		= 0;
                
                
                //pricing sheet details
                $tableList = array('styles', 
                                'style_insideprofile',
                                'style_centerpanel',
                                'style_outsideprofile',
                                'style_stilerail',
                                'style_hardware',
                                'style_material', 
                                'style_color', 
                                'style_finish');

                //create a row for each table requested above				
                foreach ($tableList as $table) {

                    if($table == 'styles'){
                        $styleID = $jobDoor; 
                        $styleLabel = "Door: ";
                    }elseif($table == 'style_material'){
                        $styleID = $jobStyleMaterial;
                        $styleLabel = "Material: ";
                    }elseif($table == 'style_color'){
                        $styleID = $jobStyleColor;
                        $styleLabel = "Color: ";
                    }elseif($table == 'style_finish'){
                        $styleID = $jobStyleFinish;
                        $styleLabel = "Finish: ";
                    }elseif($table == 'style_insideprofile'){
                        $styleID = $jobDoorInside;
                        $styleLabel = "Inside Profile: ";
                    }elseif($table == 'style_outsideprofile'){
                        $styleID = $jobDoorOutside;
                        $styleLabel = "Outside Profile: ";
                    }elseif($table == 'style_centerpanel'){
                        $styleID = $jobDoorCenter;
                        $styleLabel = "Center Panel: ";
                    }elseif($table == 'style_stilerail'){
                        $styleID = $jobDoorStile;
                        $styleLabel = "Stile Rail: ";
                    }elseif($table == 'style_hardware'){
                        $styleID = $jobDoorHardware;
                        $styleLabel = "Hardware: ";
                    }else{
                        $styleLabel = "table: $table";
                    }
                
                    $Results = DB::select(DB::raw("SELECT ID, Name, DoorList, TallDoorList, 
                    FullTallDoorList, DrawerList, LargeDrawerList, OverlayList, DoorSqFtList, DrawerSqFtList, WallEndList, BaseEndList, TallEndList, Panel1SSqFtList, Panel2SSqFtList, FinishedInteriorList FROM $table WHERE ID = $styleID"));
                        
                    if($Results === NULL OR empty($Results)){
                        $ID 				= 0; 
                        $Name 				= "n/a";
                        $DoorList 			= 0;
                        $TallDoorList 		= 0;
                        $FullTallDoorList 	= 0;
                        $DrawerList 		= 0;
                        $LargeDrawerList 	= 0;
                        $OverlayList 		= 0;
                        $DoorSqFtList 		= 0;
                        $DrawerSqFtList 	= 0;					
                        $WallEndList 		= 0;
                        $BaseEndList 		= 0;
                        $TallEndList 		= 0;
                        $Panel1SSqFtList 	= 0;
                        $Panel2SSqFtList 	= 0;
                        $FinIntList		 	= 0;
                            
                    
                    }else {
                        
                    
                        foreach ($Results as $row) {
                            
                            $ID 				= $row->ID; 
                            $Name 				= $row->Name;
                            $DoorList 			= $row->DoorList;
                            $TallDoorList 		= $row->TallDoorList;
                            $FullTallDoorList 	= $row->FullTallDoorList;
                            $DrawerList 		= $row->DrawerList;
                            $LargeDrawerList 	= $row->LargeDrawerList;
                            $OverlayList 		= $row->OverlayList;
                            $DoorSqFtList 		= $row->DoorSqFtList;
                            $DrawerSqFtList 	= $row->DrawerSqFtList;					
                            $WallEndList 		= $row->WallEndList;
                            $BaseEndList 		= $row->BaseEndList;
                            $TallEndList 		= $row->TallEndList;
                            $Panel1SSqFtList 	= $row->Panel1SSqFtList;
                            $Panel2SSqFtList 	= $row->Panel2SSqFtList;
                            $FinIntList		 	= $row->FinishedInteriorList;


                            //properly style the non 0 values
                            $priceStyle = "priceStyle";

                            if($DoorList > 0){
                                $DoorBold = $priceStyle;
                            }else{
                                $DoorBold = "";
                                // $DoorList = "n/a";
                            }

                            if($TallDoorList > 0){
                                $TallDoorBold = $priceStyle;
                            }else{
                                $TallDoorBold = "";
                            }

                            if($FullTallDoorList > 0){
                                $FullTallDoorBold = $priceStyle;
                            }else{
                                $FullTallDoorBold = "";
                            }

                            if($DrawerList > 0){
                                $DrawerBold = $priceStyle;
                            }else{
                                $DrawerBold = "";
                            }

                            if($LargeDrawerList > 0){
                                $LargeDrawerBold = $priceStyle;
                            }else{
                                $LargeDrawerBold = "";
                            }

                            if($OverlayList > 0){
                                $OverlayBold = $priceStyle;
                            }else{
                                $OverlayBold = "";
                            }

                            if($DoorSqFtList > 0){
                                $DoorSqBold = $priceStyle;
                            }else{
                                $DoorSqBold = "";
                            }

                            if($DrawerSqFtList > 0){
                                $DrawerSqBold = $priceStyle;
                            }else{
                                $DrawerSqBold = "";
                            }

                            if($WallEndList > 0){
                                $WallEndBold = $priceStyle;
                            }else{
                                $WallEndBold = "";
                            }

                            if($BaseEndList > 0){
                                $BaseEndBold = $priceStyle;
                            }else{
                                $BaseEndBold = "";
                            }

                            if($TallEndList > 0){
                                $TallEndBold = $priceStyle;
                            }else{
                                $TallEndBold = "";
                            }

                            if($Panel1SSqFtList > 0){
                                $Panel1Bold = $priceStyle;
                            }else{
                                $Panel1Bold = "";
                            }

                            if($Panel2SSqFtList > 0){
                                $Panel2Bold = $priceStyle;
                            }else{
                                $Panel2Bold = "";
                            }
                        }
                        //Create Totals
                        $totalDoor 		= $DoorList + $totalDoor;
                        $totalTDoor 	= $TallDoorList + $totalTDoor;
                        $totalFDoor 	= $FullTallDoorList + $totalFDoor;
                        $totalDrw 		= $DrawerList + $totalDrw;
                        $totalLgDrw 	= $LargeDrawerList + $totalLgDrw;
                        $totalOver 		= $OverlayList + $totalOver;
                        $totalDSq 		= $DoorSqFtList + $totalDSq;
                        $totalDrwSq 	= $DrawerSqFtList + $totalDrwSq;
                        $totalFinInt 	= $FinIntList + $totalFinInt;
                        $totalP1 		= $Panel1SSqFtList + $totalP1;
                        $totalP2 		= $Panel2SSqFtList + $totalP2;
                        $totalBE		= $BaseEndList + $totalBE;
                        $totalWE		= $WallEndList + $totalWE;
                        $totalTE		= $TallEndList + $totalTE;
                        
                        $response .= " <tr>
                            <td><b>$styleLabel</b> $Name</td>
                            <td class='$DoorBold'>$DoorList</td>
                            <td class='$TallDoorBold'>$TallDoorList</td>
                            <td class='$FullTallDoorBold'>$FullTallDoorList</td>
                            <td class='$DrawerBold'>$DrawerList</td>
                            <td class='$LargeDrawerBold'>$LargeDrawerList</td>
                            <td class='$OverlayBold'>$OverlayList</td>
                            <td class='$DoorSqBold'>$DoorSqFtList</td>
                            <td class='$DrawerSqBold'>$DrawerSqFtList</td>
                            <td class='$WallEndBold'>$WallEndList</td>
                            <td class='$BaseEndBold'>$BaseEndList</td>
                            <td class='$TallEndBold'>$TallEndList</td>
                            <td class='$Panel1Bold'>$Panel1SSqFtList</td>
                            <td class='$Panel2Bold'>$Panel2SSqFtList</td>
                            <td class='$Panel2Bold'>$FinIntList</td>
                            </tr>";

                        
                    }// else
                }//foreach

                $response = $response. " <tr style='border:1px;'>
                    <td></td>
                    <td><b>$totalDoor</b></td>
                    <td><b>$totalTDoor</b></td>
                    <td><b>$totalFDoor</b></td>
                    <td><b>$totalDrw</b></td>
                    <td><b>$totalLgDrw</b></td>
                    <td><b>$totalOver</b></td>
                    <td><b>$totalDSq</b></td>
                    <td><b>$totalDrwSq</b></td>
                    <td><b>$totalWE</b></td>
                    <td><b>$totalBE</b></td>
                    <td><b>$totalTE</b></td>
                    <td><b>$totalP1</b></td>
                    <td><b>$totalP2</b></td>
                    <td><b>$totalFinInt</b></td>
                </tr>
                
                <tr>
                    <td colspan='100%'>

                    <a href=\" ". route('priceprint',['action' => 'PrintPriceSheet', 'varDoor' => $jobDoor, 'varMaterial' => $jobStyleMaterial, 'varStyle' => $jobStyleColor, 'varFinish' => $jobStyleFinish, 'varInside' => $jobDoorInside, 'varOutside' => $jobDoorOutside, 'varCenter' => $jobDoorCenter, 'varStile' => $jobDoorStile])."\">
                    <input type='button' name='printPrice' value='PRINT'></a>
                    </td>
                </tr>";

                    // }


                $response .= '</table>';
            echo $response;			
        } 

        if($postAjax == 'StyleList'){
            
            $Front = Input::get('Front');
            $jobStyleID = Input::get('Style');
            $jobMaterialID = Input::get('Material');
            $jobColorID = Input::get('Color');
            $jobFinishID = Input::get('Finish');
            if($jobFinishID=='undefined')
                return;
            $query = "SELECT
            `styles`.`StyleType` AS `StyleType`,
            `styles`.`ID` AS `StyleID`,
            `styles`.`Name` AS `StyleName`,
            `styles`.`LgDrawerStyleID` AS `LgDrawerStyleID`,
            `styles`.`DrawerStyleID` AS `DrawerStyleID`,
            `style_material`.`ID` AS `MaterialID`,
            `style_material`.`Name` AS `MaterialName`,
            `style_color`.`ID` AS `ColorID`,
            `style_color`.`Name` AS `ColorName`,
            `style_finish`.`ID` AS `FinishID`,
            `style_finish`.`Name` AS `FinishName`,
            `styles`.`DoorList` + `style_material`.`DoorList` + `style_color`.`DoorList` + `style_finish`.`DoorList` AS `DoorList`,
            `styles`.`TallDoorList` + `style_material`.`TallDoorList` + `style_color`.`TallDoorList` + `style_finish`.`TallDoorList` AS `TallDoorList`, IFNULL(`styles`.`GlassList`,0) + IFNULL(`style_material`.`GlassList`,0) + IFNULL(`style_color`.`GlassList`,0) + IFNULL(`style_finish`.`GlassList`,0) AS `GlassList`, 
            `styles`.`FullTallDoorList` + `style_material`.`FullTallDoorList` + `style_color`.`FullTallDoorList` + `style_finish`.`FullTallDoorList` AS `FullTallDoorList`,
            `styles`.`DrawerList` + `style_material`.`DrawerList` + `style_color`.`DrawerList` + `style_finish`.`DrawerList` AS `DrawerList`,
            `styles`.`LargeDrawerList` + `style_material`.`LargeDrawerList` + `style_color`.`LargeDrawerList` + `style_finish`.`LargeDrawerList` AS `LargeDrawerList`,
            `styles`.`OverlayList` + `style_material`.`OverlayList` + `style_color`.`OverlayList` + `style_finish`.`OverlayList` AS `OverlayList`,
            `styles`.`DoorSqFtList` + `style_material`.`DoorSqFtList` + `style_color`.`DoorSqFtList` + `style_finish`.`DoorSqFtList` AS `DoorSqFtList`,
            `styles`.`DrawerSqFtList` + `style_material`.`DrawerSqFtList` + `style_color`.`DrawerSqFtList` + `style_finish`.`DrawerSqFtList` AS `DrawerSqFtList`,
            `style_material`.`FinishMaterialType`,
            `styles`.`WallEndList` + `style_material`.`WallEndList` + `style_color`.`WallEndList` + `style_finish`.`WallEndList` AS `WallEndList`,
            `styles`.`BaseEndList` + `style_material`.`BaseEndList` + `style_color`.`BaseEndList` + `style_finish`.`BaseEndList` AS `BaseEndList`,
            `styles`.`TallEndList` + `style_material`.`TallEndList` + `style_color`.`TallEndList` + `style_finish`.`TallEndList` AS `TallEndList`,
            `styles`.`FinishedInteriorList` + `style_material`.`FinishedInteriorList` + `style_color`.`FinishedInteriorList` + `style_finish`.`FinishedInteriorList` AS `FinishedInteriorList`,
            `styles`.`Panel1SSqFtList` + `style_material`.`Panel1SSqFtList` + `style_color`.`Panel1SSqFtList` + `style_finish`.`Panel1SSqFtList` AS `Panel1SSqFtList`,
            `styles`.`Panel2SSqFtList` + `style_material`.`Panel2SSqFtList` + `style_color`.`Panel2SSqFtList` + `style_finish`.`Panel2SSqFtList` AS `Panel2SSqFtList`
            FROM
                `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
            WHERE
            styles.ID = $jobStyleID AND
            style_material.ID = $jobMaterialID AND
            style_color.ID = $jobColorID AND
            style_finish.ID = $jobFinishID";
            $StyleLIST = DB::select(DB::raw($query));

   
            foreach ($StyleLIST as $row){
                $GetStyleType = $row->StyleType;
                $GetStyleID = $row->StyleID;
                $GetStyleName = $row->StyleName;
                $GetLgDrawerStyleID = $row->LgDrawerStyleID;
                $GetDrawerStyleID = $row->DrawerStyleID;
                $GetMaterialID = $row->MaterialID;
                $GetMaterialName = $row->MaterialName;
                $GetColorID = $row->ColorID;
                $GetColorName = $row->ColorName;
                $GetFinishID = $row->FinishID;
                $GetFinishName = $row->FinishName;
                $GetDoorList = $row->DoorList;
                $GetTallDoorList = $row->TallDoorList;
                $GetFullTallDoorList = $row->FullTallDoorList;
                $GetDrawerList = $row->DrawerList;
                $GetLargeDrawerList = $row->LargeDrawerList;
                $GetOverlayList = $row->OverlayList;
                $GetDoorSqFtList = $row->DoorSqFtList;
                $GetDrawerSqFtList = $row->DrawerSqFtList;
                $GetFinishMaterialType = $row->FinishMaterialType;
                $GetWallEndList = $row->WallEndList;
                $GetBaseEndList = $row->BaseEndList;
                $GetTallEndList = $row->TallEndList;
                $GetFinishedInteriorList = $row->FinishedInteriorList;
                $GetPanel1SSqFtList = $row->Panel1SSqFtList;
                $GetPanel2SSqFtList = $row->Panel2SSqFtList;
                $GetGlassSqFtList = $row->GlassList;             
                        
            }
                
                //match door
                if($GetStyleID == 0 || $GetStyleName == 'Matching Door *' || $GetStyleName == 'Matching Door'){
                
                //
                    
                } else {
                    
                    if($Front == 'Glass'){
                        $GlassWidth     = Input::get('Width');
                        $GlassHeight    = Input::get('Height');
                        $frontSel       = Input::get('frontSel');
                        $hasAllFronts   = Input::get('hasAllFronts');
                        $Sqft = (($GlassWidth * $GlassHeight) / 144); 
                        if($frontSel != 'All Fronts' && $hasAllFronts != 0)
                        {
                            $Sqft = $Sqft / 2;
                        }
                        $Total = ROUND($Sqft  * $GetGlassSqFtList,2);

                        setlocale(LC_MONETARY, 'en_US');
                        $GetGlassSqFtList = number_format($GetGlassSqFtList, 2, '.', '');
                        $Sqft = number_format($Sqft, 2, '.', '');
                        $Total = number_format($Total, 2, '.', '');


                        $html = "<div class=\"form-horizontal\">";
                        
                        $html .= "<div class=\"form-group\">";
          
                        $html .= "<div class=\"col-sm-7\">";
                        $html .= "<div class=\"input-group\">";
             
                        $html .= "<input type=\"hidden\" value=\"$GetGlassSqFtList\" style=\"text-align:right;\" class=\"form-control\" name=\"glassList\" id=\"glassList\" readonly=\"readonly\">";
                        $html .= "</div>";
                        $html .= "</div>";
                        $html .= "</div>";
                        
                        $html .= "<div class=\"form-group\">";    
                        $html .= "<div class=\"col-sm-7\">";
                        $html .= "<div class=\"input-group\">";
                        $html .= "<input type=\"hidden\" value=\"$Sqft\" style=\"text-align:right;\" class=\"form-control\" name=\"glassSqFt\" id=\"glassSqFt\" readonly=\"readonly\">";
                        $html .= "</div>";
                        $html .= "</div>";
                        $html .= "</div>";
                        
                        $html .= "<div class=\"form-group\">";
                        $html .= "<div class=\"col-sm-7\">";
                        $html .= "<div class=\"input-group\">";
                        $html .= "<input type=\"hidden\" value=\"$GetDoorList\" style=\"text-align:right;\" class=\"form-control\" name=\"glassListTotal\" id=\"glassListTotal\" readonly=\"readonly\">";
                        $html .= "</div>";
                        $html .= "</div>";
                        $html .= "</div>";

                        $html .= "<div class=\"form-group\">";
                        $html .= "<div class=\"col-sm-7\">";
                        $html .= "<div class=\"input-group\">";
                        $html .= "<input type=\"hidden\" value=\"$GetDoorList\" style=\"text-align:right;\" class=\"form-control\" name=\"doorListTotal\" id=\"doorListTotal\" readonly=\"readonly\">";
                        $html .= "</div>";
                        $html .= "</div>";
                        $html .= "</div>";
                        
                        $html .= "</div>";
                        
                        
                    }
                    
                    
                    
                    if($Front == 'Door'){
                        
                        
                        $html = "<div >";
                        $html .= "<p><strong>";
                        $html .= $GetStyleName; //echo $GetStyleID;
                        $html .= "</strong></p>";
                        
                        $html .= "<hr><p><strong>Door: </strong>$";
                        $html .= $GetDoorList;
                        $html .= "</p>";
                        $html .= "<p><strong>Tall Door: </strong>$";
                        $html .= $GetTallDoorList;
                        $html .= "</p>";
                        $html .= "<p><strong>Full Tall Door: </strong>$";
                        $html .= $GetFullTallDoorList;
                        $html .= "</p>";
                        
                        if(empty($GetLgDrawerStyleID)){
                            $html .= "<p><strong>Lg Drw: </strong>$";
                            $html .= $GetLargeDrawerList;
                            $html .= "</p>";
                        }
                        if(empty($GetDrawerStyleID)){
                            $html .= "<p><strong>Drw: </strong>$";
                            $html .= $GetDrawerList;
                            $html .= "</p>";
                        }
                        $html .= "<p><strong>Overlay: </strong>$";
                        $html .= $GetOverlayList;
                        $html .= "</p>";
                        $html .= "<p><strong>Door SqFt: </strong>$";
                        $html .= $GetDoorSqFtList;
                        $html .= "</p>";
                        if(empty($GetDrawerStyleID)){
                            $html .= "<p><strong>Drw SqFt: </strong>$";
                            $html .= $GetDrawerSqFtList;
                            $html .= "</p>";
                        }
                        $html .= "<hr><p><strong>Finished End Material: </strong>";
                        $html .= $GetFinishMaterialType;
                        $html .= "</p>";
                        $html .= "<p><strong>Wall End: </strong>$";
                        $html .= $GetWallEndList;
                        $html .= "</p>";
                        $html .= "<p><strong>Base End: </strong>$";
                        $html .= $GetBaseEndList;
                        $html .= "</p>";
                        $html .= "<p><strong>Tall End: </strong>$";
                        $html .= $GetTallEndList;
                        $html .= "</p>";
                        $html .= "<p><strong>Finished Interior SqFt: </strong>$";
                        $html .= $GetFinishedInteriorList;
                        $html .= "</p>";
                        $html .= "<p><strong>Panel 1 Sided SqFt: </strong>$";
                        $html .= $GetPanel1SSqFtList;
                        $html .= "</p>";
                        $html .= "<p><strong>Panel 2 Sided SqFt: </strong>$";
                        $html .= $GetPanel2SSqFtList;
                        $html .= "</p>";
                        
                        $html .= "</div>";
                        
                        $html .= "<hr><div >";
                        $html .= "<p><strong>Material: </strong>";
                        $html .= $GetMaterialName; //echo $GetMaterialID;
                        $html .= "<br><strong>Color: </strong>";
                        $html .= $GetColorName; //echo $GetColorID;
                        $html .= "<br><strong>Finish: </strong>";
                        $html .= $GetFinishName; //echo $GetFinishID;
                        $html .= "</p>";                       
                        
                    } 
                        
                    if($Front == 'LgDrw'){
                        
                        $html .= "<div >";
                        $html .= "<p><strong>";
                        $html .= $GetStyleName; //echo $GetStyleID;
                        $html .= "</strong></p>";
                        
                        
                        $html .= "<hr><p><strong>Lg Drw: </strong>$";
                        $html .= $GetLargeDrawerList;
                        $html .= "</p>";
                    
                        $html .= "<p><strong>Drw SqFt: </strong>$";
                        $html .= $GetDrawerSqFtList;
                        $html .= "</p>";
                        
                        $html .= "</div>";
                        
                    }
                
                    if ($Front == 'Drw'){
                        
                        $html .= "<div >";
                        $html .= "<p><strong>";
                        $html .= $GetStyleName; //echo $GetStyleID;
                        $html .= "</strong></p>";
                        
                        
                        $html .= "<hr><p><strong>Drw: </strong>$";
                        $html .= $GetDrawerList;
                        $html .= "</p>";
                        $html .= "<p><strong>Drw SqFt: </strong>$";
                        $html .= $GetDrawerSqFtList;
                        $html .= "</p>";
                        
                        $html .= "</div>";
                        
                    }         
                }
                echo $html;
        }
    

        /////
    }

    public function getDefaultSpec($table, $id, $field = 'SpecDefault', $debug = 'parent')
    {

        $SpecDefaultID = 0;
        $SpecDefaultResults = DB::select(DB::raw("SELECT $field FROM $table WHERE ID = $id"));

        if (count($SpecDefaultResults)) {
            foreach ($SpecDefaultResults as $row) {
                $SpecDefaultID = $row->$field;
            }
        }

        if(!isset($SpecDefaultID)){
            return 0;
        }
        echo "<script>console.log('call getDefaultSpec($table, $id, $field, $debug | $SpecDefaultID) ');</script>";
        return $SpecDefaultID;
    }
}