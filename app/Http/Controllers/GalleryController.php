<?php
namespace App\Http\Controllers;

class GalleryController extends Controller 
{
	 public function __construct()
    {
        $this->middleware('auth');
    }

	public function index()
	{
		if(session('username')){
			
			return view('gallery/index');

        }else{
            return redirect()->route('home');
        }
		
	}

	public function subcontent()
	{
		return view('gallery/show');
	}

}
