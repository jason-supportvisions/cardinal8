<?php
class DoorBuilderController extends Controller {

    public function __construct()
    {
        $this->notes_max_length = 200;
    }

    public function index()
    {
        $inside_profiles    = InsideProfile::where('show_in_door_builder',1)->get();
        $inside_profile_id  = Input::get('inside_profile_id', $inside_profiles->first()->ID);

        $inside_profile     = InsideProfile::find($inside_profile_id);

        $center_panels      = $inside_profile->center_panels;
        $center_panel_id    = $center_panels->first()->ID;

        $outside_profiles   = $inside_profile->outside_profiles;
        $outside_profile_id = $outside_profiles->first()->ID;

        $stile_rails        = $inside_profile->stile_rails;
        $stile_rail_id      = $stile_rails->first()->ID;

        return View::make('door-builder.index', compact(
            'inside_profiles', 'inside_profile_id',
            'center_panels', 'center_panel_id',
            'outside_profiles', 'outside_profile_id',
            'stile_rails', 'stile_rail_id'
        ));
    }

    public function lastThreeSliders()
    {
        $inside_profile  = InsideProfile::find(Input::get('inside_profile_id'));

        return View::make('door-builder.last-three-sliders', [
            'center_panels'      => $inside_profile->center_panels,
            'center_panel_id'    => $inside_profile->center_panels->first()->ID,
            'outside_profiles'   => $inside_profile->outside_profiles,
            'outside_profile_id' => $inside_profile->outside_profiles->first()->ID,
            'stile_rails'        => $inside_profile->stile_rails,
            'stile_rail_id'      => $inside_profile->stile_rails->first()->ID
        ]);
    }

    public function printSpecs()
    {
        $inside_profile  = InsideProfile::find(Input::get('inside_profile_id'));
        $center_panel    = CenterPanel::find(Input::get('center_panel_id'));
        $outside_profile = OutsideProfile::find(Input::get('outside_profile_id'));
        $stile_rail      = StileRail::find(Input::get('stile_rail_id'));
        $door            = new Door($inside_profile, $center_panel, $outside_profile, $stile_rail);
        $dealer          = Input::get('account_id') ? 
                           Account::find(Input::get('account_id'))->Name :'';
        $designer        = Input::get('first_name');
        $date            = Input::get('date');
        $notes           = substr(Input::get('notes'), 0, $this->notes_max_length);

        $http_base_path  = 'http:///'.public_path().'/';
        // echo $http_base_path;

        return PDF::loadView('door-builder.print-specs', compact(
        // return View::make('door-builder.print-specs', compact(
            'inside_profile',
            'center_panel',
            'outside_profile',
            'stile_rail',
            'door',
            'http_base_path',
            'dealer',
            'designer',
            'date',
            'notes'
            //  ));
        ))->setPaper('Letter')->download('snapie-custom-door-specification.pdf');
    }

    public function getSendSpecsToDealer()
    {
        $inside_profile  = InsideProfile::find(Input::get('inside_profile_id'));
        $center_panel    = CenterPanel::find(Input::get('center_panel_id'));
        $outside_profile = OutsideProfile::find(Input::get('outside_profile_id'));
        $stile_rail      = StileRail::find(Input::get('stile_rail_id'));
        $built_from       = Input::get('builtfrom');
        $door            = new Door($inside_profile, $center_panel, $outside_profile, $stile_rail);
        $account_options = [''=>''] + Account::orderBy('Name')
                                        ->where('show_in_door_builder', 1)
                                        ->get()
                                        ->lists('Name', 'ID', 'City', 'State');
        $notes_max_length = $this->notes_max_length;

        return View::make('door-builder.send-specs-to-dealer', compact(
            'inside_profile',
            'center_panel',
            'outside_profile',
            'stile_rail',
            'built_from',
            'door',
            'account_options',
            'notes_max_length'
            
        ));
    }

    public function postSendSpecsToDealer()
    {
        //send Email 
        function sendDoorEmail($email, 
                                $subject=NULL, 
                                $first_name=NULL, 
                                $last_name=NULL, 
                                $attachment=NULL, 
                                $from_name=NULL, 
                                $phone=NULL, 
                                $notes=NULL, 
                                $user_email=NULL,
                                $recipients=NULL,
                                $built_from=NULL) { 

                                require '../vendor/autoload.php';        
                                $htmlBody;
                                // $mail = new PHPMailer;
                                $mail = new PHPMailer\PHPMailer\PHPMailer();
                                $mail->isSMTP();
                                $mail->SMTPDebug = 0;
                                $mail->Host = 'vps21082.inmotionhosting.com';
                                $mail->Port = 587;
                                $mail->SMTPSecure = 'tls';
                                $mail->SMTPAuth = true;
                                $mail->Username = "auto-mail@cardinal.corymfg.com";
                                $mail->Password = "3WcF+TTn";
                                
                                $mail->setFrom('auto-mail@cardinal.corymfg.com', 'CoryMfg Cardinal Door Builder');
                                $mail->addReplyTo('accounts@corymfg.com', 'Accounts @ CoryMfg.com');
                                $mail->addAddress($email, "$from_name");
                                $mail->Subject = $subject;

                                //build email body from includes and remove 'â' '€' from string
                                // $htmlBody = file_get_contents('https://cardinal.corymfg.com/door-builder-email.php?');
                                // $htmlBody .= '<a href="' . $attachment . '"><span>VIEW Customer SPEC</span></a>';
                                // $htmlBody .= file_get_contents('https://cardinal.corymfg.com/door-builder-email-secondhalf.php');
                                
                                // $htmlBody = file_get_contents('door-builder-email.php', FILE_USE_INCLUDE_PATH);
                                $htmlBody = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                                <head>
                                    <meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
                                </head>
                                <body> 

                                    <table bgcolor="#EDEDED" cellpadding="0" cellspacing="0" valign="top" width="100%">
                                        <tbody>
                                            <tr valign="top">
                                                <td valign="top">
                                                    
                                                    <div style="background-color:transparent;">                    
                                                        <div style="width:100% !important;">
                                                            
                                                            <div>
                                                                <p><strong>New Custom Door Specification</strong></p>
                                                                <p><strong>Submission made at <a href="http://www.%%BUILTFROM%%" target="_blank">%%BUILTFROM%%</a></strong></p>
                                                                <p><strong>Submitted by:</strong></p>
                                                                <p><strong>%%NAME%% 
                                                                    <br />%%PHONE%% 
                                                                    <br /> %%EMAIL%% 
                                                                    <br />
                                                                </strong></p>
                                                                <p><strong>Customer Notes: %%NOTES%%<br /></strong></p>                                                   
                                                            </div>
                                            
                                                            <div align="left" class="button-container">
                                                                <a href="%%URL%%"><span>VIEW Custom Door PDF</span></a>
                                                            </div>
                                                    
                                                        </div>
                                                    </div>

                                                    <div style="background-color:transparent;">                    
                                                        <div style="width:100% !important;">
                                                            <div>
                                                                <p>You are receiving this email because of your affiliation with CoryMfg Inc. If you have any questions regarding this email please contact us below. </p>                                                   
                                                            </div>   
                                                        </div>
                                                    </div>

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                </body>

                                </html>';
                              

                                //replace template words
                                //replace body content with variables ex %%NAME%%
                                $htmlBody = str_replace('%%NAME%%', $from_name, $htmlBody); 
                                $htmlBody = str_replace('%%BUILTFROM%%', $built_from, $htmlBody); 
                                $htmlBody = str_replace('%%PHONE%%', $phone, $htmlBody); 
                                $htmlBody = str_replace('%%EMAIL%%', $email, $htmlBody); 
                                $htmlBody = str_replace('%%NOTES%%', $notes, $htmlBody); 
                                $htmlBody = str_replace('%%URL%%', $attachment, $htmlBody); 

                                //replace funny characters
                                $htmlBody = html_entity_decode($htmlBody, ENT_QUOTES, "UTF-8");
                                $htmlBody = preg_replace('/[^(\x20-\x7F)\x0A\x0D]*/','', $htmlBody);

                                
                                // show CC list at the end of the email template                                
                                foreach($recipients as $recipient){ 
                                    $htmlBody .= '<br>CC: ' . $recipient->FirstName . ' ' . $recipient->LastName . ', ' . $recipient->Email;
                                }

                                $mail->Body = $htmlBody;
                                $mail->isHTML(true);
                                
                                
                                // $mail->AltBody = 'This is a plain-text message body';
                                // $mail->addAttachment('images/phpmailer_mini.png');


                                //send the message
                                //check for errors
                                if (!$mail->send()) {
                                    // echo "Mailer Error: " . $mail->ErrorInfo;
                                } else {
                                    // echo "Message sent!";
                                }

                } 
                // /* Email function





                // validate form data
                $validator = Validator::make(Input::all(), [
                    'first_name' => 'required',
                    'last_name' => 'required',
                    'phone' => 'required',
                    'email' => 'required|email',
                    'account_id' => 'required',
                    'notes' => "max:{$this->notes_max_length}"
                ]);

                if ($validator->fails())
                {
                    return Redirect::back()->withInput()->withErrors($validator);
                }
                // /* validator form






                $account = Account::find(Input::get('account_id'));        
                $data = Input::all();
                $data['account'] = $account;
                $data['request'] = Request::input('builtfrom', 'CoryMfg');
                
                //build URL
                $data['pdf_url'] = url('door-builder/print-specs?')
                                . 'center_panel_id='.Input::get('center_panel_id').'&'
                                . 'inside_profile_id='.Input::get('inside_profile_id').'&'
                                . 'outside_profile_id='.Input::get('outside_profile_id').'&'
                                . 'stile_rail_id='.Input::get('stile_rail_id').'&'
                                . 'account_id='.Input::get('account_id').'&'
                                . 'notes='.urlencode(Input::get('notes')).'&'
                                . 'first_name='.Input::get('first_name').'&'
                                . 'date='.urlencode(date('m/d/Y g:ia'));
                
                $builtFrom          = Input::get('builtfrom');
                $recipients         = User::where('receive_door_spec_emails',1)->where('AccountID',Input::get('account_id'))->get(); //get users attached to this account
                $admins             = User::where('receive_door_spec_emails',1)->where('AccountType',-1)->get(); //add in admins that want ALL submissions from the DYOD email form       
                $recipients         = $recipients->merge($admins); //merge both collections
                $data['recipients'] = $recipients; //pass these arrays to the Make page command below
            
                $emailSubject       = "NEW Custom Door Specification from: ".Input::get('first_name').' '.Input::get('last_name');
                $attachmentURL      = $data['pdf_url']; 
                $userEmail          = $data["email"];
                $userName           = $data["first_name"] . " " . $data["last_name"];
                $userPhone          = $data["phone"];
                $userNotes          = $data["notes"];


            foreach($recipients as $recipient){ //send out emails to array of recipients

                $accountEmail       = $recipient->Email;
                $accountFirstName   = $recipient->FirstName;
                $accountLastName    = $recipient->LastName;
                
                //email body can be found in functions.php
                sendDoorEmail($accountEmail, $emailSubject, $accountFirstName, $accountLastName, $attachmentURL, $userName, $userPhone, $userNotes, $userEmail, $recipients, $builtFrom);
            
        }

        // return Redirect::to("/door-builder/specs-sent-to-dealer")->withInput();
        return View::make('door-builder.specs-sent-to-dealer', compact('recipients','account'));
        // sendEmail("hello world emaail test","emails.door-spec-test", $data);


    }

    public function specsSentToDealer()
    {
        $account = Account::find(Input::get('account_id'));
        $data = ['account'=>$account] + Input::all();
        $view = View::make('door-builder.specs-sent-to-dealer', $data);
        return $view;
    }

    public function doorImagePath()
    {
        $inside_profile  = InsideProfile::find(Input::get('inside_profile_id'));
        $center_panel    = CenterPanel::find(Input::get('center_panel_id'));
        $outside_profile = OutsideProfile::find(Input::get('outside_profile_id'));
        $stile_rail      = StileRail::find(Input::get('stile_rail_id'));
        $door            = new Door($inside_profile, $center_panel, $outside_profile, $stile_rail);

        return $door->image_url;
    }

}
