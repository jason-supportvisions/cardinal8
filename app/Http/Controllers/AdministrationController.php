<?php
namespace App\Http\Controllers;

use App\Model\Account;
use Illuminate\Http\Request;
use App\Model\Input;
use App\Model\EstimationParameters;
use App\Model\StyleFinish;
use App\Model\User;
use App\Model\StyleGroup;
use App\Model\Job;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class AdministrationController extends Controller {

     public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('administration/index');
    }

    public function getUserAccount()
    {
        if(session('username')){

            $accounts = Account::All();
            return view('admin/account')->with('accounts', $accounts)->with('success', 'Product created successfully.');

        }else{
            return redirect()->Auth::routes();
        }
       
    }

    public function accountAdd(){
        return view('admin/add-account')->with('page','Add');
    }

   public function accountCreate(Request $request)
   {
       $id = $request->input('id');
        $validator = Account::validator($request->all());
        if ($validator->fails()) {
            return Redirect()->back()->withErrors($validator)->withInput();
        }

        $show_in_door_builder = $request->input('builder');
        if(!$show_in_door_builder){
            $show_in_door_builder = 0;
        }
        $potential = $request->input('potential');
        if(!$potential){
            $potential = 0;
        }
        $active=$request->input('active');
        if(isset($active) && $active == 1){
            $active = 1;
        }else{
            $active = 0;

        }

        if($request->input('act') =='accountCreate'){
            $account = new Account;
        }else{
            $account = Account::where('ID',$id)->first();
        }  
        $account->Name = $request->input('CompanyName');
        $account->AddressLine1 = $request->input('Address');
        $account->AddressLine2 = $request->input('Address2');
        $account->City = $request->input('City');
        $account->State = $request->input('State');
        $account->Zip = $request->input('Zip');
        $account->Phone = $request->input('Phone');
        $account->MultiplierDiscount = $request->input('XDiscount');
        $account->show_in_door_builder = $show_in_door_builder;
        $account->potential = $potential;
        $account->PrefBrand = $request->input('defaultbrand');
        $account->ShowBrands = implode(',',$request->input('brand'));
        $account->Active = $active;
        // dd($request->all());
        $account->save();
        return $this->getUserAccount(); 
    
   }

   public function accountShowEdit($id, $page)
   {
        session(['back' => 'account']);
       $accounts = Account::where('ID', $id)->get();
       $users = User::where('AccountID', $id)->get();
       return view('admin/edit-account')->with('accounts', $accounts)->with('users', $users)->with('page', $page);
   }

   public function userViewEdit($id, $page)
   {
        $user = User::where('UserID', $id)->get();
        $accounts = DB::table('account')->get()->pluck('Name','ID');
        return view('admin/view_edit_user')->with('user', $user)->with('accounts', $accounts)->with('page', $page);
   }

   public function userUpdate(Request $request)
   {
       $userid      = $request->input('userid');
       $accountid   = $request->input('accountid');

      
       $request->merge( [ 
           'Active' => (int) $request->input('Active') ?? 0,
           'receive_door_spec_emails' => (int) $request->input('receive_door_spec_emails') ?? 0,
           'ChatSupport' => (int) $request->input('ChatSupport') ?? 0 
           ] );
       

    //  dd($request->all());

       $page        = 'edit';    
       $user = User::findOrFail($userid);
       $user->update($request->all()); 
        

       if(session('back') == 'account'){
        return $this->accountShowEdit($accountid, $page);
       }else{
           return redirect()->route('all-users')->with('success', 'Updated Successfully');
       }
       
   }

   public function getEstimationTestResults()
    {
        $parameters_array = $job_array = Input::get_all();

        $estimation_values = Job::getEstimationValues($job_array, $parameters_array);

        return view('administration.estimation-test-results', $estimation_values);
    }

    public function getEstimationTuning()
    {
        if(!Auth::user()->hasAccountType('master-administrator')){
            abort(403, 'Unauthorized action.');
        }

        return view('administration.estimation-tuning')->with([
            'estimation_parameters' => EstimationParameters::retrieve()
        ]);
    }

    public function postEstimationTuning()
    {
        if(!Auth::user()->hasAccountType('master-administrator')){
            abort(403, 'Unauthorized action.');
        }

        $validator = EstimationParameters::validator(Input::get_all());

        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        
        $estimation_parameters = EstimationParameters::retrieve();
        $estimation_parameters->update(Input::get_all());

        return redirect()->back()->with('success', 'Update Successful');
    }

    public function getFinishes()
    {
        return view('administration.finishes')->with([
            'style_finishes' => StyleFinish::orderBy('Name')->get(),
            'style_groups'  => StyleGroup::orderBy('Name')->get(),
            'style_group_major_categories' => ['laminate'=>'Laminate', 'veneer'=>'Veneer', 'paint'=>'Paint']
        ]);
    }

    public function postFinishes()
    {   
        if(!Auth::user()->hasAccountType('master-administrator')){
            abort(403, 'Unauthorized action.');
        }

        DB::beginTransaction();

        foreach(Input::get('style_groups') as $id => $values){
            StyleGroup::find($id)->update($values);
        }

        foreach(Input::get('style_finishes') as $id => $values){
            StyleFinish::find($id)->update($values);
        }

        DB::commit();
        
        return redirect()->back()->with('success', 'Update Successful');
    }
}
