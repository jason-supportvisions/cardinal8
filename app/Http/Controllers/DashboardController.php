<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Dashboard;
use App\Model\JobStage;
use App\Model\Job;


class DashboardController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('username')){
            
            $value = Dashboard::getData();
            return view('dashboard/index', [ 'jobs' => $value ]);

        }else{
            return redirect()->route('home');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
    public function inProcess()
    {
        $stage_ids = JobStage::where('in_process', 1)->get()->lists('ID');

        $page_title = "Orders - Overview";

        $jobs = Job::getExpandedSetByStageId($stage_ids);

     
        return view('orders/index', ['page_title' => $page_title, 'stage_ids_list' => $stage_ids, 'jobs' => $jobs]);
    }

}
