<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Job;
use App\Model\AccessoryItem;
use App\Model\JobConstruction;
use App\Model\ProductItem;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class PricerController extends Controller
{
    public function index()
    {
        return view('pricer/index');
    }

    public function getItemPrice($jobID, $Code, $FinishInterior = 'No', $FinishedEnds = 'None')
    {
        
//=============================== JOB ================================================================

        //get base objects/sql needed
            $accessory_item     = AccessoryItem::where('Code', $Code)->first(); //change these to query builder for increased speed DB::table
            $product_item       = ProductItem::where('Code', $Code)->first();
            $job                = Job::where('JobID',$jobID)->first();
            $construction       = JobConstruction::where('JobID',$jobID)->first();
            $specifications     = $job->Specifications;
            $debug_out          = array();
            $item_total         = 0.00;

        //Accessory/Product Check
            if(isset($accessory_item)){
                $item = $accessory_item;
                $prefix = "accessory";
                $item_group = $prefix . "_group";
            }else{
                $item = $product_item;
                $prefix = "product";
                $item_group = $prefix . "_group";
            }
        
        //get Door, style, finish etc prices for this spec
            $jobDoor            = $this->getDoorPricing($construction);
           
//=============================== CABINET =============================================================

        //CABINET->Class Wall, Base, Tall
            $itemClass = $item->$item_group->Class;

        //Base MatGroup Cab pricing (A, B, C, D, E)
            $MaterialGroup          = $construction->CabinetBoxMaterialGroup;
            $CabinetPricingResults  = $item->pricingOptions->where('MaterialGroup', $MaterialGroup );

            foreach($CabinetPricingResults as $row){
                $CabinetCode        = $row->Code;
                $CabinetMaterial    = $row->MaterialGroup;
                $CabinetList        = $row->List;
                $CabinetListMinimum = $row->ListMinimum;
            }

            $debug_label = "<b>Base Price: </b>" . $CabinetCode . " (<b>" . $CabinetMaterial . "</b>) $";
                array_push($debug_out, "$debug_label $CabinetList"); 

            $debug_label = "<b>List Minimum: </b>";
                array_push($debug_out, "$debug_label $CabinetListMinimum"); 

            $item_total += $CabinetList;
            $debug_label = "<b>Base Price $</b>";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->UOM (Each/SqFt)
            
            $ItemUOM = $item->$item_group->UOM;
            $debug_label = "<b>UOM: </b>" ;
                array_push($debug_out, "$debug_label $ItemUOM"); 

        //CABINET->FinishedInterior ($FinishedInteriorList)

            $ItemAllowFinInt = $item->$item_group->FinishedInterior;
            $debug_label = "<b>Allow Finished Interior: </b>";
                array_push($debug_out, "$debug_label $ItemAllowFinInt");

            $debug_label = "<b>Price out Finished Interior? </b> ";
                array_push($debug_out, "$debug_label $FinishInterior");
            
            // run based on URL variable choice 
            // /yes/no
            if($FinishInterior == 'yes'){
                $ItemFinishedIntList = $jobDoor[0]->FinishedInteriorList;
                    array_push($debug_out, "$item_total + $ItemFinishedIntList");

                $item_total += $ItemFinishedIntList;
                $debug_label = "&nbsp;&nbsp;$ ";
                    array_push($debug_out, "$debug_label $item_total");
            }

        //CABINET->FinishedEnd(s) ($FinishedEndsList)

            $ItemFinEnds = $item->$item_group->FinishedEndOption;
            $debug_label = "<b>Allow Finished Ends: </b>";
                array_push($debug_out, "$debug_label $ItemFinEnds");

            //Class determines price on finished ends
            if($itemClass == "Base"){
                $ItemFinEndList = $jobDoor[0]->BaseEndList;
            }
            if($itemClass == "Wall"){
                $ItemFinEndList = $jobDoor[0]->WallEndList;
            }
            if($itemClass == "Tall"){
                $ItemFinEndList = $jobDoor[0]->TallEndList;
            }
            
            $item_total += $ItemFinEndList;            
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->EdgeBanding ($EdgeBandingList)

            $ItemAllowEdge = $item->$item_group->EdgeBanding;
            $debug_label = "<b>Allow Edge Banding: </b>";
                array_push($debug_out, "$debug_label $ItemAllowEdge");

            //get edge banding IDs and Options from spec
            $jobEdge  = $this->getEdgeBanding($construction->EdgeBandingID, $construction->EdgeBandingOption);

            //EdgeBanding 
                // Yes/No

            // $item_total += $jobDoor[0]->CabinetExteriorEdgeID;
            $debug_label = "&nbsp;&nbsp; + $ ";
            array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->Handle/Pull 

            $ItemHandle = $item->$item_group->HandleCount;
            $debug_label = "<b>Handle/Pull: </b>";
                array_push($debug_out, "$debug_label $ItemHandle");

            //$item_total += EdgeBandingList
            $debug_label = "&nbsp;&nbsp; + $ ";
            array_push($debug_out, "$debug_label $item_total "); 
        
        //CABINET->Hinges

            $ItemHinges = $item->$item_group->HingeCount;
            $debug_label = "<b>Hinges: </b>";
                array_push($debug_out, "$debug_label $ItemHinges");

            //$item_total += HingeList
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->Divider

            $ItemDivider = $item->$item_group->DividerCount;
            $debug_label = "<b>Dividers: </b>";
                array_push($debug_out, "$debug_label $ItemDivider");

            //Divider Count

            //$item_total += DividerList;
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->Partition

            $ItemPartition = $item->$item_group->PartitionCount;
            $debug_label = "<b>Partition Count: </b>";
                array_push($debug_out, "$debug_label $ItemPartition");

            //Divider Count

            //$item_total += DividerList;
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->Glass

            $ItemGlass = $item->$item_group->GlassOption;
            $debug_label = "<b>Glass: </b>";
                array_push($debug_out, "$debug_label $ItemGlass");

            //Glass Option
                // Yes / No

            //$item_total += GlassList
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->AdjustableShelf

            $ItemAdjShelf = $item->$item_group->AdjShelfCount;
            $debug_label = "<b>AdjShelf Count: </b>";
                array_push($debug_out, "$debug_label $ItemAdjShelf");

            //AdjShelf Count

            //$item_total += DividerList;
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 

        //CABINET->FixedShelf

            $ItemFixedShelf = $item->$item_group->FixedShelfCount;
            $debug_label = "<b>FixedShelf Count: </b>";
                array_push($debug_out, "$debug_label $ItemFixedShelf");

            //FixedShelf Count

            //$item_total += DividerList;
            $debug_label = "&nbsp;&nbsp; + $ ";
                array_push($debug_out, "$debug_label $item_total "); 


//=============================== DOORS ==================================

            //DOOR->

            //DOOR->[S/M/C/F]

            //DOOR->SpecOptions

            //TALLDOOR->BasePrice

            //TALLDOOR->[S/M/C/F]

            //TALLDOOR->SpecOptions

            //FULLTALLDOOR->BasePrice

            //FULLTALLDOOR->[S/M/C/F]

            //FULLTALLDOOR->SpecOptions

//=============================== DRAWERS ==================================

            //DRAWER->BasePrice

            //DRAWER->[S/M/C/F]

            //DRAWER->SpecOptions

            //LGDRAWER->BasePrice

            //LGDRAWER->[S/M/C/F]

            //LGDRAWER->SpecOptions
                        
//=============================== DRAWERBOXES ==================================

            //DRAWERBOX->Price

            //LGDRAWERBOX->Price

//================================ ACCESSORY ====================================
        
            //ACCESSORY->BasePrice 

            //ACCESSORY->MaterialGroupPricing

            //ACCESSORY->ToeKick

            //ACCESSORY->Overlay

            //ACCESSORY->Panel1

            //ACCESSORY->Panel2

            //Accessory->Shelf

            //Accessory->BoxMaterial

            //Accessory->DOOR

            //Accessory->TALLDOOR

            //Accessory->FULLTALLDOOR

            //Accessory->DRAWER

            //Accessory->LGDRAWER

            //Accessory->Additional Charges

// ======================== Final Pricing =======================================


            //Custom List

            //Admin List
            
        //-----------------------
        // Prices
        //-----------------------

            //TotalList
            //TotalCost
            //TotalNet

            //PerCabinet
            //PerDoor
            //PerDrawer
            //Modifications

        return view('pricer.index')
                    ->with('job', $job)
                    ->with('specifications', $specifications)
                    ->with('construction', $construction)
                    ->with('accessory_item', $accessory_item)
                    ->with('debug_out', $debug_out)
                    ->with('product_item', $product_item);
    }

    public function getSavedPrice($jobID, $job_product_id)
    {
        //Get the prices we need from Job_Construction - do not recalculate if spec hasn't changed
    }

    public function getDoorPricing($spec)
    {

        $DoorStyleID        = $spec->DoorStyleID;
        $StyleMaterialID    = $spec->StyleMaterialID;
        $StyleColorID       = $spec->StyleColorID;
        $StyleFinishID      = $spec->StyleFinishID;
        
        $jobDoorFronts = DB::select(DB::raw("SELECT
                `styles`.`ID` AS `StyleID`,
                `styles`.`StyleType` AS `StyleType`,
                `styles`.`Name` AS `StyleName`,
                `style_material`.`ID` AS `MaterialID`,
                `style_material`.`Name` AS `MaterialName`,
                `style_color`.`ID` AS `ColorID`,
                `style_color`.`Name` AS `ColorName`,
                `style_finish`.`ID` AS `FinishID`,
                `style_finish`.`Name` AS `FinishName`,
    
                `styles`.`DoorList` AS `StyleDoorList`,
                `style_material`.`DoorList` AS `StyleMaterialDoorList`,
                `style_color`.`DoorList` AS `StyleColorDoorList`,
                `style_finish`.`DoorList` AS `StyleFinishDoorList`, 
    
                `styles`.`TallDoorList` AS `StyleTallDoorList`,
                `style_material`.`TallDoorList` AS `StyleMaterialTallDoorList`,
                `style_color`.`TallDoorList` AS `StyleColorTallDoorList`,
                `style_finish`.`TallDoorList` AS `StyleFinishTallDoorList`,
    
                `styles`.`FullTallDoorList` AS `StyleFullTallDoorList`,
                `style_material`.`FullTallDoorList` AS `StyleMaterialFullTallDoorList`,
                `style_color`.`FullTallDoorList` AS `StyleColorFullTallDoorList`,
                `style_finish`.`FullTallDoorList` AS `StyleFinishFullTallDoorList`,
    
                `styles`.`OverlayList` AS `StyleOverlayList`,
                `style_material`.`OverlayList` AS `StyleMaterialOverlayList`,
                `style_color`.`OverlayList` AS `StyleColorOverlayList`,
                `style_finish`.`OverlayList` AS `StyleFinishOverlayList`,
    
                `styles`.`DoorSqFtList` AS `StyleDoorSqFtList`,
                `style_material`.`DoorSqFtList` AS `StyleMaterialDoorSqFtList`,
                `style_color`.`DoorSqFtList` AS `StyleColorDoorSqFtList`,
                `style_finish`.`DoorSqFtList` AS `StyleFinishDoorSqFtList`,
    
                `styles`.`DrawerSqFtList` AS `StyleDrawerSqFtList`,
                `style_material`.`DrawerSqFtList` AS `StyleMaterialDrawerSqFtList`,
                `style_color`.`DrawerSqFtList` AS `StyleColorDrawerSqFtList`,
                `style_finish`.`DrawerSqFtList` AS `StyleFinishDrawerSqFtList`,
    
                `styles`.`WallEndList` AS `StyleWallEndList`,
                `style_material`.`WallEndList` AS `StyleMaterialWallEndList`,
                `style_color`.`WallEndList` AS `StyleColorWallEndList`,
                `style_finish`.`WallEndList` AS `StyleFinishWallEndList`,
    
                `styles`.`BaseEndList` AS `StyleBaseEndList`,
                `style_material`.`BaseEndList` AS `StyleMaterialBaseEndList`,
                `style_color`.`BaseEndList` AS `StyleColorBaseEndList`,
                `style_finish`.`BaseEndList` AS `StyleFinishBaseEndList`,
    
                `styles`.`TallEndList` AS `StyleTallEndList`,
                `style_material`.`TallEndList` AS `StyleMaterialTallEndList`,
                `style_color`.`TallEndList` AS `StyleColorTallEndList`,
                `style_finish`.`TallEndList` AS `StyleFinishTallEndList`,
    
                `styles`.`FinishedInteriorList` AS `StyleFinishedInteriorList`,
                `style_material`.`FinishedInteriorList` AS `StyleMaterialFinishedInteriorList`,
                `style_color`.`FinishedInteriorList` AS `StyleColorFinishedInteriorList`,
                `style_finish`.`FinishedInteriorList` AS `StyleFinishFinishedInteriorList`,
    
                `styles`.`Panel1SSqFtList` AS `StylePanel1List`,
                `style_material`.`Panel1SSqFtList` AS `StyleMaterialPanel1List`,
                `style_color`.`Panel1SSqFtList` AS `StyleColorPanel1List`,
                `style_finish`.`Panel1SSqFtList` AS `StyleFinishPanel1List`,
                
                `styles`.`Panel2SSqFtList` AS `StylePanel2List`,
                `style_material`.`Panel2SSqFtList` AS `StyleMaterialPanel2List`,
                `style_color`.`Panel2SSqFtList` AS `StyleColorPanel2List`,
                `style_finish`.`Panel2SSqFtList` AS `StyleFinishPanel2List`,
    
                `styles`.`DoorList` + `style_material`.`DoorList` + `style_color`.`DoorList` + `style_finish`.`DoorList` AS `DoorList`,
                `style_material`.`DoorList` + `style_color`.`DoorList` + `style_finish`.`DoorList` AS `ShelfStyleList`,
                `styles`.`TallDoorList` + `style_material`.`TallDoorList` + `style_color`.`TallDoorList` + `style_finish`.`TallDoorList` AS `TallDoorList`,
                `styles`.`FullTallDoorList` + `style_material`.`FullTallDoorList` + `style_color`.`FullTallDoorList` + `style_finish`.`FullTallDoorList` AS `FullTallDoorList`,
    
                `styles`.`DrawerList` + `style_material`.`DrawerList` + `style_color`.`DrawerList` + `style_finish`.`DrawerList` AS `DrawerList`,
                `styles`.`LargeDrawerList` + `style_material`.`LargeDrawerList` + `style_color`.`LargeDrawerList` + `style_finish`.`LargeDrawerList` AS `LargeDrawerList`,
                `styles`.`OverlayList` + `style_material`.`OverlayList` + `style_color`.`OverlayList` + `style_finish`.`OverlayList` AS `OverlayList`,
                
                `styles`.`DoorSqFtList` + `style_material`.`DoorSqFtList` + `style_color`.`DoorSqFtList` + `style_finish`.`DoorSqFtList` AS `DoorSqFtList`,
                `styles`.`DrawerSqFtList` + `style_material`.`DrawerSqFtList` + `style_color`.`DrawerSqFtList` + `style_finish`.`DrawerSqFtList` AS `DrawerSqFtList`,
                `style_material`.`FinishMaterialType`,
                
                `styles`.`WallEndList` + `style_material`.`WallEndList` + `style_color`.`WallEndList` + `style_finish`.`WallEndList` AS `WallEndList`,
                `styles`.`BaseEndList` + `style_material`.`BaseEndList` + `style_color`.`BaseEndList` + `style_finish`.`BaseEndList` AS `BaseEndList`,
                `styles`.`TallEndList` + `style_material`.`TallEndList` + `style_color`.`TallEndList` + `style_finish`.`TallEndList` AS `TallEndList`,
                
                `styles`.`FinishedInteriorList` + `style_material`.`FinishedInteriorList` + `style_color`.`FinishedInteriorList` + `style_finish`.`FinishedInteriorList` AS `FinishedInteriorList`,
    
                `styles`.`Panel1SSqFtList` + `style_material`.`Panel1SSqFtList` + `style_color`.`Panel1SSqFtList` + `style_finish`.`Panel1SSqFtList` AS `Panel1SSqFtList`,
                `styles`.`Panel2SSqFtList` + `style_material`.`Panel2SSqFtList` + `style_color`.`Panel2SSqFtList` + `style_finish`.`Panel2SSqFtList` AS `Panel2SSqFtList`
            FROM
                `styles` JOIN `style_material` JOIN `style_color` JOIN `style_finish`
            WHERE
                styles.ID = '$DoorStyleID' AND style_material.ID = '$StyleMaterialID' AND style_color.ID = '$StyleColorID' AND style_finish.ID = '$StyleFinishID'"));
        
        return $jobDoorFronts;
    
    }

    public function getEdgeBanding($CabinetExteriorEdgeID, $EdgeBandingOption)
    {
    
        $EdgeBandingList    = 0;
        $EdgeBanding        = 0;

        if ($EdgeBandingOption == 1) {

            if ($CabinetExteriorEdgeID < 5) {
                $table = "edge_banding_options";
            } else {
                $table = "edge_banding";
            }

            $jobEdgeBanding = DB::table($table)
                ->select('*')
                ->distinct()
                ->where('ID', '=', $CabinetExteriorEdgeID)
                ->get();

            // echo "extEdgeID: $CabinetExteriorEdgeID";
            foreach ($jobEdgeBanding as $row) {
                $EdgeBandingList          = $row->List;
                $EdgeBandingName          = $row->Name;

                if ($CabinetExteriorEdgeID != 0) {
                    $EdgeBanding = 1;
                } else {
                    $EdgeBanding = 0;
                }
            }
        } else {
            $EdgeBanding = 0;
            $EdgeBandingList = 0;
        }
    }

}
