<?php
namespace App\Http\Controllers;
use App\Model\Input;
use App\Model\User;
use App\Model\Account;

class ContactsController extends Controller {

     public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index()
	{
        if(session('username')){
            $users = User::whereIn('AccountType', [1,2])
                    ->with('account')
                    ->get();

		    return view('contacts/index')
                    ->with('users', $users);

        }else{
            return redirect()->route('home');
        }
        
	}

    public function edit($UserID)
    {
        $user = User::find($UserID);
        
        $accounts_list = Account::all()->pluck('Name', 'ID');

        return view('contacts.edit')
                ->with('accounts_list', $accounts_list)
                ->with('user', $user);
    }

    public function update($UserID)
    {
        $user = User::find($UserID);

        $user->update(Input::get_all());

        return Redirect()->to('/crm/contacts')
                ->with('success', "{$user->FirstName} {$user->LastName} updated.");
    }

}
