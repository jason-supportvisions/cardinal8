<?php

class FinishedEndsController extends BaseController {

	public function index()
	{
        if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }

        $finish_material_types = [
            '' => '',
            'Laminate' => 'Laminate',
            'Paint' => 'Paint',
            'Veneer' => 'Veneer',
            'N/A' => 'N/A'
        ];

        $styles = Style::with('vendor')->with('group')->with('brand')
                    ->orderBy('VendorID')
                    ->orderBy('GroupID')
                    ->orderBy('BrandID')
                    ->orderBy('StyleType')
                    ->orderBy('Name')
                    ->get();

        return View::make('finished-ends.index', compact(
            'styles',
            'finish_material_types'
        ));
	}

    public function store()
    {
        if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }

        foreach(Input::get('styles') as $id => $style_data){
            
            Style::find($id)
                ->update($style_data);
        }

        return Redirect::back()->with('success', 'Finished Ends Updated');
    }

   

}
