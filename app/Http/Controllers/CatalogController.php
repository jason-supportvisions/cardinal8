<?php
namespace App\Http\Controllers;
use App\Model\Input;
use App\Model\CatalogCategory;
use PDF;
class CatalogController extends Controller {

	// public function show()
	// {
    //     $categories = CatalogCategory::all();

    //     return View::make('catalog.index', compact(
    //         'categories'
    //     ));
    // }

    public function index()
	{
        $categories = CatalogCategory::all();
        return view('catalog.index', compact('categories'));
	}

    public function ord()
	{
        $category = CatalogCategory::find(11);
        return view('catalog.ord', compact('category'));
    }

    public function getDownload($filename)
    {
        //PDF file is stored under project/public/download/info.pdf
        $file= public_path(). "/download/info.pdf";

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'filename.pdf', $headers);
    }
    
    public function generate()
    {
        $debug = "";
        $format             = Input::get('format');
        $category_id        = Input::get('category_id');
        //door pdfs
        if($category_id == 101 || $category_id == 102 || $category_id == 103){

            if($category_id == 101){
                $file= public_path(). "/downloads/TransitionalDoors20181.pdf";
                $filename = "TransitionalDoors20181.pdf";
            }
            if($category_id == 102){
                $file= public_path(). "/downloads/TraditionalDoors20181.pdf";
                $filename = "TraditionalDoors20181.pdf";
            }
            if($category_id == 103){
                $file= public_path(). "/downloads/ContemporaryDoors20181.pdf";
                $filename = "ContemporaryDoors20181.pdf";
            }
            
            $headers = array('Content-Type: application/pdf',);
            return response()->download($file, $filename, $headers);
        }else{

            if($format == 'pdf'){
                $http_base_path = 'file:///'.$_SERVER['SERVER_NAME'].'/';
            }else {
                $http_base_path = 'https:///'.$_SERVER['SERVER_NAME'].'/';
            }
            $category           = CatalogCategory::find($category_id);

    
            //accessories
            if ($category->name == "Accessory"){
                $debug              .= "<br> ACCESSORY";
                $title              = "Accessories";
                $itemType           = "accessory_item";
                $groupType          = "accessory_group";
                $debug              .= "[$title] [$itemType] [$groupType] ";

            
            //mods
            }elseif( $category->name == "Mods"){
                $debug              .= "<br> Mods";
                $title              = "Modifications";
                $itemType           = "modification_item";
                $groupType          = "modification_item_type"; 
                $debug              .= "[$title] [$itemType] [$groupType] ";

            //most categories
            }else{
                $debug              .= "not accessory / not mods";
                $title              = $category->name . " Cabinets";
                $itemType           = "product_item";
                $groupType          = "product_group";
                $debug             .= "[$title] [$itemType] [$groupType] ";
            }

            //  echo $debug;
            $productGroupObj    = $category->$groupType ; 
            $data               = compact('http_base_path', 'category', 'format', 'productGroupObj', 'title');

            $header_html        = view('catalog.header')
                                        ->with(compact('category', 'http_base_path', 'title'))
                                        ->render();

            $footer_html        = view('catalog.footer')
                                        ->with(compact('category'))
                                        ->render();

            $footer_center      = "{$category->page_number_prefix}.[page]";
            $dYear              = date('Y');
            $dDate              = date('m') . '-' . date('d') . '-' . date('Y');
            if($format == 'pdf') {
            $response           = PDF::loadView('catalog.generate', $data)
                                    ->setOption('margin-top', '24')
                                    ->setOption('margin-left', '16')
                                    ->setOption('margin-right', '16')
                                    ->setOption('margin-bottom', '19')
                                    ->setOption('page-size', 'Letter')
                                    ->setOption('header-spacing', '5')
                                    ->setOption('header-html', $header_html)
                                    ->setOption('footer-html', $footer_html)
                                    ->setOption('footer-center', $footer_center)
                                    ->setOption('title', 'CoryMFG ' . $category->name . ' Catalog ' . $dYear)
                                    ->download("CoryMFG-{$category->slug}-catalog-{$dDate}.pdf");
                                    setCookie('downloadStarted', 1, time() + 20, '/', '', false, false);
                                return $response;
            } else {
                return view('catalog.generate', $data);
            }
        }
    }

}
