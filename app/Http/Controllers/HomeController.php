<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        
        session(['userid'       => $user->UserID]);
        session(['accountid'    => $user->AccountID]);
        session(['accounttype'  => $user->AccountType]);
        session(['firstname'    => $user->FirstName]);
        session(['lastname'     => $user->LastName]);
        session(['username'     => $user->Username]);

        return redirect()->route('dashboard');
    }
}
