<?php

class DoorController extends \BaseController {


	public function __construct()
	{
		parent::__construct();
		View::share('page_name', 'Door Combinations');

		if(!isset($this->user) || !$this->user->hasAccountType('master-administrator')){
			App::abort(403);
		}
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$inside_profiles = InsideProfile::all();

		return View::make('doors.index', compact(
			'inside_profiles'
		));
	}

	public function doorsWithMissingImages()
	{
		$doors_with_missing_images = Door::getDoorsWithMissingImages();

		return View::make('doors.doors-with-missing-images', compact(
			'doors_with_missing_images'
		));
	}

	public function drawersWithMissingImages()
	{
		$drawers_with_missing_images = Door::getDrawersWithMissingImages();

		return View::make('doors.drawers-with-missing-images', compact(
			'drawers_with_missing_images'
		));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$inside_profile = InsideProfile::find($id);

        $center_panels      = CenterPanel::all();
        $outside_profiles   = OutsideProfile::all();
        $stile_rails        = StileRail::all();

		return View::make('doors.edit', compact(
			'inside_profile',
			'center_panels',
			'outside_profiles',
			'stile_rails'
		));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$inside_profile = InsideProfile::find($id);
		$style= $inside_profile->style;

		$inside_profile->Name = Input::get('Name');
		$inside_profile->show_in_door_builder = Input::get('show_in_door_builder', 0);
		$inside_profile->save();

		$inside_profile->setAssociatedComponentIds('OutsideProfileID', Input::get('outside_profile_ids'));
		$inside_profile->setAssociatedComponentIds('CenterPanelID', Input::get('center_panel_ids'));
		$inside_profile->setAssociatedComponentIds('StileRailID', Input::get('stile_rail_ids'));

		return Redirect::to('/backend/doors/')
			->with('success', "{$inside_profile->Name} Door Combination Options Updated");
	}




}
