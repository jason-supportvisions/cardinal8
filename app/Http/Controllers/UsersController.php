<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use Illuminate\Support\Facades\DB;


class UsersController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('username')){
            $SesAcct   = session('accountid');
            $SesType   = session('accounttype');
            $SesUser   = session('userid');
 
            session(['back'=>'user']);
            $users = User::All();

            //only allow admins
            if($SesType == -1){
                return view('admin/index')->with('users', $users);
            }else{
                return redirect()->route('home');
            }
            
        }else{
            return redirect()->route('home');
        }
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $accounts   = DB::table('account')->get()->pluck('Name','ID');
        $page       = 'edit';
        return view('admin/create-user')->with('accounts', $accounts)->with('page', $page);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $accountID = $request->input('accountid');
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'email' => 'required|email'
        ]);
        User::create($request->all());

        return redirect()->route('show-edit',['id'=>$accountID, 'page'=>'edit'])->with('success', 'User created successfully.');
    }

    public function userStore(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
            'email' => 'required|email'
        ]);
        $user = User::create([
            'Username' => $request->Username,
            'password' => $request->password,
            'email' => $request->email
        ]);
        $user->update($request->all());

        return $this->index()->with('success', 'User created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        User::where('UserID', $id)->delete();

        return redirect()->back()->with('success', 'User deleted successfully');
    }
}
