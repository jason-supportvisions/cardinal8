<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\User;
use App\Model\Input;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Model\Job;


class PassresetController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function index()
    {
        return view('admin/reset');
        
    }
    public function updatePassword(Request $request)
    {        
        $email          = Input::get('email');
        $oldpassword    = Input::get('oldpassword');
        $newpassword    = Input::get('password');
        $comfirmation   = Input::get('password_confirmation');
        $this->validate($request, [
            'email' => 'required|email',
            'oldpassword' => 'required|min:8',
            'password' => 'min:8|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:8'
        ]);
        $user           = User::where('email', $email)->first();
        $old_hash           = $user->password;
        
        $salt           = $user->Salt;
        $new_hash       = $this->phphashPassword($oldpassword, $salt);
            
        if($old_hash == $new_hash){
            $user->update(['password' => $newpassword]);
        } 
        return redirect()->route('home');
    }

    public function phphashPassword($pass,$salt)
    {
        $check_password = hash('sha256', $pass . $salt);
        $check_password = hash('sha256', $check_password . $salt);
        return $check_password;
    }
    
}
