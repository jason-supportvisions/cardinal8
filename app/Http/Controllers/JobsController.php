<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DataTables;
use App\Model\Jobs;
use PDF;
use App\Model\Input;

class JobsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('username')){
            $SesAcct   = session('accountid');
            $SesType   = session('accounttype');
            $SesUser   = session('userid');
            $data = Jobs::getJobs($SesAcct, $SesType, $SesUser);
            // dd($data);
            return view('jobs/index')->with('data',$data);

        }else{
            return redirect()->route('home');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancel()
    {
        $SesAcct   = session('accountid');
        $SesType   = session('accounttype');
        
        $data = Jobs::getcancelledjobs($SesAcct, $SesType);
        return view('jobs/cancelled')->with('data',$data);
    }

    public function jobStage($jobstage)
    {
        $SesAcct   = session('accountid');
        $SesType   = session('accounttype');
        $data = Jobs::getstagejobs($SesAcct, $SesType, $jobstage);
        return view('jobs/cancelled')->with('data',$data);
    }

    public function jobPdf($jobid){
        $type = Input::get('type');
        if($type == 'view-job'){
            $data = Jobs::getjobinfo($jobid);
            $http_base_path  = 'file:///' . public_path() . '/';
            $logo_image_path = './images/logos/' . $data[1][0]->logo_image_small;
            
            $pdf = PDF::loadView('jobs.pdf', compact(
                'http_base_path',
                'data',
                'logo_image_path'
            ))->setOption('margin-top', '10mm')
            ->setOption('margin-bottom', '10mm')
            ->setOption('margin-left', '5mm')
            ->setOption('margin-right', '5mm')
            ->setOption('enable-local-file-access', 'true')
            ->setPaper('Letter');
            return $pdf->stream('CoryMFG-Pdf-Jobs' . $jobid . '.pdf');
        }else{
            $data = Jobs::getjobinfo($jobid);
            $http_base_path  = 'file:///' . public_path() . '/';
            $logo_image_path = './images/logos/' . $data[1][0]->logo_image_small;
            
             return PDF::loadView('jobs.pdf', compact(
                    'http_base_path',
                    'data',
                    'logo_image_path'           
                    ))->setOption('margin-top', '15mm')
                        ->setOption('margin-bottom', '7mm')
                        ->setOption('margin-left', '9mm')
                        ->setOption('margin-right', '7mm')
                        ->setOption('enable-local-file-access', 'true')
                        ->setPaper('Letter')
                        ->stream('CoryMfg-CabCard-Job' . $jobid . '.pdf');
        }
        
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $jobID = $request->id;
        if ($request->ajax()) {
            $data = Jobs::getjobinfo($jobID);
            $data = view('jobs.job_modal', compact('data'))->render();
            return response()->json(['html'=>$data]);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($jobID, $jobAcct,$active)
    {
        $result = Jobs::updateActive($jobID, $jobAcct,$active);
        if($result == 1){
            return redirect('/jobs');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
