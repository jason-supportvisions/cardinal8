<?php

class FrontsController extends BaseController {

	public function index()
	{
        if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }

        $styles = Style::with('vendor')->with('group')->with('brand')
                    ->orderBy('VendorID')
                    ->orderBy('GroupID')
                    ->orderBy('BrandID')
                    ->orderBy('StyleType')
                    ->orderBy('Name')
                    ->get();

        return View::make('fronts.index', compact(
            'styles'
        ));

	}

    public function store()
    {
        if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }

        foreach(Input::get('styles') as $id => $style_data){
            
            Style::find($id)
                ->update($style_data);
        }

        return Redirect::back()->with('success', 'Styles Updated');
    }

}
