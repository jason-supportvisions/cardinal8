<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Model\Quoteorders;
use App\Model\Job;
use App\Model\Account;
use App\Model\User;
use App\Model\AccessoryItem;
use App\Model\Input;
use App\Model\JobProduct;
use App\Model\ModificationItemType;
use App\Model\JobProductModification;
use App\Model\JobProductPart;
use PDF;
use Illuminate\Support\Facades\DB;

class QuoteordersController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        
        if(session('username')){
            $init_data = Quoteorders::initData();
            return view('quote_orders/index', ['data' =>  $init_data]);
        }else{
            return redirect()->route('home');
        }
       
    }

    public function product($jobID)
    {
        $job = Job::find($jobID);
        // session(['accountid' => $job->AccountID]);
		$pricing = "LIST";
        $job->updateList();
        return view('quote_orders/products/index', ['job' => $job, 'pricing' => $pricing]);
    }

    public function store(Request $request)
    {
        $jobAcct                = $request->input('jobAcct');
        $created_at             = $updated_at = $request->input('created_at');
        $userid                 = $request->input('userid');
        $user                   = User::find($userid)->first();
        $jobMultiplier          = $request->input('jobMultiplier');
        $jobCabinetDrwRunner    = 0;
        $accountid              =  $user->AccountID;
        $account                = Account::find($accountid)->first();
        $multiplier_discount    = $account->MultiplierDiscount;
        $jobID                  = $request->input('jobID');
        $brand                  = $request->input('brand');
        $jobType                = $request->input('jobType');
        $jobStage               = $request->input('jobStage');
        $jobRef                 = substr($request->input('jobRef'),0,50); 
        $mod                    = $request->input('jobmod');


        // dd($request->session('userid'));

/*

"_token" => "1XIoIohd2ffrkxc9WR0DnRx2duK8ayLTFfHemJk9"
    "url" => []
    "_previous" => array:1 [▶]
    "_flash" => array:2 [▶]
    "login_web_59ba36addc2b2f9401580f014c7f58ea4e30989d" => 187
    "auth" => array:1 [▶]
    "userid" => 187
    "accountid" => 55
    "accounttype" => -1
    "firstname" => "Jay Dev"
    "lastname" => null
    "username" => "Development"
    "jobAcct" => "55"
    "jobID" => "41660"
    "alert" => ""
    "product_type" => "product"
    "addtype" => ""
    "back" => "user"

*/


        //put this job's acct in the session
        $request->session()->put('jobAcct', $jobAcct);

        
        if(isset($jobMultiplier)){
            $multiplier_customer = $request->input('jobMultiplier');
        }else{
            $multiplier_customer = $user->MultiplierCustomer;
        }      
        
        $cusCompany             = $request->input('cusCompany');
        $cusName                = $request->input('cusName');
        $cusAdd1                = $request->input('cusAdd1');
        $cusAdd2                = $request->input('cusAdd2');
        $cusCity                = $request->input('cusCity');
        $cusState               = $request->input('cusState');
        $cusZip                 = $request->input('cusZip');
        $cusPhone               = $request->input('cusPhone');
        $cusEmail               = $request->input('cusEmail');
        $cusNotes               = $request->input('cusNotes');

        $shipCompany            = $request->input('shipCompany');
        $shipName               = $request->input('shipName');
        $shipAdd1               = $request->input('shipAdd1');
        $shipAdd2               = $request->input('shipAdd2');
        $shipCity               = $request->input('shipCity');
        $shipState              = $request->input('shipState');
        $shipZip                = $request->input('shipZip');
        $shipPhone              = $request->input('shipPhone');
        $shipEmail              = $request->input('shipEmail');
        $shipNotes              = $request->input('shipNotes');
        $cusCompany             = isset($cusCompany) ? $cusCompany : '';
        $cusName                = isset($cusName) ? $cusName : '';
        $cusAdd1                = isset($cusAdd1) ? $cusAdd1 : '';
        $cusAdd2                = isset($cusAdd2) ? $cusAdd2 : '';
        $cusCity                = isset($cusCity) ? $cusCity : '';
        $cusState               = isset($cusState) ? $cusState : '';
        $cusZip                 = isset($cusZip) ? $cusZip : '';
        $cusPhone               = isset($cusPhone) ? $cusPhone : '';
        $cusEmail               = isset($cusEmail) ? $cusEmail : '';
        $cusNotes               = isset($cusNotes) ? $cusNotes : '';
        $shipCompany            = isset($shipCompany) ? $shipCompany : '';
        $shipName               = isset($shipName) ? $shipName : '';
        $shipAdd1               = isset($shipAdd1) ? $shipAdd1 : '';
        $shipAdd2               = isset($shipAdd2) ? $shipAdd2 : '';
        $shipCity               = isset($shipCity) ? $shipCity : '';
        $shipState              = isset($shipState) ? $shipState : '';
        $shipZip                = isset($shipZip) ? $shipZip : '';
        $shipPhone              = isset($shipPhone) ? $shipPhone : '';
        $shipEmail              = isset($shipEmail) ? $shipEmail : ''; 
        $shipNotes              = isset($shipNotes) ? $shipNotes : '';

        
        $jobSQL = array(
            'AccountID' => $jobAcct, 'Brand' => $brand, 'Type' => $jobType,
            'Stage' => $jobStage, 'Reference' => $jobRef, 'CreatedID' => $userid,
            'multiplier_customer' => $multiplier_customer, 'multiplier_discount' => $multiplier_discount, 'ModifiedDate' => $updated_at
        );
        
        
        //Store new Job Header
        if($jobID != null){
            
            $jobSQL = array(
                'AccountID' => $jobAcct, 'Brand' => $brand, 'Type' => $jobType,
                'Stage' => $jobStage, 'Reference' => $jobRef, 
                'multiplier_customer' => $multiplier_customer, 'multiplier_discount' => $multiplier_discount, 'ModifiedDate' => $updated_at
            );
            
            if($mod == 'edit'){
                
                //update existing job 
                $result = Quoteorders::updateJob($jobID, $jobSQL);

                $cusSQL = array(
                    'AccountID' => $jobAcct,
                    'ContactName' => $cusName, 'Company' => $cusCompany,
                    'AddressLine1' => $cusAdd1, 'AddressLine2' => $cusAdd2,
                    'City' => $cusCity, 'State' => $cusState,
                    'PostalCode' => $cusZip, 'PhoneNumber' => $cusPhone,
                    'EmailAddress' => $cusEmail, 'Notes' => $cusNotes
                );

                $shipSQL = array(
                    'AccountID' => $jobAcct,
                    'ContactName' => $shipName, 'Company' => $shipCompany,
                    'AddressLine1' => $shipAdd1, 'AddressLine2' => $shipAdd2,
                    'City' => $shipCity, 'State' => $shipState, 
                    'PostalCode' => $shipZip, 'PhoneNumber' => $shipPhone,
                    'EmailAddress' => $shipEmail, 'Notes' => $shipNotes
                );

                $historySQL = array(
                    'job_id' => $jobID, 'stage_id' => $jobStage, 'user_id' => $userid,
                    'created_at' => $created_at, 'updated_at' => $updated_at
                );

                $data = array($cusSQL, $shipSQL, $historySQL);
                Quoteorders::updateData($jobID, $data);
                
       
            }else{
                
                //Insert New Job
                $insertID   = Quoteorders::insertJob($jobSQL);
                $result     = Quoteorders::getjobID($jobRef, $jobAcct);
                $jobID      = $result->JobID;
                
                $historySQL = array(
                    'job_id' => $jobID, 'stage_id' => $jobStage, 'user_id' => $userid,
                    'created_at' => $created_at, 'updated_at' => $updated_at
                );

                // if($mod == 'copyfull'){
                    
                //     $cusSQL = array(
                //         'JobID' => $jobID, 'AccountID' => $jobAcct, 'AddressType' => 'Customer',
                //         'ContactName' => $cusName, 'Company' => $cusCompany,
                //         'AddressLine1' => $cusAdd1, 'AddressLine2' => $cusAdd2,
                //         'City' => $cusCity, 'State' => $cusState, 
                //         'PostalCode' => $cusZip, 'PhoneNumber' => $cusPhone,
                //         'EmailAddress' => $cusEmail, 'Notes' => $cusNotes
                //     );

                //     $shipSQL = array(
                //         'JobID' => $jobID, 'AccountID' => $jobAcct, 'AddressType' => 'Shipping',
                //         'ContactName' => $shipName, 'Company' => $shipCompany,
                //         'AddressLine1' => $shipAdd1, 'AddressLine2' => $shipAdd2,
                //         'City' => $shipCity, 'State' => $shipState, 
                //         'PostalCode' => $shipZip, 'PhoneNumber' => $shipPhone,
                //         'EmailAddress' => $shipEmail, 'Notes' => $shipNotes
                //     );



                //     $data = array($cusSQL, $shipSQL, $historySQL);
                //     $value = Quoteorders::insertData($data);
                // }

                // /* not called */
                // if($mod == 'copyhead'){
                //     // $inserID = Quoteorders::insertJob($jobSQL);
                //     // $result = Quoteorders::getjobID($jobRef, $jobAcct);
                //     // dd($result);
                //     // DB::table('job_header')->insert($jobSQL);
                //     DB::table('job_histories')->insert($historySQL);
                // }
            }
            
        }else{
            
            $result = Quoteorders::insertJob($jobSQL);
            // $result = Quoteorders::getjobID($jobRef, $jobAcct);
            // dd($result->JobID);
            $jobID = $result->JobID;
            
            // $jobID = $result->JobID;
            $cusSQL = array(
                'JobID' => $jobID, 'AccountID' => $jobAcct, 'AddressType' => 'Customer',
                'ContactName' => $cusName, 'Company' => $cusCompany,
                'AddressLine1' => $cusAdd1, 'AddressLine2' => $cusAdd2,
                'City' => $cusCity, 'State' => $cusState, 
                'PostalCode' => $cusZip, 'PhoneNumber' => $cusPhone,
                'EmailAddress' => $cusEmail, 'Notes' => $cusNotes
            );
            $shipSQL = array(
                'JobID' => $jobID, 'AccountID' => $jobAcct, 'AddressType' => 'Shipping',
                'ContactName' => $shipName, 'Company' => $shipCompany,
                'AddressLine1' => $shipAdd1, 'AddressLine2' => $shipAdd2,
                'City' => $shipCity, 'State' => $shipState,
                'PostalCode' => $shipZip, 'PhoneNumber' => $shipPhone,
                'EmailAddress' => $shipEmail, 'Notes' => $shipNotes
            );
            $historySQL = array(
                'job_id' => $jobID, 'stage_id' => $jobStage, 
                'created_at' => $created_at, 'updated_at' => $updated_at
            );

            $data = array($cusSQL, $shipSQL, $historySQL);
            $value = Quoteorders::insertData($data);
        }
        
        $request->session()->put('jobID', $jobID);

        return $this->consIndex(); 
    }

    public function getgeneralnotes($jobID, $jobAcct)
    {
        $notes = Quoteorders::getnotes($jobID, $jobAcct);
        $data = array($jobID, $jobAcct, $notes);
        $filePath = '/documents/' . $jobID . '/';
        return view('quote_orders/generalnotes', ['data' => $data, 'filePath' => $filePath]);
    }

    public function fileupload(Request $request)
    {
        $uploadedfile   =   $request->file('loadfile');
        $jobID          =   $request->input('jobID');
        $userID         =   $request->input('userID');
        $jobAcct        =   $request->input('jobAcct');
        $type           =   $request->input('type');
        $title          =  $request->input('title');
        $note           =   $request->input('note');
        $id             =   $request->input('ID');
        $loaddate       =   date('c');
        $filename           = '';
        $filepath       = '\\documents\\';
        $title          = '';

        //does post have a file? if so upload to cardinal/public/documents/jobid/filename
        if ($request->hasfile('loadfile')) {
            $filename = $uploadedfile->getClientOriginalName();
            $uploadedfile->move(public_path() . '/documents/' . $jobID . '/', $filename);
            $filepath = public_path() . '\\documents\\' . $jobID . '\\' . $filename;
        }

        //does the file already exist?
        if(isset($id)){
            $existing_file = DB::table('job_files')->select('File')->where('ID', '=', $id)->first();
            if($filename == ''){
                $filename = $existing_file->File;
            }
        }

        //build obj
        $job_file = array(
            'JobID' => $jobID, 'AccountID' => $jobAcct, 'Type' => $type, 'File' => $filename,
            'Note' => $note, 'Date' => $loaddate, 'CreatedID' => $userID, 'FilePath' => $filepath, 'Title' => $title
        );

        //convert to object
        $job_file = json_decode( json_encode($job_file ), true);

        if(isset($id)){
            Quoteorders::updateFiles($id, $job_file);
        }else{
            Quoteorders::insertFiles($job_file);
        }
        
        return $this->getgeneralnotes($jobID, $jobAcct);
    }

    public function upload_edit($jobID, $jobAcct, $id)
    {
        $notes = Quoteorders::geteditnotes($jobID, $jobAcct, $id);
        $data = array($jobID, $jobAcct, $notes);
        return view('quote_orders/generalnotes_edit', ['data' => $data]);
    }

    public function upload_delete($id)
    {
        $data = DB::table('job_files')->select('JobID', 'AccountID')->where('ID', '=', $id)->first();
        $uploadfile = DB::table('job_files')->where('ID', '=', $id);
        $uploadfile->delete();
        return $this->getgeneralnotes($data->JobID, $data->AccountID);
        
    }

    public function summary($jobID)
    {
        $job = Job::find($jobID);
        $pricing = "LIST";
        $logo_image_path = $job->brand->logo_image_large_path;
        return view('quote_orders/summary', ['job' => $job, 'pricing' => $pricing, 'logo_image_path' => $logo_image_path]);
    }

    public function final_save()
    {
        return  app('App\Http\Controllers\DashboardController')->index();
    }

    public function summaryPdf($JobID)
    {
        $type = Input::get('type', 'view-summary');
        $job = Job::find($JobID);
        $pricing = Input::get('Price', 'LIST');
        $http_base_path  = 'file:///' . public_path() . '/';
        $http_host = $_SERVER['HTTP_HOST'];
        $logo_image_path = 'http://'. $http_host .'/images/logos/' . $job->brand->logo_image_large;

        $pdf = PDF::loadView('quote_orders.standalone-summary', compact(
            'http_base_path',
            'job',
            'pricing',
            'logo_image_path',
            'http_host'
        ))->setOption('enable-local-file-access', true)
        ->setOption('enable-internal-links' , true)
        ->setOption('disable-external-links' ,true)
        ->setOption('header-html', '')
        ->setOption('margin-top', '10mm')
        ->setOption('margin-bottom', '10mm')
        ->setOption('margin-left', '5mm')
        ->setOption('margin-right', '5mm')
        ->setPaper('Letter');

        if ($type == 'view-summary') {
            return $pdf->stream('CoryMFG-design-spec-Job' . $JobID . '.pdf');
        } elseif ($type == 'download-summary') {
            return $pdf->download('CoryMFG-design-spec-Job' . $JobID . '.pdf');
        } elseif ($type == 'download-cabcard') {
            return $this->cabinetCardsPdf($JobID);
        } elseif ($type == 'download-signoff') {
            return $this->signoffPdf($JobID);
        } else {
            // return 'view cards';
            return $pdf->download('CoryMFG-design-spec-Job' . $JobID . '.pdf');
        }
    }

    public function cabinetCardsPdf($JobID)
    {
        $job = Job::find($JobID);
        $http_base_path  = 'file:///' . public_path() . '/';
        $http_host = $_SERVER['HTTP_HOST'];

        $pdf = PDF::loadView('quote_orders.cabinet-cards', compact(
            'http_base_path',
            'job',
            'http_host'            
            ))->setOption('enable-local-file-access', true)
            ->setOption('enable-internal-links' , true)
            ->setOption('disable-external-links' ,true)
            ->setOption('header-html', '')
            ->setOption('margin-top', '15mm')
            ->setOption('margin-bottom', '7mm')
            ->setOption('margin-left', '9mm')
            ->setOption('margin-right', '7mm')
            ->setPaper('Letter')
            ->stream('CoryMFG-CabCard-Job' . $JobID . '.pdf');
        
            return $pdf;
    }

    public function signoffPdf($JobID)
    {
        $job = Job::find($JobID);
        $pricing = 'COST'; 
        $http_base_path  = 'file:///' . public_path() . '/';
        $http_host = $_SERVER['HTTP_HOST'];
        $logo_image_path = 'http://'. $http_host .'/images/logos/' . $job->brand->logo_image_large;

        return PDF::loadView('quote_orders.summary-signoff', compact(
                'http_base_path',
                'http_host',
                'job',
                'pricing',
                'logo_image_path'
                ))->setOption('margin-top', '10mm')
                ->setOption('margin-bottom', '10mm')
                ->setOption('margin-left', '5mm')
                ->setOption('margin-right', '5mm')
                ->setPaper('Letter')
                ->setOption('enable-local-file-access', true)
                ->setOption('enable-internal-links' , true)
                ->setOption('disable-external-links' ,true)
                ->download('CoryMfgSignOff-Job' . $JobID . '.pdf');
            }

    
            public function addProduct($jobID, $jobAcct , $Category){
        return view('quote_orders/products/add', ['jobID' => $jobID, 'jobAcct' => $jobAcct, 'Category' => $Category]);
    }

    public function searchProduct(Request $request){
        $jobID              = $request->input('jobID');
        $jobAcct            = $request->input('jobAcct');
        $searchItemCode     = $request->input('searchItemCode');
        $searchDescription  = $request->input('searchDescription');
        $autosearch         = $request->input('autosearch');
        $searchDescription     = isset($searchDescription) ? $searchDescription : '';
        $search = 'yes';
        return view('quote_orders/products/search', ['jobID' => $jobID, 'jobAcct' => $jobAcct, 'searchItemCode' => $searchItemCode, 'searchDescription' => $searchDescription, 'autosearch' => $autosearch, 'search' => $search]);
    }

    public function createProduct($jobID, $product_item_id, $productType){
        session(['product_type' => $productType]);
        $job = Job::findOrFail($jobID);

        $job_product    = new JobProduct([
            'JobID' => $jobID,
            'product_item_id' => $product_item_id,
            'product_type' => $productType
        ]);

        if($productType == "accessory"){
            $job_product->product_type = "accessory";
            $job_product->item_type = "accessory_item";
            $job_product->group_type = "accessory_group";
        }else{
            $job_product->product_type = "product";
            $job_product->item_type = "product_item";
            $job_product->group_type = "product_group";
        }

        $itemType = $job_product->item_type;
        $job_product->Qty = 1;
        
        if($job_product->$itemType){
            $job_product->Width = $job_product->$itemType->Width;
            $job_product->Height = $job_product->$itemType->Height;
            $job_product->Depth = $job_product->$itemType->Depth;
        }
        $add_or_edit = 'add';
        $modification_item_types = ModificationItemType::all();
        return view('quote_orders/products/create', ['job_product' =>  $job_product, 'modification_item_types' => $modification_item_types, 'add_or_edit' => $add_or_edit]);
    }

    public function fastAdd(Request $request){
        $fastAddRadio   = $request->input('addType');
        $fastAddCode    = $request->input('fastAdd');
        $jobID          = $request->input('jobID');  
        $itemSQL        = Quoteorders::getItems($fastAddCode);
        if(count($itemSQL)){
            foreach ($itemSQL as $row){
                $product_item_id    = $row->ID;
                $productType        = $row->Type;
            }
            if($fastAddRadio == "edit"){
                return $this->createProduct($jobID, $product_item_id, $productType);
            }else{
                session(['addtype' => 'fast']);
                return $this->createProduct($jobID, $product_item_id, $productType);
            }
        }else{
            session(['alert' => 'FAST ADD error: Code Not Found']);
            return $this->product($jobID);
        }
    }

    public function storeProduct($JobID){
        $job        = Job::findOrFail($JobID);
        $prodType   = "product";
    
        $job_product = JobProduct::create([
            'JobID' => $JobID,
            'Type' => $prodType,
            'AccountID' => $job->AccountID,
            'product_item_id' => Input::get('product_item_id')
        ]);

		$job_product->update(Input::get_all());
        // dd($job);
		$job->updateList();

        return redirect()->route('quote_orders.product', ['jobID' => $JobID]);
        // return back();quote_orders.product
		// return $this->product($JobID);

    }

    public function show($jobID, $job_product_id)
    {
        $job_product    = JobProduct::where('ID', $job_product_id)->first();                      
		$job = $job_product->job;


		$modification_item_types = ModificationItemType::all();
		$saved_mods = JobProductModification::where('job_product_id', $job_product_id)->get();
		$saved_mods_array = [];

		foreach($saved_mods as $saved_mod){
			$saved_mods_array[$saved_mod->modification_item_id] = $saved_mod;
		}

		$add_or_edit = 'add';
        // $job_product->job->updateList();

		return view('quote_orders.products.show')->with(compact(
			'job_product', 
			'modification_item_types',
			'saved_mods_array',
			'add_or_edit'
		));
    }

    public function edit($jobID, $jobAcct, $mod)
    {
        $data       = Quoteorders::getCopyJobData($jobID, $jobAcct);
        $dataEdit   = Quoteorders::getData($jobID, $jobAcct, "edit");

        //Duplicate HEADER Only
        if($mod === 'copyhead'){
           
            //insert job  
                $task       = 'Header Copy';
                $data       = json_decode(json_encode($data), true);
                              array_shift($data['job_results']); //remove ID field from array                
                $data       = (object) $data; //convert array back to obj
                $newJob     = Quoteorders::insertJob($data->job_results);
                $newJobID   = $newJob->JobID;

            //remove Primary Keys from copied tables
                unset($data->cus_results['JobID']);
                unset($data->cus_results['HeaderID']);
                unset($data->ship_results['JobID']);
                unset($data->ship_results['HeaderID']);
                unset($data->job_histories['id']);
                unset($data->job_histories['job_id']);
                unset($data->spec_results['ID']);
                unset($data->spec_results['JobID']);

            //add NEW JobID into table data
                $data->cus_results['JobID'] = $newJobID;
                $data->ship_results['JobID'] = $newJobID;
                $data->spec_results['JobID'] = $newJobID;
                $data->job_results['JobID'] = $jobID;
                

            //insert copied data with new IDs
                Quoteorders::insertCopyData($data);
            
                
            //return confirmation page
            return view('jobs/confirmation', 
                        ['data' =>  $data],
                        ['message' => 'its header copied to Job# '. $newJobID],
                        ['task_message' => $task]
                    );

  

        }elseif($mod == 'copyfull'){

            //insert job  
            $task       = 'Header & Products Copy';
            $data       = json_decode(json_encode($data), true);
            $data       = (object) $data; //convert array back to obj
            
            //remove ID field from array  
            unset($data->job_results['JobID']);

            
            $newJob     = Quoteorders::insertJob($data->job_results);
            $newJobID   = $newJob->JobID;
            $origJob    = Job::select('*')->where('jobid', $jobID)->first();
          

            //remove Primary Keys from copied tables
            unset($data->cus_results['JobID']);
            unset($data->cus_results['HeaderID']);
            unset($data->ship_results['JobID']);
            unset($data->ship_results['HeaderID']);
            unset($data->job_histories['id']);
            unset($data->job_histories['job_id']);
            unset($data->spec_results['ID']);
            unset($data->spec_results['JobID']);

            //add NEW JobID into table data
            $data->cus_results['JobID'] = $newJobID;
            $data->ship_results['JobID'] = $newJobID;
            $data->spec_results['JobID'] = $newJobID;
            $data->job_results['JobID'] = $jobID;

            //insert new JOB QUOTE copied data with new IDs
            Quoteorders::insertData($data);  
            

            //get jobProducts and copy
            $cleanedProduct[] = array();

            foreach($origJob->products as $product){

                //get all mods attached to original copy product
                $origModifications = $product->modifications;

                //remove product ID and jobid
                unset($product->JobID);
                unset($product->ID);

                //add new JobID
                $product->JobID = $newJobID;

                // dd($product);

                //convert product to array for JobProduct::create
                $productArray = json_decode(json_encode($product), true);
                
                //create a new job
                $newJobProduct = JobProduct::create($productArray);

                

                //find and copy all the mods associated withold job_product
                foreach($origModifications as $modification){
                    
                    //remove ID and job_product_id
                        unset($modification->Line);
                        unset($modification->ID);
                        unset($modification->job_product_id);
                        
                    //update jobp id
                    $job_product_id = $newJobProduct->ID;
                    $modification->job_product_id = $job_product_id;

                    //insert modification
                    JobProductModification::insert($modification);
                
                }     
                  
                //add all products to same array
                array_push($cleanedProduct, $newJobProduct);         
                  
            }




            //return confirmation page
            return view('jobs/confirmation', 
                ['data' =>  $data],
                ['message' => 'copied Header, Specifications, Products, Modifications, and any customizations you made over to Job# '. $newJobID],
                ['task_message' => $task],
                ['product_list' => $cleanedProduct]
            );

     

        }else{

            
            //return first page of order (header)
            return view('quote_orders/header', ['data' =>  $dataEdit]);

        }
    }

    public function update($jobID, $job_product_id)
	{
        // dd(Input::get_all());
		$job_product = JobProduct::findOrFail($job_product_id);
		$job_product->update(Input::get_all());
		$job_product->job->updateList();
		
		return $this->product($jobID);
    }
    
    public function deleteModification($jobID, $job_product_id, $modification_id)
    {
        $modification = JobProductModification::findOrFail($modification_id);
		$job = $modification->jobProduct->job;
		$modification->delete();
		$job->updateList();

		return $this->product($jobID);
    }

    public function destroy($JobID, $job_product_id)
	{
		$job_product = JobProduct::findOrFail($job_product_id);
		$job_product->delete();
		$job_product->job->resetProductLines();
		$job_product->job->updateList();

		return $this->product($JobID);
    }

    public function copy($jobID, $productID){
        return view('quote_orders/products/copy', ['jobID' => $jobID, 'productID' => $productID]);

    }

    public function paste(Request $request)
    {
        $copyJobAction  = Input::get('copyJobAction');
        $jobID          = Input::get('jobID');
        $productID      = Input::get('productID');
        $copyJob        = Input::get('copyJob');
        $jobAccts       = Job::select('AccountID')->where('jobid', $jobID)->first();
        $job            = Job::all()->where('JobID', $jobID)->first();
        $jobAcct        = $jobAccts->AccountID;
        // $copyJob        = isset($copyJob) ? $copyJob : "";
        $copyJobAction  = isset($copyJobAction) ? $copyJobAction : "";
        $newJobID       = $jobID;
        $debug          = "";       

        
        if ($copyJobAction == "yes") {

            //send to diff job
            $newJobID                   = $copyJob;
            $copyjobdata                = JobProduct::where('jobID', $jobID)->where('ID', $productID)->first()->toArray();
            $copyjobdata['refnumber']   = $copyjobdata['ID'];
            $copyjobdata['JobID']       = $newJobID;
            // dd($copyjobdata['refnumber']);
            unset($copyjobdata['ID']);

            $resultsProd                = JobProduct::create($copyjobdata);
            // $resultsProdID              = JobProduct::where('JobID', $jobID)->first()->toArray();
            // $origProdID                 = $resultsProdID['ID']; 
            $ids                        = JobProduct::where('refnumber', $copyjobdata['refnumber'])->orderByDesc('ID')->limit(1)->get();
            $copyModdata                = JobProductModification::where('job_product_id', $copyjobdata['refnumber'])->get()->toArray();
            // dd($ids);

            foreach($ids as $sub_id){
                $id = $sub_id->ID;
            }

            
            if($copyModdata != null){
                foreach($copyModdata as $value) {
                    $value['job_product_id'] = $id;
                    // echo "jobproductid: $id <br />";
                    unset($value['ID']);
                    $prodModSQL = JobProductModification::create($value);
                }
                
            }

            $copypartdata = JobProductPart::where('jobID', $jobID)->first();
            if($copypartdata !=null){
                $copypartdata = JobProductPart::where('jobID', $jobID)->first()->toArray();
                unset($copypartdata['ID']);
                $prodPartsSQL = JobProductPart::create($copypartdata);
            }

            // $prodPartsSQL = "INSERT INTO job_product_parts (JobID, AccountID, Line, `Code`, OrigCode, Class, Type, Description, Width, Height, Depth, Qty, UOM, UOMPer, UOMQty, ListPer, ListQty,  CreatedBy, StyleID, StyleMaterialID, StyleColorID, StyleFinishID, CabinetBoxMaterialGroup, CabinetBoxMaterialID, CabinetDrawerBoxID, StyleOverride, CabinetBoxOverride, CabinetDrawerBoxOverride, ProdID) SELECT $jobID, AccountID, Line, `Code`, OrigCode, Class, Type, Description, Width, Height, Depth, Qty, UOM, UOMPer, UOMQty, ListPer, ListQty,  CreatedBy, StyleID, StyleMaterialID, StyleColorID, StyleFinishID, CabinetBoxMaterialGroup, CabinetBoxMaterialID, CabinetDrawerBoxID, StyleOverride, CabinetBoxOverride, CabinetDrawerBoxOverride, ProdID FROM job_product_parts WHERE jobID = $jobID";

            $prodPartsUpdSQL = DB::table(DB::raw('job_product_parts AS jp'))
                                    ->join(DB::raw('job_product AS j'), function($join)
                                    {
                                        $join->on('j.JobID', '=', 'jp.JobID')->on('j.AccountID', '=', 'jp.AccountID')->on('jp.Line', '=', 'j.Line');
                                    })
                                    ->where('j.JobID', '=', $jobID)
                                    ->where('j.AccountID', '=', $jobAcct)
                                    ->where('jp.Line', '=', 'j.Line')
                                    ->update(['jp.ProdID' => 'j.ID']);

            $job = Job::where('jobID', $copyJob)->first();




            // =========== original =============
            // //send to a different job
            // $newJobID = $copyJob;
            // $copyjobdata = JobProduct::where('JobID', $jobID)->where('ID', $productID)->first()->toArray();
            // $copyjobdata['refnumber'] = $copyjobdata['ID'];
            // $copyjobdata['JobID'] = $newJobID;
            // unset($copyjobdata['ID']);
            // $resultsProd = JobProduct::create($copyjobdata);
            
            // //get original job_product->ID
            // $resultsProdID = JobProduct::where('JobID', $jobID)->first()->toArray();
            // $origProdID     = $resultsProdID['ID'];
            // $copyModdata = JobProductModification::where('job_product_id', $origProdID)->get()->toArray();
            
            // if($copyModdata != null){
            //     foreach($copyModdata as $value) {
            //         $value['job_product_id'] = $resultsProd->ID;
            //         unset($value['ID']);
            //         $prodModSQL = JobProductModification::create($value);
            //     }
                
            // }

            // $copypartdata = JobProductPart::where('jobID', $jobID)->first();
            // if($copypartdata !=null){
            //     $copypartdata = JobProductPart::where('jobID', $jobID)->first()->toArray();
            //     unset($copypartdata['ID']);
            //     $prodPartsSQL = JobProductPart::create($copypartdata);
            // }

            // $prodPartsUpdSQL = DB::table(DB::raw('job_product_parts AS jp'))
            //     ->join(DB::raw('job_product AS j'), function($join)
            //     {
            //         $join->on('j.JobID', '=', 'jp.JobID')->on('j.AccountID', '=', 'jp.AccountID')->on('jp.Line', '=', 'j.Line');
            //     })
            //     ->where('j.JobID', '=', $jobID)
            //     ->where('j.AccountID', '=', $jobAcct)
            //     ->where('jp.Line', '=', 'j.Line')
            //     ->update(['jp.ProdID' => 'j.ID']);
               
            //     $pastedJob = Job::where('jobID', $newJobID)->first();
            //     $pastedJob->updateList();

        } else {
        
            //send to current job
            $newJobID = $jobID;
            $copyjobdata = JobProduct::where('jobID', $jobID)->where('ID', $productID)->first()->toArray();
            $copyjobdata['refnumber'] = $copyjobdata['ID'];
            unset($copyjobdata['ID']);
            $resultsProd = JobProduct::create($copyjobdata);

            $resultsProdID = JobProduct::where('jobID', $jobID)->where('ID', $productID)->first()->toArray();
            $origProdID     = $resultsProdID['ID']; 
            $ids = JobProduct::where('refnumber', $origProdID)->orderByDesc('ID')->limit(1)->get();
            
            foreach($ids as $sub_id){
                $id = $sub_id->ID;
            }
            
            $copyModdata = JobProductModification::where('job_product_id', $origProdID)->get()->toArray();

            if($copyModdata != null){
                foreach($copyModdata as $value) {
                    $value['job_product_id'] = $id;
                    unset($value['ID']);
                    $prodModSQL = JobProductModification::create($value);
                }
                
            }

            $copypartdata = JobProductPart::where('jobID', $jobID)->first();
            if($copypartdata !=null){
                $copypartdata = JobProductPart::where('jobID', $jobID)->first()->toArray();
                unset($copypartdata['ID']);
                $prodPartsSQL = JobProductPart::create($copypartdata);
            }
            
            // $prodPartsSQL = "INSERT INTO job_product_parts (JobID, AccountID, Line, `Code`, OrigCode, Class, Type, Description, Width, Height, Depth, Qty, UOM, UOMPer, UOMQty, ListPer, ListQty,  CreatedBy, StyleID, StyleMaterialID, StyleColorID, StyleFinishID, CabinetBoxMaterialGroup, CabinetBoxMaterialID, CabinetDrawerBoxID, StyleOverride, CabinetBoxOverride, CabinetDrawerBoxOverride, ProdID) SELECT $jobID, AccountID, Line, `Code`, OrigCode, Class, Type, Description, Width, Height, Depth, Qty, UOM, UOMPer, UOMQty, ListPer, ListQty,  CreatedBy, StyleID, StyleMaterialID, StyleColorID, StyleFinishID, CabinetBoxMaterialGroup, CabinetBoxMaterialID, CabinetDrawerBoxID, StyleOverride, CabinetBoxOverride, CabinetDrawerBoxOverride, ProdID FROM job_product_parts WHERE jobID = $jobID";

            $prodPartsUpdSQL = DB::table(DB::raw('job_product_parts AS jp'))
                                    ->join(DB::raw('job_product AS j'), function($join)
                                    {
                                        $join->on('j.JobID', '=', 'jp.JobID')->on('j.AccountID', '=', 'jp.AccountID')->on('jp.Line', '=', 'j.Line');
                                    })
                                    ->where('j.JobID', '=', $jobID)
                                    ->where('j.AccountID', '=', $jobAcct)
                                    ->where('jp.Line', '=', 'j.Line')
                                    ->update(['jp.ProdID' => 'j.ID']);
        
        }    
        
        $job->updateList();

        return $this->product($newJobID);

    }


    public function updateProductLine(Request $request)
	{
     
        $job_product_id     = Input::get('productID');
        $JobID              = Input::get('jobID');
        $new_line           = Input::get('Line');        
		$job_product        = JobProduct::findOrFail($job_product_id);
		                      $job_product->updateLine($new_line);

		return $this->product($JobID);
    } 


    public function consIndex()
    {
        $jobID      = session('jobID');
        $jobAcct    = session('jobAcct');
        return view('quote_orders/construction/index', ['JobID' => $jobID, 'JobAcct' => $jobAcct]);
    }
    
    
    public function autocompletesearch(Request $request)
    {
        $searchDescription = $request->get('term');       
        $search_sql_products = "(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM product_groups WHERE";
        $search_sql_accessory = "(SELECT ID, Category, GroupCode, Description, BookDescription, Image_File, ClassBuild, Class, Notes, Type FROM accessory_groups WHERE";
        $search_sql_order = " ORDER BY GroupCode LIMIT 10)";
        $search_union = " UNION ";

        $search_sql = " BookDescription LIKE '%$searchDescription%'";
        $search_tags = " Tags LIKE '%$searchDescription%'";

        //build full query string
        $fullQuery = $search_sql_products . $search_sql . " OR " . $search_tags . $search_sql_order
            .  $search_union .
            $search_sql_accessory . $search_sql . " OR " . $search_tags . $search_sql_order;

        //execute query
        $result = DB::select(DB::raw($fullQuery));
        
        $response = array();
        $i = 0;
        foreach($result as $row){
            
            $response[$i] = array("id"=>$row->ID, 
                                "category"=>$row->Category,
                                "groupcode"=>$row->GroupCode, 
                                "label"=>$row->Description, 
                                "book"=>$row->BookDescription,
                                "image_file"=>$row->Image_File,
                                "classbuild"=>$row->ClassBuild,
                                "class"=>$row->Class,
                                "notes"=>$row->Notes,
                                "type"=>$row->Type,
                                "items" =>array());
            $ItemSQL = "(SELECT Type, ID, Code, Width FROM product_items WHERE GroupCode = '$row->GroupCode' AND Active = 1  ORDER BY Width) UNION (SELECT Type, ID, Code, Width FROM accessory_items WHERE GroupCode = '$row->GroupCode' AND Active = 1  ORDER BY Width)";
            $ItemSQL = DB::select(DB::raw($ItemSQL));
            foreach($ItemSQL as $item){
                $response[$i]['items'][] = array(
                    "type"=>$item->Type,
                    "id"=>$item->ID, 
                    "code"=>$item->Code, 
                    "width"=>$item->Width
                );    
            }
            $i++;
            
        }

      return response()->json($response);
        
    }
    
    public function pricesheetPrint($action, $varDoor, $varMaterial, $varStyle, $varFinish, $varInside, $varOutside, $varCenter, $varStile)
    {
        return view('quote_orders/construction/print', ['action' => $action, 'varDoor' => $varDoor, 'varMaterial' => $varMaterial, 'varStyle' => $varStyle, 'varFinish' => $varFinish, 'varInside' => $varInside, 'varOutside' => $varOutside, 'varCenter' => $varCenter, 'varStile' => $varStile]);
    }

    public function spec_store(Request $request)
    {
        $AdminNote          = "";
        $action              = Input::get('action');
        $JobID               = Input::get('JobID');
        $JobAcct             = Input::get('JobAcct');
        $Load                = Input::get('Load');
        $Save                = Input::get('Save');
        $SaveCustomName      = Input::get('SaveCustomName');
        $brand               = Input::get('brand');
        $styGroup            = Input::get('styGroup');

        $doorSty             = Input::get('doorSty');
        $DoorOutside         = Input::get('DoorOutside');
        $DoorStileRail       = Input::get('DoorStileRail');
        $DoorPeg             = Input::get('DoorPeg');
        $DoorInside          = Input::get('DoorInside');
        $DoorCenterPanel     = Input::get('DoorCenterPanel');
        $DoorHardware        = Input::get('DoorHardware');
        $DoorLocation        = Input::get('DoorLocation');

        $drwSty              = Input::get('drwSty');
        $DrwOutside          = Input::get('DrwOutside');
        $DrwStileRail        = Input::get('DrwStileRail');
        $DoorPeg             = Input::get('DoorPeg');
        $DoorInside          = Input::get('DoorInside');
        $DoorCenterPanel     = Input::get('DoorCenterPanel');
        $DrwHardware         = Input::get('DrwHardware');
        $DrwLocation         = Input::get('DrwLocation');

        $lgdrwSty            = Input::get('lgdrwSty');
        $LgDrwOutside        = Input::get('LgDrwOutside');
        $LgDrwStileRail      = Input::get('LgDrwOutside');
        $LgDrwPeg            = Input::get('LgDrwPeg');
        $LgDrwInside         = Input::get('LgDrwInside');
        $LgDrwCenterPanel    = Input::get('LgDrwCenterPanel');
        $LgDrwHardware       = Input::get('LgDrwHardware');
        $LgDrwLocation       = Input::get('LgDrwLocation');
        $styMatl             = Input::get('styMatl');
        $styMatlCustom       = Input::get('styMatlCustom');
        $styColor            = Input::get('styColor');
        $styColorCustom      = Input::get('styColorCustom');
        $styFinish           = Input::get('styFinish');
        $styFinishCustom     = Input::get('styFinishCustom');
        $cabMatl             = Input::get('cabMatl');
        $drwBox              = Input::get('drwBox');
        $drwRunner           = Input::get('drwRunner');
        $hinge               = Input::get('hinge');
        $cabFinEndMat        = Input::get('cabFinEndMat');
        $cabFinEndColor      = Input::get('cabFinEndColor');
        $cabFinEndFin        = Input::get('cabFinEndFin');
        $cabFinEndEdge       = Input::get('cabFinEndEdge');
        $jobHeaderNotes      = Input::get('jobHeaderNotes');
        $adminNote           = Input::get('adminNote');
        $adminNote           = Input::get('adminNote');
        $toekick             = Input::get('toekick');
        $redirect  				= isset($action) ? $action : 0;
        $jobID 					= isset($JobID) ? $JobID : 0;
        $jobAcct 				= isset($JobAcct) ? $JobAcct : 0;
        $jobCusSty 				= isset($Load) ? $Load : 0;
        $jobSaveSty 			= isset($Save) ? $Save : 0;
        $SaveStyleName 			= isset($SaveCustomName) ? $SaveCustomName : 0;
        $jobBrand 				= isset($brand) ? $brand : 1;
        $jobStyleGroup 			= isset($styGroup) ? $styGroup : 0;

        $jobDoor 				= isset($doorSty) ? $doorSty : 0;
        $jobDoorOutside 		= isset($DoorOutside) ? $DoorOutside : 0;
        $jobDoorSR 				= isset($DoorStileRail) ? $DoorStileRail : 0;
        $jobDoorPeg 			= isset($DoorPeg) ? $DoorPeg : 0;
        $jobDoorInside 			= isset($DoorInside) ? $DoorInside : 0;
        $jobDoorCenter 			= isset($DoorCenterPanel) ? $DoorCenterPanel : 0;
        $jobDoorHardware 		= isset($DoorHardware) ? $DoorHardware : 0;
        $jobDoorLocation 		= isset($DoorLocation) ? $DoorLocation : 0;

        $jobDrw 				= isset($drwSty) ? $drwSty : 0;
        $jobDrwOutside 			= isset($DrwOutside) ? $DrwOutside : 0;
        $jobDrwSR 				= isset($DrwStileRail) ? $DrwStileRail : 0;
        $jobDrwPeg 				= isset($DoorPeg) ? $DoorPeg : 0;
        $jobDrwInside 			= isset($DoorInside) ? $DoorInside : 0;
        $jobDrwCenter 			= isset($DoorCenterPanel) ? $DoorCenterPanel : 0;
        $jobDrwHardware 		= isset($DrwHardware) ? $DrwHardware : 0;
        $jobDrwLocation 		= isset($DrwLocation) ? $DrwLocation : 0;

        $jobLgDrw 				= isset($lgdrwSty) ? $lgdrwSty : 0;
        $jobLgDrwOutside 		= isset($LgDrwOutside) ? $LgDrwOutside : 0;
        $jobLgDrwSR 			= isset($LgDrwStileRail) ? $LgDrwStileRail : 0;
        $jobLgDrwPeg 			= isset($LgDrwPeg) ? $LgDrwPeg : 0;
        $jobLgDrwInside 		= isset($LgDrwInside) ? $LgDrwInside : 0;
        $jobLgDrwCenter 		= isset($LgDrwCenterPanel) ? $LgDrwCenterPanel : 0;
        $jobLgDrwHardware 		= isset($LgDrwHardware) ? $LgDrwHardware : 0;
        $jobLgDrwLocation 		= isset($LgDrwLocation) ? $LgDrwLocation : 0;

        $jobStyleMaterial 		= isset($styMatl) ? $styMatl : 0;
        $jobMaterialGroup 		= DB::select(DB::raw("SELECT MaterialGroupID FROM style_material WHERE ID = $jobStyleMaterial"));
        foreach ($jobMaterialGroup as $MGrow){
            $MaterialGroupID = $MGrow->MaterialGroupID;
        }
        $jobStyleMaterialGroup 	= isset($MaterialGroupID) ? $MaterialGroupID : 0;
        $jobStyleMaterialCustom = isset($styMatlCustom) ? $styMatlCustom : "";


        $jobStyleColor 			= isset($styColor) ? $styColor : 0;
        $jobColorGroup 			= DB::select(DB::raw("SELECT ColorGroupID FROM style_color WHERE ID = $jobStyleColor"));
        foreach ($jobColorGroup as $CGrow ){
            $ColorGroupID       = $CGrow->ColorGroupID;
        }
        $jobStyleColorGroup 	= isset($ColorGroupID) ? $ColorGroupID  : 0;
        $jobStyleColorCustom 	= isset($styColorCustom) ? $styColorCustom : "";

        $jobStyleFinish 		= isset($styFinish) ? $styFinish : 0;
        $jobFinishGroup 		= DB::select(DB::raw("SELECT FinishGroupID FROM style_finish WHERE ID = $jobStyleFinish"));
        foreach($jobFinishGroup as $FGrow){
            $FinishGroupID      = $FGrow->FinishGroupID;
        }
        $jobStyleFinishGroup 	= isset($FinishGroupID) ? $FinishGroupID : 0;
        $jobStyleFinishCustom 	= isset($styFinishCustom) ? $styFinishCustom : "";


        $jobCabinetMaterial 	= $cabMatl;
        $jobBoxMaterialGroup 	= DB::select(DB::raw("SELECT MaterialGroup FROM cabinet_material WHERE ID = $jobCabinetMaterial"));
        foreach ($jobBoxMaterialGroup as $CBMGrow){
            $jobCabBoxMaterialGroup = $CBMGrow->MaterialGroup;
        }

        $jobCabinetDrwBox 		= $drwBox;
        $jobCabinetDrwBoxRunner = $drwRunner;
        $jobCabinetHinge 		= $hinge;

        $jobCabinetExtMaterial 	= $cabFinEndMat;
        $jobCabinetExtColor 	= $cabFinEndColor;
        $jobCabinetExtFinish 	= $cabFinEndFin;
        $jobCabinetExtEdge 		= $cabFinEndEdge;

        $jobCabinetIntMaterial 	= 99; //$_POST["cabIntEdge"];
        $jobCabinetIntColor 	= 99; //$_POST["cabIntEdge"];
        $jobCabinetIntFinish 	= 99; //$_POST["cabIntEdge"];
        $jobCabinetIntEdge 		= 99; //$_POST["cabIntEdge"];

        $jobCabinetTK 			= 0; //$_POST["cabTK"];
        $jobCabinetToeKick 		= isset($toekick) ? $toekick : 0;

        

        if(isset($jobHeaderNotes)) {
        $jobHeaderNotes			= htmlspecialchars($jobHeaderNotes);
        // $jobHeaderNotes			= $jobHeaderNotes;
        }

        if(isset($adminNote)) {
        $AdminNote			= htmlspecialchars($adminNote);
        // $AdminNote			= $adminNote;
        }

        $UserID    				= session('userid');
        $UserAccID    			= session('accountid');
        $SaveStyleID			= 0;
        
    $inserdata = array(
            'JobID'                     => $jobID,
            'AccountID'                 => $jobAcct,
            'Name'                      => $SaveStyleName,
            'BrandID'                   => $jobBrand,
            'StyleGroupID'              => $jobStyleGroup,
            'DoorStyleID'               => $jobDoor,
            'DoorOutsideProfileID'      => $jobDoorOutside,
            'DoorStileRailID'           => $jobDoorSR,
            'DoorPegID'                 => $jobDoorPeg,
            'DoorInsideProfileID'       => $jobDoorInside,
            'DoorCenterPanelID'         => $jobDoorCenter,
            'DoorHardwareID'            => $jobDoorHardware,
            'DoorHardwareLocationID'    => $jobDoorLocation,
            'DrawerStyleID'             => $jobDrw,
            'DrawerOutsideProfileID'    => $jobDrwOutside,
            'DrawerStileRailID'         => $jobDrwSR,
            'DrawerPegID'               => $jobDrwPeg,
            'DrawerInsideProfileID'     => $jobDrwInside,
            'DrawerCenterPanelID'       => $jobDrwCenter,
            'DrawerHardwareID'          => $jobDrwHardware,
            'DrawerHardwareLocationID'  => $jobDrwLocation,
            'LargeDrawerStyleID'        => $jobLgDrw,
            'LargeDrawerOutsideProfileID'=> $jobLgDrwOutside,
            'LargeDrawerStileRailID'    => $jobLgDrwSR,
            'LargeDrawerPegID'          => $jobLgDrwPeg,
            'LargeDrawerInsideProfileID'=> $jobLgDrwInside,
            'LargeDrawerCenterPanelID'  => $jobLgDrwCenter,
            'LargeDrawerHardwareID'     => $jobLgDrwHardware,
            'LargeDrawerHardwareLocationID'=> $jobLgDrwLocation,
            'StyleMaterialID'           => $jobStyleMaterial,
            'StyleMaterialGroupID'      => $jobStyleMaterialGroup,
            'StyleColorID'              => $jobStyleColor,
            'StyleColorGroupID'         => $jobStyleColorGroup,
            'StyleFinishID'             => $jobStyleFinish,
            'StyleFinishGroupID'        => $jobStyleFinishGroup,
            'CabinetBoxMaterialID'      => $jobCabinetMaterial,
            'CabinetBoxMaterialGroup'   => $jobCabBoxMaterialGroup,
            'CabinetDrawerBoxID'        => $jobCabinetDrwBox,
            'DrwRunnerID'               => $jobCabinetDrwBoxRunner,
            'CabinetHingeID'            => $jobCabinetHinge,
            'CabinetExteriorMaterialID' => $jobCabinetExtMaterial,
            'CabinetExteriorColorID'    => $jobCabinetExtColor,
            'CabinetExteriorFinishID'   => $jobCabinetExtFinish,
            'CabinetExteriorEdgeID'     => $jobCabinetExtEdge,
            'CabinetInteriorMaterialID' => $jobCabinetIntMaterial,
            'CabinetInteriorColorID'    => $jobCabinetIntColor,
            'CabinetInteriorFinishID'   => $jobCabinetIntFinish,
            'CabinetInteriorEdgeID'     => $jobCabinetIntEdge,
            'CabinetToeHeightID'        => $jobCabinetTK,
            'CreatedBy'	                => $UserID
            );


        if($jobSaveSty == 1){
            DB::table('acct_custom_styles')->insert($inserdata);

        } 

        //if job spec exists
        $SpecLookup = DB::select(DB::raw("SELECT job_construction.ID FROM job_construction WHERE job_construction.JobID = '$jobID'"));

        //if cabinet material changes entire job total needs recalculating. 
        
        
        if (count($SpecLookup)) {
            $updatedata = array(
                                'JobID' => $jobID,
                                'HeaderNote' => $jobHeaderNotes,
                                'AdminNote' => $AdminNote,
                                'AccountID' => $jobAcct,
                                'BrandID' => $jobBrand,
                                'StyleGroupID' => $jobStyleGroup,

                                'DoorStyleID' => $jobDoor,
                                'DoorOutsideProfileID' => $jobDoorOutside,
                                'DoorStileRailID' => $jobDoorSR,
                                'DoorPegID' => $jobDoorPeg,
                                'DoorInsideProfileID' => $jobDoorInside,
                                'DoorCenterPanelID' => $jobDoorCenter,
                                'DoorHardwareID' => $jobDoorHardware,
                                'DoorHardwareLocationID' => $jobDoorLocation,

                                'DrawerStyleID' => $jobDrw,
                                'DrawerOutsideProfileID' => $jobDrwOutside,
                                'DrawerStileRailID' => $jobDrwSR,
                                'DrawerPegID' => $jobDrwPeg,
                                'DrawerInsideProfileID' => $jobDrwInside,
                                'DrawerCenterPanelID' => $jobDrwCenter,
                                'DrawerHardwareID' => $jobDrwHardware,
                                'DrawerHardwareLocationID' => $jobDrwLocation,

                                'LargeDrawerStyleID'=> $jobLgDrw,
                                'LargeDrawerOutsideProfileID' => $jobLgDrwOutside,
                                'LargeDrawerStileRailID' => $jobLgDrwSR,
                                'LargeDrawerPegID'=> $jobLgDrwPeg,
                                'LargeDrawerInsideProfileID' => $jobLgDrwInside,
                                'LargeDrawerCenterPanelID' => $jobLgDrwCenter,
                                'LargeDrawerHardwareID'=> $jobLgDrwHardware,
                                'LargeDrawerHardwareLocationID' => $jobLgDrwLocation,

                                'StyleMaterialID' => $jobStyleMaterial,
                                'StyleMaterialGroupID' => $jobStyleMaterialGroup,
                                'StyleMaterialCustom' => $jobStyleMaterialCustom,

                                'StyleColorID' => $jobStyleColor,
                                'StyleColorGroupID' => $jobStyleColorGroup,
                                'StyleColorCustom' => $jobStyleColorCustom,

                                'StyleFinishID' => $jobStyleFinish,
                                'StyleFinishGroupID' => $jobStyleFinishGroup,
                                'StyleFinishCustom' => $jobStyleFinishCustom,

                                'CabinetBoxMaterialID' => $jobCabinetMaterial,
                                'CabinetBoxMaterialGroup' => $jobCabBoxMaterialGroup,
                                'CabinetDrawerBoxID' => $jobCabinetDrwBox,
                                'DrwRunnerID' => $jobCabinetDrwBoxRunner,
                                'CabinetHingeID' => $jobCabinetHinge,

                                'CabinetExteriorMaterialID' => $jobCabinetExtMaterial,
                                'CabinetExteriorColorID' => $jobCabinetExtColor,
                                'CabinetExteriorFinishID' => $jobCabinetExtFinish,
                                'CabinetExteriorEdgeID' => $jobCabinetExtEdge,
                                'CabinetInteriorMaterialID' => $jobCabinetIntMaterial,
                                'CabinetInteriorColorID' => $jobCabinetIntColor,
                                'CabinetInteriorFinishID' => $jobCabinetIntFinish,
                                'CabinetInteriorEdgeID' => $jobCabinetIntEdge,
                                'CabinetToeHeightID' => $jobCabinetTK,
                                'CabinetToeKickID' => $jobCabinetToeKick,
                                'ModifiedBy' => $UserID,
                                'LoadName' => $SaveStyleName,
                                'LoadID' => $SaveStyleID
                            );
                DB::table('job_construction')->where('JobID', '=', $jobID)->update($updatedata);  
                
            } else { 
                $jobCreatedDate	 = date("Y-m-d H:i:s");
                $jobModifiedDate = date("Y-m-d H:i:s");
                $jobCabinetDrwRunner = isset($jobCabinetDrwRunner) ? $jobCabinetDrwRunner : 0;
                $insertcons = array(
                                    'JobID' => $jobID,
                                    'HeaderNote' => $jobHeaderNotes,
                                    'AdminNote' => $AdminNote,
                                    'AccountID' => $jobAcct,
                                    'BrandID' => $jobBrand,
                                    'StyleGroupID' => $jobStyleGroup,

                                    'DoorStyleID' => $jobDoor,
                                    'DoorOutsideProfileID' => $jobDoorOutside,
                                    'DoorStileRailID' => $jobDoorSR,
                                    'DoorPegID' => $jobDoorPeg,
                                    'DoorInsideProfileID' => $jobDoorInside,
                                    'DoorCenterPanelID' => $jobDoorCenter,
                                    'DoorHardwareID' => $jobDoorHardware,
                                    'DoorHardwareLocationID' => $jobDoorLocation,

                                    'DrawerStyleID' => $jobDrw,
                                    'DrawerOutsideProfileID' => $jobDrwOutside,
                                    'DrawerStileRailID' => $jobDrwSR,
                                    'DrawerPegID' => $jobDrwPeg,
                                    'DrawerInsideProfileID' => $jobDrwInside,
                                    'DrawerCenterPanelID' => $jobDrwCenter,
                                    'DrawerHardwareID' => $jobDrwHardware,
                                    'DrawerHardwareLocationID' => $jobDrwLocation,

                                    'LargeDrawerStyleID'=> $jobLgDrw,
                                    'LargeDrawerOutsideProfileID' => $jobLgDrwOutside,
                                    'LargeDrawerStileRailID' => $jobLgDrwSR,
                                    'LargeDrawerPegID'=> $jobLgDrwPeg,
                                    'LargeDrawerInsideProfileID' => $jobLgDrwInside,
                                    'LargeDrawerCenterPanelID' => $jobLgDrwCenter,
                                    'LargeDrawerHardwareID'=> $jobLgDrwHardware,
                                    'LargeDrawerHardwareLocationID' => $jobLgDrwLocation,

                                    'StyleMaterialID' => $jobStyleMaterial,
                                    'StyleMaterialGroupID' => $jobStyleMaterialGroup,
                                    'StyleMaterialCustom' => $jobStyleMaterialCustom,

                                    'StyleColorID' => $jobStyleColor,
                                    'StyleColorGroupID' => $jobStyleColorGroup,
                                    'StyleColorCustom' => $jobStyleColorCustom,

                                    'StyleFinishID' => $jobStyleFinish,
                                    'StyleFinishGroupID' => $jobStyleFinishGroup,
                                    'StyleFinishCustom' => $jobStyleFinishCustom,

                                    'CabinetBoxMaterialID' => $jobCabinetMaterial,
                                    'CabinetBoxMaterialGroup' => $jobCabBoxMaterialGroup,
                                    'CabinetDrawerBoxID' => $jobCabinetDrwBox,
                                    'DrwRunnerID' => $jobCabinetDrwRunner,
                                    'CabinetHingeID' => $jobCabinetHinge,

                                    'CabinetExteriorMaterialID' => $jobCabinetExtMaterial,
                                    'CabinetExteriorColorID' => $jobCabinetExtColor,
                                    'CabinetExteriorFinishID' => $jobCabinetExtFinish,
                                    'CabinetExteriorEdgeID' => $jobCabinetExtEdge,


                                    'CabinetInteriorMaterialID' => $jobCabinetIntMaterial,
                                    'CabinetInteriorColorID' => $jobCabinetIntColor,
                                    'CabinetInteriorFinishID' => $jobCabinetIntFinish,
                                    'CabinetInteriorEdgeID' => $jobCabinetIntEdge,

                                    'CabinetToeHeightID' => $jobCabinetTK,
                                    'CreatedBy' => $UserID,
                                    'CreatedDate' => $jobCreatedDate,
                                    'ModifiedDate' => $jobModifiedDate,
                                    'LoadName' => $SaveStyleName,
                                    'LoadID' => $SaveStyleID
                                    );
                DB::table('job_construction')->insert($insertcons);
            
        }


        //UPDATE 
        // DB::table('job')->where('JobID', '=', $jobID)->update(['Brand' => $jobBrand]);  
        
        return $this->getgeneralnotes($JobID, $JobAcct);
    }
    
}
