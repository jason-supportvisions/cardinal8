<?php

class CatalogCategoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }
        
		$categories = CatalogCategory::all();

		return View::make('catalog.categories.index', compact(
			'categories'
		));
	}

	public function store()
	{
	    if(!Auth::user()->hasAccountType('master-administrator')){
            App::abort(403, 'Unauthorized action.');
        }
        
		foreach(Input::get('categories') as $id => $data){
			CatalogCategory::find($id)->update($data);
		}

		return Redirect::back()->with('success', 'updates saved');
	}




}
