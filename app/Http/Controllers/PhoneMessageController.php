<?php
namespace App\Http\Controllers;
use App\Model\PhoneMessage;
use App\Model\Input;
use Carbon\Carbon;
use App\Model\User;

class PhoneMessageController extends Controller {

	 public function __construct()
    {
        $this->middleware('auth');
	}
	
	public function index()
	{
		if(session('username')){
			$phone_messages = PhoneMessage::with('assignees')->get();
		
			return view('phone-message.index')
				->with('phone_messages', $phone_messages);

        }else{
            return redirect()->route('home');
		}
		
		
	}

	public function create()
	{
		$phone_message = new PhoneMessage([
			'datetime' => Carbon::now()
		]);

		$user_list = User::where('IncludeInPhoneLog', '=', 1)
						->orderBy('LastName', 'ASC')
						->orderby('FirstName', 'ASC')
						->get()
						->pluck('email', 'UserID');

		return view('phone-message.create', compact('phone_message', 'user_list'));
	}

	public function store()
	{
		$validator = PhoneMessage::validator(Input::get_all());

        if($validator->fails()){
            return Redirect()->back()->withInput()->withErrors($validator);
        }

		$phone_message = PhoneMessage::create([
			'created_by' => session('userid')
		]);

		$phone_message->setAll(Input::get_all());

		return Redirect()->to('/crm/phone-log')->with('success', 'message created');
	}

	public function show($id)
	{
		$phone_message = PhoneMessage::find($id);

		return view('phone-message.show', compact('phone_message'));
	}

	public function edit($id)
	{
		$phone_message = PhoneMessage::find($id);

		$user_list = User::where('IncludeInPhoneLog', '=', 1)
						->orderBy('LastName', 'ASC')
						->orderby('FirstName', 'ASC')
						->get()
						->pluck('email', 'UserID');
		
		return view('phone-message.edit', compact('phone_message', 'user_list')
		);
	}

	public function update($id)
	{
		$validator = PhoneMessage::validator(Input::get_all());

        if($validator->fails()){
            return Redirect()->back()->withInput()->withErrors($validator);
        }

		PhoneMessage::find($id)->setAll(Input::get_all());

		return Redirect()->to('crm/phone-log')->with('success', 'message updated');
	}

	public function destroy($id)
	{
		PhoneMessage::find($id)->delete();

		return Redirect()->to('/phone-log')->with('success', 'message deleted');
	}



}
