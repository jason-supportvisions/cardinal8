<?php 
namespace App\Http\Controllers;
use App\Model\Job;
use App\Model\JobStage;


class QuotesController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
	{
        if(session('username')){ 
            
            $stage_ids = JobStage::where('pre_process', '=', 1)->get()->pluck('ID');
            $quote_stage = JobStage::where('slug', 'quote')->first();
            $quote_substages = $quote_stage->substages;
            $page_title = "Quotes";
            // dd("quote substages = " . $quote_substages);
            // dd($stage_ids);
            $jobs = Job::getExpandedSetByStageId($stage_ids);

            /*
            "quote substages = [
                {"id":1,"stage_id":1,"slug":"received_order","name":"Received Order","color":"#FF4444","order":1},
                {"id":2,"stage_id":1,"slug":"sent_for_sign_off","name":"Sent for Sign Off","color":"#ffeb3b","order":2},
                {"id":3,"stage_id":1,"slug":"received_sign_off","name":"Received Sign Off","color":"#00C851","order":3}
                ] ◀" 
            */

            return view('orders/index')
                ->with('page_title', $page_title)
                ->with('job_stage_slug', 'quote')
                ->with('stage_ids_list', $stage_ids)
                ->with('substages', $quote_substages)
                ->with('jobs', $jobs)
                ->with('initial_ordered_column', 2);

        }else{
            return redirect()->route('home');
        }
       
	}
}