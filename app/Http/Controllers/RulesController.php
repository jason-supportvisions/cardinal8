<?php

namespace App\Http\Controllers;
use App\Model\RulesMouldings;

class RulesController extends Controller
{
    
    public static function Mouldings($RuleName,$ID)
    {

        //RUN RULES ENGINE
        $Rule               = RulesMouldings::where('Name',$RuleName)->first();  
        $Search = "none";
        $buttonLink = "none";
        $buttonColor = "none";
        $buttonText = "Available (no rule)";
        $Notes = "none";

        $Name               = $Rule->Name;
        $Search             = $Rule->CodeSearchString;
        $ClassBuild         = $Rule->ClassBuild;
        $AllowIDs           = $Rule->AllowIDs;
        $RefuseIDs          = $Rule->RefuseIDs;
        $AllowMessage       = $Rule->AllowMessage;
        $RefuseMessage      = $Rule->RefuseMessage;
        $Notes              = $Rule->Notes;
        $buttonText         = "Not available because of MATERIAL choice";
        $buttonColor        = "btn-secondary";
        $buttonLink         = false;
        $matchArray         = explode(",",$AllowIDs);
        
        //REFUSED IDs if NULL - means all are refused that are not in the ALLOWEDIDS list
        if(isset($RefuseIDs)){
            //make array of ids
            $refuseArray    = explode(",",$RefuseIDs);
        }else{
            //set flag to use ALL
            $refuseArray    = "ALL";
        }
        
        //ALLOWED ID MATCH the material IDs? 
        if (in_array($ID, $matchArray)) {
            if(str_contains($Name,$Search)){
                $buttonLink         = true;
                $buttonColor        = "btn-success";
                $buttonText         = $AllowMessage;
            }else{
                $buttonLink         = false;
                $buttonColor        = "btn-secondary";
                $buttonText         = $RefuseMessage ;
            }
        }

        // //REFUSED ID MATCH  
        // if (in_array($ID, $refuseArray)) {        
        // }
        
        //Build Return Object
        $rulesObject = [[
            'searchID' => $ID,
            'name' => $Search,
            'show' => $buttonLink,
            'btnColor' => $buttonColor,
            'btnText' => $buttonText,
            'notes' => $Notes
        ]];
  
        return $rulesObject;
    }
    
}
