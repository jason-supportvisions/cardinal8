<?php

namespace App\Http\Controllers;

use App\Model\Input;
use App\Model\Download;
use Illuminate\Http\Request;

class DownloadsController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session('username')){
 
            $allDownloads = Download::all();
            return view('download/index')->with('data', $allDownloads);
        }else{
            return redirect()->route('home');
        }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function show($id)
    {
        $showitem = Download::where('ID', $id)->get();
        return view('download/show')->with('news', $showitem);
    }

    public function edit($id)
    {
        $edititem = Download::where('ID', $id)->get();
        return view('download/edit')->with('editItem', $edititem);
    }

    public function update(Request $request)
    {
        $filename               = '';
        $id                     = Input::get('id');
        $downloadfield          = Download::find($id);        
        $uploadedfile           = $request->file('loadfile');
        $oldfilename            = Input::get('oldfilename');
        if ($request->hasfile('loadfile')) {         
            $filename = $uploadedfile->getClientOriginalName();
            $uploadedfile->move(public_path() . '/downloads/' , $filename);
        }else{
            $filename = $oldfilename;
        }

        $downloadfield->Type    = Input::get('type');
        $downloadfield->Subject = Input::get('subject');
        $downloadfield->File    = $filename;
        $downloadfield->save();

        return  redirect('/download');      

    }

    public function create()
    {
        return view('download/create');
    }

    public function store(Request $request)
    {
        $filename               = '';
        $uploadedfile           = $request->file('loadfile');
        if ($request->hasfile('loadfile')) {          
            $filename = $uploadedfile->getClientOriginalName();
            $uploadedfile->move(public_path() . '/downloads/' , $filename);
        }
        $newDownload            = new Download;
        $newDownload->Type      = Input::get('type');
        $newDownload->Subject   = Input::get('subject');
        $newDownload->File      = $filename;
        $newDownload->save();

        return  redirect('/download'); 
    }

    public function destroy($id)
    {
        Download::where('ID', $id)->delete();

        return redirect()->back()->with('success', 'Deleted successfully');
    }
    
}
