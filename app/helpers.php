<?php

use App\Model\Rules;
use Illuminate\Database\Eloquent\Model;


if (!function_exists('YearlyUpCharge')) {
    function YearlyUpCharge($List, $YearlyMultiplier){
        return $List + ($List * $YearlyMultiplier);
    }
}



if (!function_exists('getORDCode')) {
	function getORDCode($product)
	{
		
		$debug 			= "";
		$customPrefix 	= "";
		$customSuffix 	= "";
		$ordType 		= 0;
		$concatCode 	= "";
		$ordDimensions 	= 0;
		$notOriginalCheck = false;

		// return $product; 
		if(isset($product->product_type)){
			$productType = $product->product_type;
		}else{
			$productType	= $product->Type;
		}

		if ($productType == "accessory") {
			$itemStr    	= "accessory";
			$origWidth			= $product->accessory_item->Width;
			$origHeight			= $product->accessory_item->Height;
			$origDepth 			= $product->accessory_item->Depth;
		} else {
			$itemStr    	= "product";
			$origWidth			= $product->product_item->Width;
			$origHeight			= $product->product_item->Height;
			$origDepth 			= $product->product_item->Depth;

		}

		$itemType           = $itemStr . "_item";
		$groupType			= $itemStr . "_group";
		$Code 				= $product->Code;
		$ItemCode 			= $Code;

		//get CODE from item if new
		if(!isset($Code)){
			$Code = $product->$itemType->Code;
			$ItemCode = $Code;
		}

		//get ORD Settings from Manager / Group
		if (isset($product->$itemType->GroupCode)) {
			$concatCode = $product->$itemType->GroupCode;
		}

		//show this PREFIX string only on ORD CRM processes
		if (isset($product->$itemType->$groupType->ORDPrefix)) {
			$customPrefix = $product->$itemType->$groupType->ORDPrefix;
		}

		//show this PREFIX string only on ORD CRM processes
		if (isset($product->$itemType->$groupType->ORDSuffix)) {
			$customSuffix = $product->$itemType->$groupType->ORDSuffix;
		}

		//show ITEM code only OR GROUP CODE as usual
		if (isset($product->$itemType->$groupType->ORDCodeType)) {
			$ordType = $product->$itemType->$groupType->ORDCodeType;
		}

		//show the dimensions if changed
		if (isset($product->$itemType->$groupType->ORDDimensions)) {
			$ordDimensions  = $product->$itemType->$groupType->ORDDimensions;
		}

		//ORDCodeType
			//unchecked display the ITEM code without any dimensions
			//checked show the normal group code with all the dimensions filled in where __ exists
			if ($ordType === 0) {
				$groupCode 		 	= $Code; //items code
				$concatGroupCode 	= $groupCode;
				$needDash 		 	= '-';
			} else {
				$groupCode 		 	= $concatCode; //groupCode
				$concatGroupCode 	= $groupCode;
				$needDash 		 	= '-';
			}
		
		//Check Dimensions
			//dimensions from the job_product / construction
			//give originally if nothing saved
			if (isset($product->Width)) {
				$Width 			= $product->Width;
				$Height 		= $product->Height;
				$Depth 			= $product->Depth;
			}else {
				$Width			= $origWidth;
				$Height			= $origHeight;
				$Depth 			= $origDepth;
			} 
		
			//are any of the dimensions different than original?
			if($Width != $origWidth){ $notOriginalCheck = true; }
			if($Height != $origHeight){ $notOriginalCheck = true; }
			if($Depth != $origDepth){ $notOriginalCheck = true; }

			//force showing dimensions?
			if ($ordDimensions === 1) {
				$showDimensions 	= TRUE;
			} else {
				$showDimensions 	= FALSE;
			}

			//dimensions have changed?
			if($notOriginalCheck === true){
				$showDimensions = TRUE;
			}

			//create string used in CODE
			$ORDCodeDimensions = round($Width, 4) . "x" . round($Height, 4)  . "x" . round($Depth, 4) . $needDash;
		
		//Massage CODES to properly escape necessary characters
		$codeDashLocation = strcspn($concatCode, "-", 0);
		$codeUnderscoreLocation = strcspn($groupCode, "__", 0);
		$leftParenLocation = strcspn($concatGroupCode, "(", 0);
		$rightParenLocation = strcspn($concatGroupCode, ")", 0);
		$underscoreLocation = strcspn($concatGroupCode, "__", 0);
		$plusLocation = strcspn($concatGroupCode, "+", 0);
		$concatGroupCode = substr($concatGroupCode, 0, $plusLocation);
		$parenthesisData = substr($concatGroupCode, $leftParenLocation);
		$concatGroupCode = substr($concatGroupCode, 0, $leftParenLocation);
		$initDimension = substr($concatGroupCode, ($underscoreLocation + 2), 2);
		$initDimensionNumber = is_numeric($initDimension);
		$initDash = strpos($initDimension, "-");
		$initDashNumber = is_numeric($initDash);
		
		//no dash 
		if ($initDimensionNumber == true && $initDashNumber == false) {
			$concatGroupCode = str_replace($initDimension, "", $concatGroupCode);
		} 		

		//if dims are 0
		if (stristr($ORDCodeDimensions, "0x0x0")) {
			$ORDCodeDimensions = "";
		}


		if ($showDimensions === TRUE) {
			$ORDCode = str_replace("__", $ORDCodeDimensions, $concatGroupCode);
		} else {
			$ORDCode = str_replace("__", "", $concatGroupCode);
		}

		
		$ORDCode = str_replace("--", "-", $ORDCode);
		$ORDCode = rtrim($ORDCode, "-");
		if ($customPrefix != "") {
			$ORDCode = $customPrefix . $ORDCode;
		}
		if ($customSuffix != "") {
			$ORDCode = $ORDCode . $customSuffix;
		}

		//if no dimensions changed, simply return the itemcode
		if($Width == $origWidth && $Height == $origHeight && $Depth == $origDepth){
			$ORDCode = $ItemCode;
		}

		// return $ORDCode . "" . $Width.$origWidth."".$Height.$origHeight."".$Depth.$origDepth;
		// return $ORDCode . " " . $ItemCode;
		return $ORDCode;	
	}
}



?>